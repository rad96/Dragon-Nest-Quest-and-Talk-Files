<VillageServer>

function sq08_282_test_sorceress1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 229 then
		sq08_282_test_sorceress1_OnTalk_n229_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		sq08_282_test_sorceress1_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n229_sorceress_tara--------------------------------------------------------------------------------
function sq08_282_test_sorceress1_OnTalk_n229_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n229_sorceress_tara-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n229_sorceress_tara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n229_sorceress_tara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n229_sorceress_tara-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n229_sorceress_tara-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n229_sorceress_tara-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n229_sorceress_tara-1-b" then 
	end
	if npc_talk_index == "n229_sorceress_tara-1-c" then 
	end
	if npc_talk_index == "n229_sorceress_tara-1-d" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n229_sorceress_tara-2-b" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-c" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-d" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-d" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-d" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-e" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-c" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-f" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-f" then 
	end
	if npc_talk_index == "n229_sorceress_tara-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n229_sorceress_tara-2-f" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-e" then 
	end
	if npc_talk_index == "n229_sorceress_tara-3-b" then 
	end
	if npc_talk_index == "n229_sorceress_tara-3-c" then 
	end
	if npc_talk_index == "n229_sorceress_tara-3-d" then 
	end
	if npc_talk_index == "n229_sorceress_tara-3-e" then 
	end
	if npc_talk_index == "n229_sorceress_tara-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n229_sorceress_tara-4-member1" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n229_sorceress_tara-4-a";

				else
									npc_talk_index = "n229_sorceress_tara-4-b";

				end
	end
	if npc_talk_index == "n229_sorceress_tara-4-changemap1" then 
				api_user_ChangeMap(userObjID,13010,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function sq08_282_test_sorceress1_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n035_soceress_master_tiana-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n035_soceress_master_tiana-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2820, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2820, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2820, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2820, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2820, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2820, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2820, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2820, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2820, false);
			 end 

	end
	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest(userObjID,282, 1);
				api_quest_SetJournalStep(userObjID,282, 1);
				api_quest_SetQuestStep(userObjID,282, 1);
				npc_talk_index = "n035_soceress_master_tiana-6";
				api_quest_SetQuestStep(userObjID, questID,6);

	end
	if npc_talk_index == "n035_soceress_master_tiana-5-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2820, true);
				 api_quest_RewardQuestUser(userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2820, true);
				 api_quest_RewardQuestUser(userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2820, true);
				 api_quest_RewardQuestUser(userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2820, true);
				 api_quest_RewardQuestUser(userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2820, true);
				 api_quest_RewardQuestUser(userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2820, true);
				 api_quest_RewardQuestUser(userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2820, true);
				 api_quest_RewardQuestUser(userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2820, true);
				 api_quest_RewardQuestUser(userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2820, true);
				 api_quest_RewardQuestUser(userObjID, 2820, questID, 1);
			 end 
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 283, 1);
					api_quest_SetQuestStep(userObjID, 283, 1);
					api_quest_SetJournalStep(userObjID, 283, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n035_soceress_master_tiana-1", "sq08_283_test_sorceress2.xml");
			return;
		end
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-g" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1" then 
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_282_test_sorceress1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 282);
end

function sq08_282_test_sorceress1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 282);
end

function sq08_282_test_sorceress1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 282);
	local questID=282;
end

function sq08_282_test_sorceress1_OnRemoteStart( userObjID, questID )
				api_quest_SetQuestStep(userObjID, questID,6);
end

function sq08_282_test_sorceress1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_282_test_sorceress1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,282, 1);
				api_quest_SetJournalStep(userObjID,282, 1);
				api_quest_SetQuestStep(userObjID,282, 1);
				npc_talk_index = "n035_soceress_master_tiana-6";
				api_quest_SetQuestStep(userObjID, questID,6);
end

</VillageServer>

<GameServer>
function sq08_282_test_sorceress1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 229 then
		sq08_282_test_sorceress1_OnTalk_n229_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		sq08_282_test_sorceress1_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n229_sorceress_tara--------------------------------------------------------------------------------
function sq08_282_test_sorceress1_OnTalk_n229_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n229_sorceress_tara-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n229_sorceress_tara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n229_sorceress_tara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n229_sorceress_tara-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n229_sorceress_tara-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n229_sorceress_tara-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n229_sorceress_tara-1-b" then 
	end
	if npc_talk_index == "n229_sorceress_tara-1-c" then 
	end
	if npc_talk_index == "n229_sorceress_tara-1-d" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n229_sorceress_tara-2-b" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-c" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-d" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-d" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-d" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-e" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-c" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-f" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-f" then 
	end
	if npc_talk_index == "n229_sorceress_tara-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n229_sorceress_tara-2-f" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-e" then 
	end
	if npc_talk_index == "n229_sorceress_tara-3-b" then 
	end
	if npc_talk_index == "n229_sorceress_tara-3-c" then 
	end
	if npc_talk_index == "n229_sorceress_tara-3-d" then 
	end
	if npc_talk_index == "n229_sorceress_tara-3-e" then 
	end
	if npc_talk_index == "n229_sorceress_tara-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n229_sorceress_tara-4-member1" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n229_sorceress_tara-4-a";

				else
									npc_talk_index = "n229_sorceress_tara-4-b";

				end
	end
	if npc_talk_index == "n229_sorceress_tara-4-changemap1" then 
				api_user_ChangeMap( pRoom, userObjID,13010,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function sq08_282_test_sorceress1_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n035_soceress_master_tiana-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n035_soceress_master_tiana-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, false);
			 end 

	end
	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,282, 1);
				api_quest_SetJournalStep( pRoom, userObjID,282, 1);
				api_quest_SetQuestStep( pRoom, userObjID,282, 1);
				npc_talk_index = "n035_soceress_master_tiana-6";
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);

	end
	if npc_talk_index == "n035_soceress_master_tiana-5-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2820, questID, 1);
			 end 
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 283, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 283, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 283, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n035_soceress_master_tiana-1", "sq08_283_test_sorceress2.xml");
			return;
		end
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-g" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_282_test_sorceress1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 282);
end

function sq08_282_test_sorceress1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 282);
end

function sq08_282_test_sorceress1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 282);
	local questID=282;
end

function sq08_282_test_sorceress1_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
end

function sq08_282_test_sorceress1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_282_test_sorceress1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,282, 1);
				api_quest_SetJournalStep( pRoom, userObjID,282, 1);
				api_quest_SetQuestStep( pRoom, userObjID,282, 1);
				npc_talk_index = "n035_soceress_master_tiana-6";
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
end

</GameServer>