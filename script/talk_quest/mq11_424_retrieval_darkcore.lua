<VillageServer>

function mq11_424_retrieval_darkcore_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 39 then
		mq11_424_retrieval_darkcore_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n039_bishop_ignasio--------------------------------------------------------------------------------
function mq11_424_retrieval_darkcore_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n039_bishop_ignasio-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n039_bishop_ignasio-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-accepting-acceptted" then
				api_quest_AddQuest(userObjID,424, 2);
				api_quest_SetJournalStep(userObjID,424, 1);
				api_quest_SetQuestStep(userObjID,424, 1);
				npc_talk_index = "n039_bishop_ignasio-1";

	end
	if npc_talk_index == "n039_bishop_ignasio-1-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-g" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-h" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300026, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1028, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201028, 30000);
	end
	if npc_talk_index == "n039_bishop_ignasio-3-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4241, true);
				 api_quest_RewardQuestUser(userObjID, 4241, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4242, true);
				 api_quest_RewardQuestUser(userObjID, 4242, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4243, true);
				 api_quest_RewardQuestUser(userObjID, 4243, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4244, true);
				 api_quest_RewardQuestUser(userObjID, 4244, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4244, true);
				 api_quest_RewardQuestUser(userObjID, 4244, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4246, true);
				 api_quest_RewardQuestUser(userObjID, 4246, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4241, true);
				 api_quest_RewardQuestUser(userObjID, 4241, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4241, true);
				 api_quest_RewardQuestUser(userObjID, 4241, questID, 1);
			 end 
	end
	if npc_talk_index == "n039_bishop_ignasio-3-d" then 

				if api_quest_HasQuestItem(userObjID, 300026, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300026, api_quest_HasQuestItem(userObjID, 300026, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 425, 2);
					api_quest_SetQuestStep(userObjID, 425, 1);
					api_quest_SetJournalStep(userObjID, 425, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n039_bishop_ignasio-1", "mq11_425_missing_spirits.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_424_retrieval_darkcore_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 424);
	if qstep == 2 and CountIndex == 300026 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 1028 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300026, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300026, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201028 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300026, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300026, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq11_424_retrieval_darkcore_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 424);
	if qstep == 2 and CountIndex == 300026 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1028 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201028 and Count >= TargetCount  then

	end
end

function mq11_424_retrieval_darkcore_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 424);
	local questID=424;
end

function mq11_424_retrieval_darkcore_OnRemoteStart( userObjID, questID )
end

function mq11_424_retrieval_darkcore_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq11_424_retrieval_darkcore_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,424, 2);
				api_quest_SetJournalStep(userObjID,424, 1);
				api_quest_SetQuestStep(userObjID,424, 1);
				npc_talk_index = "n039_bishop_ignasio-1";
end

</VillageServer>

<GameServer>
function mq11_424_retrieval_darkcore_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 39 then
		mq11_424_retrieval_darkcore_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n039_bishop_ignasio--------------------------------------------------------------------------------
function mq11_424_retrieval_darkcore_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n039_bishop_ignasio-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n039_bishop_ignasio-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,424, 2);
				api_quest_SetJournalStep( pRoom, userObjID,424, 1);
				api_quest_SetQuestStep( pRoom, userObjID,424, 1);
				npc_talk_index = "n039_bishop_ignasio-1";

	end
	if npc_talk_index == "n039_bishop_ignasio-1-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-g" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-h" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300026, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1028, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201028, 30000);
	end
	if npc_talk_index == "n039_bishop_ignasio-3-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4241, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4241, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4242, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4242, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4243, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4243, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4244, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4244, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4244, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4244, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4246, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4246, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4241, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4241, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4241, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4241, questID, 1);
			 end 
	end
	if npc_talk_index == "n039_bishop_ignasio-3-d" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300026, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300026, api_quest_HasQuestItem( pRoom, userObjID, 300026, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 425, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 425, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 425, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n039_bishop_ignasio-1", "mq11_425_missing_spirits.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_424_retrieval_darkcore_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 424);
	if qstep == 2 and CountIndex == 300026 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 1028 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300026, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300026, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201028 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300026, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300026, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq11_424_retrieval_darkcore_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 424);
	if qstep == 2 and CountIndex == 300026 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1028 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201028 and Count >= TargetCount  then

	end
end

function mq11_424_retrieval_darkcore_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 424);
	local questID=424;
end

function mq11_424_retrieval_darkcore_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq11_424_retrieval_darkcore_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq11_424_retrieval_darkcore_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,424, 2);
				api_quest_SetJournalStep( pRoom, userObjID,424, 1);
				api_quest_SetQuestStep( pRoom, userObjID,424, 1);
				npc_talk_index = "n039_bishop_ignasio-1";
end

</GameServer>