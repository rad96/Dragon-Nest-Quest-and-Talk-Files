<VillageServer>

function mqc06_9516_true_strength_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1691 then
		mqc06_9516_true_strength_OnTalk_n1691_eltia_lotus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 495 then
		mqc06_9516_true_strength_OnTalk_n495_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mqc06_9516_true_strength_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mqc06_9516_true_strength_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1691_eltia_lotus--------------------------------------------------------------------------------
function mqc06_9516_true_strength_OnTalk_n1691_eltia_lotus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1691_eltia_lotus-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1691_eltia_lotus-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1691_eltia_lotus-4-b" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-c" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-d" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-e" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-f" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-g" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-h" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-i" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-j" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-k" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-l" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-m" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95160, true);
				 api_quest_RewardQuestUser(userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95160, true);
				 api_quest_RewardQuestUser(userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95160, true);
				 api_quest_RewardQuestUser(userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95160, true);
				 api_quest_RewardQuestUser(userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95160, true);
				 api_quest_RewardQuestUser(userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95160, true);
				 api_quest_RewardQuestUser(userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95160, true);
				 api_quest_RewardQuestUser(userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95160, true);
				 api_quest_RewardQuestUser(userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95160, true);
				 api_quest_RewardQuestUser(userObjID, 95160, questID, 1);
			 end 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-n" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9517, 2);
					api_quest_SetQuestStep(userObjID, 9517, 1);
					api_quest_SetJournalStep(userObjID, 9517, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1691_eltia_lotus-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1691_eltia_lotus-1", "mqc07_9517_karakule_remedy.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n495_sidel--------------------------------------------------------------------------------
function mqc06_9516_true_strength_OnTalk_n495_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n495_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n495_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n495_sidel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n495_sidel-3-b" then 
	end
	if npc_talk_index == "n495_sidel-3-c" then 
	end
	if npc_talk_index == "n495_sidel-3-d" then 
	end
	if npc_talk_index == "n495_sidel-3-e" then 
	end
	if npc_talk_index == "n495_sidel-3-f" then 
	end
	if npc_talk_index == "n495_sidel-3-g" then 
	end
	if npc_talk_index == "n495_sidel-3-h" then 
	end
	if npc_talk_index == "n495_sidel-3-i" then 
	end
	if npc_talk_index == "n495_sidel-3-j" then 
	end
	if npc_talk_index == "n495_sidel-3-k" then 
	end
	if npc_talk_index == "n495_sidel-3-l" then 
	end
	if npc_talk_index == "n495_sidel-3-m" then 
	end
	if npc_talk_index == "n495_sidel-3-n" then 
	end
	if npc_talk_index == "n495_sidel-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mqc06_9516_true_strength_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				if api_quest_IsMarkingCompleteQuest(userObjID, 4607) == 1 then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									npc_talk_index = "n701_oldkarakule-1";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9516, 2);
				api_quest_SetJournalStep(userObjID,9516, 1);
				api_quest_SetQuestStep(userObjID,9516, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mqc06_9516_true_strength_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
	end
	if npc_talk_index == "n707_sidel-2-d" then 
	end
	if npc_talk_index == "n707_sidel-2-e" then 
	end
	if npc_talk_index == "n707_sidel-2-f" then 
	end
	if npc_talk_index == "n707_sidel-2-g" then 
	end
	if npc_talk_index == "n707_sidel-2-h" then 
	end
	if npc_talk_index == "n707_sidel-2-i" then 
	end
	if npc_talk_index == "n707_sidel-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc06_9516_true_strength_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9516);
end

function mqc06_9516_true_strength_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9516);
end

function mqc06_9516_true_strength_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9516);
	local questID=9516;
end

function mqc06_9516_true_strength_OnRemoteStart( userObjID, questID )
end

function mqc06_9516_true_strength_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc06_9516_true_strength_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9516, 2);
				api_quest_SetJournalStep(userObjID,9516, 1);
				api_quest_SetQuestStep(userObjID,9516, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</VillageServer>

<GameServer>
function mqc06_9516_true_strength_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1691 then
		mqc06_9516_true_strength_OnTalk_n1691_eltia_lotus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 495 then
		mqc06_9516_true_strength_OnTalk_n495_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mqc06_9516_true_strength_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mqc06_9516_true_strength_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1691_eltia_lotus--------------------------------------------------------------------------------
function mqc06_9516_true_strength_OnTalk_n1691_eltia_lotus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1691_eltia_lotus-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1691_eltia_lotus-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1691_eltia_lotus-4-b" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-c" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-d" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-e" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-f" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-g" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-h" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-i" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-j" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-k" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-l" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-m" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95160, questID, 1);
			 end 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-n" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9517, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9517, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9517, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1691_eltia_lotus-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1691_eltia_lotus-1", "mqc07_9517_karakule_remedy.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n495_sidel--------------------------------------------------------------------------------
function mqc06_9516_true_strength_OnTalk_n495_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n495_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n495_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n495_sidel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n495_sidel-3-b" then 
	end
	if npc_talk_index == "n495_sidel-3-c" then 
	end
	if npc_talk_index == "n495_sidel-3-d" then 
	end
	if npc_talk_index == "n495_sidel-3-e" then 
	end
	if npc_talk_index == "n495_sidel-3-f" then 
	end
	if npc_talk_index == "n495_sidel-3-g" then 
	end
	if npc_talk_index == "n495_sidel-3-h" then 
	end
	if npc_talk_index == "n495_sidel-3-i" then 
	end
	if npc_talk_index == "n495_sidel-3-j" then 
	end
	if npc_talk_index == "n495_sidel-3-k" then 
	end
	if npc_talk_index == "n495_sidel-3-l" then 
	end
	if npc_talk_index == "n495_sidel-3-m" then 
	end
	if npc_talk_index == "n495_sidel-3-n" then 
	end
	if npc_talk_index == "n495_sidel-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mqc06_9516_true_strength_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 4607) == 1 then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									npc_talk_index = "n701_oldkarakule-1";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9516, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9516, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9516, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mqc06_9516_true_strength_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
	end
	if npc_talk_index == "n707_sidel-2-d" then 
	end
	if npc_talk_index == "n707_sidel-2-e" then 
	end
	if npc_talk_index == "n707_sidel-2-f" then 
	end
	if npc_talk_index == "n707_sidel-2-g" then 
	end
	if npc_talk_index == "n707_sidel-2-h" then 
	end
	if npc_talk_index == "n707_sidel-2-i" then 
	end
	if npc_talk_index == "n707_sidel-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc06_9516_true_strength_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9516);
end

function mqc06_9516_true_strength_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9516);
end

function mqc06_9516_true_strength_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9516);
	local questID=9516;
end

function mqc06_9516_true_strength_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc06_9516_true_strength_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc06_9516_true_strength_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9516, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9516, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9516, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</GameServer>