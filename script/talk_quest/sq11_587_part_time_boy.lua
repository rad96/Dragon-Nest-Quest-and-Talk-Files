<VillageServer>

function sq11_587_part_time_boy_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 49 then
		sq11_587_part_time_boy_OnTalk_n049_sainthaven_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n049_sainthaven_kid--------------------------------------------------------------------------------
function sq11_587_part_time_boy_OnTalk_n049_sainthaven_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n049_sainthaven_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n049_sainthaven_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n049_sainthaven_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n049_sainthaven_kid-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5871, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5872, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5873, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5874, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5875, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5876, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5877, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5878, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5879, false);
			 end 

	end
	if npc_talk_index == "n049_sainthaven_kid-accepting-acceppted" then
				api_quest_AddQuest(userObjID,587, 1);
				api_quest_SetJournalStep(userObjID,587, 1);
				api_quest_SetQuestStep(userObjID,587, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1067, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201067, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300276, 2);
				npc_talk_index = "n049_sainthaven_kid-1";

	end
	if npc_talk_index == "n049_sainthaven_kid-2-b" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-c" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5871, true);
				 api_quest_RewardQuestUser(userObjID, 5871, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5872, true);
				 api_quest_RewardQuestUser(userObjID, 5872, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5873, true);
				 api_quest_RewardQuestUser(userObjID, 5873, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5874, true);
				 api_quest_RewardQuestUser(userObjID, 5874, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5875, true);
				 api_quest_RewardQuestUser(userObjID, 5875, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5876, true);
				 api_quest_RewardQuestUser(userObjID, 5876, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5877, true);
				 api_quest_RewardQuestUser(userObjID, 5877, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5878, true);
				 api_quest_RewardQuestUser(userObjID, 5878, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5879, true);
				 api_quest_RewardQuestUser(userObjID, 5879, questID, 1);
			 end 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-e" then 

				if api_quest_HasQuestItem(userObjID, 300276, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300276, api_quest_HasQuestItem(userObjID, 300276, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_587_part_time_boy_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 587);
	if qstep == 1 and CountIndex == 1067 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300276, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300276, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300276 then

	end
	if qstep == 1 and CountIndex == 201067 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300276, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300276, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_587_part_time_boy_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 587);
	if qstep == 1 and CountIndex == 1067 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300276 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201067 and Count >= TargetCount  then

	end
end

function sq11_587_part_time_boy_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 587);
	local questID=587;
end

function sq11_587_part_time_boy_OnRemoteStart( userObjID, questID )
end

function sq11_587_part_time_boy_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_587_part_time_boy_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_587_part_time_boy_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 49 then
		sq11_587_part_time_boy_OnTalk_n049_sainthaven_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n049_sainthaven_kid--------------------------------------------------------------------------------
function sq11_587_part_time_boy_OnTalk_n049_sainthaven_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n049_sainthaven_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n049_sainthaven_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n049_sainthaven_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n049_sainthaven_kid-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5871, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5872, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5873, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5874, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5875, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5876, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5877, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5878, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5879, false);
			 end 

	end
	if npc_talk_index == "n049_sainthaven_kid-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,587, 1);
				api_quest_SetJournalStep( pRoom, userObjID,587, 1);
				api_quest_SetQuestStep( pRoom, userObjID,587, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1067, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201067, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300276, 2);
				npc_talk_index = "n049_sainthaven_kid-1";

	end
	if npc_talk_index == "n049_sainthaven_kid-2-b" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-c" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5871, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5871, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5872, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5872, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5873, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5873, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5874, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5874, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5875, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5875, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5876, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5876, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5877, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5877, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5878, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5878, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5879, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5879, questID, 1);
			 end 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-e" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300276, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300276, api_quest_HasQuestItem( pRoom, userObjID, 300276, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_587_part_time_boy_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 587);
	if qstep == 1 and CountIndex == 1067 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300276, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300276, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300276 then

	end
	if qstep == 1 and CountIndex == 201067 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300276, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300276, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_587_part_time_boy_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 587);
	if qstep == 1 and CountIndex == 1067 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300276 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201067 and Count >= TargetCount  then

	end
end

function sq11_587_part_time_boy_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 587);
	local questID=587;
end

function sq11_587_part_time_boy_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_587_part_time_boy_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_587_part_time_boy_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>