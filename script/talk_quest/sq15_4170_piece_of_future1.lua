<VillageServer>

function sq15_4170_piece_of_future1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 45 then
		sq15_4170_piece_of_future1_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 880 then
		sq15_4170_piece_of_future1_OnTalk_n880_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 888 then
		sq15_4170_piece_of_future1_OnTalk_n888_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq15_4170_piece_of_future1_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n045_soceress_master_stella-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n045_soceress_master_stella-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41700, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41700, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41700, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41700, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41700, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41700, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41700, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41700, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41700, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4170, 1);
				api_quest_SetJournalStep(userObjID,4170, 1);
				api_quest_SetQuestStep(userObjID,4170, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-4-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n045_soceress_master_stella-5-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41700, true);
				 api_quest_RewardQuestUser(userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41700, true);
				 api_quest_RewardQuestUser(userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41700, true);
				 api_quest_RewardQuestUser(userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41700, true);
				 api_quest_RewardQuestUser(userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41700, true);
				 api_quest_RewardQuestUser(userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41700, true);
				 api_quest_RewardQuestUser(userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41700, true);
				 api_quest_RewardQuestUser(userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41700, true);
				 api_quest_RewardQuestUser(userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41700, true);
				 api_quest_RewardQuestUser(userObjID, 41700, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n880_academic--------------------------------------------------------------------------------
function sq15_4170_piece_of_future1_OnTalk_n880_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n880_academic-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n880_academic-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n880_academic-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n880_academic-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n880_academic-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n880_academic-2-b" then 
	end
	if npc_talk_index == "n880_academic-2-c" then 
	end
	if npc_talk_index == "n880_academic-2-d" then 
	end
	if npc_talk_index == "n880_academic-2-e" then 
	end
	if npc_talk_index == "n880_academic-2-e" then 
	end
	if npc_talk_index == "n880_academic-2-f" then 
	end
	if npc_talk_index == "n880_academic-2-g" then 
	end
	if npc_talk_index == "n880_academic-2-h" then 
	end
	if npc_talk_index == "n880_academic-2-i" then 
	end
	if npc_talk_index == "n880_academic-2-j" then 
	end
	if npc_talk_index == "n880_academic-2-k" then 
	end
	if npc_talk_index == "n880_academic-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n880_academic-3-b" then 
	end
	if npc_talk_index == "n880_academic-3-c" then 
	end
	if npc_talk_index == "n880_academic-3-d" then 
	end
	if npc_talk_index == "n880_academic-3-e" then 
	end
	if npc_talk_index == "n880_academic-3-e" then 
	end
	if npc_talk_index == "n880_academic-3-f" then 
	end
	if npc_talk_index == "n880_academic-3-g" then 
	end
	if npc_talk_index == "n880_academic-3-g" then 
	end
	if npc_talk_index == "n880_academic-3-g" then 
	end
	if npc_talk_index == "n880_academic-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n888_academic--------------------------------------------------------------------------------
function sq15_4170_piece_of_future1_OnTalk_n888_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n888_academic-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n888_academic-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n888_academic-1-b" then 
	end
	if npc_talk_index == "n888_academic-1-c" then 
	end
	if npc_talk_index == "n888_academic-1-d" then 
	end
	if npc_talk_index == "n888_academic-1-e" then 
	end
	if npc_talk_index == "n888_academic-1-f" then 
	end
	if npc_talk_index == "n888_academic-1-g" then 
	end
	if npc_talk_index == "n888_academic-1-h" then 
	end
	if npc_talk_index == "n888_academic-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4170_piece_of_future1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4170);
end

function sq15_4170_piece_of_future1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4170);
end

function sq15_4170_piece_of_future1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4170);
	local questID=4170;
end

function sq15_4170_piece_of_future1_OnRemoteStart( userObjID, questID )
end

function sq15_4170_piece_of_future1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4170_piece_of_future1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4170, 1);
				api_quest_SetJournalStep(userObjID,4170, 1);
				api_quest_SetQuestStep(userObjID,4170, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
end

</VillageServer>

<GameServer>
function sq15_4170_piece_of_future1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 45 then
		sq15_4170_piece_of_future1_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 880 then
		sq15_4170_piece_of_future1_OnTalk_n880_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 888 then
		sq15_4170_piece_of_future1_OnTalk_n888_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq15_4170_piece_of_future1_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n045_soceress_master_stella-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n045_soceress_master_stella-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4170, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4170, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4170, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-4-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n045_soceress_master_stella-5-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41700, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n880_academic--------------------------------------------------------------------------------
function sq15_4170_piece_of_future1_OnTalk_n880_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n880_academic-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n880_academic-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n880_academic-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n880_academic-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n880_academic-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n880_academic-2-b" then 
	end
	if npc_talk_index == "n880_academic-2-c" then 
	end
	if npc_talk_index == "n880_academic-2-d" then 
	end
	if npc_talk_index == "n880_academic-2-e" then 
	end
	if npc_talk_index == "n880_academic-2-e" then 
	end
	if npc_talk_index == "n880_academic-2-f" then 
	end
	if npc_talk_index == "n880_academic-2-g" then 
	end
	if npc_talk_index == "n880_academic-2-h" then 
	end
	if npc_talk_index == "n880_academic-2-i" then 
	end
	if npc_talk_index == "n880_academic-2-j" then 
	end
	if npc_talk_index == "n880_academic-2-k" then 
	end
	if npc_talk_index == "n880_academic-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n880_academic-3-b" then 
	end
	if npc_talk_index == "n880_academic-3-c" then 
	end
	if npc_talk_index == "n880_academic-3-d" then 
	end
	if npc_talk_index == "n880_academic-3-e" then 
	end
	if npc_talk_index == "n880_academic-3-e" then 
	end
	if npc_talk_index == "n880_academic-3-f" then 
	end
	if npc_talk_index == "n880_academic-3-g" then 
	end
	if npc_talk_index == "n880_academic-3-g" then 
	end
	if npc_talk_index == "n880_academic-3-g" then 
	end
	if npc_talk_index == "n880_academic-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n888_academic--------------------------------------------------------------------------------
function sq15_4170_piece_of_future1_OnTalk_n888_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n888_academic-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n888_academic-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n888_academic-1-b" then 
	end
	if npc_talk_index == "n888_academic-1-c" then 
	end
	if npc_talk_index == "n888_academic-1-d" then 
	end
	if npc_talk_index == "n888_academic-1-e" then 
	end
	if npc_talk_index == "n888_academic-1-f" then 
	end
	if npc_talk_index == "n888_academic-1-g" then 
	end
	if npc_talk_index == "n888_academic-1-h" then 
	end
	if npc_talk_index == "n888_academic-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4170_piece_of_future1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4170);
end

function sq15_4170_piece_of_future1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4170);
end

function sq15_4170_piece_of_future1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4170);
	local questID=4170;
end

function sq15_4170_piece_of_future1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4170_piece_of_future1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4170_piece_of_future1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4170, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4170, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4170, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
end

</GameServer>