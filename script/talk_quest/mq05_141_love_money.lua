<VillageServer>

function mq05_141_love_money_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 12 then
		mq05_141_love_money_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 13 then
		mq05_141_love_money_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 14 then
		mq05_141_love_money_OnTalk_n014_cleric_cedric(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 15 then
		mq05_141_love_money_OnTalk_n015_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 314 then
		mq05_141_love_money_OnTalk_n314_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_141_love_money_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n012_sorceress_master_cynthia-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-4-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-g" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1410, true);
				 api_quest_RewardQuestUser(userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1410, true);
				 api_quest_RewardQuestUser(userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1410, true);
				 api_quest_RewardQuestUser(userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1410, true);
				 api_quest_RewardQuestUser(userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1410, true);
				 api_quest_RewardQuestUser(userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1410, true);
				 api_quest_RewardQuestUser(userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1410, true);
				 api_quest_RewardQuestUser(userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1410, true);
				 api_quest_RewardQuestUser(userObjID, 1410, questID, 1);
			 end 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-i" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 142, 2);
					api_quest_SetQuestStep(userObjID, 142, 1);
					api_quest_SetJournalStep(userObjID, 142, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n012_sorceress_master_cynthia-1", "mq05_142_snatch_orb.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n013_cleric_tomas--------------------------------------------------------------------------------
function mq05_141_love_money_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n013_cleric_tomas-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n013_cleric_tomas-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-1-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-d" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-e" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-f" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n014_cleric_cedric--------------------------------------------------------------------------------
function mq05_141_love_money_OnTalk_n014_cleric_cedric(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n014_cleric_cedric-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n014_cleric_cedric-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n014_cleric_cedric-1-b" then 
	end
	if npc_talk_index == "n014_cleric_cedric-1-c" then 
	end
	if npc_talk_index == "n014_cleric_cedric-1-d" then 
	end
	if npc_talk_index == "n014_cleric_cedric-1-e" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n015_sorceress_tara--------------------------------------------------------------------------------
function mq05_141_love_money_OnTalk_n015_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n015_sorceress_tara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n015_sorceress_tara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n015_sorceress_tara-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n015_sorceress_tara-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n015_sorceress_tara-accepting-acceptted" then
				api_quest_AddQuest(userObjID,141, 2);
				api_quest_SetJournalStep(userObjID,141, 1);
				api_quest_SetQuestStep(userObjID,141, 1);
				npc_talk_index = "n015_sorceress_tara-1";

	end
	if npc_talk_index == "n015_sorceress_tara-3-b" then 
	end
	if npc_talk_index == "n015_sorceress_tara-3-c" then 
	end
	if npc_talk_index == "n015_sorceress_tara-3-d" then 
	end
	if npc_talk_index == "n015_sorceress_tara-3-d" then 
	end
	if npc_talk_index == "n015_sorceress_tara-3-e" then 
	end
	if npc_talk_index == "n015_sorceress_tara-3-f" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n314_cleric_jake--------------------------------------------------------------------------------
function mq05_141_love_money_OnTalk_n314_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n314_cleric_jake-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n314_cleric_jake-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n314_cleric_jake-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n314_cleric_jake-2-b" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_141_love_money_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 141);
end

function mq05_141_love_money_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 141);
end

function mq05_141_love_money_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 141);
	local questID=141;
end

function mq05_141_love_money_OnRemoteStart( userObjID, questID )
end

function mq05_141_love_money_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq05_141_love_money_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,141, 2);
				api_quest_SetJournalStep(userObjID,141, 1);
				api_quest_SetQuestStep(userObjID,141, 1);
				npc_talk_index = "n015_sorceress_tara-1";
end

</VillageServer>

<GameServer>
function mq05_141_love_money_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 12 then
		mq05_141_love_money_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 13 then
		mq05_141_love_money_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 14 then
		mq05_141_love_money_OnTalk_n014_cleric_cedric( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 15 then
		mq05_141_love_money_OnTalk_n015_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 314 then
		mq05_141_love_money_OnTalk_n314_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_141_love_money_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n012_sorceress_master_cynthia-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-4-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-g" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1410, questID, 1);
			 end 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-i" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 142, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 142, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 142, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n012_sorceress_master_cynthia-1", "mq05_142_snatch_orb.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n013_cleric_tomas--------------------------------------------------------------------------------
function mq05_141_love_money_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n013_cleric_tomas-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n013_cleric_tomas-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-1-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-d" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-e" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-f" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n014_cleric_cedric--------------------------------------------------------------------------------
function mq05_141_love_money_OnTalk_n014_cleric_cedric( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n014_cleric_cedric-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n014_cleric_cedric-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n014_cleric_cedric-1-b" then 
	end
	if npc_talk_index == "n014_cleric_cedric-1-c" then 
	end
	if npc_talk_index == "n014_cleric_cedric-1-d" then 
	end
	if npc_talk_index == "n014_cleric_cedric-1-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n015_sorceress_tara--------------------------------------------------------------------------------
function mq05_141_love_money_OnTalk_n015_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n015_sorceress_tara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n015_sorceress_tara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n015_sorceress_tara-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n015_sorceress_tara-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n015_sorceress_tara-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,141, 2);
				api_quest_SetJournalStep( pRoom, userObjID,141, 1);
				api_quest_SetQuestStep( pRoom, userObjID,141, 1);
				npc_talk_index = "n015_sorceress_tara-1";

	end
	if npc_talk_index == "n015_sorceress_tara-3-b" then 
	end
	if npc_talk_index == "n015_sorceress_tara-3-c" then 
	end
	if npc_talk_index == "n015_sorceress_tara-3-d" then 
	end
	if npc_talk_index == "n015_sorceress_tara-3-d" then 
	end
	if npc_talk_index == "n015_sorceress_tara-3-e" then 
	end
	if npc_talk_index == "n015_sorceress_tara-3-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n314_cleric_jake--------------------------------------------------------------------------------
function mq05_141_love_money_OnTalk_n314_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n314_cleric_jake-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n314_cleric_jake-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n314_cleric_jake-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n314_cleric_jake-2-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_141_love_money_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 141);
end

function mq05_141_love_money_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 141);
end

function mq05_141_love_money_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 141);
	local questID=141;
end

function mq05_141_love_money_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq05_141_love_money_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq05_141_love_money_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,141, 2);
				api_quest_SetJournalStep( pRoom, userObjID,141, 1);
				api_quest_SetQuestStep( pRoom, userObjID,141, 1);
				npc_talk_index = "n015_sorceress_tara-1";
end

</GameServer>