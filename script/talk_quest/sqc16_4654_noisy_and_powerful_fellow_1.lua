<VillageServer>

function sqc16_4654_noisy_and_powerful_fellow_1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1138 then
		sqc16_4654_noisy_and_powerful_fellow_1_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1800 then
		sqc16_4654_noisy_and_powerful_fellow_1_OnTalk_n1800_book(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function sqc16_4654_noisy_and_powerful_fellow_1_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1138_elf_guard_sitredel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1138_elf_guard_sitredel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46540, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46540, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46540, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46540, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46540, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46540, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46540, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46540, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46540, false);
			 end 

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4654, 1);
				api_quest_SetJournalStep(userObjID,4654, 1);
				api_quest_SetQuestStep(userObjID,4654, 1);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400476, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400476, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n1138_elf_guard_sitredel-1";

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46540, true);
				 api_quest_RewardQuestUser(userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46540, true);
				 api_quest_RewardQuestUser(userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46540, true);
				 api_quest_RewardQuestUser(userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46540, true);
				 api_quest_RewardQuestUser(userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46540, true);
				 api_quest_RewardQuestUser(userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46540, true);
				 api_quest_RewardQuestUser(userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46540, true);
				 api_quest_RewardQuestUser(userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46540, true);
				 api_quest_RewardQuestUser(userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46540, true);
				 api_quest_RewardQuestUser(userObjID, 46540, questID, 1);
			 end 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4656, 1);
					api_quest_SetQuestStep(userObjID, 4656, 1);
					api_quest_SetJournalStep(userObjID, 4656, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400476, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400476, api_quest_HasQuestItem(userObjID, 400476, 1));
				end
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1138_elf_guard_sitredel-1", "sqc16_4656_noisy_and_powerful_fellow_2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1800_book--------------------------------------------------------------------------------
function sqc16_4654_noisy_and_powerful_fellow_1_OnTalk_n1800_book(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1800_book-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1800_book-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1800_book-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1800_book-1-b" then 
	end
	if npc_talk_index == "n1800_book-1-c" then 
	end
	if npc_talk_index == "n1800_book-1-d" then 
	end
	if npc_talk_index == "n1800_book-1-e" then 
	end
	if npc_talk_index == "n1800_book-1-f" then 
	end
	if npc_talk_index == "n1800_book-1-g" then 
	end
	if npc_talk_index == "n1800_book-1-h" then 
	end
	if npc_talk_index == "n1800_book-1-i" then 
	end
	if npc_talk_index == "n1800_book-1-j" then 
	end
	if npc_talk_index == "n1800_book-1-k" then 
	end
	if npc_talk_index == "n1800_book-1-l" then 
	end
	if npc_talk_index == "n1800_book-1-m" then 
	end
	if npc_talk_index == "n1800_book-1-n" then 
	end
	if npc_talk_index == "n1800_book-1-o" then 
	end
	if npc_talk_index == "n1800_book-1-p" then 
	end
	if npc_talk_index == "n1800_book-1-q" then 
	end
	if npc_talk_index == "n1800_book-1-r" then 
	end
	if npc_talk_index == "n1800_book-1-s" then 
	end
	if npc_talk_index == "n1800_book-1-t" then 
	end
	if npc_talk_index == "n1800_book-1-u" then 
	end
	if npc_talk_index == "n1800_book-1-v" then 
	end
	if npc_talk_index == "n1800_book-1-w" then 
	end
	if npc_talk_index == "n1800_book-1-x" then 
	end
	if npc_talk_index == "n1800_book-1-y" then 
	end
	if npc_talk_index == "n1800_book-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n1800_book-2-b" then 
	end
	if npc_talk_index == "n1800_book-2-c" then 
	end
	if npc_talk_index == "n1800_book-2-d" then 
	end
	if npc_talk_index == "n1800_book-2-e" then 
	end
	if npc_talk_index == "n1800_book-2-f" then 
	end
	if npc_talk_index == "n1800_book-2-g" then 
	end
	if npc_talk_index == "n1800_book-2-h" then 
	end
	if npc_talk_index == "n1800_book-2-i" then 
	end
	if npc_talk_index == "n1800_book-2-j" then 
	end
	if npc_talk_index == "n1800_book-2-k" then 
	end
	if npc_talk_index == "n1800_book-2-l" then 
	end
	if npc_talk_index == "n1800_book-2-m" then 
	end
	if npc_talk_index == "n1800_book-2-n" then 
	end
	if npc_talk_index == "n1800_book-2-o" then 
	end
	if npc_talk_index == "n1800_book-2-p" then 
	end
	if npc_talk_index == "n1800_book-2-q" then 
	end
	if npc_talk_index == "n1800_book-2-r" then 
	end
	if npc_talk_index == "n1800_book-2-s" then 
	end
	if npc_talk_index == "n1800_book-2-t" then 
	end
	if npc_talk_index == "n1800_book-2-u" then 
	end
	if npc_talk_index == "n1800_book-2-v" then 
	end
	if npc_talk_index == "n1800_book-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4654_noisy_and_powerful_fellow_1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4654);
end

function sqc16_4654_noisy_and_powerful_fellow_1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4654);
end

function sqc16_4654_noisy_and_powerful_fellow_1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4654);
	local questID=4654;
end

function sqc16_4654_noisy_and_powerful_fellow_1_OnRemoteStart( userObjID, questID )
end

function sqc16_4654_noisy_and_powerful_fellow_1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc16_4654_noisy_and_powerful_fellow_1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4654, 1);
				api_quest_SetJournalStep(userObjID,4654, 1);
				api_quest_SetQuestStep(userObjID,4654, 1);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400476, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400476, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n1138_elf_guard_sitredel-1";
end

</VillageServer>

<GameServer>
function sqc16_4654_noisy_and_powerful_fellow_1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1138 then
		sqc16_4654_noisy_and_powerful_fellow_1_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1800 then
		sqc16_4654_noisy_and_powerful_fellow_1_OnTalk_n1800_book( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function sqc16_4654_noisy_and_powerful_fellow_1_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1138_elf_guard_sitredel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1138_elf_guard_sitredel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, false);
			 end 

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4654, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4654, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4654, 1);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400476, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400476, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n1138_elf_guard_sitredel-1";

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46540, questID, 1);
			 end 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4656, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4656, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4656, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400476, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400476, api_quest_HasQuestItem( pRoom, userObjID, 400476, 1));
				end
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1138_elf_guard_sitredel-1", "sqc16_4656_noisy_and_powerful_fellow_2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1800_book--------------------------------------------------------------------------------
function sqc16_4654_noisy_and_powerful_fellow_1_OnTalk_n1800_book( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1800_book-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1800_book-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1800_book-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1800_book-1-b" then 
	end
	if npc_talk_index == "n1800_book-1-c" then 
	end
	if npc_talk_index == "n1800_book-1-d" then 
	end
	if npc_talk_index == "n1800_book-1-e" then 
	end
	if npc_talk_index == "n1800_book-1-f" then 
	end
	if npc_talk_index == "n1800_book-1-g" then 
	end
	if npc_talk_index == "n1800_book-1-h" then 
	end
	if npc_talk_index == "n1800_book-1-i" then 
	end
	if npc_talk_index == "n1800_book-1-j" then 
	end
	if npc_talk_index == "n1800_book-1-k" then 
	end
	if npc_talk_index == "n1800_book-1-l" then 
	end
	if npc_talk_index == "n1800_book-1-m" then 
	end
	if npc_talk_index == "n1800_book-1-n" then 
	end
	if npc_talk_index == "n1800_book-1-o" then 
	end
	if npc_talk_index == "n1800_book-1-p" then 
	end
	if npc_talk_index == "n1800_book-1-q" then 
	end
	if npc_talk_index == "n1800_book-1-r" then 
	end
	if npc_talk_index == "n1800_book-1-s" then 
	end
	if npc_talk_index == "n1800_book-1-t" then 
	end
	if npc_talk_index == "n1800_book-1-u" then 
	end
	if npc_talk_index == "n1800_book-1-v" then 
	end
	if npc_talk_index == "n1800_book-1-w" then 
	end
	if npc_talk_index == "n1800_book-1-x" then 
	end
	if npc_talk_index == "n1800_book-1-y" then 
	end
	if npc_talk_index == "n1800_book-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n1800_book-2-b" then 
	end
	if npc_talk_index == "n1800_book-2-c" then 
	end
	if npc_talk_index == "n1800_book-2-d" then 
	end
	if npc_talk_index == "n1800_book-2-e" then 
	end
	if npc_talk_index == "n1800_book-2-f" then 
	end
	if npc_talk_index == "n1800_book-2-g" then 
	end
	if npc_talk_index == "n1800_book-2-h" then 
	end
	if npc_talk_index == "n1800_book-2-i" then 
	end
	if npc_talk_index == "n1800_book-2-j" then 
	end
	if npc_talk_index == "n1800_book-2-k" then 
	end
	if npc_talk_index == "n1800_book-2-l" then 
	end
	if npc_talk_index == "n1800_book-2-m" then 
	end
	if npc_talk_index == "n1800_book-2-n" then 
	end
	if npc_talk_index == "n1800_book-2-o" then 
	end
	if npc_talk_index == "n1800_book-2-p" then 
	end
	if npc_talk_index == "n1800_book-2-q" then 
	end
	if npc_talk_index == "n1800_book-2-r" then 
	end
	if npc_talk_index == "n1800_book-2-s" then 
	end
	if npc_talk_index == "n1800_book-2-t" then 
	end
	if npc_talk_index == "n1800_book-2-u" then 
	end
	if npc_talk_index == "n1800_book-2-v" then 
	end
	if npc_talk_index == "n1800_book-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4654_noisy_and_powerful_fellow_1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4654);
end

function sqc16_4654_noisy_and_powerful_fellow_1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4654);
end

function sqc16_4654_noisy_and_powerful_fellow_1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4654);
	local questID=4654;
end

function sqc16_4654_noisy_and_powerful_fellow_1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc16_4654_noisy_and_powerful_fellow_1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc16_4654_noisy_and_powerful_fellow_1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4654, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4654, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4654, 1);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400476, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400476, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n1138_elf_guard_sitredel-1";
end

</GameServer>