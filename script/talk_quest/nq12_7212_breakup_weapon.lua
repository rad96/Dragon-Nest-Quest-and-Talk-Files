<VillageServer>

function nq12_7212_breakup_weapon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 91 then
		nq12_7212_breakup_weapon_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n091_trader_bellin--------------------------------------------------------------------------------
function nq12_7212_breakup_weapon_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n091_trader_bellin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n091_trader_bellin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-h" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 72120, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 72120, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 72120, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 72120, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 72120, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 72120, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 72120, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 72120, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 72120, false);
			 end 

	end
	if npc_talk_index == "n091_trader_bellin-accepting-acceppted" then
				api_quest_AddQuest(userObjID,7212, 1);
				api_quest_SetJournalStep(userObjID,7212, 1);
				api_quest_SetQuestStep(userObjID,7212, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 827, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200827, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300325, 1);
				npc_talk_index = "n091_trader_bellin-1";

	end
	if npc_talk_index == "n091_trader_bellin-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);

				if api_quest_HasQuestItem(userObjID, 300325, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300325, api_quest_HasQuestItem(userObjID, 300325, 1));
				end
	end
	if npc_talk_index == "n091_trader_bellin-3-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-c" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-d" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 72120, true);
				 api_quest_RewardQuestUser(userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 72120, true);
				 api_quest_RewardQuestUser(userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 72120, true);
				 api_quest_RewardQuestUser(userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 72120, true);
				 api_quest_RewardQuestUser(userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 72120, true);
				 api_quest_RewardQuestUser(userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 72120, true);
				 api_quest_RewardQuestUser(userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 72120, true);
				 api_quest_RewardQuestUser(userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 72120, true);
				 api_quest_RewardQuestUser(userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 72120, true);
				 api_quest_RewardQuestUser(userObjID, 72120, questID, 1);
			 end 
	end
	if npc_talk_index == "n091_trader_bellin-3-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 91, 600 );
	end
	if npc_talk_index == "n091_trader_bellin-3-g" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq12_7212_breakup_weapon_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7212);
	if qstep == 1 and CountIndex == 827 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300325, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300325, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200827 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300325, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300325, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300325 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function nq12_7212_breakup_weapon_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7212);
	if qstep == 1 and CountIndex == 827 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200827 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300325 and Count >= TargetCount  then

	end
end

function nq12_7212_breakup_weapon_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7212);
	local questID=7212;
end

function nq12_7212_breakup_weapon_OnRemoteStart( userObjID, questID )
end

function nq12_7212_breakup_weapon_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq12_7212_breakup_weapon_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq12_7212_breakup_weapon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 91 then
		nq12_7212_breakup_weapon_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n091_trader_bellin--------------------------------------------------------------------------------
function nq12_7212_breakup_weapon_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n091_trader_bellin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n091_trader_bellin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-h" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, false);
			 end 

	end
	if npc_talk_index == "n091_trader_bellin-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,7212, 1);
				api_quest_SetJournalStep( pRoom, userObjID,7212, 1);
				api_quest_SetQuestStep( pRoom, userObjID,7212, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 827, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200827, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300325, 1);
				npc_talk_index = "n091_trader_bellin-1";

	end
	if npc_talk_index == "n091_trader_bellin-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);

				if api_quest_HasQuestItem( pRoom, userObjID, 300325, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300325, api_quest_HasQuestItem( pRoom, userObjID, 300325, 1));
				end
	end
	if npc_talk_index == "n091_trader_bellin-3-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-c" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-d" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72120, questID, 1);
			 end 
	end
	if npc_talk_index == "n091_trader_bellin-3-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 91, 600 );
	end
	if npc_talk_index == "n091_trader_bellin-3-g" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq12_7212_breakup_weapon_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7212);
	if qstep == 1 and CountIndex == 827 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300325, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300325, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200827 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300325, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300325, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300325 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function nq12_7212_breakup_weapon_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7212);
	if qstep == 1 and CountIndex == 827 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200827 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300325 and Count >= TargetCount  then

	end
end

function nq12_7212_breakup_weapon_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7212);
	local questID=7212;
end

function nq12_7212_breakup_weapon_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq12_7212_breakup_weapon_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq12_7212_breakup_weapon_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>