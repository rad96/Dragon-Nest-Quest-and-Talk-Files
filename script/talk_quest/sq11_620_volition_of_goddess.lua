<VillageServer>

function sq11_620_volition_of_goddess_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 277 then
		sq11_620_volition_of_goddess_OnTalk_n277_bluff_dealers_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n277_bluff_dealers_pope--------------------------------------------------------------------------------
function sq11_620_volition_of_goddess_OnTalk_n277_bluff_dealers_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n277_bluff_dealers_pope-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n277_bluff_dealers_pope-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n277_bluff_dealers_pope-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n277_bluff_dealers_pope-accepting-h" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6200, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6200, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6200, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6200, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6200, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6200, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6200, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6200, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6200, false);
			 end 

	end
	if npc_talk_index == "n277_bluff_dealers_pope-accepting-acceptted" then
				api_quest_AddQuest(userObjID,620, 1);
				api_quest_SetJournalStep(userObjID,620, 1);
				api_quest_SetQuestStep(userObjID,620, 1);
				npc_talk_index = "n277_bluff_dealers_pope-1";

	end
	if npc_talk_index == "n277_bluff_dealers_pope-1-b" then 
	end
	if npc_talk_index == "n277_bluff_dealers_pope-1-c" then 
	end
	if npc_talk_index == "n277_bluff_dealers_pope-1-open_shop" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				if api_user_GetUserClassID(userObjID) == 1 then
									api_ui_OpenShop(userObjID,16001);

				else
									if api_user_GetUserClassID(userObjID) == 2 then
									api_ui_OpenShop(userObjID,16002);

				else
									if api_user_GetUserClassID(userObjID) == 3 then
									api_ui_OpenShop(userObjID,16003);

				else
									if api_user_GetUserClassID(userObjID) == 4 then
									api_ui_OpenShop(userObjID,16004);

				else
									api_ui_OpenShop(userObjID,16004);

				end

				end

				end

				end
	end
	if npc_talk_index == "n277_bluff_dealers_pope-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6200, true);
				 api_quest_RewardQuestUser(userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6200, true);
				 api_quest_RewardQuestUser(userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6200, true);
				 api_quest_RewardQuestUser(userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6200, true);
				 api_quest_RewardQuestUser(userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6200, true);
				 api_quest_RewardQuestUser(userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6200, true);
				 api_quest_RewardQuestUser(userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6200, true);
				 api_quest_RewardQuestUser(userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6200, true);
				 api_quest_RewardQuestUser(userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6200, true);
				 api_quest_RewardQuestUser(userObjID, 6200, questID, 1);
			 end 
	end
	if npc_talk_index == "n277_bluff_dealers_pope-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_620_volition_of_goddess_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 620);
end

function sq11_620_volition_of_goddess_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 620);
end

function sq11_620_volition_of_goddess_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 620);
	local questID=620;
end

function sq11_620_volition_of_goddess_OnRemoteStart( userObjID, questID )
end

function sq11_620_volition_of_goddess_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_620_volition_of_goddess_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,620, 1);
				api_quest_SetJournalStep(userObjID,620, 1);
				api_quest_SetQuestStep(userObjID,620, 1);
				npc_talk_index = "n277_bluff_dealers_pope-1";
end

</VillageServer>

<GameServer>
function sq11_620_volition_of_goddess_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 277 then
		sq11_620_volition_of_goddess_OnTalk_n277_bluff_dealers_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n277_bluff_dealers_pope--------------------------------------------------------------------------------
function sq11_620_volition_of_goddess_OnTalk_n277_bluff_dealers_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n277_bluff_dealers_pope-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n277_bluff_dealers_pope-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n277_bluff_dealers_pope-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n277_bluff_dealers_pope-accepting-h" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, false);
			 end 

	end
	if npc_talk_index == "n277_bluff_dealers_pope-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,620, 1);
				api_quest_SetJournalStep( pRoom, userObjID,620, 1);
				api_quest_SetQuestStep( pRoom, userObjID,620, 1);
				npc_talk_index = "n277_bluff_dealers_pope-1";

	end
	if npc_talk_index == "n277_bluff_dealers_pope-1-b" then 
	end
	if npc_talk_index == "n277_bluff_dealers_pope-1-c" then 
	end
	if npc_talk_index == "n277_bluff_dealers_pope-1-open_shop" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									api_ui_OpenShop( pRoom, userObjID,16001);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									api_ui_OpenShop( pRoom, userObjID,16002);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									api_ui_OpenShop( pRoom, userObjID,16003);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									api_ui_OpenShop( pRoom, userObjID,16004);

				else
									api_ui_OpenShop( pRoom, userObjID,16004);

				end

				end

				end

				end
	end
	if npc_talk_index == "n277_bluff_dealers_pope-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6200, questID, 1);
			 end 
	end
	if npc_talk_index == "n277_bluff_dealers_pope-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_620_volition_of_goddess_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 620);
end

function sq11_620_volition_of_goddess_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 620);
end

function sq11_620_volition_of_goddess_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 620);
	local questID=620;
end

function sq11_620_volition_of_goddess_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_620_volition_of_goddess_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_620_volition_of_goddess_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,620, 1);
				api_quest_SetJournalStep( pRoom, userObjID,620, 1);
				api_quest_SetQuestStep( pRoom, userObjID,620, 1);
				npc_talk_index = "n277_bluff_dealers_pope-1";
end

</GameServer>