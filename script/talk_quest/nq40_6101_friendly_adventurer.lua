<VillageServer>

function nq40_6101_friendly_adventurer_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 709 then
		nq40_6101_friendly_adventurer_OnTalk_n709_trader_lucita(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n709_trader_lucita--------------------------------------------------------------------------------
function nq40_6101_friendly_adventurer_OnTalk_n709_trader_lucita(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n709_trader_lucita-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n709_trader_lucita-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n709_trader_lucita-accepting-acceptted" then
				api_quest_AddQuest(userObjID,6101, 1);
				api_quest_SetJournalStep(userObjID,6101, 1);
				api_quest_SetQuestStep(userObjID,6101, 1);
				npc_talk_index = "n709_trader_lucita-1";

	end
	if npc_talk_index == "n709_trader_lucita-1-b" then 
	end
	if npc_talk_index == "n709_trader_lucita-1-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 709, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq40_6101_friendly_adventurer_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6101);
end

function nq40_6101_friendly_adventurer_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6101);
end

function nq40_6101_friendly_adventurer_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6101);
	local questID=6101;
end

function nq40_6101_friendly_adventurer_OnRemoteStart( userObjID, questID )
end

function nq40_6101_friendly_adventurer_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq40_6101_friendly_adventurer_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,6101, 1);
				api_quest_SetJournalStep(userObjID,6101, 1);
				api_quest_SetQuestStep(userObjID,6101, 1);
				npc_talk_index = "n709_trader_lucita-1";
end

</VillageServer>

<GameServer>
function nq40_6101_friendly_adventurer_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 709 then
		nq40_6101_friendly_adventurer_OnTalk_n709_trader_lucita( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n709_trader_lucita--------------------------------------------------------------------------------
function nq40_6101_friendly_adventurer_OnTalk_n709_trader_lucita( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n709_trader_lucita-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n709_trader_lucita-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n709_trader_lucita-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,6101, 1);
				api_quest_SetJournalStep( pRoom, userObjID,6101, 1);
				api_quest_SetQuestStep( pRoom, userObjID,6101, 1);
				npc_talk_index = "n709_trader_lucita-1";

	end
	if npc_talk_index == "n709_trader_lucita-1-b" then 
	end
	if npc_talk_index == "n709_trader_lucita-1-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 709, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq40_6101_friendly_adventurer_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6101);
end

function nq40_6101_friendly_adventurer_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6101);
end

function nq40_6101_friendly_adventurer_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6101);
	local questID=6101;
end

function nq40_6101_friendly_adventurer_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq40_6101_friendly_adventurer_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq40_6101_friendly_adventurer_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,6101, 1);
				api_quest_SetJournalStep( pRoom, userObjID,6101, 1);
				api_quest_SetQuestStep( pRoom, userObjID,6101, 1);
				npc_talk_index = "n709_trader_lucita-1";
end

</GameServer>