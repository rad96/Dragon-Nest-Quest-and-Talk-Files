<VillageServer>

function mq15_719_dark_past_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 701 then
		mq15_719_dark_past_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_719_dark_past_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 726 then
		mq15_719_dark_past_OnTalk_n726_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_719_dark_past_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n701_oldkarakule-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n701_oldkarakule-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest(userObjID,719, 2);
				api_quest_SetJournalStep(userObjID,719, 1);
				api_quest_SetQuestStep(userObjID,719, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-j" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-k" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-l" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-m" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-n" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-o" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-p" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-q" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-r" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-s" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-t" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-u" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-v" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-w" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-x" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n701_oldkarakule-3-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_719_dark_past_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n707_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
	end
	if npc_talk_index == "n707_sidel-2-d" then 
	end
	if npc_talk_index == "n707_sidel-2-e" then 
	end
	if npc_talk_index == "n707_sidel-2-f" then 
	end
	if npc_talk_index == "n707_sidel-2-g" then 
	end
	if npc_talk_index == "n707_sidel-2-h" then 
	end
	if npc_talk_index == "n707_sidel-2-i" then 
	end
	if npc_talk_index == "n707_sidel-2-j" then 
	end
	if npc_talk_index == "n707_sidel-2-k" then 
	end
	if npc_talk_index == "n707_sidel-2-l" then 
	end
	if npc_talk_index == "n707_sidel-2-m" then 
	end
	if npc_talk_index == "n707_sidel-2-n" then 
	end
	if npc_talk_index == "n707_sidel-2-o" then 
	end
	if npc_talk_index == "n707_sidel-2-p" then 
	end
	if npc_talk_index == "n707_sidel-2-q" then 
	end
	if npc_talk_index == "n707_sidel-2-r" then 
	end
	if npc_talk_index == "n707_sidel-2-s" then 
	end
	if npc_talk_index == "n707_sidel-2-t" then 
	end
	if npc_talk_index == "n707_sidel-2-u" then 
	end
	if npc_talk_index == "n707_sidel-2-v" then 
	end
	if npc_talk_index == "n707_sidel-2-w" then 
	end
	if npc_talk_index == "n707_sidel-2-x" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n726_velskud--------------------------------------------------------------------------------
function mq15_719_dark_past_OnTalk_n726_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n726_velskud-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n726_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n726_velskud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n726_velskud-4-b" then 
	end
	if npc_talk_index == "n726_velskud-4-c" then 
	end
	if npc_talk_index == "n726_velskud-4-d" then 
	end
	if npc_talk_index == "n726_velskud-4-e" then 
	end
	if npc_talk_index == "n726_velskud-4-f" then 
	end
	if npc_talk_index == "n726_velskud-4-g" then 
	end
	if npc_talk_index == "n726_velskud-4-h" then 
	end
	if npc_talk_index == "n726_velskud-4-i" then 
	end
	if npc_talk_index == "n726_velskud-4-j" then 
	end
	if npc_talk_index == "n726_velskud-4-k" then 
	end
	if npc_talk_index == "n726_velskud-4-l" then 
	end
	if npc_talk_index == "n726_velskud-4-m" then 
	end
	if npc_talk_index == "n726_velskud-4-n" then 
	end
	if npc_talk_index == "n726_velskud-4-o" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7191, true);
				 api_quest_RewardQuestUser(userObjID, 7191, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7192, true);
				 api_quest_RewardQuestUser(userObjID, 7192, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7193, true);
				 api_quest_RewardQuestUser(userObjID, 7193, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7194, true);
				 api_quest_RewardQuestUser(userObjID, 7194, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7195, true);
				 api_quest_RewardQuestUser(userObjID, 7195, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7196, true);
				 api_quest_RewardQuestUser(userObjID, 7196, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7191, true);
				 api_quest_RewardQuestUser(userObjID, 7191, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7191, true);
				 api_quest_RewardQuestUser(userObjID, 7191, questID, 1);
			 end 
	end
	if npc_talk_index == "n726_velskud-4-p" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 720, 2);
					api_quest_SetQuestStep(userObjID, 720, 1);
					api_quest_SetJournalStep(userObjID, 720, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_719_dark_past_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 719);
end

function mq15_719_dark_past_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 719);
end

function mq15_719_dark_past_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 719);
	local questID=719;
end

function mq15_719_dark_past_OnRemoteStart( userObjID, questID )
end

function mq15_719_dark_past_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_719_dark_past_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,719, 2);
				api_quest_SetJournalStep(userObjID,719, 1);
				api_quest_SetQuestStep(userObjID,719, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</VillageServer>

<GameServer>
function mq15_719_dark_past_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 701 then
		mq15_719_dark_past_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_719_dark_past_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 726 then
		mq15_719_dark_past_OnTalk_n726_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_719_dark_past_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n701_oldkarakule-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n701_oldkarakule-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,719, 2);
				api_quest_SetJournalStep( pRoom, userObjID,719, 1);
				api_quest_SetQuestStep( pRoom, userObjID,719, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-j" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-k" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-l" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-m" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-n" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-o" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-p" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-q" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-r" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-s" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-t" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-u" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-v" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-w" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-x" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n701_oldkarakule-3-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_719_dark_past_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n707_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
	end
	if npc_talk_index == "n707_sidel-2-d" then 
	end
	if npc_talk_index == "n707_sidel-2-e" then 
	end
	if npc_talk_index == "n707_sidel-2-f" then 
	end
	if npc_talk_index == "n707_sidel-2-g" then 
	end
	if npc_talk_index == "n707_sidel-2-h" then 
	end
	if npc_talk_index == "n707_sidel-2-i" then 
	end
	if npc_talk_index == "n707_sidel-2-j" then 
	end
	if npc_talk_index == "n707_sidel-2-k" then 
	end
	if npc_talk_index == "n707_sidel-2-l" then 
	end
	if npc_talk_index == "n707_sidel-2-m" then 
	end
	if npc_talk_index == "n707_sidel-2-n" then 
	end
	if npc_talk_index == "n707_sidel-2-o" then 
	end
	if npc_talk_index == "n707_sidel-2-p" then 
	end
	if npc_talk_index == "n707_sidel-2-q" then 
	end
	if npc_talk_index == "n707_sidel-2-r" then 
	end
	if npc_talk_index == "n707_sidel-2-s" then 
	end
	if npc_talk_index == "n707_sidel-2-t" then 
	end
	if npc_talk_index == "n707_sidel-2-u" then 
	end
	if npc_talk_index == "n707_sidel-2-v" then 
	end
	if npc_talk_index == "n707_sidel-2-w" then 
	end
	if npc_talk_index == "n707_sidel-2-x" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n726_velskud--------------------------------------------------------------------------------
function mq15_719_dark_past_OnTalk_n726_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n726_velskud-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n726_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n726_velskud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n726_velskud-4-b" then 
	end
	if npc_talk_index == "n726_velskud-4-c" then 
	end
	if npc_talk_index == "n726_velskud-4-d" then 
	end
	if npc_talk_index == "n726_velskud-4-e" then 
	end
	if npc_talk_index == "n726_velskud-4-f" then 
	end
	if npc_talk_index == "n726_velskud-4-g" then 
	end
	if npc_talk_index == "n726_velskud-4-h" then 
	end
	if npc_talk_index == "n726_velskud-4-i" then 
	end
	if npc_talk_index == "n726_velskud-4-j" then 
	end
	if npc_talk_index == "n726_velskud-4-k" then 
	end
	if npc_talk_index == "n726_velskud-4-l" then 
	end
	if npc_talk_index == "n726_velskud-4-m" then 
	end
	if npc_talk_index == "n726_velskud-4-n" then 
	end
	if npc_talk_index == "n726_velskud-4-o" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7191, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7191, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7192, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7192, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7193, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7193, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7194, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7194, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7195, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7195, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7196, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7196, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7191, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7191, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7191, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7191, questID, 1);
			 end 
	end
	if npc_talk_index == "n726_velskud-4-p" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 720, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 720, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 720, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_719_dark_past_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 719);
end

function mq15_719_dark_past_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 719);
end

function mq15_719_dark_past_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 719);
	local questID=719;
end

function mq15_719_dark_past_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_719_dark_past_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_719_dark_past_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,719, 2);
				api_quest_SetJournalStep( pRoom, userObjID,719, 1);
				api_quest_SetQuestStep( pRoom, userObjID,719, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</GameServer>