<VillageServer>

function sq11_679_purpose_of_mystery_man_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 343 then
		sq11_679_purpose_of_mystery_man_OnTalk_n343_littlegirl_daisy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 345 then
		sq11_679_purpose_of_mystery_man_OnTalk_n345_mystery_man(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n343_littlegirl_daisy--------------------------------------------------------------------------------
function sq11_679_purpose_of_mystery_man_OnTalk_n343_littlegirl_daisy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n343_littlegirl_daisy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n343_littlegirl_daisy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n343_littlegirl_daisy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n343_littlegirl_daisy-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6790, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6790, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6790, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6790, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6790, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6790, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6790, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6790, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6790, false);
			 end 

	end
	if npc_talk_index == "n343_littlegirl_daisy-accepting-acceptted" then
				api_quest_AddQuest(userObjID,679, 1);
				api_quest_SetJournalStep(userObjID,679, 1);
				api_quest_SetQuestStep(userObjID,679, 1);
				npc_talk_index = "n343_littlegirl_daisy-1";

	end
	if npc_talk_index == "n343_littlegirl_daisy-2-b" then 
	end
	if npc_talk_index == "n343_littlegirl_daisy-2-c" then 
	end
	if npc_talk_index == "n343_littlegirl_daisy-2-d" then 
	end
	if npc_talk_index == "n343_littlegirl_daisy-2-e" then 
	end
	if npc_talk_index == "n343_littlegirl_daisy-2-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6790, true);
				 api_quest_RewardQuestUser(userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6790, true);
				 api_quest_RewardQuestUser(userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6790, true);
				 api_quest_RewardQuestUser(userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6790, true);
				 api_quest_RewardQuestUser(userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6790, true);
				 api_quest_RewardQuestUser(userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6790, true);
				 api_quest_RewardQuestUser(userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6790, true);
				 api_quest_RewardQuestUser(userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6790, true);
				 api_quest_RewardQuestUser(userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6790, true);
				 api_quest_RewardQuestUser(userObjID, 6790, questID, 1);
			 end 
	end
	if npc_talk_index == "n343_littlegirl_daisy-2-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n345_mystery_man--------------------------------------------------------------------------------
function sq11_679_purpose_of_mystery_man_OnTalk_n345_mystery_man(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n345_mystery_man-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n345_mystery_man-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n345_mystery_man-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n345_mystery_man-1-b" then 
	end
	if npc_talk_index == "n345_mystery_man-1-c" then 
	end
	if npc_talk_index == "n345_mystery_man-1-d" then 
	end
	if npc_talk_index == "n345_mystery_man-1-e" then 
	end
	if npc_talk_index == "n345_mystery_man-1-ck_job1" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n345_mystery_man-1-m";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n345_mystery_man-1-m";

				else
									npc_talk_index = "n345_mystery_man-1-f";

				end

				end
	end
	if npc_talk_index == "n345_mystery_man-1-g" then 
	end
	if npc_talk_index == "n345_mystery_man-1-h" then 
	end
	if npc_talk_index == "n345_mystery_man-1-i" then 
	end
	if npc_talk_index == "n345_mystery_man-1-j" then 
	end
	if npc_talk_index == "n345_mystery_man-1-k" then 
	end
	if npc_talk_index == "n345_mystery_man-1-l" then 
	end
	if npc_talk_index == "n345_mystery_man-2" then 
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n345_mystery_man-1-h" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_679_purpose_of_mystery_man_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 679);
end

function sq11_679_purpose_of_mystery_man_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 679);
end

function sq11_679_purpose_of_mystery_man_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 679);
	local questID=679;
end

function sq11_679_purpose_of_mystery_man_OnRemoteStart( userObjID, questID )
end

function sq11_679_purpose_of_mystery_man_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_679_purpose_of_mystery_man_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,679, 1);
				api_quest_SetJournalStep(userObjID,679, 1);
				api_quest_SetQuestStep(userObjID,679, 1);
				npc_talk_index = "n343_littlegirl_daisy-1";
end

</VillageServer>

<GameServer>
function sq11_679_purpose_of_mystery_man_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 343 then
		sq11_679_purpose_of_mystery_man_OnTalk_n343_littlegirl_daisy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 345 then
		sq11_679_purpose_of_mystery_man_OnTalk_n345_mystery_man( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n343_littlegirl_daisy--------------------------------------------------------------------------------
function sq11_679_purpose_of_mystery_man_OnTalk_n343_littlegirl_daisy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n343_littlegirl_daisy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n343_littlegirl_daisy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n343_littlegirl_daisy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n343_littlegirl_daisy-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, false);
			 end 

	end
	if npc_talk_index == "n343_littlegirl_daisy-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,679, 1);
				api_quest_SetJournalStep( pRoom, userObjID,679, 1);
				api_quest_SetQuestStep( pRoom, userObjID,679, 1);
				npc_talk_index = "n343_littlegirl_daisy-1";

	end
	if npc_talk_index == "n343_littlegirl_daisy-2-b" then 
	end
	if npc_talk_index == "n343_littlegirl_daisy-2-c" then 
	end
	if npc_talk_index == "n343_littlegirl_daisy-2-d" then 
	end
	if npc_talk_index == "n343_littlegirl_daisy-2-e" then 
	end
	if npc_talk_index == "n343_littlegirl_daisy-2-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6790, questID, 1);
			 end 
	end
	if npc_talk_index == "n343_littlegirl_daisy-2-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n345_mystery_man--------------------------------------------------------------------------------
function sq11_679_purpose_of_mystery_man_OnTalk_n345_mystery_man( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n345_mystery_man-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n345_mystery_man-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n345_mystery_man-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n345_mystery_man-1-b" then 
	end
	if npc_talk_index == "n345_mystery_man-1-c" then 
	end
	if npc_talk_index == "n345_mystery_man-1-d" then 
	end
	if npc_talk_index == "n345_mystery_man-1-e" then 
	end
	if npc_talk_index == "n345_mystery_man-1-ck_job1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n345_mystery_man-1-m";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n345_mystery_man-1-m";

				else
									npc_talk_index = "n345_mystery_man-1-f";

				end

				end
	end
	if npc_talk_index == "n345_mystery_man-1-g" then 
	end
	if npc_talk_index == "n345_mystery_man-1-h" then 
	end
	if npc_talk_index == "n345_mystery_man-1-i" then 
	end
	if npc_talk_index == "n345_mystery_man-1-j" then 
	end
	if npc_talk_index == "n345_mystery_man-1-k" then 
	end
	if npc_talk_index == "n345_mystery_man-1-l" then 
	end
	if npc_talk_index == "n345_mystery_man-2" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n345_mystery_man-1-h" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_679_purpose_of_mystery_man_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 679);
end

function sq11_679_purpose_of_mystery_man_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 679);
end

function sq11_679_purpose_of_mystery_man_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 679);
	local questID=679;
end

function sq11_679_purpose_of_mystery_man_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_679_purpose_of_mystery_man_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_679_purpose_of_mystery_man_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,679, 1);
				api_quest_SetJournalStep( pRoom, userObjID,679, 1);
				api_quest_SetQuestStep( pRoom, userObjID,679, 1);
				npc_talk_index = "n343_littlegirl_daisy-1";
end

</GameServer>