<VillageServer>

function mqc15_9447_shadow_prophetess_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1067 then
		mqc15_9447_shadow_prophetess_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1500 then
		mqc15_9447_shadow_prophetess_OnTalk_n1500_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1558 then
		mqc15_9447_shadow_prophetess_OnTalk_n1558_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1573 then
		mqc15_9447_shadow_prophetess_OnTalk_n1573_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1585 then
		mqc15_9447_shadow_prophetess_OnTalk_n1585_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9447_shadow_prophetess_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1067_elder_elf-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-7-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94470, true);
				 api_quest_RewardQuestUser(userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94470, true);
				 api_quest_RewardQuestUser(userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94470, true);
				 api_quest_RewardQuestUser(userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94470, true);
				 api_quest_RewardQuestUser(userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94470, true);
				 api_quest_RewardQuestUser(userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94470, true);
				 api_quest_RewardQuestUser(userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94470, true);
				 api_quest_RewardQuestUser(userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94470, true);
				 api_quest_RewardQuestUser(userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94470, true);
				 api_quest_RewardQuestUser(userObjID, 94470, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-7-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1067_elder_elf-7-c" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1500_gaharam--------------------------------------------------------------------------------
function mqc15_9447_shadow_prophetess_OnTalk_n1500_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1500_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1500_gaharam-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1500_gaharam-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1500_gaharam-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1500_gaharam-1-b" then 
	end
	if npc_talk_index == "n1500_gaharam-1-c" then 
	end
	if npc_talk_index == "n1500_gaharam-1-d" then 
	end
	if npc_talk_index == "n1500_gaharam-1-e" then 
	end
	if npc_talk_index == "n1500_gaharam-1-f" then 
	end
	if npc_talk_index == "n1500_gaharam-1-g" then 
	end
	if npc_talk_index == "n1500_gaharam-1-h" then 
	end
	if npc_talk_index == "n1500_gaharam-1-i" then 
	end
	if npc_talk_index == "n1500_gaharam-1-j" then 
	end
	if npc_talk_index == "n1500_gaharam-1-k" then 
	end
	if npc_talk_index == "n1500_gaharam-1-l" then 
	end
	if npc_talk_index == "n1500_gaharam-1-m" then 
	end
	if npc_talk_index == "n1500_gaharam-1-n" then 
	end
	if npc_talk_index == "n1500_gaharam-1-o" then 
	end
	if npc_talk_index == "n1500_gaharam-1-p" then 
	end
	if npc_talk_index == "n1500_gaharam-1-q" then 
	end
	if npc_talk_index == "n1500_gaharam-1-r" then 
	end
	if npc_talk_index == "n1500_gaharam-1-s" then 
	end
	if npc_talk_index == "n1500_gaharam-1-t" then 
	end
	if npc_talk_index == "n1500_gaharam-1-u" then 
	end
	if npc_talk_index == "n1500_gaharam-1-v" then 
	end
	if npc_talk_index == "n1500_gaharam-1-w" then 
	end
	if npc_talk_index == "n1500_gaharam-1-x" then 
	end
	if npc_talk_index == "n1500_gaharam-1-y" then 
	end
	if npc_talk_index == "n1500_gaharam-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1500_gaharam-2-b" then 
	end
	if npc_talk_index == "n1500_gaharam-2-c" then 
	end
	if npc_talk_index == "n1500_gaharam-2-d" then 
	end
	if npc_talk_index == "n1500_gaharam-2-e" then 
	end
	if npc_talk_index == "n1500_gaharam-2-f" then 
	end
	if npc_talk_index == "n1500_gaharam-2-g" then 
	end
	if npc_talk_index == "n1500_gaharam-2-h" then 
	end
	if npc_talk_index == "n1500_gaharam-2-i" then 
	end
	if npc_talk_index == "n1500_gaharam-2-j" then 
	end
	if npc_talk_index == "n1500_gaharam-2-k" then 
	end
	if npc_talk_index == "n1500_gaharam-2-l" then 
	end
	if npc_talk_index == "n1500_gaharam-2-m" then 
	end
	if npc_talk_index == "n1500_gaharam-2-n" then 
	end
	if npc_talk_index == "n1500_gaharam-2-o" then 
	end
	if npc_talk_index == "n1500_gaharam-2-p" then 
	end
	if npc_talk_index == "n1500_gaharam-2-q" then 
	end
	if npc_talk_index == "n1500_gaharam-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n1500_gaharam-3-b" then 
	end
	if npc_talk_index == "n1500_gaharam-3-c" then 
	end
	if npc_talk_index == "n1500_gaharam-3-d" then 
	end
	if npc_talk_index == "n1500_gaharam-3-e" then 
	end
	if npc_talk_index == "n1500_gaharam-3-f" then 
	end
	if npc_talk_index == "n1500_gaharam-3-g" then 
	end
	if npc_talk_index == "n1500_gaharam-3-h" then 
	end
	if npc_talk_index == "n1500_gaharam-3-i" then 
	end
	if npc_talk_index == "n1500_gaharam-3-j" then 
	end
	if npc_talk_index == "n1500_gaharam-3-k" then 
	end
	if npc_talk_index == "n1500_gaharam-3-l" then 
	end
	if npc_talk_index == "n1500_gaharam-3-m" then 
	end
	if npc_talk_index == "n1500_gaharam-3-n" then 
	end
	if npc_talk_index == "n1500_gaharam-3-n" then 
	end
	if npc_talk_index == "n1500_gaharam-3-n" then 
	end
	if npc_talk_index == "n1500_gaharam-3-o" then 
	end
	if npc_talk_index == "n1500_gaharam-3-p" then 
	end
	if npc_talk_index == "n1500_gaharam-3-q" then 
	end
	if npc_talk_index == "n1500_gaharam-3-r" then 
	end
	if npc_talk_index == "n1500_gaharam-3-s" then 
	end
	if npc_talk_index == "n1500_gaharam-3-t" then 
	end
	if npc_talk_index == "n1500_gaharam-3-u" then 
	end
	if npc_talk_index == "n1500_gaharam-3-v" then 
	end
	if npc_talk_index == "n1500_gaharam-3-w" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1500_gaharam-3-x" then 
	end
	if npc_talk_index == "n1500_gaharam-3-y" then 
	end
	if npc_talk_index == "n1500_gaharam-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1558_gaharam--------------------------------------------------------------------------------
function mqc15_9447_shadow_prophetess_OnTalk_n1558_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1558_gaharam-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1558_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1558_gaharam-accepting-acceptted_1" then
				api_quest_AddQuest(userObjID,9447, 2);
				api_quest_SetJournalStep(userObjID,9447, 1);
				api_quest_SetQuestStep(userObjID,9447, 1);
				npc_talk_index = "n1558_gaharam-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1573_gaharam--------------------------------------------------------------------------------
function mqc15_9447_shadow_prophetess_OnTalk_n1573_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1573_gaharam-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1573_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1573_gaharam-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,9447, 2);
				api_quest_SetJournalStep(userObjID,9447, 1);
				api_quest_SetQuestStep(userObjID,9447, 1);
				npc_talk_index = "n1573_gaharam-1";

	end
	if npc_talk_index == "n1573_gaharam-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1585_for_memory--------------------------------------------------------------------------------
function mqc15_9447_shadow_prophetess_OnTalk_n1585_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1585_for_memory-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1585_for_memory-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1585_for_memory-5-b" then 
	end
	if npc_talk_index == "n1585_for_memory-5-c" then 
	end
	if npc_talk_index == "n1585_for_memory-5-d" then 
	end
	if npc_talk_index == "n1585_for_memory-5-e" then 
	end
	if npc_talk_index == "n1585_for_memory-5-f" then 
	end
	if npc_talk_index == "n1585_for_memory-5-g" then 
	end
	if npc_talk_index == "n1585_for_memory-5-h" then 
	end
	if npc_talk_index == "n1585_for_memory-5-i" then 
	end
	if npc_talk_index == "n1585_for_memory-5-j" then 
	end
	if npc_talk_index == "n1585_for_memory-5-k" then 
	end
	if npc_talk_index == "n1585_for_memory-5-l" then 
	end
	if npc_talk_index == "n1585_for_memory-5-m" then 
	end
	if npc_talk_index == "n1585_for_memory-5-n" then 
	end
	if npc_talk_index == "n1585_for_memory-5-o" then 
	end
	if npc_talk_index == "n1585_for_memory-5-p" then 
	end
	if npc_talk_index == "n1585_for_memory-5-q" then 
	end
	if npc_talk_index == "n1585_for_memory-5-r" then 
	end
	if npc_talk_index == "n1585_for_memory-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9447_shadow_prophetess_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9447);
end

function mqc15_9447_shadow_prophetess_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9447);
end

function mqc15_9447_shadow_prophetess_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9447);
	local questID=9447;
end

function mqc15_9447_shadow_prophetess_OnRemoteStart( userObjID, questID )
end

function mqc15_9447_shadow_prophetess_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9447_shadow_prophetess_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mqc15_9447_shadow_prophetess_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1067 then
		mqc15_9447_shadow_prophetess_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1500 then
		mqc15_9447_shadow_prophetess_OnTalk_n1500_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1558 then
		mqc15_9447_shadow_prophetess_OnTalk_n1558_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1573 then
		mqc15_9447_shadow_prophetess_OnTalk_n1573_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1585 then
		mqc15_9447_shadow_prophetess_OnTalk_n1585_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9447_shadow_prophetess_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1067_elder_elf-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-7-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94470, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-7-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1067_elder_elf-7-c" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1500_gaharam--------------------------------------------------------------------------------
function mqc15_9447_shadow_prophetess_OnTalk_n1500_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1500_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1500_gaharam-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1500_gaharam-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1500_gaharam-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1500_gaharam-1-b" then 
	end
	if npc_talk_index == "n1500_gaharam-1-c" then 
	end
	if npc_talk_index == "n1500_gaharam-1-d" then 
	end
	if npc_talk_index == "n1500_gaharam-1-e" then 
	end
	if npc_talk_index == "n1500_gaharam-1-f" then 
	end
	if npc_talk_index == "n1500_gaharam-1-g" then 
	end
	if npc_talk_index == "n1500_gaharam-1-h" then 
	end
	if npc_talk_index == "n1500_gaharam-1-i" then 
	end
	if npc_talk_index == "n1500_gaharam-1-j" then 
	end
	if npc_talk_index == "n1500_gaharam-1-k" then 
	end
	if npc_talk_index == "n1500_gaharam-1-l" then 
	end
	if npc_talk_index == "n1500_gaharam-1-m" then 
	end
	if npc_talk_index == "n1500_gaharam-1-n" then 
	end
	if npc_talk_index == "n1500_gaharam-1-o" then 
	end
	if npc_talk_index == "n1500_gaharam-1-p" then 
	end
	if npc_talk_index == "n1500_gaharam-1-q" then 
	end
	if npc_talk_index == "n1500_gaharam-1-r" then 
	end
	if npc_talk_index == "n1500_gaharam-1-s" then 
	end
	if npc_talk_index == "n1500_gaharam-1-t" then 
	end
	if npc_talk_index == "n1500_gaharam-1-u" then 
	end
	if npc_talk_index == "n1500_gaharam-1-v" then 
	end
	if npc_talk_index == "n1500_gaharam-1-w" then 
	end
	if npc_talk_index == "n1500_gaharam-1-x" then 
	end
	if npc_talk_index == "n1500_gaharam-1-y" then 
	end
	if npc_talk_index == "n1500_gaharam-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1500_gaharam-2-b" then 
	end
	if npc_talk_index == "n1500_gaharam-2-c" then 
	end
	if npc_talk_index == "n1500_gaharam-2-d" then 
	end
	if npc_talk_index == "n1500_gaharam-2-e" then 
	end
	if npc_talk_index == "n1500_gaharam-2-f" then 
	end
	if npc_talk_index == "n1500_gaharam-2-g" then 
	end
	if npc_talk_index == "n1500_gaharam-2-h" then 
	end
	if npc_talk_index == "n1500_gaharam-2-i" then 
	end
	if npc_talk_index == "n1500_gaharam-2-j" then 
	end
	if npc_talk_index == "n1500_gaharam-2-k" then 
	end
	if npc_talk_index == "n1500_gaharam-2-l" then 
	end
	if npc_talk_index == "n1500_gaharam-2-m" then 
	end
	if npc_talk_index == "n1500_gaharam-2-n" then 
	end
	if npc_talk_index == "n1500_gaharam-2-o" then 
	end
	if npc_talk_index == "n1500_gaharam-2-p" then 
	end
	if npc_talk_index == "n1500_gaharam-2-q" then 
	end
	if npc_talk_index == "n1500_gaharam-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n1500_gaharam-3-b" then 
	end
	if npc_talk_index == "n1500_gaharam-3-c" then 
	end
	if npc_talk_index == "n1500_gaharam-3-d" then 
	end
	if npc_talk_index == "n1500_gaharam-3-e" then 
	end
	if npc_talk_index == "n1500_gaharam-3-f" then 
	end
	if npc_talk_index == "n1500_gaharam-3-g" then 
	end
	if npc_talk_index == "n1500_gaharam-3-h" then 
	end
	if npc_talk_index == "n1500_gaharam-3-i" then 
	end
	if npc_talk_index == "n1500_gaharam-3-j" then 
	end
	if npc_talk_index == "n1500_gaharam-3-k" then 
	end
	if npc_talk_index == "n1500_gaharam-3-l" then 
	end
	if npc_talk_index == "n1500_gaharam-3-m" then 
	end
	if npc_talk_index == "n1500_gaharam-3-n" then 
	end
	if npc_talk_index == "n1500_gaharam-3-n" then 
	end
	if npc_talk_index == "n1500_gaharam-3-n" then 
	end
	if npc_talk_index == "n1500_gaharam-3-o" then 
	end
	if npc_talk_index == "n1500_gaharam-3-p" then 
	end
	if npc_talk_index == "n1500_gaharam-3-q" then 
	end
	if npc_talk_index == "n1500_gaharam-3-r" then 
	end
	if npc_talk_index == "n1500_gaharam-3-s" then 
	end
	if npc_talk_index == "n1500_gaharam-3-t" then 
	end
	if npc_talk_index == "n1500_gaharam-3-u" then 
	end
	if npc_talk_index == "n1500_gaharam-3-v" then 
	end
	if npc_talk_index == "n1500_gaharam-3-w" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1500_gaharam-3-x" then 
	end
	if npc_talk_index == "n1500_gaharam-3-y" then 
	end
	if npc_talk_index == "n1500_gaharam-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1558_gaharam--------------------------------------------------------------------------------
function mqc15_9447_shadow_prophetess_OnTalk_n1558_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1558_gaharam-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1558_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1558_gaharam-accepting-acceptted_1" then
				api_quest_AddQuest( pRoom, userObjID,9447, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9447, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9447, 1);
				npc_talk_index = "n1558_gaharam-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1573_gaharam--------------------------------------------------------------------------------
function mqc15_9447_shadow_prophetess_OnTalk_n1573_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1573_gaharam-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1573_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1573_gaharam-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,9447, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9447, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9447, 1);
				npc_talk_index = "n1573_gaharam-1";

	end
	if npc_talk_index == "n1573_gaharam-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1585_for_memory--------------------------------------------------------------------------------
function mqc15_9447_shadow_prophetess_OnTalk_n1585_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1585_for_memory-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1585_for_memory-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1585_for_memory-5-b" then 
	end
	if npc_talk_index == "n1585_for_memory-5-c" then 
	end
	if npc_talk_index == "n1585_for_memory-5-d" then 
	end
	if npc_talk_index == "n1585_for_memory-5-e" then 
	end
	if npc_talk_index == "n1585_for_memory-5-f" then 
	end
	if npc_talk_index == "n1585_for_memory-5-g" then 
	end
	if npc_talk_index == "n1585_for_memory-5-h" then 
	end
	if npc_talk_index == "n1585_for_memory-5-i" then 
	end
	if npc_talk_index == "n1585_for_memory-5-j" then 
	end
	if npc_talk_index == "n1585_for_memory-5-k" then 
	end
	if npc_talk_index == "n1585_for_memory-5-l" then 
	end
	if npc_talk_index == "n1585_for_memory-5-m" then 
	end
	if npc_talk_index == "n1585_for_memory-5-n" then 
	end
	if npc_talk_index == "n1585_for_memory-5-o" then 
	end
	if npc_talk_index == "n1585_for_memory-5-p" then 
	end
	if npc_talk_index == "n1585_for_memory-5-q" then 
	end
	if npc_talk_index == "n1585_for_memory-5-r" then 
	end
	if npc_talk_index == "n1585_for_memory-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9447_shadow_prophetess_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9447);
end

function mqc15_9447_shadow_prophetess_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9447);
end

function mqc15_9447_shadow_prophetess_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9447);
	local questID=9447;
end

function mqc15_9447_shadow_prophetess_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9447_shadow_prophetess_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9447_shadow_prophetess_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>