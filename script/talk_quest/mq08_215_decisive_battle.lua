<VillageServer>

function mq08_215_decisive_battle_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 24 then
		mq08_215_decisive_battle_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mq08_215_decisive_battle_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest(userObjID,215, 2);
				api_quest_SetJournalStep(userObjID,215, 1);
				api_quest_SetQuestStep(userObjID,215, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-a" then 
				if api_user_GetUserLevel(userObjID) >= 20 then
									npc_talk_index = "n024_adventurer_guildmaster_decud-1-a";

				else
									npc_talk_index = "n024_adventurer_guildmaster_decud-1-d";

				end
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-c" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 612, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200612, 30000);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2151, true);
				 api_quest_RewardQuestUser(userObjID, 2151, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2152, true);
				 api_quest_RewardQuestUser(userObjID, 2152, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2153, true);
				 api_quest_RewardQuestUser(userObjID, 2153, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2154, true);
				 api_quest_RewardQuestUser(userObjID, 2154, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2156, true);
				 api_quest_RewardQuestUser(userObjID, 2156, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2156, true);
				 api_quest_RewardQuestUser(userObjID, 2156, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2151, true);
				 api_quest_RewardQuestUser(userObjID, 2151, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2158, true);
				 api_quest_RewardQuestUser(userObjID, 2158, questID, 1);
			 end 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-com_01" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_quest_AddQuest(userObjID,216, 2);
				api_quest_SetJournalStep(userObjID,216, 1);
				api_quest_SetQuestStep(userObjID,216, 1);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-com_02" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_quest_AddQuest(userObjID,9476, 2);
				api_quest_SetJournalStep(userObjID,9476, 1);
				api_quest_SetQuestStep(userObjID,9476, 1);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_215_decisive_battle_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 215);
	if qstep == 2 and CountIndex == 612 then
				api_quest_ClearCountingInfo(userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300103, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300103, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200612 then
				api_quest_IncCounting(userObjID, 2, 612);

	end
end

function mq08_215_decisive_battle_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 215);
	if qstep == 2 and CountIndex == 612 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200612 and Count >= TargetCount  then

	end
end

function mq08_215_decisive_battle_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 215);
	local questID=215;
end

function mq08_215_decisive_battle_OnRemoteStart( userObjID, questID )
end

function mq08_215_decisive_battle_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq08_215_decisive_battle_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,215, 2);
				api_quest_SetJournalStep(userObjID,215, 1);
				api_quest_SetQuestStep(userObjID,215, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
end

</VillageServer>

<GameServer>
function mq08_215_decisive_battle_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 24 then
		mq08_215_decisive_battle_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mq08_215_decisive_battle_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,215, 2);
				api_quest_SetJournalStep( pRoom, userObjID,215, 1);
				api_quest_SetQuestStep( pRoom, userObjID,215, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-a" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 20 then
									npc_talk_index = "n024_adventurer_guildmaster_decud-1-a";

				else
									npc_talk_index = "n024_adventurer_guildmaster_decud-1-d";

				end
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 612, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200612, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2151, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2151, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2152, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2152, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2153, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2153, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2154, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2154, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2156, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2156, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2156, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2156, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2151, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2151, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2158, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2158, questID, 1);
			 end 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-com_01" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_quest_AddQuest( pRoom, userObjID,216, 2);
				api_quest_SetJournalStep( pRoom, userObjID,216, 1);
				api_quest_SetQuestStep( pRoom, userObjID,216, 1);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-com_02" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_quest_AddQuest( pRoom, userObjID,9476, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9476, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9476, 1);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_215_decisive_battle_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 215);
	if qstep == 2 and CountIndex == 612 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300103, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300103, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200612 then
				api_quest_IncCounting( pRoom, userObjID, 2, 612);

	end
end

function mq08_215_decisive_battle_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 215);
	if qstep == 2 and CountIndex == 612 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200612 and Count >= TargetCount  then

	end
end

function mq08_215_decisive_battle_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 215);
	local questID=215;
end

function mq08_215_decisive_battle_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq08_215_decisive_battle_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq08_215_decisive_battle_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,215, 2);
				api_quest_SetJournalStep( pRoom, userObjID,215, 1);
				api_quest_SetQuestStep( pRoom, userObjID,215, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
end

</GameServer>