<VillageServer>

function sq08_281_test_archer2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 33 then
		sq08_281_test_archer2_OnTalk_n033_archer_master_adellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n033_archer_master_adellin--------------------------------------------------------------------------------
function sq08_281_test_archer2_OnTalk_n033_archer_master_adellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n033_archer_master_adellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n033_archer_master_adellin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n033_archer_master_adellin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n033_archer_master_adellin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n033_archer_master_adellin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n033_archer_master_adellin-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2810, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2810, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2810, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2810, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2810, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2810, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2810, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2810, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2810, false);
			 end 

	end
	if npc_talk_index == "n033_archer_master_adellin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,281, 1);
				api_quest_SetJournalStep(userObjID,281, 1);
				api_quest_SetQuestStep(userObjID,281, 1);
				npc_talk_index = "n033_archer_master_adellin-1";

	end
	if npc_talk_index == "n033_archer_master_adellin-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300018, 1);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n033_archer_master_adellin-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);

				if api_quest_HasQuestItem(userObjID, 300018, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300018, api_quest_HasQuestItem(userObjID, 300018, 1));
				end
	end
	if npc_talk_index == "n033_archer_master_adellin-4" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2811, true);
				 api_quest_RewardQuestUser(userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2811, true);
				 api_quest_RewardQuestUser(userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2811, true);
				 api_quest_RewardQuestUser(userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2811, true);
				 api_quest_RewardQuestUser(userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2811, true);
				 api_quest_RewardQuestUser(userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2811, true);
				 api_quest_RewardQuestUser(userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2811, true);
				 api_quest_RewardQuestUser(userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2811, true);
				 api_quest_RewardQuestUser(userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2811, true);
				 api_quest_RewardQuestUser(userObjID, 2811, questID, 1);
			 end 
	end
	if npc_talk_index == "n033_archer_master_adellin-4" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2812, true);
				 api_quest_RewardQuestUser(userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2812, true);
				 api_quest_RewardQuestUser(userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2812, true);
				 api_quest_RewardQuestUser(userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2812, true);
				 api_quest_RewardQuestUser(userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2812, true);
				 api_quest_RewardQuestUser(userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2812, true);
				 api_quest_RewardQuestUser(userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2812, true);
				 api_quest_RewardQuestUser(userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2812, true);
				 api_quest_RewardQuestUser(userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2812, true);
				 api_quest_RewardQuestUser(userObjID, 2812, questID, 1);
			 end 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-complete_quest1" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 14);
				npc_talk_index = "n033_archer_master_adellin-4-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n033_archer_master_adellin-4-complete_quest2" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 15);
				npc_talk_index = "n033_archer_master_adellin-4-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n033_archer_master_adellin-4-next_quest_check" then 
				if api_quest_IsMarkingCompleteQuest(userObjID, 341) == 1  then
									npc_talk_index = "n033_archer_master_adellin-4-f";

				else
									api_quest_AddQuest(userObjID,341, 1);
				api_quest_SetJournalStep(userObjID,341, 1);
				api_quest_SetQuestStep(userObjID,341, 1);
				api_npc_NextScript(userObjID, npcObjID, "n033_archer_master_adellin-1", "sq08_341_change_job2.xml");

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_281_test_archer2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 281);
	if qstep == 2 and CountIndex == 413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300018, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300018, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300018 then

	end
	if qstep == 2 and CountIndex == 200413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300018, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300018, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_281_test_archer2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 281);
	if qstep == 2 and CountIndex == 413 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300018 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200413 and Count >= TargetCount  then

	end
end

function sq08_281_test_archer2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 281);
	local questID=281;
end

function sq08_281_test_archer2_OnRemoteStart( userObjID, questID )
end

function sq08_281_test_archer2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_281_test_archer2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,281, 1);
				api_quest_SetJournalStep(userObjID,281, 1);
				api_quest_SetQuestStep(userObjID,281, 1);
				npc_talk_index = "n033_archer_master_adellin-1";
end

</VillageServer>

<GameServer>
function sq08_281_test_archer2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 33 then
		sq08_281_test_archer2_OnTalk_n033_archer_master_adellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n033_archer_master_adellin--------------------------------------------------------------------------------
function sq08_281_test_archer2_OnTalk_n033_archer_master_adellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n033_archer_master_adellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n033_archer_master_adellin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n033_archer_master_adellin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n033_archer_master_adellin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n033_archer_master_adellin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n033_archer_master_adellin-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2810, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2810, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2810, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2810, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2810, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2810, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2810, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2810, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2810, false);
			 end 

	end
	if npc_talk_index == "n033_archer_master_adellin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,281, 1);
				api_quest_SetJournalStep( pRoom, userObjID,281, 1);
				api_quest_SetQuestStep( pRoom, userObjID,281, 1);
				npc_talk_index = "n033_archer_master_adellin-1";

	end
	if npc_talk_index == "n033_archer_master_adellin-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300018, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n033_archer_master_adellin-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);

				if api_quest_HasQuestItem( pRoom, userObjID, 300018, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300018, api_quest_HasQuestItem( pRoom, userObjID, 300018, 1));
				end
	end
	if npc_talk_index == "n033_archer_master_adellin-4" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2811, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2811, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2811, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2811, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2811, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2811, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2811, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2811, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2811, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2811, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2811, questID, 1);
			 end 
	end
	if npc_talk_index == "n033_archer_master_adellin-4" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2812, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2812, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2812, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2812, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2812, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2812, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2812, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2812, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2812, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2812, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2812, questID, 1);
			 end 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-complete_quest1" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 14);
				npc_talk_index = "n033_archer_master_adellin-4-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n033_archer_master_adellin-4-complete_quest2" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 15);
				npc_talk_index = "n033_archer_master_adellin-4-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n033_archer_master_adellin-4-next_quest_check" then 
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 341) == 1  then
									npc_talk_index = "n033_archer_master_adellin-4-f";

				else
									api_quest_AddQuest( pRoom, userObjID,341, 1);
				api_quest_SetJournalStep( pRoom, userObjID,341, 1);
				api_quest_SetQuestStep( pRoom, userObjID,341, 1);
				api_npc_NextScript( pRoom, userObjID, npcObjID, "n033_archer_master_adellin-1", "sq08_341_change_job2.xml");

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_281_test_archer2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 281);
	if qstep == 2 and CountIndex == 413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300018, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300018, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300018 then

	end
	if qstep == 2 and CountIndex == 200413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300018, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300018, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_281_test_archer2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 281);
	if qstep == 2 and CountIndex == 413 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300018 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200413 and Count >= TargetCount  then

	end
end

function sq08_281_test_archer2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 281);
	local questID=281;
end

function sq08_281_test_archer2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_281_test_archer2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_281_test_archer2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,281, 1);
				api_quest_SetJournalStep( pRoom, userObjID,281, 1);
				api_quest_SetQuestStep( pRoom, userObjID,281, 1);
				npc_talk_index = "n033_archer_master_adellin-1";
end

</GameServer>