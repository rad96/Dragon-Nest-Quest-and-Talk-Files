<VillageServer>

function sq08_325_legend_of_legendary_weapon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 25 then
		sq08_325_legend_of_legendary_weapon_OnTalk_n025_trader_corin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 999 then
		sq08_325_legend_of_legendary_weapon_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n025_trader_corin--------------------------------------------------------------------------------
function sq08_325_legend_of_legendary_weapon_OnTalk_n025_trader_corin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n025_trader_corin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n025_trader_corin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n025_trader_corin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n025_trader_corin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n025_trader_corin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n025_trader_corin-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n025_trader_corin-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3251, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3252, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3253, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3254, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3255, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3256, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3257, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3258, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3259, false);
			 end 

	end
	if npc_talk_index == "n025_trader_corin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,325, 1);
				api_quest_SetJournalStep(userObjID,325, 1);
				api_quest_SetQuestStep(userObjID,325, 1);
				npc_talk_index = "n025_trader_corin-1";

	end
	if npc_talk_index == "n025_trader_corin-1-repeatchk" then 
				if api_user_IsMissionAchieved(userObjID, 3062) == 1  then
									npc_talk_index = "n025_trader_corin-1-i";

				else
									if api_user_IsMissionAchieved(userObjID, 3063) == 1  then
									npc_talk_index = "n025_trader_corin-1-k";

				else
									npc_talk_index = "n025_trader_corin-1-a";

				end

				end
	end
	if npc_talk_index == "n025_trader_corin-1-b" then 
	end
	if npc_talk_index == "n025_trader_corin-1-upgrade7check" then 
				if api_user_IsMissionAchieved(userObjID, 12) == 1  then
									npc_talk_index = "n025_trader_corin-1-c";

				else
									npc_talk_index = "n025_trader_corin-1-e";

				end
	end
	if npc_talk_index == "n025_trader_corin-1-d" then 
	end
	if npc_talk_index == "n025_trader_corin-1-d" then 
	end
	if npc_talk_index == "n025_trader_corin-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n025_trader_corin-1-f" then 
	end
	if npc_talk_index == "n025_trader_corin-1-g" then 
	end
	if npc_talk_index == "n025_trader_corin-1-m" then 
	end
	if npc_talk_index == "n025_trader_corin-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n025_trader_corin-1-h" then 
	end
	if npc_talk_index == "n025_trader_corin-1-h" then 
	end
	if npc_talk_index == "n025_trader_corin-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n025_trader_corin-1-j" then 
	end
	if npc_talk_index == "n025_trader_corin-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n025_trader_corin-1-l" then 
	end
	if npc_talk_index == "n025_trader_corin-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n025_trader_corin-1-f" then 
	end
	if npc_talk_index == "n025_trader_corin-2-b" then 
	end
	if npc_talk_index == "n025_trader_corin-2-c" then 
	end
	if npc_talk_index == "n025_trader_corin-2-d" then 
	end
	if npc_talk_index == "n025_trader_corin-2-d" then 
	end
	if npc_talk_index == "n025_trader_corin-2-e" then 
	end
	if npc_talk_index == "n025_trader_corin-2-invenchk" then 
				if api_user_HasItem(userObjID, 536872356, 1) >= 1 then
									npc_talk_index = "n025_trader_corin-2-f";

				else
									if api_user_GetUserInvenBlankCount(userObjID) >= 1 then
									if api_user_CheckInvenForAddItem(userObjID, 536872356, 1) == 1 then
					api_user_AddItem(userObjID, 536872356, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n025_trader_corin-2-f";

				else
									npc_talk_index = "n025_trader_corin-2-m";

				end

				end
	end
	if npc_talk_index == "n025_trader_corin-2-g" then 
	end
	if npc_talk_index == "n025_trader_corin-2-h" then 
	end
	if npc_talk_index == "n025_trader_corin-2-i" then 
	end
	if npc_talk_index == "n025_trader_corin-2-j" then 
	end
	if npc_talk_index == "n025_trader_corin-2-k" then 
	end
	if npc_talk_index == "n025_trader_corin-2-l" then 
	end
	if npc_talk_index == "n025_trader_corin-2-openupgradeui" then 
				api_ui_OpenUpgradeItem(userObjID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n025_trader_corin-3-checkdestroy" then 
				if api_user_IsMissionAchieved(userObjID, 3062) == 1  then
									api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n025_trader_corin-4";

				else
									if api_user_IsMissionAchieved(userObjID, 3063) == 1  then
									api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n025_trader_corin-4";

				else
									npc_talk_index = "n025_trader_corin-3-a";

				end

				end
	end
	if npc_talk_index == "n025_trader_corin-3-openupgradeui2" then 
				api_ui_OpenUpgradeItem(userObjID);
	end
	if npc_talk_index == "n025_trader_corin-4-check4" then 
				if api_user_IsMissionAchieved(userObjID, 3062) == 1  then
									npc_talk_index = "n025_trader_corin-4-a";

				else
									npc_talk_index = "n025_trader_corin-4-b";

				end
	end
	if npc_talk_index == "n025_trader_corin-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n025_trader_corin-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n025_trader_corin-5-b" then 
	end
	if npc_talk_index == "n025_trader_corin-5-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3251, true);
				 api_quest_RewardQuestUser(userObjID, 3251, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3252, true);
				 api_quest_RewardQuestUser(userObjID, 3252, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3253, true);
				 api_quest_RewardQuestUser(userObjID, 3253, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3254, true);
				 api_quest_RewardQuestUser(userObjID, 3254, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3255, true);
				 api_quest_RewardQuestUser(userObjID, 3255, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3256, true);
				 api_quest_RewardQuestUser(userObjID, 3256, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3257, true);
				 api_quest_RewardQuestUser(userObjID, 3257, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3258, true);
				 api_quest_RewardQuestUser(userObjID, 3258, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3259, true);
				 api_quest_RewardQuestUser(userObjID, 3259, questID, 1);
			 end 
	end
	if npc_talk_index == "n025_trader_corin-5-chkend" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_user_HasItem(userObjID, 536872356, 1) > 0 then 
					api_user_DelItem(userObjID, 536872356, api_user_HasItem(userObjID, 536872356, 1, questID));
				end


				else
					npc_talk_index = "_full_inventory";

				end
				if api_user_IsMissionAchieved(userObjID, 3062) == 1  then
									npc_talk_index = "n025_trader_corin-5-d";

				else
									npc_talk_index = "n025_trader_corin-5-f";

				end
	end
	if npc_talk_index == "n025_trader_corin-5-a" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n999_remote--------------------------------------------------------------------------------
function sq08_325_legend_of_legendary_weapon_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n999_remote-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_325_legend_of_legendary_weapon_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 325);
end

function sq08_325_legend_of_legendary_weapon_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 325);
end

function sq08_325_legend_of_legendary_weapon_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 325);
	local questID=325;
end

function sq08_325_legend_of_legendary_weapon_OnRemoteStart( userObjID, questID )
end

function sq08_325_legend_of_legendary_weapon_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_325_legend_of_legendary_weapon_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,325, 1);
				api_quest_SetJournalStep(userObjID,325, 1);
				api_quest_SetQuestStep(userObjID,325, 1);
				npc_talk_index = "n025_trader_corin-1";
end

</VillageServer>

<GameServer>
function sq08_325_legend_of_legendary_weapon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 25 then
		sq08_325_legend_of_legendary_weapon_OnTalk_n025_trader_corin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 999 then
		sq08_325_legend_of_legendary_weapon_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n025_trader_corin--------------------------------------------------------------------------------
function sq08_325_legend_of_legendary_weapon_OnTalk_n025_trader_corin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n025_trader_corin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n025_trader_corin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n025_trader_corin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n025_trader_corin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n025_trader_corin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n025_trader_corin-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n025_trader_corin-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3251, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3252, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3253, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3254, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3255, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3256, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3257, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3258, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3259, false);
			 end 

	end
	if npc_talk_index == "n025_trader_corin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,325, 1);
				api_quest_SetJournalStep( pRoom, userObjID,325, 1);
				api_quest_SetQuestStep( pRoom, userObjID,325, 1);
				npc_talk_index = "n025_trader_corin-1";

	end
	if npc_talk_index == "n025_trader_corin-1-repeatchk" then 
				if api_user_IsMissionAchieved( pRoom, userObjID, 3062) == 1  then
									npc_talk_index = "n025_trader_corin-1-i";

				else
									if api_user_IsMissionAchieved( pRoom, userObjID, 3063) == 1  then
									npc_talk_index = "n025_trader_corin-1-k";

				else
									npc_talk_index = "n025_trader_corin-1-a";

				end

				end
	end
	if npc_talk_index == "n025_trader_corin-1-b" then 
	end
	if npc_talk_index == "n025_trader_corin-1-upgrade7check" then 
				if api_user_IsMissionAchieved( pRoom, userObjID, 12) == 1  then
									npc_talk_index = "n025_trader_corin-1-c";

				else
									npc_talk_index = "n025_trader_corin-1-e";

				end
	end
	if npc_talk_index == "n025_trader_corin-1-d" then 
	end
	if npc_talk_index == "n025_trader_corin-1-d" then 
	end
	if npc_talk_index == "n025_trader_corin-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n025_trader_corin-1-f" then 
	end
	if npc_talk_index == "n025_trader_corin-1-g" then 
	end
	if npc_talk_index == "n025_trader_corin-1-m" then 
	end
	if npc_talk_index == "n025_trader_corin-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n025_trader_corin-1-h" then 
	end
	if npc_talk_index == "n025_trader_corin-1-h" then 
	end
	if npc_talk_index == "n025_trader_corin-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n025_trader_corin-1-j" then 
	end
	if npc_talk_index == "n025_trader_corin-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n025_trader_corin-1-l" then 
	end
	if npc_talk_index == "n025_trader_corin-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n025_trader_corin-1-f" then 
	end
	if npc_talk_index == "n025_trader_corin-2-b" then 
	end
	if npc_talk_index == "n025_trader_corin-2-c" then 
	end
	if npc_talk_index == "n025_trader_corin-2-d" then 
	end
	if npc_talk_index == "n025_trader_corin-2-d" then 
	end
	if npc_talk_index == "n025_trader_corin-2-e" then 
	end
	if npc_talk_index == "n025_trader_corin-2-invenchk" then 
				if api_user_HasItem( pRoom, userObjID, 536872356, 1) >= 1 then
									npc_talk_index = "n025_trader_corin-2-f";

				else
									if api_user_GetUserInvenBlankCount( pRoom, userObjID) >= 1 then
									if api_user_CheckInvenForAddItem( pRoom, userObjID, 536872356, 1) == 1 then
					api_user_AddItem( pRoom, userObjID, 536872356, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n025_trader_corin-2-f";

				else
									npc_talk_index = "n025_trader_corin-2-m";

				end

				end
	end
	if npc_talk_index == "n025_trader_corin-2-g" then 
	end
	if npc_talk_index == "n025_trader_corin-2-h" then 
	end
	if npc_talk_index == "n025_trader_corin-2-i" then 
	end
	if npc_talk_index == "n025_trader_corin-2-j" then 
	end
	if npc_talk_index == "n025_trader_corin-2-k" then 
	end
	if npc_talk_index == "n025_trader_corin-2-l" then 
	end
	if npc_talk_index == "n025_trader_corin-2-openupgradeui" then 
				api_ui_OpenUpgradeItem( pRoom, userObjID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n025_trader_corin-3-checkdestroy" then 
				if api_user_IsMissionAchieved( pRoom, userObjID, 3062) == 1  then
									api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n025_trader_corin-4";

				else
									if api_user_IsMissionAchieved( pRoom, userObjID, 3063) == 1  then
									api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n025_trader_corin-4";

				else
									npc_talk_index = "n025_trader_corin-3-a";

				end

				end
	end
	if npc_talk_index == "n025_trader_corin-3-openupgradeui2" then 
				api_ui_OpenUpgradeItem( pRoom, userObjID);
	end
	if npc_talk_index == "n025_trader_corin-4-check4" then 
				if api_user_IsMissionAchieved( pRoom, userObjID, 3062) == 1  then
									npc_talk_index = "n025_trader_corin-4-a";

				else
									npc_talk_index = "n025_trader_corin-4-b";

				end
	end
	if npc_talk_index == "n025_trader_corin-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n025_trader_corin-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n025_trader_corin-5-b" then 
	end
	if npc_talk_index == "n025_trader_corin-5-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3251, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3251, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3252, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3252, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3253, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3253, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3254, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3254, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3255, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3255, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3256, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3256, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3257, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3257, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3258, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3258, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3259, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3259, questID, 1);
			 end 
	end
	if npc_talk_index == "n025_trader_corin-5-chkend" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_user_HasItem( pRoom, userObjID, 536872356, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 536872356, api_user_HasItem( pRoom, userObjID, 536872356, 1, questID));
				end


				else
					npc_talk_index = "_full_inventory";

				end
				if api_user_IsMissionAchieved( pRoom, userObjID, 3062) == 1  then
									npc_talk_index = "n025_trader_corin-5-d";

				else
									npc_talk_index = "n025_trader_corin-5-f";

				end
	end
	if npc_talk_index == "n025_trader_corin-5-a" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n999_remote--------------------------------------------------------------------------------
function sq08_325_legend_of_legendary_weapon_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n999_remote-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_325_legend_of_legendary_weapon_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 325);
end

function sq08_325_legend_of_legendary_weapon_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 325);
end

function sq08_325_legend_of_legendary_weapon_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 325);
	local questID=325;
end

function sq08_325_legend_of_legendary_weapon_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_325_legend_of_legendary_weapon_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_325_legend_of_legendary_weapon_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,325, 1);
				api_quest_SetJournalStep( pRoom, userObjID,325, 1);
				api_quest_SetQuestStep( pRoom, userObjID,325, 1);
				npc_talk_index = "n025_trader_corin-1";
end

</GameServer>