<VillageServer>

function sq11_619_to_the_seadragon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 42 then
		sq11_619_to_the_seadragon_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		sq11_619_to_the_seadragon_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function sq11_619_to_the_seadragon_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n042_general_duglars-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n042_general_duglars-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-4-b" then 
	end
	if npc_talk_index == "n042_general_duglars-4-c" then 
	end
	if npc_talk_index == "n042_general_duglars-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6190, true);
				 api_quest_RewardQuestUser(userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6190, true);
				 api_quest_RewardQuestUser(userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6190, true);
				 api_quest_RewardQuestUser(userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6190, true);
				 api_quest_RewardQuestUser(userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6190, true);
				 api_quest_RewardQuestUser(userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6190, true);
				 api_quest_RewardQuestUser(userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6190, true);
				 api_quest_RewardQuestUser(userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6190, true);
				 api_quest_RewardQuestUser(userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6190, true);
				 api_quest_RewardQuestUser(userObjID, 6190, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-4-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n042_general_duglars-4-class_check" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n042_general_duglars-4-f";

				else
									if api_user_GetUserClassID(userObjID) == 2 then
									npc_talk_index = "n042_general_duglars-4-g";

				else
									if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n042_general_duglars-4-h";

				else
									if api_user_GetUserClassID(userObjID) == 4 then
									npc_talk_index = "n042_general_duglars-4-i";

				else
									if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n042_general_duglars-4-k";

				else
									if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n042_general_duglars-4-j";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n042_general_duglars-4-l";

				else
									npc_talk_index = "n042_general_duglars-4-l";

				end

				end

				end

				end

				end

				end

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_619_to_the_seadragon_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n088_scholar_starshy-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6190, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6190, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6190, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6190, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6190, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6190, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6190, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6190, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6190, false);
			 end 

	end
	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest(userObjID,619, 1);
				api_quest_SetJournalStep(userObjID,619, 1);
				api_quest_SetQuestStep(userObjID,619, 1);
				npc_talk_index = "n088_scholar_starshy-1";

	end
	if npc_talk_index == "n088_scholar_starshy-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-check_j1" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n088_scholar_starshy-3-i";

				else
									npc_talk_index = "n088_scholar_starshy-3-g";

				end
	end
	if npc_talk_index == "n088_scholar_starshy-3-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_619_to_the_seadragon_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 619);
end

function sq11_619_to_the_seadragon_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 619);
end

function sq11_619_to_the_seadragon_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 619);
	local questID=619;
end

function sq11_619_to_the_seadragon_OnRemoteStart( userObjID, questID )
end

function sq11_619_to_the_seadragon_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_619_to_the_seadragon_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,619, 1);
				api_quest_SetJournalStep(userObjID,619, 1);
				api_quest_SetQuestStep(userObjID,619, 1);
				npc_talk_index = "n088_scholar_starshy-1";
end

</VillageServer>

<GameServer>
function sq11_619_to_the_seadragon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 42 then
		sq11_619_to_the_seadragon_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		sq11_619_to_the_seadragon_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function sq11_619_to_the_seadragon_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n042_general_duglars-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n042_general_duglars-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-4-b" then 
	end
	if npc_talk_index == "n042_general_duglars-4-c" then 
	end
	if npc_talk_index == "n042_general_duglars-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6190, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-4-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n042_general_duglars-4-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n042_general_duglars-4-f";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									npc_talk_index = "n042_general_duglars-4-g";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n042_general_duglars-4-h";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									npc_talk_index = "n042_general_duglars-4-i";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n042_general_duglars-4-k";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n042_general_duglars-4-j";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n042_general_duglars-4-l";

				else
									npc_talk_index = "n042_general_duglars-4-l";

				end

				end

				end

				end

				end

				end

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_619_to_the_seadragon_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n088_scholar_starshy-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6190, false);
			 end 

	end
	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,619, 1);
				api_quest_SetJournalStep( pRoom, userObjID,619, 1);
				api_quest_SetQuestStep( pRoom, userObjID,619, 1);
				npc_talk_index = "n088_scholar_starshy-1";

	end
	if npc_talk_index == "n088_scholar_starshy-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-check_j1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n088_scholar_starshy-3-i";

				else
									npc_talk_index = "n088_scholar_starshy-3-g";

				end
	end
	if npc_talk_index == "n088_scholar_starshy-3-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_619_to_the_seadragon_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 619);
end

function sq11_619_to_the_seadragon_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 619);
end

function sq11_619_to_the_seadragon_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 619);
	local questID=619;
end

function sq11_619_to_the_seadragon_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_619_to_the_seadragon_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_619_to_the_seadragon_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,619, 1);
				api_quest_SetJournalStep( pRoom, userObjID,619, 1);
				api_quest_SetQuestStep( pRoom, userObjID,619, 1);
				npc_talk_index = "n088_scholar_starshy-1";
end

</GameServer>