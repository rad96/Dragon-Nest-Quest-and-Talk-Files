<VillageServer>

function mqc15_9431_start_of_journey_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1529 then
		mqc15_9431_start_of_journey_OnTalk_n1529_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1558 then
		mqc15_9431_start_of_journey_OnTalk_n1558_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1529_gaharam--------------------------------------------------------------------------------
function mqc15_9431_start_of_journey_OnTalk_n1529_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1529_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1558_gaharam--------------------------------------------------------------------------------
function mqc15_9431_start_of_journey_OnTalk_n1558_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1558_gaharam-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1558_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1558_gaharam-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9431, 2);
				api_quest_SetJournalStep(userObjID,9431, 1);
				api_quest_SetQuestStep(userObjID,9431, 1);
				npc_talk_index = "n1558_gaharam-1";

	end
	if npc_talk_index == "n1558_gaharam-1-b" then 
	end
	if npc_talk_index == "n1558_gaharam-1-c" then 
	end
	if npc_talk_index == "n1558_gaharam-1-d" then 
	end
	if npc_talk_index == "n1558_gaharam-1-e" then 
	end
	if npc_talk_index == "n1558_gaharam-1-f" then 
	end
	if npc_talk_index == "n1558_gaharam-1-level_check" then 
				if api_user_GetUserLevel(userObjID) >= 75 then
									npc_talk_index = "n1558_gaharam-1-h";

				else
									npc_talk_index = "n1558_gaharam-1-g";

				end
	end
	if npc_talk_index == "n1558_gaharam-1-i" then 
	end
	if npc_talk_index == "n1558_gaharam-1-j" then 
	end
	if npc_talk_index == "n1558_gaharam-1-k" then 
	end
	if npc_talk_index == "n1558_gaharam-1-l" then 
	end
	if npc_talk_index == "n1558_gaharam-1-m" then 
	end
	if npc_talk_index == "n1558_gaharam-1-class_check" then 
				if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n1558_gaharam-1-p";

				else
									npc_talk_index = "n1558_gaharam-1-n";

				end
	end
	if npc_talk_index == "n1558_gaharam-1-o" then 
	end
	if npc_talk_index == "n1558_gaharam-1-v" then 
	end
	if npc_talk_index == "n1558_gaharam-1-q" then 
	end
	if npc_talk_index == "n1558_gaharam-1-r" then 
	end
	if npc_talk_index == "n1558_gaharam-1-s" then 
	end
	if npc_talk_index == "n1558_gaharam-1-t" then 
	end
	if npc_talk_index == "n1558_gaharam-1-u" then 
	end
	if npc_talk_index == "n1558_gaharam-1-v" then 
	end
	if npc_talk_index == "n1558_gaharam-1-w" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9432, 2);
					api_quest_SetQuestStep(userObjID, 9432, 1);
					api_quest_SetJournalStep(userObjID, 9432, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1558_gaharam-1-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1558_gaharam-1", "mqc15_9432_nervousness.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9431_start_of_journey_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9431);
end

function mqc15_9431_start_of_journey_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9431);
end

function mqc15_9431_start_of_journey_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9431);
	local questID=9431;
end

function mqc15_9431_start_of_journey_OnRemoteStart( userObjID, questID )
end

function mqc15_9431_start_of_journey_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9431_start_of_journey_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9431, 2);
				api_quest_SetJournalStep(userObjID,9431, 1);
				api_quest_SetQuestStep(userObjID,9431, 1);
				npc_talk_index = "n1558_gaharam-1";
end

</VillageServer>

<GameServer>
function mqc15_9431_start_of_journey_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1529 then
		mqc15_9431_start_of_journey_OnTalk_n1529_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1558 then
		mqc15_9431_start_of_journey_OnTalk_n1558_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1529_gaharam--------------------------------------------------------------------------------
function mqc15_9431_start_of_journey_OnTalk_n1529_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1529_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1558_gaharam--------------------------------------------------------------------------------
function mqc15_9431_start_of_journey_OnTalk_n1558_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1558_gaharam-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1558_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1558_gaharam-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9431, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9431, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9431, 1);
				npc_talk_index = "n1558_gaharam-1";

	end
	if npc_talk_index == "n1558_gaharam-1-b" then 
	end
	if npc_talk_index == "n1558_gaharam-1-c" then 
	end
	if npc_talk_index == "n1558_gaharam-1-d" then 
	end
	if npc_talk_index == "n1558_gaharam-1-e" then 
	end
	if npc_talk_index == "n1558_gaharam-1-f" then 
	end
	if npc_talk_index == "n1558_gaharam-1-level_check" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 75 then
									npc_talk_index = "n1558_gaharam-1-h";

				else
									npc_talk_index = "n1558_gaharam-1-g";

				end
	end
	if npc_talk_index == "n1558_gaharam-1-i" then 
	end
	if npc_talk_index == "n1558_gaharam-1-j" then 
	end
	if npc_talk_index == "n1558_gaharam-1-k" then 
	end
	if npc_talk_index == "n1558_gaharam-1-l" then 
	end
	if npc_talk_index == "n1558_gaharam-1-m" then 
	end
	if npc_talk_index == "n1558_gaharam-1-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n1558_gaharam-1-p";

				else
									npc_talk_index = "n1558_gaharam-1-n";

				end
	end
	if npc_talk_index == "n1558_gaharam-1-o" then 
	end
	if npc_talk_index == "n1558_gaharam-1-v" then 
	end
	if npc_talk_index == "n1558_gaharam-1-q" then 
	end
	if npc_talk_index == "n1558_gaharam-1-r" then 
	end
	if npc_talk_index == "n1558_gaharam-1-s" then 
	end
	if npc_talk_index == "n1558_gaharam-1-t" then 
	end
	if npc_talk_index == "n1558_gaharam-1-u" then 
	end
	if npc_talk_index == "n1558_gaharam-1-v" then 
	end
	if npc_talk_index == "n1558_gaharam-1-w" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9432, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9432, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9432, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1558_gaharam-1-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1558_gaharam-1", "mqc15_9432_nervousness.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9431_start_of_journey_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9431);
end

function mqc15_9431_start_of_journey_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9431);
end

function mqc15_9431_start_of_journey_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9431);
	local questID=9431;
end

function mqc15_9431_start_of_journey_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9431_start_of_journey_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9431_start_of_journey_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9431, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9431, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9431, 1);
				npc_talk_index = "n1558_gaharam-1";
end

</GameServer>