<VillageServer>

function mqc15_9458_the_story_of_prophecy_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1065 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1138 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1244 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1244_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1505 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1505_elf_king(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1549 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1549_cleric_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1551 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1551_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1065_geraint_kid-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1065_geraint_kid-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-6-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-f" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1067_elder_elf-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1067_elder_elf-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9458, 2);
				api_quest_SetJournalStep(userObjID,9458, 1);
				api_quest_SetQuestStep(userObjID,9458, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-l" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-m" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n1067_elder_elf-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1067_elder_elf-8-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-k" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-l" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-m" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-n" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94580, true);
				 api_quest_RewardQuestUser(userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94580, true);
				 api_quest_RewardQuestUser(userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94580, true);
				 api_quest_RewardQuestUser(userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94580, true);
				 api_quest_RewardQuestUser(userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94580, true);
				 api_quest_RewardQuestUser(userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94580, true);
				 api_quest_RewardQuestUser(userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94580, true);
				 api_quest_RewardQuestUser(userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94580, true);
				 api_quest_RewardQuestUser(userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94580, true);
				 api_quest_RewardQuestUser(userObjID, 94580, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-8-o" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1138_elf_guard_sitredel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1138_elf_guard_sitredel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1138_elf_guard_sitredel-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1138_elf_guard_sitredel-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-3-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-e" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-f" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-g" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-e" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-f" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-g" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-h" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-i" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-j" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-k" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-l" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-m" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-n" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-o" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-p" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-q" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-r" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-s" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-t" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-u" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-v" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-w" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1244_for_memory--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1244_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1244_for_memory-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1244_for_memory-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1244_for_memory-7-b" then 
	end
	if npc_talk_index == "n1244_for_memory-7-c" then 
	end
	if npc_talk_index == "n1244_for_memory-7-d" then 
	end
	if npc_talk_index == "n1244_for_memory-7-e" then 
	end
	if npc_talk_index == "n1244_for_memory-7-f" then 
	end
	if npc_talk_index == "n1244_for_memory-7-g" then 
	end
	if npc_talk_index == "n1244_for_memory-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1505_elf_king--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1505_elf_king(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1505_elf_king-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1549_cleric_teramai--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1549_cleric_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1549_cleric_teramai-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1551_elder_elf--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1551_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1551_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9458_the_story_of_prophecy_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9458);
end

function mqc15_9458_the_story_of_prophecy_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9458);
end

function mqc15_9458_the_story_of_prophecy_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9458);
	local questID=9458;
end

function mqc15_9458_the_story_of_prophecy_OnRemoteStart( userObjID, questID )
end

function mqc15_9458_the_story_of_prophecy_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9458_the_story_of_prophecy_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9458, 2);
				api_quest_SetJournalStep(userObjID,9458, 1);
				api_quest_SetQuestStep(userObjID,9458, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</VillageServer>

<GameServer>
function mqc15_9458_the_story_of_prophecy_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1065 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1138 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1244 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1244_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1505 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1505_elf_king( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1549 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1549_cleric_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1551 then
		mqc15_9458_the_story_of_prophecy_OnTalk_n1551_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1065_geraint_kid-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1065_geraint_kid-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-6-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-f" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1067_elder_elf-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1067_elder_elf-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9458, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9458, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9458, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-l" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-m" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n1067_elder_elf-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1067_elder_elf-8-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-k" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-l" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-m" then 
	end
	if npc_talk_index == "n1067_elder_elf-8-n" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94580, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-8-o" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1138_elf_guard_sitredel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1138_elf_guard_sitredel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1138_elf_guard_sitredel-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1138_elf_guard_sitredel-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-3-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-e" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-f" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-g" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-e" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-f" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-g" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-h" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-i" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-j" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-k" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-l" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-m" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-n" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-o" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-p" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-q" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-r" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-s" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-t" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-u" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-v" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-5-w" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1244_for_memory--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1244_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1244_for_memory-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1244_for_memory-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1244_for_memory-7-b" then 
	end
	if npc_talk_index == "n1244_for_memory-7-c" then 
	end
	if npc_talk_index == "n1244_for_memory-7-d" then 
	end
	if npc_talk_index == "n1244_for_memory-7-e" then 
	end
	if npc_talk_index == "n1244_for_memory-7-f" then 
	end
	if npc_talk_index == "n1244_for_memory-7-g" then 
	end
	if npc_talk_index == "n1244_for_memory-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1505_elf_king--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1505_elf_king( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1505_elf_king-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1549_cleric_teramai--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1549_cleric_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1549_cleric_teramai-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1551_elder_elf--------------------------------------------------------------------------------
function mqc15_9458_the_story_of_prophecy_OnTalk_n1551_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1551_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9458_the_story_of_prophecy_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9458);
end

function mqc15_9458_the_story_of_prophecy_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9458);
end

function mqc15_9458_the_story_of_prophecy_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9458);
	local questID=9458;
end

function mqc15_9458_the_story_of_prophecy_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9458_the_story_of_prophecy_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9458_the_story_of_prophecy_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9458, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9458, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9458, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</GameServer>