<VillageServer>

function sq15_758_ordeal_and_answer_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 456 then
		sq15_758_ordeal_and_answer_OnTalk_n456_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 457 then
		sq15_758_ordeal_and_answer_OnTalk_n457_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 458 then
		sq15_758_ordeal_and_answer_OnTalk_n458_sword_of_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 711 then
		sq15_758_ordeal_and_answer_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n456_velskud--------------------------------------------------------------------------------
function sq15_758_ordeal_and_answer_OnTalk_n456_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n456_velskud-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n456_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n456_velskud-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n456_velskud-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n456_velskud-5-b" then 
	end
	if npc_talk_index == "n456_velskud-5-c" then 
	end
	if npc_talk_index == "n456_velskud-5-d" then 
	end
	if npc_talk_index == "n456_velskud-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n457_geraint--------------------------------------------------------------------------------
function sq15_758_ordeal_and_answer_OnTalk_n457_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n457_geraint-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n457_geraint-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n457_geraint-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n457_geraint-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n457_geraint-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n457_geraint-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n457_geraint-2-b" then 
	end
	if npc_talk_index == "n457_geraint-2-c" then 
	end
	if npc_talk_index == "n457_geraint-2-open_ui2" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n457_geraint-2-e" then 
	end
	if npc_talk_index == "n457_geraint-2-f" then 
	end
	if npc_talk_index == "n457_geraint-2-g" then 
	end
	if npc_talk_index == "n457_geraint-2-gla1" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n457_geraint-3";
				api_user_SetSecondJobSkill( userObjID, 23 );
	end
	if npc_talk_index == "n457_geraint-2-moon1" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n457_geraint-3";
				api_user_SetSecondJobSkill( userObjID, 24 );
	end
	if npc_talk_index == "n457_geraint-2-i" then 
	end
	if npc_talk_index == "n457_geraint-2-j" then 
	end
	if npc_talk_index == "n457_geraint-2-g" then 
	end
	if npc_talk_index == "n457_geraint-2-baba1" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n457_geraint-3";
				api_user_SetSecondJobSkill( userObjID, 25 );
	end
	if npc_talk_index == "n457_geraint-2-des1" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n457_geraint-3";
				api_user_SetSecondJobSkill( userObjID, 26 );
	end
	if npc_talk_index == "n457_geraint-4-open_ui4" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n457_geraint-4-gla2" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n457_geraint-4-f";
				api_user_SetSecondJobSkill( userObjID, 23 );
	end
	if npc_talk_index == "n457_geraint-4-moon2" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n457_geraint-4-f";
				api_user_SetSecondJobSkill( userObjID, 24 );
	end
	if npc_talk_index == "n457_geraint-4-barbarian2" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n457_geraint-4-f";
				api_user_SetSecondJobSkill( userObjID, 25 );
	end
	if npc_talk_index == "n457_geraint-4-destroyer2" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n457_geraint-4-f";
				api_user_SetSecondJobSkill( userObjID, 26 );
	end
	if npc_talk_index == "n457_geraint-4-e" then 
	end
	if npc_talk_index == "n457_geraint-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n457_geraint-5-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n458_sword_of_geraint--------------------------------------------------------------------------------
function sq15_758_ordeal_and_answer_OnTalk_n458_sword_of_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n458_sword_of_geraint-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n458_sword_of_geraint-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n458_sword_of_geraint-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n458_sword_of_geraint-1-level" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									if api_user_GetUserJobID(userObjID) == 11   or api_user_GetUserJobID(userObjID) == 12  then
									if api_user_GetUserLevel(userObjID) >= 45 then
									npc_talk_index = "n458_sword_of_geraint-1-g";

				else
									npc_talk_index = "n458_sword_of_geraint-1-h";

				end

				else
									npc_talk_index = "n458_sword_of_geraint-1-h";

				end

				else
									npc_talk_index = "n458_sword_of_geraint-1-f";

				end
	end
	if npc_talk_index == "n458_sword_of_geraint-1-b" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-c" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-d" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-e" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-move1" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13512,1);
	end
	if npc_talk_index == "n458_sword_of_geraint-1-a" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-2-move2" then 
				api_user_ChangeMap(userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n711_warrior_fedro--------------------------------------------------------------------------------
function sq15_758_ordeal_and_answer_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n711_warrior_fedro-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n711_warrior_fedro-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-accepting-a" then
				if api_user_GetUserJobID(userObjID) == 23   or api_user_GetUserJobID(userObjID) == 24   or api_user_GetUserJobID(userObjID) == 25   or api_user_GetUserJobID(userObjID) == 26  then
									npc_talk_index = "n711_warrior_fedro-accepting-b";

				else
									npc_talk_index = "n711_warrior_fedro-accepting-a";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 end 

				end

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted" then
				api_quest_AddQuest(userObjID,758, 1);
				api_quest_SetJournalStep(userObjID,758, 1);
				api_quest_SetQuestStep(userObjID,758, 1);
				npc_talk_index = "n711_warrior_fedro-1";

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7580, false);
			 end 

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted_com" then
				api_quest_AddQuest(userObjID,758, 1);
				api_quest_SetJournalStep(userObjID,758, 1);
				api_quest_SetQuestStep(userObjID,758, 1);
				api_quest_SetJournalStep(userObjID, questID, 6);
				api_quest_SetQuestStep(userObjID, questID,6);
				npc_talk_index = "n711_warrior_fedro-6";

	end
	if npc_talk_index == "n711_warrior_fedro-6-b" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-o" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-6-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-f" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-g" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-h" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-i" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-6-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-6-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-l" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-6-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-m" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7580, true);
				 api_quest_RewardQuestUser(userObjID, 7580, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-6-chj" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 23);
				npc_talk_index = "n711_warrior_fedro-6-n";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n711_warrior_fedro-6-chk" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 24);
				npc_talk_index = "n711_warrior_fedro-6-n";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n711_warrior_fedro-6-chl" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 25);
				npc_talk_index = "n711_warrior_fedro-6-n";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n711_warrior_fedro-6-chm" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 26);
				npc_talk_index = "n711_warrior_fedro-6-n";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n711_warrior_fedro-6-p" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_758_ordeal_and_answer_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 758);
end

function sq15_758_ordeal_and_answer_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 758);
end

function sq15_758_ordeal_and_answer_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 758);
	local questID=758;
end

function sq15_758_ordeal_and_answer_OnRemoteStart( userObjID, questID )
end

function sq15_758_ordeal_and_answer_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_758_ordeal_and_answer_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,758, 1);
				api_quest_SetJournalStep(userObjID,758, 1);
				api_quest_SetQuestStep(userObjID,758, 1);
				npc_talk_index = "n711_warrior_fedro-1";
end

</VillageServer>

<GameServer>
function sq15_758_ordeal_and_answer_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 456 then
		sq15_758_ordeal_and_answer_OnTalk_n456_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 457 then
		sq15_758_ordeal_and_answer_OnTalk_n457_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 458 then
		sq15_758_ordeal_and_answer_OnTalk_n458_sword_of_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 711 then
		sq15_758_ordeal_and_answer_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n456_velskud--------------------------------------------------------------------------------
function sq15_758_ordeal_and_answer_OnTalk_n456_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n456_velskud-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n456_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n456_velskud-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n456_velskud-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n456_velskud-5-b" then 
	end
	if npc_talk_index == "n456_velskud-5-c" then 
	end
	if npc_talk_index == "n456_velskud-5-d" then 
	end
	if npc_talk_index == "n456_velskud-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n457_geraint--------------------------------------------------------------------------------
function sq15_758_ordeal_and_answer_OnTalk_n457_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n457_geraint-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n457_geraint-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n457_geraint-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n457_geraint-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n457_geraint-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n457_geraint-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n457_geraint-2-b" then 
	end
	if npc_talk_index == "n457_geraint-2-c" then 
	end
	if npc_talk_index == "n457_geraint-2-open_ui2" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n457_geraint-2-e" then 
	end
	if npc_talk_index == "n457_geraint-2-f" then 
	end
	if npc_talk_index == "n457_geraint-2-g" then 
	end
	if npc_talk_index == "n457_geraint-2-gla1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n457_geraint-3";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 23 );
	end
	if npc_talk_index == "n457_geraint-2-moon1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n457_geraint-3";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 24 );
	end
	if npc_talk_index == "n457_geraint-2-i" then 
	end
	if npc_talk_index == "n457_geraint-2-j" then 
	end
	if npc_talk_index == "n457_geraint-2-g" then 
	end
	if npc_talk_index == "n457_geraint-2-baba1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n457_geraint-3";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 25 );
	end
	if npc_talk_index == "n457_geraint-2-des1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n457_geraint-3";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 26 );
	end
	if npc_talk_index == "n457_geraint-4-open_ui4" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n457_geraint-4-gla2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n457_geraint-4-f";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 23 );
	end
	if npc_talk_index == "n457_geraint-4-moon2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n457_geraint-4-f";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 24 );
	end
	if npc_talk_index == "n457_geraint-4-barbarian2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n457_geraint-4-f";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 25 );
	end
	if npc_talk_index == "n457_geraint-4-destroyer2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n457_geraint-4-f";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 26 );
	end
	if npc_talk_index == "n457_geraint-4-e" then 
	end
	if npc_talk_index == "n457_geraint-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n457_geraint-5-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n458_sword_of_geraint--------------------------------------------------------------------------------
function sq15_758_ordeal_and_answer_OnTalk_n458_sword_of_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n458_sword_of_geraint-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n458_sword_of_geraint-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n458_sword_of_geraint-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n458_sword_of_geraint-1-level" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									if api_user_GetUserJobID( pRoom, userObjID) == 11   or api_user_GetUserJobID( pRoom, userObjID) == 12  then
									if api_user_GetUserLevel( pRoom, userObjID) >= 45 then
									npc_talk_index = "n458_sword_of_geraint-1-g";

				else
									npc_talk_index = "n458_sword_of_geraint-1-h";

				end

				else
									npc_talk_index = "n458_sword_of_geraint-1-h";

				end

				else
									npc_talk_index = "n458_sword_of_geraint-1-f";

				end
	end
	if npc_talk_index == "n458_sword_of_geraint-1-b" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-c" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-d" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-e" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-move1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end
	if npc_talk_index == "n458_sword_of_geraint-1-a" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-2-move2" then 
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n711_warrior_fedro--------------------------------------------------------------------------------
function sq15_758_ordeal_and_answer_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n711_warrior_fedro-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n711_warrior_fedro-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-accepting-a" then
				if api_user_GetUserJobID( pRoom, userObjID) == 23   or api_user_GetUserJobID( pRoom, userObjID) == 24   or api_user_GetUserJobID( pRoom, userObjID) == 25   or api_user_GetUserJobID( pRoom, userObjID) == 26  then
									npc_talk_index = "n711_warrior_fedro-accepting-b";

				else
									npc_talk_index = "n711_warrior_fedro-accepting-a";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 end 

				end

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,758, 1);
				api_quest_SetJournalStep( pRoom, userObjID,758, 1);
				api_quest_SetQuestStep( pRoom, userObjID,758, 1);
				npc_talk_index = "n711_warrior_fedro-1";

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, false);
			 end 

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted_com" then
				api_quest_AddQuest( pRoom, userObjID,758, 1);
				api_quest_SetJournalStep( pRoom, userObjID,758, 1);
				api_quest_SetQuestStep( pRoom, userObjID,758, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				npc_talk_index = "n711_warrior_fedro-6";

	end
	if npc_talk_index == "n711_warrior_fedro-6-b" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-o" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-6-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-f" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-g" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-h" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-i" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-6-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-6-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-l" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-6-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-6-m" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7580, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7580, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-6-chj" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 23);
				npc_talk_index = "n711_warrior_fedro-6-n";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n711_warrior_fedro-6-chk" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 24);
				npc_talk_index = "n711_warrior_fedro-6-n";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n711_warrior_fedro-6-chl" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 25);
				npc_talk_index = "n711_warrior_fedro-6-n";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n711_warrior_fedro-6-chm" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 26);
				npc_talk_index = "n711_warrior_fedro-6-n";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n711_warrior_fedro-6-p" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_758_ordeal_and_answer_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 758);
end

function sq15_758_ordeal_and_answer_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 758);
end

function sq15_758_ordeal_and_answer_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 758);
	local questID=758;
end

function sq15_758_ordeal_and_answer_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_758_ordeal_and_answer_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_758_ordeal_and_answer_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,758, 1);
				api_quest_SetJournalStep( pRoom, userObjID,758, 1);
				api_quest_SetQuestStep( pRoom, userObjID,758, 1);
				npc_talk_index = "n711_warrior_fedro-1";
end

</GameServer>