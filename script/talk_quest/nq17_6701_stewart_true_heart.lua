<VillageServer>

function nq17_6701_stewart_true_heart_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 41 then
		nq17_6701_stewart_true_heart_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n041_duke_stwart--------------------------------------------------------------------------------
function nq17_6701_stewart_true_heart_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n041_duke_stwart-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n041_duke_stwart-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-accepting-acceptted" then
				api_quest_AddQuest(userObjID,6701, 1);
				api_quest_SetJournalStep(userObjID,6701, 1);
				api_quest_SetQuestStep(userObjID,6701, 1);
				npc_talk_index = "n041_duke_stwart-1";

	end
	if npc_talk_index == "n041_duke_stwart-1-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-1-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-1-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-1-e" then 
				api_npc_Rage( userObjID, 41 );
	end
	if npc_talk_index == "n041_duke_stwart-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n041_duke_stwart-2-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-e" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-f" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-g" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-h" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-i" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-j" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-k" then 
	end
	if npc_talk_index == "n041_duke_stwart-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n041_duke_stwart-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 41, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq17_6701_stewart_true_heart_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6701);
end

function nq17_6701_stewart_true_heart_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6701);
end

function nq17_6701_stewart_true_heart_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6701);
	local questID=6701;
end

function nq17_6701_stewart_true_heart_OnRemoteStart( userObjID, questID )
end

function nq17_6701_stewart_true_heart_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq17_6701_stewart_true_heart_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,6701, 1);
				api_quest_SetJournalStep(userObjID,6701, 1);
				api_quest_SetQuestStep(userObjID,6701, 1);
				npc_talk_index = "n041_duke_stwart-1";
end

</VillageServer>

<GameServer>
function nq17_6701_stewart_true_heart_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 41 then
		nq17_6701_stewart_true_heart_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n041_duke_stwart--------------------------------------------------------------------------------
function nq17_6701_stewart_true_heart_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n041_duke_stwart-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n041_duke_stwart-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,6701, 1);
				api_quest_SetJournalStep( pRoom, userObjID,6701, 1);
				api_quest_SetQuestStep( pRoom, userObjID,6701, 1);
				npc_talk_index = "n041_duke_stwart-1";

	end
	if npc_talk_index == "n041_duke_stwart-1-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-1-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-1-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-1-e" then 
				api_npc_Rage( pRoom,  userObjID, 41 );
	end
	if npc_talk_index == "n041_duke_stwart-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n041_duke_stwart-2-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-e" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-f" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-g" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-h" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-i" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-j" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-k" then 
	end
	if npc_talk_index == "n041_duke_stwart-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n041_duke_stwart-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 41, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq17_6701_stewart_true_heart_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6701);
end

function nq17_6701_stewart_true_heart_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6701);
end

function nq17_6701_stewart_true_heart_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6701);
	local questID=6701;
end

function nq17_6701_stewart_true_heart_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq17_6701_stewart_true_heart_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq17_6701_stewart_true_heart_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,6701, 1);
				api_quest_SetJournalStep( pRoom, userObjID,6701, 1);
				api_quest_SetQuestStep( pRoom, userObjID,6701, 1);
				npc_talk_index = "n041_duke_stwart-1";
end

</GameServer>