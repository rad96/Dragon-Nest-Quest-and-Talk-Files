<VillageServer>

function sq_2298_level_up_7_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 100 then
		sq_2298_level_up_7_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq_2298_level_up_7_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 22981, true);
				 api_quest_RewardQuestUser(userObjID, 22981, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 22982, true);
				 api_quest_RewardQuestUser(userObjID, 22982, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 22983, true);
				 api_quest_RewardQuestUser(userObjID, 22983, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 22984, true);
				 api_quest_RewardQuestUser(userObjID, 22984, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 22985, true);
				 api_quest_RewardQuestUser(userObjID, 22985, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 22986, true);
				 api_quest_RewardQuestUser(userObjID, 22986, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 22987, true);
				 api_quest_RewardQuestUser(userObjID, 22987, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 22988, true);
				 api_quest_RewardQuestUser(userObjID, 22988, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 22980, true);
				 api_quest_RewardQuestUser(userObjID, 22980, questID, 1);
			 end 
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2298, 1);
				api_quest_SetJournalStep(userObjID,2298, 1);
				api_quest_SetQuestStep(userObjID,2298, 1);
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 22981, true);
				 api_quest_RewardQuestUser(userObjID, 22981, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 22982, true);
				 api_quest_RewardQuestUser(userObjID, 22982, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 22983, true);
				 api_quest_RewardQuestUser(userObjID, 22983, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 22984, true);
				 api_quest_RewardQuestUser(userObjID, 22984, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 22985, true);
				 api_quest_RewardQuestUser(userObjID, 22985, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 22986, true);
				 api_quest_RewardQuestUser(userObjID, 22986, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 22987, true);
				 api_quest_RewardQuestUser(userObjID, 22987, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 22988, true);
				 api_quest_RewardQuestUser(userObjID, 22988, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 22980, true);
				 api_quest_RewardQuestUser(userObjID, 22980, questID, 1);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-1-a" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_user_HasItem(userObjID, 805308883, 1) > 0 then 
					api_user_DelItem(userObjID, 805308883, api_user_HasItem(userObjID, 805308883, 1, questID));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2298_level_up_7_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2298);
end

function sq_2298_level_up_7_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2298);
end

function sq_2298_level_up_7_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2298);
	local questID=2298;
end

function sq_2298_level_up_7_OnRemoteStart( userObjID, questID )
end

function sq_2298_level_up_7_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_2298_level_up_7_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2298, 1);
				api_quest_SetJournalStep(userObjID,2298, 1);
				api_quest_SetQuestStep(userObjID,2298, 1);
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 22981, true);
				 api_quest_RewardQuestUser(userObjID, 22981, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 22982, true);
				 api_quest_RewardQuestUser(userObjID, 22982, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 22983, true);
				 api_quest_RewardQuestUser(userObjID, 22983, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 22984, true);
				 api_quest_RewardQuestUser(userObjID, 22984, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 22985, true);
				 api_quest_RewardQuestUser(userObjID, 22985, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 22986, true);
				 api_quest_RewardQuestUser(userObjID, 22986, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 22987, true);
				 api_quest_RewardQuestUser(userObjID, 22987, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 22988, true);
				 api_quest_RewardQuestUser(userObjID, 22988, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 22980, true);
				 api_quest_RewardQuestUser(userObjID, 22980, questID, 1);
			 end 
end

</VillageServer>

<GameServer>
function sq_2298_level_up_7_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 100 then
		sq_2298_level_up_7_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq_2298_level_up_7_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22981, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22981, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22982, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22982, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22983, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22983, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22984, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22984, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22985, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22985, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22986, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22986, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22987, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22987, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22988, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22988, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22980, questID, 1);
			 end 
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2298, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2298, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2298, 1);
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22981, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22981, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22982, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22982, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22983, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22983, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22984, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22984, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22985, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22985, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22986, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22986, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22987, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22987, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22988, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22988, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22980, questID, 1);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-1-a" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_user_HasItem( pRoom, userObjID, 805308883, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 805308883, api_user_HasItem( pRoom, userObjID, 805308883, 1, questID));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2298_level_up_7_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2298);
end

function sq_2298_level_up_7_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2298);
end

function sq_2298_level_up_7_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2298);
	local questID=2298;
end

function sq_2298_level_up_7_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_2298_level_up_7_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_2298_level_up_7_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2298, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2298, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2298, 1);
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22981, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22981, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22982, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22982, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22983, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22983, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22984, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22984, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22985, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22985, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22986, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22986, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22987, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22987, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22988, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22988, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22980, questID, 1);
			 end 
end

</GameServer>