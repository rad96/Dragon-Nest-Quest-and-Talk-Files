<VillageServer>

function sq34_4431_recipe_of_love_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1138 then
		sq34_4431_recipe_of_love_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1247 then
		sq34_4431_recipe_of_love_OnTalk_n1247_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1103 then
		sq34_4431_recipe_of_love_OnTalk_n1103_trader_phara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		sq34_4431_recipe_of_love_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function sq34_4431_recipe_of_love_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1138_elf_guard_sitredel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1138_elf_guard_sitredel-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1138_elf_guard_sitredel-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 44310, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 44310, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 44310, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 44310, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 44310, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 44310, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 44310, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 44310, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 44310, false);
			 end 

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4431, 1);
				api_quest_SetJournalStep(userObjID,4431, 1);
				api_quest_SetQuestStep(userObjID,4431, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1247_for_memory--------------------------------------------------------------------------------
function sq34_4431_recipe_of_love_OnTalk_n1247_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1247_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1247_for_memory-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1247_for_memory-1-b" then 
	end
	if npc_talk_index == "n1247_for_memory-1-c" then 
	end
	if npc_talk_index == "n1247_for_memory-1-c" then 
	end
	if npc_talk_index == "n1247_for_memory-1-d" then 
	end
	if npc_talk_index == "n1247_for_memory-1-d" then 
	end
	if npc_talk_index == "n1247_for_memory-1-e" then 
	end
	if npc_talk_index == "n1247_for_memory-1-e" then 
	end
	if npc_talk_index == "n1247_for_memory-1-f" then 
	end
	if npc_talk_index == "n1247_for_memory-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1103_trader_phara--------------------------------------------------------------------------------
function sq34_4431_recipe_of_love_OnTalk_n1103_trader_phara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1103_trader_phara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1103_trader_phara-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1103_trader_phara-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1103_trader_phara-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1103_trader_phara-2-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-2-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-2-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-2-e" then 
	end
	if npc_talk_index == "n1103_trader_phara-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1103_trader_phara-3-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-e" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-f" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-g" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-h" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-i" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-j" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-k" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-l" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-m" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-n" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-o" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-p" then 
	end
	if npc_talk_index == "n1103_trader_phara-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1103_trader_phara-4-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function sq34_4431_recipe_of_love_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1067_elder_elf-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-6-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-6-chk_class" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1067_elder_elf-6-c";

				else
									npc_talk_index = "n1067_elder_elf-6-d";

				end
	end
	if npc_talk_index == "n1067_elder_elf-6-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-6-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-6-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-6-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-6-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 44310, true);
				 api_quest_RewardQuestUser(userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 44310, true);
				 api_quest_RewardQuestUser(userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 44310, true);
				 api_quest_RewardQuestUser(userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 44310, true);
				 api_quest_RewardQuestUser(userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 44310, true);
				 api_quest_RewardQuestUser(userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 44310, true);
				 api_quest_RewardQuestUser(userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 44310, true);
				 api_quest_RewardQuestUser(userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 44310, true);
				 api_quest_RewardQuestUser(userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 44310, true);
				 api_quest_RewardQuestUser(userObjID, 44310, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-6-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4431_recipe_of_love_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4431);
end

function sq34_4431_recipe_of_love_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4431);
end

function sq34_4431_recipe_of_love_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4431);
	local questID=4431;
end

function sq34_4431_recipe_of_love_OnRemoteStart( userObjID, questID )
end

function sq34_4431_recipe_of_love_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq34_4431_recipe_of_love_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4431, 1);
				api_quest_SetJournalStep(userObjID,4431, 1);
				api_quest_SetQuestStep(userObjID,4431, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";
end

</VillageServer>

<GameServer>
function sq34_4431_recipe_of_love_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1138 then
		sq34_4431_recipe_of_love_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1247 then
		sq34_4431_recipe_of_love_OnTalk_n1247_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1103 then
		sq34_4431_recipe_of_love_OnTalk_n1103_trader_phara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		sq34_4431_recipe_of_love_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function sq34_4431_recipe_of_love_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1138_elf_guard_sitredel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1138_elf_guard_sitredel-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1138_elf_guard_sitredel-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, false);
			 end 

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4431, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4431, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4431, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1247_for_memory--------------------------------------------------------------------------------
function sq34_4431_recipe_of_love_OnTalk_n1247_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1247_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1247_for_memory-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1247_for_memory-1-b" then 
	end
	if npc_talk_index == "n1247_for_memory-1-c" then 
	end
	if npc_talk_index == "n1247_for_memory-1-c" then 
	end
	if npc_talk_index == "n1247_for_memory-1-d" then 
	end
	if npc_talk_index == "n1247_for_memory-1-d" then 
	end
	if npc_talk_index == "n1247_for_memory-1-e" then 
	end
	if npc_talk_index == "n1247_for_memory-1-e" then 
	end
	if npc_talk_index == "n1247_for_memory-1-f" then 
	end
	if npc_talk_index == "n1247_for_memory-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1103_trader_phara--------------------------------------------------------------------------------
function sq34_4431_recipe_of_love_OnTalk_n1103_trader_phara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1103_trader_phara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1103_trader_phara-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1103_trader_phara-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1103_trader_phara-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1103_trader_phara-2-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-2-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-2-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-2-e" then 
	end
	if npc_talk_index == "n1103_trader_phara-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1103_trader_phara-3-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-e" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-f" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-g" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-h" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-i" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-j" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-k" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-l" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-m" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-n" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-o" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-p" then 
	end
	if npc_talk_index == "n1103_trader_phara-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1103_trader_phara-4-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function sq34_4431_recipe_of_love_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1067_elder_elf-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-6-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-6-chk_class" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1067_elder_elf-6-c";

				else
									npc_talk_index = "n1067_elder_elf-6-d";

				end
	end
	if npc_talk_index == "n1067_elder_elf-6-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-6-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-6-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-6-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-6-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44310, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-6-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4431_recipe_of_love_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4431);
end

function sq34_4431_recipe_of_love_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4431);
end

function sq34_4431_recipe_of_love_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4431);
	local questID=4431;
end

function sq34_4431_recipe_of_love_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq34_4431_recipe_of_love_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq34_4431_recipe_of_love_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4431, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4431, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4431, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";
end

</GameServer>