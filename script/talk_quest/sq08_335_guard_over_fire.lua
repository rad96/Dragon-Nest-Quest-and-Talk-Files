<VillageServer>

function sq08_335_guard_over_fire_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 34 then
		sq08_335_guard_over_fire_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n034_cleric_master_germain--------------------------------------------------------------------------------
function sq08_335_guard_over_fire_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n034_cleric_master_germain-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3351, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3352, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3353, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3354, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3355, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3356, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3357, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3358, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3359, false);
			 end 

	end
	if npc_talk_index == "n034_cleric_master_germain-accepting-acceptted" then
				api_quest_AddQuest(userObjID,335, 1);
				api_quest_SetJournalStep(userObjID,335, 1);
				api_quest_SetQuestStep(userObjID,335, 1);
				npc_talk_index = "n034_cleric_master_germain-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 468, 2);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200468, 30000);

	end
	if npc_talk_index == "n034_cleric_master_germain-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3351, true);
				 api_quest_RewardQuestUser(userObjID, 3351, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3352, true);
				 api_quest_RewardQuestUser(userObjID, 3352, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3353, true);
				 api_quest_RewardQuestUser(userObjID, 3353, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3354, true);
				 api_quest_RewardQuestUser(userObjID, 3354, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3355, true);
				 api_quest_RewardQuestUser(userObjID, 3355, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3356, true);
				 api_quest_RewardQuestUser(userObjID, 3356, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3357, true);
				 api_quest_RewardQuestUser(userObjID, 3357, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3358, true);
				 api_quest_RewardQuestUser(userObjID, 3358, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3359, true);
				 api_quest_RewardQuestUser(userObjID, 3359, questID, 1);
			 end 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_335_guard_over_fire_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 335);
	if qstep == 1 and CountIndex == 468 then

	end
	if qstep == 1 and CountIndex == 200468 then
				api_quest_IncCounting(userObjID, 2, 468);

	end
end

function sq08_335_guard_over_fire_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 335);
	if qstep == 1 and CountIndex == 468 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200468 and Count >= TargetCount  then

	end
end

function sq08_335_guard_over_fire_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 335);
	local questID=335;
end

function sq08_335_guard_over_fire_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 468, 2);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200468, 30000);
end

function sq08_335_guard_over_fire_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_335_guard_over_fire_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,335, 1);
				api_quest_SetJournalStep(userObjID,335, 1);
				api_quest_SetQuestStep(userObjID,335, 1);
				npc_talk_index = "n034_cleric_master_germain-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 468, 2);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200468, 30000);
end

</VillageServer>

<GameServer>
function sq08_335_guard_over_fire_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 34 then
		sq08_335_guard_over_fire_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n034_cleric_master_germain--------------------------------------------------------------------------------
function sq08_335_guard_over_fire_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n034_cleric_master_germain-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3351, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3352, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3353, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3354, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3355, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3356, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3357, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3358, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3359, false);
			 end 

	end
	if npc_talk_index == "n034_cleric_master_germain-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,335, 1);
				api_quest_SetJournalStep( pRoom, userObjID,335, 1);
				api_quest_SetQuestStep( pRoom, userObjID,335, 1);
				npc_talk_index = "n034_cleric_master_germain-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 468, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200468, 30000);

	end
	if npc_talk_index == "n034_cleric_master_germain-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3351, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3351, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3352, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3352, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3353, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3353, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3354, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3354, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3355, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3355, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3356, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3356, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3357, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3357, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3358, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3358, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3359, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3359, questID, 1);
			 end 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_335_guard_over_fire_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 335);
	if qstep == 1 and CountIndex == 468 then

	end
	if qstep == 1 and CountIndex == 200468 then
				api_quest_IncCounting( pRoom, userObjID, 2, 468);

	end
end

function sq08_335_guard_over_fire_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 335);
	if qstep == 1 and CountIndex == 468 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200468 and Count >= TargetCount  then

	end
end

function sq08_335_guard_over_fire_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 335);
	local questID=335;
end

function sq08_335_guard_over_fire_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 468, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200468, 30000);
end

function sq08_335_guard_over_fire_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_335_guard_over_fire_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,335, 1);
				api_quest_SetJournalStep( pRoom, userObjID,335, 1);
				api_quest_SetQuestStep( pRoom, userObjID,335, 1);
				npc_talk_index = "n034_cleric_master_germain-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 468, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200468, 30000);
end

</GameServer>