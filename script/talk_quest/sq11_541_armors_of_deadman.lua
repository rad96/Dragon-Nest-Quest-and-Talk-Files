<VillageServer>

function sq11_541_armors_of_deadman_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 36 then
		sq11_541_armors_of_deadman_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		sq11_541_armors_of_deadman_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq11_541_armors_of_deadman_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function sq11_541_armors_of_deadman_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n042_general_duglars-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest(userObjID,541, 1);
				api_quest_SetJournalStep(userObjID,541, 1);
				api_quest_SetQuestStep(userObjID,541, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-1-b" then 
	end
	if npc_talk_index == "n042_general_duglars-1-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5411, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5412, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5413, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5414, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5415, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5416, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5417, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5418, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5418, false);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 971, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 400422, 10);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 972, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200971, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200972, 30000);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n042_general_duglars-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5411, true);
				 api_quest_RewardQuestUser(userObjID, 5411, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5412, true);
				 api_quest_RewardQuestUser(userObjID, 5412, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5413, true);
				 api_quest_RewardQuestUser(userObjID, 5413, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5414, true);
				 api_quest_RewardQuestUser(userObjID, 5414, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5415, true);
				 api_quest_RewardQuestUser(userObjID, 5415, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5416, true);
				 api_quest_RewardQuestUser(userObjID, 5416, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5417, true);
				 api_quest_RewardQuestUser(userObjID, 5417, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5418, true);
				 api_quest_RewardQuestUser(userObjID, 5418, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5419, true);
				 api_quest_RewardQuestUser(userObjID, 5419, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_quest_HasQuestItem(userObjID, 400422, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400422, api_quest_HasQuestItem(userObjID, 400422, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_541_armors_of_deadman_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 541);
	if qstep == 2 and CountIndex == 971 then
				if api_quest_HasQuestItem(userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400422, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400422, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 400422 then

	end
	if qstep == 2 and CountIndex == 972 then
				if api_quest_HasQuestItem(userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400422, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400422, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200971 then
				if api_quest_HasQuestItem(userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400422, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400422, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200972 then
				if api_quest_HasQuestItem(userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400422, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400422, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

				end

	end
end

function sq11_541_armors_of_deadman_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 541);
	if qstep == 2 and CountIndex == 971 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400422 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 972 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200971 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200972 and Count >= TargetCount  then

	end
end

function sq11_541_armors_of_deadman_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 541);
	local questID=541;
end

function sq11_541_armors_of_deadman_OnRemoteStart( userObjID, questID )
end

function sq11_541_armors_of_deadman_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_541_armors_of_deadman_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,541, 1);
				api_quest_SetJournalStep(userObjID,541, 1);
				api_quest_SetQuestStep(userObjID,541, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</VillageServer>

<GameServer>
function sq11_541_armors_of_deadman_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 36 then
		sq11_541_armors_of_deadman_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		sq11_541_armors_of_deadman_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq11_541_armors_of_deadman_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function sq11_541_armors_of_deadman_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n042_general_duglars-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,541, 1);
				api_quest_SetJournalStep( pRoom, userObjID,541, 1);
				api_quest_SetQuestStep( pRoom, userObjID,541, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-1-b" then 
	end
	if npc_talk_index == "n042_general_duglars-1-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5411, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5412, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5413, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5414, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5415, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5416, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5417, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5418, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5418, false);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 971, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 400422, 10);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 972, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200971, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200972, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n042_general_duglars-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5411, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5412, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5413, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5413, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5414, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5414, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5415, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5415, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5416, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5416, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5417, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5417, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5418, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5418, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5419, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5419, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_quest_HasQuestItem( pRoom, userObjID, 400422, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400422, api_quest_HasQuestItem( pRoom, userObjID, 400422, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_541_armors_of_deadman_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 541);
	if qstep == 2 and CountIndex == 971 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400422, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400422, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 400422 then

	end
	if qstep == 2 and CountIndex == 972 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400422, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400422, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200971 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400422, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400422, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200972 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400422, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400422, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400422, 10) >= 10 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

				end

	end
end

function sq11_541_armors_of_deadman_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 541);
	if qstep == 2 and CountIndex == 971 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400422 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 972 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200971 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200972 and Count >= TargetCount  then

	end
end

function sq11_541_armors_of_deadman_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 541);
	local questID=541;
end

function sq11_541_armors_of_deadman_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_541_armors_of_deadman_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_541_armors_of_deadman_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,541, 1);
				api_quest_SetJournalStep( pRoom, userObjID,541, 1);
				api_quest_SetQuestStep( pRoom, userObjID,541, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</GameServer>