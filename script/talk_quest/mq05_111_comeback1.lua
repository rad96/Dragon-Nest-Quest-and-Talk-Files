<VillageServer>

function mq05_111_comeback1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 12 then
		mq05_111_comeback1_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 15 then
		mq05_111_comeback1_OnTalk_n015_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_111_comeback1_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n012_sorceress_master_cynthia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n012_sorceress_master_cynthia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,111, 2);
				api_quest_SetJournalStep(userObjID,111, 1);
				api_quest_SetQuestStep(userObjID,111, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1-a";

	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-party" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n012_sorceress_master_cynthia-2-e";

				else
									npc_talk_index = "n012_sorceress_master_cynthia-2-d";

				end
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-move" then 
				api_user_ChangeMap(userObjID,13511,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n015_sorceress_tara--------------------------------------------------------------------------------
function mq05_111_comeback1_OnTalk_n015_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n015_sorceress_tara-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n015_sorceress_tara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n015_sorceress_tara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n015_sorceress_tara-2-b" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-c" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-d" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-e" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-f" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-g" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-h" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-i" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1110, true);
				 api_quest_RewardQuestUser(userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1110, true);
				 api_quest_RewardQuestUser(userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1110, true);
				 api_quest_RewardQuestUser(userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1110, true);
				 api_quest_RewardQuestUser(userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1110, true);
				 api_quest_RewardQuestUser(userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1110, true);
				 api_quest_RewardQuestUser(userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1110, true);
				 api_quest_RewardQuestUser(userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1110, true);
				 api_quest_RewardQuestUser(userObjID, 1110, questID, 1);
			 end 
	end
	if npc_talk_index == "n015_sorceress_tara-2-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 122, 2);
					api_quest_SetQuestStep(userObjID, 122, 1);
					api_quest_SetJournalStep(userObjID, 122, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n015_sorceress_tara-1", "mq05_122_comeback2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_111_comeback1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 111);
end

function mq05_111_comeback1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 111);
end

function mq05_111_comeback1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 111);
	local questID=111;
end

function mq05_111_comeback1_OnRemoteStart( userObjID, questID )
end

function mq05_111_comeback1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq05_111_comeback1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,111, 2);
				api_quest_SetJournalStep(userObjID,111, 1);
				api_quest_SetQuestStep(userObjID,111, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1-a";
end

</VillageServer>

<GameServer>
function mq05_111_comeback1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 12 then
		mq05_111_comeback1_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 15 then
		mq05_111_comeback1_OnTalk_n015_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_111_comeback1_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n012_sorceress_master_cynthia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n012_sorceress_master_cynthia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,111, 2);
				api_quest_SetJournalStep( pRoom, userObjID,111, 1);
				api_quest_SetQuestStep( pRoom, userObjID,111, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1-a";

	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-party" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n012_sorceress_master_cynthia-2-e";

				else
									npc_talk_index = "n012_sorceress_master_cynthia-2-d";

				end
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-move" then 
				api_user_ChangeMap( pRoom, userObjID,13511,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n015_sorceress_tara--------------------------------------------------------------------------------
function mq05_111_comeback1_OnTalk_n015_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n015_sorceress_tara-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n015_sorceress_tara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n015_sorceress_tara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n015_sorceress_tara-2-b" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-c" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-d" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-e" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-f" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-g" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-h" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-i" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1110, questID, 1);
			 end 
	end
	if npc_talk_index == "n015_sorceress_tara-2-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 122, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 122, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 122, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n015_sorceress_tara-1", "mq05_122_comeback2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_111_comeback1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 111);
end

function mq05_111_comeback1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 111);
end

function mq05_111_comeback1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 111);
	local questID=111;
end

function mq05_111_comeback1_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq05_111_comeback1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq05_111_comeback1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,111, 2);
				api_quest_SetJournalStep( pRoom, userObjID,111, 1);
				api_quest_SetQuestStep( pRoom, userObjID,111, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1-a";
end

</GameServer>