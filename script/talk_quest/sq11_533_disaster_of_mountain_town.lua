<VillageServer>

function sq11_533_disaster_of_mountain_town_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 41 then
		sq11_533_disaster_of_mountain_town_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n041_duke_stwart--------------------------------------------------------------------------------
function sq11_533_disaster_of_mountain_town_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n041_duke_stwart-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n041_duke_stwart-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n041_duke_stwart-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5331, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5332, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5333, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5334, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5335, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5336, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5337, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5338, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5339, false);
			 end 

	end
	if npc_talk_index == "n041_duke_stwart-accepting-acceptted" then
				npc_talk_index = "n041_duke_stwart-1";
				api_quest_AddQuest(userObjID,533, 1);
				api_quest_SetJournalStep(userObjID,533, 1);
				api_quest_SetQuestStep(userObjID,533, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 882, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 883, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 888, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200882, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200883, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200888, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 3, 400127, 1);
				api_quest_SetCountingInfo(userObjID, questID, 7, 3, 400128, 1);
				api_quest_SetCountingInfo(userObjID, questID, 8, 3, 400129, 1);

	end
	if npc_talk_index == "n041_duke_stwart-4-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5331, true);
				 api_quest_RewardQuestUser(userObjID, 5331, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5332, true);
				 api_quest_RewardQuestUser(userObjID, 5332, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5333, true);
				 api_quest_RewardQuestUser(userObjID, 5333, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5334, true);
				 api_quest_RewardQuestUser(userObjID, 5334, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5335, true);
				 api_quest_RewardQuestUser(userObjID, 5335, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5336, true);
				 api_quest_RewardQuestUser(userObjID, 5336, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5337, true);
				 api_quest_RewardQuestUser(userObjID, 5337, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5338, true);
				 api_quest_RewardQuestUser(userObjID, 5338, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5339, true);
				 api_quest_RewardQuestUser(userObjID, 5339, questID, 1);
			 end 
	end
	if npc_talk_index == "n041_duke_stwart-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 534, 1);
					api_quest_SetQuestStep(userObjID, 534, 1);
					api_quest_SetJournalStep(userObjID, 534, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400127, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400127, api_quest_HasQuestItem(userObjID, 400127, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400128, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400128, api_quest_HasQuestItem(userObjID, 400128, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400129, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400129, api_quest_HasQuestItem(userObjID, 400129, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n041_duke_stwart-1", "sq11_534_arrest_the_falsifier1.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_533_disaster_of_mountain_town_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 533);
	if qstep == 1 and CountIndex == 882 then
				if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400127, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400127, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 883 then
				if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 400128, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400128, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400128, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

				else
				end

	end
	if qstep == 1 and CountIndex == 888 then
				if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400128, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 400129, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400129, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400129, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				end

				else
				end

	end
	if qstep == 1 and CountIndex == 400127 then

	end
	if qstep == 1 and CountIndex == 400128 then

	end
	if qstep == 1 and CountIndex == 400129 then

	end
	if qstep == 1 and CountIndex == 200882 then
				if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400127, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400127, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 200883 then
				if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 400128, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400128, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400128, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

				else
				end

	end
	if qstep == 1 and CountIndex == 200888 then
				if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400128, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 400129, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400129, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400129, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				end

				else
				end

	end
end

function sq11_533_disaster_of_mountain_town_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 533);
	if qstep == 1 and CountIndex == 882 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 883 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 888 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400127 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400128 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400129 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200882 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200883 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200888 and Count >= TargetCount  then

	end
end

function sq11_533_disaster_of_mountain_town_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 533);
	local questID=533;
end

function sq11_533_disaster_of_mountain_town_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 882, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 883, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 888, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200882, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200883, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200888, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 3, 400127, 1);
				api_quest_SetCountingInfo(userObjID, questID, 7, 3, 400128, 1);
				api_quest_SetCountingInfo(userObjID, questID, 8, 3, 400129, 1);
end

function sq11_533_disaster_of_mountain_town_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_533_disaster_of_mountain_town_ForceAccept( userObjID, npcObjID, questID )
				npc_talk_index = "n041_duke_stwart-1";
				api_quest_AddQuest(userObjID,533, 1);
				api_quest_SetJournalStep(userObjID,533, 1);
				api_quest_SetQuestStep(userObjID,533, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 882, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 883, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 888, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200882, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200883, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200888, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 3, 400127, 1);
				api_quest_SetCountingInfo(userObjID, questID, 7, 3, 400128, 1);
				api_quest_SetCountingInfo(userObjID, questID, 8, 3, 400129, 1);
end

</VillageServer>

<GameServer>
function sq11_533_disaster_of_mountain_town_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 41 then
		sq11_533_disaster_of_mountain_town_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n041_duke_stwart--------------------------------------------------------------------------------
function sq11_533_disaster_of_mountain_town_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n041_duke_stwart-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n041_duke_stwart-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n041_duke_stwart-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5331, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5332, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5333, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5334, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5335, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5336, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5337, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5338, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5339, false);
			 end 

	end
	if npc_talk_index == "n041_duke_stwart-accepting-acceptted" then
				npc_talk_index = "n041_duke_stwart-1";
				api_quest_AddQuest( pRoom, userObjID,533, 1);
				api_quest_SetJournalStep( pRoom, userObjID,533, 1);
				api_quest_SetQuestStep( pRoom, userObjID,533, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 882, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 883, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 888, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200882, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200883, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200888, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 3, 400127, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 3, 400128, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 3, 400129, 1);

	end
	if npc_talk_index == "n041_duke_stwart-4-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5331, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5331, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5332, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5332, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5333, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5333, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5334, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5334, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5335, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5335, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5336, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5336, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5337, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5337, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5338, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5338, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5339, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5339, questID, 1);
			 end 
	end
	if npc_talk_index == "n041_duke_stwart-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 534, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 534, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 534, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400127, api_quest_HasQuestItem( pRoom, userObjID, 400127, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400128, api_quest_HasQuestItem( pRoom, userObjID, 400128, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400129, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400129, api_quest_HasQuestItem( pRoom, userObjID, 400129, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n041_duke_stwart-1", "sq11_534_arrest_the_falsifier1.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_533_disaster_of_mountain_town_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 533);
	if qstep == 1 and CountIndex == 882 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400127, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400127, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 883 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400128, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400128, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

				else
				end

	end
	if qstep == 1 and CountIndex == 888 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400129, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400129, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400129, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				end

				else
				end

	end
	if qstep == 1 and CountIndex == 400127 then

	end
	if qstep == 1 and CountIndex == 400128 then

	end
	if qstep == 1 and CountIndex == 400129 then

	end
	if qstep == 1 and CountIndex == 200882 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400127, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400127, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 200883 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400128, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400128, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

				else
				end

	end
	if qstep == 1 and CountIndex == 200888 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400129, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400129, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400129, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400129, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				end

				else
				end

	end
end

function sq11_533_disaster_of_mountain_town_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 533);
	if qstep == 1 and CountIndex == 882 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 883 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 888 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400127 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400128 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400129 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200882 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200883 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200888 and Count >= TargetCount  then

	end
end

function sq11_533_disaster_of_mountain_town_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 533);
	local questID=533;
end

function sq11_533_disaster_of_mountain_town_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 882, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 883, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 888, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200882, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200883, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200888, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 3, 400127, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 3, 400128, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 3, 400129, 1);
end

function sq11_533_disaster_of_mountain_town_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_533_disaster_of_mountain_town_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				npc_talk_index = "n041_duke_stwart-1";
				api_quest_AddQuest( pRoom, userObjID,533, 1);
				api_quest_SetJournalStep( pRoom, userObjID,533, 1);
				api_quest_SetQuestStep( pRoom, userObjID,533, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 882, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 883, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 888, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200882, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200883, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200888, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 3, 400127, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 3, 400128, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 3, 400129, 1);
end

</GameServer>