<VillageServer>

function mq01_031_desiny_girl_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1 then
		mq01_031_desiny_girl_OnTalk_n001_elder_harold(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1091 then
		mq01_031_desiny_girl_OnTalk_n1091_illusion(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1206 then
		mq01_031_desiny_girl_OnTalk_n1206_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n001_elder_harold--------------------------------------------------------------------------------
function mq01_031_desiny_girl_OnTalk_n001_elder_harold(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n001_elder_harold-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n001_elder_harold-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n001_elder_harold-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n001_elder_harold-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n001_elder_harold-accepting-acceptted0" then
				api_quest_AddQuest(userObjID,31, 2);
				api_quest_SetJournalStep(userObjID,31, 1);
				api_quest_SetQuestStep(userObjID,31, 1);
				npc_talk_index = "n001_elder_harold-1";

	end
	if npc_talk_index == "n001_elder_harold-1-b" then 
	end
	if npc_talk_index == "n001_elder_harold-1-c" then 
	end
	if npc_talk_index == "n001_elder_harold-1-d" then 
	end
	if npc_talk_index == "n001_elder_harold-1-e" then 
	end
	if npc_talk_index == "n001_elder_harold-1-f" then 
	end
	if npc_talk_index == "n001_elder_harold-1-g" then 
	end
	if npc_talk_index == "n001_elder_harold-1-h" then 
	end
	if npc_talk_index == "n001_elder_harold-1-i" then 
	end
	if npc_talk_index == "n001_elder_harold-1-j" then 
	end
	if npc_talk_index == "n001_elder_harold-1-k" then 
	end
	if npc_talk_index == "n001_elder_harold-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300572, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 104, 30000);
	end
	if npc_talk_index == "n001_elder_harold-2-party1" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n001_elder_harold-2-d";

				else
									npc_talk_index = "n001_elder_harold-2-c";

				end
	end
	if npc_talk_index == "n001_elder_harold-2-e" then 
	end
	if npc_talk_index == "n001_elder_harold-2-move_harold" then 
				api_user_ChangeMap(userObjID,13511,1);
	end
	if npc_talk_index == "n001_elder_harold-2-f" then 
	end
	if npc_talk_index == "n001_elder_harold-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 310, true);
				 api_quest_RewardQuestUser(userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 310, true);
				 api_quest_RewardQuestUser(userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 310, true);
				 api_quest_RewardQuestUser(userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 310, true);
				 api_quest_RewardQuestUser(userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 310, true);
				 api_quest_RewardQuestUser(userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 310, true);
				 api_quest_RewardQuestUser(userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 310, true);
				 api_quest_RewardQuestUser(userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 310, true);
				 api_quest_RewardQuestUser(userObjID, 310, questID, 1);
			 end 
	end
	if npc_talk_index == "n001_elder_harold-4-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 32, 2);
					api_quest_SetQuestStep(userObjID, 32, 1);
					api_quest_SetJournalStep(userObjID, 32, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300572, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300572, api_quest_HasQuestItem(userObjID, 300572, 1));
				end
	end
	if npc_talk_index == "n001_elder_harold-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n001_elder_harold-1", "mq01_032_befogged.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1091_illusion--------------------------------------------------------------------------------
function mq01_031_desiny_girl_OnTalk_n1091_illusion(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1091_illusion-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1091_illusion-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1091_illusion-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1091_illusion-3-b" then 
	end
	if npc_talk_index == "n1091_illusion-3-c" then 
	end
	if npc_talk_index == "n1091_illusion-3-d" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1091_illusion-3-a" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1206_lunaria--------------------------------------------------------------------------------
function mq01_031_desiny_girl_OnTalk_n1206_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1206_lunaria-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1206_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1206_lunaria-accepting-acceptted1" then
				api_quest_AddQuest(userObjID,31, 2);
				api_quest_SetJournalStep(userObjID,31, 1);
				api_quest_SetQuestStep(userObjID,31, 1);
				npc_talk_index = "n1206_lunaria-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq01_031_desiny_girl_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 31);
	if qstep == 2 and CountIndex == 104 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300572, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300572, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300572 then

	end
end

function mq01_031_desiny_girl_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 31);
	if qstep == 2 and CountIndex == 104 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300572 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function mq01_031_desiny_girl_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 31);
	local questID=31;
end

function mq01_031_desiny_girl_OnRemoteStart( userObjID, questID )
end

function mq01_031_desiny_girl_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq01_031_desiny_girl_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mq01_031_desiny_girl_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1 then
		mq01_031_desiny_girl_OnTalk_n001_elder_harold( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1091 then
		mq01_031_desiny_girl_OnTalk_n1091_illusion( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1206 then
		mq01_031_desiny_girl_OnTalk_n1206_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n001_elder_harold--------------------------------------------------------------------------------
function mq01_031_desiny_girl_OnTalk_n001_elder_harold( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n001_elder_harold-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n001_elder_harold-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n001_elder_harold-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n001_elder_harold-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n001_elder_harold-accepting-acceptted0" then
				api_quest_AddQuest( pRoom, userObjID,31, 2);
				api_quest_SetJournalStep( pRoom, userObjID,31, 1);
				api_quest_SetQuestStep( pRoom, userObjID,31, 1);
				npc_talk_index = "n001_elder_harold-1";

	end
	if npc_talk_index == "n001_elder_harold-1-b" then 
	end
	if npc_talk_index == "n001_elder_harold-1-c" then 
	end
	if npc_talk_index == "n001_elder_harold-1-d" then 
	end
	if npc_talk_index == "n001_elder_harold-1-e" then 
	end
	if npc_talk_index == "n001_elder_harold-1-f" then 
	end
	if npc_talk_index == "n001_elder_harold-1-g" then 
	end
	if npc_talk_index == "n001_elder_harold-1-h" then 
	end
	if npc_talk_index == "n001_elder_harold-1-i" then 
	end
	if npc_talk_index == "n001_elder_harold-1-j" then 
	end
	if npc_talk_index == "n001_elder_harold-1-k" then 
	end
	if npc_talk_index == "n001_elder_harold-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300572, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 104, 30000);
	end
	if npc_talk_index == "n001_elder_harold-2-party1" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n001_elder_harold-2-d";

				else
									npc_talk_index = "n001_elder_harold-2-c";

				end
	end
	if npc_talk_index == "n001_elder_harold-2-e" then 
	end
	if npc_talk_index == "n001_elder_harold-2-move_harold" then 
				api_user_ChangeMap( pRoom, userObjID,13511,1);
	end
	if npc_talk_index == "n001_elder_harold-2-f" then 
	end
	if npc_talk_index == "n001_elder_harold-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 310, questID, 1);
			 end 
	end
	if npc_talk_index == "n001_elder_harold-4-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 32, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 32, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 32, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300572, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300572, api_quest_HasQuestItem( pRoom, userObjID, 300572, 1));
				end
	end
	if npc_talk_index == "n001_elder_harold-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n001_elder_harold-1", "mq01_032_befogged.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1091_illusion--------------------------------------------------------------------------------
function mq01_031_desiny_girl_OnTalk_n1091_illusion( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1091_illusion-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1091_illusion-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1091_illusion-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1091_illusion-3-b" then 
	end
	if npc_talk_index == "n1091_illusion-3-c" then 
	end
	if npc_talk_index == "n1091_illusion-3-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1091_illusion-3-a" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1206_lunaria--------------------------------------------------------------------------------
function mq01_031_desiny_girl_OnTalk_n1206_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1206_lunaria-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1206_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1206_lunaria-accepting-acceptted1" then
				api_quest_AddQuest( pRoom, userObjID,31, 2);
				api_quest_SetJournalStep( pRoom, userObjID,31, 1);
				api_quest_SetQuestStep( pRoom, userObjID,31, 1);
				npc_talk_index = "n1206_lunaria-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq01_031_desiny_girl_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 31);
	if qstep == 2 and CountIndex == 104 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300572, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300572, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300572 then

	end
end

function mq01_031_desiny_girl_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 31);
	if qstep == 2 and CountIndex == 104 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300572 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function mq01_031_desiny_girl_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 31);
	local questID=31;
end

function mq01_031_desiny_girl_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq01_031_desiny_girl_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq01_031_desiny_girl_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>