<VillageServer>

function mqc09_9646_reconciliation_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1861 then
		mqc09_9646_reconciliation_OnTalk_n1861_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1915 then
		mqc09_9646_reconciliation_OnTalk_n1915_suriya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1918 then
		mqc09_9646_reconciliation_OnTalk_n1918_cleric_chris(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1928 then
		mqc09_9646_reconciliation_OnTalk_n1928_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mqc09_9646_reconciliation_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1861_shaolong--------------------------------------------------------------------------------
function mqc09_9646_reconciliation_OnTalk_n1861_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1861_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1861_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1861_shaolong-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9646, 2);
				api_quest_SetJournalStep(userObjID,9646, 1);
				api_quest_SetQuestStep(userObjID,9646, 1);
				npc_talk_index = "n1861_shaolong-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1915_suriya--------------------------------------------------------------------------------
function mqc09_9646_reconciliation_OnTalk_n1915_suriya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1915_suriya-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1915_suriya-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1915_suriya-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1915_suriya-3-b" then 
	end
	if npc_talk_index == "n1915_suriya-3-c" then 
	end
	if npc_talk_index == "n1915_suriya-3-d" then 
	end
	if npc_talk_index == "n1915_suriya-3-e" then 
	end
	if npc_talk_index == "n1915_suriya-3-f" then 
	end
	if npc_talk_index == "n1915_suriya-3-g" then 
	end
	if npc_talk_index == "n1915_suriya-3-h" then 
	end
	if npc_talk_index == "n1915_suriya-3-i" then 
	end
	if npc_talk_index == "n1915_suriya-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1918_cleric_chris--------------------------------------------------------------------------------
function mqc09_9646_reconciliation_OnTalk_n1918_cleric_chris(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1918_cleric_chris-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1918_cleric_chris-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1918_cleric_chris-1-b" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-c" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-d" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-e" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-f" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-g" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-h" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-i" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-j" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-k" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-l" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-m" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-n" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-o" then 
	end
	if npc_talk_index == "n1918_cleric_chris-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1928_shaolong--------------------------------------------------------------------------------
function mqc09_9646_reconciliation_OnTalk_n1928_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1928_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1928_shaolong-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1928_shaolong-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 96460, true);
				 api_quest_RewardQuestUser(userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 96460, true);
				 api_quest_RewardQuestUser(userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 96460, true);
				 api_quest_RewardQuestUser(userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 96460, true);
				 api_quest_RewardQuestUser(userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 96460, true);
				 api_quest_RewardQuestUser(userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 96460, true);
				 api_quest_RewardQuestUser(userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 96460, true);
				 api_quest_RewardQuestUser(userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 96460, true);
				 api_quest_RewardQuestUser(userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 96460, true);
				 api_quest_RewardQuestUser(userObjID, 96460, questID, 1);
			 end 
	end
	if npc_talk_index == "n1928_shaolong-4-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9647, 2);
					api_quest_SetQuestStep(userObjID, 9647, 1);
					api_quest_SetJournalStep(userObjID, 9647, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1928_shaolong-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1928_shaolong-1", "mqc09_9647_twisted_home.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mqc09_9646_reconciliation_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
	end
	if npc_talk_index == "n707_sidel-2-d" then 
	end
	if npc_talk_index == "n707_sidel-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc09_9646_reconciliation_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9646);
end

function mqc09_9646_reconciliation_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9646);
end

function mqc09_9646_reconciliation_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9646);
	local questID=9646;
end

function mqc09_9646_reconciliation_OnRemoteStart( userObjID, questID )
end

function mqc09_9646_reconciliation_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc09_9646_reconciliation_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9646, 2);
				api_quest_SetJournalStep(userObjID,9646, 1);
				api_quest_SetQuestStep(userObjID,9646, 1);
				npc_talk_index = "n1861_shaolong-1";
end

</VillageServer>

<GameServer>
function mqc09_9646_reconciliation_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1861 then
		mqc09_9646_reconciliation_OnTalk_n1861_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1915 then
		mqc09_9646_reconciliation_OnTalk_n1915_suriya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1918 then
		mqc09_9646_reconciliation_OnTalk_n1918_cleric_chris( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1928 then
		mqc09_9646_reconciliation_OnTalk_n1928_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mqc09_9646_reconciliation_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1861_shaolong--------------------------------------------------------------------------------
function mqc09_9646_reconciliation_OnTalk_n1861_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1861_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1861_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1861_shaolong-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9646, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9646, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9646, 1);
				npc_talk_index = "n1861_shaolong-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1915_suriya--------------------------------------------------------------------------------
function mqc09_9646_reconciliation_OnTalk_n1915_suriya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1915_suriya-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1915_suriya-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1915_suriya-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1915_suriya-3-b" then 
	end
	if npc_talk_index == "n1915_suriya-3-c" then 
	end
	if npc_talk_index == "n1915_suriya-3-d" then 
	end
	if npc_talk_index == "n1915_suriya-3-e" then 
	end
	if npc_talk_index == "n1915_suriya-3-f" then 
	end
	if npc_talk_index == "n1915_suriya-3-g" then 
	end
	if npc_talk_index == "n1915_suriya-3-h" then 
	end
	if npc_talk_index == "n1915_suriya-3-i" then 
	end
	if npc_talk_index == "n1915_suriya-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1918_cleric_chris--------------------------------------------------------------------------------
function mqc09_9646_reconciliation_OnTalk_n1918_cleric_chris( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1918_cleric_chris-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1918_cleric_chris-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1918_cleric_chris-1-b" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-c" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-d" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-e" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-f" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-g" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-h" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-i" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-j" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-k" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-l" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-m" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-n" then 
	end
	if npc_talk_index == "n1918_cleric_chris-1-o" then 
	end
	if npc_talk_index == "n1918_cleric_chris-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1928_shaolong--------------------------------------------------------------------------------
function mqc09_9646_reconciliation_OnTalk_n1928_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1928_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1928_shaolong-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1928_shaolong-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96460, questID, 1);
			 end 
	end
	if npc_talk_index == "n1928_shaolong-4-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9647, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9647, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9647, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1928_shaolong-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1928_shaolong-1", "mqc09_9647_twisted_home.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mqc09_9646_reconciliation_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
	end
	if npc_talk_index == "n707_sidel-2-d" then 
	end
	if npc_talk_index == "n707_sidel-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc09_9646_reconciliation_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9646);
end

function mqc09_9646_reconciliation_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9646);
end

function mqc09_9646_reconciliation_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9646);
	local questID=9646;
end

function mqc09_9646_reconciliation_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc09_9646_reconciliation_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc09_9646_reconciliation_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9646, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9646, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9646, 1);
				npc_talk_index = "n1861_shaolong-1";
end

</GameServer>