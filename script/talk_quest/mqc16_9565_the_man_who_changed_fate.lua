<VillageServer>

function mqc16_9565_the_man_who_changed_fate_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1770 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1770_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1774 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1774_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1775 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1775_book(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1776 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1776_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1777 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1777_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1778 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1778_princess_elizabeth(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1770_gereint_kid--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1770_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1770_gereint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1770_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1770_gereint_kid-accepting-acceptted_1" then
				api_quest_AddQuest(userObjID,9565, 2);
				api_quest_SetJournalStep(userObjID,9565, 1);
				api_quest_SetQuestStep(userObjID,9565, 1);
				npc_talk_index = "n1770_gereint_kid-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1065_geraint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1065_geraint_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1065_geraint_kid-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1065_geraint_kid-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1065_geraint_kid-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1065_geraint_kid-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1065_geraint_kid-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,9565, 2);
				api_quest_SetJournalStep(userObjID,9565, 1);
				api_quest_SetQuestStep(userObjID,9565, 1);
				npc_talk_index = "n1065_geraint_kid-1";

	end
	if npc_talk_index == "n1065_geraint_kid-2-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-2-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-2-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-2-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n1065_geraint_kid-4-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-f" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-j" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-k" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-l" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-m" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-n" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-o" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-p" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-q" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-r" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-s" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-t" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end
	if npc_talk_index == "n1065_geraint_kid-5-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-f" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-j" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-k" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-l" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-m" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-n" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-o" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-p" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
	end
	if npc_talk_index == "n1065_geraint_kid-6-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-f" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-j" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-k" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-l" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-m" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-n" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-o" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-p" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-q" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-r" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-s" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-t" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-u" then 
	end
	if npc_talk_index == "n1065_geraint_kid-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1065_geraint_kid-7-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-7-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-7-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-7-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-7-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95650, true);
				 api_quest_RewardQuestUser(userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95650, true);
				 api_quest_RewardQuestUser(userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95650, true);
				 api_quest_RewardQuestUser(userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95650, true);
				 api_quest_RewardQuestUser(userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95650, true);
				 api_quest_RewardQuestUser(userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95650, true);
				 api_quest_RewardQuestUser(userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95650, true);
				 api_quest_RewardQuestUser(userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95650, true);
				 api_quest_RewardQuestUser(userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95650, true);
				 api_quest_RewardQuestUser(userObjID, 95650, questID, 1);
			 end 
	end
	if npc_talk_index == "n1065_geraint_kid-7-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9566, 2);
					api_quest_SetQuestStep(userObjID, 9566, 1);
					api_quest_SetJournalStep(userObjID, 9566, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1065_geraint_kid-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1065_geraint_kid-1", "mqc16_9566_twisted_fate.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1774_gereint_kid--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1774_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1774_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1774_gereint_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1774_gereint_kid-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1774_gereint_kid-1-b" then 
	end
	if npc_talk_index == "n1774_gereint_kid-1-c" then 
	end
	if npc_talk_index == "n1774_gereint_kid-1-c" then 
	end
	if npc_talk_index == "n1774_gereint_kid-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1774_gereint_kid-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1775_book--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1775_book(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1776_meriendel--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1776_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1777_pether--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1777_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1778_princess_elizabeth--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1778_princess_elizabeth(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9565_the_man_who_changed_fate_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9565);
end

function mqc16_9565_the_man_who_changed_fate_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9565);
end

function mqc16_9565_the_man_who_changed_fate_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9565);
	local questID=9565;
end

function mqc16_9565_the_man_who_changed_fate_OnRemoteStart( userObjID, questID )
end

function mqc16_9565_the_man_who_changed_fate_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc16_9565_the_man_who_changed_fate_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mqc16_9565_the_man_who_changed_fate_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1770 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1770_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1774 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1774_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1775 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1775_book( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1776 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1776_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1777 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1777_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1778 then
		mqc16_9565_the_man_who_changed_fate_OnTalk_n1778_princess_elizabeth( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1770_gereint_kid--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1770_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1770_gereint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1770_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1770_gereint_kid-accepting-acceptted_1" then
				api_quest_AddQuest( pRoom, userObjID,9565, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9565, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9565, 1);
				npc_talk_index = "n1770_gereint_kid-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1065_geraint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1065_geraint_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1065_geraint_kid-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1065_geraint_kid-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1065_geraint_kid-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1065_geraint_kid-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1065_geraint_kid-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,9565, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9565, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9565, 1);
				npc_talk_index = "n1065_geraint_kid-1";

	end
	if npc_talk_index == "n1065_geraint_kid-2-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-2-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-2-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-2-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n1065_geraint_kid-4-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-f" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-j" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-k" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-l" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-m" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-n" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-o" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-p" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-q" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-r" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-s" then 
	end
	if npc_talk_index == "n1065_geraint_kid-4-t" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end
	if npc_talk_index == "n1065_geraint_kid-5-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-f" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-j" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-k" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-l" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-m" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-n" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-o" then 
	end
	if npc_talk_index == "n1065_geraint_kid-5-p" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
	end
	if npc_talk_index == "n1065_geraint_kid-6-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-f" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-j" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-k" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-l" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-m" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-n" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-o" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-p" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-q" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-r" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-s" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-t" then 
	end
	if npc_talk_index == "n1065_geraint_kid-6-u" then 
	end
	if npc_talk_index == "n1065_geraint_kid-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1065_geraint_kid-7-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-7-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-7-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-7-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-7-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95650, questID, 1);
			 end 
	end
	if npc_talk_index == "n1065_geraint_kid-7-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9566, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9566, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9566, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1065_geraint_kid-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1065_geraint_kid-1", "mqc16_9566_twisted_fate.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1774_gereint_kid--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1774_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1774_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1774_gereint_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1774_gereint_kid-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1774_gereint_kid-1-b" then 
	end
	if npc_talk_index == "n1774_gereint_kid-1-c" then 
	end
	if npc_talk_index == "n1774_gereint_kid-1-c" then 
	end
	if npc_talk_index == "n1774_gereint_kid-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1774_gereint_kid-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1775_book--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1775_book( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1776_meriendel--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1776_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1777_pether--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1777_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1778_princess_elizabeth--------------------------------------------------------------------------------
function mqc16_9565_the_man_who_changed_fate_OnTalk_n1778_princess_elizabeth( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9565_the_man_who_changed_fate_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9565);
end

function mqc16_9565_the_man_who_changed_fate_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9565);
end

function mqc16_9565_the_man_who_changed_fate_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9565);
	local questID=9565;
end

function mqc16_9565_the_man_who_changed_fate_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc16_9565_the_man_who_changed_fate_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc16_9565_the_man_who_changed_fate_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>