<VillageServer>

function mq08_186_sweep_all_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1092 then
		mq08_186_sweep_all_OnTalk_n1092_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1101 then
		mq08_186_sweep_all_OnTalk_n1101_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 23 then
		mq08_186_sweep_all_OnTalk_n023_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 33 then
		mq08_186_sweep_all_OnTalk_n033_archer_master_adellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 547 then
		mq08_186_sweep_all_OnTalk_n547_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 549 then
		mq08_186_sweep_all_OnTalk_n549_triana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1092_lunaria--------------------------------------------------------------------------------
function mq08_186_sweep_all_OnTalk_n1092_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1092_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1101_lunaria--------------------------------------------------------------------------------
function mq08_186_sweep_all_OnTalk_n1101_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1101_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1101_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1101_lunaria-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n023_ranger_fugus--------------------------------------------------------------------------------
function mq08_186_sweep_all_OnTalk_n023_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n023_ranger_fugus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n023_ranger_fugus-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n023_ranger_fugus-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n023_ranger_fugus-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n023_ranger_fugus-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n023_ranger_fugus-accepting-acceptted" then
				api_quest_AddQuest(userObjID,186, 2);
				api_quest_SetJournalStep(userObjID,186, 1);
				api_quest_SetQuestStep(userObjID,186, 1);
				npc_talk_index = "n023_ranger_fugus-1";

	end
	if npc_talk_index == "n023_ranger_fugus-1-a" then 

				if api_quest_HasQuestItem(userObjID, 300012, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300012, api_quest_HasQuestItem(userObjID, 300012, 1));
				end
	end
	if npc_talk_index == "n023_ranger_fugus-1-b" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-c" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-d" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-e" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-f" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-g" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-h" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-i" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-j" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-k" then 
	end
	if npc_talk_index == "n023_ranger_fugus-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 350, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200350, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300015, 1);
	end
	if npc_talk_index == "n023_ranger_fugus-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n033_archer_master_adellin--------------------------------------------------------------------------------
function mq08_186_sweep_all_OnTalk_n033_archer_master_adellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n033_archer_master_adellin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n033_archer_master_adellin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n033_archer_master_adellin-4-b" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-c" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-d" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-e" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1860, true);
				 api_quest_RewardQuestUser(userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1860, true);
				 api_quest_RewardQuestUser(userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1860, true);
				 api_quest_RewardQuestUser(userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1860, true);
				 api_quest_RewardQuestUser(userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1860, true);
				 api_quest_RewardQuestUser(userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1860, true);
				 api_quest_RewardQuestUser(userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1860, true);
				 api_quest_RewardQuestUser(userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1860, true);
				 api_quest_RewardQuestUser(userObjID, 1860, questID, 1);
			 end 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300015, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300015, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n033_archer_master_adellin-4-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 187, 2);
					api_quest_SetQuestStep(userObjID, 187, 1);
					api_quest_SetJournalStep(userObjID, 187, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n033_archer_master_adellin-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n033_archer_master_adellin-1", "mq08_187_lavor_in_vain.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n547_cian--------------------------------------------------------------------------------
function mq08_186_sweep_all_OnTalk_n547_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n547_cian-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n547_cian-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n547_cian-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n549_triana--------------------------------------------------------------------------------
function mq08_186_sweep_all_OnTalk_n549_triana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n549_triana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n549_triana-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_186_sweep_all_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 186);
	if qstep == 2 and CountIndex == 350 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300015, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300015, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200350 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300015, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300015, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300015 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function mq08_186_sweep_all_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 186);
	if qstep == 2 and CountIndex == 350 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200350 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300015 and Count >= TargetCount  then

	end
end

function mq08_186_sweep_all_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 186);
	local questID=186;
end

function mq08_186_sweep_all_OnRemoteStart( userObjID, questID )
end

function mq08_186_sweep_all_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq08_186_sweep_all_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,186, 2);
				api_quest_SetJournalStep(userObjID,186, 1);
				api_quest_SetQuestStep(userObjID,186, 1);
				npc_talk_index = "n023_ranger_fugus-1";
end

</VillageServer>

<GameServer>
function mq08_186_sweep_all_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1092 then
		mq08_186_sweep_all_OnTalk_n1092_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1101 then
		mq08_186_sweep_all_OnTalk_n1101_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 23 then
		mq08_186_sweep_all_OnTalk_n023_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 33 then
		mq08_186_sweep_all_OnTalk_n033_archer_master_adellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 547 then
		mq08_186_sweep_all_OnTalk_n547_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 549 then
		mq08_186_sweep_all_OnTalk_n549_triana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1092_lunaria--------------------------------------------------------------------------------
function mq08_186_sweep_all_OnTalk_n1092_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1092_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1101_lunaria--------------------------------------------------------------------------------
function mq08_186_sweep_all_OnTalk_n1101_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1101_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1101_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1101_lunaria-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n023_ranger_fugus--------------------------------------------------------------------------------
function mq08_186_sweep_all_OnTalk_n023_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n023_ranger_fugus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n023_ranger_fugus-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n023_ranger_fugus-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n023_ranger_fugus-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n023_ranger_fugus-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n023_ranger_fugus-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,186, 2);
				api_quest_SetJournalStep( pRoom, userObjID,186, 1);
				api_quest_SetQuestStep( pRoom, userObjID,186, 1);
				npc_talk_index = "n023_ranger_fugus-1";

	end
	if npc_talk_index == "n023_ranger_fugus-1-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300012, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300012, api_quest_HasQuestItem( pRoom, userObjID, 300012, 1));
				end
	end
	if npc_talk_index == "n023_ranger_fugus-1-b" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-c" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-d" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-e" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-f" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-g" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-h" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-i" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-j" then 
	end
	if npc_talk_index == "n023_ranger_fugus-1-k" then 
	end
	if npc_talk_index == "n023_ranger_fugus-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 350, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200350, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300015, 1);
	end
	if npc_talk_index == "n023_ranger_fugus-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n033_archer_master_adellin--------------------------------------------------------------------------------
function mq08_186_sweep_all_OnTalk_n033_archer_master_adellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n033_archer_master_adellin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n033_archer_master_adellin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n033_archer_master_adellin-4-b" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-c" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-d" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-e" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1860, questID, 1);
			 end 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300015, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300015, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n033_archer_master_adellin-4-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 187, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 187, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 187, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n033_archer_master_adellin-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n033_archer_master_adellin-1", "mq08_187_lavor_in_vain.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n547_cian--------------------------------------------------------------------------------
function mq08_186_sweep_all_OnTalk_n547_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n547_cian-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n547_cian-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n547_cian-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n549_triana--------------------------------------------------------------------------------
function mq08_186_sweep_all_OnTalk_n549_triana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n549_triana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n549_triana-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_186_sweep_all_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 186);
	if qstep == 2 and CountIndex == 350 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300015, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300015, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200350 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300015, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300015, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300015 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function mq08_186_sweep_all_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 186);
	if qstep == 2 and CountIndex == 350 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200350 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300015 and Count >= TargetCount  then

	end
end

function mq08_186_sweep_all_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 186);
	local questID=186;
end

function mq08_186_sweep_all_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq08_186_sweep_all_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq08_186_sweep_all_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,186, 2);
				api_quest_SetJournalStep( pRoom, userObjID,186, 1);
				api_quest_SetQuestStep( pRoom, userObjID,186, 1);
				npc_talk_index = "n023_ranger_fugus-1";
end

</GameServer>