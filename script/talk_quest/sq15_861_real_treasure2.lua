<VillageServer>

function sq15_861_real_treasure2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 710 then
		sq15_861_real_treasure2_OnTalk_n710_scholar_hancock(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n710_scholar_hancock--------------------------------------------------------------------------------
function sq15_861_real_treasure2_OnTalk_n710_scholar_hancock(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n710_scholar_hancock-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n710_scholar_hancock-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n710_scholar_hancock-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n710_scholar_hancock-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n710_scholar_hancock-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8610, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8610, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8610, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8610, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8610, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8610, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8610, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8610, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8610, false);
			 end 

	end
	if npc_talk_index == "n710_scholar_hancock-accepting-acceptted" then
				api_quest_AddQuest(userObjID,861, 1);
				api_quest_SetJournalStep(userObjID,861, 1);
				api_quest_SetQuestStep(userObjID,861, 1);
				npc_talk_index = "n710_scholar_hancock-1";

	end
	if npc_talk_index == "n710_scholar_hancock-1-b" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 2101, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 202101, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300489, 1);
	end
	if npc_talk_index == "n710_scholar_hancock-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8610, true);
				 api_quest_RewardQuestUser(userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8610, true);
				 api_quest_RewardQuestUser(userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8610, true);
				 api_quest_RewardQuestUser(userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8610, true);
				 api_quest_RewardQuestUser(userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8610, true);
				 api_quest_RewardQuestUser(userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8610, true);
				 api_quest_RewardQuestUser(userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8610, true);
				 api_quest_RewardQuestUser(userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8610, true);
				 api_quest_RewardQuestUser(userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8610, true);
				 api_quest_RewardQuestUser(userObjID, 8610, questID, 1);
			 end 
	end
	if npc_talk_index == "n710_scholar_hancock-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 862, 1);
					api_quest_SetQuestStep(userObjID, 862, 1);
					api_quest_SetJournalStep(userObjID, 862, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300489, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300489, api_quest_HasQuestItem(userObjID, 300489, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n710_scholar_hancock-1", "sq15_862_real_treasure3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_861_real_treasure2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 861);
	if qstep == 2 and CountIndex == 2101 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300489, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300489, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 202101 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300489, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300489, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300489 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function sq15_861_real_treasure2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 861);
	if qstep == 2 and CountIndex == 2101 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 202101 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300489 and Count >= TargetCount  then

	end
end

function sq15_861_real_treasure2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 861);
	local questID=861;
end

function sq15_861_real_treasure2_OnRemoteStart( userObjID, questID )
end

function sq15_861_real_treasure2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_861_real_treasure2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,861, 1);
				api_quest_SetJournalStep(userObjID,861, 1);
				api_quest_SetQuestStep(userObjID,861, 1);
				npc_talk_index = "n710_scholar_hancock-1";
end

</VillageServer>

<GameServer>
function sq15_861_real_treasure2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 710 then
		sq15_861_real_treasure2_OnTalk_n710_scholar_hancock( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n710_scholar_hancock--------------------------------------------------------------------------------
function sq15_861_real_treasure2_OnTalk_n710_scholar_hancock( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n710_scholar_hancock-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n710_scholar_hancock-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n710_scholar_hancock-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n710_scholar_hancock-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n710_scholar_hancock-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, false);
			 end 

	end
	if npc_talk_index == "n710_scholar_hancock-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,861, 1);
				api_quest_SetJournalStep( pRoom, userObjID,861, 1);
				api_quest_SetQuestStep( pRoom, userObjID,861, 1);
				npc_talk_index = "n710_scholar_hancock-1";

	end
	if npc_talk_index == "n710_scholar_hancock-1-b" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 2101, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 202101, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300489, 1);
	end
	if npc_talk_index == "n710_scholar_hancock-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8610, questID, 1);
			 end 
	end
	if npc_talk_index == "n710_scholar_hancock-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 862, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 862, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 862, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300489, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300489, api_quest_HasQuestItem( pRoom, userObjID, 300489, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n710_scholar_hancock-1", "sq15_862_real_treasure3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_861_real_treasure2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 861);
	if qstep == 2 and CountIndex == 2101 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300489, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300489, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 202101 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300489, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300489, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300489 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function sq15_861_real_treasure2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 861);
	if qstep == 2 and CountIndex == 2101 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 202101 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300489 and Count >= TargetCount  then

	end
end

function sq15_861_real_treasure2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 861);
	local questID=861;
end

function sq15_861_real_treasure2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_861_real_treasure2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_861_real_treasure2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,861, 1);
				api_quest_SetJournalStep( pRoom, userObjID,861, 1);
				api_quest_SetQuestStep( pRoom, userObjID,861, 1);
				npc_talk_index = "n710_scholar_hancock-1";
end

</GameServer>