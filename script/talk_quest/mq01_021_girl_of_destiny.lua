<VillageServer>

function mq01_021_girl_of_destiny_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1 then
		mq01_021_girl_of_destiny_OnTalk_n001_elder_harold(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 4 then
		mq01_021_girl_of_destiny_OnTalk_n004_guard_steave(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n001_elder_harold--------------------------------------------------------------------------------
function mq01_021_girl_of_destiny_OnTalk_n001_elder_harold(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n001_elder_harold-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n001_elder_harold-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n001_elder_harold-2-b" then 
	end
	if npc_talk_index == "n001_elder_harold-2-c" then 
	end
	if npc_talk_index == "n001_elder_harold-2-d" then 
	end
	if npc_talk_index == "n001_elder_harold-2-e" then 
	end
	if npc_talk_index == "n001_elder_harold-2-f" then 
	end
	if npc_talk_index == "n001_elder_harold-2-g" then 
	end
	if npc_talk_index == "n001_elder_harold-2-g" then 
	end
	if npc_talk_index == "n001_elder_harold-2-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 211, true);
				 api_quest_RewardQuestUser(userObjID, 211, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 212, true);
				 api_quest_RewardQuestUser(userObjID, 212, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 212, true);
				 api_quest_RewardQuestUser(userObjID, 212, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 212, true);
				 api_quest_RewardQuestUser(userObjID, 212, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 211, true);
				 api_quest_RewardQuestUser(userObjID, 211, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 211, true);
				 api_quest_RewardQuestUser(userObjID, 211, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 211, true);
				 api_quest_RewardQuestUser(userObjID, 211, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 211, true);
				 api_quest_RewardQuestUser(userObjID, 211, questID, 1);
			 end 
	end
	if npc_talk_index == "n001_elder_harold-2-i" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 201, 2);
					api_quest_SetQuestStep(userObjID, 201, 1);
					api_quest_SetJournalStep(userObjID, 201, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n001_elder_harold-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n001_elder_harold-1", "mq08_201_secret_melody.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n004_guard_steave--------------------------------------------------------------------------------
function mq01_021_girl_of_destiny_OnTalk_n004_guard_steave(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n004_guard_steave-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n004_guard_steave-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n004_guard_steave-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n004_guard_steave-accepting-acceptted" then
				api_quest_AddQuest(userObjID,21, 2);
				api_quest_SetJournalStep(userObjID,21, 1);
				api_quest_SetQuestStep(userObjID,21, 1);
				npc_talk_index = "n004_guard_steave-1";

	end
	if npc_talk_index == "n004_guard_steave-1-a" then 
				if api_user_GetUserLevel(userObjID) >= 9 then
									npc_talk_index = "n004_guard_steave-1-a";

				else
									npc_talk_index = "n004_guard_steave-1-e";

				end
	end
	if npc_talk_index == "n004_guard_steave-1-b" then 
	end
	if npc_talk_index == "n004_guard_steave-1-b" then 
	end
	if npc_talk_index == "n004_guard_steave-1-c" then 
	end
	if npc_talk_index == "n004_guard_steave-1-d" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300012, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300012, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				if api_quest_HasQuestItem(userObjID, 300010, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300010, api_quest_HasQuestItem(userObjID, 300010, 1));
				end
	end
	if npc_talk_index == "n004_guard_steave-1-f" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq01_021_girl_of_destiny_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 21);
end

function mq01_021_girl_of_destiny_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 21);
end

function mq01_021_girl_of_destiny_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 21);
	local questID=21;
end

function mq01_021_girl_of_destiny_OnRemoteStart( userObjID, questID )
end

function mq01_021_girl_of_destiny_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq01_021_girl_of_destiny_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,21, 2);
				api_quest_SetJournalStep(userObjID,21, 1);
				api_quest_SetQuestStep(userObjID,21, 1);
				npc_talk_index = "n004_guard_steave-1";
end

</VillageServer>

<GameServer>
function mq01_021_girl_of_destiny_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1 then
		mq01_021_girl_of_destiny_OnTalk_n001_elder_harold( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 4 then
		mq01_021_girl_of_destiny_OnTalk_n004_guard_steave( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n001_elder_harold--------------------------------------------------------------------------------
function mq01_021_girl_of_destiny_OnTalk_n001_elder_harold( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n001_elder_harold-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n001_elder_harold-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n001_elder_harold-2-b" then 
	end
	if npc_talk_index == "n001_elder_harold-2-c" then 
	end
	if npc_talk_index == "n001_elder_harold-2-d" then 
	end
	if npc_talk_index == "n001_elder_harold-2-e" then 
	end
	if npc_talk_index == "n001_elder_harold-2-f" then 
	end
	if npc_talk_index == "n001_elder_harold-2-g" then 
	end
	if npc_talk_index == "n001_elder_harold-2-g" then 
	end
	if npc_talk_index == "n001_elder_harold-2-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 211, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 211, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 212, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 212, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 212, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 212, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 212, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 212, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 211, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 211, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 211, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 211, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 211, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 211, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 211, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 211, questID, 1);
			 end 
	end
	if npc_talk_index == "n001_elder_harold-2-i" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 201, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 201, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 201, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n001_elder_harold-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n001_elder_harold-1", "mq08_201_secret_melody.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n004_guard_steave--------------------------------------------------------------------------------
function mq01_021_girl_of_destiny_OnTalk_n004_guard_steave( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n004_guard_steave-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n004_guard_steave-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n004_guard_steave-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n004_guard_steave-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,21, 2);
				api_quest_SetJournalStep( pRoom, userObjID,21, 1);
				api_quest_SetQuestStep( pRoom, userObjID,21, 1);
				npc_talk_index = "n004_guard_steave-1";

	end
	if npc_talk_index == "n004_guard_steave-1-a" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 9 then
									npc_talk_index = "n004_guard_steave-1-a";

				else
									npc_talk_index = "n004_guard_steave-1-e";

				end
	end
	if npc_talk_index == "n004_guard_steave-1-b" then 
	end
	if npc_talk_index == "n004_guard_steave-1-b" then 
	end
	if npc_talk_index == "n004_guard_steave-1-c" then 
	end
	if npc_talk_index == "n004_guard_steave-1-d" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300012, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300012, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				if api_quest_HasQuestItem( pRoom, userObjID, 300010, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300010, api_quest_HasQuestItem( pRoom, userObjID, 300010, 1));
				end
	end
	if npc_talk_index == "n004_guard_steave-1-f" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq01_021_girl_of_destiny_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 21);
end

function mq01_021_girl_of_destiny_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 21);
end

function mq01_021_girl_of_destiny_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 21);
	local questID=21;
end

function mq01_021_girl_of_destiny_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq01_021_girl_of_destiny_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq01_021_girl_of_destiny_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,21, 2);
				api_quest_SetJournalStep( pRoom, userObjID,21, 1);
				api_quest_SetQuestStep( pRoom, userObjID,21, 1);
				npc_talk_index = "n004_guard_steave-1";
end

</GameServer>