<VillageServer>

function sq11_469_saint_challenger2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 243 then
		sq11_469_saint_challenger2_OnTalk_n243_orc_oneeye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 98 then
		sq11_469_saint_challenger2_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n243_orc_oneeye--------------------------------------------------------------------------------
function sq11_469_saint_challenger2_OnTalk_n243_orc_oneeye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n243_orc_oneeye-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n243_orc_oneeye-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n243_orc_oneeye-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n243_orc_oneeye-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n243_orc_oneeye-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n243_orc_oneeye-2-b" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-c" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-d" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-e" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-f" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-g" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-h" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-i" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 973, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200973, 30000);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n243_orc_oneeye-4-b" then 
				api_quest_DelQuestItem(userObjID, 300092, 1);
	end
	if npc_talk_index == "n243_orc_oneeye-4-c" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-d" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-e" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-f" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-g" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-h" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-i" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-j" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-k" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-l" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-m" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-n" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-o" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-p" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4691, true);
				 api_quest_RewardQuestUser(userObjID, 4691, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4692, true);
				 api_quest_RewardQuestUser(userObjID, 4692, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4693, true);
				 api_quest_RewardQuestUser(userObjID, 4693, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4694, true);
				 api_quest_RewardQuestUser(userObjID, 4694, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4695, true);
				 api_quest_RewardQuestUser(userObjID, 4695, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4696, true);
				 api_quest_RewardQuestUser(userObjID, 4696, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4697, true);
				 api_quest_RewardQuestUser(userObjID, 4697, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4698, true);
				 api_quest_RewardQuestUser(userObjID, 4698, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4699, true);
				 api_quest_RewardQuestUser(userObjID, 4699, questID, 1);
			 end 
	end
	if npc_talk_index == "n243_orc_oneeye-4-q" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n243_orc_oneeye-4-r" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq11_469_saint_challenger2_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n098_warrior_master_lodrigo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-h" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4691, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4692, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4693, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4694, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4695, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4696, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4697, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4698, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4699, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-acceptted" then
				api_quest_AddQuest(userObjID,469, 1);
				api_quest_SetJournalStep(userObjID,469, 1);
				api_quest_SetQuestStep(userObjID,469, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_469_saint_challenger2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 469);
	if qstep == 3 and CountIndex == 973 then
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300092, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300092, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 3 and CountIndex == 200973 then
				api_quest_IncCounting(userObjID, 2, 973);

	end
end

function sq11_469_saint_challenger2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 469);
	if qstep == 3 and CountIndex == 973 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200973 and Count >= TargetCount  then

	end
end

function sq11_469_saint_challenger2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 469);
	local questID=469;
end

function sq11_469_saint_challenger2_OnRemoteStart( userObjID, questID )
end

function sq11_469_saint_challenger2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_469_saint_challenger2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,469, 1);
				api_quest_SetJournalStep(userObjID,469, 1);
				api_quest_SetQuestStep(userObjID,469, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
end

</VillageServer>

<GameServer>
function sq11_469_saint_challenger2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 243 then
		sq11_469_saint_challenger2_OnTalk_n243_orc_oneeye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 98 then
		sq11_469_saint_challenger2_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n243_orc_oneeye--------------------------------------------------------------------------------
function sq11_469_saint_challenger2_OnTalk_n243_orc_oneeye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n243_orc_oneeye-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n243_orc_oneeye-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n243_orc_oneeye-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n243_orc_oneeye-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n243_orc_oneeye-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n243_orc_oneeye-2-b" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-c" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-d" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-e" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-f" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-g" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-h" then 
	end
	if npc_talk_index == "n243_orc_oneeye-2-i" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 973, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200973, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n243_orc_oneeye-4-b" then 
				api_quest_DelQuestItem( pRoom, userObjID, 300092, 1);
	end
	if npc_talk_index == "n243_orc_oneeye-4-c" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-d" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-e" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-f" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-g" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-h" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-i" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-j" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-k" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-l" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-m" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-n" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-o" then 
	end
	if npc_talk_index == "n243_orc_oneeye-4-p" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4691, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4691, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4692, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4692, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4693, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4693, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4694, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4694, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4695, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4695, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4696, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4696, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4697, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4697, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4698, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4698, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4699, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4699, questID, 1);
			 end 
	end
	if npc_talk_index == "n243_orc_oneeye-4-q" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n243_orc_oneeye-4-r" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq11_469_saint_challenger2_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n098_warrior_master_lodrigo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-h" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4691, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4692, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4693, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4694, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4695, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4696, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4697, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4698, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4699, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,469, 1);
				api_quest_SetJournalStep( pRoom, userObjID,469, 1);
				api_quest_SetQuestStep( pRoom, userObjID,469, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_469_saint_challenger2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 469);
	if qstep == 3 and CountIndex == 973 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300092, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300092, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 3 and CountIndex == 200973 then
				api_quest_IncCounting( pRoom, userObjID, 2, 973);

	end
end

function sq11_469_saint_challenger2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 469);
	if qstep == 3 and CountIndex == 973 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200973 and Count >= TargetCount  then

	end
end

function sq11_469_saint_challenger2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 469);
	local questID=469;
end

function sq11_469_saint_challenger2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_469_saint_challenger2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_469_saint_challenger2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,469, 1);
				api_quest_SetJournalStep( pRoom, userObjID,469, 1);
				api_quest_SetQuestStep( pRoom, userObjID,469, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
end

</GameServer>