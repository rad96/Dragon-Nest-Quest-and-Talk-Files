<VillageServer>

function mqc15_9439_corrupt_people_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1572 then
		mqc15_9439_corrupt_people_OnTalk_n1572_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1573 then
		mqc15_9439_corrupt_people_OnTalk_n1573_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1578 then
		mqc15_9439_corrupt_people_OnTalk_n1578_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1572_gaharam--------------------------------------------------------------------------------
function mqc15_9439_corrupt_people_OnTalk_n1572_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1572_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1573_gaharam--------------------------------------------------------------------------------
function mqc15_9439_corrupt_people_OnTalk_n1573_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1573_gaharam-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1573_gaharam-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1573_gaharam-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1573_gaharam-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1573_gaharam-2-b" then 
	end
	if npc_talk_index == "n1573_gaharam-2-c" then 
	end
	if npc_talk_index == "n1573_gaharam-2-d" then 
	end
	if npc_talk_index == "n1573_gaharam-2-e" then 
	end
	if npc_talk_index == "n1573_gaharam-2-f" then 
	end
	if npc_talk_index == "n1573_gaharam-2-g" then 
	end
	if npc_talk_index == "n1573_gaharam-2-h" then 
	end
	if npc_talk_index == "n1573_gaharam-2-i" then 
	end
	if npc_talk_index == "n1573_gaharam-2-j" then 
	end
	if npc_talk_index == "n1573_gaharam-2-k" then 
	end
	if npc_talk_index == "n1573_gaharam-2-l" then 
	end
	if npc_talk_index == "n1573_gaharam-2-m" then 
	end
	if npc_talk_index == "n1573_gaharam-2-n" then 
	end
	if npc_talk_index == "n1573_gaharam-2-o" then 
	end
	if npc_talk_index == "n1573_gaharam-2-p" then 
	end
	if npc_talk_index == "n1573_gaharam-2-q" then 
	end
	if npc_talk_index == "n1573_gaharam-2-r" then 
	end
	if npc_talk_index == "n1573_gaharam-2-s" then 
	end
	if npc_talk_index == "n1573_gaharam-2-t" then 
	end
	if npc_talk_index == "n1573_gaharam-2-u" then 
	end
	if npc_talk_index == "n1573_gaharam-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n1573_gaharam-3-b" then 
	end
	if npc_talk_index == "n1573_gaharam-3-c" then 
	end
	if npc_talk_index == "n1573_gaharam-3-d" then 
	end
	if npc_talk_index == "n1573_gaharam-3-e" then 
	end
	if npc_talk_index == "n1573_gaharam-3-f" then 
	end
	if npc_talk_index == "n1573_gaharam-3-g" then 
	end
	if npc_talk_index == "n1573_gaharam-3-h" then 
	end
	if npc_talk_index == "n1573_gaharam-3-i" then 
	end
	if npc_talk_index == "n1573_gaharam-3-j" then 
	end
	if npc_talk_index == "n1573_gaharam-3-k" then 
	end
	if npc_talk_index == "n1573_gaharam-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1573_gaharam-5-b" then 
	end
	if npc_talk_index == "n1573_gaharam-5-c" then 
	end
	if npc_talk_index == "n1573_gaharam-5-d" then 
	end
	if npc_talk_index == "n1573_gaharam-5-e" then 
	end
	if npc_talk_index == "n1573_gaharam-5-f" then 
	end
	if npc_talk_index == "n1573_gaharam-5-g" then 
	end
	if npc_talk_index == "n1573_gaharam-5-h" then 
	end
	if npc_talk_index == "n1573_gaharam-5-i" then 
	end
	if npc_talk_index == "n1573_gaharam-5-j" then 
	end
	if npc_talk_index == "n1573_gaharam-5-k" then 
	end
	if npc_talk_index == "n1573_gaharam-5-l" then 
	end
	if npc_talk_index == "n1573_gaharam-5-m" then 
	end
	if npc_talk_index == "n1573_gaharam-5-n" then 
	end
	if npc_talk_index == "n1573_gaharam-5-o" then 
	end
	if npc_talk_index == "n1573_gaharam-5-p" then 
	end
	if npc_talk_index == "n1573_gaharam-5-q" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94390, true);
				 api_quest_RewardQuestUser(userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94390, true);
				 api_quest_RewardQuestUser(userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94390, true);
				 api_quest_RewardQuestUser(userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94390, true);
				 api_quest_RewardQuestUser(userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94390, true);
				 api_quest_RewardQuestUser(userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94390, true);
				 api_quest_RewardQuestUser(userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94390, true);
				 api_quest_RewardQuestUser(userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94390, true);
				 api_quest_RewardQuestUser(userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94390, true);
				 api_quest_RewardQuestUser(userObjID, 94390, questID, 1);
			 end 
	end
	if npc_talk_index == "n1573_gaharam-5-t" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9440, 2);
					api_quest_SetQuestStep(userObjID, 9440, 1);
					api_quest_SetJournalStep(userObjID, 9440, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1573_gaharam-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1573_gaharam-1", "mqc15_9440_the_man_who_wish_dark.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1578_for_memory--------------------------------------------------------------------------------
function mqc15_9439_corrupt_people_OnTalk_n1578_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1578_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1578_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1578_for_memory-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1578_for_memory-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9439, 2);
				api_quest_SetJournalStep(userObjID,9439, 1);
				api_quest_SetQuestStep(userObjID,9439, 1);
				npc_talk_index = "n1578_for_memory-1";

	end
	if npc_talk_index == "n1578_for_memory-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9439_corrupt_people_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9439);
end

function mqc15_9439_corrupt_people_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9439);
end

function mqc15_9439_corrupt_people_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9439);
	local questID=9439;
end

function mqc15_9439_corrupt_people_OnRemoteStart( userObjID, questID )
end

function mqc15_9439_corrupt_people_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9439_corrupt_people_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9439, 2);
				api_quest_SetJournalStep(userObjID,9439, 1);
				api_quest_SetQuestStep(userObjID,9439, 1);
				npc_talk_index = "n1578_for_memory-1";
end

</VillageServer>

<GameServer>
function mqc15_9439_corrupt_people_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1572 then
		mqc15_9439_corrupt_people_OnTalk_n1572_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1573 then
		mqc15_9439_corrupt_people_OnTalk_n1573_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1578 then
		mqc15_9439_corrupt_people_OnTalk_n1578_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1572_gaharam--------------------------------------------------------------------------------
function mqc15_9439_corrupt_people_OnTalk_n1572_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1572_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1573_gaharam--------------------------------------------------------------------------------
function mqc15_9439_corrupt_people_OnTalk_n1573_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1573_gaharam-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1573_gaharam-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1573_gaharam-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1573_gaharam-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1573_gaharam-2-b" then 
	end
	if npc_talk_index == "n1573_gaharam-2-c" then 
	end
	if npc_talk_index == "n1573_gaharam-2-d" then 
	end
	if npc_talk_index == "n1573_gaharam-2-e" then 
	end
	if npc_talk_index == "n1573_gaharam-2-f" then 
	end
	if npc_talk_index == "n1573_gaharam-2-g" then 
	end
	if npc_talk_index == "n1573_gaharam-2-h" then 
	end
	if npc_talk_index == "n1573_gaharam-2-i" then 
	end
	if npc_talk_index == "n1573_gaharam-2-j" then 
	end
	if npc_talk_index == "n1573_gaharam-2-k" then 
	end
	if npc_talk_index == "n1573_gaharam-2-l" then 
	end
	if npc_talk_index == "n1573_gaharam-2-m" then 
	end
	if npc_talk_index == "n1573_gaharam-2-n" then 
	end
	if npc_talk_index == "n1573_gaharam-2-o" then 
	end
	if npc_talk_index == "n1573_gaharam-2-p" then 
	end
	if npc_talk_index == "n1573_gaharam-2-q" then 
	end
	if npc_talk_index == "n1573_gaharam-2-r" then 
	end
	if npc_talk_index == "n1573_gaharam-2-s" then 
	end
	if npc_talk_index == "n1573_gaharam-2-t" then 
	end
	if npc_talk_index == "n1573_gaharam-2-u" then 
	end
	if npc_talk_index == "n1573_gaharam-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n1573_gaharam-3-b" then 
	end
	if npc_talk_index == "n1573_gaharam-3-c" then 
	end
	if npc_talk_index == "n1573_gaharam-3-d" then 
	end
	if npc_talk_index == "n1573_gaharam-3-e" then 
	end
	if npc_talk_index == "n1573_gaharam-3-f" then 
	end
	if npc_talk_index == "n1573_gaharam-3-g" then 
	end
	if npc_talk_index == "n1573_gaharam-3-h" then 
	end
	if npc_talk_index == "n1573_gaharam-3-i" then 
	end
	if npc_talk_index == "n1573_gaharam-3-j" then 
	end
	if npc_talk_index == "n1573_gaharam-3-k" then 
	end
	if npc_talk_index == "n1573_gaharam-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1573_gaharam-5-b" then 
	end
	if npc_talk_index == "n1573_gaharam-5-c" then 
	end
	if npc_talk_index == "n1573_gaharam-5-d" then 
	end
	if npc_talk_index == "n1573_gaharam-5-e" then 
	end
	if npc_talk_index == "n1573_gaharam-5-f" then 
	end
	if npc_talk_index == "n1573_gaharam-5-g" then 
	end
	if npc_talk_index == "n1573_gaharam-5-h" then 
	end
	if npc_talk_index == "n1573_gaharam-5-i" then 
	end
	if npc_talk_index == "n1573_gaharam-5-j" then 
	end
	if npc_talk_index == "n1573_gaharam-5-k" then 
	end
	if npc_talk_index == "n1573_gaharam-5-l" then 
	end
	if npc_talk_index == "n1573_gaharam-5-m" then 
	end
	if npc_talk_index == "n1573_gaharam-5-n" then 
	end
	if npc_talk_index == "n1573_gaharam-5-o" then 
	end
	if npc_talk_index == "n1573_gaharam-5-p" then 
	end
	if npc_talk_index == "n1573_gaharam-5-q" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94390, questID, 1);
			 end 
	end
	if npc_talk_index == "n1573_gaharam-5-t" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9440, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9440, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9440, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1573_gaharam-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1573_gaharam-1", "mqc15_9440_the_man_who_wish_dark.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1578_for_memory--------------------------------------------------------------------------------
function mqc15_9439_corrupt_people_OnTalk_n1578_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1578_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1578_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1578_for_memory-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1578_for_memory-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9439, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9439, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9439, 1);
				npc_talk_index = "n1578_for_memory-1";

	end
	if npc_talk_index == "n1578_for_memory-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9439_corrupt_people_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9439);
end

function mqc15_9439_corrupt_people_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9439);
end

function mqc15_9439_corrupt_people_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9439);
	local questID=9439;
end

function mqc15_9439_corrupt_people_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9439_corrupt_people_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9439_corrupt_people_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9439, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9439, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9439, 1);
				npc_talk_index = "n1578_for_memory-1";
end

</GameServer>