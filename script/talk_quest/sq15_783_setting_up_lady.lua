<VillageServer>

function sq15_783_setting_up_lady_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 722 then
		sq15_783_setting_up_lady_OnTalk_n722_cat_mewmew(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n722_cat_mewmew--------------------------------------------------------------------------------
function sq15_783_setting_up_lady_OnTalk_n722_cat_mewmew(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n722_cat_mewmew-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n722_cat_mewmew-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n722_cat_mewmew-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n722_cat_mewmew-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7830, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7830, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7830, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7830, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7830, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7830, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7830, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7830, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7830, false);
			 end 

	end
	if npc_talk_index == "n722_cat_mewmew-accepting-acceptted" then
				api_quest_AddQuest(userObjID,783, 1);
				api_quest_SetJournalStep(userObjID,783, 1);
				api_quest_SetQuestStep(userObjID,783, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1412, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201412, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300414, 3);
				npc_talk_index = "n722_cat_mewmew-1";

	end
	if npc_talk_index == "n722_cat_mewmew-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7830, true);
				 api_quest_RewardQuestUser(userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7830, true);
				 api_quest_RewardQuestUser(userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7830, true);
				 api_quest_RewardQuestUser(userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7830, true);
				 api_quest_RewardQuestUser(userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7830, true);
				 api_quest_RewardQuestUser(userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7830, true);
				 api_quest_RewardQuestUser(userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7830, true);
				 api_quest_RewardQuestUser(userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7830, true);
				 api_quest_RewardQuestUser(userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7830, true);
				 api_quest_RewardQuestUser(userObjID, 7830, questID, 1);
			 end 
	end
	if npc_talk_index == "n722_cat_mewmew-2-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300414, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300414, api_quest_HasQuestItem(userObjID, 300414, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_783_setting_up_lady_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 783);
	if qstep == 1 and CountIndex == 1412 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300414, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300414, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 1413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300414, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300414, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300414 then

	end
	if qstep == 1 and CountIndex == 201412 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300414, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300414, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300414, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300414, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_783_setting_up_lady_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 783);
	if qstep == 1 and CountIndex == 1412 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1413 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300414 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201412 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201413 and Count >= TargetCount  then

	end
end

function sq15_783_setting_up_lady_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 783);
	local questID=783;
end

function sq15_783_setting_up_lady_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1412, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201412, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300414, 3);
end

function sq15_783_setting_up_lady_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_783_setting_up_lady_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,783, 1);
				api_quest_SetJournalStep(userObjID,783, 1);
				api_quest_SetQuestStep(userObjID,783, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1412, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201412, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300414, 3);
				npc_talk_index = "n722_cat_mewmew-1";
end

</VillageServer>

<GameServer>
function sq15_783_setting_up_lady_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 722 then
		sq15_783_setting_up_lady_OnTalk_n722_cat_mewmew( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n722_cat_mewmew--------------------------------------------------------------------------------
function sq15_783_setting_up_lady_OnTalk_n722_cat_mewmew( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n722_cat_mewmew-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n722_cat_mewmew-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n722_cat_mewmew-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n722_cat_mewmew-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, false);
			 end 

	end
	if npc_talk_index == "n722_cat_mewmew-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,783, 1);
				api_quest_SetJournalStep( pRoom, userObjID,783, 1);
				api_quest_SetQuestStep( pRoom, userObjID,783, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1412, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201412, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300414, 3);
				npc_talk_index = "n722_cat_mewmew-1";

	end
	if npc_talk_index == "n722_cat_mewmew-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7830, questID, 1);
			 end 
	end
	if npc_talk_index == "n722_cat_mewmew-2-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300414, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300414, api_quest_HasQuestItem( pRoom, userObjID, 300414, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_783_setting_up_lady_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 783);
	if qstep == 1 and CountIndex == 1412 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300414, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300414, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 1413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300414, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300414, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300414 then

	end
	if qstep == 1 and CountIndex == 201412 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300414, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300414, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300414, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300414, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_783_setting_up_lady_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 783);
	if qstep == 1 and CountIndex == 1412 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1413 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300414 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201412 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201413 and Count >= TargetCount  then

	end
end

function sq15_783_setting_up_lady_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 783);
	local questID=783;
end

function sq15_783_setting_up_lady_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1412, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201412, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300414, 3);
end

function sq15_783_setting_up_lady_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_783_setting_up_lady_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,783, 1);
				api_quest_SetJournalStep( pRoom, userObjID,783, 1);
				api_quest_SetQuestStep( pRoom, userObjID,783, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1412, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201412, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300414, 3);
				npc_talk_index = "n722_cat_mewmew-1";
end

</GameServer>