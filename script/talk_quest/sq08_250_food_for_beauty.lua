<VillageServer>

function sq08_250_food_for_beauty_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 164 then
		sq08_250_food_for_beauty_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		sq08_250_food_for_beauty_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function sq08_250_food_for_beauty_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n164_argenta_wounded-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n164_argenta_wounded-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n164_argenta_wounded-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-3-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-d" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-e" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-f" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-g" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-h" then 
				api_quest_SetQuestStep(userObjID, questID,4);

				if api_quest_HasQuestItem(userObjID, 300113, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300113, api_quest_HasQuestItem(userObjID, 300113, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300003, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300003, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function sq08_250_food_for_beauty_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest(userObjID,250, 1);
				api_quest_SetJournalStep(userObjID,250, 1);
				api_quest_SetQuestStep(userObjID,250, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-b" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300113, 5);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-4-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-4-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2500, true);
				 api_quest_RewardQuestUser(userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2500, true);
				 api_quest_RewardQuestUser(userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2500, true);
				 api_quest_RewardQuestUser(userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2500, true);
				 api_quest_RewardQuestUser(userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2500, true);
				 api_quest_RewardQuestUser(userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2500, true);
				 api_quest_RewardQuestUser(userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2500, true);
				 api_quest_RewardQuestUser(userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2500, true);
				 api_quest_RewardQuestUser(userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2500, true);
				 api_quest_RewardQuestUser(userObjID, 2500, questID, 1);
			 end 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-4-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 249, 1);
					api_quest_SetQuestStep(userObjID, 249, 1);
					api_quest_SetJournalStep(userObjID, 249, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n024_adventurer_guildmaster_decud-1", "sq08_249_meet_david.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_250_food_for_beauty_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 250);
	if qstep == 2 and CountIndex == 300113 then

	end
end

function sq08_250_food_for_beauty_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 250);
	if qstep == 2 and CountIndex == 300113 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function sq08_250_food_for_beauty_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 250);
	local questID=250;
end

function sq08_250_food_for_beauty_OnRemoteStart( userObjID, questID )
end

function sq08_250_food_for_beauty_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_250_food_for_beauty_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,250, 1);
				api_quest_SetJournalStep(userObjID,250, 1);
				api_quest_SetQuestStep(userObjID,250, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
end

</VillageServer>

<GameServer>
function sq08_250_food_for_beauty_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 164 then
		sq08_250_food_for_beauty_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		sq08_250_food_for_beauty_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function sq08_250_food_for_beauty_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n164_argenta_wounded-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n164_argenta_wounded-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n164_argenta_wounded-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-3-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-d" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-e" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-f" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-g" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-h" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);

				if api_quest_HasQuestItem( pRoom, userObjID, 300113, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300113, api_quest_HasQuestItem( pRoom, userObjID, 300113, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300003, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300003, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function sq08_250_food_for_beauty_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,250, 1);
				api_quest_SetJournalStep( pRoom, userObjID,250, 1);
				api_quest_SetQuestStep( pRoom, userObjID,250, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-b" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300113, 5);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-4-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-4-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2500, questID, 1);
			 end 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-4-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 249, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 249, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 249, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n024_adventurer_guildmaster_decud-1", "sq08_249_meet_david.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_250_food_for_beauty_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 250);
	if qstep == 2 and CountIndex == 300113 then

	end
end

function sq08_250_food_for_beauty_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 250);
	if qstep == 2 and CountIndex == 300113 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function sq08_250_food_for_beauty_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 250);
	local questID=250;
end

function sq08_250_food_for_beauty_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_250_food_for_beauty_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_250_food_for_beauty_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,250, 1);
				api_quest_SetJournalStep( pRoom, userObjID,250, 1);
				api_quest_SetQuestStep( pRoom, userObjID,250, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
end

</GameServer>