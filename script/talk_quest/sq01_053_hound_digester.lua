<VillageServer>

function sq01_053_hound_digester_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 2 then
		sq01_053_hound_digester_OnTalk_n002_trader_dorin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 3 then
		sq01_053_hound_digester_OnTalk_n003_trader_dolis(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n002_trader_dorin--------------------------------------------------------------------------------
function sq01_053_hound_digester_OnTalk_n002_trader_dorin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n002_trader_dorin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n002_trader_dorin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n002_trader_dorin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n002_trader_dorin-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 531, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 532, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 532, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 532, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 531, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 536, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 537, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 531, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 531, false);
			 end 

	end
	if npc_talk_index == "n002_trader_dorin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,53, 1);
				api_quest_SetJournalStep(userObjID,53, 1);
				api_quest_SetQuestStep(userObjID,53, 1);
				npc_talk_index = "n002_trader_dorin-1";

	end
	if npc_talk_index == "n002_trader_dorin-4-b" then 
	end
	if npc_talk_index == "n002_trader_dorin-4-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 531, true);
				 api_quest_RewardQuestUser(userObjID, 531, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 532, true);
				 api_quest_RewardQuestUser(userObjID, 532, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 532, true);
				 api_quest_RewardQuestUser(userObjID, 532, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 532, true);
				 api_quest_RewardQuestUser(userObjID, 532, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 531, true);
				 api_quest_RewardQuestUser(userObjID, 531, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 536, true);
				 api_quest_RewardQuestUser(userObjID, 536, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 537, true);
				 api_quest_RewardQuestUser(userObjID, 537, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 531, true);
				 api_quest_RewardQuestUser(userObjID, 531, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 531, true);
				 api_quest_RewardQuestUser(userObjID, 531, questID, 1);
			 end 
	end
	if npc_talk_index == "n002_trader_dorin-4-d" then 

				if api_quest_HasQuestItem(userObjID, 300085, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300085, api_quest_HasQuestItem(userObjID, 300085, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n003_trader_dolis--------------------------------------------------------------------------------
function sq01_053_hound_digester_OnTalk_n003_trader_dolis(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n003_trader_dolis-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n003_trader_dolis-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n003_trader_dolis-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n003_trader_dolis-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n003_trader_dolis-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n003_trader_dolis-1-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-c" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-d" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-e" then 
	end
	if npc_talk_index == "n003_trader_dolis-2" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 148, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300084, 1);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n003_trader_dolis-3-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-4" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300085, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300085, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				if api_quest_HasQuestItem(userObjID, 300084, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300084, api_quest_HasQuestItem(userObjID, 300084, 1));
				end
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq01_053_hound_digester_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 53);
	if qstep == 2 and CountIndex == 300084 then

	end
	if qstep == 2 and CountIndex == 148 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300084, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300084, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq01_053_hound_digester_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 53);
	if qstep == 2 and CountIndex == 300084 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 148 and Count >= TargetCount  then

	end
end

function sq01_053_hound_digester_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 53);
	local questID=53;
end

function sq01_053_hound_digester_OnRemoteStart( userObjID, questID )
end

function sq01_053_hound_digester_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq01_053_hound_digester_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,53, 1);
				api_quest_SetJournalStep(userObjID,53, 1);
				api_quest_SetQuestStep(userObjID,53, 1);
				npc_talk_index = "n002_trader_dorin-1";
end

</VillageServer>

<GameServer>
function sq01_053_hound_digester_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 2 then
		sq01_053_hound_digester_OnTalk_n002_trader_dorin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 3 then
		sq01_053_hound_digester_OnTalk_n003_trader_dolis( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n002_trader_dorin--------------------------------------------------------------------------------
function sq01_053_hound_digester_OnTalk_n002_trader_dorin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n002_trader_dorin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n002_trader_dorin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n002_trader_dorin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n002_trader_dorin-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 531, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 532, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 532, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 532, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 531, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 536, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 537, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 531, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 531, false);
			 end 

	end
	if npc_talk_index == "n002_trader_dorin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,53, 1);
				api_quest_SetJournalStep( pRoom, userObjID,53, 1);
				api_quest_SetQuestStep( pRoom, userObjID,53, 1);
				npc_talk_index = "n002_trader_dorin-1";

	end
	if npc_talk_index == "n002_trader_dorin-4-b" then 
	end
	if npc_talk_index == "n002_trader_dorin-4-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 531, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 531, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 532, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 532, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 532, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 532, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 532, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 532, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 531, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 531, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 536, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 536, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 537, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 537, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 531, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 531, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 531, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 531, questID, 1);
			 end 
	end
	if npc_talk_index == "n002_trader_dorin-4-d" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300085, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300085, api_quest_HasQuestItem( pRoom, userObjID, 300085, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n003_trader_dolis--------------------------------------------------------------------------------
function sq01_053_hound_digester_OnTalk_n003_trader_dolis( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n003_trader_dolis-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n003_trader_dolis-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n003_trader_dolis-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n003_trader_dolis-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n003_trader_dolis-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n003_trader_dolis-1-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-c" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-d" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-e" then 
	end
	if npc_talk_index == "n003_trader_dolis-2" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 148, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300084, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n003_trader_dolis-3-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-4" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300085, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300085, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300084, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300084, api_quest_HasQuestItem( pRoom, userObjID, 300084, 1));
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq01_053_hound_digester_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 53);
	if qstep == 2 and CountIndex == 300084 then

	end
	if qstep == 2 and CountIndex == 148 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300084, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300084, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq01_053_hound_digester_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 53);
	if qstep == 2 and CountIndex == 300084 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 148 and Count >= TargetCount  then

	end
end

function sq01_053_hound_digester_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 53);
	local questID=53;
end

function sq01_053_hound_digester_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq01_053_hound_digester_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq01_053_hound_digester_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,53, 1);
				api_quest_SetJournalStep( pRoom, userObjID,53, 1);
				api_quest_SetQuestStep( pRoom, userObjID,53, 1);
				npc_talk_index = "n002_trader_dorin-1";
end

</GameServer>