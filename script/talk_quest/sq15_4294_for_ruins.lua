<VillageServer>

function sq15_4294_for_ruins_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 36 then
		sq15_4294_for_ruins_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		sq15_4294_for_ruins_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq15_4294_for_ruins_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 42940, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 42940, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 42940, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 42940, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 42940, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 42940, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 42940, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 42940, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 42940, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4294, 1);
				api_quest_SetJournalStep(userObjID,4294, 1);
				api_quest_SetQuestStep(userObjID,4294, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 600932, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 600933, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700932, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 700933, 30000);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 400374, 10);

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 42940, true);
				 api_quest_RewardQuestUser(userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 42940, true);
				 api_quest_RewardQuestUser(userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 42940, true);
				 api_quest_RewardQuestUser(userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 42940, true);
				 api_quest_RewardQuestUser(userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 42940, true);
				 api_quest_RewardQuestUser(userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 42940, true);
				 api_quest_RewardQuestUser(userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 42940, true);
				 api_quest_RewardQuestUser(userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 42940, true);
				 api_quest_RewardQuestUser(userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 42940, true);
				 api_quest_RewardQuestUser(userObjID, 42940, questID, 1);
			 end 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function sq15_4294_for_ruins_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4294_for_ruins_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4294);
	if qstep == 1 and CountIndex == 400374 then

	end
	if qstep == 1 and CountIndex == 600932 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400374, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400374, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600933 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400374, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400374, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700932 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400374, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400374, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700933 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400374, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400374, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_4294_for_ruins_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4294);
	if qstep == 1 and CountIndex == 400374 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 600932 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600933 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700932 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700933 and Count >= TargetCount  then

	end
end

function sq15_4294_for_ruins_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4294);
	local questID=4294;
end

function sq15_4294_for_ruins_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 600932, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 600933, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700932, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 700933, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 400374, 10);
end

function sq15_4294_for_ruins_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4294_for_ruins_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4294, 1);
				api_quest_SetJournalStep(userObjID,4294, 1);
				api_quest_SetQuestStep(userObjID,4294, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 600932, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 600933, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700932, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 700933, 30000);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 400374, 10);
end

</VillageServer>

<GameServer>
function sq15_4294_for_ruins_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 36 then
		sq15_4294_for_ruins_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		sq15_4294_for_ruins_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq15_4294_for_ruins_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4294, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4294, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4294, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 600932, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 600933, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700932, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 700933, 30000);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 400374, 10);

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42940, questID, 1);
			 end 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function sq15_4294_for_ruins_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4294_for_ruins_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4294);
	if qstep == 1 and CountIndex == 400374 then

	end
	if qstep == 1 and CountIndex == 600932 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400374, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400374, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600933 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400374, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400374, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700932 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400374, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400374, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700933 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400374, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400374, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_4294_for_ruins_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4294);
	if qstep == 1 and CountIndex == 400374 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 600932 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600933 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700932 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700933 and Count >= TargetCount  then

	end
end

function sq15_4294_for_ruins_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4294);
	local questID=4294;
end

function sq15_4294_for_ruins_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 600932, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 600933, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700932, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 700933, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 400374, 10);
end

function sq15_4294_for_ruins_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4294_for_ruins_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4294, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4294, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4294, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 600932, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 600933, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700932, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 700933, 30000);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 400374, 10);
end

</GameServer>