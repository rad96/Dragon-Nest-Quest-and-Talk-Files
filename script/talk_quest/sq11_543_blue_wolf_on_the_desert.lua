<VillageServer>

function sq11_543_blue_wolf_on_the_desert_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 70 then
		sq11_543_blue_wolf_on_the_desert_OnTalk_n070_plane_ticketer_sorene(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 98 then
		sq11_543_blue_wolf_on_the_desert_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n070_plane_ticketer_sorene--------------------------------------------------------------------------------
function sq11_543_blue_wolf_on_the_desert_OnTalk_n070_plane_ticketer_sorene(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n070_plane_ticketer_sorene-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n070_plane_ticketer_sorene-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n070_plane_ticketer_sorene-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n070_plane_ticketer_sorene-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5430, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5430, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5430, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5430, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5430, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5430, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5430, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5430, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5430, false);
			 end 

	end
	if npc_talk_index == "n070_plane_ticketer_sorene-accepting-acceptted" then
				api_quest_AddQuest(userObjID,543, 1);
				api_quest_SetJournalStep(userObjID,543, 1);
				api_quest_SetQuestStep(userObjID,543, 1);
				npc_talk_index = "n070_plane_ticketer_sorene-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400139, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400139, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq11_543_blue_wolf_on_the_desert_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n098_warrior_master_lodrigo-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n098_warrior_master_lodrigo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n098_warrior_master_lodrigo-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-1-b" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-c" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-d" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-e" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-f" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-g" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-h" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-b" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-c" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-d" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-e" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5430, true);
				 api_quest_RewardQuestUser(userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5430, true);
				 api_quest_RewardQuestUser(userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5430, true);
				 api_quest_RewardQuestUser(userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5430, true);
				 api_quest_RewardQuestUser(userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5430, true);
				 api_quest_RewardQuestUser(userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5430, true);
				 api_quest_RewardQuestUser(userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5430, true);
				 api_quest_RewardQuestUser(userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5430, true);
				 api_quest_RewardQuestUser(userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5430, true);
				 api_quest_RewardQuestUser(userObjID, 5430, questID, 1);
			 end 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400139, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400139, api_quest_HasQuestItem(userObjID, 400139, 1));
				end
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_543_blue_wolf_on_the_desert_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 543);
end

function sq11_543_blue_wolf_on_the_desert_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 543);
end

function sq11_543_blue_wolf_on_the_desert_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 543);
	local questID=543;
end

function sq11_543_blue_wolf_on_the_desert_OnRemoteStart( userObjID, questID )
end

function sq11_543_blue_wolf_on_the_desert_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_543_blue_wolf_on_the_desert_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,543, 1);
				api_quest_SetJournalStep(userObjID,543, 1);
				api_quest_SetQuestStep(userObjID,543, 1);
				npc_talk_index = "n070_plane_ticketer_sorene-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400139, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400139, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
end

</VillageServer>

<GameServer>
function sq11_543_blue_wolf_on_the_desert_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 70 then
		sq11_543_blue_wolf_on_the_desert_OnTalk_n070_plane_ticketer_sorene( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 98 then
		sq11_543_blue_wolf_on_the_desert_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n070_plane_ticketer_sorene--------------------------------------------------------------------------------
function sq11_543_blue_wolf_on_the_desert_OnTalk_n070_plane_ticketer_sorene( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n070_plane_ticketer_sorene-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n070_plane_ticketer_sorene-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n070_plane_ticketer_sorene-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n070_plane_ticketer_sorene-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, false);
			 end 

	end
	if npc_talk_index == "n070_plane_ticketer_sorene-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,543, 1);
				api_quest_SetJournalStep( pRoom, userObjID,543, 1);
				api_quest_SetQuestStep( pRoom, userObjID,543, 1);
				npc_talk_index = "n070_plane_ticketer_sorene-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400139, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400139, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq11_543_blue_wolf_on_the_desert_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n098_warrior_master_lodrigo-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n098_warrior_master_lodrigo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n098_warrior_master_lodrigo-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-1-b" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-c" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-d" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-e" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-f" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-g" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-h" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-b" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-c" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-d" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-e" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5430, questID, 1);
			 end 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400139, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400139, api_quest_HasQuestItem( pRoom, userObjID, 400139, 1));
				end
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_543_blue_wolf_on_the_desert_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 543);
end

function sq11_543_blue_wolf_on_the_desert_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 543);
end

function sq11_543_blue_wolf_on_the_desert_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 543);
	local questID=543;
end

function sq11_543_blue_wolf_on_the_desert_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_543_blue_wolf_on_the_desert_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_543_blue_wolf_on_the_desert_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,543, 1);
				api_quest_SetJournalStep( pRoom, userObjID,543, 1);
				api_quest_SetQuestStep( pRoom, userObjID,543, 1);
				npc_talk_index = "n070_plane_ticketer_sorene-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400139, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400139, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
end

</GameServer>