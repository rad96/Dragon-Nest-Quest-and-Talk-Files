<VillageServer>

function sq11_664_grand_open_farm_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 100 then
		sq11_664_grand_open_farm_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 351 then
		sq11_664_grand_open_farm_OnTalk_n351_farm(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq11_664_grand_open_farm_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6640, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6640, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6640, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6640, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6640, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6640, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6640, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6640, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6640, false);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest(userObjID,664, 1);
				api_quest_SetJournalStep(userObjID,664, 1);
				api_quest_SetQuestStep(userObjID,664, 1);
				npc_talk_index = "n100_event_ilyn-1";

	end
	if npc_talk_index == "n100_event_ilyn-1-b" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-c" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-d" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-e" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-f" then 
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n351_farm--------------------------------------------------------------------------------
function sq11_664_grand_open_farm_OnTalk_n351_farm(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n351_farm-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n351_farm-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n351_farm-1-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6640, true);
				 api_quest_RewardQuestUser(userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6640, true);
				 api_quest_RewardQuestUser(userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6640, true);
				 api_quest_RewardQuestUser(userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6640, true);
				 api_quest_RewardQuestUser(userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6640, true);
				 api_quest_RewardQuestUser(userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6640, true);
				 api_quest_RewardQuestUser(userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6640, true);
				 api_quest_RewardQuestUser(userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6640, true);
				 api_quest_RewardQuestUser(userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6640, true);
				 api_quest_RewardQuestUser(userObjID, 6640, questID, 1);
			 end 
	end
	if npc_talk_index == "n351_farm-1-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n351_farm-1-quest_end" then 
				api_npc_NextScript(userObjID, npcObjID, "001", "n351_farm.xml");
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_664_grand_open_farm_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 664);
end

function sq11_664_grand_open_farm_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 664);
end

function sq11_664_grand_open_farm_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 664);
	local questID=664;
end

function sq11_664_grand_open_farm_OnRemoteStart( userObjID, questID )
end

function sq11_664_grand_open_farm_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_664_grand_open_farm_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,664, 1);
				api_quest_SetJournalStep(userObjID,664, 1);
				api_quest_SetQuestStep(userObjID,664, 1);
				npc_talk_index = "n100_event_ilyn-1";
end

</VillageServer>

<GameServer>
function sq11_664_grand_open_farm_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 100 then
		sq11_664_grand_open_farm_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 351 then
		sq11_664_grand_open_farm_OnTalk_n351_farm( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq11_664_grand_open_farm_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, false);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,664, 1);
				api_quest_SetJournalStep( pRoom, userObjID,664, 1);
				api_quest_SetQuestStep( pRoom, userObjID,664, 1);
				npc_talk_index = "n100_event_ilyn-1";

	end
	if npc_talk_index == "n100_event_ilyn-1-b" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-c" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-d" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-e" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-f" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n351_farm--------------------------------------------------------------------------------
function sq11_664_grand_open_farm_OnTalk_n351_farm( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n351_farm-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n351_farm-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n351_farm-1-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6640, questID, 1);
			 end 
	end
	if npc_talk_index == "n351_farm-1-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n351_farm-1-quest_end" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "001", "n351_farm.xml");
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_664_grand_open_farm_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 664);
end

function sq11_664_grand_open_farm_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 664);
end

function sq11_664_grand_open_farm_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 664);
	local questID=664;
end

function sq11_664_grand_open_farm_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_664_grand_open_farm_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_664_grand_open_farm_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,664, 1);
				api_quest_SetJournalStep( pRoom, userObjID,664, 1);
				api_quest_SetQuestStep( pRoom, userObjID,664, 1);
				npc_talk_index = "n100_event_ilyn-1";
end

</GameServer>