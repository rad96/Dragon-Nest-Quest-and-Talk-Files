<VillageServer>

function sq15_814_father_road_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 147 then
		sq15_814_father_road_OnTalk_n147_mailbox(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 711 then
		sq15_814_father_road_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n147_mailbox--------------------------------------------------------------------------------
function sq15_814_father_road_OnTalk_n147_mailbox(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n147_mailbox-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n147_mailbox-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n147_mailbox-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n147_mailbox-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8140, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8140, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8140, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8140, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8140, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8140, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8140, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8140, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8140, false);
			 end 

	end
	if npc_talk_index == "n147_mailbox-accepting-acceptted" then
				api_quest_AddQuest(userObjID,814, 1);
				api_quest_SetJournalStep(userObjID,814, 1);
				api_quest_SetQuestStep(userObjID,814, 1);
				npc_talk_index = "n147_mailbox-1";

	end
	if npc_talk_index == "n147_mailbox-1-b" then 
	end
	if npc_talk_index == "n147_mailbox-1-c" then 
	end
	if npc_talk_index == "n147_mailbox-1-d" then 
	end
	if npc_talk_index == "n147_mailbox-1-e" then 
	end
	if npc_talk_index == "n147_mailbox-1-f" then 
	end
	if npc_talk_index == "n147_mailbox-1-g" then 
	end
	if npc_talk_index == "n147_mailbox-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n711_warrior_fedro--------------------------------------------------------------------------------
function sq15_814_father_road_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n711_warrior_fedro-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n711_warrior_fedro-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-2-b" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-c" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-d" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-f" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-g" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-h" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-i" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-j" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-k" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-l" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8140, true);
				 api_quest_RewardQuestUser(userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8140, true);
				 api_quest_RewardQuestUser(userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8140, true);
				 api_quest_RewardQuestUser(userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8140, true);
				 api_quest_RewardQuestUser(userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8140, true);
				 api_quest_RewardQuestUser(userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8140, true);
				 api_quest_RewardQuestUser(userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8140, true);
				 api_quest_RewardQuestUser(userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8140, true);
				 api_quest_RewardQuestUser(userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8140, true);
				 api_quest_RewardQuestUser(userObjID, 8140, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-2-m" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_814_father_road_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 814);
end

function sq15_814_father_road_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 814);
end

function sq15_814_father_road_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 814);
	local questID=814;
end

function sq15_814_father_road_OnRemoteStart( userObjID, questID )
end

function sq15_814_father_road_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_814_father_road_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,814, 1);
				api_quest_SetJournalStep(userObjID,814, 1);
				api_quest_SetQuestStep(userObjID,814, 1);
				npc_talk_index = "n147_mailbox-1";
end

</VillageServer>

<GameServer>
function sq15_814_father_road_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 147 then
		sq15_814_father_road_OnTalk_n147_mailbox( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 711 then
		sq15_814_father_road_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n147_mailbox--------------------------------------------------------------------------------
function sq15_814_father_road_OnTalk_n147_mailbox( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n147_mailbox-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n147_mailbox-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n147_mailbox-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n147_mailbox-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, false);
			 end 

	end
	if npc_talk_index == "n147_mailbox-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,814, 1);
				api_quest_SetJournalStep( pRoom, userObjID,814, 1);
				api_quest_SetQuestStep( pRoom, userObjID,814, 1);
				npc_talk_index = "n147_mailbox-1";

	end
	if npc_talk_index == "n147_mailbox-1-b" then 
	end
	if npc_talk_index == "n147_mailbox-1-c" then 
	end
	if npc_talk_index == "n147_mailbox-1-d" then 
	end
	if npc_talk_index == "n147_mailbox-1-e" then 
	end
	if npc_talk_index == "n147_mailbox-1-f" then 
	end
	if npc_talk_index == "n147_mailbox-1-g" then 
	end
	if npc_talk_index == "n147_mailbox-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n711_warrior_fedro--------------------------------------------------------------------------------
function sq15_814_father_road_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n711_warrior_fedro-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n711_warrior_fedro-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-2-b" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-c" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-d" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-f" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-g" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-h" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-i" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-j" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-k" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-l" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8140, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-2-m" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_814_father_road_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 814);
end

function sq15_814_father_road_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 814);
end

function sq15_814_father_road_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 814);
	local questID=814;
end

function sq15_814_father_road_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_814_father_road_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_814_father_road_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,814, 1);
				api_quest_SetJournalStep( pRoom, userObjID,814, 1);
				api_quest_SetQuestStep( pRoom, userObjID,814, 1);
				npc_talk_index = "n147_mailbox-1";
end

</GameServer>