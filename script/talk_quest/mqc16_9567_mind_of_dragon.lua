<VillageServer>

function mqc16_9567_mind_of_dragon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1782 then
		mqc16_9567_mind_of_dragon_OnTalk_n1782_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1789 then
		mqc16_9567_mind_of_dragon_OnTalk_n1789_gereint_kid_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1138 then
		mqc16_9567_mind_of_dragon_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1790 then
		mqc16_9567_mind_of_dragon_OnTalk_n1790_jasmin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1774 then
		mqc16_9567_mind_of_dragon_OnTalk_n1774_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1791 then
		mqc16_9567_mind_of_dragon_OnTalk_n1791_jasmin_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1782_rubinat--------------------------------------------------------------------------------
function mqc16_9567_mind_of_dragon_OnTalk_n1782_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1782_rubinat-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1782_rubinat-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1782_rubinat-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,9567, 2);
				api_quest_SetJournalStep(userObjID,9567, 1);
				api_quest_SetQuestStep(userObjID,9567, 1);
				npc_talk_index = "n1782_rubinat-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1789_gereint_kid_memory--------------------------------------------------------------------------------
function mqc16_9567_mind_of_dragon_OnTalk_n1789_gereint_kid_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1789_gereint_kid_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1789_gereint_kid_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1789_gereint_kid_memory-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1789_gereint_kid_memory-accepting-acceptted_1" then
				api_quest_AddQuest(userObjID,9567, 2);
				api_quest_SetJournalStep(userObjID,9567, 1);
				api_quest_SetQuestStep(userObjID,9567, 1);
				npc_talk_index = "n1789_gereint_kid_memory-1";

	end
	if npc_talk_index == "n1789_gereint_kid_memory-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function mqc16_9567_mind_of_dragon_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1138_elf_guard_sitredel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1138_elf_guard_sitredel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-2-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-e" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1790_jasmin--------------------------------------------------------------------------------
function mqc16_9567_mind_of_dragon_OnTalk_n1790_jasmin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1790_jasmin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1790_jasmin-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1790_jasmin-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1790_jasmin-4-class_check" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1790_jasmin-4-a";

				else
									api_quest_SetQuestStep(userObjID, questID,5);
				npc_talk_index = "n1790_jasmin-5";

				end
	end
	if npc_talk_index == "n1790_jasmin-4-b" then 
	end
	if npc_talk_index == "n1790_jasmin-4-b" then 
	end
	if npc_talk_index == "n1790_jasmin-4-b" then 
	end
	if npc_talk_index == "n1790_jasmin-4-c" then 
	end
	if npc_talk_index == "n1790_jasmin-4-d" then 
	end
	if npc_talk_index == "n1790_jasmin-4-d" then 
	end
	if npc_talk_index == "n1790_jasmin-4-d" then 
	end
	if npc_talk_index == "n1790_jasmin-4-e" then 
	end
	if npc_talk_index == "n1790_jasmin-4-f" then 
	end
	if npc_talk_index == "n1790_jasmin-4-g" then 
	end
	if npc_talk_index == "n1790_jasmin-4-g" then 
	end
	if npc_talk_index == "n1790_jasmin-4-g" then 
	end
	if npc_talk_index == "n1790_jasmin-4-h" then 
	end
	if npc_talk_index == "n1790_jasmin-4-i" then 
	end
	if npc_talk_index == "n1790_jasmin-4-j" then 
	end
	if npc_talk_index == "n1790_jasmin-4-k" then 
	end
	if npc_talk_index == "n1790_jasmin-4-l" then 
	end
	if npc_talk_index == "n1790_jasmin-4-m" then 
	end
	if npc_talk_index == "n1790_jasmin-4-n" then 
	end
	if npc_talk_index == "n1790_jasmin-4-o" then 
	end
	if npc_talk_index == "n1790_jasmin-4-p" then 
	end
	if npc_talk_index == "n1790_jasmin-4-q" then 
	end
	if npc_talk_index == "n1790_jasmin-4-u" then 
	end
	if npc_talk_index == "n1790_jasmin-4-r" then 
	end
	if npc_talk_index == "n1790_jasmin-4-s" then 
	end
	if npc_talk_index == "n1790_jasmin-4-s" then 
	end
	if npc_talk_index == "n1790_jasmin-4-t" then 
	end
	if npc_talk_index == "n1790_jasmin-4-t" then 
	end
	if npc_talk_index == "n1790_jasmin-4-u" then 
	end
	if npc_talk_index == "n1790_jasmin-4-v" then 
	end
	if npc_talk_index == "n1790_jasmin-4-w" then 
	end
	if npc_talk_index == "n1790_jasmin-4-x" then 
	end
	if npc_talk_index == "n1790_jasmin-4-y" then 
	end
	if npc_talk_index == "n1790_jasmin-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
	end
	if npc_talk_index == "n1790_jasmin-5-b" then 
	end
	if npc_talk_index == "n1790_jasmin-5-c" then 
	end
	if npc_talk_index == "n1790_jasmin-5-d" then 
	end
	if npc_talk_index == "n1790_jasmin-5-e" then 
	end
	if npc_talk_index == "n1790_jasmin-5-f" then 
	end
	if npc_talk_index == "n1790_jasmin-5-g" then 
	end
	if npc_talk_index == "n1790_jasmin-5-h" then 
	end
	if npc_talk_index == "n1790_jasmin-5-i" then 
	end
	if npc_talk_index == "n1790_jasmin-5-i" then 
	end
	if npc_talk_index == "n1790_jasmin-5-j" then 
	end
	if npc_talk_index == "n1790_jasmin-5-k" then 
	end
	if npc_talk_index == "n1790_jasmin-5-l" then 
	end
	if npc_talk_index == "n1790_jasmin-5-m" then 
	end
	if npc_talk_index == "n1790_jasmin-5-n" then 
	end
	if npc_talk_index == "n1790_jasmin-5-n" then 
	end
	if npc_talk_index == "n1790_jasmin-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1774_gereint_kid--------------------------------------------------------------------------------
function mqc16_9567_mind_of_dragon_OnTalk_n1774_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1774_gereint_kid-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1774_gereint_kid-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1774_gereint_kid-8-b" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-b" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-b" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-c" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-c" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-d" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-e" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-f" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-g" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-h" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-i" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-j" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-k" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-l" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-m" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-n" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-n" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-o" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-o" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-o" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9" then 
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end
	if npc_talk_index == "n1774_gereint_kid-9" then 
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end
	if npc_talk_index == "n1774_gereint_kid-9-b" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-b" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-c" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-d" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-d" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-e" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-f" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-g" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-g" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-g" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-h" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-i" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-j" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-k" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-l" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95670, true);
				 api_quest_RewardQuestUser(userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95670, true);
				 api_quest_RewardQuestUser(userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95670, true);
				 api_quest_RewardQuestUser(userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95670, true);
				 api_quest_RewardQuestUser(userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95670, true);
				 api_quest_RewardQuestUser(userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95670, true);
				 api_quest_RewardQuestUser(userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95670, true);
				 api_quest_RewardQuestUser(userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95670, true);
				 api_quest_RewardQuestUser(userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95670, true);
				 api_quest_RewardQuestUser(userObjID, 95670, questID, 1);
			 end 
	end
	if npc_talk_index == "n1774_gereint_kid-9-m" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9568, 2);
					api_quest_SetQuestStep(userObjID, 9568, 1);
					api_quest_SetJournalStep(userObjID, 9568, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1774_gereint_kid-9-n" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-n" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1774_gereint_kid-1", "mqc16_9568_belated_reconciliation.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1791_jasmin_memory--------------------------------------------------------------------------------
function mqc16_9567_mind_of_dragon_OnTalk_n1791_jasmin_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1791_jasmin_memory-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1791_jasmin_memory-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1791_jasmin_memory-7-class_check_2" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1791_jasmin_memory-7-a";

				else
									npc_talk_index = "n1791_jasmin_memory-7-c";

				end
	end
	if npc_talk_index == "n1791_jasmin_memory-7-b" then 
	end
	if npc_talk_index == "n1791_jasmin_memory-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end
	if npc_talk_index == "n1791_jasmin_memory-7-d" then 
	end
	if npc_talk_index == "n1791_jasmin_memory-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9567_mind_of_dragon_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9567);
end

function mqc16_9567_mind_of_dragon_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9567);
end

function mqc16_9567_mind_of_dragon_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9567);
	local questID=9567;
end

function mqc16_9567_mind_of_dragon_OnRemoteStart( userObjID, questID )
end

function mqc16_9567_mind_of_dragon_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc16_9567_mind_of_dragon_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mqc16_9567_mind_of_dragon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1782 then
		mqc16_9567_mind_of_dragon_OnTalk_n1782_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1789 then
		mqc16_9567_mind_of_dragon_OnTalk_n1789_gereint_kid_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1138 then
		mqc16_9567_mind_of_dragon_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1790 then
		mqc16_9567_mind_of_dragon_OnTalk_n1790_jasmin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1774 then
		mqc16_9567_mind_of_dragon_OnTalk_n1774_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1791 then
		mqc16_9567_mind_of_dragon_OnTalk_n1791_jasmin_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1782_rubinat--------------------------------------------------------------------------------
function mqc16_9567_mind_of_dragon_OnTalk_n1782_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1782_rubinat-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1782_rubinat-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1782_rubinat-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,9567, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9567, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9567, 1);
				npc_talk_index = "n1782_rubinat-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1789_gereint_kid_memory--------------------------------------------------------------------------------
function mqc16_9567_mind_of_dragon_OnTalk_n1789_gereint_kid_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1789_gereint_kid_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1789_gereint_kid_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1789_gereint_kid_memory-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1789_gereint_kid_memory-accepting-acceptted_1" then
				api_quest_AddQuest( pRoom, userObjID,9567, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9567, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9567, 1);
				npc_talk_index = "n1789_gereint_kid_memory-1";

	end
	if npc_talk_index == "n1789_gereint_kid_memory-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function mqc16_9567_mind_of_dragon_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1138_elf_guard_sitredel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1138_elf_guard_sitredel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-2-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-e" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1790_jasmin--------------------------------------------------------------------------------
function mqc16_9567_mind_of_dragon_OnTalk_n1790_jasmin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1790_jasmin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1790_jasmin-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1790_jasmin-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1790_jasmin-4-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1790_jasmin-4-a";

				else
									api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				npc_talk_index = "n1790_jasmin-5";

				end
	end
	if npc_talk_index == "n1790_jasmin-4-b" then 
	end
	if npc_talk_index == "n1790_jasmin-4-b" then 
	end
	if npc_talk_index == "n1790_jasmin-4-b" then 
	end
	if npc_talk_index == "n1790_jasmin-4-c" then 
	end
	if npc_talk_index == "n1790_jasmin-4-d" then 
	end
	if npc_talk_index == "n1790_jasmin-4-d" then 
	end
	if npc_talk_index == "n1790_jasmin-4-d" then 
	end
	if npc_talk_index == "n1790_jasmin-4-e" then 
	end
	if npc_talk_index == "n1790_jasmin-4-f" then 
	end
	if npc_talk_index == "n1790_jasmin-4-g" then 
	end
	if npc_talk_index == "n1790_jasmin-4-g" then 
	end
	if npc_talk_index == "n1790_jasmin-4-g" then 
	end
	if npc_talk_index == "n1790_jasmin-4-h" then 
	end
	if npc_talk_index == "n1790_jasmin-4-i" then 
	end
	if npc_talk_index == "n1790_jasmin-4-j" then 
	end
	if npc_talk_index == "n1790_jasmin-4-k" then 
	end
	if npc_talk_index == "n1790_jasmin-4-l" then 
	end
	if npc_talk_index == "n1790_jasmin-4-m" then 
	end
	if npc_talk_index == "n1790_jasmin-4-n" then 
	end
	if npc_talk_index == "n1790_jasmin-4-o" then 
	end
	if npc_talk_index == "n1790_jasmin-4-p" then 
	end
	if npc_talk_index == "n1790_jasmin-4-q" then 
	end
	if npc_talk_index == "n1790_jasmin-4-u" then 
	end
	if npc_talk_index == "n1790_jasmin-4-r" then 
	end
	if npc_talk_index == "n1790_jasmin-4-s" then 
	end
	if npc_talk_index == "n1790_jasmin-4-s" then 
	end
	if npc_talk_index == "n1790_jasmin-4-t" then 
	end
	if npc_talk_index == "n1790_jasmin-4-t" then 
	end
	if npc_talk_index == "n1790_jasmin-4-u" then 
	end
	if npc_talk_index == "n1790_jasmin-4-v" then 
	end
	if npc_talk_index == "n1790_jasmin-4-w" then 
	end
	if npc_talk_index == "n1790_jasmin-4-x" then 
	end
	if npc_talk_index == "n1790_jasmin-4-y" then 
	end
	if npc_talk_index == "n1790_jasmin-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
	end
	if npc_talk_index == "n1790_jasmin-5-b" then 
	end
	if npc_talk_index == "n1790_jasmin-5-c" then 
	end
	if npc_talk_index == "n1790_jasmin-5-d" then 
	end
	if npc_talk_index == "n1790_jasmin-5-e" then 
	end
	if npc_talk_index == "n1790_jasmin-5-f" then 
	end
	if npc_talk_index == "n1790_jasmin-5-g" then 
	end
	if npc_talk_index == "n1790_jasmin-5-h" then 
	end
	if npc_talk_index == "n1790_jasmin-5-i" then 
	end
	if npc_talk_index == "n1790_jasmin-5-i" then 
	end
	if npc_talk_index == "n1790_jasmin-5-j" then 
	end
	if npc_talk_index == "n1790_jasmin-5-k" then 
	end
	if npc_talk_index == "n1790_jasmin-5-l" then 
	end
	if npc_talk_index == "n1790_jasmin-5-m" then 
	end
	if npc_talk_index == "n1790_jasmin-5-n" then 
	end
	if npc_talk_index == "n1790_jasmin-5-n" then 
	end
	if npc_talk_index == "n1790_jasmin-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1774_gereint_kid--------------------------------------------------------------------------------
function mqc16_9567_mind_of_dragon_OnTalk_n1774_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1774_gereint_kid-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1774_gereint_kid-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1774_gereint_kid-8-b" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-b" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-b" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-c" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-c" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-d" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-e" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-f" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-g" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-h" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-i" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-j" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-k" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-l" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-m" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-n" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-n" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-o" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-o" then 
	end
	if npc_talk_index == "n1774_gereint_kid-8-o" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end
	if npc_talk_index == "n1774_gereint_kid-9" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end
	if npc_talk_index == "n1774_gereint_kid-9-b" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-b" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-c" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-d" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-d" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-e" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-f" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-g" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-g" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-g" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-h" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-i" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-j" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-k" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-l" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95670, questID, 1);
			 end 
	end
	if npc_talk_index == "n1774_gereint_kid-9-m" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9568, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9568, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9568, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1774_gereint_kid-9-n" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-n" then 
	end
	if npc_talk_index == "n1774_gereint_kid-9-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1774_gereint_kid-1", "mqc16_9568_belated_reconciliation.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1791_jasmin_memory--------------------------------------------------------------------------------
function mqc16_9567_mind_of_dragon_OnTalk_n1791_jasmin_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1791_jasmin_memory-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1791_jasmin_memory-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1791_jasmin_memory-7-class_check_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1791_jasmin_memory-7-a";

				else
									npc_talk_index = "n1791_jasmin_memory-7-c";

				end
	end
	if npc_talk_index == "n1791_jasmin_memory-7-b" then 
	end
	if npc_talk_index == "n1791_jasmin_memory-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end
	if npc_talk_index == "n1791_jasmin_memory-7-d" then 
	end
	if npc_talk_index == "n1791_jasmin_memory-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9567_mind_of_dragon_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9567);
end

function mqc16_9567_mind_of_dragon_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9567);
end

function mqc16_9567_mind_of_dragon_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9567);
	local questID=9567;
end

function mqc16_9567_mind_of_dragon_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc16_9567_mind_of_dragon_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc16_9567_mind_of_dragon_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>