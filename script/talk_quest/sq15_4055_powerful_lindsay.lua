<VillageServer>

function sq15_4055_powerful_lindsay_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 62 then
		sq15_4055_powerful_lindsay_OnTalk_n062_trainer_lindsay(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n062_trainer_lindsay--------------------------------------------------------------------------------
function sq15_4055_powerful_lindsay_OnTalk_n062_trainer_lindsay(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n062_trainer_lindsay-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n062_trainer_lindsay-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n062_trainer_lindsay-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n062_trainer_lindsay-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40550, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40550, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40550, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40550, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40550, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40550, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40550, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40550, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40550, false);
			 end 

	end
	if npc_talk_index == "n062_trainer_lindsay-accepting-acceppted" then
				api_quest_AddQuest(userObjID,4055, 1);
				api_quest_SetJournalStep(userObjID,4055, 1);
				api_quest_SetQuestStep(userObjID,4055, 1);
				npc_talk_index = "n062_trainer_lindsay-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 600153, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 600154, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700153, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 700154, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300542, 2);

	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-c" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40550, true);
				 api_quest_RewardQuestUser(userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40550, true);
				 api_quest_RewardQuestUser(userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40550, true);
				 api_quest_RewardQuestUser(userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40550, true);
				 api_quest_RewardQuestUser(userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40550, true);
				 api_quest_RewardQuestUser(userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40550, true);
				 api_quest_RewardQuestUser(userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40550, true);
				 api_quest_RewardQuestUser(userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40550, true);
				 api_quest_RewardQuestUser(userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40550, true);
				 api_quest_RewardQuestUser(userObjID, 40550, questID, 1);
			 end 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-e" then 

				if api_quest_HasQuestItem(userObjID, 300542, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300542, api_quest_HasQuestItem(userObjID, 300542, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4055_powerful_lindsay_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4055);
	if qstep == 1 and CountIndex == 600153 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300542, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300542, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600154 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300542, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300542, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700153 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300542, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300542, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700154 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300542, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300542, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300542 then

	end
end

function sq15_4055_powerful_lindsay_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4055);
	if qstep == 1 and CountIndex == 600153 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600154 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700153 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700154 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300542 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function sq15_4055_powerful_lindsay_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4055);
	local questID=4055;
end

function sq15_4055_powerful_lindsay_OnRemoteStart( userObjID, questID )
end

function sq15_4055_powerful_lindsay_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4055_powerful_lindsay_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq15_4055_powerful_lindsay_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 62 then
		sq15_4055_powerful_lindsay_OnTalk_n062_trainer_lindsay( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n062_trainer_lindsay--------------------------------------------------------------------------------
function sq15_4055_powerful_lindsay_OnTalk_n062_trainer_lindsay( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n062_trainer_lindsay-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n062_trainer_lindsay-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n062_trainer_lindsay-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n062_trainer_lindsay-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, false);
			 end 

	end
	if npc_talk_index == "n062_trainer_lindsay-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,4055, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4055, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4055, 1);
				npc_talk_index = "n062_trainer_lindsay-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 600153, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 600154, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700153, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 700154, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300542, 2);

	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-c" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40550, questID, 1);
			 end 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-e" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300542, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300542, api_quest_HasQuestItem( pRoom, userObjID, 300542, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4055_powerful_lindsay_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4055);
	if qstep == 1 and CountIndex == 600153 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300542, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300542, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600154 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300542, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300542, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700153 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300542, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300542, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700154 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300542, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300542, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300542 then

	end
end

function sq15_4055_powerful_lindsay_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4055);
	if qstep == 1 and CountIndex == 600153 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600154 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700153 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700154 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300542 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function sq15_4055_powerful_lindsay_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4055);
	local questID=4055;
end

function sq15_4055_powerful_lindsay_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4055_powerful_lindsay_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4055_powerful_lindsay_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>