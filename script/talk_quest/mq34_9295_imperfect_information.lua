<VillageServer>

function mq34_9295_imperfect_information_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9295_imperfect_information_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1060 then
		mq34_9295_imperfect_information_OnTalk_n1060_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1085 then
		mq34_9295_imperfect_information_OnTalk_n1085_rojalin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9295_imperfect_information_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1047_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1047_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9295, 2);
				api_quest_SetJournalStep(userObjID,9295, 1);
				api_quest_SetQuestStep(userObjID,9295, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
	end
	if npc_talk_index == "n1047_argenta-1-f" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1047_argenta-4-b" then 
	end
	if npc_talk_index == "n1047_argenta-4-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92951, true);
				 api_quest_RewardQuestUser(userObjID, 92951, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92951, true);
				 api_quest_RewardQuestUser(userObjID, 92951, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92953, true);
				 api_quest_RewardQuestUser(userObjID, 92953, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92954, true);
				 api_quest_RewardQuestUser(userObjID, 92954, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92955, true);
				 api_quest_RewardQuestUser(userObjID, 92955, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92956, true);
				 api_quest_RewardQuestUser(userObjID, 92956, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92957, true);
				 api_quest_RewardQuestUser(userObjID, 92957, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92958, true);
				 api_quest_RewardQuestUser(userObjID, 92958, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92959, true);
				 api_quest_RewardQuestUser(userObjID, 92959, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9296, 2);
					api_quest_SetQuestStep(userObjID, 9296, 1);
					api_quest_SetJournalStep(userObjID, 9296, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1047_argenta-1", "mq34_9296_division_foe.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1060_yuvenciel--------------------------------------------------------------------------------
function mq34_9295_imperfect_information_OnTalk_n1060_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1060_yuvenciel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1060_yuvenciel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1060_yuvenciel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1060_yuvenciel-3-job3_2" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1060_yuvenciel-3-a";

				else
									npc_talk_index = "n1060_yuvenciel-3-b";

				end
	end
	if npc_talk_index == "n1060_yuvenciel-3-c" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-c" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-job3_3" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1060_yuvenciel-3-d";

				else
									npc_talk_index = "n1060_yuvenciel-3-e";

				end
	end
	if npc_talk_index == "n1060_yuvenciel-3-f" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-f" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-job3_4" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1060_yuvenciel-3-g";

				else
									npc_talk_index = "n1060_yuvenciel-3-h";

				end
	end
	if npc_talk_index == "n1060_yuvenciel-3-i" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-i" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-job3_5" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1060_yuvenciel-3-j";

				else
									npc_talk_index = "n1060_yuvenciel-3-k";

				end
	end
	if npc_talk_index == "n1060_yuvenciel-3-l" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-l" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-job3_6" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1060_yuvenciel-3-m";

				else
									npc_talk_index = "n1060_yuvenciel-3-n";

				end
	end
	if npc_talk_index == "n1060_yuvenciel-3-o" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1060_yuvenciel-3-o" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1085_rojalin--------------------------------------------------------------------------------
function mq34_9295_imperfect_information_OnTalk_n1085_rojalin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1085_rojalin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1085_rojalin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1085_rojalin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1085_rojalin-2-b" then 
	end
	if npc_talk_index == "n1085_rojalin-2-c" then 
	end
	if npc_talk_index == "n1085_rojalin-2-d" then 
	end
	if npc_talk_index == "n1085_rojalin-2-e" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9295_imperfect_information_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9295);
end

function mq34_9295_imperfect_information_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9295);
end

function mq34_9295_imperfect_information_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9295);
	local questID=9295;
end

function mq34_9295_imperfect_information_OnRemoteStart( userObjID, questID )
end

function mq34_9295_imperfect_information_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9295_imperfect_information_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9295, 2);
				api_quest_SetJournalStep(userObjID,9295, 1);
				api_quest_SetQuestStep(userObjID,9295, 1);
				npc_talk_index = "n1047_argenta-1";
end

</VillageServer>

<GameServer>
function mq34_9295_imperfect_information_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9295_imperfect_information_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1060 then
		mq34_9295_imperfect_information_OnTalk_n1060_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1085 then
		mq34_9295_imperfect_information_OnTalk_n1085_rojalin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9295_imperfect_information_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1047_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1047_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9295, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9295, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9295, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
	end
	if npc_talk_index == "n1047_argenta-1-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1047_argenta-4-b" then 
	end
	if npc_talk_index == "n1047_argenta-4-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92951, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92951, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92951, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92951, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92953, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92953, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92954, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92954, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92955, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92955, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92956, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92956, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92957, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92957, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92958, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92958, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92959, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92959, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9296, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9296, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9296, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1047_argenta-1", "mq34_9296_division_foe.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1060_yuvenciel--------------------------------------------------------------------------------
function mq34_9295_imperfect_information_OnTalk_n1060_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1060_yuvenciel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1060_yuvenciel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1060_yuvenciel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1060_yuvenciel-3-job3_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1060_yuvenciel-3-a";

				else
									npc_talk_index = "n1060_yuvenciel-3-b";

				end
	end
	if npc_talk_index == "n1060_yuvenciel-3-c" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-c" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-job3_3" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1060_yuvenciel-3-d";

				else
									npc_talk_index = "n1060_yuvenciel-3-e";

				end
	end
	if npc_talk_index == "n1060_yuvenciel-3-f" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-f" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-job3_4" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1060_yuvenciel-3-g";

				else
									npc_talk_index = "n1060_yuvenciel-3-h";

				end
	end
	if npc_talk_index == "n1060_yuvenciel-3-i" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-i" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-job3_5" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1060_yuvenciel-3-j";

				else
									npc_talk_index = "n1060_yuvenciel-3-k";

				end
	end
	if npc_talk_index == "n1060_yuvenciel-3-l" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-l" then 
	end
	if npc_talk_index == "n1060_yuvenciel-3-job3_6" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1060_yuvenciel-3-m";

				else
									npc_talk_index = "n1060_yuvenciel-3-n";

				end
	end
	if npc_talk_index == "n1060_yuvenciel-3-o" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1060_yuvenciel-3-o" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1085_rojalin--------------------------------------------------------------------------------
function mq34_9295_imperfect_information_OnTalk_n1085_rojalin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1085_rojalin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1085_rojalin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1085_rojalin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1085_rojalin-2-b" then 
	end
	if npc_talk_index == "n1085_rojalin-2-c" then 
	end
	if npc_talk_index == "n1085_rojalin-2-d" then 
	end
	if npc_talk_index == "n1085_rojalin-2-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9295_imperfect_information_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9295);
end

function mq34_9295_imperfect_information_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9295);
end

function mq34_9295_imperfect_information_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9295);
	local questID=9295;
end

function mq34_9295_imperfect_information_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9295_imperfect_information_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9295_imperfect_information_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9295, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9295, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9295, 1);
				npc_talk_index = "n1047_argenta-1";
end

</GameServer>