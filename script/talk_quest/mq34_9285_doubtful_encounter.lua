<VillageServer>

function mq34_9285_doubtful_encounter_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9285_doubtful_encounter_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1054 then
		mq34_9285_doubtful_encounter_OnTalk_n1054_azelia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1055 then
		mq34_9285_doubtful_encounter_OnTalk_n1055_elf_guard_basha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1056 then
		mq34_9285_doubtful_encounter_OnTalk_n1056_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1083 then
		mq34_9285_doubtful_encounter_OnTalk_n1083_triana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1084 then
		mq34_9285_doubtful_encounter_OnTalk_n1084_triana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9285_doubtful_encounter_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9285, 2);
				api_quest_SetJournalStep(userObjID,9285, 1);
				api_quest_SetQuestStep(userObjID,9285, 1);

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1054_azelia--------------------------------------------------------------------------------
function mq34_9285_doubtful_encounter_OnTalk_n1054_azelia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1054_azelia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1054_azelia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1055_elf_guard_basha--------------------------------------------------------------------------------
function mq34_9285_doubtful_encounter_OnTalk_n1055_elf_guard_basha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1055_elf_guard_basha-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1055_elf_guard_basha-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1055_elf_guard_basha-5-job5_2" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1055_elf_guard_basha-5-a";

				else
									npc_talk_index = "n1055_elf_guard_basha-5-b";

				end
	end
	if npc_talk_index == "n1055_elf_guard_basha-5-c" then 
	end
	if npc_talk_index == "n1055_elf_guard_basha-5-c" then 
	end
	if npc_talk_index == "n1055_elf_guard_basha-5-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92851, true);
				 api_quest_RewardQuestUser(userObjID, 92851, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92850, true);
				 api_quest_RewardQuestUser(userObjID, 92850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92853, true);
				 api_quest_RewardQuestUser(userObjID, 92853, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92854, true);
				 api_quest_RewardQuestUser(userObjID, 92854, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92855, true);
				 api_quest_RewardQuestUser(userObjID, 92855, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92856, true);
				 api_quest_RewardQuestUser(userObjID, 92856, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92857, true);
				 api_quest_RewardQuestUser(userObjID, 92857, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92858, true);
				 api_quest_RewardQuestUser(userObjID, 92858, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92859, true);
				 api_quest_RewardQuestUser(userObjID, 92859, questID, 1);
			 end 
	end
	if npc_talk_index == "n1055_elf_guard_basha-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9286, 2);
					api_quest_SetQuestStep(userObjID, 9286, 1);
					api_quest_SetJournalStep(userObjID, 9286, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1055_elf_guard_basha-1", "mq34_9286_dissonance.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1056_yuvenciel--------------------------------------------------------------------------------
function mq34_9285_doubtful_encounter_OnTalk_n1056_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1056_yuvenciel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1056_yuvenciel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1056_yuvenciel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1056_yuvenciel-3-b" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-c" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-d" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-e" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-f" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-g" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-h" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-i" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-j" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1083_triana--------------------------------------------------------------------------------
function mq34_9285_doubtful_encounter_OnTalk_n1083_triana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1083_triana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1083_triana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1083_triana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1083_triana-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1083_triana-accepting-chk_j1" then
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1083_triana-accepting-a";

				else
									npc_talk_index = "n1083_triana-accepting-b";

				end

	end
	if npc_talk_index == "n1083_triana-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9285, 2);
				api_quest_SetJournalStep(userObjID,9285, 1);
				api_quest_SetQuestStep(userObjID,9285, 1);
				npc_talk_index = "n1083_triana-1";

	end
	if npc_talk_index == "n1083_triana-1-c" then 
	end
	if npc_talk_index == "n1083_triana-1-c" then 
	end
	if npc_talk_index == "n1083_triana-1-job3" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1083_triana-1-d";

				else
									npc_talk_index = "n1083_triana-1-e";

				end
	end
	if npc_talk_index == "n1083_triana-1-f" then 
	end
	if npc_talk_index == "n1083_triana-1-f" then 
	end
	if npc_talk_index == "n1083_triana-1-g" then 
	end
	if npc_talk_index == "n1083_triana-1-h" then 
	end
	if npc_talk_index == "n1083_triana-1-i" then 
	end
	if npc_talk_index == "n1083_triana-1-j" then 
	end
	if npc_talk_index == "n1083_triana-1-k" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1083_triana-1-job2" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1083_triana-1-a";

				else
									npc_talk_index = "n1083_triana-1-b";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1084_triana--------------------------------------------------------------------------------
function mq34_9285_doubtful_encounter_OnTalk_n1084_triana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1084_triana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1084_triana-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1084_triana-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1084_triana-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1084_triana-4-job4_2" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1084_triana-4-a";

				else
									npc_talk_index = "n1084_triana-4-b";

				end
	end
	if npc_talk_index == "n1084_triana-4-c" then 
	end
	if npc_talk_index == "n1084_triana-4-c" then 
	end
	if npc_talk_index == "n1084_triana-4-job4_3" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1084_triana-4-d";

				else
									npc_talk_index = "n1084_triana-4-e";

				end
	end
	if npc_talk_index == "n1084_triana-4-f" then 
	end
	if npc_talk_index == "n1084_triana-4-f" then 
	end
	if npc_talk_index == "n1084_triana-4-g" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9285_doubtful_encounter_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9285);
end

function mq34_9285_doubtful_encounter_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9285);
end

function mq34_9285_doubtful_encounter_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9285);
	local questID=9285;
end

function mq34_9285_doubtful_encounter_OnRemoteStart( userObjID, questID )
end

function mq34_9285_doubtful_encounter_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9285_doubtful_encounter_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9285, 2);
				api_quest_SetJournalStep(userObjID,9285, 1);
				api_quest_SetQuestStep(userObjID,9285, 1);
				npc_talk_index = "n1083_triana-1";
end

</VillageServer>

<GameServer>
function mq34_9285_doubtful_encounter_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9285_doubtful_encounter_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1054 then
		mq34_9285_doubtful_encounter_OnTalk_n1054_azelia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1055 then
		mq34_9285_doubtful_encounter_OnTalk_n1055_elf_guard_basha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1056 then
		mq34_9285_doubtful_encounter_OnTalk_n1056_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1083 then
		mq34_9285_doubtful_encounter_OnTalk_n1083_triana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1084 then
		mq34_9285_doubtful_encounter_OnTalk_n1084_triana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9285_doubtful_encounter_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9285, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9285, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9285, 1);

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1054_azelia--------------------------------------------------------------------------------
function mq34_9285_doubtful_encounter_OnTalk_n1054_azelia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1054_azelia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1054_azelia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1055_elf_guard_basha--------------------------------------------------------------------------------
function mq34_9285_doubtful_encounter_OnTalk_n1055_elf_guard_basha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1055_elf_guard_basha-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1055_elf_guard_basha-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1055_elf_guard_basha-5-job5_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1055_elf_guard_basha-5-a";

				else
									npc_talk_index = "n1055_elf_guard_basha-5-b";

				end
	end
	if npc_talk_index == "n1055_elf_guard_basha-5-c" then 
	end
	if npc_talk_index == "n1055_elf_guard_basha-5-c" then 
	end
	if npc_talk_index == "n1055_elf_guard_basha-5-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92851, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92851, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92853, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92853, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92854, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92854, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92855, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92855, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92856, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92856, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92857, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92857, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92858, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92858, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92859, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92859, questID, 1);
			 end 
	end
	if npc_talk_index == "n1055_elf_guard_basha-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9286, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9286, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9286, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1055_elf_guard_basha-1", "mq34_9286_dissonance.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1056_yuvenciel--------------------------------------------------------------------------------
function mq34_9285_doubtful_encounter_OnTalk_n1056_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1056_yuvenciel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1056_yuvenciel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1056_yuvenciel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1056_yuvenciel-3-b" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-c" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-d" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-e" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-f" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-g" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-h" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-i" then 
	end
	if npc_talk_index == "n1056_yuvenciel-3-j" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1083_triana--------------------------------------------------------------------------------
function mq34_9285_doubtful_encounter_OnTalk_n1083_triana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1083_triana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1083_triana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1083_triana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1083_triana-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1083_triana-accepting-chk_j1" then
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1083_triana-accepting-a";

				else
									npc_talk_index = "n1083_triana-accepting-b";

				end

	end
	if npc_talk_index == "n1083_triana-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9285, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9285, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9285, 1);
				npc_talk_index = "n1083_triana-1";

	end
	if npc_talk_index == "n1083_triana-1-c" then 
	end
	if npc_talk_index == "n1083_triana-1-c" then 
	end
	if npc_talk_index == "n1083_triana-1-job3" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1083_triana-1-d";

				else
									npc_talk_index = "n1083_triana-1-e";

				end
	end
	if npc_talk_index == "n1083_triana-1-f" then 
	end
	if npc_talk_index == "n1083_triana-1-f" then 
	end
	if npc_talk_index == "n1083_triana-1-g" then 
	end
	if npc_talk_index == "n1083_triana-1-h" then 
	end
	if npc_talk_index == "n1083_triana-1-i" then 
	end
	if npc_talk_index == "n1083_triana-1-j" then 
	end
	if npc_talk_index == "n1083_triana-1-k" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1083_triana-1-job2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1083_triana-1-a";

				else
									npc_talk_index = "n1083_triana-1-b";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1084_triana--------------------------------------------------------------------------------
function mq34_9285_doubtful_encounter_OnTalk_n1084_triana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1084_triana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1084_triana-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1084_triana-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1084_triana-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1084_triana-4-job4_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1084_triana-4-a";

				else
									npc_talk_index = "n1084_triana-4-b";

				end
	end
	if npc_talk_index == "n1084_triana-4-c" then 
	end
	if npc_talk_index == "n1084_triana-4-c" then 
	end
	if npc_talk_index == "n1084_triana-4-job4_3" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1084_triana-4-d";

				else
									npc_talk_index = "n1084_triana-4-e";

				end
	end
	if npc_talk_index == "n1084_triana-4-f" then 
	end
	if npc_talk_index == "n1084_triana-4-f" then 
	end
	if npc_talk_index == "n1084_triana-4-g" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9285_doubtful_encounter_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9285);
end

function mq34_9285_doubtful_encounter_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9285);
end

function mq34_9285_doubtful_encounter_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9285);
	local questID=9285;
end

function mq34_9285_doubtful_encounter_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9285_doubtful_encounter_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9285_doubtful_encounter_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9285, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9285, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9285, 1);
				npc_talk_index = "n1083_triana-1";
end

</GameServer>