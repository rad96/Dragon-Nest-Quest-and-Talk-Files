<VillageServer>

function dq_1003_globalevent_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 807 then
		dq_1003_globalevent_OnTalk_n807_market(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n807_market--------------------------------------------------------------------------------
function dq_1003_globalevent_OnTalk_n807_market(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n807_market-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n807_market-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n807_market-2";
				if api_quest_GetWorldEventStep(userObjID, 2) == 0  then
									npc_talk_index = "n807_market-2-e";

				else
									if api_quest_GetWorldEventStep(userObjID, 2) == 1  then
									npc_talk_index = "n807_market-2-e";

				else
									if api_quest_GetWorldEventStep(userObjID, 2) == 2  then
									npc_talk_index = "n807_market-2";

				else
									if api_quest_GetWorldEventStep(userObjID, 2) == 3  then
									npc_talk_index = "n807_market-2-f";

				else
									if api_quest_GetWorldEventStep(userObjID, 2) == 4  then
									if api_quest_GetQuestStep(userObjID, 1003) == 3  then
									npc_talk_index = "n807_market-2-b";

				else
					
				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				npc_talk_index = "n807_market-2-h";

				end

				else
									npc_talk_index = "n807_market-2-e";

				end

				end

				end

				end

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n807_market-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 10030, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 10030, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 10030, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 10030, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 10030, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 10030, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 10030, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 10030, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 10030, false);
			 end 

	end
	if npc_talk_index == "n807_market-accepting-acceptted" then
				api_quest_AddQuest(userObjID,1003, 1);
				api_quest_SetJournalStep(userObjID,1003, 1);
				api_quest_SetQuestStep(userObjID,1003, 1);
				npc_talk_index = "n807_market-1";
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805306965, 30002);

	end
	if npc_talk_index == "n807_market-1-b" then 
	end
	if npc_talk_index == "n807_market-1-c" then 
	end
	if npc_talk_index == "n807_market-2-retrunitem" then 
				if api_user_HasItem(userObjID, 805306965, 1) >= 1 then
									api_quest_ReturnItemToNpc (userObjID, 2);
				npc_talk_index = "n807_market-2-a";
				if api_quest_GetQuestStep(userObjID, 1003) == 3  then
				else
									api_quest_SetQuestStep(userObjID, questID,3);

				end

				else
									npc_talk_index = "n807_market-2-g";

				end
	end
	if npc_talk_index == "n807_market-2-openscore" then 
				api_quest_OpenScoreWorldEvent (userObjID, 2);
	end
	if npc_talk_index == "n807_market-2-c" then 
				if api_quest_GetWorldEventCount (userObjID, 2) >= 2000000000  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 10030, true);
				 api_quest_RewardQuestUser(userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 10030, true);
				 api_quest_RewardQuestUser(userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 10030, true);
				 api_quest_RewardQuestUser(userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 10030, true);
				 api_quest_RewardQuestUser(userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 10030, true);
				 api_quest_RewardQuestUser(userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 10030, true);
				 api_quest_RewardQuestUser(userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 10030, true);
				 api_quest_RewardQuestUser(userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 10030, true);
				 api_quest_RewardQuestUser(userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 10030, true);
				 api_quest_RewardQuestUser(userObjID, 10030, questID, 1);
			 end 

				else
									if api_quest_GetWorldEventCount (userObjID, 2) >= 1000000000  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 10031, true);
				 api_quest_RewardQuestUser(userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 10031, true);
				 api_quest_RewardQuestUser(userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 10031, true);
				 api_quest_RewardQuestUser(userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 10031, true);
				 api_quest_RewardQuestUser(userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 10031, true);
				 api_quest_RewardQuestUser(userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 10031, true);
				 api_quest_RewardQuestUser(userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 10031, true);
				 api_quest_RewardQuestUser(userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 10031, true);
				 api_quest_RewardQuestUser(userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 10031, true);
				 api_quest_RewardQuestUser(userObjID, 10031, questID, 1);
			 end 

				else
									if api_quest_GetWorldEventCount (userObjID, 2) >= 500000000  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 10032, true);
				 api_quest_RewardQuestUser(userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 10032, true);
				 api_quest_RewardQuestUser(userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 10032, true);
				 api_quest_RewardQuestUser(userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 10032, true);
				 api_quest_RewardQuestUser(userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 10032, true);
				 api_quest_RewardQuestUser(userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 10032, true);
				 api_quest_RewardQuestUser(userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 10032, true);
				 api_quest_RewardQuestUser(userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 10032, true);
				 api_quest_RewardQuestUser(userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 10032, true);
				 api_quest_RewardQuestUser(userObjID, 10032, questID, 1);
			 end 

				else
									if api_quest_GetWorldEventCount (userObjID, 2) >= 100000000  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 10033, true);
				 api_quest_RewardQuestUser(userObjID, 10033, questID, 1);
			 end 

				end

				end

				end

				end
	end
	if npc_talk_index == "n807_market-2-openscore" then 
				api_quest_OpenScoreWorldEvent (userObjID, 2);
	end
	if npc_talk_index == "n807_market-2-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dq_1003_globalevent_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1003);
	if qstep == 1 and CountIndex == 805306965 then

	end
end

function dq_1003_globalevent_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1003);
	if qstep == 1 and CountIndex == 805306965 and Count >= TargetCount  then

	end
end

function dq_1003_globalevent_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1003);
	local questID=1003;
end

function dq_1003_globalevent_OnRemoteStart( userObjID, questID )
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805306965, 30002);
end

function dq_1003_globalevent_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function dq_1003_globalevent_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,1003, 1);
				api_quest_SetJournalStep(userObjID,1003, 1);
				api_quest_SetQuestStep(userObjID,1003, 1);
				npc_talk_index = "n807_market-1";
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805306965, 30002);
end

</VillageServer>

<GameServer>
function dq_1003_globalevent_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 807 then
		dq_1003_globalevent_OnTalk_n807_market( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n807_market--------------------------------------------------------------------------------
function dq_1003_globalevent_OnTalk_n807_market( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n807_market-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n807_market-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n807_market-2";
				if api_quest_GetWorldEventStep( pRoom, userObjID, 2) == 0  then
									npc_talk_index = "n807_market-2-e";

				else
									if api_quest_GetWorldEventStep( pRoom, userObjID, 2) == 1  then
									npc_talk_index = "n807_market-2-e";

				else
									if api_quest_GetWorldEventStep( pRoom, userObjID, 2) == 2  then
									npc_talk_index = "n807_market-2";

				else
									if api_quest_GetWorldEventStep( pRoom, userObjID, 2) == 3  then
									npc_talk_index = "n807_market-2-f";

				else
									if api_quest_GetWorldEventStep( pRoom, userObjID, 2) == 4  then
									if api_quest_GetQuestStep( pRoom, userObjID, 1003) == 3  then
									npc_talk_index = "n807_market-2-b";

				else
					
				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				npc_talk_index = "n807_market-2-h";

				end

				else
									npc_talk_index = "n807_market-2-e";

				end

				end

				end

				end

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n807_market-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, false);
			 end 

	end
	if npc_talk_index == "n807_market-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,1003, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1003, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1003, 1);
				npc_talk_index = "n807_market-1";
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805306965, 30002);

	end
	if npc_talk_index == "n807_market-1-b" then 
	end
	if npc_talk_index == "n807_market-1-c" then 
	end
	if npc_talk_index == "n807_market-2-retrunitem" then 
				if api_user_HasItem( pRoom, userObjID, 805306965, 1) >= 1 then
									api_quest_ReturnItemToNpc ( pRoom, userObjID, 2);
				npc_talk_index = "n807_market-2-a";
				if api_quest_GetQuestStep( pRoom, userObjID, 1003) == 3  then
				else
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);

				end

				else
									npc_talk_index = "n807_market-2-g";

				end
	end
	if npc_talk_index == "n807_market-2-openscore" then 
				api_quest_OpenScoreWorldEvent ( pRoom, userObjID, 2);
	end
	if npc_talk_index == "n807_market-2-c" then 
				if api_quest_GetWorldEventCount ( pRoom, userObjID, 2) >= 2000000000  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10030, questID, 1);
			 end 

				else
									if api_quest_GetWorldEventCount ( pRoom, userObjID, 2) >= 1000000000  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10031, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10031, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10031, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10031, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10031, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10031, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10031, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10031, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10031, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10031, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10031, questID, 1);
			 end 

				else
									if api_quest_GetWorldEventCount ( pRoom, userObjID, 2) >= 500000000  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10032, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10032, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10032, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10032, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10032, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10032, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10032, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10032, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10032, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10032, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10032, questID, 1);
			 end 

				else
									if api_quest_GetWorldEventCount ( pRoom, userObjID, 2) >= 100000000  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10033, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10033, questID, 1);
			 end 

				end

				end

				end

				end
	end
	if npc_talk_index == "n807_market-2-openscore" then 
				api_quest_OpenScoreWorldEvent ( pRoom, userObjID, 2);
	end
	if npc_talk_index == "n807_market-2-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dq_1003_globalevent_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1003);
	if qstep == 1 and CountIndex == 805306965 then

	end
end

function dq_1003_globalevent_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1003);
	if qstep == 1 and CountIndex == 805306965 and Count >= TargetCount  then

	end
end

function dq_1003_globalevent_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1003);
	local questID=1003;
end

function dq_1003_globalevent_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805306965, 30002);
end

function dq_1003_globalevent_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function dq_1003_globalevent_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,1003, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1003, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1003, 1);
				npc_talk_index = "n807_market-1";
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805306965, 30002);
end

</GameServer>