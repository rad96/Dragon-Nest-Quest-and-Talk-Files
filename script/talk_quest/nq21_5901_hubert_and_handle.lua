<VillageServer>

function nq21_5901_hubert_and_handle_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 99 then
		nq21_5901_hubert_and_handle_OnTalk_n099_engineer_hubert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n099_engineer_hubert--------------------------------------------------------------------------------
function nq21_5901_hubert_and_handle_OnTalk_n099_engineer_hubert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n099_engineer_hubert-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n099_engineer_hubert-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n099_engineer_hubert-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n099_engineer_hubert-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n099_engineer_hubert-accepting-acceppted" then
				api_quest_AddQuest(userObjID,5901, 1);
				api_quest_SetJournalStep(userObjID,5901, 1);
				api_quest_SetQuestStep(userObjID,5901, 1);
				npc_talk_index = "n099_engineer_hubert-1";

	end
	if npc_talk_index == "n099_engineer_hubert-1-b" then 
	end
	if npc_talk_index == "n099_engineer_hubert-1-c" then 
	end
	if npc_talk_index == "n099_engineer_hubert-1-d" then 
	end
	if npc_talk_index == "n099_engineer_hubert-1-e" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 417, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200417, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400249, 1);
	end
	if npc_talk_index == "n099_engineer_hubert-3-b" then 
	end
	if npc_talk_index == "n099_engineer_hubert-3-c" then 
	end
	if npc_talk_index == "n099_engineer_hubert-3-d" then 
	end
	if npc_talk_index == "n099_engineer_hubert-3-e" then 
	end
	if npc_talk_index == "n099_engineer_hubert-3-f" then 

				if api_quest_HasQuestItem(userObjID, 400249, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400249, api_quest_HasQuestItem(userObjID, 400249, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 99, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq21_5901_hubert_and_handle_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5901);
	if qstep == 2 and CountIndex == 417 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400249, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400249, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200417 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400249, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400249, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400249 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function nq21_5901_hubert_and_handle_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5901);
	if qstep == 2 and CountIndex == 417 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200417 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400249 and Count >= TargetCount  then

	end
end

function nq21_5901_hubert_and_handle_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5901);
	local questID=5901;
end

function nq21_5901_hubert_and_handle_OnRemoteStart( userObjID, questID )
end

function nq21_5901_hubert_and_handle_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq21_5901_hubert_and_handle_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq21_5901_hubert_and_handle_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 99 then
		nq21_5901_hubert_and_handle_OnTalk_n099_engineer_hubert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n099_engineer_hubert--------------------------------------------------------------------------------
function nq21_5901_hubert_and_handle_OnTalk_n099_engineer_hubert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n099_engineer_hubert-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n099_engineer_hubert-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n099_engineer_hubert-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n099_engineer_hubert-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n099_engineer_hubert-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,5901, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5901, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5901, 1);
				npc_talk_index = "n099_engineer_hubert-1";

	end
	if npc_talk_index == "n099_engineer_hubert-1-b" then 
	end
	if npc_talk_index == "n099_engineer_hubert-1-c" then 
	end
	if npc_talk_index == "n099_engineer_hubert-1-d" then 
	end
	if npc_talk_index == "n099_engineer_hubert-1-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 417, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200417, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400249, 1);
	end
	if npc_talk_index == "n099_engineer_hubert-3-b" then 
	end
	if npc_talk_index == "n099_engineer_hubert-3-c" then 
	end
	if npc_talk_index == "n099_engineer_hubert-3-d" then 
	end
	if npc_talk_index == "n099_engineer_hubert-3-e" then 
	end
	if npc_talk_index == "n099_engineer_hubert-3-f" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400249, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400249, api_quest_HasQuestItem( pRoom, userObjID, 400249, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 99, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq21_5901_hubert_and_handle_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5901);
	if qstep == 2 and CountIndex == 417 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400249, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400249, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200417 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400249, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400249, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400249 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function nq21_5901_hubert_and_handle_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5901);
	if qstep == 2 and CountIndex == 417 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200417 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400249 and Count >= TargetCount  then

	end
end

function nq21_5901_hubert_and_handle_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5901);
	local questID=5901;
end

function nq21_5901_hubert_and_handle_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq21_5901_hubert_and_handle_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq21_5901_hubert_and_handle_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>