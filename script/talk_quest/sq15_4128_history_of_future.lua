<VillageServer>

function sq15_4128_history_of_future_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 45 then
		sq15_4128_history_of_future_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 880 then
		sq15_4128_history_of_future_OnTalk_n880_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 881 then
		sq15_4128_history_of_future_OnTalk_n881_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 882 then
		sq15_4128_history_of_future_OnTalk_n882_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 91 then
		sq15_4128_history_of_future_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq15_4128_history_of_future_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n045_soceress_master_stella-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n045_soceress_master_stella-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-6-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-6-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-6-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-6-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41280, true);
				 api_quest_RewardQuestUser(userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41280, true);
				 api_quest_RewardQuestUser(userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41280, true);
				 api_quest_RewardQuestUser(userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41280, true);
				 api_quest_RewardQuestUser(userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41280, true);
				 api_quest_RewardQuestUser(userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41280, true);
				 api_quest_RewardQuestUser(userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41280, true);
				 api_quest_RewardQuestUser(userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41280, true);
				 api_quest_RewardQuestUser(userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41280, true);
				 api_quest_RewardQuestUser(userObjID, 41280, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-6-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n880_academic--------------------------------------------------------------------------------
function sq15_4128_history_of_future_OnTalk_n880_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n880_academic-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n880_academic-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n880_academic-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n880_academic-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n880_academic-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n880_academic-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n880_academic-2-b" then 
	end
	if npc_talk_index == "n880_academic-2-c" then 
	end
	if npc_talk_index == "n880_academic-2-d" then 
	end
	if npc_talk_index == "n880_academic-2-e" then 
	end
	if npc_talk_index == "n880_academic-2-f" then 
	end
	if npc_talk_index == "n880_academic-2-g" then 
	end
	if npc_talk_index == "n880_academic-2-h" then 
	end
	if npc_talk_index == "n880_academic-2-i" then 
	end
	if npc_talk_index == "n880_academic-2-j" then 
	end
	if npc_talk_index == "n880_academic-2-k" then 
	end
	if npc_talk_index == "n880_academic-2-l" then 
	end
	if npc_talk_index == "n880_academic-2-m" then 
	end
	if npc_talk_index == "n880_academic-2-n" then 
	end
	if npc_talk_index == "n880_academic-2-o" then 
	end
	if npc_talk_index == "n880_academic-2-p" then 
	end
	if npc_talk_index == "n880_academic-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n880_academic-5-b" then 
	end
	if npc_talk_index == "n880_academic-5-c" then 
	end
	if npc_talk_index == "n880_academic-5-d" then 
	end
	if npc_talk_index == "n880_academic-5-a" then 
	end
	if npc_talk_index == "n880_academic-5-a" then 
	end
	if npc_talk_index == "n880_academic-5-e" then 
	end
	if npc_talk_index == "n880_academic-5-f" then 
	end
	if npc_talk_index == "n880_academic-5-g" then 
	end
	if npc_talk_index == "n880_academic-5-h" then 
	end
	if npc_talk_index == "n880_academic-5-i" then 
	end
	if npc_talk_index == "n880_academic-5-j" then 
	end
	if npc_talk_index == "n880_academic-5-k" then 
	end
	if npc_talk_index == "n880_academic-5-l" then 
	end
	if npc_talk_index == "n880_academic-5-m" then 
	end
	if npc_talk_index == "n880_academic-5-n" then 
	end
	if npc_talk_index == "n880_academic-5-o" then 
	end
	if npc_talk_index == "n880_academic-5-p" then 
	end
	if npc_talk_index == "n880_academic-5-q" then 
	end
	if npc_talk_index == "n880_academic-5-s" then 
	end
	if npc_talk_index == "n880_academic-5-r" then 
	end
	if npc_talk_index == "n880_academic-5-t" then 
	end
	if npc_talk_index == "n880_academic-5-t" then 
	end
	if npc_talk_index == "n880_academic-5-u" then 
	end
	if npc_talk_index == "n880_academic-5-v" then 
	end
	if npc_talk_index == "n880_academic-5-w" then 
	end
	if npc_talk_index == "n880_academic-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n881_trader_bellin--------------------------------------------------------------------------------
function sq15_4128_history_of_future_OnTalk_n881_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n881_trader_bellin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n881_trader_bellin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n881_trader_bellin-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n881_trader_bellin-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n881_trader_bellin-3-a" then 

				if api_quest_HasQuestItem(userObjID, 400338, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400338, api_quest_HasQuestItem(userObjID, 400338, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400339, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400339, api_quest_HasQuestItem(userObjID, 400339, 1));
				end
	end
	if npc_talk_index == "n881_trader_bellin-3-b" then 
	end
	if npc_talk_index == "n881_trader_bellin-3-c" then 
	end
	if npc_talk_index == "n881_trader_bellin-3-d" then 
	end
	if npc_talk_index == "n881_trader_bellin-3-e" then 
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_SetQuestStep(userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n882_soceress_master_stella--------------------------------------------------------------------------------
function sq15_4128_history_of_future_OnTalk_n882_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n882_soceress_master_stella-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n882_soceress_master_stella-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n882_soceress_master_stella-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n882_soceress_master_stella-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n882_soceress_master_stella-4-b" then 
	end
	if npc_talk_index == "n882_soceress_master_stella-4-c" then 
	end
	if npc_talk_index == "n882_soceress_master_stella-4-d" then 
	end
	if npc_talk_index == "n882_soceress_master_stella-4-e" then 
	end
	if npc_talk_index == "n882_soceress_master_stella-4-f" then 
	end
	if npc_talk_index == "n882_soceress_master_stella-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n091_trader_bellin--------------------------------------------------------------------------------
function sq15_4128_history_of_future_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n091_trader_bellin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n091_trader_bellin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4128, 1);
				api_quest_SetJournalStep(userObjID,4128, 1);
				api_quest_SetQuestStep(userObjID,4128, 1);
				npc_talk_index = "n091_trader_bellin-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400339, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400339, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n091_trader_bellin-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400338, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400338, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4128_history_of_future_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4128);
end

function sq15_4128_history_of_future_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4128);
end

function sq15_4128_history_of_future_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4128);
	local questID=4128;
end

function sq15_4128_history_of_future_OnRemoteStart( userObjID, questID )
end

function sq15_4128_history_of_future_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4128_history_of_future_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4128, 1);
				api_quest_SetJournalStep(userObjID,4128, 1);
				api_quest_SetQuestStep(userObjID,4128, 1);
				npc_talk_index = "n091_trader_bellin-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400339, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400339, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
end

</VillageServer>

<GameServer>
function sq15_4128_history_of_future_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 45 then
		sq15_4128_history_of_future_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 880 then
		sq15_4128_history_of_future_OnTalk_n880_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 881 then
		sq15_4128_history_of_future_OnTalk_n881_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 882 then
		sq15_4128_history_of_future_OnTalk_n882_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 91 then
		sq15_4128_history_of_future_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq15_4128_history_of_future_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n045_soceress_master_stella-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n045_soceress_master_stella-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-6-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-6-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-6-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-6-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41280, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-6-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n880_academic--------------------------------------------------------------------------------
function sq15_4128_history_of_future_OnTalk_n880_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n880_academic-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n880_academic-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n880_academic-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n880_academic-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n880_academic-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n880_academic-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n880_academic-2-b" then 
	end
	if npc_talk_index == "n880_academic-2-c" then 
	end
	if npc_talk_index == "n880_academic-2-d" then 
	end
	if npc_talk_index == "n880_academic-2-e" then 
	end
	if npc_talk_index == "n880_academic-2-f" then 
	end
	if npc_talk_index == "n880_academic-2-g" then 
	end
	if npc_talk_index == "n880_academic-2-h" then 
	end
	if npc_talk_index == "n880_academic-2-i" then 
	end
	if npc_talk_index == "n880_academic-2-j" then 
	end
	if npc_talk_index == "n880_academic-2-k" then 
	end
	if npc_talk_index == "n880_academic-2-l" then 
	end
	if npc_talk_index == "n880_academic-2-m" then 
	end
	if npc_talk_index == "n880_academic-2-n" then 
	end
	if npc_talk_index == "n880_academic-2-o" then 
	end
	if npc_talk_index == "n880_academic-2-p" then 
	end
	if npc_talk_index == "n880_academic-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n880_academic-5-b" then 
	end
	if npc_talk_index == "n880_academic-5-c" then 
	end
	if npc_talk_index == "n880_academic-5-d" then 
	end
	if npc_talk_index == "n880_academic-5-a" then 
	end
	if npc_talk_index == "n880_academic-5-a" then 
	end
	if npc_talk_index == "n880_academic-5-e" then 
	end
	if npc_talk_index == "n880_academic-5-f" then 
	end
	if npc_talk_index == "n880_academic-5-g" then 
	end
	if npc_talk_index == "n880_academic-5-h" then 
	end
	if npc_talk_index == "n880_academic-5-i" then 
	end
	if npc_talk_index == "n880_academic-5-j" then 
	end
	if npc_talk_index == "n880_academic-5-k" then 
	end
	if npc_talk_index == "n880_academic-5-l" then 
	end
	if npc_talk_index == "n880_academic-5-m" then 
	end
	if npc_talk_index == "n880_academic-5-n" then 
	end
	if npc_talk_index == "n880_academic-5-o" then 
	end
	if npc_talk_index == "n880_academic-5-p" then 
	end
	if npc_talk_index == "n880_academic-5-q" then 
	end
	if npc_talk_index == "n880_academic-5-s" then 
	end
	if npc_talk_index == "n880_academic-5-r" then 
	end
	if npc_talk_index == "n880_academic-5-t" then 
	end
	if npc_talk_index == "n880_academic-5-t" then 
	end
	if npc_talk_index == "n880_academic-5-u" then 
	end
	if npc_talk_index == "n880_academic-5-v" then 
	end
	if npc_talk_index == "n880_academic-5-w" then 
	end
	if npc_talk_index == "n880_academic-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n881_trader_bellin--------------------------------------------------------------------------------
function sq15_4128_history_of_future_OnTalk_n881_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n881_trader_bellin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n881_trader_bellin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n881_trader_bellin-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n881_trader_bellin-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n881_trader_bellin-3-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400338, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400338, api_quest_HasQuestItem( pRoom, userObjID, 400338, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400339, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400339, api_quest_HasQuestItem( pRoom, userObjID, 400339, 1));
				end
	end
	if npc_talk_index == "n881_trader_bellin-3-b" then 
	end
	if npc_talk_index == "n881_trader_bellin-3-c" then 
	end
	if npc_talk_index == "n881_trader_bellin-3-d" then 
	end
	if npc_talk_index == "n881_trader_bellin-3-e" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n882_soceress_master_stella--------------------------------------------------------------------------------
function sq15_4128_history_of_future_OnTalk_n882_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n882_soceress_master_stella-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n882_soceress_master_stella-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n882_soceress_master_stella-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n882_soceress_master_stella-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n882_soceress_master_stella-4-b" then 
	end
	if npc_talk_index == "n882_soceress_master_stella-4-c" then 
	end
	if npc_talk_index == "n882_soceress_master_stella-4-d" then 
	end
	if npc_talk_index == "n882_soceress_master_stella-4-e" then 
	end
	if npc_talk_index == "n882_soceress_master_stella-4-f" then 
	end
	if npc_talk_index == "n882_soceress_master_stella-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n091_trader_bellin--------------------------------------------------------------------------------
function sq15_4128_history_of_future_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n091_trader_bellin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n091_trader_bellin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4128, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4128, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4128, 1);
				npc_talk_index = "n091_trader_bellin-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400339, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400339, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n091_trader_bellin-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400338, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400338, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4128_history_of_future_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4128);
end

function sq15_4128_history_of_future_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4128);
end

function sq15_4128_history_of_future_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4128);
	local questID=4128;
end

function sq15_4128_history_of_future_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4128_history_of_future_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4128_history_of_future_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4128, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4128, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4128, 1);
				npc_talk_index = "n091_trader_bellin-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400339, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400339, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
end

</GameServer>