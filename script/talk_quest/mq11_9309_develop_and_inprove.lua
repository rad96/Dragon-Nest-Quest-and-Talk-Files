<VillageServer>

function mq11_9309_develop_and_inprove_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 574 then
		mq11_9309_develop_and_inprove_OnTalk_n574_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mq11_9309_develop_and_inprove_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n574_shadow_meow--------------------------------------------------------------------------------
function mq11_9309_develop_and_inprove_OnTalk_n574_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n574_shadow_meow-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n574_shadow_meow-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n574_shadow_meow-4-b" then 

				if api_quest_HasQuestItem(userObjID, 300573, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300573, api_quest_HasQuestItem(userObjID, 300573, 1));
				end
	end
	if npc_talk_index == "n574_shadow_meow-4-c" then 
	end
	if npc_talk_index == "n574_shadow_meow-4-d" then 
	end
	if npc_talk_index == "n574_shadow_meow-4-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93090, true);
				 api_quest_RewardQuestUser(userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93090, true);
				 api_quest_RewardQuestUser(userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93090, true);
				 api_quest_RewardQuestUser(userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93090, true);
				 api_quest_RewardQuestUser(userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93090, true);
				 api_quest_RewardQuestUser(userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93090, true);
				 api_quest_RewardQuestUser(userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93090, true);
				 api_quest_RewardQuestUser(userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93090, true);
				 api_quest_RewardQuestUser(userObjID, 93090, questID, 1);
			 end 
	end
	if npc_talk_index == "n574_shadow_meow-4-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9310, 2);
					api_quest_SetQuestStep(userObjID, 9310, 1);
					api_quest_SetJournalStep(userObjID, 9310, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n574_shadow_meow-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n574_shadow_meow-1", "mq11_9310_end_of_raide.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mq11_9309_develop_and_inprove_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n088_scholar_starshy-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9309, 2);
				api_quest_SetJournalStep(userObjID,9309, 1);
				api_quest_SetQuestStep(userObjID,9309, 1);
				npc_talk_index = "n088_scholar_starshy-1";

	end
	if npc_talk_index == "n088_scholar_starshy-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 951, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200951, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300021, 1);
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4" then 

				if api_quest_HasQuestItem(userObjID, 300021, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300021, api_quest_HasQuestItem(userObjID, 300021, 1));
				end
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300573, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300573, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_9309_develop_and_inprove_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9309);
	if qstep == 2 and CountIndex == 951 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300021, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300021, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200951 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300021, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300021, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300021 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function mq11_9309_develop_and_inprove_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9309);
	if qstep == 2 and CountIndex == 951 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200951 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300021 and Count >= TargetCount  then

	end
end

function mq11_9309_develop_and_inprove_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9309);
	local questID=9309;
end

function mq11_9309_develop_and_inprove_OnRemoteStart( userObjID, questID )
end

function mq11_9309_develop_and_inprove_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq11_9309_develop_and_inprove_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9309, 2);
				api_quest_SetJournalStep(userObjID,9309, 1);
				api_quest_SetQuestStep(userObjID,9309, 1);
				npc_talk_index = "n088_scholar_starshy-1";
end

</VillageServer>

<GameServer>
function mq11_9309_develop_and_inprove_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 574 then
		mq11_9309_develop_and_inprove_OnTalk_n574_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mq11_9309_develop_and_inprove_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n574_shadow_meow--------------------------------------------------------------------------------
function mq11_9309_develop_and_inprove_OnTalk_n574_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n574_shadow_meow-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n574_shadow_meow-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n574_shadow_meow-4-b" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300573, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300573, api_quest_HasQuestItem( pRoom, userObjID, 300573, 1));
				end
	end
	if npc_talk_index == "n574_shadow_meow-4-c" then 
	end
	if npc_talk_index == "n574_shadow_meow-4-d" then 
	end
	if npc_talk_index == "n574_shadow_meow-4-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93090, questID, 1);
			 end 
	end
	if npc_talk_index == "n574_shadow_meow-4-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9310, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9310, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9310, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n574_shadow_meow-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n574_shadow_meow-1", "mq11_9310_end_of_raide.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mq11_9309_develop_and_inprove_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n088_scholar_starshy-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9309, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9309, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9309, 1);
				npc_talk_index = "n088_scholar_starshy-1";

	end
	if npc_talk_index == "n088_scholar_starshy-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 951, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200951, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300021, 1);
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300021, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300021, api_quest_HasQuestItem( pRoom, userObjID, 300021, 1));
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300573, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300573, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_9309_develop_and_inprove_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9309);
	if qstep == 2 and CountIndex == 951 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300021, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300021, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200951 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300021, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300021, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300021 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function mq11_9309_develop_and_inprove_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9309);
	if qstep == 2 and CountIndex == 951 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200951 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300021 and Count >= TargetCount  then

	end
end

function mq11_9309_develop_and_inprove_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9309);
	local questID=9309;
end

function mq11_9309_develop_and_inprove_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq11_9309_develop_and_inprove_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq11_9309_develop_and_inprove_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9309, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9309, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9309, 1);
				npc_talk_index = "n088_scholar_starshy-1";
end

</GameServer>