<VillageServer>

function sq08_297_find_out_abyss2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 28 then
		sq08_297_find_out_abyss2_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_297_find_out_abyss2_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n028_scholar_bailey-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2971, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2972, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2973, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2974, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2975, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2976, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2977, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2977, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2977, false);
			 end 

	end
	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,297, 1);
				api_quest_SetJournalStep(userObjID,297, 1);
				api_quest_SetQuestStep(userObjID,297, 1);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-1-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 200433, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200450, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 3, 300124, 1);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300125, 1);
				api_quest_SetCountingInfo(userObjID, questID, 5, 3, 300126, 1);
	end
	if npc_talk_index == "n028_scholar_bailey-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2971, true);
				 api_quest_RewardQuestUser(userObjID, 2971, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2972, true);
				 api_quest_RewardQuestUser(userObjID, 2972, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2973, true);
				 api_quest_RewardQuestUser(userObjID, 2973, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2974, true);
				 api_quest_RewardQuestUser(userObjID, 2974, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2975, true);
				 api_quest_RewardQuestUser(userObjID, 2975, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2976, true);
				 api_quest_RewardQuestUser(userObjID, 2976, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2977, true);
				 api_quest_RewardQuestUser(userObjID, 2977, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2977, true);
				 api_quest_RewardQuestUser(userObjID, 2977, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2977, true);
				 api_quest_RewardQuestUser(userObjID, 2977, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-3-c" then 

				if api_quest_HasQuestItem(userObjID, 300124, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300124, api_quest_HasQuestItem(userObjID, 300124, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300125, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300125, api_quest_HasQuestItem(userObjID, 300125, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300126, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300126, api_quest_HasQuestItem(userObjID, 300126, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_297_find_out_abyss2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 297);
	if qstep == 2 and CountIndex == 200433 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300124, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300124, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300125, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300125, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200450 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300126, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300126, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300124 then

	end
	if qstep == 2 and CountIndex == 300125 then

	end
	if qstep == 2 and CountIndex == 300126 then

	end
end

function sq08_297_find_out_abyss2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 297);
	if qstep == 2 and CountIndex == 200433 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200413 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200450 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300124 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300125 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300126 and Count >= TargetCount  then

	end
end

function sq08_297_find_out_abyss2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 297);
	local questID=297;
end

function sq08_297_find_out_abyss2_OnRemoteStart( userObjID, questID )
end

function sq08_297_find_out_abyss2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_297_find_out_abyss2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,297, 1);
				api_quest_SetJournalStep(userObjID,297, 1);
				api_quest_SetQuestStep(userObjID,297, 1);
				npc_talk_index = "n028_scholar_bailey-1";
end

</VillageServer>

<GameServer>
function sq08_297_find_out_abyss2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 28 then
		sq08_297_find_out_abyss2_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_297_find_out_abyss2_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n028_scholar_bailey-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2971, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2972, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2973, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2974, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2975, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2976, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2977, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2977, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2977, false);
			 end 

	end
	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,297, 1);
				api_quest_SetJournalStep( pRoom, userObjID,297, 1);
				api_quest_SetQuestStep( pRoom, userObjID,297, 1);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-1-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 200433, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200450, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 3, 300124, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300125, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 3, 300126, 1);
	end
	if npc_talk_index == "n028_scholar_bailey-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2971, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2971, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2972, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2972, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2973, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2973, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2974, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2974, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2975, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2975, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2976, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2976, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2977, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2977, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2977, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2977, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2977, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2977, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-3-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300124, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300124, api_quest_HasQuestItem( pRoom, userObjID, 300124, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300125, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300125, api_quest_HasQuestItem( pRoom, userObjID, 300125, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300126, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300126, api_quest_HasQuestItem( pRoom, userObjID, 300126, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_297_find_out_abyss2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 297);
	if qstep == 2 and CountIndex == 200433 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300124, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300124, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300125, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300125, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200450 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300126, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300126, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300124 then

	end
	if qstep == 2 and CountIndex == 300125 then

	end
	if qstep == 2 and CountIndex == 300126 then

	end
end

function sq08_297_find_out_abyss2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 297);
	if qstep == 2 and CountIndex == 200433 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200413 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200450 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300124 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300125 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300126 and Count >= TargetCount  then

	end
end

function sq08_297_find_out_abyss2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 297);
	local questID=297;
end

function sq08_297_find_out_abyss2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_297_find_out_abyss2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_297_find_out_abyss2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,297, 1);
				api_quest_SetJournalStep( pRoom, userObjID,297, 1);
				api_quest_SetQuestStep( pRoom, userObjID,297, 1);
				npc_talk_index = "n028_scholar_bailey-1";
end

</GameServer>