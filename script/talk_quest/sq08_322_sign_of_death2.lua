<VillageServer>

function sq08_322_sign_of_death2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 28 then
		sq08_322_sign_of_death2_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_322_sign_of_death2_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3221, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3222, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3223, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3224, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3225, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3226, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3227, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3228, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3229, false);
			 end 

	end
	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,322, 1);
				api_quest_SetJournalStep(userObjID,322, 1);
				api_quest_SetQuestStep(userObjID,322, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 653, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 654, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200653, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200654, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 400126, 10);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3221, true);
				 api_quest_RewardQuestUser(userObjID, 3221, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3222, true);
				 api_quest_RewardQuestUser(userObjID, 3222, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3223, true);
				 api_quest_RewardQuestUser(userObjID, 3223, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3224, true);
				 api_quest_RewardQuestUser(userObjID, 3224, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3225, true);
				 api_quest_RewardQuestUser(userObjID, 3225, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3226, true);
				 api_quest_RewardQuestUser(userObjID, 3226, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3227, true);
				 api_quest_RewardQuestUser(userObjID, 3227, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3228, true);
				 api_quest_RewardQuestUser(userObjID, 3228, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3229, true);
				 api_quest_RewardQuestUser(userObjID, 3229, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400126, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400126, api_quest_HasQuestItem(userObjID, 400126, 1));
				end
	end
	if npc_talk_index == "n028_scholar_bailey-2-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-2-d" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_322_sign_of_death2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 322);
	if qstep == 1 and CountIndex == 653 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400126, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400126, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400126 then

	end
	if qstep == 1 and CountIndex == 654 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400126, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400126, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200653 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400126, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400126, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200654 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400126, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400126, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_322_sign_of_death2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 322);
	if qstep == 1 and CountIndex == 653 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400126 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 654 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200653 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200654 and Count >= TargetCount  then

	end
end

function sq08_322_sign_of_death2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 322);
	local questID=322;
end

function sq08_322_sign_of_death2_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 653, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 654, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200653, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200654, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 400126, 10);
end

function sq08_322_sign_of_death2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_322_sign_of_death2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,322, 1);
				api_quest_SetJournalStep(userObjID,322, 1);
				api_quest_SetQuestStep(userObjID,322, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 653, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 654, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200653, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200654, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 400126, 10);
				npc_talk_index = "n028_scholar_bailey-1";
end

</VillageServer>

<GameServer>
function sq08_322_sign_of_death2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 28 then
		sq08_322_sign_of_death2_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_322_sign_of_death2_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3221, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3222, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3223, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3224, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3225, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3226, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3227, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3228, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3229, false);
			 end 

	end
	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,322, 1);
				api_quest_SetJournalStep( pRoom, userObjID,322, 1);
				api_quest_SetQuestStep( pRoom, userObjID,322, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 653, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 654, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200653, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200654, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 400126, 10);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3221, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3221, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3222, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3222, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3223, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3223, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3224, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3224, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3225, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3225, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3226, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3226, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3227, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3227, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3228, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3228, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3229, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3229, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400126, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400126, api_quest_HasQuestItem( pRoom, userObjID, 400126, 1));
				end
	end
	if npc_talk_index == "n028_scholar_bailey-2-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-2-d" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_322_sign_of_death2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 322);
	if qstep == 1 and CountIndex == 653 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400126, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400126, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400126 then

	end
	if qstep == 1 and CountIndex == 654 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400126, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400126, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200653 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400126, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400126, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200654 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400126, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400126, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_322_sign_of_death2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 322);
	if qstep == 1 and CountIndex == 653 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400126 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 654 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200653 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200654 and Count >= TargetCount  then

	end
end

function sq08_322_sign_of_death2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 322);
	local questID=322;
end

function sq08_322_sign_of_death2_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 653, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 654, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200653, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200654, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 400126, 10);
end

function sq08_322_sign_of_death2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_322_sign_of_death2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,322, 1);
				api_quest_SetJournalStep( pRoom, userObjID,322, 1);
				api_quest_SetQuestStep( pRoom, userObjID,322, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 653, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 654, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200653, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200654, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 400126, 10);
				npc_talk_index = "n028_scholar_bailey-1";
end

</GameServer>