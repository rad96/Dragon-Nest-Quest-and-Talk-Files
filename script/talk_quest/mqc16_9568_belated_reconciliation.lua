<VillageServer>

function mqc16_9568_belated_reconciliation_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1774 then
		mqc16_9568_belated_reconciliation_OnTalk_n1774_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mqc16_9568_belated_reconciliation_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1782 then
		mqc16_9568_belated_reconciliation_OnTalk_n1782_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1792 then
		mqc16_9568_belated_reconciliation_OnTalk_n1792_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1779 then
		mqc16_9568_belated_reconciliation_OnTalk_n1779_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mqc16_9568_belated_reconciliation_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1774_gereint_kid--------------------------------------------------------------------------------
function mqc16_9568_belated_reconciliation_OnTalk_n1774_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1774_gereint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1774_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1774_gereint_kid-accepting-acceptted_1" then
				api_quest_AddQuest(userObjID,9568, 2);
				api_quest_SetJournalStep(userObjID,9568, 1);
				api_quest_SetQuestStep(userObjID,9568, 1);
				npc_talk_index = "n1774_gereint_kid-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc16_9568_belated_reconciliation_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1065_geraint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,9568, 2);
				api_quest_SetJournalStep(userObjID,9568, 1);
				api_quest_SetQuestStep(userObjID,9568, 1);
				npc_talk_index = "n1065_geraint_kid-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1782_rubinat--------------------------------------------------------------------------------
function mqc16_9568_belated_reconciliation_OnTalk_n1782_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1782_rubinat-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1782_rubinat-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1782_rubinat-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1782_rubinat-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1782_rubinat-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1782_rubinat-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1782_rubinat-1-b" then 
	end
	if npc_talk_index == "n1782_rubinat-1-c" then 
	end
	if npc_talk_index == "n1782_rubinat-1-d" then 
	end
	if npc_talk_index == "n1782_rubinat-1-e" then 
	end
	if npc_talk_index == "n1782_rubinat-1-f" then 
	end
	if npc_talk_index == "n1782_rubinat-1-g" then 
	end
	if npc_talk_index == "n1782_rubinat-1-h" then 
	end
	if npc_talk_index == "n1782_rubinat-1-i" then 
	end
	if npc_talk_index == "n1782_rubinat-1-j" then 
	end
	if npc_talk_index == "n1782_rubinat-1-k" then 
	end
	if npc_talk_index == "n1782_rubinat-1-l" then 
	end
	if npc_talk_index == "n1782_rubinat-1-m" then 
	end
	if npc_talk_index == "n1782_rubinat-1-n" then 
	end
	if npc_talk_index == "n1782_rubinat-1-o" then 
	end
	if npc_talk_index == "n1782_rubinat-1-p" then 
	end
	if npc_talk_index == "n1782_rubinat-1-q" then 
	end
	if npc_talk_index == "n1782_rubinat-1-r" then 
	end
	if npc_talk_index == "n1782_rubinat-1-s" then 
	end
	if npc_talk_index == "n1782_rubinat-1-t" then 
	end
	if npc_talk_index == "n1782_rubinat-1-u" then 
	end
	if npc_talk_index == "n1782_rubinat-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1782_rubinat-3-b" then 
	end
	if npc_talk_index == "n1782_rubinat-3-c" then 
	end
	if npc_talk_index == "n1782_rubinat-3-c" then 
	end
	if npc_talk_index == "n1782_rubinat-3-d" then 
	end
	if npc_talk_index == "n1782_rubinat-3-class_check" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1782_rubinat-3-f";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1782_rubinat-3-n";

				else
									npc_talk_index = "n1782_rubinat-3-e";

				end

				end
	end
	if npc_talk_index == "n1782_rubinat-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1782_rubinat-3-g" then 
	end
	if npc_talk_index == "n1782_rubinat-3-h" then 
	end
	if npc_talk_index == "n1782_rubinat-3-h" then 
	end
	if npc_talk_index == "n1782_rubinat-3-i" then 
	end
	if npc_talk_index == "n1782_rubinat-3-j" then 
	end
	if npc_talk_index == "n1782_rubinat-3-k" then 
	end
	if npc_talk_index == "n1782_rubinat-3-l" then 
	end
	if npc_talk_index == "n1782_rubinat-3-m" then 
	end
	if npc_talk_index == "n1782_rubinat-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1782_rubinat-3-o" then 
	end
	if npc_talk_index == "n1782_rubinat-3-p" then 
	end
	if npc_talk_index == "n1782_rubinat-3-p" then 
	end
	if npc_talk_index == "n1782_rubinat-3-p" then 
	end
	if npc_talk_index == "n1782_rubinat-3-q" then 
	end
	if npc_talk_index == "n1782_rubinat-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1782_rubinat-6-b" then 
	end
	if npc_talk_index == "n1782_rubinat-6-c" then 
	end
	if npc_talk_index == "n1782_rubinat-6-d" then 
	end
	if npc_talk_index == "n1782_rubinat-6-d" then 
	end
	if npc_talk_index == "n1782_rubinat-6-e" then 
	end
	if npc_talk_index == "n1782_rubinat-6-e" then 
	end
	if npc_talk_index == "n1782_rubinat-6-f" then 
	end
	if npc_talk_index == "n1782_rubinat-6-g" then 
	end
	if npc_talk_index == "n1782_rubinat-6-g" then 
	end
	if npc_talk_index == "n1782_rubinat-6-g" then 
	end
	if npc_talk_index == "n1782_rubinat-6-h" then 
	end
	if npc_talk_index == "n1782_rubinat-6-h" then 
	end
	if npc_talk_index == "n1782_rubinat-6-i" then 
	end
	if npc_talk_index == "n1782_rubinat-6-i" then 
	end
	if npc_talk_index == "n1782_rubinat-6-i" then 
	end
	if npc_talk_index == "n1782_rubinat-6-j" then 
	end
	if npc_talk_index == "n1782_rubinat-6-j" then 
	end
	if npc_talk_index == "n1782_rubinat-6-j" then 
	end
	if npc_talk_index == "n1782_rubinat-6-k" then 
	end
	if npc_talk_index == "n1782_rubinat-6-l" then 
	end
	if npc_talk_index == "n1782_rubinat-6-m" then 
	end
	if npc_talk_index == "n1782_rubinat-6-m" then 
	end
	if npc_talk_index == "n1782_rubinat-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1792_velskud--------------------------------------------------------------------------------
function mqc16_9568_belated_reconciliation_OnTalk_n1792_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1792_velskud-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1792_velskud-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1792_velskud-5-b" then 
	end
	if npc_talk_index == "n1792_velskud-5-c" then 
	end
	if npc_talk_index == "n1792_velskud-5-d" then 
	end
	if npc_talk_index == "n1792_velskud-5-e" then 
	end
	if npc_talk_index == "n1792_velskud-5-f" then 
	end
	if npc_talk_index == "n1792_velskud-5-g" then 
	end
	if npc_talk_index == "n1792_velskud-5-h" then 
	end
	if npc_talk_index == "n1792_velskud-5-i" then 
	end
	if npc_talk_index == "n1792_velskud-5-j" then 
	end
	if npc_talk_index == "n1792_velskud-5-k" then 
	end
	if npc_talk_index == "n1792_velskud-5-l" then 
	end
	if npc_talk_index == "n1792_velskud-5-m" then 
	end
	if npc_talk_index == "n1792_velskud-5-n" then 
	end
	if npc_talk_index == "n1792_velskud-5-o" then 
	end
	if npc_talk_index == "n1792_velskud-5-p" then 
	end
	if npc_talk_index == "n1792_velskud-5-q" then 
	end
	if npc_talk_index == "n1792_velskud-5-r" then 
	end
	if npc_talk_index == "n1792_velskud-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1779_gereint_kid--------------------------------------------------------------------------------
function mqc16_9568_belated_reconciliation_OnTalk_n1779_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1779_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mqc16_9568_belated_reconciliation_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n822_teramai-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-7-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95680, true);
				 api_quest_RewardQuestUser(userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95680, true);
				 api_quest_RewardQuestUser(userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95680, true);
				 api_quest_RewardQuestUser(userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95680, true);
				 api_quest_RewardQuestUser(userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95680, true);
				 api_quest_RewardQuestUser(userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95680, true);
				 api_quest_RewardQuestUser(userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95680, true);
				 api_quest_RewardQuestUser(userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95680, true);
				 api_quest_RewardQuestUser(userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95680, true);
				 api_quest_RewardQuestUser(userObjID, 95680, questID, 1);
			 end 
	end
	if npc_talk_index == "n822_teramai-7-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n822_teramai-7-c" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9568_belated_reconciliation_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9568);
end

function mqc16_9568_belated_reconciliation_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9568);
end

function mqc16_9568_belated_reconciliation_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9568);
	local questID=9568;
end

function mqc16_9568_belated_reconciliation_OnRemoteStart( userObjID, questID )
end

function mqc16_9568_belated_reconciliation_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc16_9568_belated_reconciliation_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mqc16_9568_belated_reconciliation_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1774 then
		mqc16_9568_belated_reconciliation_OnTalk_n1774_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mqc16_9568_belated_reconciliation_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1782 then
		mqc16_9568_belated_reconciliation_OnTalk_n1782_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1792 then
		mqc16_9568_belated_reconciliation_OnTalk_n1792_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1779 then
		mqc16_9568_belated_reconciliation_OnTalk_n1779_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mqc16_9568_belated_reconciliation_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1774_gereint_kid--------------------------------------------------------------------------------
function mqc16_9568_belated_reconciliation_OnTalk_n1774_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1774_gereint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1774_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1774_gereint_kid-accepting-acceptted_1" then
				api_quest_AddQuest( pRoom, userObjID,9568, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9568, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9568, 1);
				npc_talk_index = "n1774_gereint_kid-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc16_9568_belated_reconciliation_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1065_geraint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,9568, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9568, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9568, 1);
				npc_talk_index = "n1065_geraint_kid-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1782_rubinat--------------------------------------------------------------------------------
function mqc16_9568_belated_reconciliation_OnTalk_n1782_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1782_rubinat-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1782_rubinat-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1782_rubinat-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1782_rubinat-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1782_rubinat-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1782_rubinat-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1782_rubinat-1-b" then 
	end
	if npc_talk_index == "n1782_rubinat-1-c" then 
	end
	if npc_talk_index == "n1782_rubinat-1-d" then 
	end
	if npc_talk_index == "n1782_rubinat-1-e" then 
	end
	if npc_talk_index == "n1782_rubinat-1-f" then 
	end
	if npc_talk_index == "n1782_rubinat-1-g" then 
	end
	if npc_talk_index == "n1782_rubinat-1-h" then 
	end
	if npc_talk_index == "n1782_rubinat-1-i" then 
	end
	if npc_talk_index == "n1782_rubinat-1-j" then 
	end
	if npc_talk_index == "n1782_rubinat-1-k" then 
	end
	if npc_talk_index == "n1782_rubinat-1-l" then 
	end
	if npc_talk_index == "n1782_rubinat-1-m" then 
	end
	if npc_talk_index == "n1782_rubinat-1-n" then 
	end
	if npc_talk_index == "n1782_rubinat-1-o" then 
	end
	if npc_talk_index == "n1782_rubinat-1-p" then 
	end
	if npc_talk_index == "n1782_rubinat-1-q" then 
	end
	if npc_talk_index == "n1782_rubinat-1-r" then 
	end
	if npc_talk_index == "n1782_rubinat-1-s" then 
	end
	if npc_talk_index == "n1782_rubinat-1-t" then 
	end
	if npc_talk_index == "n1782_rubinat-1-u" then 
	end
	if npc_talk_index == "n1782_rubinat-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1782_rubinat-3-b" then 
	end
	if npc_talk_index == "n1782_rubinat-3-c" then 
	end
	if npc_talk_index == "n1782_rubinat-3-c" then 
	end
	if npc_talk_index == "n1782_rubinat-3-d" then 
	end
	if npc_talk_index == "n1782_rubinat-3-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1782_rubinat-3-f";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1782_rubinat-3-n";

				else
									npc_talk_index = "n1782_rubinat-3-e";

				end

				end
	end
	if npc_talk_index == "n1782_rubinat-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1782_rubinat-3-g" then 
	end
	if npc_talk_index == "n1782_rubinat-3-h" then 
	end
	if npc_talk_index == "n1782_rubinat-3-h" then 
	end
	if npc_talk_index == "n1782_rubinat-3-i" then 
	end
	if npc_talk_index == "n1782_rubinat-3-j" then 
	end
	if npc_talk_index == "n1782_rubinat-3-k" then 
	end
	if npc_talk_index == "n1782_rubinat-3-l" then 
	end
	if npc_talk_index == "n1782_rubinat-3-m" then 
	end
	if npc_talk_index == "n1782_rubinat-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1782_rubinat-3-o" then 
	end
	if npc_talk_index == "n1782_rubinat-3-p" then 
	end
	if npc_talk_index == "n1782_rubinat-3-p" then 
	end
	if npc_talk_index == "n1782_rubinat-3-p" then 
	end
	if npc_talk_index == "n1782_rubinat-3-q" then 
	end
	if npc_talk_index == "n1782_rubinat-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1782_rubinat-6-b" then 
	end
	if npc_talk_index == "n1782_rubinat-6-c" then 
	end
	if npc_talk_index == "n1782_rubinat-6-d" then 
	end
	if npc_talk_index == "n1782_rubinat-6-d" then 
	end
	if npc_talk_index == "n1782_rubinat-6-e" then 
	end
	if npc_talk_index == "n1782_rubinat-6-e" then 
	end
	if npc_talk_index == "n1782_rubinat-6-f" then 
	end
	if npc_talk_index == "n1782_rubinat-6-g" then 
	end
	if npc_talk_index == "n1782_rubinat-6-g" then 
	end
	if npc_talk_index == "n1782_rubinat-6-g" then 
	end
	if npc_talk_index == "n1782_rubinat-6-h" then 
	end
	if npc_talk_index == "n1782_rubinat-6-h" then 
	end
	if npc_talk_index == "n1782_rubinat-6-i" then 
	end
	if npc_talk_index == "n1782_rubinat-6-i" then 
	end
	if npc_talk_index == "n1782_rubinat-6-i" then 
	end
	if npc_talk_index == "n1782_rubinat-6-j" then 
	end
	if npc_talk_index == "n1782_rubinat-6-j" then 
	end
	if npc_talk_index == "n1782_rubinat-6-j" then 
	end
	if npc_talk_index == "n1782_rubinat-6-k" then 
	end
	if npc_talk_index == "n1782_rubinat-6-l" then 
	end
	if npc_talk_index == "n1782_rubinat-6-m" then 
	end
	if npc_talk_index == "n1782_rubinat-6-m" then 
	end
	if npc_talk_index == "n1782_rubinat-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1792_velskud--------------------------------------------------------------------------------
function mqc16_9568_belated_reconciliation_OnTalk_n1792_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1792_velskud-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1792_velskud-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1792_velskud-5-b" then 
	end
	if npc_talk_index == "n1792_velskud-5-c" then 
	end
	if npc_talk_index == "n1792_velskud-5-d" then 
	end
	if npc_talk_index == "n1792_velskud-5-e" then 
	end
	if npc_talk_index == "n1792_velskud-5-f" then 
	end
	if npc_talk_index == "n1792_velskud-5-g" then 
	end
	if npc_talk_index == "n1792_velskud-5-h" then 
	end
	if npc_talk_index == "n1792_velskud-5-i" then 
	end
	if npc_talk_index == "n1792_velskud-5-j" then 
	end
	if npc_talk_index == "n1792_velskud-5-k" then 
	end
	if npc_talk_index == "n1792_velskud-5-l" then 
	end
	if npc_talk_index == "n1792_velskud-5-m" then 
	end
	if npc_talk_index == "n1792_velskud-5-n" then 
	end
	if npc_talk_index == "n1792_velskud-5-o" then 
	end
	if npc_talk_index == "n1792_velskud-5-p" then 
	end
	if npc_talk_index == "n1792_velskud-5-q" then 
	end
	if npc_talk_index == "n1792_velskud-5-r" then 
	end
	if npc_talk_index == "n1792_velskud-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1779_gereint_kid--------------------------------------------------------------------------------
function mqc16_9568_belated_reconciliation_OnTalk_n1779_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1779_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mqc16_9568_belated_reconciliation_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n822_teramai-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-7-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95680, questID, 1);
			 end 
	end
	if npc_talk_index == "n822_teramai-7-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n822_teramai-7-c" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9568_belated_reconciliation_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9568);
end

function mqc16_9568_belated_reconciliation_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9568);
end

function mqc16_9568_belated_reconciliation_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9568);
	local questID=9568;
end

function mqc16_9568_belated_reconciliation_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc16_9568_belated_reconciliation_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc16_9568_belated_reconciliation_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>