<VillageServer>

function sq08_352_beyond_the_nightmare_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 199 then
		sq08_352_beyond_the_nightmare_OnTalk_n199_darklair_castia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 201 then
		sq08_352_beyond_the_nightmare_OnTalk_n201_darklair_millefeuille(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n199_darklair_castia--------------------------------------------------------------------------------
function sq08_352_beyond_the_nightmare_OnTalk_n199_darklair_castia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n199_darklair_castia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n199_darklair_castia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n199_darklair_castia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n199_darklair_castia-accepting-acceppted" then
				api_quest_AddQuest(userObjID,352, 1);
				api_quest_SetJournalStep(userObjID,352, 1);
				api_quest_SetQuestStep(userObjID,352, 1);
				api_npc_NextTalk(userObjID, npcObjID, "n199_darklair_castia-1-a", npc_talk_target);

	end
	if npc_talk_index == "n199_darklair_castia-1-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3520, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3520, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3520, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3520, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3520, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3520, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3520, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3520, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3520, false);
			 end 
	end
	if npc_talk_index == "n199_darklair_castia-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n201_darklair_millefeuille--------------------------------------------------------------------------------
function sq08_352_beyond_the_nightmare_OnTalk_n201_darklair_millefeuille(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n201_darklair_millefeuille-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n201_darklair_millefeuille-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n201_darklair_millefeuille-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n201_darklair_millefeuille-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n201_darklair_millefeuille-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n201_darklair_millefeuille-2-b" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-2-c" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-2-d" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-2-e" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-2-f" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 400061, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 400093, 1);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-b" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-c" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-d" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-e" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-e" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3520, true);
				 api_quest_RewardQuestUser(userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3520, true);
				 api_quest_RewardQuestUser(userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3520, true);
				 api_quest_RewardQuestUser(userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3520, true);
				 api_quest_RewardQuestUser(userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3520, true);
				 api_quest_RewardQuestUser(userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3520, true);
				 api_quest_RewardQuestUser(userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3520, true);
				 api_quest_RewardQuestUser(userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3520, true);
				 api_quest_RewardQuestUser(userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3520, true);
				 api_quest_RewardQuestUser(userObjID, 3520, questID, 1);
			 end 
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_352_beyond_the_nightmare_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 352);
	if qstep == 3 and CountIndex == 400061 then

	end
	if qstep == 3 and CountIndex == 400093 then

	end
end

function sq08_352_beyond_the_nightmare_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 352);
	if qstep == 3 and CountIndex == 400061 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 400093 and Count >= TargetCount  then

	end
end

function sq08_352_beyond_the_nightmare_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 352);
	local questID=352;
	if qstep == 3 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
end

function sq08_352_beyond_the_nightmare_OnRemoteStart( userObjID, questID )
end

function sq08_352_beyond_the_nightmare_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_352_beyond_the_nightmare_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq08_352_beyond_the_nightmare_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 199 then
		sq08_352_beyond_the_nightmare_OnTalk_n199_darklair_castia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 201 then
		sq08_352_beyond_the_nightmare_OnTalk_n201_darklair_millefeuille( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n199_darklair_castia--------------------------------------------------------------------------------
function sq08_352_beyond_the_nightmare_OnTalk_n199_darklair_castia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n199_darklair_castia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n199_darklair_castia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n199_darklair_castia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n199_darklair_castia-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,352, 1);
				api_quest_SetJournalStep( pRoom, userObjID,352, 1);
				api_quest_SetQuestStep( pRoom, userObjID,352, 1);
				api_npc_NextTalk( pRoom, userObjID, npcObjID, "n199_darklair_castia-1-a", npc_talk_target);

	end
	if npc_talk_index == "n199_darklair_castia-1-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, false);
			 end 
	end
	if npc_talk_index == "n199_darklair_castia-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n201_darklair_millefeuille--------------------------------------------------------------------------------
function sq08_352_beyond_the_nightmare_OnTalk_n201_darklair_millefeuille( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n201_darklair_millefeuille-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n201_darklair_millefeuille-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n201_darklair_millefeuille-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n201_darklair_millefeuille-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n201_darklair_millefeuille-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n201_darklair_millefeuille-2-b" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-2-c" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-2-d" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-2-e" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-2-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 400061, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 400093, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-b" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-c" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-d" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-e" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-e" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3520, questID, 1);
			 end 
	end
	if npc_talk_index == "n201_darklair_millefeuille-4-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_352_beyond_the_nightmare_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 352);
	if qstep == 3 and CountIndex == 400061 then

	end
	if qstep == 3 and CountIndex == 400093 then

	end
end

function sq08_352_beyond_the_nightmare_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 352);
	if qstep == 3 and CountIndex == 400061 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 400093 and Count >= TargetCount  then

	end
end

function sq08_352_beyond_the_nightmare_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 352);
	local questID=352;
	if qstep == 3 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
end

function sq08_352_beyond_the_nightmare_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_352_beyond_the_nightmare_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_352_beyond_the_nightmare_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>