<VillageServer>

function mqc10_9548_unanswered_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1737 then
		mqc10_9548_unanswered_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 874 then
		mqc10_9548_unanswered_OnTalk_n874_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1725 then
		mqc10_9548_unanswered_OnTalk_n1725_comelina_mad2(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1737_eltia--------------------------------------------------------------------------------
function mqc10_9548_unanswered_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1737_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1737_eltia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1737_eltia-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9548, 2);
				api_quest_SetJournalStep(userObjID,9548, 1);
				api_quest_SetQuestStep(userObjID,9548, 1);
				npc_talk_index = "n1737_eltia-1";

	end
	if npc_talk_index == "n1737_eltia-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1737_eltia-5-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95480, true);
				 api_quest_RewardQuestUser(userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95480, true);
				 api_quest_RewardQuestUser(userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95480, true);
				 api_quest_RewardQuestUser(userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95480, true);
				 api_quest_RewardQuestUser(userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95480, true);
				 api_quest_RewardQuestUser(userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95480, true);
				 api_quest_RewardQuestUser(userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95480, true);
				 api_quest_RewardQuestUser(userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95480, true);
				 api_quest_RewardQuestUser(userObjID, 95480, questID, 1);
			 end 
	end
	if npc_talk_index == "n1737_eltia-5-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9549, 2);
					api_quest_SetQuestStep(userObjID, 9549, 1);
					api_quest_SetJournalStep(userObjID, 9549, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1737_eltia-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1737_eltia-1", "mqc10_9549_chase_kaye.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n874_academic--------------------------------------------------------------------------------
function mqc10_9548_unanswered_OnTalk_n874_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n874_academic-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n874_academic-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n874_academic-2-b" then 
	end
	if npc_talk_index == "n874_academic-2-c" then 
	end
	if npc_talk_index == "n874_academic-2-d" then 
	end
	if npc_talk_index == "n874_academic-2-e" then 
	end
	if npc_talk_index == "n874_academic-2-f" then 
	end
	if npc_talk_index == "n874_academic-2-g" then 
	end
	if npc_talk_index == "n874_academic-2-h" then 
	end
	if npc_talk_index == "n874_academic-2-i" then 
	end
	if npc_talk_index == "n874_academic-2-j" then 
	end
	if npc_talk_index == "n874_academic-2-k" then 
	end
	if npc_talk_index == "n874_academic-2-l" then 
	end
	if npc_talk_index == "n874_academic-2-m" then 
	end
	if npc_talk_index == "n874_academic-2-n" then 
	end
	if npc_talk_index == "n874_academic-2-o" then 
	end
	if npc_talk_index == "n874_academic-2-p" then 
	end
	if npc_talk_index == "n874_academic-2-q" then 
	end
	if npc_talk_index == "n874_academic-2-r" then 
	end
	if npc_talk_index == "n874_academic-2-s" then 
	end
	if npc_talk_index == "n874_academic-2-t" then 
	end
	if npc_talk_index == "n874_academic-2-u" then 
	end
	if npc_talk_index == "n874_academic-2-v" then 
	end
	if npc_talk_index == "n874_academic-2-w" then 
	end
	if npc_talk_index == "n874_academic-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1725_comelina_mad2--------------------------------------------------------------------------------
function mqc10_9548_unanswered_OnTalk_n1725_comelina_mad2(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1725_comelina_mad2-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1725_comelina_mad2-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1725_comelina_mad2-4-b" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-c" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-d" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-e" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-f" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-g" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-h" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-i" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc10_9548_unanswered_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9548);
end

function mqc10_9548_unanswered_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9548);
end

function mqc10_9548_unanswered_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9548);
	local questID=9548;
end

function mqc10_9548_unanswered_OnRemoteStart( userObjID, questID )
end

function mqc10_9548_unanswered_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc10_9548_unanswered_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9548, 2);
				api_quest_SetJournalStep(userObjID,9548, 1);
				api_quest_SetQuestStep(userObjID,9548, 1);
				npc_talk_index = "n1737_eltia-1";
end

</VillageServer>

<GameServer>
function mqc10_9548_unanswered_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1737 then
		mqc10_9548_unanswered_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 874 then
		mqc10_9548_unanswered_OnTalk_n874_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1725 then
		mqc10_9548_unanswered_OnTalk_n1725_comelina_mad2( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1737_eltia--------------------------------------------------------------------------------
function mqc10_9548_unanswered_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1737_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1737_eltia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1737_eltia-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9548, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9548, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9548, 1);
				npc_talk_index = "n1737_eltia-1";

	end
	if npc_talk_index == "n1737_eltia-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1737_eltia-5-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95480, questID, 1);
			 end 
	end
	if npc_talk_index == "n1737_eltia-5-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9549, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9549, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9549, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1737_eltia-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1737_eltia-1", "mqc10_9549_chase_kaye.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n874_academic--------------------------------------------------------------------------------
function mqc10_9548_unanswered_OnTalk_n874_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n874_academic-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n874_academic-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n874_academic-2-b" then 
	end
	if npc_talk_index == "n874_academic-2-c" then 
	end
	if npc_talk_index == "n874_academic-2-d" then 
	end
	if npc_talk_index == "n874_academic-2-e" then 
	end
	if npc_talk_index == "n874_academic-2-f" then 
	end
	if npc_talk_index == "n874_academic-2-g" then 
	end
	if npc_talk_index == "n874_academic-2-h" then 
	end
	if npc_talk_index == "n874_academic-2-i" then 
	end
	if npc_talk_index == "n874_academic-2-j" then 
	end
	if npc_talk_index == "n874_academic-2-k" then 
	end
	if npc_talk_index == "n874_academic-2-l" then 
	end
	if npc_talk_index == "n874_academic-2-m" then 
	end
	if npc_talk_index == "n874_academic-2-n" then 
	end
	if npc_talk_index == "n874_academic-2-o" then 
	end
	if npc_talk_index == "n874_academic-2-p" then 
	end
	if npc_talk_index == "n874_academic-2-q" then 
	end
	if npc_talk_index == "n874_academic-2-r" then 
	end
	if npc_talk_index == "n874_academic-2-s" then 
	end
	if npc_talk_index == "n874_academic-2-t" then 
	end
	if npc_talk_index == "n874_academic-2-u" then 
	end
	if npc_talk_index == "n874_academic-2-v" then 
	end
	if npc_talk_index == "n874_academic-2-w" then 
	end
	if npc_talk_index == "n874_academic-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1725_comelina_mad2--------------------------------------------------------------------------------
function mqc10_9548_unanswered_OnTalk_n1725_comelina_mad2( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1725_comelina_mad2-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1725_comelina_mad2-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1725_comelina_mad2-4-b" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-c" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-d" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-e" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-f" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-g" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-h" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-4-i" then 
	end
	if npc_talk_index == "n1725_comelina_mad2-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc10_9548_unanswered_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9548);
end

function mqc10_9548_unanswered_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9548);
end

function mqc10_9548_unanswered_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9548);
	local questID=9548;
end

function mqc10_9548_unanswered_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc10_9548_unanswered_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc10_9548_unanswered_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9548, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9548, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9548, 1);
				npc_talk_index = "n1737_eltia-1";
end

</GameServer>