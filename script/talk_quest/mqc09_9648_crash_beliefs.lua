<VillageServer>

function mqc09_9648_crash_beliefs_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1928 then
		mqc09_9648_crash_beliefs_OnTalk_n1928_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1913 then
		mqc09_9648_crash_beliefs_OnTalk_n1913_sorceress_lantana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1920 then
		mqc09_9648_crash_beliefs_OnTalk_n1920_suriya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1930 then
		mqc09_9648_crash_beliefs_OnTalk_n1930_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1737 then
		mqc09_9648_crash_beliefs_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1928_shaolong--------------------------------------------------------------------------------
function mqc09_9648_crash_beliefs_OnTalk_n1928_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1928_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1928_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1928_shaolong-1-b" then 
	end
	if npc_talk_index == "n1928_shaolong-1-c" then 
	end
	if npc_talk_index == "n1928_shaolong-1-d" then 
	end
	if npc_talk_index == "n1928_shaolong-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1913_sorceress_lantana--------------------------------------------------------------------------------
function mqc09_9648_crash_beliefs_OnTalk_n1913_sorceress_lantana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1913_sorceress_lantana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1913_sorceress_lantana-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1913_sorceress_lantana-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1913_sorceress_lantana-3-b" then 
	end
	if npc_talk_index == "n1913_sorceress_lantana-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1920_suriya--------------------------------------------------------------------------------
function mqc09_9648_crash_beliefs_OnTalk_n1920_suriya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1920_suriya-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1920_suriya-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1920_suriya-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1920_suriya-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1920_suriya-4-b" then 
	end
	if npc_talk_index == "n1920_suriya-4-c" then 
	end
	if npc_talk_index == "n1920_suriya-4-d" then 
	end
	if npc_talk_index == "n1920_suriya-4-e" then 
	end
	if npc_talk_index == "n1920_suriya-4-f" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n1920_suriya-5-b" then 
	end
	if npc_talk_index == "n1920_suriya-5-c" then 
	end
	if npc_talk_index == "n1920_suriya-5-d" then 
	end
	if npc_talk_index == "n1920_suriya-5-e" then 
	end
	if npc_talk_index == "n1920_suriya-5-f" then 
	end
	if npc_talk_index == "n1920_suriya-5-g" then 
	end
	if npc_talk_index == "n1920_suriya-5-h" then 
	end
	if npc_talk_index == "n1920_suriya-5-i" then 
	end
	if npc_talk_index == "n1920_suriya-5-j" then 
	end
	if npc_talk_index == "n1920_suriya-5-k" then 
	end
	if npc_talk_index == "n1920_suriya-5-l" then 
	end
	if npc_talk_index == "n1920_suriya-5-m" then 
	end
	if npc_talk_index == "n1920_suriya-5-n" then 
	end
	if npc_talk_index == "n1920_suriya-5-o" then 
	end
	if npc_talk_index == "n1920_suriya-5-p" then 
	end
	if npc_talk_index == "n1920_suriya-5-q" then 
	end
	if npc_talk_index == "n1920_suriya-5-r" then 
	end
	if npc_talk_index == "n1920_suriya-5-s" then 
	end
	if npc_talk_index == "n1920_suriya-5-t" then 
	end
	if npc_talk_index == "n1920_suriya-5-u" then 
	end
	if npc_talk_index == "n1920_suriya-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1930_shaolong--------------------------------------------------------------------------------
function mqc09_9648_crash_beliefs_OnTalk_n1930_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1930_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1930_shaolong-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1930_shaolong-6-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 96480, true);
				 api_quest_RewardQuestUser(userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 96480, true);
				 api_quest_RewardQuestUser(userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 96480, true);
				 api_quest_RewardQuestUser(userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 96480, true);
				 api_quest_RewardQuestUser(userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 96480, true);
				 api_quest_RewardQuestUser(userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 96480, true);
				 api_quest_RewardQuestUser(userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 96480, true);
				 api_quest_RewardQuestUser(userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 96480, true);
				 api_quest_RewardQuestUser(userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 96480, true);
				 api_quest_RewardQuestUser(userObjID, 96480, questID, 1);
			 end 
	end
	if npc_talk_index == "n1930_shaolong-6-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9649, 2);
					api_quest_SetQuestStep(userObjID, 9649, 1);
					api_quest_SetJournalStep(userObjID, 9649, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1930_shaolong-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1930_shaolong-1", "mqc09_9649_dilemma.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1737_eltia--------------------------------------------------------------------------------
function mqc09_9648_crash_beliefs_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1737_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9648, 2);
				api_quest_SetJournalStep(userObjID,9648, 1);
				api_quest_SetQuestStep(userObjID,9648, 1);
				npc_talk_index = "n1737_eltia-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc09_9648_crash_beliefs_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9648);
end

function mqc09_9648_crash_beliefs_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9648);
end

function mqc09_9648_crash_beliefs_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9648);
	local questID=9648;
end

function mqc09_9648_crash_beliefs_OnRemoteStart( userObjID, questID )
end

function mqc09_9648_crash_beliefs_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc09_9648_crash_beliefs_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9648, 2);
				api_quest_SetJournalStep(userObjID,9648, 1);
				api_quest_SetQuestStep(userObjID,9648, 1);
				npc_talk_index = "n1737_eltia-1";
end

</VillageServer>

<GameServer>
function mqc09_9648_crash_beliefs_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1928 then
		mqc09_9648_crash_beliefs_OnTalk_n1928_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1913 then
		mqc09_9648_crash_beliefs_OnTalk_n1913_sorceress_lantana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1920 then
		mqc09_9648_crash_beliefs_OnTalk_n1920_suriya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1930 then
		mqc09_9648_crash_beliefs_OnTalk_n1930_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1737 then
		mqc09_9648_crash_beliefs_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1928_shaolong--------------------------------------------------------------------------------
function mqc09_9648_crash_beliefs_OnTalk_n1928_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1928_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1928_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1928_shaolong-1-b" then 
	end
	if npc_talk_index == "n1928_shaolong-1-c" then 
	end
	if npc_talk_index == "n1928_shaolong-1-d" then 
	end
	if npc_talk_index == "n1928_shaolong-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1913_sorceress_lantana--------------------------------------------------------------------------------
function mqc09_9648_crash_beliefs_OnTalk_n1913_sorceress_lantana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1913_sorceress_lantana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1913_sorceress_lantana-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1913_sorceress_lantana-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1913_sorceress_lantana-3-b" then 
	end
	if npc_talk_index == "n1913_sorceress_lantana-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1920_suriya--------------------------------------------------------------------------------
function mqc09_9648_crash_beliefs_OnTalk_n1920_suriya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1920_suriya-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1920_suriya-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1920_suriya-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1920_suriya-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1920_suriya-4-b" then 
	end
	if npc_talk_index == "n1920_suriya-4-c" then 
	end
	if npc_talk_index == "n1920_suriya-4-d" then 
	end
	if npc_talk_index == "n1920_suriya-4-e" then 
	end
	if npc_talk_index == "n1920_suriya-4-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n1920_suriya-5-b" then 
	end
	if npc_talk_index == "n1920_suriya-5-c" then 
	end
	if npc_talk_index == "n1920_suriya-5-d" then 
	end
	if npc_talk_index == "n1920_suriya-5-e" then 
	end
	if npc_talk_index == "n1920_suriya-5-f" then 
	end
	if npc_talk_index == "n1920_suriya-5-g" then 
	end
	if npc_talk_index == "n1920_suriya-5-h" then 
	end
	if npc_talk_index == "n1920_suriya-5-i" then 
	end
	if npc_talk_index == "n1920_suriya-5-j" then 
	end
	if npc_talk_index == "n1920_suriya-5-k" then 
	end
	if npc_talk_index == "n1920_suriya-5-l" then 
	end
	if npc_talk_index == "n1920_suriya-5-m" then 
	end
	if npc_talk_index == "n1920_suriya-5-n" then 
	end
	if npc_talk_index == "n1920_suriya-5-o" then 
	end
	if npc_talk_index == "n1920_suriya-5-p" then 
	end
	if npc_talk_index == "n1920_suriya-5-q" then 
	end
	if npc_talk_index == "n1920_suriya-5-r" then 
	end
	if npc_talk_index == "n1920_suriya-5-s" then 
	end
	if npc_talk_index == "n1920_suriya-5-t" then 
	end
	if npc_talk_index == "n1920_suriya-5-u" then 
	end
	if npc_talk_index == "n1920_suriya-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1930_shaolong--------------------------------------------------------------------------------
function mqc09_9648_crash_beliefs_OnTalk_n1930_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1930_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1930_shaolong-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1930_shaolong-6-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96480, questID, 1);
			 end 
	end
	if npc_talk_index == "n1930_shaolong-6-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9649, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9649, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9649, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1930_shaolong-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1930_shaolong-1", "mqc09_9649_dilemma.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1737_eltia--------------------------------------------------------------------------------
function mqc09_9648_crash_beliefs_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1737_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9648, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9648, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9648, 1);
				npc_talk_index = "n1737_eltia-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc09_9648_crash_beliefs_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9648);
end

function mqc09_9648_crash_beliefs_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9648);
end

function mqc09_9648_crash_beliefs_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9648);
	local questID=9648;
end

function mqc09_9648_crash_beliefs_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc09_9648_crash_beliefs_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc09_9648_crash_beliefs_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9648, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9648, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9648, 1);
				npc_talk_index = "n1737_eltia-1";
end

</GameServer>