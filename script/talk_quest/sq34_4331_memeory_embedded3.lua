<VillageServer>

function sq34_4331_memeory_embedded3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1067 then
		sq34_4331_memeory_embedded3_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1115 then
		sq34_4331_memeory_embedded3_OnTalk_n1115_wanderer_elf_riana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1138 then
		sq34_4331_memeory_embedded3_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function sq34_4331_memeory_embedded3_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1067_elder_elf-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43310, true);
				 api_quest_RewardQuestUser(userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43310, true);
				 api_quest_RewardQuestUser(userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43310, true);
				 api_quest_RewardQuestUser(userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43310, true);
				 api_quest_RewardQuestUser(userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43310, true);
				 api_quest_RewardQuestUser(userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43310, true);
				 api_quest_RewardQuestUser(userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43310, true);
				 api_quest_RewardQuestUser(userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43310, true);
				 api_quest_RewardQuestUser(userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43310, true);
				 api_quest_RewardQuestUser(userObjID, 43310, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1115_wanderer_elf_riana--------------------------------------------------------------------------------
function sq34_4331_memeory_embedded3_OnTalk_n1115_wanderer_elf_riana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1115_wanderer_elf_riana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1115_wanderer_elf_riana-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1115_wanderer_elf_riana-2-b" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-c" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-d" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-e" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-f" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-g" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-h" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-i" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-j" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-j" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-k" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-l" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				if api_quest_HasQuestItem(userObjID, 400402, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400402, api_quest_HasQuestItem(userObjID, 400402, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400403, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400403, api_quest_HasQuestItem(userObjID, 400403, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function sq34_4331_memeory_embedded3_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1138_elf_guard_sitredel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1138_elf_guard_sitredel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43310, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43310, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43310, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43310, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43310, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43310, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43310, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43310, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43310, false);
			 end 

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4331, 1);
				api_quest_SetJournalStep(userObjID,4331, 1);
				api_quest_SetQuestStep(userObjID,4331, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-1-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-1-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-1-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-1-e" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-1-f" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400402, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400402, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-1-g" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400403, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400403, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4331_memeory_embedded3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4331);
end

function sq34_4331_memeory_embedded3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4331);
end

function sq34_4331_memeory_embedded3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4331);
	local questID=4331;
end

function sq34_4331_memeory_embedded3_OnRemoteStart( userObjID, questID )
end

function sq34_4331_memeory_embedded3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq34_4331_memeory_embedded3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4331, 1);
				api_quest_SetJournalStep(userObjID,4331, 1);
				api_quest_SetQuestStep(userObjID,4331, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";
end

</VillageServer>

<GameServer>
function sq34_4331_memeory_embedded3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1067 then
		sq34_4331_memeory_embedded3_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1115 then
		sq34_4331_memeory_embedded3_OnTalk_n1115_wanderer_elf_riana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1138 then
		sq34_4331_memeory_embedded3_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function sq34_4331_memeory_embedded3_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1067_elder_elf-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43310, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1115_wanderer_elf_riana--------------------------------------------------------------------------------
function sq34_4331_memeory_embedded3_OnTalk_n1115_wanderer_elf_riana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1115_wanderer_elf_riana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1115_wanderer_elf_riana-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1115_wanderer_elf_riana-2-b" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-c" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-d" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-e" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-f" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-g" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-h" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-i" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-j" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-j" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-k" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-2-l" then 
	end
	if npc_talk_index == "n1115_wanderer_elf_riana-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				if api_quest_HasQuestItem( pRoom, userObjID, 400402, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400402, api_quest_HasQuestItem( pRoom, userObjID, 400402, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400403, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400403, api_quest_HasQuestItem( pRoom, userObjID, 400403, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function sq34_4331_memeory_embedded3_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1138_elf_guard_sitredel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1138_elf_guard_sitredel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43310, false);
			 end 

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4331, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4331, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4331, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-1-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-1-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-1-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-1-e" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-1-f" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400402, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400402, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-1-g" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400403, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400403, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4331_memeory_embedded3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4331);
end

function sq34_4331_memeory_embedded3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4331);
end

function sq34_4331_memeory_embedded3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4331);
	local questID=4331;
end

function sq34_4331_memeory_embedded3_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq34_4331_memeory_embedded3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq34_4331_memeory_embedded3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4331, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4331, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4331, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";
end

</GameServer>