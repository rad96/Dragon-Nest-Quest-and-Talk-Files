<VillageServer>

function mq15_9340_chaotic_voice_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1169 then
		mq15_9340_chaotic_voice_OnTalk_n1169_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1176 then
		mq15_9340_chaotic_voice_OnTalk_n1176_illusion(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1177 then
		mq15_9340_chaotic_voice_OnTalk_n1177_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 492 then
		mq15_9340_chaotic_voice_OnTalk_n492_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 493 then
		mq15_9340_chaotic_voice_OnTalk_n493_charty_grandfather(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 709 then
		mq15_9340_chaotic_voice_OnTalk_n709_trader_lucita(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1169_lunaria--------------------------------------------------------------------------------
function mq15_9340_chaotic_voice_OnTalk_n1169_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1169_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1176_illusion--------------------------------------------------------------------------------
function mq15_9340_chaotic_voice_OnTalk_n1176_illusion(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1176_illusion-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1176_illusion-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1176_illusion-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1176_illusion-2-b" then 
	end
	if npc_talk_index == "n1176_illusion-2-c" then 
	end
	if npc_talk_index == "n1176_illusion-2-d" then 
	end
	if npc_talk_index == "n1176_illusion-2-e" then 
	end
	if npc_talk_index == "n1176_illusion-2-f" then 
	end
	if npc_talk_index == "n1176_illusion-2-g" then 
	end
	if npc_talk_index == "n1176_illusion-2-h" then 
	end
	if npc_talk_index == "n1176_illusion-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1177_lunaria--------------------------------------------------------------------------------
function mq15_9340_chaotic_voice_OnTalk_n1177_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1177_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1177_lunaria-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1177_lunaria-3-b" then 
	end
	if npc_talk_index == "n1177_lunaria-3-c" then 
	end
	if npc_talk_index == "n1177_lunaria-3-d" then 
	end
	if npc_talk_index == "n1177_lunaria-3-e" then 
	end
	if npc_talk_index == "n1177_lunaria-3-f" then 
	end
	if npc_talk_index == "n1177_lunaria-3-g" then 
	end
	if npc_talk_index == "n1177_lunaria-3-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93400, true);
				 api_quest_RewardQuestUser(userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93400, true);
				 api_quest_RewardQuestUser(userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93400, true);
				 api_quest_RewardQuestUser(userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93400, true);
				 api_quest_RewardQuestUser(userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93400, true);
				 api_quest_RewardQuestUser(userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93400, true);
				 api_quest_RewardQuestUser(userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93400, true);
				 api_quest_RewardQuestUser(userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93400, true);
				 api_quest_RewardQuestUser(userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93400, true);
				 api_quest_RewardQuestUser(userObjID, 93400, questID, 1);
			 end 
	end
	if npc_talk_index == "n1177_lunaria-3-i" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9341, 2);
					api_quest_SetQuestStep(userObjID, 9341, 1);
					api_quest_SetJournalStep(userObjID, 9341, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1177_lunaria-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1177_lunaria-1", "mq15_9341_crisscross.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n492_charty--------------------------------------------------------------------------------
function mq15_9340_chaotic_voice_OnTalk_n492_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n493_charty_grandfather--------------------------------------------------------------------------------
function mq15_9340_chaotic_voice_OnTalk_n493_charty_grandfather(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n493_charty_grandfather-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n493_charty_grandfather-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n493_charty_grandfather-1-b" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-c" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-d" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-e" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-f" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-g" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-h" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-i" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-j" then 
	end
	if npc_talk_index == "n493_charty_grandfather-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n709_trader_lucita--------------------------------------------------------------------------------
function mq15_9340_chaotic_voice_OnTalk_n709_trader_lucita(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n709_trader_lucita-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n709_trader_lucita-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n709_trader_lucita-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9340, 2);
				api_quest_SetJournalStep(userObjID,9340, 1);
				api_quest_SetQuestStep(userObjID,9340, 1);
				npc_talk_index = "n709_trader_lucita-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9340_chaotic_voice_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9340);
end

function mq15_9340_chaotic_voice_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9340);
end

function mq15_9340_chaotic_voice_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9340);
	local questID=9340;
end

function mq15_9340_chaotic_voice_OnRemoteStart( userObjID, questID )
end

function mq15_9340_chaotic_voice_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9340_chaotic_voice_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9340, 2);
				api_quest_SetJournalStep(userObjID,9340, 1);
				api_quest_SetQuestStep(userObjID,9340, 1);
				npc_talk_index = "n709_trader_lucita-1";
end

</VillageServer>

<GameServer>
function mq15_9340_chaotic_voice_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1169 then
		mq15_9340_chaotic_voice_OnTalk_n1169_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1176 then
		mq15_9340_chaotic_voice_OnTalk_n1176_illusion( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1177 then
		mq15_9340_chaotic_voice_OnTalk_n1177_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 492 then
		mq15_9340_chaotic_voice_OnTalk_n492_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 493 then
		mq15_9340_chaotic_voice_OnTalk_n493_charty_grandfather( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 709 then
		mq15_9340_chaotic_voice_OnTalk_n709_trader_lucita( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1169_lunaria--------------------------------------------------------------------------------
function mq15_9340_chaotic_voice_OnTalk_n1169_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1169_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1176_illusion--------------------------------------------------------------------------------
function mq15_9340_chaotic_voice_OnTalk_n1176_illusion( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1176_illusion-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1176_illusion-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1176_illusion-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1176_illusion-2-b" then 
	end
	if npc_talk_index == "n1176_illusion-2-c" then 
	end
	if npc_talk_index == "n1176_illusion-2-d" then 
	end
	if npc_talk_index == "n1176_illusion-2-e" then 
	end
	if npc_talk_index == "n1176_illusion-2-f" then 
	end
	if npc_talk_index == "n1176_illusion-2-g" then 
	end
	if npc_talk_index == "n1176_illusion-2-h" then 
	end
	if npc_talk_index == "n1176_illusion-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1177_lunaria--------------------------------------------------------------------------------
function mq15_9340_chaotic_voice_OnTalk_n1177_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1177_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1177_lunaria-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1177_lunaria-3-b" then 
	end
	if npc_talk_index == "n1177_lunaria-3-c" then 
	end
	if npc_talk_index == "n1177_lunaria-3-d" then 
	end
	if npc_talk_index == "n1177_lunaria-3-e" then 
	end
	if npc_talk_index == "n1177_lunaria-3-f" then 
	end
	if npc_talk_index == "n1177_lunaria-3-g" then 
	end
	if npc_talk_index == "n1177_lunaria-3-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93400, questID, 1);
			 end 
	end
	if npc_talk_index == "n1177_lunaria-3-i" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9341, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9341, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9341, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1177_lunaria-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1177_lunaria-1", "mq15_9341_crisscross.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n492_charty--------------------------------------------------------------------------------
function mq15_9340_chaotic_voice_OnTalk_n492_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n493_charty_grandfather--------------------------------------------------------------------------------
function mq15_9340_chaotic_voice_OnTalk_n493_charty_grandfather( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n493_charty_grandfather-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n493_charty_grandfather-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n493_charty_grandfather-1-b" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-c" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-d" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-e" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-f" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-g" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-h" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-i" then 
	end
	if npc_talk_index == "n493_charty_grandfather-1-j" then 
	end
	if npc_talk_index == "n493_charty_grandfather-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n709_trader_lucita--------------------------------------------------------------------------------
function mq15_9340_chaotic_voice_OnTalk_n709_trader_lucita( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n709_trader_lucita-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n709_trader_lucita-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n709_trader_lucita-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9340, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9340, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9340, 1);
				npc_talk_index = "n709_trader_lucita-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9340_chaotic_voice_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9340);
end

function mq15_9340_chaotic_voice_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9340);
end

function mq15_9340_chaotic_voice_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9340);
	local questID=9340;
end

function mq15_9340_chaotic_voice_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9340_chaotic_voice_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9340_chaotic_voice_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9340, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9340, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9340, 1);
				npc_talk_index = "n709_trader_lucita-1";
end

</GameServer>