<VillageServer>

function mqc09_9645_donot_believe_the_truth_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1861 then
		mqc09_9645_donot_believe_the_truth_OnTalk_n1861_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1917 then
		mqc09_9645_donot_believe_the_truth_OnTalk_n1917_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1918 then
		mqc09_9645_donot_believe_the_truth_OnTalk_n1918_cleric_chris(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mqc09_9645_donot_believe_the_truth_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1861_shaolong--------------------------------------------------------------------------------
function mqc09_9645_donot_believe_the_truth_OnTalk_n1861_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1861_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1861_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1861_shaolong-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9645, 2);
				api_quest_SetJournalStep(userObjID,9645, 1);
				api_quest_SetQuestStep(userObjID,9645, 1);
				npc_talk_index = "n1861_shaolong-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1917_light_pole--------------------------------------------------------------------------------
function mqc09_9645_donot_believe_the_truth_OnTalk_n1917_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1917_light_pole-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1917_light_pole-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1917_light_pole-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1917_light_pole-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400493, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400493, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1918_cleric_chris--------------------------------------------------------------------------------
function mqc09_9645_donot_believe_the_truth_OnTalk_n1918_cleric_chris(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1918_cleric_chris-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1918_cleric_chris-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1918_cleric_chris-4-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 96450, true);
				 api_quest_RewardQuestUser(userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 96450, true);
				 api_quest_RewardQuestUser(userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 96450, true);
				 api_quest_RewardQuestUser(userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 96450, true);
				 api_quest_RewardQuestUser(userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 96450, true);
				 api_quest_RewardQuestUser(userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 96450, true);
				 api_quest_RewardQuestUser(userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 96450, true);
				 api_quest_RewardQuestUser(userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 96450, true);
				 api_quest_RewardQuestUser(userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 96450, true);
				 api_quest_RewardQuestUser(userObjID, 96450, questID, 1);
			 end 
	end
	if npc_talk_index == "n1918_cleric_chris-4-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9646, 2);
					api_quest_SetQuestStep(userObjID, 9646, 1);
					api_quest_SetJournalStep(userObjID, 9646, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1918_cleric_chris-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1918_cleric_chris-1", "mqc09_9646_reconciliation.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mqc09_9645_donot_believe_the_truth_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n707_sidel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
	end
	if npc_talk_index == "n707_sidel-1-d" then 
	end
	if npc_talk_index == "n707_sidel-1-e" then 
	end
	if npc_talk_index == "n707_sidel-1-f" then 
	end
	if npc_talk_index == "n707_sidel-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400493, 1);
	end
	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc09_9645_donot_believe_the_truth_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9645);
	if qstep == 2 and CountIndex == 400493 then

	end
end

function mqc09_9645_donot_believe_the_truth_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9645);
	if qstep == 2 and CountIndex == 400493 and Count >= TargetCount  then

	end
end

function mqc09_9645_donot_believe_the_truth_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9645);
	local questID=9645;
end

function mqc09_9645_donot_believe_the_truth_OnRemoteStart( userObjID, questID )
end

function mqc09_9645_donot_believe_the_truth_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc09_9645_donot_believe_the_truth_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9645, 2);
				api_quest_SetJournalStep(userObjID,9645, 1);
				api_quest_SetQuestStep(userObjID,9645, 1);
				npc_talk_index = "n1861_shaolong-1";
end

</VillageServer>

<GameServer>
function mqc09_9645_donot_believe_the_truth_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1861 then
		mqc09_9645_donot_believe_the_truth_OnTalk_n1861_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1917 then
		mqc09_9645_donot_believe_the_truth_OnTalk_n1917_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1918 then
		mqc09_9645_donot_believe_the_truth_OnTalk_n1918_cleric_chris( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mqc09_9645_donot_believe_the_truth_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1861_shaolong--------------------------------------------------------------------------------
function mqc09_9645_donot_believe_the_truth_OnTalk_n1861_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1861_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1861_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1861_shaolong-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9645, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9645, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9645, 1);
				npc_talk_index = "n1861_shaolong-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1917_light_pole--------------------------------------------------------------------------------
function mqc09_9645_donot_believe_the_truth_OnTalk_n1917_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1917_light_pole-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1917_light_pole-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1917_light_pole-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1917_light_pole-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400493, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400493, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1918_cleric_chris--------------------------------------------------------------------------------
function mqc09_9645_donot_believe_the_truth_OnTalk_n1918_cleric_chris( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1918_cleric_chris-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1918_cleric_chris-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1918_cleric_chris-4-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96450, questID, 1);
			 end 
	end
	if npc_talk_index == "n1918_cleric_chris-4-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9646, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9646, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9646, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1918_cleric_chris-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1918_cleric_chris-1", "mqc09_9646_reconciliation.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mqc09_9645_donot_believe_the_truth_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n707_sidel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
	end
	if npc_talk_index == "n707_sidel-1-d" then 
	end
	if npc_talk_index == "n707_sidel-1-e" then 
	end
	if npc_talk_index == "n707_sidel-1-f" then 
	end
	if npc_talk_index == "n707_sidel-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400493, 1);
	end
	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc09_9645_donot_believe_the_truth_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9645);
	if qstep == 2 and CountIndex == 400493 then

	end
end

function mqc09_9645_donot_believe_the_truth_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9645);
	if qstep == 2 and CountIndex == 400493 and Count >= TargetCount  then

	end
end

function mqc09_9645_donot_believe_the_truth_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9645);
	local questID=9645;
end

function mqc09_9645_donot_believe_the_truth_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc09_9645_donot_believe_the_truth_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc09_9645_donot_believe_the_truth_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9645, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9645, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9645, 1);
				npc_talk_index = "n1861_shaolong-1";
end

</GameServer>