<VillageServer>

function mqc14_9413_imperfect_dragon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mqc14_9413_imperfect_dragon_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1511 then
		mqc14_9413_imperfect_dragon_OnTalk_n1511_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1512 then
		mqc14_9413_imperfect_dragon_OnTalk_n1512_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1514 then
		mqc14_9413_imperfect_dragon_OnTalk_n1514_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9413_imperfect_dragon_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1513 then
		mqc14_9413_imperfect_dragon_OnTalk_n1513_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mqc14_9413_imperfect_dragon_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9413, 2);
				api_quest_SetJournalStep(userObjID,9413, 1);
				api_quest_SetQuestStep(userObjID,9413, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400449, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 601950, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 701950, 30000);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1511_argenta--------------------------------------------------------------------------------
function mqc14_9413_imperfect_dragon_OnTalk_n1511_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1511_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1511_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1511_argenta-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 3, 3, 400449, 1);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 601950, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 701950, 30000);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1512_gereint_kid--------------------------------------------------------------------------------
function mqc14_9413_imperfect_dragon_OnTalk_n1512_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1512_gereint_kid-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1512_gereint_kid-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1514_for_memory--------------------------------------------------------------------------------
function mqc14_9413_imperfect_dragon_OnTalk_n1514_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1514_for_memory-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1514_for_memory-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1514_for_memory-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1514_for_memory-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1514_for_memory-4-b" then 
	end
	if npc_talk_index == "n1514_for_memory-4-c" then 
	end
	if npc_talk_index == "n1514_for_memory-4-d" then 
	end
	if npc_talk_index == "n1514_for_memory-4-e" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400450, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400450, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1514_for_memory-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end
	if npc_talk_index == "n1514_for_memory-6-b" then 
	end
	if npc_talk_index == "n1514_for_memory-6-c" then 

				if api_quest_HasQuestItem(userObjID, 400450, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400450, api_quest_HasQuestItem(userObjID, 400450, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400451, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400451, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1514_for_memory-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9413_imperfect_dragon_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1515_for_memory-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-8-b" then 
	end
	if npc_talk_index == "n1515_for_memory-8-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94131, true);
				 api_quest_RewardQuestUser(userObjID, 94131, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94132, true);
				 api_quest_RewardQuestUser(userObjID, 94132, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94133, true);
				 api_quest_RewardQuestUser(userObjID, 94133, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94134, true);
				 api_quest_RewardQuestUser(userObjID, 94134, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94135, true);
				 api_quest_RewardQuestUser(userObjID, 94135, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94136, true);
				 api_quest_RewardQuestUser(userObjID, 94136, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94137, true);
				 api_quest_RewardQuestUser(userObjID, 94137, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94138, true);
				 api_quest_RewardQuestUser(userObjID, 94138, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94139, true);
				 api_quest_RewardQuestUser(userObjID, 94139, questID, 1);
			 end 
	end
	if npc_talk_index == "n1515_for_memory-8-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9414, 2);
					api_quest_SetQuestStep(userObjID, 9414, 1);
					api_quest_SetJournalStep(userObjID, 9414, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1515_for_memory-8-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1515_for_memory-1", "mqc14_9414_rage.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1513_argenta--------------------------------------------------------------------------------
function mqc14_9413_imperfect_dragon_OnTalk_n1513_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1513_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1513_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1513_argenta-3-b" then 
	end
	if npc_talk_index == "n1513_argenta-3-b" then 
	end
	if npc_talk_index == "n1513_argenta-3-b" then 
	end
	if npc_talk_index == "n1513_argenta-3-c" then 
	end
	if npc_talk_index == "n1513_argenta-3-d" then 
	end
	if npc_talk_index == "n1513_argenta-3-e" then 
	end
	if npc_talk_index == "n1513_argenta-3-f" then 
	end
	if npc_talk_index == "n1513_argenta-3-g" then 
	end
	if npc_talk_index == "n1513_argenta-3-h" then 
	end
	if npc_talk_index == "n1513_argenta-3-i" then 
	end
	if npc_talk_index == "n1513_argenta-3-j" then 
	end
	if npc_talk_index == "n1513_argenta-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9413_imperfect_dragon_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9413);
	if qstep == 2 and CountIndex == 400449 then

	end
	if qstep == 2 and CountIndex == 601950 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400449, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400449, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 701950 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400449, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400449, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mqc14_9413_imperfect_dragon_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9413);
	if qstep == 2 and CountIndex == 400449 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 601950 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 701950 and Count >= TargetCount  then

	end
end

function mqc14_9413_imperfect_dragon_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9413);
	local questID=9413;
end

function mqc14_9413_imperfect_dragon_OnRemoteStart( userObjID, questID )
end

function mqc14_9413_imperfect_dragon_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc14_9413_imperfect_dragon_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9413, 2);
				api_quest_SetJournalStep(userObjID,9413, 1);
				api_quest_SetQuestStep(userObjID,9413, 1);
				npc_talk_index = "n1047_argenta-1";
end

</VillageServer>

<GameServer>
function mqc14_9413_imperfect_dragon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mqc14_9413_imperfect_dragon_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1511 then
		mqc14_9413_imperfect_dragon_OnTalk_n1511_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1512 then
		mqc14_9413_imperfect_dragon_OnTalk_n1512_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1514 then
		mqc14_9413_imperfect_dragon_OnTalk_n1514_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9413_imperfect_dragon_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1513 then
		mqc14_9413_imperfect_dragon_OnTalk_n1513_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mqc14_9413_imperfect_dragon_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9413, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9413, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9413, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400449, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 601950, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 701950, 30000);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1511_argenta--------------------------------------------------------------------------------
function mqc14_9413_imperfect_dragon_OnTalk_n1511_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1511_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1511_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1511_argenta-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 3, 400449, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 601950, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 701950, 30000);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1512_gereint_kid--------------------------------------------------------------------------------
function mqc14_9413_imperfect_dragon_OnTalk_n1512_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1512_gereint_kid-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1512_gereint_kid-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1514_for_memory--------------------------------------------------------------------------------
function mqc14_9413_imperfect_dragon_OnTalk_n1514_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1514_for_memory-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1514_for_memory-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1514_for_memory-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1514_for_memory-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1514_for_memory-4-b" then 
	end
	if npc_talk_index == "n1514_for_memory-4-c" then 
	end
	if npc_talk_index == "n1514_for_memory-4-d" then 
	end
	if npc_talk_index == "n1514_for_memory-4-e" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400450, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400450, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1514_for_memory-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end
	if npc_talk_index == "n1514_for_memory-6-b" then 
	end
	if npc_talk_index == "n1514_for_memory-6-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400450, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400450, api_quest_HasQuestItem( pRoom, userObjID, 400450, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400451, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400451, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1514_for_memory-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9413_imperfect_dragon_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1515_for_memory-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-8-b" then 
	end
	if npc_talk_index == "n1515_for_memory-8-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94131, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94131, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94132, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94132, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94133, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94133, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94134, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94134, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94135, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94135, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94136, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94136, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94137, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94137, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94138, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94138, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94139, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94139, questID, 1);
			 end 
	end
	if npc_talk_index == "n1515_for_memory-8-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9414, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9414, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9414, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1515_for_memory-8-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1515_for_memory-1", "mqc14_9414_rage.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1513_argenta--------------------------------------------------------------------------------
function mqc14_9413_imperfect_dragon_OnTalk_n1513_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1513_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1513_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1513_argenta-3-b" then 
	end
	if npc_talk_index == "n1513_argenta-3-b" then 
	end
	if npc_talk_index == "n1513_argenta-3-b" then 
	end
	if npc_talk_index == "n1513_argenta-3-c" then 
	end
	if npc_talk_index == "n1513_argenta-3-d" then 
	end
	if npc_talk_index == "n1513_argenta-3-e" then 
	end
	if npc_talk_index == "n1513_argenta-3-f" then 
	end
	if npc_talk_index == "n1513_argenta-3-g" then 
	end
	if npc_talk_index == "n1513_argenta-3-h" then 
	end
	if npc_talk_index == "n1513_argenta-3-i" then 
	end
	if npc_talk_index == "n1513_argenta-3-j" then 
	end
	if npc_talk_index == "n1513_argenta-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9413_imperfect_dragon_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9413);
	if qstep == 2 and CountIndex == 400449 then

	end
	if qstep == 2 and CountIndex == 601950 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400449, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400449, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 701950 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400449, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400449, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mqc14_9413_imperfect_dragon_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9413);
	if qstep == 2 and CountIndex == 400449 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 601950 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 701950 and Count >= TargetCount  then

	end
end

function mqc14_9413_imperfect_dragon_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9413);
	local questID=9413;
end

function mqc14_9413_imperfect_dragon_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc14_9413_imperfect_dragon_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc14_9413_imperfect_dragon_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9413, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9413, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9413, 1);
				npc_talk_index = "n1047_argenta-1";
end

</GameServer>