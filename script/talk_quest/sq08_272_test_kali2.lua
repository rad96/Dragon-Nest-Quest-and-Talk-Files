<VillageServer>

function sq08_272_test_kali2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 28 then
		sq08_272_test_kali2_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 564 then
		sq08_272_test_kali2_OnTalk_n564_kali_totem(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_272_test_kali2_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n028_scholar_bailey-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n028_scholar_bailey-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n028_scholar_bailey-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-1-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-2" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200413, 30001);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300064, 30001);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n028_scholar_bailey-3-b" then 

				if api_quest_HasQuestItem(userObjID, 300064, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300064, api_quest_HasQuestItem(userObjID, 300064, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400324, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400324, api_quest_HasQuestItem(userObjID, 400324, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400325, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400325, api_quest_HasQuestItem(userObjID, 400325, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400326, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400326, api_quest_HasQuestItem(userObjID, 400326, 1));
				end
	end
	if npc_talk_index == "n028_scholar_bailey-3-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400327, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400327, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n564_kali_totem--------------------------------------------------------------------------------
function sq08_272_test_kali2_OnTalk_n564_kali_totem(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n564_kali_totem-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n564_kali_totem-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n564_kali_totem-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n564_kali_totem-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n564_kali_totem-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2720, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2720, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2720, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2720, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2720, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2720, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2720, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2720, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2720, false);
			 end 

	end
	if npc_talk_index == "n564_kali_totem-accepting-acceptted" then
				api_quest_AddQuest(userObjID,272, 1);
				api_quest_SetJournalStep(userObjID,272, 1);
				api_quest_SetQuestStep(userObjID,272, 1);
				npc_talk_index = "n564_kali_totem-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400325, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400325, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n564_kali_totem-4-b" then 
	end
	if npc_talk_index == "n564_kali_totem-4-c" then 
	end
	if npc_talk_index == "n564_kali_totem-4-d" then 
	end
	if npc_talk_index == "n564_kali_totem-4-e" then 
	end
	if npc_talk_index == "n564_kali_totem-4-f" then 
	end
	if npc_talk_index == "n564_kali_totem-4-g" then 
	end
	if npc_talk_index == "n564_kali_totem-4-h" then 
	end
	if npc_talk_index == "n564_kali_totem-5" then 

				if api_quest_HasQuestItem(userObjID, 400327, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400327, api_quest_HasQuestItem(userObjID, 400327, 1));
				end
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n564_kali_totem-5" then 
	end
	if npc_talk_index == "n564_kali_totem-5-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2721, true);
				 api_quest_RewardQuestUser(userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2721, true);
				 api_quest_RewardQuestUser(userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2721, true);
				 api_quest_RewardQuestUser(userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2721, true);
				 api_quest_RewardQuestUser(userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2721, true);
				 api_quest_RewardQuestUser(userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2721, true);
				 api_quest_RewardQuestUser(userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2721, true);
				 api_quest_RewardQuestUser(userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2721, true);
				 api_quest_RewardQuestUser(userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2721, true);
				 api_quest_RewardQuestUser(userObjID, 2721, questID, 1);
			 end 
	end
	if npc_talk_index == "n564_kali_totem-5-complete1" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 287, 1);
					api_quest_SetQuestStep(userObjID, 287, 1);
					api_quest_SetJournalStep(userObjID, 287, 1);

									api_user_SetUserJobID(userObjID, 54);
				npc_talk_index = "n564_kali_totem-5-c";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n564_kali_totem-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n564_kali_totem-1", "sq08_287_change_job6.xml");
			return;
		end
	end
	if npc_talk_index == "n564_kali_totem-5" then 
	end
	if npc_talk_index == "n564_kali_totem-5-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2722, true);
				 api_quest_RewardQuestUser(userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2722, true);
				 api_quest_RewardQuestUser(userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2722, true);
				 api_quest_RewardQuestUser(userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2722, true);
				 api_quest_RewardQuestUser(userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2722, true);
				 api_quest_RewardQuestUser(userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2722, true);
				 api_quest_RewardQuestUser(userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2722, true);
				 api_quest_RewardQuestUser(userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2722, true);
				 api_quest_RewardQuestUser(userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2722, true);
				 api_quest_RewardQuestUser(userObjID, 2722, questID, 1);
			 end 
	end
	if npc_talk_index == "n564_kali_totem-5-complete2" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 287, 1);
					api_quest_SetQuestStep(userObjID, 287, 1);
					api_quest_SetJournalStep(userObjID, 287, 1);

									api_user_SetUserJobID(userObjID, 57);
				npc_talk_index = "n564_kali_totem-5-c";


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_272_test_kali2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 272);
	if qstep == 2 and CountIndex == 413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300064, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300064, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300064 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300064, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300064, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_272_test_kali2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 272);
	if qstep == 2 and CountIndex == 413 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300064 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200413 and Count >= TargetCount  then

	end
end

function sq08_272_test_kali2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 272);
	local questID=272;
end

function sq08_272_test_kali2_OnRemoteStart( userObjID, questID )
end

function sq08_272_test_kali2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_272_test_kali2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,272, 1);
				api_quest_SetJournalStep(userObjID,272, 1);
				api_quest_SetQuestStep(userObjID,272, 1);
				npc_talk_index = "n564_kali_totem-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400325, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400325, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
end

</VillageServer>

<GameServer>
function sq08_272_test_kali2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 28 then
		sq08_272_test_kali2_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 564 then
		sq08_272_test_kali2_OnTalk_n564_kali_totem( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_272_test_kali2_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n028_scholar_bailey-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n028_scholar_bailey-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n028_scholar_bailey-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-1-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-2" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200413, 30001);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300064, 30001);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n028_scholar_bailey-3-b" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300064, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300064, api_quest_HasQuestItem( pRoom, userObjID, 300064, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400324, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400324, api_quest_HasQuestItem( pRoom, userObjID, 400324, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400325, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400325, api_quest_HasQuestItem( pRoom, userObjID, 400325, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400326, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400326, api_quest_HasQuestItem( pRoom, userObjID, 400326, 1));
				end
	end
	if npc_talk_index == "n028_scholar_bailey-3-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400327, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400327, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n564_kali_totem--------------------------------------------------------------------------------
function sq08_272_test_kali2_OnTalk_n564_kali_totem( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n564_kali_totem-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n564_kali_totem-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n564_kali_totem-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n564_kali_totem-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n564_kali_totem-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2720, false);
			 end 

	end
	if npc_talk_index == "n564_kali_totem-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,272, 1);
				api_quest_SetJournalStep( pRoom, userObjID,272, 1);
				api_quest_SetQuestStep( pRoom, userObjID,272, 1);
				npc_talk_index = "n564_kali_totem-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400325, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400325, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n564_kali_totem-4-b" then 
	end
	if npc_talk_index == "n564_kali_totem-4-c" then 
	end
	if npc_talk_index == "n564_kali_totem-4-d" then 
	end
	if npc_talk_index == "n564_kali_totem-4-e" then 
	end
	if npc_talk_index == "n564_kali_totem-4-f" then 
	end
	if npc_talk_index == "n564_kali_totem-4-g" then 
	end
	if npc_talk_index == "n564_kali_totem-4-h" then 
	end
	if npc_talk_index == "n564_kali_totem-5" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400327, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400327, api_quest_HasQuestItem( pRoom, userObjID, 400327, 1));
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n564_kali_totem-5" then 
	end
	if npc_talk_index == "n564_kali_totem-5-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2721, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2721, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2721, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2721, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2721, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2721, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2721, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2721, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2721, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2721, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2721, questID, 1);
			 end 
	end
	if npc_talk_index == "n564_kali_totem-5-complete1" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 287, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 287, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 287, 1);

									api_user_SetUserJobID( pRoom, userObjID, 54);
				npc_talk_index = "n564_kali_totem-5-c";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n564_kali_totem-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n564_kali_totem-1", "sq08_287_change_job6.xml");
			return;
		end
	end
	if npc_talk_index == "n564_kali_totem-5" then 
	end
	if npc_talk_index == "n564_kali_totem-5-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2722, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2722, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2722, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2722, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2722, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2722, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2722, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2722, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2722, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2722, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2722, questID, 1);
			 end 
	end
	if npc_talk_index == "n564_kali_totem-5-complete2" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 287, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 287, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 287, 1);

									api_user_SetUserJobID( pRoom, userObjID, 57);
				npc_talk_index = "n564_kali_totem-5-c";


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_272_test_kali2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 272);
	if qstep == 2 and CountIndex == 413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300064, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300064, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300064 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300064, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300064, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_272_test_kali2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 272);
	if qstep == 2 and CountIndex == 413 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300064 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200413 and Count >= TargetCount  then

	end
end

function sq08_272_test_kali2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 272);
	local questID=272;
end

function sq08_272_test_kali2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_272_test_kali2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_272_test_kali2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,272, 1);
				api_quest_SetJournalStep( pRoom, userObjID,272, 1);
				api_quest_SetQuestStep( pRoom, userObjID,272, 1);
				npc_talk_index = "n564_kali_totem-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400325, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400325, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
end

</GameServer>