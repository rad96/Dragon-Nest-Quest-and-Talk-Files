<VillageServer>

function sqc17_4760_labyrinth_of_mirage_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1888 then
		sqc17_4760_labyrinth_of_mirage_OnTalk_n1888_storyteller_kesey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		sqc17_4760_labyrinth_of_mirage_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1888_storyteller_kesey--------------------------------------------------------------------------------
function sqc17_4760_labyrinth_of_mirage_OnTalk_n1888_storyteller_kesey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1888_storyteller_kesey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1888_storyteller_kesey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1888_storyteller_kesey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1888_storyteller_kesey-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47600, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47600, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47600, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47600, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47600, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47600, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47600, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47600, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47600, false);
			 end 

	end
	if npc_talk_index == "n1888_storyteller_kesey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4760, 1);
				api_quest_SetJournalStep(userObjID,4760, 1);
				api_quest_SetQuestStep(userObjID,4760, 1);
				npc_talk_index = "n1888_storyteller_kesey-1";

	end
	if npc_talk_index == "n1888_storyteller_kesey-1-b" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-c" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-d" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-e" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-f" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-g" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-h" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-i" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-j" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-k" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-l" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-m" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function sqc17_4760_labyrinth_of_mirage_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-2-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-f" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47600, true);
				 api_quest_RewardQuestUser(userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47600, true);
				 api_quest_RewardQuestUser(userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47600, true);
				 api_quest_RewardQuestUser(userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47600, true);
				 api_quest_RewardQuestUser(userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47600, true);
				 api_quest_RewardQuestUser(userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47600, true);
				 api_quest_RewardQuestUser(userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47600, true);
				 api_quest_RewardQuestUser(userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47600, true);
				 api_quest_RewardQuestUser(userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47600, true);
				 api_quest_RewardQuestUser(userObjID, 47600, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4760_labyrinth_of_mirage_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4760);
end

function sqc17_4760_labyrinth_of_mirage_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4760);
end

function sqc17_4760_labyrinth_of_mirage_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4760);
	local questID=4760;
end

function sqc17_4760_labyrinth_of_mirage_OnRemoteStart( userObjID, questID )
end

function sqc17_4760_labyrinth_of_mirage_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc17_4760_labyrinth_of_mirage_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4760, 1);
				api_quest_SetJournalStep(userObjID,4760, 1);
				api_quest_SetQuestStep(userObjID,4760, 1);
				npc_talk_index = "n1888_storyteller_kesey-1";
end

</VillageServer>

<GameServer>
function sqc17_4760_labyrinth_of_mirage_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1888 then
		sqc17_4760_labyrinth_of_mirage_OnTalk_n1888_storyteller_kesey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		sqc17_4760_labyrinth_of_mirage_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1888_storyteller_kesey--------------------------------------------------------------------------------
function sqc17_4760_labyrinth_of_mirage_OnTalk_n1888_storyteller_kesey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1888_storyteller_kesey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1888_storyteller_kesey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1888_storyteller_kesey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1888_storyteller_kesey-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, false);
			 end 

	end
	if npc_talk_index == "n1888_storyteller_kesey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4760, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4760, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4760, 1);
				npc_talk_index = "n1888_storyteller_kesey-1";

	end
	if npc_talk_index == "n1888_storyteller_kesey-1-b" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-c" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-d" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-e" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-f" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-g" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-h" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-i" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-j" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-k" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-l" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-m" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function sqc17_4760_labyrinth_of_mirage_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-2-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-f" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47600, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4760_labyrinth_of_mirage_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4760);
end

function sqc17_4760_labyrinth_of_mirage_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4760);
end

function sqc17_4760_labyrinth_of_mirage_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4760);
	local questID=4760;
end

function sqc17_4760_labyrinth_of_mirage_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc17_4760_labyrinth_of_mirage_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc17_4760_labyrinth_of_mirage_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4760, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4760, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4760, 1);
				npc_talk_index = "n1888_storyteller_kesey-1";
end

</GameServer>