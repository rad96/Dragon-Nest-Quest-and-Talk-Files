<VillageServer>

function mqc15_9441_empty_hope_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1558 then
		mqc15_9441_empty_hope_OnTalk_n1558_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1577 then
		mqc15_9441_empty_hope_OnTalk_n1577_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1578 then
		mqc15_9441_empty_hope_OnTalk_n1578_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1558_gaharam--------------------------------------------------------------------------------
function mqc15_9441_empty_hope_OnTalk_n1558_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1558_gaharam-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1558_gaharam-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1558_gaharam-2-b" then 
	end
	if npc_talk_index == "n1558_gaharam-2-c" then 
	end
	if npc_talk_index == "n1558_gaharam-2-d" then 
	end
	if npc_talk_index == "n1558_gaharam-2-e" then 
	end
	if npc_talk_index == "n1558_gaharam-2-level_check_1" then 
				if api_user_GetUserLevel(userObjID) >= 77 then
									npc_talk_index = "n1558_gaharam-2-g";

				else
									npc_talk_index = "n1558_gaharam-2-f";

				end
	end
	if npc_talk_index == "n1558_gaharam-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1577_gaharam--------------------------------------------------------------------------------
function mqc15_9441_empty_hope_OnTalk_n1577_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1577_gaharam-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1577_gaharam-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1577_gaharam-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1577_gaharam-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1577_gaharam-3-b" then 
	end
	if npc_talk_index == "n1577_gaharam-3-c" then 
	end
	if npc_talk_index == "n1577_gaharam-3-d" then 
	end
	if npc_talk_index == "n1577_gaharam-3-e" then 
	end
	if npc_talk_index == "n1577_gaharam-3-f" then 
	end
	if npc_talk_index == "n1577_gaharam-3-g" then 
	end
	if npc_talk_index == "n1577_gaharam-3-h" then 
	end
	if npc_talk_index == "n1577_gaharam-3-i" then 
	end
	if npc_talk_index == "n1577_gaharam-3-j" then 
	end
	if npc_talk_index == "n1577_gaharam-3-k" then 
	end
	if npc_talk_index == "n1577_gaharam-3-l" then 
	end
	if npc_talk_index == "n1577_gaharam-3-m" then 
	end
	if npc_talk_index == "n1577_gaharam-3-n" then 
	end
	if npc_talk_index == "n1577_gaharam-3-o" then 
	end
	if npc_talk_index == "n1577_gaharam-3-p" then 
	end
	if npc_talk_index == "n1577_gaharam-3-q" then 
	end
	if npc_talk_index == "n1577_gaharam-3-r" then 
	end
	if npc_talk_index == "n1577_gaharam-3-s" then 
	end
	if npc_talk_index == "n1577_gaharam-3-t" then 
	end
	if npc_talk_index == "n1577_gaharam-3-u" then 
	end
	if npc_talk_index == "n1577_gaharam-3-v" then 
	end
	if npc_talk_index == "n1577_gaharam-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1577_gaharam-5-b" then 
	end
	if npc_talk_index == "n1577_gaharam-5-c" then 
	end
	if npc_talk_index == "n1577_gaharam-5-d" then 
	end
	if npc_talk_index == "n1577_gaharam-5-e" then 
	end
	if npc_talk_index == "n1577_gaharam-5-f" then 
	end
	if npc_talk_index == "n1577_gaharam-5-g" then 
	end
	if npc_talk_index == "n1577_gaharam-5-h" then 
	end
	if npc_talk_index == "n1577_gaharam-5-i" then 
	end
	if npc_talk_index == "n1577_gaharam-5-j" then 
	end
	if npc_talk_index == "n1577_gaharam-5-k" then 
	end
	if npc_talk_index == "n1577_gaharam-5-l" then 
	end
	if npc_talk_index == "n1577_gaharam-5-m" then 
	end
	if npc_talk_index == "n1577_gaharam-5-n" then 
	end
	if npc_talk_index == "n1577_gaharam-5-o" then 
	end
	if npc_talk_index == "n1577_gaharam-5-class_check_0" then 
				if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1577_gaharam-5-q";

				else
									npc_talk_index = "n1577_gaharam-5-p";

				end
	end
	if npc_talk_index == "n1577_gaharam-5-r" then 
	end
	if npc_talk_index == "n1577_gaharam-5-r" then 
	end
	if npc_talk_index == "n1577_gaharam-5-s" then 
	end
	if npc_talk_index == "n1577_gaharam-5-t" then 
	end
	if npc_talk_index == "n1577_gaharam-5-u" then 
	end
	if npc_talk_index == "n1577_gaharam-5-v" then 
	end
	if npc_talk_index == "n1577_gaharam-5-w" then 
	end
	if npc_talk_index == "n1577_gaharam-5-x" then 
	end
	if npc_talk_index == "n1577_gaharam-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
	end
	if npc_talk_index == "n1577_gaharam-6-b" then 
	end
	if npc_talk_index == "n1577_gaharam-6-class_check_2" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1577_gaharam-6-c";

				else
									npc_talk_index = "n1577_gaharam-6-i";

				end
	end
	if npc_talk_index == "n1577_gaharam-6-d" then 
	end
	if npc_talk_index == "n1577_gaharam-6-e" then 
	end
	if npc_talk_index == "n1577_gaharam-6-f" then 
	end
	if npc_talk_index == "n1577_gaharam-6-g" then 
	end
	if npc_talk_index == "n1577_gaharam-6-h" then 
	end
	if npc_talk_index == "n1577_gaharam-6-i" then 
	end
	if npc_talk_index == "n1577_gaharam-6-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94410, true);
				 api_quest_RewardQuestUser(userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94410, true);
				 api_quest_RewardQuestUser(userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94410, true);
				 api_quest_RewardQuestUser(userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94410, true);
				 api_quest_RewardQuestUser(userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94410, true);
				 api_quest_RewardQuestUser(userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94410, true);
				 api_quest_RewardQuestUser(userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94410, true);
				 api_quest_RewardQuestUser(userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94410, true);
				 api_quest_RewardQuestUser(userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94410, true);
				 api_quest_RewardQuestUser(userObjID, 94410, questID, 1);
			 end 
	end
	if npc_talk_index == "n1577_gaharam-6-k" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9442, 2);
					api_quest_SetQuestStep(userObjID, 9442, 1);
					api_quest_SetJournalStep(userObjID, 9442, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1577_gaharam-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1577_gaharam-1", "mqc15_9442_first_desperation.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1578_for_memory--------------------------------------------------------------------------------
function mqc15_9441_empty_hope_OnTalk_n1578_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1578_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1578_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1578_for_memory-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1578_for_memory-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9441, 2);
				api_quest_SetJournalStep(userObjID,9441, 1);
				api_quest_SetQuestStep(userObjID,9441, 1);
				npc_talk_index = "n1578_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9441_empty_hope_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9441);
end

function mqc15_9441_empty_hope_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9441);
end

function mqc15_9441_empty_hope_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9441);
	local questID=9441;
end

function mqc15_9441_empty_hope_OnRemoteStart( userObjID, questID )
end

function mqc15_9441_empty_hope_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9441_empty_hope_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9441, 2);
				api_quest_SetJournalStep(userObjID,9441, 1);
				api_quest_SetQuestStep(userObjID,9441, 1);
				npc_talk_index = "n1578_for_memory-1";
end

</VillageServer>

<GameServer>
function mqc15_9441_empty_hope_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1558 then
		mqc15_9441_empty_hope_OnTalk_n1558_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1577 then
		mqc15_9441_empty_hope_OnTalk_n1577_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1578 then
		mqc15_9441_empty_hope_OnTalk_n1578_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1558_gaharam--------------------------------------------------------------------------------
function mqc15_9441_empty_hope_OnTalk_n1558_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1558_gaharam-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1558_gaharam-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1558_gaharam-2-b" then 
	end
	if npc_talk_index == "n1558_gaharam-2-c" then 
	end
	if npc_talk_index == "n1558_gaharam-2-d" then 
	end
	if npc_talk_index == "n1558_gaharam-2-e" then 
	end
	if npc_talk_index == "n1558_gaharam-2-level_check_1" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 77 then
									npc_talk_index = "n1558_gaharam-2-g";

				else
									npc_talk_index = "n1558_gaharam-2-f";

				end
	end
	if npc_talk_index == "n1558_gaharam-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1577_gaharam--------------------------------------------------------------------------------
function mqc15_9441_empty_hope_OnTalk_n1577_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1577_gaharam-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1577_gaharam-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1577_gaharam-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1577_gaharam-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1577_gaharam-3-b" then 
	end
	if npc_talk_index == "n1577_gaharam-3-c" then 
	end
	if npc_talk_index == "n1577_gaharam-3-d" then 
	end
	if npc_talk_index == "n1577_gaharam-3-e" then 
	end
	if npc_talk_index == "n1577_gaharam-3-f" then 
	end
	if npc_talk_index == "n1577_gaharam-3-g" then 
	end
	if npc_talk_index == "n1577_gaharam-3-h" then 
	end
	if npc_talk_index == "n1577_gaharam-3-i" then 
	end
	if npc_talk_index == "n1577_gaharam-3-j" then 
	end
	if npc_talk_index == "n1577_gaharam-3-k" then 
	end
	if npc_talk_index == "n1577_gaharam-3-l" then 
	end
	if npc_talk_index == "n1577_gaharam-3-m" then 
	end
	if npc_talk_index == "n1577_gaharam-3-n" then 
	end
	if npc_talk_index == "n1577_gaharam-3-o" then 
	end
	if npc_talk_index == "n1577_gaharam-3-p" then 
	end
	if npc_talk_index == "n1577_gaharam-3-q" then 
	end
	if npc_talk_index == "n1577_gaharam-3-r" then 
	end
	if npc_talk_index == "n1577_gaharam-3-s" then 
	end
	if npc_talk_index == "n1577_gaharam-3-t" then 
	end
	if npc_talk_index == "n1577_gaharam-3-u" then 
	end
	if npc_talk_index == "n1577_gaharam-3-v" then 
	end
	if npc_talk_index == "n1577_gaharam-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1577_gaharam-5-b" then 
	end
	if npc_talk_index == "n1577_gaharam-5-c" then 
	end
	if npc_talk_index == "n1577_gaharam-5-d" then 
	end
	if npc_talk_index == "n1577_gaharam-5-e" then 
	end
	if npc_talk_index == "n1577_gaharam-5-f" then 
	end
	if npc_talk_index == "n1577_gaharam-5-g" then 
	end
	if npc_talk_index == "n1577_gaharam-5-h" then 
	end
	if npc_talk_index == "n1577_gaharam-5-i" then 
	end
	if npc_talk_index == "n1577_gaharam-5-j" then 
	end
	if npc_talk_index == "n1577_gaharam-5-k" then 
	end
	if npc_talk_index == "n1577_gaharam-5-l" then 
	end
	if npc_talk_index == "n1577_gaharam-5-m" then 
	end
	if npc_talk_index == "n1577_gaharam-5-n" then 
	end
	if npc_talk_index == "n1577_gaharam-5-o" then 
	end
	if npc_talk_index == "n1577_gaharam-5-class_check_0" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1577_gaharam-5-q";

				else
									npc_talk_index = "n1577_gaharam-5-p";

				end
	end
	if npc_talk_index == "n1577_gaharam-5-r" then 
	end
	if npc_talk_index == "n1577_gaharam-5-r" then 
	end
	if npc_talk_index == "n1577_gaharam-5-s" then 
	end
	if npc_talk_index == "n1577_gaharam-5-t" then 
	end
	if npc_talk_index == "n1577_gaharam-5-u" then 
	end
	if npc_talk_index == "n1577_gaharam-5-v" then 
	end
	if npc_talk_index == "n1577_gaharam-5-w" then 
	end
	if npc_talk_index == "n1577_gaharam-5-x" then 
	end
	if npc_talk_index == "n1577_gaharam-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
	end
	if npc_talk_index == "n1577_gaharam-6-b" then 
	end
	if npc_talk_index == "n1577_gaharam-6-class_check_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1577_gaharam-6-c";

				else
									npc_talk_index = "n1577_gaharam-6-i";

				end
	end
	if npc_talk_index == "n1577_gaharam-6-d" then 
	end
	if npc_talk_index == "n1577_gaharam-6-e" then 
	end
	if npc_talk_index == "n1577_gaharam-6-f" then 
	end
	if npc_talk_index == "n1577_gaharam-6-g" then 
	end
	if npc_talk_index == "n1577_gaharam-6-h" then 
	end
	if npc_talk_index == "n1577_gaharam-6-i" then 
	end
	if npc_talk_index == "n1577_gaharam-6-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94410, questID, 1);
			 end 
	end
	if npc_talk_index == "n1577_gaharam-6-k" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9442, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9442, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9442, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1577_gaharam-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1577_gaharam-1", "mqc15_9442_first_desperation.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1578_for_memory--------------------------------------------------------------------------------
function mqc15_9441_empty_hope_OnTalk_n1578_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1578_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1578_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1578_for_memory-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1578_for_memory-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9441, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9441, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9441, 1);
				npc_talk_index = "n1578_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9441_empty_hope_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9441);
end

function mqc15_9441_empty_hope_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9441);
end

function mqc15_9441_empty_hope_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9441);
	local questID=9441;
end

function mqc15_9441_empty_hope_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9441_empty_hope_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9441_empty_hope_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9441, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9441, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9441, 1);
				npc_talk_index = "n1578_for_memory-1";
end

</GameServer>