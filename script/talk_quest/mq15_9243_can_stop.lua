<VillageServer>

function mq15_9243_can_stop_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 393 then
		mq15_9243_can_stop_OnTalk_n393_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n393_academic_station--------------------------------------------------------------------------------
function mq15_9243_can_stop_OnTalk_n393_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n393_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n393_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n393_academic_station-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n393_academic_station-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n393_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9243, 2);
				api_quest_SetJournalStep(userObjID,9243, 1);
				api_quest_SetQuestStep(userObjID,9243, 1);
				npc_talk_index = "n393_academic_station-1";

	end
	if npc_talk_index == "n393_academic_station-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 21112, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 400342, 1);
	end
	if npc_talk_index == "n393_academic_station-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92430, true);
				 api_quest_RewardQuestUser(userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92430, true);
				 api_quest_RewardQuestUser(userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92430, true);
				 api_quest_RewardQuestUser(userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92430, true);
				 api_quest_RewardQuestUser(userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92430, true);
				 api_quest_RewardQuestUser(userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92430, true);
				 api_quest_RewardQuestUser(userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92430, true);
				 api_quest_RewardQuestUser(userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92430, true);
				 api_quest_RewardQuestUser(userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92430, true);
				 api_quest_RewardQuestUser(userObjID, 92430, questID, 1);
			 end 
	end
	if npc_talk_index == "n393_academic_station-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9244, 2);
					api_quest_SetQuestStep(userObjID, 9244, 1);
					api_quest_SetJournalStep(userObjID, 9244, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400342, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400342, api_quest_HasQuestItem(userObjID, 400342, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n393_academic_station-1", "mq15_9244_what_to_do.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9243_can_stop_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9243);
	if qstep == 2 and CountIndex == 21112 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400342, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400342, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 400342 then

	end
end

function mq15_9243_can_stop_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9243);
	if qstep == 2 and CountIndex == 21112 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400342 and Count >= TargetCount  then

	end
end

function mq15_9243_can_stop_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9243);
	local questID=9243;
end

function mq15_9243_can_stop_OnRemoteStart( userObjID, questID )
end

function mq15_9243_can_stop_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9243_can_stop_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9243, 2);
				api_quest_SetJournalStep(userObjID,9243, 1);
				api_quest_SetQuestStep(userObjID,9243, 1);
				npc_talk_index = "n393_academic_station-1";
end

</VillageServer>

<GameServer>
function mq15_9243_can_stop_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 393 then
		mq15_9243_can_stop_OnTalk_n393_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n393_academic_station--------------------------------------------------------------------------------
function mq15_9243_can_stop_OnTalk_n393_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n393_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n393_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n393_academic_station-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n393_academic_station-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n393_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9243, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9243, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9243, 1);
				npc_talk_index = "n393_academic_station-1";

	end
	if npc_talk_index == "n393_academic_station-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 21112, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 400342, 1);
	end
	if npc_talk_index == "n393_academic_station-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92430, questID, 1);
			 end 
	end
	if npc_talk_index == "n393_academic_station-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9244, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9244, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9244, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400342, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400342, api_quest_HasQuestItem( pRoom, userObjID, 400342, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n393_academic_station-1", "mq15_9244_what_to_do.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9243_can_stop_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9243);
	if qstep == 2 and CountIndex == 21112 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400342, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400342, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 400342 then

	end
end

function mq15_9243_can_stop_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9243);
	if qstep == 2 and CountIndex == 21112 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400342 and Count >= TargetCount  then

	end
end

function mq15_9243_can_stop_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9243);
	local questID=9243;
end

function mq15_9243_can_stop_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9243_can_stop_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9243_can_stop_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9243, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9243, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9243, 1);
				npc_talk_index = "n393_academic_station-1";
end

</GameServer>