<VillageServer>

function sq34_4320_what_man_live_by1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1103 then
		sq34_4320_what_man_live_by1_OnTalk_n1103_trader_phara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1106 then
		sq34_4320_what_man_live_by1_OnTalk_n1106_shadowelf_risione(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1103_trader_phara--------------------------------------------------------------------------------
function sq34_4320_what_man_live_by1_OnTalk_n1103_trader_phara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1103_trader_phara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1103_trader_phara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1103_trader_phara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1103_trader_phara-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1103_trader_phara-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43200, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43200, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43200, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43200, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43200, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43200, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43200, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43200, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43200, false);
			 end 

	end
	if npc_talk_index == "n1103_trader_phara-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4320, 1);
				api_quest_SetJournalStep(userObjID,4320, 1);
				api_quest_SetQuestStep(userObjID,4320, 1);
				npc_talk_index = "n1103_trader_phara-1";

	end
	if npc_talk_index == "n1103_trader_phara-1-class_check" then 
				if api_user_GetUserClassID(userObjID) == 2 then
									npc_talk_index = "n1103_trader_phara-1-b";

				else
									if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1103_trader_phara-1-c";

				else
									npc_talk_index = "n1103_trader_phara-1-a";

				end

				end
	end
	if npc_talk_index == "n1103_trader_phara-1-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-1-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-1-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400389, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 601115, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 701115, 30000);
	end
	if npc_talk_index == "n1103_trader_phara-4-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43200, true);
				 api_quest_RewardQuestUser(userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43200, true);
				 api_quest_RewardQuestUser(userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43200, true);
				 api_quest_RewardQuestUser(userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43200, true);
				 api_quest_RewardQuestUser(userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43200, true);
				 api_quest_RewardQuestUser(userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43200, true);
				 api_quest_RewardQuestUser(userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43200, true);
				 api_quest_RewardQuestUser(userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43200, true);
				 api_quest_RewardQuestUser(userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43200, true);
				 api_quest_RewardQuestUser(userObjID, 43200, questID, 1);
			 end 
	end
	if npc_talk_index == "n1103_trader_phara-4-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4321, 1);
					api_quest_SetQuestStep(userObjID, 4321, 1);
					api_quest_SetJournalStep(userObjID, 4321, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1103_trader_phara-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1103_trader_phara-1", "sq34_4321_what_man_live_by2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1106_shadowelf_risione--------------------------------------------------------------------------------
function sq34_4320_what_man_live_by1_OnTalk_n1106_shadowelf_risione(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1106_shadowelf_risione-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1106_shadowelf_risione-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1106_shadowelf_risione-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1106_shadowelf_risione-3-b" then 
	end
	if npc_talk_index == "n1106_shadowelf_risione-3-c" then 
	end
	if npc_talk_index == "n1106_shadowelf_risione-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4320_what_man_live_by1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4320);
	if qstep == 2 and CountIndex == 400389 then

	end
	if qstep == 2 and CountIndex == 601115 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400389, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400389, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 701115 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400389, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400389, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq34_4320_what_man_live_by1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4320);
	if qstep == 2 and CountIndex == 400389 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 601115 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 701115 and Count >= TargetCount  then

	end
end

function sq34_4320_what_man_live_by1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4320);
	local questID=4320;
end

function sq34_4320_what_man_live_by1_OnRemoteStart( userObjID, questID )
end

function sq34_4320_what_man_live_by1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq34_4320_what_man_live_by1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4320, 1);
				api_quest_SetJournalStep(userObjID,4320, 1);
				api_quest_SetQuestStep(userObjID,4320, 1);
				npc_talk_index = "n1103_trader_phara-1";
end

</VillageServer>

<GameServer>
function sq34_4320_what_man_live_by1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1103 then
		sq34_4320_what_man_live_by1_OnTalk_n1103_trader_phara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1106 then
		sq34_4320_what_man_live_by1_OnTalk_n1106_shadowelf_risione( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1103_trader_phara--------------------------------------------------------------------------------
function sq34_4320_what_man_live_by1_OnTalk_n1103_trader_phara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1103_trader_phara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1103_trader_phara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1103_trader_phara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1103_trader_phara-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1103_trader_phara-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, false);
			 end 

	end
	if npc_talk_index == "n1103_trader_phara-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4320, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4320, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4320, 1);
				npc_talk_index = "n1103_trader_phara-1";

	end
	if npc_talk_index == "n1103_trader_phara-1-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									npc_talk_index = "n1103_trader_phara-1-b";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1103_trader_phara-1-c";

				else
									npc_talk_index = "n1103_trader_phara-1-a";

				end

				end
	end
	if npc_talk_index == "n1103_trader_phara-1-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-1-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-1-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400389, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 601115, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 701115, 30000);
	end
	if npc_talk_index == "n1103_trader_phara-4-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43200, questID, 1);
			 end 
	end
	if npc_talk_index == "n1103_trader_phara-4-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4321, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4321, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4321, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1103_trader_phara-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1103_trader_phara-1", "sq34_4321_what_man_live_by2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1106_shadowelf_risione--------------------------------------------------------------------------------
function sq34_4320_what_man_live_by1_OnTalk_n1106_shadowelf_risione( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1106_shadowelf_risione-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1106_shadowelf_risione-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1106_shadowelf_risione-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1106_shadowelf_risione-3-b" then 
	end
	if npc_talk_index == "n1106_shadowelf_risione-3-c" then 
	end
	if npc_talk_index == "n1106_shadowelf_risione-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4320_what_man_live_by1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4320);
	if qstep == 2 and CountIndex == 400389 then

	end
	if qstep == 2 and CountIndex == 601115 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400389, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400389, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 701115 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400389, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400389, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq34_4320_what_man_live_by1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4320);
	if qstep == 2 and CountIndex == 400389 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 601115 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 701115 and Count >= TargetCount  then

	end
end

function sq34_4320_what_man_live_by1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4320);
	local questID=4320;
end

function sq34_4320_what_man_live_by1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq34_4320_what_man_live_by1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq34_4320_what_man_live_by1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4320, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4320, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4320, 1);
				npc_talk_index = "n1103_trader_phara-1";
end

</GameServer>