<VillageServer>

function mqc01_9463_orb_to_find_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 11 then
		mqc01_9463_orb_to_find_OnTalk_n011_priest_leonardo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1686 then
		mqc01_9463_orb_to_find_OnTalk_n1686_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1687 then
		mqc01_9463_orb_to_find_OnTalk_n1687_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 206 then
		mqc01_9463_orb_to_find_OnTalk_n206_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n011_priest_leonardo--------------------------------------------------------------------------------
function mqc01_9463_orb_to_find_OnTalk_n011_priest_leonardo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n011_priest_leonardo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n011_priest_leonardo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n011_priest_leonardo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n011_priest_leonardo-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n011_priest_leonardo-accepting-!next" then
				api_quest_AddQuest(userObjID,9463, 2);
				api_quest_SetJournalStep(userObjID,9463, 1);
				api_quest_SetQuestStep(userObjID,9463, 1);
				npc_talk_index = "n011_priest_leonardo-1";

	end
	if npc_talk_index == "n011_priest_leonardo-1-b" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-c" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-d" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-e" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-f" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-g" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-h" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-i" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-j" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-k" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-l" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-m" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-n" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-o" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-p" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n011_priest_leonardo-6-b" then 

				if api_quest_HasQuestItem(userObjID, 300059, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300059, api_quest_HasQuestItem(userObjID, 300059, 1));
				end
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94630, true);
				 api_quest_RewardQuestUser(userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94630, true);
				 api_quest_RewardQuestUser(userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94630, true);
				 api_quest_RewardQuestUser(userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94630, true);
				 api_quest_RewardQuestUser(userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94630, true);
				 api_quest_RewardQuestUser(userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94630, true);
				 api_quest_RewardQuestUser(userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94630, true);
				 api_quest_RewardQuestUser(userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94630, true);
				 api_quest_RewardQuestUser(userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94630, true);
				 api_quest_RewardQuestUser(userObjID, 94630, questID, 1);
			 end 
	end
	if npc_talk_index == "n011_priest_leonardo-6-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9464, 2);
					api_quest_SetQuestStep(userObjID, 9464, 1);
					api_quest_SetJournalStep(userObjID, 9464, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n011_priest_leonardo-1", "mqc01_9464_first_mission.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1686_eltia--------------------------------------------------------------------------------
function mqc01_9463_orb_to_find_OnTalk_n1686_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1686_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1686_eltia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1686_eltia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1686_eltia-2-b" then 
	end
	if npc_talk_index == "n1686_eltia-2-c" then 
	end
	if npc_talk_index == "n1686_eltia-2-d" then 
	end
	if npc_talk_index == "n1686_eltia-2-e" then 
	end
	if npc_talk_index == "n1686_eltia-2-f" then 
	end
	if npc_talk_index == "n1686_eltia-2-g" then 
	end
	if npc_talk_index == "n1686_eltia-2-h" then 
	end
	if npc_talk_index == "n1686_eltia-2-i" then 
	end
	if npc_talk_index == "n1686_eltia-2-j" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1687_kaye--------------------------------------------------------------------------------
function mqc01_9463_orb_to_find_OnTalk_n1687_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1687_kaye-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1687_kaye-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1687_kaye-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1687_kaye-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1687_kaye-3-b" then 
	end
	if npc_talk_index == "n1687_kaye-3-c" then 
	end
	if npc_talk_index == "n1687_kaye-3-d" then 
	end
	if npc_talk_index == "n1687_kaye-3-e" then 
	end
	if npc_talk_index == "n1687_kaye-3-f" then 
	end
	if npc_talk_index == "n1687_kaye-3-g" then 
	end
	if npc_talk_index == "n1687_kaye-3-m" then 
	end
	if npc_talk_index == "n1687_kaye-3-n" then 
	end
	if npc_talk_index == "n1687_kaye-3-h" then 
	end
	if npc_talk_index == "n1687_kaye-3-i" then 
	end
	if npc_talk_index == "n1687_kaye-3-j" then 
	end
	if npc_talk_index == "n1687_kaye-3-k" then 
	end
	if npc_talk_index == "n1687_kaye-3-l" then 
	end
	if npc_talk_index == "n1687_kaye-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 269, 1);
	end
	if npc_talk_index == "n1687_kaye-3-f" then 
	end
	if npc_talk_index == "n1687_kaye-3-f" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n206_cleric_jake--------------------------------------------------------------------------------
function mqc01_9463_orb_to_find_OnTalk_n206_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n206_cleric_jake-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n206_cleric_jake-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n206_cleric_jake-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n206_cleric_jake-5-b" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-c" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-d" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-e" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-f" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-g" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-h" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-i" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-j" then 
	end
	if npc_talk_index == "n206_cleric_jake-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9463_orb_to_find_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9463);
	if qstep == 4 and CountIndex == 269 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300059, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300059, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);

	end
end

function mqc01_9463_orb_to_find_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9463);
	if qstep == 4 and CountIndex == 269 and Count >= TargetCount  then

	end
end

function mqc01_9463_orb_to_find_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9463);
	local questID=9463;
end

function mqc01_9463_orb_to_find_OnRemoteStart( userObjID, questID )
end

function mqc01_9463_orb_to_find_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc01_9463_orb_to_find_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mqc01_9463_orb_to_find_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 11 then
		mqc01_9463_orb_to_find_OnTalk_n011_priest_leonardo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1686 then
		mqc01_9463_orb_to_find_OnTalk_n1686_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1687 then
		mqc01_9463_orb_to_find_OnTalk_n1687_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 206 then
		mqc01_9463_orb_to_find_OnTalk_n206_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n011_priest_leonardo--------------------------------------------------------------------------------
function mqc01_9463_orb_to_find_OnTalk_n011_priest_leonardo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n011_priest_leonardo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n011_priest_leonardo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n011_priest_leonardo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n011_priest_leonardo-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n011_priest_leonardo-accepting-!next" then
				api_quest_AddQuest( pRoom, userObjID,9463, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9463, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9463, 1);
				npc_talk_index = "n011_priest_leonardo-1";

	end
	if npc_talk_index == "n011_priest_leonardo-1-b" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-c" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-d" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-e" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-f" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-g" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-h" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-i" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-j" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-k" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-l" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-m" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-n" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-o" then 
	end
	if npc_talk_index == "n011_priest_leonardo-1-p" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n011_priest_leonardo-6-b" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300059, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300059, api_quest_HasQuestItem( pRoom, userObjID, 300059, 1));
				end
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94630, questID, 1);
			 end 
	end
	if npc_talk_index == "n011_priest_leonardo-6-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9464, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9464, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9464, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n011_priest_leonardo-1", "mqc01_9464_first_mission.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1686_eltia--------------------------------------------------------------------------------
function mqc01_9463_orb_to_find_OnTalk_n1686_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1686_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1686_eltia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1686_eltia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1686_eltia-2-b" then 
	end
	if npc_talk_index == "n1686_eltia-2-c" then 
	end
	if npc_talk_index == "n1686_eltia-2-d" then 
	end
	if npc_talk_index == "n1686_eltia-2-e" then 
	end
	if npc_talk_index == "n1686_eltia-2-f" then 
	end
	if npc_talk_index == "n1686_eltia-2-g" then 
	end
	if npc_talk_index == "n1686_eltia-2-h" then 
	end
	if npc_talk_index == "n1686_eltia-2-i" then 
	end
	if npc_talk_index == "n1686_eltia-2-j" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1687_kaye--------------------------------------------------------------------------------
function mqc01_9463_orb_to_find_OnTalk_n1687_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1687_kaye-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1687_kaye-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1687_kaye-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1687_kaye-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1687_kaye-3-b" then 
	end
	if npc_talk_index == "n1687_kaye-3-c" then 
	end
	if npc_talk_index == "n1687_kaye-3-d" then 
	end
	if npc_talk_index == "n1687_kaye-3-e" then 
	end
	if npc_talk_index == "n1687_kaye-3-f" then 
	end
	if npc_talk_index == "n1687_kaye-3-g" then 
	end
	if npc_talk_index == "n1687_kaye-3-m" then 
	end
	if npc_talk_index == "n1687_kaye-3-n" then 
	end
	if npc_talk_index == "n1687_kaye-3-h" then 
	end
	if npc_talk_index == "n1687_kaye-3-i" then 
	end
	if npc_talk_index == "n1687_kaye-3-j" then 
	end
	if npc_talk_index == "n1687_kaye-3-k" then 
	end
	if npc_talk_index == "n1687_kaye-3-l" then 
	end
	if npc_talk_index == "n1687_kaye-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 269, 1);
	end
	if npc_talk_index == "n1687_kaye-3-f" then 
	end
	if npc_talk_index == "n1687_kaye-3-f" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n206_cleric_jake--------------------------------------------------------------------------------
function mqc01_9463_orb_to_find_OnTalk_n206_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n206_cleric_jake-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n206_cleric_jake-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n206_cleric_jake-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n206_cleric_jake-5-b" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-c" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-d" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-e" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-f" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-g" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-h" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-i" then 
	end
	if npc_talk_index == "n206_cleric_jake-5-j" then 
	end
	if npc_talk_index == "n206_cleric_jake-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9463_orb_to_find_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9463);
	if qstep == 4 and CountIndex == 269 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300059, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300059, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

	end
end

function mqc01_9463_orb_to_find_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9463);
	if qstep == 4 and CountIndex == 269 and Count >= TargetCount  then

	end
end

function mqc01_9463_orb_to_find_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9463);
	local questID=9463;
end

function mqc01_9463_orb_to_find_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc01_9463_orb_to_find_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc01_9463_orb_to_find_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>