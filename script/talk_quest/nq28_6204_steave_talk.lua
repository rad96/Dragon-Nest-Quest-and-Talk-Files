<VillageServer>

function nq28_6204_steave_talk_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 4 then
		nq28_6204_steave_talk_OnTalk_n004_guard_steave(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n004_guard_steave--------------------------------------------------------------------------------
function nq28_6204_steave_talk_OnTalk_n004_guard_steave(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n004_guard_steave-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n004_guard_steave-accepting-a" then
				api_npc_AddFavorPoint( userObjID, 4, 100 );
				api_quest_ForceCompleteQuest(userObjID, 6204, 0, 1, 1, 0);

	end
	if npc_talk_index == "n004_guard_steave-accepting-b" then
				api_npc_AddMalicePoint( userObjID, 4, 200 );
				api_quest_ForceCompleteQuest(userObjID, 6204, 0, 1, 1, 0);

	end
	if npc_talk_index == "n004_guard_steave-accepting-c" then
				api_npc_AddMalicePoint( userObjID, 4, 100 );
				api_quest_ForceCompleteQuest(userObjID, 6204, 0, 1, 1, 0);

	end
	if npc_talk_index == "n004_guard_steave-accepting-d" then
				api_npc_AddFavorPoint( userObjID, 4, 200 );
				api_quest_ForceCompleteQuest(userObjID, 6204, 0, 1, 1, 0);

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq28_6204_steave_talk_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6204);
end

function nq28_6204_steave_talk_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6204);
end

function nq28_6204_steave_talk_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6204);
	local questID=6204;
end

function nq28_6204_steave_talk_OnRemoteStart( userObjID, questID )
end

function nq28_6204_steave_talk_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq28_6204_steave_talk_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq28_6204_steave_talk_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 4 then
		nq28_6204_steave_talk_OnTalk_n004_guard_steave( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n004_guard_steave--------------------------------------------------------------------------------
function nq28_6204_steave_talk_OnTalk_n004_guard_steave( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n004_guard_steave-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n004_guard_steave-accepting-a" then
				api_npc_AddFavorPoint( pRoom,  userObjID, 4, 100 );
				api_quest_ForceCompleteQuest( pRoom, userObjID, 6204, 0, 1, 1, 0);

	end
	if npc_talk_index == "n004_guard_steave-accepting-b" then
				api_npc_AddMalicePoint( pRoom,  userObjID, 4, 200 );
				api_quest_ForceCompleteQuest( pRoom, userObjID, 6204, 0, 1, 1, 0);

	end
	if npc_talk_index == "n004_guard_steave-accepting-c" then
				api_npc_AddMalicePoint( pRoom,  userObjID, 4, 100 );
				api_quest_ForceCompleteQuest( pRoom, userObjID, 6204, 0, 1, 1, 0);

	end
	if npc_talk_index == "n004_guard_steave-accepting-d" then
				api_npc_AddFavorPoint( pRoom,  userObjID, 4, 200 );
				api_quest_ForceCompleteQuest( pRoom, userObjID, 6204, 0, 1, 1, 0);

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq28_6204_steave_talk_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6204);
end

function nq28_6204_steave_talk_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6204);
end

function nq28_6204_steave_talk_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6204);
	local questID=6204;
end

function nq28_6204_steave_talk_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq28_6204_steave_talk_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq28_6204_steave_talk_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>