<VillageServer>

function sq15_4169_flower_of_desert3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 304 then
		sq15_4169_flower_of_desert3_OnTalk_n304_auction_manger_duwen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 91 then
		sq15_4169_flower_of_desert3_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n304_auction_manger_duwen--------------------------------------------------------------------------------
function sq15_4169_flower_of_desert3_OnTalk_n304_auction_manger_duwen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n304_auction_manger_duwen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n304_auction_manger_duwen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n304_auction_manger_duwen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n304_auction_manger_duwen-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n304_auction_manger_duwen-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n304_auction_manger_duwen-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41690, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41690, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41690, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41690, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41690, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41690, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41690, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41690, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41690, false);
			 end 

	end
	if npc_talk_index == "n304_auction_manger_duwen-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4169, 1);
				api_quest_SetJournalStep(userObjID,4169, 1);
				api_quest_SetQuestStep(userObjID,4169, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400366, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 600561, 30000);
				npc_talk_index = "n304_auction_manger_duwen-1";
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700561, 30000);

	end
	if npc_talk_index == "n304_auction_manger_duwen-2-b" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-c" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-c" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-d" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-e" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-f" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-f" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-g" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-h" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-i" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-j" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-k" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n304_auction_manger_duwen-4-b" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-4-c" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41690, true);
				 api_quest_RewardQuestUser(userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41690, true);
				 api_quest_RewardQuestUser(userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41690, true);
				 api_quest_RewardQuestUser(userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41690, true);
				 api_quest_RewardQuestUser(userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41690, true);
				 api_quest_RewardQuestUser(userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41690, true);
				 api_quest_RewardQuestUser(userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41690, true);
				 api_quest_RewardQuestUser(userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41690, true);
				 api_quest_RewardQuestUser(userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41690, true);
				 api_quest_RewardQuestUser(userObjID, 41690, questID, 1);
			 end 
	end
	if npc_talk_index == "n304_auction_manger_duwen-4-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n091_trader_bellin--------------------------------------------------------------------------------
function sq15_4169_flower_of_desert3_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n091_trader_bellin-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n091_trader_bellin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n091_trader_bellin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n091_trader_bellin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-3-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-c" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-d" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-e" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-f" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-g" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-h" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-i" then 
	end
	if npc_talk_index == "n091_trader_bellin-4" then 
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_SetQuestStep(userObjID, questID,4);

				if api_quest_HasQuestItem(userObjID, 400366, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400366, api_quest_HasQuestItem(userObjID, 400366, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4169_flower_of_desert3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4169);
	if qstep == 1 and CountIndex == 400366 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestStep(userObjID, questID,2);

	end
	if qstep == 1 and CountIndex == 600561 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400366, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400366, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700561 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400366, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400366, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_4169_flower_of_desert3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4169);
	if qstep == 1 and CountIndex == 400366 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600561 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700561 and Count >= TargetCount  then

	end
end

function sq15_4169_flower_of_desert3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4169);
	local questID=4169;
end

function sq15_4169_flower_of_desert3_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400366, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 600561, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700561, 30000);
end

function sq15_4169_flower_of_desert3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4169_flower_of_desert3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4169, 1);
				api_quest_SetJournalStep(userObjID,4169, 1);
				api_quest_SetQuestStep(userObjID,4169, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400366, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 600561, 30000);
				npc_talk_index = "n304_auction_manger_duwen-1";
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700561, 30000);
end

</VillageServer>

<GameServer>
function sq15_4169_flower_of_desert3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 304 then
		sq15_4169_flower_of_desert3_OnTalk_n304_auction_manger_duwen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 91 then
		sq15_4169_flower_of_desert3_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n304_auction_manger_duwen--------------------------------------------------------------------------------
function sq15_4169_flower_of_desert3_OnTalk_n304_auction_manger_duwen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n304_auction_manger_duwen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n304_auction_manger_duwen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n304_auction_manger_duwen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n304_auction_manger_duwen-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n304_auction_manger_duwen-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n304_auction_manger_duwen-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, false);
			 end 

	end
	if npc_talk_index == "n304_auction_manger_duwen-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4169, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4169, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4169, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400366, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 600561, 30000);
				npc_talk_index = "n304_auction_manger_duwen-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700561, 30000);

	end
	if npc_talk_index == "n304_auction_manger_duwen-2-b" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-c" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-c" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-d" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-e" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-f" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-f" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-g" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-h" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-i" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-j" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-2-k" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n304_auction_manger_duwen-4-b" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-4-c" then 
	end
	if npc_talk_index == "n304_auction_manger_duwen-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41690, questID, 1);
			 end 
	end
	if npc_talk_index == "n304_auction_manger_duwen-4-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n091_trader_bellin--------------------------------------------------------------------------------
function sq15_4169_flower_of_desert3_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n091_trader_bellin-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n091_trader_bellin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n091_trader_bellin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n091_trader_bellin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-3-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-c" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-d" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-e" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-f" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-g" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-h" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-i" then 
	end
	if npc_talk_index == "n091_trader_bellin-4" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);

				if api_quest_HasQuestItem( pRoom, userObjID, 400366, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400366, api_quest_HasQuestItem( pRoom, userObjID, 400366, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4169_flower_of_desert3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4169);
	if qstep == 1 and CountIndex == 400366 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);

	end
	if qstep == 1 and CountIndex == 600561 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400366, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400366, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700561 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400366, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400366, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_4169_flower_of_desert3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4169);
	if qstep == 1 and CountIndex == 400366 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600561 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700561 and Count >= TargetCount  then

	end
end

function sq15_4169_flower_of_desert3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4169);
	local questID=4169;
end

function sq15_4169_flower_of_desert3_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400366, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 600561, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700561, 30000);
end

function sq15_4169_flower_of_desert3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4169_flower_of_desert3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4169, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4169, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4169, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400366, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 600561, 30000);
				npc_talk_index = "n304_auction_manger_duwen-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700561, 30000);
end

</GameServer>