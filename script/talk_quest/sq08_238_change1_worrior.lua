<VillageServer>

function sq08_238_change1_worrior_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1213 then
		sq08_238_change1_worrior_OnTalk_n1213_rambert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1214 then
		sq08_238_change1_worrior_OnTalk_n1214_empty_place(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 32 then
		sq08_238_change1_worrior_OnTalk_n032_warrior_master_chandler(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1213_rambert--------------------------------------------------------------------------------
function sq08_238_change1_worrior_OnTalk_n1213_rambert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1214_empty_place--------------------------------------------------------------------------------
function sq08_238_change1_worrior_OnTalk_n1214_empty_place(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1214_empty_place-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1214_empty_place-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1214_empty_place-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1214_empty_place-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1214_empty_place-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1214_empty_place-3-b" then 
	end
	if npc_talk_index == "n1214_empty_place-3-open_ui" then 
				api_ui_OpenJobChange(userObjID)
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1214_empty_place-4-open_ui" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1214_empty_place-6-get_skill" then 
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_user_SetSecondJobSkill( userObjID, 75 );
	end
	if npc_talk_index == "n1214_empty_place-7-go_reset" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1214_empty_place-7-ge_dark" then 
				api_user_SetUserJobID(userObjID, 75);
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n032_warrior_master_chandler--------------------------------------------------------------------------------
function sq08_238_change1_worrior_OnTalk_n032_warrior_master_chandler(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n032_warrior_master_chandler-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n032_warrior_master_chandler-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n032_warrior_master_chandler-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n032_warrior_master_chandler-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n032_warrior_master_chandler-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2380, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2380, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2380, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2380, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2380, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2380, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2380, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2380, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2380, false);
			 end 

	end
	if npc_talk_index == "n032_warrior_master_chandler-accepting-acceptted" then
				api_quest_AddQuest(userObjID,238, 1);
				api_quest_SetJournalStep(userObjID,238, 1);
				api_quest_SetQuestStep(userObjID,238, 1);
				npc_talk_index = "n032_warrior_master_chandler-1";

	end
	if npc_talk_index == "n032_warrior_master_chandler-1-b" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-d" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-check_com" then 
				if api_user_GetUserJobID(userObjID) == 11  then
									npc_talk_index = "n032_warrior_master_chandler-1-e";

				else
									if api_user_GetUserJobID(userObjID) == 12  then
									npc_talk_index = "n032_warrior_master_chandler-1-e";

				else
									if api_user_GetUserJobID(userObjID) == 23  then
									npc_talk_index = "n032_warrior_master_chandler-1-e";

				else
									if api_user_GetUserJobID(userObjID) == 24  then
									npc_talk_index = "n032_warrior_master_chandler-1-e";

				else
									if api_user_GetUserJobID(userObjID) == 25  then
									npc_talk_index = "n032_warrior_master_chandler-1-e";

				else
									if api_user_GetUserJobID(userObjID) == 26  then
									npc_talk_index = "n032_warrior_master_chandler-1-e";

				else
									npc_talk_index = "n032_warrior_master_chandler-1-f";

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-g" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-h" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-i" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-j" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-k" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-l" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-m" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-m" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-m" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-n" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-party_chek" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n032_warrior_master_chandler-1-o";

				else
									npc_talk_index = "n032_warrior_master_chandler-1-q";

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-p" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-go_map" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13520,1);
	end
	if npc_talk_index == "n032_warrior_master_chandler-2-party_chek" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_user_ChangeMap(userObjID,13520,1);

				else
									npc_talk_index = "n032_warrior_master_chandler-2-a";

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-b" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-d" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-e" then 
				if api_user_GetUserJobID(userObjID) == 11  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2381, true);
				 api_quest_RewardQuestUser(userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2381, true);
				 api_quest_RewardQuestUser(userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2381, true);
				 api_quest_RewardQuestUser(userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2381, true);
				 api_quest_RewardQuestUser(userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2381, true);
				 api_quest_RewardQuestUser(userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2381, true);
				 api_quest_RewardQuestUser(userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2381, true);
				 api_quest_RewardQuestUser(userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2381, true);
				 api_quest_RewardQuestUser(userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2381, true);
				 api_quest_RewardQuestUser(userObjID, 2381, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2382, true);
				 api_quest_RewardQuestUser(userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2382, true);
				 api_quest_RewardQuestUser(userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2382, true);
				 api_quest_RewardQuestUser(userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2382, true);
				 api_quest_RewardQuestUser(userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2382, true);
				 api_quest_RewardQuestUser(userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2382, true);
				 api_quest_RewardQuestUser(userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2382, true);
				 api_quest_RewardQuestUser(userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2382, true);
				 api_quest_RewardQuestUser(userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2382, true);
				 api_quest_RewardQuestUser(userObjID, 2382, questID, 1);
			 end 

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 340, 1);
					api_quest_SetQuestStep(userObjID, 340, 1);
					api_quest_SetJournalStep(userObjID, 340, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-g" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n032_warrior_master_chandler-1", "sq08_340_change_job1.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_238_change1_worrior_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 238);
end

function sq08_238_change1_worrior_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 238);
end

function sq08_238_change1_worrior_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 238);
	local questID=238;
end

function sq08_238_change1_worrior_OnRemoteStart( userObjID, questID )
end

function sq08_238_change1_worrior_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_238_change1_worrior_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,238, 1);
				api_quest_SetJournalStep(userObjID,238, 1);
				api_quest_SetQuestStep(userObjID,238, 1);
				npc_talk_index = "n032_warrior_master_chandler-1";
end

</VillageServer>

<GameServer>
function sq08_238_change1_worrior_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1213 then
		sq08_238_change1_worrior_OnTalk_n1213_rambert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1214 then
		sq08_238_change1_worrior_OnTalk_n1214_empty_place( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 32 then
		sq08_238_change1_worrior_OnTalk_n032_warrior_master_chandler( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1213_rambert--------------------------------------------------------------------------------
function sq08_238_change1_worrior_OnTalk_n1213_rambert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1214_empty_place--------------------------------------------------------------------------------
function sq08_238_change1_worrior_OnTalk_n1214_empty_place( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1214_empty_place-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1214_empty_place-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1214_empty_place-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1214_empty_place-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1214_empty_place-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1214_empty_place-3-b" then 
	end
	if npc_talk_index == "n1214_empty_place-3-open_ui" then 
				api_ui_OpenJobChange( pRoom, userObjID)
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1214_empty_place-4-open_ui" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1214_empty_place-6-get_skill" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_user_SetSecondJobSkill( pRoom,  userObjID, 75 );
	end
	if npc_talk_index == "n1214_empty_place-7-go_reset" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1214_empty_place-7-ge_dark" then 
				api_user_SetUserJobID( pRoom, userObjID, 75);
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n032_warrior_master_chandler--------------------------------------------------------------------------------
function sq08_238_change1_worrior_OnTalk_n032_warrior_master_chandler( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n032_warrior_master_chandler-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n032_warrior_master_chandler-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n032_warrior_master_chandler-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n032_warrior_master_chandler-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n032_warrior_master_chandler-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2380, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2380, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2380, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2380, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2380, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2380, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2380, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2380, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2380, false);
			 end 

	end
	if npc_talk_index == "n032_warrior_master_chandler-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,238, 1);
				api_quest_SetJournalStep( pRoom, userObjID,238, 1);
				api_quest_SetQuestStep( pRoom, userObjID,238, 1);
				npc_talk_index = "n032_warrior_master_chandler-1";

	end
	if npc_talk_index == "n032_warrior_master_chandler-1-b" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-d" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-check_com" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 11  then
									npc_talk_index = "n032_warrior_master_chandler-1-e";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 12  then
									npc_talk_index = "n032_warrior_master_chandler-1-e";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 23  then
									npc_talk_index = "n032_warrior_master_chandler-1-e";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 24  then
									npc_talk_index = "n032_warrior_master_chandler-1-e";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 25  then
									npc_talk_index = "n032_warrior_master_chandler-1-e";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 26  then
									npc_talk_index = "n032_warrior_master_chandler-1-e";

				else
									npc_talk_index = "n032_warrior_master_chandler-1-f";

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-g" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-h" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-i" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-j" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-k" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-l" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-m" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-m" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-m" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-n" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-party_chek" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n032_warrior_master_chandler-1-o";

				else
									npc_talk_index = "n032_warrior_master_chandler-1-q";

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-p" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-1-go_map" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13520,1);
	end
	if npc_talk_index == "n032_warrior_master_chandler-2-party_chek" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_user_ChangeMap( pRoom, userObjID,13520,1);

				else
									npc_talk_index = "n032_warrior_master_chandler-2-a";

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-b" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-d" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-e" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 11  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2381, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2381, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2381, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2381, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2381, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2381, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2381, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2381, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2381, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2381, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2381, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2382, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2382, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2382, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2382, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2382, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2382, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2382, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2382, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2382, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2382, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2382, questID, 1);
			 end 

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 340, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 340, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 340, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-g" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n032_warrior_master_chandler-1", "sq08_340_change_job1.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_238_change1_worrior_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 238);
end

function sq08_238_change1_worrior_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 238);
end

function sq08_238_change1_worrior_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 238);
	local questID=238;
end

function sq08_238_change1_worrior_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_238_change1_worrior_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_238_change1_worrior_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,238, 1);
				api_quest_SetJournalStep( pRoom, userObjID,238, 1);
				api_quest_SetQuestStep( pRoom, userObjID,238, 1);
				npc_talk_index = "n032_warrior_master_chandler-1";
end

</GameServer>