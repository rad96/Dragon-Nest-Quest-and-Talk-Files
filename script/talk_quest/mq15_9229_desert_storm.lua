<VillageServer>

function mq15_9229_desert_storm_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 45 then
		mq15_9229_desert_storm_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 880 then
		mq15_9229_desert_storm_OnTalk_n880_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function mq15_9229_desert_storm_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n045_soceress_master_stella-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_quest_AddQuest(userObjID,9229, 2);
				api_quest_SetJournalStep(userObjID,9229, 1);
				api_quest_SetQuestStep(userObjID,9229, 1);

	end
	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-j" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-k" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-l" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-m" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-n" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n045_soceress_master_stella-4-lvchk" then 
				if api_user_GetUserLevel(userObjID) >= 58 then
									npc_talk_index = "n045_soceress_master_stella-4-b";

				else
									npc_talk_index = "n045_soceress_master_stella-4-c";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-4-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-j" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-k" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-l" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-m" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-n" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-o" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-p" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-q" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-r" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-s" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92290, true);
				 api_quest_RewardQuestUser(userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92290, true);
				 api_quest_RewardQuestUser(userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92290, true);
				 api_quest_RewardQuestUser(userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92290, true);
				 api_quest_RewardQuestUser(userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92290, true);
				 api_quest_RewardQuestUser(userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92290, true);
				 api_quest_RewardQuestUser(userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92290, true);
				 api_quest_RewardQuestUser(userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92290, true);
				 api_quest_RewardQuestUser(userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92290, true);
				 api_quest_RewardQuestUser(userObjID, 92290, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-t" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9246, 2);
					api_quest_SetQuestStep(userObjID, 9246, 1);
					api_quest_SetJournalStep(userObjID, 9246, 1);

					
				if api_quest_HasQuestItem(userObjID, 400337, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400337, api_quest_HasQuestItem(userObjID, 400337, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n045_soceress_master_stella-1", "mq15_9246_portent_of_disaster.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n880_academic--------------------------------------------------------------------------------
function mq15_9229_desert_storm_OnTalk_n880_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n880_academic-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n880_academic-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n880_academic-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n880_academic-3-b" then 
	end
	if npc_talk_index == "n880_academic-3-c" then 
	end
	if npc_talk_index == "n880_academic-3-d" then 
	end
	if npc_talk_index == "n880_academic-3-e" then 
	end
	if npc_talk_index == "n880_academic-3-f" then 
	end
	if npc_talk_index == "n880_academic-3-g" then 
	end
	if npc_talk_index == "n880_academic-3-h" then 
	end
	if npc_talk_index == "n880_academic-3-i" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400337, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400337, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n880_academic-3-j" then 
	end
	if npc_talk_index == "n880_academic-3-k" then 
	end
	if npc_talk_index == "n880_academic-3-l" then 
	end
	if npc_talk_index == "n880_academic-3-m" then 
	end
	if npc_talk_index == "n880_academic-3-n" then 
	end
	if npc_talk_index == "n880_academic-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9229_desert_storm_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9229);
end

function mq15_9229_desert_storm_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9229);
end

function mq15_9229_desert_storm_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9229);
	local questID=9229;
end

function mq15_9229_desert_storm_OnRemoteStart( userObjID, questID )
end

function mq15_9229_desert_storm_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9229_desert_storm_ForceAccept( userObjID, npcObjID, questID )
				npc_talk_index = "n045_soceress_master_stella-1";
				api_quest_AddQuest(userObjID,9229, 2);
				api_quest_SetJournalStep(userObjID,9229, 1);
				api_quest_SetQuestStep(userObjID,9229, 1);
end

</VillageServer>

<GameServer>
function mq15_9229_desert_storm_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 45 then
		mq15_9229_desert_storm_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 880 then
		mq15_9229_desert_storm_OnTalk_n880_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function mq15_9229_desert_storm_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n045_soceress_master_stella-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_quest_AddQuest( pRoom, userObjID,9229, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9229, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9229, 1);

	end
	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-j" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-k" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-l" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-m" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-n" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n045_soceress_master_stella-4-lvchk" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 58 then
									npc_talk_index = "n045_soceress_master_stella-4-b";

				else
									npc_talk_index = "n045_soceress_master_stella-4-c";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-4-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-j" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-k" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-l" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-m" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-n" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-o" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-p" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-q" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-r" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-s" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92290, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-t" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9246, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9246, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9246, 1);

					
				if api_quest_HasQuestItem( pRoom, userObjID, 400337, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400337, api_quest_HasQuestItem( pRoom, userObjID, 400337, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-1", "mq15_9246_portent_of_disaster.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n880_academic--------------------------------------------------------------------------------
function mq15_9229_desert_storm_OnTalk_n880_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n880_academic-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n880_academic-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n880_academic-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n880_academic-3-b" then 
	end
	if npc_talk_index == "n880_academic-3-c" then 
	end
	if npc_talk_index == "n880_academic-3-d" then 
	end
	if npc_talk_index == "n880_academic-3-e" then 
	end
	if npc_talk_index == "n880_academic-3-f" then 
	end
	if npc_talk_index == "n880_academic-3-g" then 
	end
	if npc_talk_index == "n880_academic-3-h" then 
	end
	if npc_talk_index == "n880_academic-3-i" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400337, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400337, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n880_academic-3-j" then 
	end
	if npc_talk_index == "n880_academic-3-k" then 
	end
	if npc_talk_index == "n880_academic-3-l" then 
	end
	if npc_talk_index == "n880_academic-3-m" then 
	end
	if npc_talk_index == "n880_academic-3-n" then 
	end
	if npc_talk_index == "n880_academic-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9229_desert_storm_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9229);
end

function mq15_9229_desert_storm_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9229);
end

function mq15_9229_desert_storm_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9229);
	local questID=9229;
end

function mq15_9229_desert_storm_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9229_desert_storm_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9229_desert_storm_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				npc_talk_index = "n045_soceress_master_stella-1";
				api_quest_AddQuest( pRoom, userObjID,9229, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9229, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9229, 1);
end

</GameServer>