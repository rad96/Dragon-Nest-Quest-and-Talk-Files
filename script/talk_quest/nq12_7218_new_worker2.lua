<VillageServer>

function nq12_7218_new_worker2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 277 then
		nq12_7218_new_worker2_OnTalk_n277_bluff_dealers_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 65 then
		nq12_7218_new_worker2_OnTalk_n065_goblinson04_gosuk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 91 then
		nq12_7218_new_worker2_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n277_bluff_dealers_pope--------------------------------------------------------------------------------
function nq12_7218_new_worker2_OnTalk_n277_bluff_dealers_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n277_bluff_dealers_pope-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n277_bluff_dealers_pope-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n277_bluff_dealers_pope-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n277_bluff_dealers_pope-1-b" then 
	end
	if npc_talk_index == "n277_bluff_dealers_pope-1-c" then 
	end
	if npc_talk_index == "n277_bluff_dealers_pope-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n065_goblinson04_gosuk--------------------------------------------------------------------------------
function nq12_7218_new_worker2_OnTalk_n065_goblinson04_gosuk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n065_goblinson04_gosuk-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n065_goblinson04_gosuk-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n065_goblinson04_gosuk-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n065_goblinson04_gosuk-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n065_goblinson04_gosuk-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n065_goblinson04_gosuk-2-b" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-c" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-d" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-e" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300335, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300335, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 987, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200987, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300334, 1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n091_trader_bellin--------------------------------------------------------------------------------
function nq12_7218_new_worker2_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n091_trader_bellin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 72180, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 72180, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 72180, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 72180, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 72180, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 72180, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 72180, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 72180, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 72180, false);
			 end 

	end
	if npc_talk_index == "n091_trader_bellin-accepting-e" then
				api_quest_AddQuest(userObjID,7218, 1);
				api_quest_SetJournalStep(userObjID,7218, 1);
				api_quest_SetQuestStep(userObjID,7218, 1);

	end
	if npc_talk_index == "n091_trader_bellin-4-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-4-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 72180, true);
				 api_quest_RewardQuestUser(userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 72180, true);
				 api_quest_RewardQuestUser(userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 72180, true);
				 api_quest_RewardQuestUser(userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 72180, true);
				 api_quest_RewardQuestUser(userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 72180, true);
				 api_quest_RewardQuestUser(userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 72180, true);
				 api_quest_RewardQuestUser(userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 72180, true);
				 api_quest_RewardQuestUser(userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 72180, true);
				 api_quest_RewardQuestUser(userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 72180, true);
				 api_quest_RewardQuestUser(userObjID, 72180, questID, 1);
			 end 
	end
	if npc_talk_index == "n091_trader_bellin-4-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300334, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300334, api_quest_HasQuestItem(userObjID, 300334, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300335, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300335, api_quest_HasQuestItem(userObjID, 300335, 1));
				end
				api_npc_AddFavorPoint( userObjID, 91, 600 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq12_7218_new_worker2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7218);
	if qstep == 3 and CountIndex == 987 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300334, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300334, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 200987 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300334, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300334, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300334 then

	end
end

function nq12_7218_new_worker2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7218);
	if qstep == 3 and CountIndex == 987 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200987 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300334 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function nq12_7218_new_worker2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7218);
	local questID=7218;
end

function nq12_7218_new_worker2_OnRemoteStart( userObjID, questID )
end

function nq12_7218_new_worker2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq12_7218_new_worker2_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq12_7218_new_worker2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 277 then
		nq12_7218_new_worker2_OnTalk_n277_bluff_dealers_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 65 then
		nq12_7218_new_worker2_OnTalk_n065_goblinson04_gosuk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 91 then
		nq12_7218_new_worker2_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n277_bluff_dealers_pope--------------------------------------------------------------------------------
function nq12_7218_new_worker2_OnTalk_n277_bluff_dealers_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n277_bluff_dealers_pope-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n277_bluff_dealers_pope-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n277_bluff_dealers_pope-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n277_bluff_dealers_pope-1-b" then 
	end
	if npc_talk_index == "n277_bluff_dealers_pope-1-c" then 
	end
	if npc_talk_index == "n277_bluff_dealers_pope-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n065_goblinson04_gosuk--------------------------------------------------------------------------------
function nq12_7218_new_worker2_OnTalk_n065_goblinson04_gosuk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n065_goblinson04_gosuk-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n065_goblinson04_gosuk-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n065_goblinson04_gosuk-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n065_goblinson04_gosuk-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n065_goblinson04_gosuk-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n065_goblinson04_gosuk-2-b" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-c" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-d" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-e" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300335, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300335, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 987, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200987, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300334, 1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n091_trader_bellin--------------------------------------------------------------------------------
function nq12_7218_new_worker2_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n091_trader_bellin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, false);
			 end 

	end
	if npc_talk_index == "n091_trader_bellin-accepting-e" then
				api_quest_AddQuest( pRoom, userObjID,7218, 1);
				api_quest_SetJournalStep( pRoom, userObjID,7218, 1);
				api_quest_SetQuestStep( pRoom, userObjID,7218, 1);

	end
	if npc_talk_index == "n091_trader_bellin-4-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-4-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72180, questID, 1);
			 end 
	end
	if npc_talk_index == "n091_trader_bellin-4-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300334, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300334, api_quest_HasQuestItem( pRoom, userObjID, 300334, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300335, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300335, api_quest_HasQuestItem( pRoom, userObjID, 300335, 1));
				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 91, 600 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq12_7218_new_worker2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7218);
	if qstep == 3 and CountIndex == 987 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300334, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300334, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 200987 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300334, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300334, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300334 then

	end
end

function nq12_7218_new_worker2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7218);
	if qstep == 3 and CountIndex == 987 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200987 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300334 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function nq12_7218_new_worker2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7218);
	local questID=7218;
end

function nq12_7218_new_worker2_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq12_7218_new_worker2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq12_7218_new_worker2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>