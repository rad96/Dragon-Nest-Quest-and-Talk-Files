<VillageServer>

function mq15_9388_truth_which_has_regret_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1038 then
		mq15_9388_truth_which_has_regret_OnTalk_n1038_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1204 then
		mq15_9388_truth_which_has_regret_OnTalk_n1204_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1205 then
		mq15_9388_truth_which_has_regret_OnTalk_n1205_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 574 then
		mq15_9388_truth_which_has_regret_OnTalk_n574_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq15_9388_truth_which_has_regret_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1038_academic--------------------------------------------------------------------------------
function mq15_9388_truth_which_has_regret_OnTalk_n1038_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1038_academic-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1038_academic-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1038_academic-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1038_academic-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1038_academic-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1038_academic-1-b" then 
	end
	if npc_talk_index == "n1038_academic-1-c" then 
	end
	if npc_talk_index == "n1038_academic-1-d" then 
	end
	if npc_talk_index == "n1038_academic-1-e" then 
	end
	if npc_talk_index == "n1038_academic-1-f" then 
	end
	if npc_talk_index == "n1038_academic-1-g" then 
	end
	if npc_talk_index == "n1038_academic-1-h" then 
	end
	if npc_talk_index == "n1038_academic-1-i" then 
	end
	if npc_talk_index == "n1038_academic-1-j" then 
	end
	if npc_talk_index == "n1038_academic-1-k" then 
	end
	if npc_talk_index == "n1038_academic-1-l" then 
	end
	if npc_talk_index == "n1038_academic-2" then 
	end
	if npc_talk_index == "n1038_academic-2" then 
	end
	if npc_talk_index == "n1038_academic-2" then 
	end
	if npc_talk_index == "n1038_academic-2-b" then 
	end
	if npc_talk_index == "n1038_academic-2-c" then 
	end
	if npc_talk_index == "n1038_academic-2-d" then 
	end
	if npc_talk_index == "n1038_academic-2-e" then 
	end
	if npc_talk_index == "n1038_academic-2-f" then 
	end
	if npc_talk_index == "n1038_academic-2-g" then 
	end
	if npc_talk_index == "n1038_academic-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n1038_academic-4-b" then 
	end
	if npc_talk_index == "n1038_academic-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1038_academic-5-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1204_lunaria--------------------------------------------------------------------------------
function mq15_9388_truth_which_has_regret_OnTalk_n1204_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1204_lunaria-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1204_lunaria-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1204_lunaria-6-b" then 
	end
	if npc_talk_index == "n1204_lunaria-6-c" then 
	end
	if npc_talk_index == "n1204_lunaria-6-d" then 
	end
	if npc_talk_index == "n1204_lunaria-6-e" then 
	end
	if npc_talk_index == "n1204_lunaria-6-f" then 
	end
	if npc_talk_index == "n1204_lunaria-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1205_lunaria--------------------------------------------------------------------------------
function mq15_9388_truth_which_has_regret_OnTalk_n1205_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1205_lunaria-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1205_lunaria-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1205_lunaria-7-b" then 
	end
	if npc_talk_index == "n1205_lunaria-7-c" then 
	end
	if npc_talk_index == "n1205_lunaria-7-d" then 
	end
	if npc_talk_index == "n1205_lunaria-7-e" then 
	end
	if npc_talk_index == "n1205_lunaria-7-f" then 
	end
	if npc_talk_index == "n1205_lunaria-7-g" then 
	end
	if npc_talk_index == "n1205_lunaria-7-h" then 
	end
	if npc_talk_index == "n1205_lunaria-7-i" then 
	end
	if npc_talk_index == "n1205_lunaria-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n574_shadow_meow--------------------------------------------------------------------------------
function mq15_9388_truth_which_has_regret_OnTalk_n574_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n574_shadow_meow-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n574_shadow_meow-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n574_shadow_meow-8-b" then 
	end
	if npc_talk_index == "n574_shadow_meow-8-c" then 
	end
	if npc_talk_index == "n574_shadow_meow-8-d" then 
	end
	if npc_talk_index == "n574_shadow_meow-8-e" then 
	end
	if npc_talk_index == "n574_shadow_meow-9" then 
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq15_9388_truth_which_has_regret_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n822_teramai-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9388, 2);
				api_quest_SetJournalStep(userObjID,9388, 1);
				api_quest_SetQuestStep(userObjID,9388, 1);
				npc_talk_index = "n822_teramai-1";

	end
	if npc_talk_index == "n822_teramai-9-b" then 
	end
	if npc_talk_index == "n822_teramai-9-c" then 
	end
	if npc_talk_index == "n822_teramai-9-d" then 
	end
	if npc_talk_index == "n822_teramai-9-e" then 
	end
	if npc_talk_index == "n822_teramai-9-f" then 
	end
	if npc_talk_index == "n822_teramai-9-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93880, true);
				 api_quest_RewardQuestUser(userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93880, true);
				 api_quest_RewardQuestUser(userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93880, true);
				 api_quest_RewardQuestUser(userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93880, true);
				 api_quest_RewardQuestUser(userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93880, true);
				 api_quest_RewardQuestUser(userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93880, true);
				 api_quest_RewardQuestUser(userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93880, true);
				 api_quest_RewardQuestUser(userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93880, true);
				 api_quest_RewardQuestUser(userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93880, true);
				 api_quest_RewardQuestUser(userObjID, 93880, questID, 1);
			 end 
	end
	if npc_talk_index == "n822_teramai-9-h" then 
				api_quest_SetJournalStep(userObjID, questID, 9);

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9388_truth_which_has_regret_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9388);
end

function mq15_9388_truth_which_has_regret_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9388);
end

function mq15_9388_truth_which_has_regret_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9388);
	local questID=9388;
end

function mq15_9388_truth_which_has_regret_OnRemoteStart( userObjID, questID )
end

function mq15_9388_truth_which_has_regret_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9388_truth_which_has_regret_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9388, 2);
				api_quest_SetJournalStep(userObjID,9388, 1);
				api_quest_SetQuestStep(userObjID,9388, 1);
				npc_talk_index = "n822_teramai-1";
end

</VillageServer>

<GameServer>
function mq15_9388_truth_which_has_regret_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1038 then
		mq15_9388_truth_which_has_regret_OnTalk_n1038_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1204 then
		mq15_9388_truth_which_has_regret_OnTalk_n1204_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1205 then
		mq15_9388_truth_which_has_regret_OnTalk_n1205_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 574 then
		mq15_9388_truth_which_has_regret_OnTalk_n574_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq15_9388_truth_which_has_regret_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1038_academic--------------------------------------------------------------------------------
function mq15_9388_truth_which_has_regret_OnTalk_n1038_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1038_academic-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1038_academic-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1038_academic-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1038_academic-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1038_academic-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1038_academic-1-b" then 
	end
	if npc_talk_index == "n1038_academic-1-c" then 
	end
	if npc_talk_index == "n1038_academic-1-d" then 
	end
	if npc_talk_index == "n1038_academic-1-e" then 
	end
	if npc_talk_index == "n1038_academic-1-f" then 
	end
	if npc_talk_index == "n1038_academic-1-g" then 
	end
	if npc_talk_index == "n1038_academic-1-h" then 
	end
	if npc_talk_index == "n1038_academic-1-i" then 
	end
	if npc_talk_index == "n1038_academic-1-j" then 
	end
	if npc_talk_index == "n1038_academic-1-k" then 
	end
	if npc_talk_index == "n1038_academic-1-l" then 
	end
	if npc_talk_index == "n1038_academic-2" then 
	end
	if npc_talk_index == "n1038_academic-2" then 
	end
	if npc_talk_index == "n1038_academic-2" then 
	end
	if npc_talk_index == "n1038_academic-2-b" then 
	end
	if npc_talk_index == "n1038_academic-2-c" then 
	end
	if npc_talk_index == "n1038_academic-2-d" then 
	end
	if npc_talk_index == "n1038_academic-2-e" then 
	end
	if npc_talk_index == "n1038_academic-2-f" then 
	end
	if npc_talk_index == "n1038_academic-2-g" then 
	end
	if npc_talk_index == "n1038_academic-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n1038_academic-4-b" then 
	end
	if npc_talk_index == "n1038_academic-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1038_academic-5-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1204_lunaria--------------------------------------------------------------------------------
function mq15_9388_truth_which_has_regret_OnTalk_n1204_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1204_lunaria-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1204_lunaria-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1204_lunaria-6-b" then 
	end
	if npc_talk_index == "n1204_lunaria-6-c" then 
	end
	if npc_talk_index == "n1204_lunaria-6-d" then 
	end
	if npc_talk_index == "n1204_lunaria-6-e" then 
	end
	if npc_talk_index == "n1204_lunaria-6-f" then 
	end
	if npc_talk_index == "n1204_lunaria-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1205_lunaria--------------------------------------------------------------------------------
function mq15_9388_truth_which_has_regret_OnTalk_n1205_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1205_lunaria-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1205_lunaria-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1205_lunaria-7-b" then 
	end
	if npc_talk_index == "n1205_lunaria-7-c" then 
	end
	if npc_talk_index == "n1205_lunaria-7-d" then 
	end
	if npc_talk_index == "n1205_lunaria-7-e" then 
	end
	if npc_talk_index == "n1205_lunaria-7-f" then 
	end
	if npc_talk_index == "n1205_lunaria-7-g" then 
	end
	if npc_talk_index == "n1205_lunaria-7-h" then 
	end
	if npc_talk_index == "n1205_lunaria-7-i" then 
	end
	if npc_talk_index == "n1205_lunaria-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n574_shadow_meow--------------------------------------------------------------------------------
function mq15_9388_truth_which_has_regret_OnTalk_n574_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n574_shadow_meow-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n574_shadow_meow-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n574_shadow_meow-8-b" then 
	end
	if npc_talk_index == "n574_shadow_meow-8-c" then 
	end
	if npc_talk_index == "n574_shadow_meow-8-d" then 
	end
	if npc_talk_index == "n574_shadow_meow-8-e" then 
	end
	if npc_talk_index == "n574_shadow_meow-9" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq15_9388_truth_which_has_regret_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n822_teramai-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9388, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9388, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9388, 1);
				npc_talk_index = "n822_teramai-1";

	end
	if npc_talk_index == "n822_teramai-9-b" then 
	end
	if npc_talk_index == "n822_teramai-9-c" then 
	end
	if npc_talk_index == "n822_teramai-9-d" then 
	end
	if npc_talk_index == "n822_teramai-9-e" then 
	end
	if npc_talk_index == "n822_teramai-9-f" then 
	end
	if npc_talk_index == "n822_teramai-9-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93880, questID, 1);
			 end 
	end
	if npc_talk_index == "n822_teramai-9-h" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 9);

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9388_truth_which_has_regret_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9388);
end

function mq15_9388_truth_which_has_regret_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9388);
end

function mq15_9388_truth_which_has_regret_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9388);
	local questID=9388;
end

function mq15_9388_truth_which_has_regret_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9388_truth_which_has_regret_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9388_truth_which_has_regret_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9388, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9388, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9388, 1);
				npc_talk_index = "n822_teramai-1";
end

</GameServer>