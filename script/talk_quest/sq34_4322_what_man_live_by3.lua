<VillageServer>

function sq34_4322_what_man_live_by3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1109 then
		sq34_4322_what_man_live_by3_OnTalk_n1109_shadowelf_risione(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1138 then
		sq34_4322_what_man_live_by3_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 359 then
		sq34_4322_what_man_live_by3_OnTalk_n359_trader_merlin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1109_shadowelf_risione--------------------------------------------------------------------------------
function sq34_4322_what_man_live_by3_OnTalk_n1109_shadowelf_risione(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1109_shadowelf_risione-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1109_shadowelf_risione-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1109_shadowelf_risione-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1109_shadowelf_risione-2-b" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-c" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-d" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-d" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-e" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-f" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-f" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-g" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-h" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function sq34_4322_what_man_live_by3_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1138_elf_guard_sitredel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1138_elf_guard_sitredel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-3-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43220, true);
				 api_quest_RewardQuestUser(userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43220, true);
				 api_quest_RewardQuestUser(userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43220, true);
				 api_quest_RewardQuestUser(userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43220, true);
				 api_quest_RewardQuestUser(userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43220, true);
				 api_quest_RewardQuestUser(userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43220, true);
				 api_quest_RewardQuestUser(userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43220, true);
				 api_quest_RewardQuestUser(userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43220, true);
				 api_quest_RewardQuestUser(userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43220, true);
				 api_quest_RewardQuestUser(userObjID, 43220, questID, 1);
			 end 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n359_trader_merlin--------------------------------------------------------------------------------
function sq34_4322_what_man_live_by3_OnTalk_n359_trader_merlin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n359_trader_merlin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n359_trader_merlin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n359_trader_merlin-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43220, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43220, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43220, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43220, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43220, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43220, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43220, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43220, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43220, false);
			 end 

	end
	if npc_talk_index == "n359_trader_merlin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4322, 1);
				api_quest_SetJournalStep(userObjID,4322, 1);
				api_quest_SetQuestStep(userObjID,4322, 1);
				npc_talk_index = "n359_trader_merlin-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 701401, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 601401, 1);

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4322_what_man_live_by3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4322);
	if qstep == 1 and CountIndex == 701401 then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 601401 then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function sq34_4322_what_man_live_by3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4322);
	if qstep == 1 and CountIndex == 701401 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 601401 and Count >= TargetCount  then

	end
end

function sq34_4322_what_man_live_by3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4322);
	local questID=4322;
end

function sq34_4322_what_man_live_by3_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 701401, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 601401, 1);
end

function sq34_4322_what_man_live_by3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq34_4322_what_man_live_by3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4322, 1);
				api_quest_SetJournalStep(userObjID,4322, 1);
				api_quest_SetQuestStep(userObjID,4322, 1);
				npc_talk_index = "n359_trader_merlin-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 701401, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 601401, 1);
end

</VillageServer>

<GameServer>
function sq34_4322_what_man_live_by3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1109 then
		sq34_4322_what_man_live_by3_OnTalk_n1109_shadowelf_risione( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1138 then
		sq34_4322_what_man_live_by3_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 359 then
		sq34_4322_what_man_live_by3_OnTalk_n359_trader_merlin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1109_shadowelf_risione--------------------------------------------------------------------------------
function sq34_4322_what_man_live_by3_OnTalk_n1109_shadowelf_risione( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1109_shadowelf_risione-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1109_shadowelf_risione-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1109_shadowelf_risione-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1109_shadowelf_risione-2-b" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-c" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-d" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-d" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-e" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-f" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-f" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-g" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-2-h" then 
	end
	if npc_talk_index == "n1109_shadowelf_risione-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function sq34_4322_what_man_live_by3_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1138_elf_guard_sitredel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1138_elf_guard_sitredel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-3-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43220, questID, 1);
			 end 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n359_trader_merlin--------------------------------------------------------------------------------
function sq34_4322_what_man_live_by3_OnTalk_n359_trader_merlin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n359_trader_merlin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n359_trader_merlin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n359_trader_merlin-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43220, false);
			 end 

	end
	if npc_talk_index == "n359_trader_merlin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4322, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4322, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4322, 1);
				npc_talk_index = "n359_trader_merlin-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 701401, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 601401, 1);

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4322_what_man_live_by3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4322);
	if qstep == 1 and CountIndex == 701401 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 601401 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function sq34_4322_what_man_live_by3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4322);
	if qstep == 1 and CountIndex == 701401 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 601401 and Count >= TargetCount  then

	end
end

function sq34_4322_what_man_live_by3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4322);
	local questID=4322;
end

function sq34_4322_what_man_live_by3_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 701401, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 601401, 1);
end

function sq34_4322_what_man_live_by3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq34_4322_what_man_live_by3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4322, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4322, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4322, 1);
				npc_talk_index = "n359_trader_merlin-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 701401, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 601401, 1);
end

</GameServer>