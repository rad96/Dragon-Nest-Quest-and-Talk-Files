<VillageServer>

function mqc03_9585_the_purpose_of_bao_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1854 then
		mqc03_9585_the_purpose_of_bao_OnTalk_n1854_darkelf_elena(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1856 then
		mqc03_9585_the_purpose_of_bao_OnTalk_n1856_suriya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1855 then
		mqc03_9585_the_purpose_of_bao_OnTalk_n1855_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mqc03_9585_the_purpose_of_bao_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1854_darkelf_elena--------------------------------------------------------------------------------
function mqc03_9585_the_purpose_of_bao_OnTalk_n1854_darkelf_elena(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1854_darkelf_elena-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1854_darkelf_elena-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1854_darkelf_elena-1-b" then 
	end
	if npc_talk_index == "n1854_darkelf_elena-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400480, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 510, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200510, 30000);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1856_suriya--------------------------------------------------------------------------------
function mqc03_9585_the_purpose_of_bao_OnTalk_n1856_suriya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1856_suriya-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1856_suriya-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1856_suriya-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1856_suriya-3-b" then 
	end
	if npc_talk_index == "n1856_suriya-3-c" then 
	end
	if npc_talk_index == "n1856_suriya-3-d" then 
	end
	if npc_talk_index == "n1856_suriya-3-e" then 
	end
	if npc_talk_index == "n1856_suriya-3-f" then 
	end
	if npc_talk_index == "n1856_suriya-3-g" then 
	end
	if npc_talk_index == "n1856_suriya-3-h" then 
	end
	if npc_talk_index == "n1856_suriya-3-i" then 
	end
	if npc_talk_index == "n1856_suriya-3-j" then 
	end
	if npc_talk_index == "n1856_suriya-3-k" then 
	end
	if npc_talk_index == "n1856_suriya-3-l" then 
	end
	if npc_talk_index == "n1856_suriya-3-m" then 
	end
	if npc_talk_index == "n1856_suriya-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1855_shaolong--------------------------------------------------------------------------------
function mqc03_9585_the_purpose_of_bao_OnTalk_n1855_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1855_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1855_shaolong-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1855_shaolong-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95850, true);
				 api_quest_RewardQuestUser(userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95850, true);
				 api_quest_RewardQuestUser(userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95850, true);
				 api_quest_RewardQuestUser(userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95850, true);
				 api_quest_RewardQuestUser(userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95850, true);
				 api_quest_RewardQuestUser(userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95850, true);
				 api_quest_RewardQuestUser(userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95850, true);
				 api_quest_RewardQuestUser(userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95850, true);
				 api_quest_RewardQuestUser(userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95850, true);
				 api_quest_RewardQuestUser(userObjID, 95850, questID, 1);
			 end 
	end
	if npc_talk_index == "n1855_shaolong-4-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9586, 2);
					api_quest_SetQuestStep(userObjID, 9586, 1);
					api_quest_SetJournalStep(userObjID, 9586, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1855_shaolong-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1855_shaolong-1", "mqc03_9586_wounded_elena.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mqc03_9585_the_purpose_of_bao_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9585, 2);
				api_quest_SetJournalStep(userObjID,9585, 1);
				api_quest_SetQuestStep(userObjID,9585, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc03_9585_the_purpose_of_bao_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9585);
	if qstep == 2 and CountIndex == 400480 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 510 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400480, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400480, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200510 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400480, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400480, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mqc03_9585_the_purpose_of_bao_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9585);
	if qstep == 2 and CountIndex == 400480 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 510 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200510 and Count >= TargetCount  then

	end
end

function mqc03_9585_the_purpose_of_bao_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9585);
	local questID=9585;
end

function mqc03_9585_the_purpose_of_bao_OnRemoteStart( userObjID, questID )
end

function mqc03_9585_the_purpose_of_bao_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc03_9585_the_purpose_of_bao_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9585, 2);
				api_quest_SetJournalStep(userObjID,9585, 1);
				api_quest_SetQuestStep(userObjID,9585, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
end

</VillageServer>

<GameServer>
function mqc03_9585_the_purpose_of_bao_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1854 then
		mqc03_9585_the_purpose_of_bao_OnTalk_n1854_darkelf_elena( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1856 then
		mqc03_9585_the_purpose_of_bao_OnTalk_n1856_suriya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1855 then
		mqc03_9585_the_purpose_of_bao_OnTalk_n1855_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mqc03_9585_the_purpose_of_bao_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1854_darkelf_elena--------------------------------------------------------------------------------
function mqc03_9585_the_purpose_of_bao_OnTalk_n1854_darkelf_elena( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1854_darkelf_elena-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1854_darkelf_elena-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1854_darkelf_elena-1-b" then 
	end
	if npc_talk_index == "n1854_darkelf_elena-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400480, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 510, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200510, 30000);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1856_suriya--------------------------------------------------------------------------------
function mqc03_9585_the_purpose_of_bao_OnTalk_n1856_suriya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1856_suriya-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1856_suriya-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1856_suriya-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1856_suriya-3-b" then 
	end
	if npc_talk_index == "n1856_suriya-3-c" then 
	end
	if npc_talk_index == "n1856_suriya-3-d" then 
	end
	if npc_talk_index == "n1856_suriya-3-e" then 
	end
	if npc_talk_index == "n1856_suriya-3-f" then 
	end
	if npc_talk_index == "n1856_suriya-3-g" then 
	end
	if npc_talk_index == "n1856_suriya-3-h" then 
	end
	if npc_talk_index == "n1856_suriya-3-i" then 
	end
	if npc_talk_index == "n1856_suriya-3-j" then 
	end
	if npc_talk_index == "n1856_suriya-3-k" then 
	end
	if npc_talk_index == "n1856_suriya-3-l" then 
	end
	if npc_talk_index == "n1856_suriya-3-m" then 
	end
	if npc_talk_index == "n1856_suriya-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1855_shaolong--------------------------------------------------------------------------------
function mqc03_9585_the_purpose_of_bao_OnTalk_n1855_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1855_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1855_shaolong-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1855_shaolong-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95850, questID, 1);
			 end 
	end
	if npc_talk_index == "n1855_shaolong-4-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9586, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9586, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9586, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1855_shaolong-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1855_shaolong-1", "mqc03_9586_wounded_elena.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mqc03_9585_the_purpose_of_bao_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9585, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9585, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9585, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc03_9585_the_purpose_of_bao_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9585);
	if qstep == 2 and CountIndex == 400480 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 510 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400480, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400480, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200510 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400480, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400480, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mqc03_9585_the_purpose_of_bao_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9585);
	if qstep == 2 and CountIndex == 400480 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 510 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200510 and Count >= TargetCount  then

	end
end

function mqc03_9585_the_purpose_of_bao_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9585);
	local questID=9585;
end

function mqc03_9585_the_purpose_of_bao_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc03_9585_the_purpose_of_bao_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc03_9585_the_purpose_of_bao_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9585, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9585, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9585, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
end

</GameServer>