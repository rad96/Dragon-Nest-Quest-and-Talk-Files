<VillageServer>

function mq01_033_groping_in_the_dark_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1 then
		mq01_033_groping_in_the_dark_OnTalk_n001_elder_harold(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1092 then
		mq01_033_groping_in_the_dark_OnTalk_n1092_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1094 then
		mq01_033_groping_in_the_dark_OnTalk_n1094_rose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1095 then
		mq01_033_groping_in_the_dark_OnTalk_n1095_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n001_elder_harold--------------------------------------------------------------------------------
function mq01_033_groping_in_the_dark_OnTalk_n001_elder_harold(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n001_elder_harold-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n001_elder_harold-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n001_elder_harold-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n001_elder_harold-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n001_elder_harold-accepting-acceptted" then
				api_quest_AddQuest(userObjID,33, 2);
				api_quest_SetJournalStep(userObjID,33, 1);
				api_quest_SetQuestStep(userObjID,33, 1);
				npc_talk_index = "n001_elder_harold-1";

	end
	if npc_talk_index == "n001_elder_harold-1-b" then 
	end
	if npc_talk_index == "n001_elder_harold-1-c" then 
	end
	if npc_talk_index == "n001_elder_harold-1-d" then 
	end
	if npc_talk_index == "n001_elder_harold-1-e" then 
	end
	if npc_talk_index == "n001_elder_harold-1-e" then 
	end
	if npc_talk_index == "n001_elder_harold-1-f" then 
	end
	if npc_talk_index == "n001_elder_harold-1-g" then 
	end
	if npc_talk_index == "n001_elder_harold-1-h" then 
	end
	if npc_talk_index == "n001_elder_harold-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n001_elder_harold-5-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 330, true);
				 api_quest_RewardQuestUser(userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 330, true);
				 api_quest_RewardQuestUser(userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 330, true);
				 api_quest_RewardQuestUser(userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 330, true);
				 api_quest_RewardQuestUser(userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 330, true);
				 api_quest_RewardQuestUser(userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 330, true);
				 api_quest_RewardQuestUser(userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 330, true);
				 api_quest_RewardQuestUser(userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 330, true);
				 api_quest_RewardQuestUser(userObjID, 330, questID, 1);
			 end 
	end
	if npc_talk_index == "n001_elder_harold-5-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 34, 2);
					api_quest_SetQuestStep(userObjID, 34, 1);
					api_quest_SetJournalStep(userObjID, 34, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n001_elder_harold-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n001_elder_harold-1", "mq01_034_gone_memory.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1092_lunaria--------------------------------------------------------------------------------
function mq01_033_groping_in_the_dark_OnTalk_n1092_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1092_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1092_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1092_lunaria-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1092_lunaria-2-b" then 
	end
	if npc_talk_index == "n1092_lunaria-2-c" then 
	end
	if npc_talk_index == "n1092_lunaria-2-d" then 
	end
	if npc_talk_index == "n1092_lunaria-2-e" then 
	end
	if npc_talk_index == "n1092_lunaria-2-f" then 
	end
	if npc_talk_index == "n1092_lunaria-2-g" then 
	end
	if npc_talk_index == "n1092_lunaria-2-h" then 
	end
	if npc_talk_index == "n1092_lunaria-2-i" then 
	end
	if npc_talk_index == "n1092_lunaria-2-j" then 
	end
	if npc_talk_index == "n1092_lunaria-2-k" then 
	end
	if npc_talk_index == "n1092_lunaria-2-l" then 
	end
	if npc_talk_index == "n1092_lunaria-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1094_rose--------------------------------------------------------------------------------
function mq01_033_groping_in_the_dark_OnTalk_n1094_rose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1094_rose-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1094_rose-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1094_rose-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1094_rose-3-b" then 
	end
	if npc_talk_index == "n1094_rose-3-c" then 
	end
	if npc_talk_index == "n1094_rose-3-d" then 
	end
	if npc_talk_index == "n1094_rose-3-e" then 
	end
	if npc_talk_index == "n1094_rose-3-f" then 
	end
	if npc_talk_index == "n1094_rose-3-g" then 
	end
	if npc_talk_index == "n1094_rose-3-h" then 
	end
	if npc_talk_index == "n1094_rose-3-i" then 
	end
	if npc_talk_index == "n1094_rose-3-j" then 
	end
	if npc_talk_index == "n1094_rose-3-k" then 
	end
	if npc_talk_index == "n1094_rose-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1095_lunaria--------------------------------------------------------------------------------
function mq01_033_groping_in_the_dark_OnTalk_n1095_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1095_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1095_lunaria-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1095_lunaria-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1095_lunaria-4-b" then 
	end
	if npc_talk_index == "n1095_lunaria-4-c" then 
	end
	if npc_talk_index == "n1095_lunaria-4-d" then 
	end
	if npc_talk_index == "n1095_lunaria-4-e" then 
	end
	if npc_talk_index == "n1095_lunaria-4-f" then 
	end
	if npc_talk_index == "n1095_lunaria-4-g" then 
	end
	if npc_talk_index == "n1095_lunaria-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq01_033_groping_in_the_dark_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 33);
end

function mq01_033_groping_in_the_dark_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 33);
end

function mq01_033_groping_in_the_dark_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 33);
	local questID=33;
end

function mq01_033_groping_in_the_dark_OnRemoteStart( userObjID, questID )
end

function mq01_033_groping_in_the_dark_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq01_033_groping_in_the_dark_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,33, 2);
				api_quest_SetJournalStep(userObjID,33, 1);
				api_quest_SetQuestStep(userObjID,33, 1);
				npc_talk_index = "n001_elder_harold-1";
end

</VillageServer>

<GameServer>
function mq01_033_groping_in_the_dark_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1 then
		mq01_033_groping_in_the_dark_OnTalk_n001_elder_harold( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1092 then
		mq01_033_groping_in_the_dark_OnTalk_n1092_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1094 then
		mq01_033_groping_in_the_dark_OnTalk_n1094_rose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1095 then
		mq01_033_groping_in_the_dark_OnTalk_n1095_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n001_elder_harold--------------------------------------------------------------------------------
function mq01_033_groping_in_the_dark_OnTalk_n001_elder_harold( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n001_elder_harold-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n001_elder_harold-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n001_elder_harold-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n001_elder_harold-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n001_elder_harold-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,33, 2);
				api_quest_SetJournalStep( pRoom, userObjID,33, 1);
				api_quest_SetQuestStep( pRoom, userObjID,33, 1);
				npc_talk_index = "n001_elder_harold-1";

	end
	if npc_talk_index == "n001_elder_harold-1-b" then 
	end
	if npc_talk_index == "n001_elder_harold-1-c" then 
	end
	if npc_talk_index == "n001_elder_harold-1-d" then 
	end
	if npc_talk_index == "n001_elder_harold-1-e" then 
	end
	if npc_talk_index == "n001_elder_harold-1-e" then 
	end
	if npc_talk_index == "n001_elder_harold-1-f" then 
	end
	if npc_talk_index == "n001_elder_harold-1-g" then 
	end
	if npc_talk_index == "n001_elder_harold-1-h" then 
	end
	if npc_talk_index == "n001_elder_harold-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n001_elder_harold-5-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 330, questID, 1);
			 end 
	end
	if npc_talk_index == "n001_elder_harold-5-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 34, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 34, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 34, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n001_elder_harold-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n001_elder_harold-1", "mq01_034_gone_memory.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1092_lunaria--------------------------------------------------------------------------------
function mq01_033_groping_in_the_dark_OnTalk_n1092_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1092_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1092_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1092_lunaria-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1092_lunaria-2-b" then 
	end
	if npc_talk_index == "n1092_lunaria-2-c" then 
	end
	if npc_talk_index == "n1092_lunaria-2-d" then 
	end
	if npc_talk_index == "n1092_lunaria-2-e" then 
	end
	if npc_talk_index == "n1092_lunaria-2-f" then 
	end
	if npc_talk_index == "n1092_lunaria-2-g" then 
	end
	if npc_talk_index == "n1092_lunaria-2-h" then 
	end
	if npc_talk_index == "n1092_lunaria-2-i" then 
	end
	if npc_talk_index == "n1092_lunaria-2-j" then 
	end
	if npc_talk_index == "n1092_lunaria-2-k" then 
	end
	if npc_talk_index == "n1092_lunaria-2-l" then 
	end
	if npc_talk_index == "n1092_lunaria-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1094_rose--------------------------------------------------------------------------------
function mq01_033_groping_in_the_dark_OnTalk_n1094_rose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1094_rose-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1094_rose-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1094_rose-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1094_rose-3-b" then 
	end
	if npc_talk_index == "n1094_rose-3-c" then 
	end
	if npc_talk_index == "n1094_rose-3-d" then 
	end
	if npc_talk_index == "n1094_rose-3-e" then 
	end
	if npc_talk_index == "n1094_rose-3-f" then 
	end
	if npc_talk_index == "n1094_rose-3-g" then 
	end
	if npc_talk_index == "n1094_rose-3-h" then 
	end
	if npc_talk_index == "n1094_rose-3-i" then 
	end
	if npc_talk_index == "n1094_rose-3-j" then 
	end
	if npc_talk_index == "n1094_rose-3-k" then 
	end
	if npc_talk_index == "n1094_rose-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1095_lunaria--------------------------------------------------------------------------------
function mq01_033_groping_in_the_dark_OnTalk_n1095_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1095_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1095_lunaria-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1095_lunaria-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1095_lunaria-4-b" then 
	end
	if npc_talk_index == "n1095_lunaria-4-c" then 
	end
	if npc_talk_index == "n1095_lunaria-4-d" then 
	end
	if npc_talk_index == "n1095_lunaria-4-e" then 
	end
	if npc_talk_index == "n1095_lunaria-4-f" then 
	end
	if npc_talk_index == "n1095_lunaria-4-g" then 
	end
	if npc_talk_index == "n1095_lunaria-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq01_033_groping_in_the_dark_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 33);
end

function mq01_033_groping_in_the_dark_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 33);
end

function mq01_033_groping_in_the_dark_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 33);
	local questID=33;
end

function mq01_033_groping_in_the_dark_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq01_033_groping_in_the_dark_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq01_033_groping_in_the_dark_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,33, 2);
				api_quest_SetJournalStep( pRoom, userObjID,33, 1);
				api_quest_SetQuestStep( pRoom, userObjID,33, 1);
				npc_talk_index = "n001_elder_harold-1";
end

</GameServer>