<VillageServer>

function sqc17_4721_memories_of_mirage3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1836 then
		sqc17_4721_memories_of_mirage3_OnTalk_n1836_sarah(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1871 then
		sqc17_4721_memories_of_mirage3_OnTalk_n1871_trader_april(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1836_sarah--------------------------------------------------------------------------------
function sqc17_4721_memories_of_mirage3_OnTalk_n1836_sarah(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1836_sarah-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1836_sarah-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1836_sarah-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1836_sarah-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47210, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47210, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47210, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47210, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47210, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47210, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47210, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47210, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47210, false);
			 end 

	end
	if npc_talk_index == "n1836_sarah-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4721, 1);
				api_quest_SetJournalStep(userObjID,4721, 1);
				api_quest_SetQuestStep(userObjID,4721, 1);
				npc_talk_index = "n1836_sarah-1";

	end
	if npc_talk_index == "n1836_sarah-1-b" then 
	end
	if npc_talk_index == "n1836_sarah-1-c" then 
	end
	if npc_talk_index == "n1836_sarah-1-d" then 
	end
	if npc_talk_index == "n1836_sarah-1-e" then 
	end
	if npc_talk_index == "n1836_sarah-1-f" then 
	end
	if npc_talk_index == "n1836_sarah-1-g" then 
	end
	if npc_talk_index == "n1836_sarah-1-h" then 
	end
	if npc_talk_index == "n1836_sarah-1-i" then 
	end
	if npc_talk_index == "n1836_sarah-1-j" then 
	end
	if npc_talk_index == "n1836_sarah-1-k" then 
	end
	if npc_talk_index == "n1836_sarah-1-l" then 
	end
	if npc_talk_index == "n1836_sarah-1-m" then 
	end
	if npc_talk_index == "n1836_sarah-1-n" then 
	end
	if npc_talk_index == "n1836_sarah-1-o" then 
	end
	if npc_talk_index == "n1836_sarah-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1871_trader_april--------------------------------------------------------------------------------
function sqc17_4721_memories_of_mirage3_OnTalk_n1871_trader_april(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1871_trader_april-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1871_trader_april-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1871_trader_april-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1871_trader_april-2-b" then 
	end
	if npc_talk_index == "n1871_trader_april-2-c" then 
	end
	if npc_talk_index == "n1871_trader_april-2-d" then 
	end
	if npc_talk_index == "n1871_trader_april-2-e" then 
	end
	if npc_talk_index == "n1871_trader_april-2-f" then 
	end
	if npc_talk_index == "n1871_trader_april-2-g" then 
	end
	if npc_talk_index == "n1871_trader_april-2-h" then 
	end
	if npc_talk_index == "n1871_trader_april-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 1094, 1 );
	end
	if npc_talk_index == "n1871_trader_april-4-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47210, true);
				 api_quest_RewardQuestUser(userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47210, true);
				 api_quest_RewardQuestUser(userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47210, true);
				 api_quest_RewardQuestUser(userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47210, true);
				 api_quest_RewardQuestUser(userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47210, true);
				 api_quest_RewardQuestUser(userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47210, true);
				 api_quest_RewardQuestUser(userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47210, true);
				 api_quest_RewardQuestUser(userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47210, true);
				 api_quest_RewardQuestUser(userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47210, true);
				 api_quest_RewardQuestUser(userObjID, 47210, questID, 1);
			 end 
	end
	if npc_talk_index == "n1871_trader_april-4-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4722, 1);
					api_quest_SetQuestStep(userObjID, 4722, 1);
					api_quest_SetJournalStep(userObjID, 4722, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1871_trader_april-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1871_trader_april-1", "sqc17_4722_memories_of_mirage4.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4721_memories_of_mirage3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4721);
	 local qstep= 2; 
	 if qstep == 2 then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				if  api_user_GetStageConstructionLevel( userObjID ) >= 0 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

				else
				end
		 end 
	 end 
end

function sqc17_4721_memories_of_mirage3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4721);
end

function sqc17_4721_memories_of_mirage3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4721);
	local questID=4721;
end

function sqc17_4721_memories_of_mirage3_OnRemoteStart( userObjID, questID )
end

function sqc17_4721_memories_of_mirage3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc17_4721_memories_of_mirage3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4721, 1);
				api_quest_SetJournalStep(userObjID,4721, 1);
				api_quest_SetQuestStep(userObjID,4721, 1);
				npc_talk_index = "n1836_sarah-1";
end

</VillageServer>

<GameServer>
function sqc17_4721_memories_of_mirage3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1836 then
		sqc17_4721_memories_of_mirage3_OnTalk_n1836_sarah( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1871 then
		sqc17_4721_memories_of_mirage3_OnTalk_n1871_trader_april( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1836_sarah--------------------------------------------------------------------------------
function sqc17_4721_memories_of_mirage3_OnTalk_n1836_sarah( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1836_sarah-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1836_sarah-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1836_sarah-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1836_sarah-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, false);
			 end 

	end
	if npc_talk_index == "n1836_sarah-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4721, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4721, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4721, 1);
				npc_talk_index = "n1836_sarah-1";

	end
	if npc_talk_index == "n1836_sarah-1-b" then 
	end
	if npc_talk_index == "n1836_sarah-1-c" then 
	end
	if npc_talk_index == "n1836_sarah-1-d" then 
	end
	if npc_talk_index == "n1836_sarah-1-e" then 
	end
	if npc_talk_index == "n1836_sarah-1-f" then 
	end
	if npc_talk_index == "n1836_sarah-1-g" then 
	end
	if npc_talk_index == "n1836_sarah-1-h" then 
	end
	if npc_talk_index == "n1836_sarah-1-i" then 
	end
	if npc_talk_index == "n1836_sarah-1-j" then 
	end
	if npc_talk_index == "n1836_sarah-1-k" then 
	end
	if npc_talk_index == "n1836_sarah-1-l" then 
	end
	if npc_talk_index == "n1836_sarah-1-m" then 
	end
	if npc_talk_index == "n1836_sarah-1-n" then 
	end
	if npc_talk_index == "n1836_sarah-1-o" then 
	end
	if npc_talk_index == "n1836_sarah-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1871_trader_april--------------------------------------------------------------------------------
function sqc17_4721_memories_of_mirage3_OnTalk_n1871_trader_april( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1871_trader_april-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1871_trader_april-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1871_trader_april-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1871_trader_april-2-b" then 
	end
	if npc_talk_index == "n1871_trader_april-2-c" then 
	end
	if npc_talk_index == "n1871_trader_april-2-d" then 
	end
	if npc_talk_index == "n1871_trader_april-2-e" then 
	end
	if npc_talk_index == "n1871_trader_april-2-f" then 
	end
	if npc_talk_index == "n1871_trader_april-2-g" then 
	end
	if npc_talk_index == "n1871_trader_april-2-h" then 
	end
	if npc_talk_index == "n1871_trader_april-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 1094, 1 );
	end
	if npc_talk_index == "n1871_trader_april-4-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47210, questID, 1);
			 end 
	end
	if npc_talk_index == "n1871_trader_april-4-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4722, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4722, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4722, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1871_trader_april-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1871_trader_april-1", "sqc17_4722_memories_of_mirage4.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4721_memories_of_mirage3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4721);
	 local qstep= 2; 
	 if qstep == 2 then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				if  api_user_GetStageConstructionLevel( pRoom,  userObjID ) >= 0 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

				else
				end
		 end 
	 end 
end

function sqc17_4721_memories_of_mirage3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4721);
end

function sqc17_4721_memories_of_mirage3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4721);
	local questID=4721;
end

function sqc17_4721_memories_of_mirage3_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc17_4721_memories_of_mirage3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc17_4721_memories_of_mirage3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4721, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4721, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4721, 1);
				npc_talk_index = "n1836_sarah-1";
end

</GameServer>