<VillageServer>

function sq11_479_catastrophic_rain1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 36 then
		sq11_479_catastrophic_rain1_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq11_479_catastrophic_rain1_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-g" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4791, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4792, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4793, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4794, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4795, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4796, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4797, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4798, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4799, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceptted" then
				api_quest_AddQuest(userObjID,479, 1);
				api_quest_SetJournalStep(userObjID,479, 1);
				api_quest_SetQuestStep(userObjID,479, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 21070, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300016, 1);

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-b" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4791, true);
				 api_quest_RewardQuestUser(userObjID, 4791, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4792, true);
				 api_quest_RewardQuestUser(userObjID, 4792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4793, true);
				 api_quest_RewardQuestUser(userObjID, 4793, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4794, true);
				 api_quest_RewardQuestUser(userObjID, 4794, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4795, true);
				 api_quest_RewardQuestUser(userObjID, 4795, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4796, true);
				 api_quest_RewardQuestUser(userObjID, 4796, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4797, true);
				 api_quest_RewardQuestUser(userObjID, 4797, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4798, true);
				 api_quest_RewardQuestUser(userObjID, 4798, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4799, true);
				 api_quest_RewardQuestUser(userObjID, 4799, questID, 1);
			 end 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_quest_HasQuestItem(userObjID, 300016, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300016, api_quest_HasQuestItem(userObjID, 300016, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-next_quest" then 
				api_npc_NextScript(userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-1", "sq11_480_catastrophic_rain2.xml");
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_479_catastrophic_rain1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 479);
	if qstep == 1 and CountIndex == 21070 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300016, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300016, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 300016 then

	end
end

function sq11_479_catastrophic_rain1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 479);
	if qstep == 1 and CountIndex == 21070 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300016 and Count >= TargetCount  then

	end
end

function sq11_479_catastrophic_rain1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 479);
	local questID=479;
end

function sq11_479_catastrophic_rain1_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 21070, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300016, 1);
end

function sq11_479_catastrophic_rain1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_479_catastrophic_rain1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,479, 1);
				api_quest_SetJournalStep(userObjID,479, 1);
				api_quest_SetQuestStep(userObjID,479, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 21070, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300016, 1);
end

</VillageServer>

<GameServer>
function sq11_479_catastrophic_rain1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 36 then
		sq11_479_catastrophic_rain1_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq11_479_catastrophic_rain1_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-g" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4791, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4792, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4793, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4794, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4795, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4796, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4797, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4798, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4799, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,479, 1);
				api_quest_SetJournalStep( pRoom, userObjID,479, 1);
				api_quest_SetQuestStep( pRoom, userObjID,479, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 21070, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300016, 1);

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-b" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4791, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4791, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4793, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4793, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4794, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4794, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4795, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4795, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4796, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4796, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4797, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4797, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4798, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4798, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4799, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4799, questID, 1);
			 end 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_quest_HasQuestItem( pRoom, userObjID, 300016, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300016, api_quest_HasQuestItem( pRoom, userObjID, 300016, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-next_quest" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-1", "sq11_480_catastrophic_rain2.xml");
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_479_catastrophic_rain1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 479);
	if qstep == 1 and CountIndex == 21070 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300016, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300016, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 300016 then

	end
end

function sq11_479_catastrophic_rain1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 479);
	if qstep == 1 and CountIndex == 21070 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300016 and Count >= TargetCount  then

	end
end

function sq11_479_catastrophic_rain1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 479);
	local questID=479;
end

function sq11_479_catastrophic_rain1_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 21070, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300016, 1);
end

function sq11_479_catastrophic_rain1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_479_catastrophic_rain1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,479, 1);
				api_quest_SetJournalStep( pRoom, userObjID,479, 1);
				api_quest_SetQuestStep( pRoom, userObjID,479, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 21070, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300016, 1);
end

</GameServer>