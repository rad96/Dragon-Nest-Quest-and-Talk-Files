<VillageServer>

function eq_2379_5th_anniversary_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 990 then
		eq_2379_5th_anniversary_OnTalk_n990_5th_anniversary(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n990_5th_anniversary--------------------------------------------------------------------------------
function eq_2379_5th_anniversary_OnTalk_n990_5th_anniversary(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n990_5th_anniversary-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n990_5th_anniversary-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n990_5th_anniversary-2";
				if api_user_HasItem(userObjID, 805309719, 1) >= 1  and api_user_HasItem(userObjID, 805309720, 1) >= 1 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n990_5th_anniversary-3";

				else
									npc_talk_index = "n990_5th_anniversary-2";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n990_5th_anniversary-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n990_5th_anniversary-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23790, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23790, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23790, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23790, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23790, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23790, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23790, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 23790, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 23790, false);
			 end 

	end
	if npc_talk_index == "n990_5th_anniversary-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2379, 1);
				api_quest_SetJournalStep(userObjID,2379, 1);
				api_quest_SetQuestStep(userObjID,2379, 1);
				npc_talk_index = "n990_5th_anniversary-1";

	end
	if npc_talk_index == "n990_5th_anniversary-1-b" then 
	end
	if npc_talk_index == "n990_5th_anniversary-1-c" then 
	end
	if npc_talk_index == "n990_5th_anniversary-1-d" then 
	end
	if npc_talk_index == "n990_5th_anniversary-1-e" then 
	end
	if npc_talk_index == "n990_5th_anniversary-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805309719, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 805309720, 1);
	end
	if npc_talk_index == "n990_5th_anniversary-3-itemchk" then 
				if api_user_HasItem(userObjID, 805309719, 1) >= 1  and api_user_HasItem(userObjID, 805309720, 1) >= 1 then
									npc_talk_index = "n990_5th_anniversary-3-b";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23790, true);
				 api_quest_RewardQuestUser(userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23790, true);
				 api_quest_RewardQuestUser(userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23790, true);
				 api_quest_RewardQuestUser(userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23790, true);
				 api_quest_RewardQuestUser(userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23790, true);
				 api_quest_RewardQuestUser(userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23790, true);
				 api_quest_RewardQuestUser(userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23790, true);
				 api_quest_RewardQuestUser(userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 23790, true);
				 api_quest_RewardQuestUser(userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 23790, true);
				 api_quest_RewardQuestUser(userObjID, 23790, questID, 1);
			 end 

				else
									npc_talk_index = "n990_5th_anniversary-3-a";

				end
	end
	if npc_talk_index == "n990_5th_anniversary-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_DelItem(userObjID, 805309719, 1, questID);
				api_user_DelItem(userObjID, 805309720, 1, questID);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n990_5th_anniversary-3-d" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function eq_2379_5th_anniversary_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2379);
	if qstep == 2 and CountIndex == 805309719 then
				if api_user_HasItem(userObjID, 805309719, 1) >= 1  and api_user_HasItem(userObjID, 805309720, 1) >= 1 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 805309720 then
				if api_user_HasItem(userObjID, 805309719, 1) >= 1  and api_user_HasItem(userObjID, 805309720, 1) >= 1 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
end

function eq_2379_5th_anniversary_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2379);
	if qstep == 2 and CountIndex == 805309719 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 805309720 and Count >= TargetCount  then

	end
end

function eq_2379_5th_anniversary_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2379);
	local questID=2379;
end

function eq_2379_5th_anniversary_OnRemoteStart( userObjID, questID )
end

function eq_2379_5th_anniversary_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function eq_2379_5th_anniversary_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2379, 1);
				api_quest_SetJournalStep(userObjID,2379, 1);
				api_quest_SetQuestStep(userObjID,2379, 1);
				npc_talk_index = "n990_5th_anniversary-1";
end

</VillageServer>

<GameServer>
function eq_2379_5th_anniversary_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 990 then
		eq_2379_5th_anniversary_OnTalk_n990_5th_anniversary( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n990_5th_anniversary--------------------------------------------------------------------------------
function eq_2379_5th_anniversary_OnTalk_n990_5th_anniversary( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n990_5th_anniversary-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n990_5th_anniversary-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n990_5th_anniversary-2";
				if api_user_HasItem( pRoom, userObjID, 805309719, 1) >= 1  and api_user_HasItem( pRoom, userObjID, 805309720, 1) >= 1 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n990_5th_anniversary-3";

				else
									npc_talk_index = "n990_5th_anniversary-2";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n990_5th_anniversary-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n990_5th_anniversary-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, false);
			 end 

	end
	if npc_talk_index == "n990_5th_anniversary-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2379, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2379, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2379, 1);
				npc_talk_index = "n990_5th_anniversary-1";

	end
	if npc_talk_index == "n990_5th_anniversary-1-b" then 
	end
	if npc_talk_index == "n990_5th_anniversary-1-c" then 
	end
	if npc_talk_index == "n990_5th_anniversary-1-d" then 
	end
	if npc_talk_index == "n990_5th_anniversary-1-e" then 
	end
	if npc_talk_index == "n990_5th_anniversary-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805309719, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 805309720, 1);
	end
	if npc_talk_index == "n990_5th_anniversary-3-itemchk" then 
				if api_user_HasItem( pRoom, userObjID, 805309719, 1) >= 1  and api_user_HasItem( pRoom, userObjID, 805309720, 1) >= 1 then
									npc_talk_index = "n990_5th_anniversary-3-b";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23790, questID, 1);
			 end 

				else
									npc_talk_index = "n990_5th_anniversary-3-a";

				end
	end
	if npc_talk_index == "n990_5th_anniversary-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_DelItem( pRoom, userObjID, 805309719, 1, questID);
				api_user_DelItem( pRoom, userObjID, 805309720, 1, questID);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n990_5th_anniversary-3-d" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function eq_2379_5th_anniversary_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2379);
	if qstep == 2 and CountIndex == 805309719 then
				if api_user_HasItem( pRoom, userObjID, 805309719, 1) >= 1  and api_user_HasItem( pRoom, userObjID, 805309720, 1) >= 1 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 805309720 then
				if api_user_HasItem( pRoom, userObjID, 805309719, 1) >= 1  and api_user_HasItem( pRoom, userObjID, 805309720, 1) >= 1 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
end

function eq_2379_5th_anniversary_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2379);
	if qstep == 2 and CountIndex == 805309719 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 805309720 and Count >= TargetCount  then

	end
end

function eq_2379_5th_anniversary_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2379);
	local questID=2379;
end

function eq_2379_5th_anniversary_OnRemoteStart( pRoom,  userObjID, questID )
end

function eq_2379_5th_anniversary_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function eq_2379_5th_anniversary_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2379, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2379, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2379, 1);
				npc_talk_index = "n990_5th_anniversary-1";
end

</GameServer>