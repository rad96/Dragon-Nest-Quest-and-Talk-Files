<VillageServer>

function mqc02_9471_masters_article_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1690 then
		mqc02_9471_masters_article_OnTalk_n1690_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 34 then
		mqc02_9471_masters_article_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1690_eltia--------------------------------------------------------------------------------
function mqc02_9471_masters_article_OnTalk_n1690_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1690_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1690_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1690_eltia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1690_eltia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1690_eltia-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1690_eltia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9471, 2);
				api_quest_SetJournalStep(userObjID,9471, 1);
				api_quest_SetQuestStep(userObjID,9471, 1);
				npc_talk_index = "n1690_eltia-1";

	end
	if npc_talk_index == "n1690_eltia-1-b" then 
	end
	if npc_talk_index == "n1690_eltia-1-c" then 
	end
	if npc_talk_index == "n1690_eltia-1-d" then 
	end
	if npc_talk_index == "n1690_eltia-1-e" then 
	end
	if npc_talk_index == "n1690_eltia-1-f" then 
	end
	if npc_talk_index == "n1690_eltia-1-g" then 
	end
	if npc_talk_index == "n1690_eltia-1-h" then 
	end
	if npc_talk_index == "n1690_eltia-1-i" then 
	end
	if npc_talk_index == "n1690_eltia-1-j" then 
	end
	if npc_talk_index == "n1690_eltia-1-k" then 
	end
	if npc_talk_index == "n1690_eltia-1-l" then 
	end
	if npc_talk_index == "n1690_eltia-1-m" then 
	end
	if npc_talk_index == "n1690_eltia-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n034_cleric_master_germain--------------------------------------------------------------------------------
function mqc02_9471_masters_article_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n034_cleric_master_germain-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n034_cleric_master_germain-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-2-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-check_level12" then 
				if api_user_GetUserLevel(userObjID) >= 12 then
									npc_talk_index = "n034_cleric_master_germain-2-f";

				else
									npc_talk_index = "n034_cleric_master_germain-2-j";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-2-g" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-h" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-i" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-3" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 433, 30000);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300030, 30001);
	end
	if npc_talk_index == "n034_cleric_master_germain-4-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-f" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-g" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-h" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-i" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-j" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-k" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-l" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94710, true);
				 api_quest_RewardQuestUser(userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94710, true);
				 api_quest_RewardQuestUser(userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94710, true);
				 api_quest_RewardQuestUser(userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94710, true);
				 api_quest_RewardQuestUser(userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94710, true);
				 api_quest_RewardQuestUser(userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94710, true);
				 api_quest_RewardQuestUser(userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94710, true);
				 api_quest_RewardQuestUser(userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94710, true);
				 api_quest_RewardQuestUser(userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94710, true);
				 api_quest_RewardQuestUser(userObjID, 94710, questID, 1);
			 end 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-m" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9472, 2);
					api_quest_SetQuestStep(userObjID, 9472, 1);
					api_quest_SetJournalStep(userObjID, 9472, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n034_cleric_master_germain-1", "mqc02_9472_conflict.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc02_9471_masters_article_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9471);
	if qstep == 3 and CountIndex == 433 then
				if api_quest_HasQuestItem(userObjID, 300030, 1) == 1 then
									api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300030, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300030, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_ClearCountingInfo(userObjID, questID);

				end

	end
	if qstep == 3 and CountIndex == 300030 then
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function mqc02_9471_masters_article_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9471);
	if qstep == 3 and CountIndex == 433 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300030 and Count >= TargetCount  then

	end
end

function mqc02_9471_masters_article_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9471);
	local questID=9471;
end

function mqc02_9471_masters_article_OnRemoteStart( userObjID, questID )
end

function mqc02_9471_masters_article_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc02_9471_masters_article_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9471, 2);
				api_quest_SetJournalStep(userObjID,9471, 1);
				api_quest_SetQuestStep(userObjID,9471, 1);
				npc_talk_index = "n1690_eltia-1";
end

</VillageServer>

<GameServer>
function mqc02_9471_masters_article_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1690 then
		mqc02_9471_masters_article_OnTalk_n1690_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 34 then
		mqc02_9471_masters_article_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1690_eltia--------------------------------------------------------------------------------
function mqc02_9471_masters_article_OnTalk_n1690_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1690_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1690_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1690_eltia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1690_eltia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1690_eltia-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1690_eltia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9471, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9471, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9471, 1);
				npc_talk_index = "n1690_eltia-1";

	end
	if npc_talk_index == "n1690_eltia-1-b" then 
	end
	if npc_talk_index == "n1690_eltia-1-c" then 
	end
	if npc_talk_index == "n1690_eltia-1-d" then 
	end
	if npc_talk_index == "n1690_eltia-1-e" then 
	end
	if npc_talk_index == "n1690_eltia-1-f" then 
	end
	if npc_talk_index == "n1690_eltia-1-g" then 
	end
	if npc_talk_index == "n1690_eltia-1-h" then 
	end
	if npc_talk_index == "n1690_eltia-1-i" then 
	end
	if npc_talk_index == "n1690_eltia-1-j" then 
	end
	if npc_talk_index == "n1690_eltia-1-k" then 
	end
	if npc_talk_index == "n1690_eltia-1-l" then 
	end
	if npc_talk_index == "n1690_eltia-1-m" then 
	end
	if npc_talk_index == "n1690_eltia-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n034_cleric_master_germain--------------------------------------------------------------------------------
function mqc02_9471_masters_article_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n034_cleric_master_germain-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n034_cleric_master_germain-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-2-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-check_level12" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 12 then
									npc_talk_index = "n034_cleric_master_germain-2-f";

				else
									npc_talk_index = "n034_cleric_master_germain-2-j";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-2-g" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-h" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-i" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-3" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 433, 30000);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300030, 30001);
	end
	if npc_talk_index == "n034_cleric_master_germain-4-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-f" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-g" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-h" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-i" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-j" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-k" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-l" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94710, questID, 1);
			 end 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-m" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9472, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9472, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9472, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n034_cleric_master_germain-1", "mqc02_9472_conflict.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc02_9471_masters_article_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9471);
	if qstep == 3 and CountIndex == 433 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300030, 1) == 1 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300030, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300030, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				end

	end
	if qstep == 3 and CountIndex == 300030 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function mqc02_9471_masters_article_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9471);
	if qstep == 3 and CountIndex == 433 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300030 and Count >= TargetCount  then

	end
end

function mqc02_9471_masters_article_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9471);
	local questID=9471;
end

function mqc02_9471_masters_article_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc02_9471_masters_article_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc02_9471_masters_article_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9471, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9471, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9471, 1);
				npc_talk_index = "n1690_eltia-1";
end

</GameServer>