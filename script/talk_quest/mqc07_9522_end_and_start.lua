<VillageServer>

function mqc07_9522_end_and_start_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 701 then
		mqc07_9522_end_and_start_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mqc07_9522_end_and_start_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n701_oldkarakule--------------------------------------------------------------------------------
function mqc07_9522_end_and_start_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9522, 2);
				api_quest_SetJournalStep(userObjID,9522, 1);
				api_quest_SetQuestStep(userObjID,9522, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mqc07_9522_end_and_start_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-3-g" then 
	end
	if npc_talk_index == "n707_sidel-3-h" then 
	end
	if npc_talk_index == "n707_sidel-3-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95220, true);
				 api_quest_RewardQuestUser(userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95220, true);
				 api_quest_RewardQuestUser(userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95220, true);
				 api_quest_RewardQuestUser(userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95220, true);
				 api_quest_RewardQuestUser(userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95220, true);
				 api_quest_RewardQuestUser(userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95220, true);
				 api_quest_RewardQuestUser(userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95220, true);
				 api_quest_RewardQuestUser(userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95220, true);
				 api_quest_RewardQuestUser(userObjID, 95220, questID, 1);
			 end 
	end
	if npc_talk_index == "n707_sidel-3-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9523, 2);
					api_quest_SetQuestStep(userObjID, 9523, 1);
					api_quest_SetJournalStep(userObjID, 9523, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n707_sidel-3-k" then 
	end
	if npc_talk_index == "n707_sidel-3-l" then 
	end
	if npc_talk_index == "n707_sidel-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n707_sidel-1", "mqc08_9523_amplitude_of_sorrow.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc07_9522_end_and_start_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9522);
end

function mqc07_9522_end_and_start_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9522);
end

function mqc07_9522_end_and_start_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9522);
	local questID=9522;
end

function mqc07_9522_end_and_start_OnRemoteStart( userObjID, questID )
end

function mqc07_9522_end_and_start_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc07_9522_end_and_start_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9522, 2);
				api_quest_SetJournalStep(userObjID,9522, 1);
				api_quest_SetQuestStep(userObjID,9522, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</VillageServer>

<GameServer>
function mqc07_9522_end_and_start_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 701 then
		mqc07_9522_end_and_start_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mqc07_9522_end_and_start_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n701_oldkarakule--------------------------------------------------------------------------------
function mqc07_9522_end_and_start_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9522, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9522, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9522, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mqc07_9522_end_and_start_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-3-g" then 
	end
	if npc_talk_index == "n707_sidel-3-h" then 
	end
	if npc_talk_index == "n707_sidel-3-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95220, questID, 1);
			 end 
	end
	if npc_talk_index == "n707_sidel-3-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9523, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9523, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9523, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n707_sidel-3-k" then 
	end
	if npc_talk_index == "n707_sidel-3-l" then 
	end
	if npc_talk_index == "n707_sidel-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n707_sidel-1", "mqc08_9523_amplitude_of_sorrow.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc07_9522_end_and_start_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9522);
end

function mqc07_9522_end_and_start_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9522);
end

function mqc07_9522_end_and_start_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9522);
	local questID=9522;
end

function mqc07_9522_end_and_start_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc07_9522_end_and_start_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc07_9522_end_and_start_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9522, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9522, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9522, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</GameServer>