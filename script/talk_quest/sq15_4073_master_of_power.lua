<VillageServer>

function sq15_4073_master_of_power_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 566 then
		sq15_4073_master_of_power_OnTalk_n566_kali_totem(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 703 then
		sq15_4073_master_of_power_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 864 then
		sq15_4073_master_of_power_OnTalk_n864_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n566_kali_totem--------------------------------------------------------------------------------
function sq15_4073_master_of_power_OnTalk_n566_kali_totem(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n566_kali_totem-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n566_kali_totem-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n566_kali_totem-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n566_kali_totem-5-f" then 
	end
	if npc_talk_index == "n566_kali_totem-5-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 end 
	end
	if npc_talk_index == "n566_kali_totem-5-f" then 
	end
	if npc_talk_index == "n566_kali_totem-5-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40730, true);
				 api_quest_RewardQuestUser(userObjID, 40730, questID, 1);
			 end 
	end
	if npc_talk_index == "n566_kali_totem-5-beds" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 55);
				npc_talk_index = "n566_kali_totem-5-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n566_kali_totem-5-bese" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 56);
				npc_talk_index = "n566_kali_totem-5-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n566_kali_totem-5-a" then 
	end
	if npc_talk_index == "n566_kali_totem-5-b" then 
	end
	if npc_talk_index == "n566_kali_totem-5-f" then 
	end
	if npc_talk_index == "n566_kali_totem-5-i" then 
	end
	if npc_talk_index == "n566_kali_totem-5-j" then 
	end
	if npc_talk_index == "n566_kali_totem-5-f" then 
	end
	if npc_talk_index == "n566_kali_totem-5-f" then 
	end
	if npc_talk_index == "n566_kali_totem-5-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 end 
	end
	if npc_talk_index == "n566_kali_totem-5-f" then 
	end
	if npc_talk_index == "n566_kali_totem-5-l" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 end 
	end
	if npc_talk_index == "n566_kali_totem-5-bebd" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 58);
				npc_talk_index = "n566_kali_totem-5-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n566_kali_totem-5-besd" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 59);
				npc_talk_index = "n566_kali_totem-5-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n566_kali_totem-5-n" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40732, true);
				 api_quest_RewardQuestUser(userObjID, 40732, questID, 1);
			 end 
	end
	if npc_talk_index == "n566_kali_totem-5-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n703_book_doctor--------------------------------------------------------------------------------
function sq15_4073_master_of_power_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n703_book_doctor-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n703_book_doctor-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n703_book_doctor-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n703_book_doctor-accepting-a" then
				if api_user_GetUserJobID(userObjID) == 55   or api_user_GetUserJobID(userObjID) == 56   or api_user_GetUserJobID(userObjID) == 58   or api_user_GetUserJobID(userObjID) == 59  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 end 
				npc_talk_index = "n703_book_doctor-accepting-b";

				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40730, false);
			 end 
				npc_talk_index = "n703_book_doctor-accepting-a";

				end

	end
	if npc_talk_index == "n703_book_doctor-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4073, 1);
				api_quest_SetJournalStep(userObjID,4073, 1);
				api_quest_SetQuestStep(userObjID,4073, 1);
				npc_talk_index = "n703_book_doctor-1";

	end
	if npc_talk_index == "n703_book_doctor-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,4073, 1);
				api_quest_SetJournalStep(userObjID,4073, 1);
				api_quest_SetQuestStep(userObjID,4073, 1);
				npc_talk_index = "n703_book_doctor-5";
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);

	end
	if npc_talk_index == "n703_book_doctor-1-b" then 
	end
	if npc_talk_index == "n703_book_doctor-1-c" then 
	end
	if npc_talk_index == "n703_book_doctor-1-d" then 
	end
	if npc_talk_index == "n703_book_doctor-1-e" then 
	end
	if npc_talk_index == "n703_book_doctor-1-chkl1" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									if api_user_GetUserLevel(userObjID) >= 45 then
									if api_user_GetUserJobID(userObjID) == 54   or api_user_GetUserJobID(userObjID) == 57  then
									npc_talk_index = "n703_book_doctor-1-i";

				else
									npc_talk_index = "n703_book_doctor-1-h";

				end

				else
									npc_talk_index = "n703_book_doctor-1-h";

				end

				else
									npc_talk_index = "n703_book_doctor-1-g";

				end
	end
	if npc_talk_index == "n703_book_doctor-1-g" then 
	end
	if npc_talk_index == "n703_book_doctor-1-chgm1" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13512,1);
	end
	if npc_talk_index == "n703_book_doctor-2-chgm2" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n703_book_doctor-2-c";

				else
									npc_talk_index = "n703_book_doctor-2-a";

				end
	end
	if npc_talk_index == "n703_book_doctor-2-cgmd2" then 
				api_user_ChangeMap(userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n864_gaharam--------------------------------------------------------------------------------
function sq15_4073_master_of_power_OnTalk_n864_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n864_gaharam-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n864_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n864_gaharam-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n864_gaharam-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n864_gaharam-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n864_gaharam-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n864_gaharam-2-ckjob2" then 
				if api_user_GetUserJobID(userObjID) == 54  then
									npc_talk_index = "n864_gaharam-2-h";

				else
									npc_talk_index = "n864_gaharam-2-i";

				end
	end
	if npc_talk_index == "n864_gaharam-2-d" then 
	end
	if npc_talk_index == "n864_gaharam-2-e" then 
	end
	if npc_talk_index == "n864_gaharam-2-f" then 
	end
	if npc_talk_index == "n864_gaharam-2-g" then 
	end
	if npc_talk_index == "n864_gaharam-2-e" then 
	end
	if npc_talk_index == "n864_gaharam-2-testds" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-3";
				api_user_SetSecondJobSkill( userObjID, 55 );
	end
	if npc_talk_index == "n864_gaharam-2-d" then 
	end
	if npc_talk_index == "n864_gaharam-2-testse" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-3";
				api_user_SetSecondJobSkill( userObjID, 56 );
	end
	if npc_talk_index == "n864_gaharam-2-g" then 
	end
	if npc_talk_index == "n864_gaharam-2-testbd" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-3";
				api_user_SetSecondJobSkill( userObjID, 58 );
	end
	if npc_talk_index == "n864_gaharam-2-f" then 
	end
	if npc_talk_index == "n864_gaharam-2-testsd" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-3";
				api_user_SetSecondJobSkill( userObjID, 59 );
	end
	if npc_talk_index == "n864_gaharam-2-j" then 
	end
	if npc_talk_index == "n864_gaharam-2-j" then 
	end
	if npc_talk_index == "n864_gaharam-2-k" then 
	end
	if npc_talk_index == "n864_gaharam-2-open_ui2" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n864_gaharam-4-chds4" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-4-c";
				api_user_SetSecondJobSkill( userObjID, 55 );
	end
	if npc_talk_index == "n864_gaharam-4-chse4" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-4-c";
				api_user_SetSecondJobSkill( userObjID, 56 );
	end
	if npc_talk_index == "n864_gaharam-4-chbd4" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-4-c";
				api_user_SetSecondJobSkill( userObjID, 58 );
	end
	if npc_talk_index == "n864_gaharam-4-chsd4" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-4-c";
				api_user_SetSecondJobSkill( userObjID, 59 );
	end
	if npc_talk_index == "n864_gaharam-4-open_ui5" then 
				api_ui_OpenJobChange(userObjID)
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4073_master_of_power_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4073);
end

function sq15_4073_master_of_power_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4073);
end

function sq15_4073_master_of_power_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4073);
	local questID=4073;
end

function sq15_4073_master_of_power_OnRemoteStart( userObjID, questID )
end

function sq15_4073_master_of_power_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4073_master_of_power_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4073, 1);
				api_quest_SetJournalStep(userObjID,4073, 1);
				api_quest_SetQuestStep(userObjID,4073, 1);
				npc_talk_index = "n703_book_doctor-1";
end

</VillageServer>

<GameServer>
function sq15_4073_master_of_power_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 566 then
		sq15_4073_master_of_power_OnTalk_n566_kali_totem( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 703 then
		sq15_4073_master_of_power_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 864 then
		sq15_4073_master_of_power_OnTalk_n864_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n566_kali_totem--------------------------------------------------------------------------------
function sq15_4073_master_of_power_OnTalk_n566_kali_totem( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n566_kali_totem-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n566_kali_totem-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n566_kali_totem-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n566_kali_totem-5-f" then 
	end
	if npc_talk_index == "n566_kali_totem-5-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 end 
	end
	if npc_talk_index == "n566_kali_totem-5-f" then 
	end
	if npc_talk_index == "n566_kali_totem-5-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40730, questID, 1);
			 end 
	end
	if npc_talk_index == "n566_kali_totem-5-beds" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 55);
				npc_talk_index = "n566_kali_totem-5-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n566_kali_totem-5-bese" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 56);
				npc_talk_index = "n566_kali_totem-5-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n566_kali_totem-5-a" then 
	end
	if npc_talk_index == "n566_kali_totem-5-b" then 
	end
	if npc_talk_index == "n566_kali_totem-5-f" then 
	end
	if npc_talk_index == "n566_kali_totem-5-i" then 
	end
	if npc_talk_index == "n566_kali_totem-5-j" then 
	end
	if npc_talk_index == "n566_kali_totem-5-f" then 
	end
	if npc_talk_index == "n566_kali_totem-5-f" then 
	end
	if npc_talk_index == "n566_kali_totem-5-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 end 
	end
	if npc_talk_index == "n566_kali_totem-5-f" then 
	end
	if npc_talk_index == "n566_kali_totem-5-l" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 end 
	end
	if npc_talk_index == "n566_kali_totem-5-bebd" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 58);
				npc_talk_index = "n566_kali_totem-5-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n566_kali_totem-5-besd" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 59);
				npc_talk_index = "n566_kali_totem-5-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n566_kali_totem-5-n" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40732, questID, 1);
			 end 
	end
	if npc_talk_index == "n566_kali_totem-5-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n703_book_doctor--------------------------------------------------------------------------------
function sq15_4073_master_of_power_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n703_book_doctor-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n703_book_doctor-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n703_book_doctor-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n703_book_doctor-accepting-a" then
				if api_user_GetUserJobID( pRoom, userObjID) == 55   or api_user_GetUserJobID( pRoom, userObjID) == 56   or api_user_GetUserJobID( pRoom, userObjID) == 58   or api_user_GetUserJobID( pRoom, userObjID) == 59  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 end 
				npc_talk_index = "n703_book_doctor-accepting-b";

				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40730, false);
			 end 
				npc_talk_index = "n703_book_doctor-accepting-a";

				end

	end
	if npc_talk_index == "n703_book_doctor-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4073, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4073, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4073, 1);
				npc_talk_index = "n703_book_doctor-1";

	end
	if npc_talk_index == "n703_book_doctor-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,4073, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4073, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4073, 1);
				npc_talk_index = "n703_book_doctor-5";
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

	end
	if npc_talk_index == "n703_book_doctor-1-b" then 
	end
	if npc_talk_index == "n703_book_doctor-1-c" then 
	end
	if npc_talk_index == "n703_book_doctor-1-d" then 
	end
	if npc_talk_index == "n703_book_doctor-1-e" then 
	end
	if npc_talk_index == "n703_book_doctor-1-chkl1" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									if api_user_GetUserLevel( pRoom, userObjID) >= 45 then
									if api_user_GetUserJobID( pRoom, userObjID) == 54   or api_user_GetUserJobID( pRoom, userObjID) == 57  then
									npc_talk_index = "n703_book_doctor-1-i";

				else
									npc_talk_index = "n703_book_doctor-1-h";

				end

				else
									npc_talk_index = "n703_book_doctor-1-h";

				end

				else
									npc_talk_index = "n703_book_doctor-1-g";

				end
	end
	if npc_talk_index == "n703_book_doctor-1-g" then 
	end
	if npc_talk_index == "n703_book_doctor-1-chgm1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end
	if npc_talk_index == "n703_book_doctor-2-chgm2" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n703_book_doctor-2-c";

				else
									npc_talk_index = "n703_book_doctor-2-a";

				end
	end
	if npc_talk_index == "n703_book_doctor-2-cgmd2" then 
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n864_gaharam--------------------------------------------------------------------------------
function sq15_4073_master_of_power_OnTalk_n864_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n864_gaharam-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n864_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n864_gaharam-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n864_gaharam-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n864_gaharam-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n864_gaharam-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n864_gaharam-2-ckjob2" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 54  then
									npc_talk_index = "n864_gaharam-2-h";

				else
									npc_talk_index = "n864_gaharam-2-i";

				end
	end
	if npc_talk_index == "n864_gaharam-2-d" then 
	end
	if npc_talk_index == "n864_gaharam-2-e" then 
	end
	if npc_talk_index == "n864_gaharam-2-f" then 
	end
	if npc_talk_index == "n864_gaharam-2-g" then 
	end
	if npc_talk_index == "n864_gaharam-2-e" then 
	end
	if npc_talk_index == "n864_gaharam-2-testds" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-3";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 55 );
	end
	if npc_talk_index == "n864_gaharam-2-d" then 
	end
	if npc_talk_index == "n864_gaharam-2-testse" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-3";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 56 );
	end
	if npc_talk_index == "n864_gaharam-2-g" then 
	end
	if npc_talk_index == "n864_gaharam-2-testbd" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-3";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 58 );
	end
	if npc_talk_index == "n864_gaharam-2-f" then 
	end
	if npc_talk_index == "n864_gaharam-2-testsd" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-3";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 59 );
	end
	if npc_talk_index == "n864_gaharam-2-j" then 
	end
	if npc_talk_index == "n864_gaharam-2-j" then 
	end
	if npc_talk_index == "n864_gaharam-2-k" then 
	end
	if npc_talk_index == "n864_gaharam-2-open_ui2" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n864_gaharam-4-chds4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-4-c";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 55 );
	end
	if npc_talk_index == "n864_gaharam-4-chse4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-4-c";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 56 );
	end
	if npc_talk_index == "n864_gaharam-4-chbd4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-4-c";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 58 );
	end
	if npc_talk_index == "n864_gaharam-4-chsd4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n864_gaharam-4-c";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 59 );
	end
	if npc_talk_index == "n864_gaharam-4-open_ui5" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4073_master_of_power_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4073);
end

function sq15_4073_master_of_power_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4073);
end

function sq15_4073_master_of_power_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4073);
	local questID=4073;
end

function sq15_4073_master_of_power_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4073_master_of_power_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4073_master_of_power_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4073, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4073, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4073, 1);
				npc_talk_index = "n703_book_doctor-1";
end

</GameServer>