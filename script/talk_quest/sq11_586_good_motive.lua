<VillageServer>

function sq11_586_good_motive_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 252 then
		sq11_586_good_motive_OnTalk_n252_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n252_saint_guard--------------------------------------------------------------------------------
function sq11_586_good_motive_OnTalk_n252_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n252_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n252_saint_guard-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n252_saint_guard-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n252_saint_guard-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5861, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5862, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5863, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5864, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5865, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5866, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5867, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5868, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5869, false);
			 end 

	end
	if npc_talk_index == "n252_saint_guard-accepting-acceppted" then
				api_quest_AddQuest(userObjID,586, 1);
				api_quest_SetJournalStep(userObjID,586, 1);
				api_quest_SetQuestStep(userObjID,586, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1063, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201063, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300282, 10);
				npc_talk_index = "n252_saint_guard-1";

	end
	if npc_talk_index == "n252_saint_guard-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5861, true);
				 api_quest_RewardQuestUser(userObjID, 5861, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5862, true);
				 api_quest_RewardQuestUser(userObjID, 5862, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5863, true);
				 api_quest_RewardQuestUser(userObjID, 5863, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5864, true);
				 api_quest_RewardQuestUser(userObjID, 5864, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5865, true);
				 api_quest_RewardQuestUser(userObjID, 5865, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5866, true);
				 api_quest_RewardQuestUser(userObjID, 5866, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5867, true);
				 api_quest_RewardQuestUser(userObjID, 5867, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5868, true);
				 api_quest_RewardQuestUser(userObjID, 5868, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5869, true);
				 api_quest_RewardQuestUser(userObjID, 5869, questID, 1);
			 end 
	end
	if npc_talk_index == "n252_saint_guard-2-c" then 

				if api_quest_HasQuestItem(userObjID, 300282, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300282, api_quest_HasQuestItem(userObjID, 300282, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 252, 270 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_586_good_motive_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 586);
	if qstep == 1 and CountIndex == 1063 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300282, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300282, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300282 then

	end
	if qstep == 1 and CountIndex == 201063 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300282, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300282, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_586_good_motive_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 586);
	if qstep == 1 and CountIndex == 1063 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300282 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201063 and Count >= TargetCount  then

	end
end

function sq11_586_good_motive_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 586);
	local questID=586;
end

function sq11_586_good_motive_OnRemoteStart( userObjID, questID )
end

function sq11_586_good_motive_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_586_good_motive_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_586_good_motive_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 252 then
		sq11_586_good_motive_OnTalk_n252_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n252_saint_guard--------------------------------------------------------------------------------
function sq11_586_good_motive_OnTalk_n252_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n252_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n252_saint_guard-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n252_saint_guard-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n252_saint_guard-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5861, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5862, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5863, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5864, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5865, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5866, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5867, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5868, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5869, false);
			 end 

	end
	if npc_talk_index == "n252_saint_guard-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,586, 1);
				api_quest_SetJournalStep( pRoom, userObjID,586, 1);
				api_quest_SetQuestStep( pRoom, userObjID,586, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1063, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201063, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300282, 10);
				npc_talk_index = "n252_saint_guard-1";

	end
	if npc_talk_index == "n252_saint_guard-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5861, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5861, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5862, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5862, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5863, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5863, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5864, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5864, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5865, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5865, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5866, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5866, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5867, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5867, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5868, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5868, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5869, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5869, questID, 1);
			 end 
	end
	if npc_talk_index == "n252_saint_guard-2-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300282, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300282, api_quest_HasQuestItem( pRoom, userObjID, 300282, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 252, 270 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_586_good_motive_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 586);
	if qstep == 1 and CountIndex == 1063 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300282, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300282, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300282 then

	end
	if qstep == 1 and CountIndex == 201063 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300282, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300282, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_586_good_motive_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 586);
	if qstep == 1 and CountIndex == 1063 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300282 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201063 and Count >= TargetCount  then

	end
end

function sq11_586_good_motive_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 586);
	local questID=586;
end

function sq11_586_good_motive_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_586_good_motive_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_586_good_motive_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>