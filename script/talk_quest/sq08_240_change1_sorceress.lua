<VillageServer>

function sq08_240_change1_sorceress_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 229 then
		sq08_240_change1_sorceress_OnTalk_n229_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		sq08_240_change1_sorceress_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n229_sorceress_tara--------------------------------------------------------------------------------
function sq08_240_change1_sorceress_OnTalk_n229_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n229_sorceress_tara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n229_sorceress_tara-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n229_sorceress_tara-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n229_sorceress_tara-2-b" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-c" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-d" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-e" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-f" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-open_ui2" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n229_sorceress_tara-3-open_ui3" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n229_sorceress_tara-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function sq08_240_change1_sorceress_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n035_soceress_master_tiana-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2400, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2400, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2400, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2400, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2400, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2400, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2400, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2400, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2400, false);
			 end 

	end
	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest(userObjID,240, 1);
				api_quest_SetJournalStep(userObjID,240, 1);
				api_quest_SetQuestStep(userObjID,240, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-1-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-comp_check1" then 
				if api_user_GetUserJobID(userObjID) == 17  then
									npc_talk_index = "n035_soceress_master_tiana-1-l";

				else
									if api_user_GetUserJobID(userObjID) == 18  then
									npc_talk_index = "n035_soceress_master_tiana-1-l";

				else
									if api_user_GetUserJobID(userObjID) == 35  then
									npc_talk_index = "n035_soceress_master_tiana-1-l";

				else
									if api_user_GetUserJobID(userObjID) == 36  then
									npc_talk_index = "n035_soceress_master_tiana-1-l";

				else
									if api_user_GetUserJobID(userObjID) == 37  then
									npc_talk_index = "n035_soceress_master_tiana-1-l";

				else
									if api_user_GetUserJobID(userObjID) == 38  then
									npc_talk_index = "n035_soceress_master_tiana-1-l";

				else
									npc_talk_index = "n035_soceress_master_tiana-1-c";

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-g" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-h" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-party_check1" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n035_soceress_master_tiana-1-i";

				else
									npc_talk_index = "n035_soceress_master_tiana-1-k";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-j" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-changemap1" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13520,2);
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-m" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-party_check2" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n035_soceress_master_tiana-2-a";

				else
									npc_talk_index = "n035_soceress_master_tiana-2-c";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-changemap2" then 
				api_user_ChangeMap(userObjID,13520,2);
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-g" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2400, true);
				 api_quest_RewardQuestUser(userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2400, true);
				 api_quest_RewardQuestUser(userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2400, true);
				 api_quest_RewardQuestUser(userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2400, true);
				 api_quest_RewardQuestUser(userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2400, true);
				 api_quest_RewardQuestUser(userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2400, true);
				 api_quest_RewardQuestUser(userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2400, true);
				 api_quest_RewardQuestUser(userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2400, true);
				 api_quest_RewardQuestUser(userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2400, true);
				 api_quest_RewardQuestUser(userObjID, 2400, questID, 1);
			 end 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-i" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 342, 1);
					api_quest_SetQuestStep(userObjID, 342, 1);
					api_quest_SetJournalStep(userObjID, 342, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-j" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-k" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n035_soceress_master_tiana-1", "sq08_342_change_job3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_240_change1_sorceress_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 240);
end

function sq08_240_change1_sorceress_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 240);
end

function sq08_240_change1_sorceress_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 240);
	local questID=240;
end

function sq08_240_change1_sorceress_OnRemoteStart( userObjID, questID )
end

function sq08_240_change1_sorceress_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_240_change1_sorceress_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,240, 1);
				api_quest_SetJournalStep(userObjID,240, 1);
				api_quest_SetQuestStep(userObjID,240, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</VillageServer>

<GameServer>
function sq08_240_change1_sorceress_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 229 then
		sq08_240_change1_sorceress_OnTalk_n229_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		sq08_240_change1_sorceress_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n229_sorceress_tara--------------------------------------------------------------------------------
function sq08_240_change1_sorceress_OnTalk_n229_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n229_sorceress_tara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n229_sorceress_tara-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n229_sorceress_tara-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n229_sorceress_tara-2-b" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-c" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-d" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-e" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-f" then 
	end
	if npc_talk_index == "n229_sorceress_tara-2-open_ui2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n229_sorceress_tara-3-open_ui3" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n229_sorceress_tara-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function sq08_240_change1_sorceress_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n035_soceress_master_tiana-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, false);
			 end 

	end
	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,240, 1);
				api_quest_SetJournalStep( pRoom, userObjID,240, 1);
				api_quest_SetQuestStep( pRoom, userObjID,240, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-1-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-comp_check1" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 17  then
									npc_talk_index = "n035_soceress_master_tiana-1-l";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 18  then
									npc_talk_index = "n035_soceress_master_tiana-1-l";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 35  then
									npc_talk_index = "n035_soceress_master_tiana-1-l";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 36  then
									npc_talk_index = "n035_soceress_master_tiana-1-l";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 37  then
									npc_talk_index = "n035_soceress_master_tiana-1-l";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 38  then
									npc_talk_index = "n035_soceress_master_tiana-1-l";

				else
									npc_talk_index = "n035_soceress_master_tiana-1-c";

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-g" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-h" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-party_check1" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n035_soceress_master_tiana-1-i";

				else
									npc_talk_index = "n035_soceress_master_tiana-1-k";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-j" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-changemap1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13520,2);
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-m" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-party_check2" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n035_soceress_master_tiana-2-a";

				else
									npc_talk_index = "n035_soceress_master_tiana-2-c";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-changemap2" then 
				api_user_ChangeMap( pRoom, userObjID,13520,2);
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-g" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2400, questID, 1);
			 end 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-i" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 342, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 342, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 342, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-j" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-k" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n035_soceress_master_tiana-1", "sq08_342_change_job3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_240_change1_sorceress_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 240);
end

function sq08_240_change1_sorceress_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 240);
end

function sq08_240_change1_sorceress_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 240);
	local questID=240;
end

function sq08_240_change1_sorceress_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_240_change1_sorceress_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_240_change1_sorceress_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,240, 1);
				api_quest_SetJournalStep( pRoom, userObjID,240, 1);
				api_quest_SetQuestStep( pRoom, userObjID,240, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</GameServer>