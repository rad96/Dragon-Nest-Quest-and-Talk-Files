<VillageServer>

function nq38_7601_kind_sorceress_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 713 then
		nq38_7601_kind_sorceress_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n713_soceress_tamara--------------------------------------------------------------------------------
function nq38_7601_kind_sorceress_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-acceptted" then
				api_quest_AddQuest(userObjID,7601, 1);
				api_quest_SetJournalStep(userObjID,7601, 1);
				api_quest_SetQuestStep(userObjID,7601, 1);
				npc_talk_index = "n713_soceress_tamara-1";

	end
	if npc_talk_index == "n713_soceress_tamara-1-b" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 713, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq38_7601_kind_sorceress_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7601);
end

function nq38_7601_kind_sorceress_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7601);
end

function nq38_7601_kind_sorceress_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7601);
	local questID=7601;
end

function nq38_7601_kind_sorceress_OnRemoteStart( userObjID, questID )
end

function nq38_7601_kind_sorceress_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq38_7601_kind_sorceress_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,7601, 1);
				api_quest_SetJournalStep(userObjID,7601, 1);
				api_quest_SetQuestStep(userObjID,7601, 1);
				npc_talk_index = "n713_soceress_tamara-1";
end

</VillageServer>

<GameServer>
function nq38_7601_kind_sorceress_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 713 then
		nq38_7601_kind_sorceress_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n713_soceress_tamara--------------------------------------------------------------------------------
function nq38_7601_kind_sorceress_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,7601, 1);
				api_quest_SetJournalStep( pRoom, userObjID,7601, 1);
				api_quest_SetQuestStep( pRoom, userObjID,7601, 1);
				npc_talk_index = "n713_soceress_tamara-1";

	end
	if npc_talk_index == "n713_soceress_tamara-1-b" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 713, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq38_7601_kind_sorceress_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7601);
end

function nq38_7601_kind_sorceress_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7601);
end

function nq38_7601_kind_sorceress_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7601);
	local questID=7601;
end

function nq38_7601_kind_sorceress_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq38_7601_kind_sorceress_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq38_7601_kind_sorceress_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,7601, 1);
				api_quest_SetJournalStep( pRoom, userObjID,7601, 1);
				api_quest_SetQuestStep( pRoom, userObjID,7601, 1);
				npc_talk_index = "n713_soceress_tamara-1";
end

</GameServer>