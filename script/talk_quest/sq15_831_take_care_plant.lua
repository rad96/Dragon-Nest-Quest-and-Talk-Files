<VillageServer>

function sq15_831_take_care_plant_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 713 then
		sq15_831_take_care_plant_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n713_soceress_tamara--------------------------------------------------------------------------------
function sq15_831_take_care_plant_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n713_soceress_tamara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8310, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8310, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8310, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8310, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8310, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8310, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8310, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8310, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8310, false);
			 end 

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-acceptted" then
				api_quest_AddQuest(userObjID,831, 1);
				api_quest_SetJournalStep(userObjID,831, 1);
				api_quest_SetQuestStep(userObjID,831, 1);
				npc_talk_index = "n713_soceress_tamara-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1403, 5);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201403, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 1401, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201401, 30000);

	end
	if npc_talk_index == "n713_soceress_tamara-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8310, true);
				 api_quest_RewardQuestUser(userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8310, true);
				 api_quest_RewardQuestUser(userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8310, true);
				 api_quest_RewardQuestUser(userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8310, true);
				 api_quest_RewardQuestUser(userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8310, true);
				 api_quest_RewardQuestUser(userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8310, true);
				 api_quest_RewardQuestUser(userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8310, true);
				 api_quest_RewardQuestUser(userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8310, true);
				 api_quest_RewardQuestUser(userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8310, true);
				 api_quest_RewardQuestUser(userObjID, 8310, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n713_soceress_tamara-2-c" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_831_take_care_plant_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 831);
	if qstep == 1 and CountIndex == 1403 then

	end
	if qstep == 1 and CountIndex == 201403 then
				api_quest_IncCounting(userObjID, 2, 1403);

	end
	if qstep == 1 and CountIndex == 1401 then
				api_quest_IncCounting(userObjID, 2, 1403);

	end
	if qstep == 1 and CountIndex == 201401 then
				api_quest_IncCounting(userObjID, 2, 1403);

	end
end

function sq15_831_take_care_plant_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 831);
	if qstep == 1 and CountIndex == 1403 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201403 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1401 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201401 and Count >= TargetCount  then

	end
end

function sq15_831_take_care_plant_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 831);
	local questID=831;
end

function sq15_831_take_care_plant_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1403, 5);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201403, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 1401, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201401, 30000);
end

function sq15_831_take_care_plant_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_831_take_care_plant_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,831, 1);
				api_quest_SetJournalStep(userObjID,831, 1);
				api_quest_SetQuestStep(userObjID,831, 1);
				npc_talk_index = "n713_soceress_tamara-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1403, 5);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201403, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 1401, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201401, 30000);
end

</VillageServer>

<GameServer>
function sq15_831_take_care_plant_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 713 then
		sq15_831_take_care_plant_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n713_soceress_tamara--------------------------------------------------------------------------------
function sq15_831_take_care_plant_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n713_soceress_tamara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, false);
			 end 

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,831, 1);
				api_quest_SetJournalStep( pRoom, userObjID,831, 1);
				api_quest_SetQuestStep( pRoom, userObjID,831, 1);
				npc_talk_index = "n713_soceress_tamara-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1403, 5);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201403, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 1401, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201401, 30000);

	end
	if npc_talk_index == "n713_soceress_tamara-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8310, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8310, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8310, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n713_soceress_tamara-2-c" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_831_take_care_plant_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 831);
	if qstep == 1 and CountIndex == 1403 then

	end
	if qstep == 1 and CountIndex == 201403 then
				api_quest_IncCounting( pRoom, userObjID, 2, 1403);

	end
	if qstep == 1 and CountIndex == 1401 then
				api_quest_IncCounting( pRoom, userObjID, 2, 1403);

	end
	if qstep == 1 and CountIndex == 201401 then
				api_quest_IncCounting( pRoom, userObjID, 2, 1403);

	end
end

function sq15_831_take_care_plant_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 831);
	if qstep == 1 and CountIndex == 1403 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201403 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1401 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201401 and Count >= TargetCount  then

	end
end

function sq15_831_take_care_plant_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 831);
	local questID=831;
end

function sq15_831_take_care_plant_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1403, 5);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201403, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 1401, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201401, 30000);
end

function sq15_831_take_care_plant_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_831_take_care_plant_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,831, 1);
				api_quest_SetJournalStep( pRoom, userObjID,831, 1);
				api_quest_SetQuestStep( pRoom, userObjID,831, 1);
				npc_talk_index = "n713_soceress_tamara-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1403, 5);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201403, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 1401, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201401, 30000);
end

</GameServer>