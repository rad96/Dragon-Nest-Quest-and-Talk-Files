<VillageServer>

function sq_2122_jumping_event_3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 101 then
		sq_2122_jumping_event_3_OnTalk_n101_event_tracey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n101_event_tracey--------------------------------------------------------------------------------
function sq_2122_jumping_event_3_OnTalk_n101_event_tracey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
					 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21220, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21220, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21220, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21220, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21220, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21220, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21220, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21220, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21220, false);
			 end 

			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n101_event_tracey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n101_event_tracey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n101_event_tracey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n101_event_tracey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2122, 1);
				api_quest_SetJournalStep(userObjID,2122, 1);
				api_quest_SetQuestStep(userObjID,2122, 1);
				npc_talk_index = "n101_event_tracey-1";

	end
	if npc_talk_index == "n101_event_tracey-1-b" then 
	end
	if npc_talk_index == "n101_event_tracey-1-c" then 
	end
	if npc_talk_index == "n101_event_tracey-1-d" then 
	end
	if npc_talk_index == "n101_event_tracey-1-e" then 
	end
	if npc_talk_index == "n101_event_tracey-1-f" then 
	end
	if npc_talk_index == "n101_event_tracey-1-g" then 
	end
	if npc_talk_index == "n101_event_tracey-1-h" then 
	end
	if npc_talk_index == "n101_event_tracey-1-skill_set" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_ui_OpenSkillShop(userObjID);
	end
	if npc_talk_index == "n101_event_tracey-2-b" then 
	end
	if npc_talk_index == "n101_event_tracey-2-c" then 
	end
	if npc_talk_index == "n101_event_tracey-2-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21220, true);
				 api_quest_RewardQuestUser(userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21220, true);
				 api_quest_RewardQuestUser(userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21220, true);
				 api_quest_RewardQuestUser(userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21220, true);
				 api_quest_RewardQuestUser(userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21220, true);
				 api_quest_RewardQuestUser(userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21220, true);
				 api_quest_RewardQuestUser(userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21220, true);
				 api_quest_RewardQuestUser(userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21220, true);
				 api_quest_RewardQuestUser(userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21220, true);
				 api_quest_RewardQuestUser(userObjID, 21220, questID, 1);
			 end 
	end
	if npc_talk_index == "n101_event_tracey-2-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 2123, 1);
					api_quest_SetQuestStep(userObjID, 2123, 1);
					api_quest_SetJournalStep(userObjID, 2123, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2122_jumping_event_3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2122);
end

function sq_2122_jumping_event_3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2122);
end

function sq_2122_jumping_event_3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2122);
	local questID=2122;
end

function sq_2122_jumping_event_3_OnRemoteStart( userObjID, questID )
end

function sq_2122_jumping_event_3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_2122_jumping_event_3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2122, 1);
				api_quest_SetJournalStep(userObjID,2122, 1);
				api_quest_SetQuestStep(userObjID,2122, 1);
				npc_talk_index = "n101_event_tracey-1";
end

</VillageServer>

<GameServer>
function sq_2122_jumping_event_3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 101 then
		sq_2122_jumping_event_3_OnTalk_n101_event_tracey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n101_event_tracey--------------------------------------------------------------------------------
function sq_2122_jumping_event_3_OnTalk_n101_event_tracey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
					 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, false);
			 end 

			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n101_event_tracey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n101_event_tracey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n101_event_tracey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n101_event_tracey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2122, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2122, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2122, 1);
				npc_talk_index = "n101_event_tracey-1";

	end
	if npc_talk_index == "n101_event_tracey-1-b" then 
	end
	if npc_talk_index == "n101_event_tracey-1-c" then 
	end
	if npc_talk_index == "n101_event_tracey-1-d" then 
	end
	if npc_talk_index == "n101_event_tracey-1-e" then 
	end
	if npc_talk_index == "n101_event_tracey-1-f" then 
	end
	if npc_talk_index == "n101_event_tracey-1-g" then 
	end
	if npc_talk_index == "n101_event_tracey-1-h" then 
	end
	if npc_talk_index == "n101_event_tracey-1-skill_set" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_ui_OpenSkillShop( pRoom, userObjID);
	end
	if npc_talk_index == "n101_event_tracey-2-b" then 
	end
	if npc_talk_index == "n101_event_tracey-2-c" then 
	end
	if npc_talk_index == "n101_event_tracey-2-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21220, questID, 1);
			 end 
	end
	if npc_talk_index == "n101_event_tracey-2-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 2123, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 2123, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 2123, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2122_jumping_event_3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2122);
end

function sq_2122_jumping_event_3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2122);
end

function sq_2122_jumping_event_3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2122);
	local questID=2122;
end

function sq_2122_jumping_event_3_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_2122_jumping_event_3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_2122_jumping_event_3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2122, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2122, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2122, 1);
				npc_talk_index = "n101_event_tracey-1";
end

</GameServer>