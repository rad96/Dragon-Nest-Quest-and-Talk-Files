<VillageServer>

function sq08_242_change1_aca_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 392 then
		sq08_242_change1_aca_OnTalk_n392_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1217 then
		sq08_242_change1_aca_OnTalk_n1217_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1218 then
		sq08_242_change1_aca_OnTalk_n1218_jasmin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1219 then
		sq08_242_change1_aca_OnTalk_n1219_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n392_academic_station--------------------------------------------------------------------------------
function sq08_242_change1_aca_OnTalk_n392_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n392_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n392_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n392_academic_station-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n392_academic_station-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n392_academic_station-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n392_academic_station-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2420, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2420, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2420, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2420, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2420, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2420, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2420, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2420, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2420, false);
			 end 

	end
	if npc_talk_index == "n392_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,242, 1);
				api_quest_SetJournalStep(userObjID,242, 1);
				api_quest_SetQuestStep(userObjID,242, 1);
				npc_talk_index = "n392_academic_station-1";

	end
	if npc_talk_index == "n392_academic_station-1-check_comp" then 
				if api_user_GetUserJobID(userObjID) == 46  then
									npc_talk_index = "n392_academic_station-1-g";

				else
									if api_user_GetUserJobID(userObjID) == 49  then
									npc_talk_index = "n392_academic_station-1-g";

				else
									if api_user_GetUserJobID(userObjID) == 47  then
									npc_talk_index = "n392_academic_station-1-g";

				else
									if api_user_GetUserJobID(userObjID) == 48  then
									npc_talk_index = "n392_academic_station-1-g";

				else
									if api_user_GetUserJobID(userObjID) == 50  then
									npc_talk_index = "n392_academic_station-1-g";

				else
									if api_user_GetUserJobID(userObjID) == 51  then
									npc_talk_index = "n392_academic_station-1-g";

				else
									npc_talk_index = "n392_academic_station-1-h";

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n392_academic_station-1-b" then 
	end
	if npc_talk_index == "n392_academic_station-1-c" then 
	end
	if npc_talk_index == "n392_academic_station-1-d" then 
	end
	if npc_talk_index == "n392_academic_station-1-e" then 
	end
	if npc_talk_index == "n392_academic_station-1-party_check01" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_user_ChangeMap(userObjID,13520,4);

				else
									npc_talk_index = "n392_academic_station-1-f";

				end
	end
	if npc_talk_index == "n392_academic_station-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end
	if npc_talk_index == "n392_academic_station-1-a" then 
	end
	if npc_talk_index == "n392_academic_station-2-party_check2" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_user_ChangeMap(userObjID,13520,4);

				else
									npc_talk_index = "n392_academic_station-2-b";

				end
	end
	if npc_talk_index == "n392_academic_station-4-partty_check5" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_user_ChangeMap(userObjID,13520,5);

				else
									npc_talk_index = "n392_academic_station-4-b";

				end
	end
	if npc_talk_index == "n392_academic_station-6-b" then 
	end
	if npc_talk_index == "n392_academic_station-6-c" then 
				if api_user_GetUserJobID(userObjID) == 46  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2421, true);
				 api_quest_RewardQuestUser(userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2421, true);
				 api_quest_RewardQuestUser(userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2421, true);
				 api_quest_RewardQuestUser(userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2421, true);
				 api_quest_RewardQuestUser(userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2421, true);
				 api_quest_RewardQuestUser(userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2421, true);
				 api_quest_RewardQuestUser(userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2421, true);
				 api_quest_RewardQuestUser(userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2421, true);
				 api_quest_RewardQuestUser(userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2421, true);
				 api_quest_RewardQuestUser(userObjID, 2421, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2422, true);
				 api_quest_RewardQuestUser(userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2422, true);
				 api_quest_RewardQuestUser(userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2422, true);
				 api_quest_RewardQuestUser(userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2422, true);
				 api_quest_RewardQuestUser(userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2422, true);
				 api_quest_RewardQuestUser(userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2422, true);
				 api_quest_RewardQuestUser(userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2422, true);
				 api_quest_RewardQuestUser(userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2422, true);
				 api_quest_RewardQuestUser(userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2422, true);
				 api_quest_RewardQuestUser(userObjID, 2422, questID, 1);
			 end 

				end
	end
	if npc_talk_index == "n392_academic_station-6-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 379, 1);
					api_quest_SetQuestStep(userObjID, 379, 1);
					api_quest_SetJournalStep(userObjID, 379, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n392_academic_station-6-e" then 
	end
	if npc_talk_index == "n392_academic_station-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n392_academic_station-1", "sq08_379_change_job5.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1217_velskud--------------------------------------------------------------------------------
function sq08_242_change1_aca_OnTalk_n1217_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1217_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1217_velskud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1217_velskud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1217_velskud-3-b" then 
	end
	if npc_talk_index == "n1217_velskud-3-c" then 
	end
	if npc_talk_index == "n1217_velskud-3-d" then 
	end
	if npc_talk_index == "n1217_velskud-3-e" then 
	end
	if npc_talk_index == "n1217_velskud-3-f" then 
	end
	if npc_talk_index == "n1217_velskud-3-g" then 
	end
	if npc_talk_index == "n1217_velskud-3-h" then 
	end
	if npc_talk_index == "n1217_velskud-3-i" then 
	end
	if npc_talk_index == "n1217_velskud-3-j" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1218_jasmin--------------------------------------------------------------------------------
function sq08_242_change1_aca_OnTalk_n1218_jasmin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1218_jasmin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1218_jasmin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1218_jasmin-2-b" then 
	end
	if npc_talk_index == "n1218_jasmin-2-c" then 
	end
	if npc_talk_index == "n1218_jasmin-2-d" then 
	end
	if npc_talk_index == "n1218_jasmin-2-e" then 
	end
	if npc_talk_index == "n1218_jasmin-2-f" then 
	end
	if npc_talk_index == "n1218_jasmin-2-g" then 
	end
	if npc_talk_index == "n1218_jasmin-2-h" then 
	end
	if npc_talk_index == "n1218_jasmin-2-i" then 
	end
	if npc_talk_index == "n1218_jasmin-2-j" then 
	end
	if npc_talk_index == "n1218_jasmin-2-k" then 
	end
	if npc_talk_index == "n1218_jasmin-2-l" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1219_academic_station--------------------------------------------------------------------------------
function sq08_242_change1_aca_OnTalk_n1219_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1219_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1219_academic_station-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1219_academic_station-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1219_academic_station-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1219_academic_station-4-open_ui4" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1219_academic_station-5-open_ui5" then 
				api_ui_OpenJobChange(userObjID)
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_242_change1_aca_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 242);
end

function sq08_242_change1_aca_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 242);
end

function sq08_242_change1_aca_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 242);
	local questID=242;
end

function sq08_242_change1_aca_OnRemoteStart( userObjID, questID )
end

function sq08_242_change1_aca_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_242_change1_aca_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,242, 1);
				api_quest_SetJournalStep(userObjID,242, 1);
				api_quest_SetQuestStep(userObjID,242, 1);
				npc_talk_index = "n392_academic_station-1";
end

</VillageServer>

<GameServer>
function sq08_242_change1_aca_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 392 then
		sq08_242_change1_aca_OnTalk_n392_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1217 then
		sq08_242_change1_aca_OnTalk_n1217_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1218 then
		sq08_242_change1_aca_OnTalk_n1218_jasmin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1219 then
		sq08_242_change1_aca_OnTalk_n1219_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n392_academic_station--------------------------------------------------------------------------------
function sq08_242_change1_aca_OnTalk_n392_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n392_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n392_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n392_academic_station-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n392_academic_station-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n392_academic_station-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n392_academic_station-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2420, false);
			 end 

	end
	if npc_talk_index == "n392_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,242, 1);
				api_quest_SetJournalStep( pRoom, userObjID,242, 1);
				api_quest_SetQuestStep( pRoom, userObjID,242, 1);
				npc_talk_index = "n392_academic_station-1";

	end
	if npc_talk_index == "n392_academic_station-1-check_comp" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 46  then
									npc_talk_index = "n392_academic_station-1-g";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 49  then
									npc_talk_index = "n392_academic_station-1-g";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 47  then
									npc_talk_index = "n392_academic_station-1-g";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 48  then
									npc_talk_index = "n392_academic_station-1-g";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 50  then
									npc_talk_index = "n392_academic_station-1-g";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 51  then
									npc_talk_index = "n392_academic_station-1-g";

				else
									npc_talk_index = "n392_academic_station-1-h";

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n392_academic_station-1-b" then 
	end
	if npc_talk_index == "n392_academic_station-1-c" then 
	end
	if npc_talk_index == "n392_academic_station-1-d" then 
	end
	if npc_talk_index == "n392_academic_station-1-e" then 
	end
	if npc_talk_index == "n392_academic_station-1-party_check01" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_user_ChangeMap( pRoom, userObjID,13520,4);

				else
									npc_talk_index = "n392_academic_station-1-f";

				end
	end
	if npc_talk_index == "n392_academic_station-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end
	if npc_talk_index == "n392_academic_station-1-a" then 
	end
	if npc_talk_index == "n392_academic_station-2-party_check2" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_user_ChangeMap( pRoom, userObjID,13520,4);

				else
									npc_talk_index = "n392_academic_station-2-b";

				end
	end
	if npc_talk_index == "n392_academic_station-4-partty_check5" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_user_ChangeMap( pRoom, userObjID,13520,5);

				else
									npc_talk_index = "n392_academic_station-4-b";

				end
	end
	if npc_talk_index == "n392_academic_station-6-b" then 
	end
	if npc_talk_index == "n392_academic_station-6-c" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 46  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2421, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2421, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2421, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2421, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2421, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2421, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2421, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2421, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2421, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2421, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2421, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2422, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2422, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2422, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2422, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2422, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2422, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2422, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2422, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2422, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2422, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2422, questID, 1);
			 end 

				end
	end
	if npc_talk_index == "n392_academic_station-6-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 379, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 379, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 379, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n392_academic_station-6-e" then 
	end
	if npc_talk_index == "n392_academic_station-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n392_academic_station-1", "sq08_379_change_job5.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1217_velskud--------------------------------------------------------------------------------
function sq08_242_change1_aca_OnTalk_n1217_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1217_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1217_velskud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1217_velskud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1217_velskud-3-b" then 
	end
	if npc_talk_index == "n1217_velskud-3-c" then 
	end
	if npc_talk_index == "n1217_velskud-3-d" then 
	end
	if npc_talk_index == "n1217_velskud-3-e" then 
	end
	if npc_talk_index == "n1217_velskud-3-f" then 
	end
	if npc_talk_index == "n1217_velskud-3-g" then 
	end
	if npc_talk_index == "n1217_velskud-3-h" then 
	end
	if npc_talk_index == "n1217_velskud-3-i" then 
	end
	if npc_talk_index == "n1217_velskud-3-j" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1218_jasmin--------------------------------------------------------------------------------
function sq08_242_change1_aca_OnTalk_n1218_jasmin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1218_jasmin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1218_jasmin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1218_jasmin-2-b" then 
	end
	if npc_talk_index == "n1218_jasmin-2-c" then 
	end
	if npc_talk_index == "n1218_jasmin-2-d" then 
	end
	if npc_talk_index == "n1218_jasmin-2-e" then 
	end
	if npc_talk_index == "n1218_jasmin-2-f" then 
	end
	if npc_talk_index == "n1218_jasmin-2-g" then 
	end
	if npc_talk_index == "n1218_jasmin-2-h" then 
	end
	if npc_talk_index == "n1218_jasmin-2-i" then 
	end
	if npc_talk_index == "n1218_jasmin-2-j" then 
	end
	if npc_talk_index == "n1218_jasmin-2-k" then 
	end
	if npc_talk_index == "n1218_jasmin-2-l" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1219_academic_station--------------------------------------------------------------------------------
function sq08_242_change1_aca_OnTalk_n1219_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1219_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1219_academic_station-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1219_academic_station-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1219_academic_station-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1219_academic_station-4-open_ui4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1219_academic_station-5-open_ui5" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_242_change1_aca_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 242);
end

function sq08_242_change1_aca_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 242);
end

function sq08_242_change1_aca_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 242);
	local questID=242;
end

function sq08_242_change1_aca_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_242_change1_aca_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_242_change1_aca_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,242, 1);
				api_quest_SetJournalStep( pRoom, userObjID,242, 1);
				api_quest_SetQuestStep( pRoom, userObjID,242, 1);
				npc_talk_index = "n392_academic_station-1";
end

</GameServer>