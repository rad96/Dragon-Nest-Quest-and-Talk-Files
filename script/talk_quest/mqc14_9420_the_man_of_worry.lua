<VillageServer>

function mqc14_9420_the_man_of_worry_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1526 then
		mqc14_9420_the_man_of_worry_OnTalk_n1526_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9420_the_man_of_worry_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1534 then
		mqc14_9420_the_man_of_worry_OnTalk_n1534_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1535 then
		mqc14_9420_the_man_of_worry_OnTalk_n1535_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1536 then
		mqc14_9420_the_man_of_worry_OnTalk_n1536_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1537 then
		mqc14_9420_the_man_of_worry_OnTalk_n1537_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1526_for_memory--------------------------------------------------------------------------------
function mqc14_9420_the_man_of_worry_OnTalk_n1526_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1526_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9420_the_man_of_worry_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9420, 2);
				api_quest_SetJournalStep(userObjID,9420, 1);
				api_quest_SetQuestStep(userObjID,9420, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1534_for_memory--------------------------------------------------------------------------------
function mqc14_9420_the_man_of_worry_OnTalk_n1534_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1534_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1534_for_memory-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1534_for_memory-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1534_for_memory-1-b" then 
	end
	if npc_talk_index == "n1534_for_memory-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1535_meriendel--------------------------------------------------------------------------------
function mqc14_9420_the_man_of_worry_OnTalk_n1535_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1535_meriendel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1535_meriendel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1535_meriendel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1535_meriendel-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1535_meriendel-2-b" then 
	end
	if npc_talk_index == "n1535_meriendel-2-c" then 
	end
	if npc_talk_index == "n1535_meriendel-2-d" then 
	end
	if npc_talk_index == "n1535_meriendel-2-d" then 
	end
	if npc_talk_index == "n1535_meriendel-2-d" then 
	end
	if npc_talk_index == "n1535_meriendel-2-e" then 
	end
	if npc_talk_index == "n1535_meriendel-2-f" then 
	end
	if npc_talk_index == "n1535_meriendel-2-g" then 
	end
	if npc_talk_index == "n1535_meriendel-2-h" then 
	end
	if npc_talk_index == "n1535_meriendel-2-i" then 
	end
	if npc_talk_index == "n1535_meriendel-2-j" then 
	end
	if npc_talk_index == "n1535_meriendel-2-k" then 
	end
	if npc_talk_index == "n1535_meriendel-2-l" then 
	end
	if npc_talk_index == "n1535_meriendel-2-m" then 
	end
	if npc_talk_index == "n1535_meriendel-2-m" then 
	end
	if npc_talk_index == "n1535_meriendel-2-m" then 
	end
	if npc_talk_index == "n1535_meriendel-2-n" then 
	end
	if npc_talk_index == "n1535_meriendel-2-n" then 
	end
	if npc_talk_index == "n1535_meriendel-2-n" then 
	end
	if npc_talk_index == "n1535_meriendel-2-o" then 
	end
	if npc_talk_index == "n1535_meriendel-2-p" then 
	end
	if npc_talk_index == "n1535_meriendel-2-q" then 
	end
	if npc_talk_index == "n1535_meriendel-2-r" then 
	end
	if npc_talk_index == "n1535_meriendel-2-s" then 
	end
	if npc_talk_index == "n1535_meriendel-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1536_pether--------------------------------------------------------------------------------
function mqc14_9420_the_man_of_worry_OnTalk_n1536_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1536_pether-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1536_pether-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1536_pether-4-b" then 
	end
	if npc_talk_index == "n1536_pether-4-c" then 
	end
	if npc_talk_index == "n1536_pether-4-d" then 
	end
	if npc_talk_index == "n1536_pether-4-e" then 
	end
	if npc_talk_index == "n1536_pether-4-f" then 
	end
	if npc_talk_index == "n1536_pether-4-g" then 
	end
	if npc_talk_index == "n1536_pether-4-h" then 
	end
	if npc_talk_index == "n1536_pether-4-i" then 
	end
	if npc_talk_index == "n1536_pether-4-j" then 
	end
	if npc_talk_index == "n1536_pether-4-k" then 
	end
	if npc_talk_index == "n1536_pether-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1537_gereint_kid--------------------------------------------------------------------------------
function mqc14_9420_the_man_of_worry_OnTalk_n1537_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1537_gereint_kid-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1537_gereint_kid-6-b" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-c" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-d" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-e" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-f" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-g" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-h" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-i" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-j" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-k" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-l" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94201, true);
				 api_quest_RewardQuestUser(userObjID, 94201, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94202, true);
				 api_quest_RewardQuestUser(userObjID, 94202, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94203, true);
				 api_quest_RewardQuestUser(userObjID, 94203, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94204, true);
				 api_quest_RewardQuestUser(userObjID, 94204, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94205, true);
				 api_quest_RewardQuestUser(userObjID, 94205, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94206, true);
				 api_quest_RewardQuestUser(userObjID, 94206, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94207, true);
				 api_quest_RewardQuestUser(userObjID, 94207, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94208, true);
				 api_quest_RewardQuestUser(userObjID, 94208, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94209, true);
				 api_quest_RewardQuestUser(userObjID, 94209, questID, 1);
			 end 
	end
	if npc_talk_index == "n1537_gereint_kid-6-m" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9421, 2);
					api_quest_SetQuestStep(userObjID, 9421, 1);
					api_quest_SetJournalStep(userObjID, 9421, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1537_gereint_kid-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1537_gereint_kid-1", "mqc14_9421_swallowed_word.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9420_the_man_of_worry_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9420);
end

function mqc14_9420_the_man_of_worry_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9420);
end

function mqc14_9420_the_man_of_worry_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9420);
	local questID=9420;
end

function mqc14_9420_the_man_of_worry_OnRemoteStart( userObjID, questID )
end

function mqc14_9420_the_man_of_worry_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc14_9420_the_man_of_worry_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9420, 2);
				api_quest_SetJournalStep(userObjID,9420, 1);
				api_quest_SetQuestStep(userObjID,9420, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</VillageServer>

<GameServer>
function mqc14_9420_the_man_of_worry_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1526 then
		mqc14_9420_the_man_of_worry_OnTalk_n1526_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9420_the_man_of_worry_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1534 then
		mqc14_9420_the_man_of_worry_OnTalk_n1534_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1535 then
		mqc14_9420_the_man_of_worry_OnTalk_n1535_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1536 then
		mqc14_9420_the_man_of_worry_OnTalk_n1536_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1537 then
		mqc14_9420_the_man_of_worry_OnTalk_n1537_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1526_for_memory--------------------------------------------------------------------------------
function mqc14_9420_the_man_of_worry_OnTalk_n1526_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1526_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9420_the_man_of_worry_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9420, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9420, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9420, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1534_for_memory--------------------------------------------------------------------------------
function mqc14_9420_the_man_of_worry_OnTalk_n1534_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1534_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1534_for_memory-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1534_for_memory-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1534_for_memory-1-b" then 
	end
	if npc_talk_index == "n1534_for_memory-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1535_meriendel--------------------------------------------------------------------------------
function mqc14_9420_the_man_of_worry_OnTalk_n1535_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1535_meriendel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1535_meriendel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1535_meriendel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1535_meriendel-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1535_meriendel-2-b" then 
	end
	if npc_talk_index == "n1535_meriendel-2-c" then 
	end
	if npc_talk_index == "n1535_meriendel-2-d" then 
	end
	if npc_talk_index == "n1535_meriendel-2-d" then 
	end
	if npc_talk_index == "n1535_meriendel-2-d" then 
	end
	if npc_talk_index == "n1535_meriendel-2-e" then 
	end
	if npc_talk_index == "n1535_meriendel-2-f" then 
	end
	if npc_talk_index == "n1535_meriendel-2-g" then 
	end
	if npc_talk_index == "n1535_meriendel-2-h" then 
	end
	if npc_talk_index == "n1535_meriendel-2-i" then 
	end
	if npc_talk_index == "n1535_meriendel-2-j" then 
	end
	if npc_talk_index == "n1535_meriendel-2-k" then 
	end
	if npc_talk_index == "n1535_meriendel-2-l" then 
	end
	if npc_talk_index == "n1535_meriendel-2-m" then 
	end
	if npc_talk_index == "n1535_meriendel-2-m" then 
	end
	if npc_talk_index == "n1535_meriendel-2-m" then 
	end
	if npc_talk_index == "n1535_meriendel-2-n" then 
	end
	if npc_talk_index == "n1535_meriendel-2-n" then 
	end
	if npc_talk_index == "n1535_meriendel-2-n" then 
	end
	if npc_talk_index == "n1535_meriendel-2-o" then 
	end
	if npc_talk_index == "n1535_meriendel-2-p" then 
	end
	if npc_talk_index == "n1535_meriendel-2-q" then 
	end
	if npc_talk_index == "n1535_meriendel-2-r" then 
	end
	if npc_talk_index == "n1535_meriendel-2-s" then 
	end
	if npc_talk_index == "n1535_meriendel-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1536_pether--------------------------------------------------------------------------------
function mqc14_9420_the_man_of_worry_OnTalk_n1536_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1536_pether-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1536_pether-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1536_pether-4-b" then 
	end
	if npc_talk_index == "n1536_pether-4-c" then 
	end
	if npc_talk_index == "n1536_pether-4-d" then 
	end
	if npc_talk_index == "n1536_pether-4-e" then 
	end
	if npc_talk_index == "n1536_pether-4-f" then 
	end
	if npc_talk_index == "n1536_pether-4-g" then 
	end
	if npc_talk_index == "n1536_pether-4-h" then 
	end
	if npc_talk_index == "n1536_pether-4-i" then 
	end
	if npc_talk_index == "n1536_pether-4-j" then 
	end
	if npc_talk_index == "n1536_pether-4-k" then 
	end
	if npc_talk_index == "n1536_pether-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1537_gereint_kid--------------------------------------------------------------------------------
function mqc14_9420_the_man_of_worry_OnTalk_n1537_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1537_gereint_kid-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1537_gereint_kid-6-b" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-c" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-d" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-e" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-f" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-g" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-h" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-i" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-j" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-k" then 
	end
	if npc_talk_index == "n1537_gereint_kid-6-l" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94201, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94201, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94202, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94202, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94203, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94203, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94204, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94204, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94205, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94205, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94206, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94206, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94207, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94207, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94208, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94208, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94209, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94209, questID, 1);
			 end 
	end
	if npc_talk_index == "n1537_gereint_kid-6-m" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9421, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9421, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9421, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1537_gereint_kid-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1537_gereint_kid-1", "mqc14_9421_swallowed_word.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9420_the_man_of_worry_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9420);
end

function mqc14_9420_the_man_of_worry_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9420);
end

function mqc14_9420_the_man_of_worry_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9420);
	local questID=9420;
end

function mqc14_9420_the_man_of_worry_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc14_9420_the_man_of_worry_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc14_9420_the_man_of_worry_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9420, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9420, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9420, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</GameServer>