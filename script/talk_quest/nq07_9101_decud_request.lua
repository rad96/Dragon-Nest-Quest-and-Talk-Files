<VillageServer>

function nq07_9101_decud_request_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 24 then
		nq07_9101_decud_request_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function nq07_9101_decud_request_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-accptted" then
				api_quest_AddQuest(userObjID,9101, 1);
				api_quest_SetJournalStep(userObjID,9101, 1);
				api_quest_SetQuestStep(userObjID,9101, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-g" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 24, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq07_9101_decud_request_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9101);
end

function nq07_9101_decud_request_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9101);
end

function nq07_9101_decud_request_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9101);
	local questID=9101;
end

function nq07_9101_decud_request_OnRemoteStart( userObjID, questID )
end

function nq07_9101_decud_request_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq07_9101_decud_request_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq07_9101_decud_request_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 24 then
		nq07_9101_decud_request_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function nq07_9101_decud_request_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-accptted" then
				api_quest_AddQuest( pRoom, userObjID,9101, 1);
				api_quest_SetJournalStep( pRoom, userObjID,9101, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9101, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-g" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 24, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq07_9101_decud_request_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9101);
end

function nq07_9101_decud_request_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9101);
end

function nq07_9101_decud_request_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9101);
	local questID=9101;
end

function nq07_9101_decud_request_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq07_9101_decud_request_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq07_9101_decud_request_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>