<VillageServer>

function mq34_9298_the_power_of_the_prophet_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1067 then
		mq34_9298_the_power_of_the_prophet_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1090 then
		mq34_9298_the_power_of_the_prophet_OnTalk_n1090_triana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1133 then
		mq34_9298_the_power_of_the_prophet_OnTalk_n1133_rojalin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1134 then
		mq34_9298_the_power_of_the_prophet_OnTalk_n1034_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9298_the_power_of_the_prophet_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1067_elder_elf-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1067_elder_elf-6";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92981, true);
				 api_quest_RewardQuestUser(userObjID, 92981, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92981, true);
				 api_quest_RewardQuestUser(userObjID, 92981, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92983, true);
				 api_quest_RewardQuestUser(userObjID, 92983, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92984, true);
				 api_quest_RewardQuestUser(userObjID, 92984, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92985, true);
				 api_quest_RewardQuestUser(userObjID, 92985, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92986, true);
				 api_quest_RewardQuestUser(userObjID, 92986, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92987, true);
				 api_quest_RewardQuestUser(userObjID, 92987, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92988, true);
				 api_quest_RewardQuestUser(userObjID, 92988, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92989, true);
				 api_quest_RewardQuestUser(userObjID, 92989, questID, 1);
			 end 
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-!next" then
				api_quest_AddQuest(userObjID,9298, 2);
				api_quest_SetJournalStep(userObjID,9298, 1);
				api_quest_SetQuestStep(userObjID,9298, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-party_check" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n1067_elder_elf-1-a";

				else
									npc_talk_index = "n1067_elder_elf-1-b";

				end
	end
	if npc_talk_index == "n1067_elder_elf-6-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92981, true);
				 api_quest_RewardQuestUser(userObjID, 92981, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92981, true);
				 api_quest_RewardQuestUser(userObjID, 92981, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92983, true);
				 api_quest_RewardQuestUser(userObjID, 92983, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92984, true);
				 api_quest_RewardQuestUser(userObjID, 92984, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92985, true);
				 api_quest_RewardQuestUser(userObjID, 92985, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92986, true);
				 api_quest_RewardQuestUser(userObjID, 92986, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92987, true);
				 api_quest_RewardQuestUser(userObjID, 92987, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92988, true);
				 api_quest_RewardQuestUser(userObjID, 92988, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92989, true);
				 api_quest_RewardQuestUser(userObjID, 92989, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-6-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1090_triana--------------------------------------------------------------------------------
function mq34_9298_the_power_of_the_prophet_OnTalk_n1090_triana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1090_triana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1090_triana-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1090_triana-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1090_triana-5-job5_2" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1090_triana-5-a";

				else
									npc_talk_index = "n1090_triana-5-b";

				end
	end
	if npc_talk_index == "n1090_triana-5-c" then 
	end
	if npc_talk_index == "n1090_triana-5-c" then 
	end
	if npc_talk_index == "n1090_triana-5-d" then 
	end
	if npc_talk_index == "n1090_triana-5-job5_3" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1090_triana-5-e";

				else
									npc_talk_index = "n1090_triana-5-f";

				end
	end
	if npc_talk_index == "n1090_triana-5-g" then 
	end
	if npc_talk_index == "n1090_triana-5-g" then 
	end
	if npc_talk_index == "n1090_triana-5-job5_4" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1090_triana-5-h";

				else
									npc_talk_index = "n1090_triana-5-i";

				end
	end
	if npc_talk_index == "n1090_triana-5-j" then 
	end
	if npc_talk_index == "n1090_triana-5-j" then 
	end
	if npc_talk_index == "n1090_triana-5-job5_5" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1090_triana-5-k";

				else
									npc_talk_index = "n1090_triana-5-m";

				end
	end
	if npc_talk_index == "n1090_triana-5-l" then 
	end
	if npc_talk_index == "n1090_triana-5-p" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end
	if npc_talk_index == "n1090_triana-5-n" then 
	end
	if npc_talk_index == "n1090_triana-5-o" then 
	end
	if npc_talk_index == "n1090_triana-5-p" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1133_rojalin--------------------------------------------------------------------------------
function mq34_9298_the_power_of_the_prophet_OnTalk_n1133_rojalin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1133_rojalin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1133_rojalin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1133_rojalin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1133_rojalin-3-b" then 
	end
	if npc_talk_index == "n1133_rojalin-3-c" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1034_argenta--------------------------------------------------------------------------------
function mq34_9298_the_power_of_the_prophet_OnTalk_n1034_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1034_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1034_argenta-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1034_argenta-4-b" then 
	end
	if npc_talk_index == "n1034_argenta-4-c" then 
	end
	if npc_talk_index == "n1034_argenta-4-d" then 
	end
	if npc_talk_index == "n1034_argenta-4-e" then 
	end
	if npc_talk_index == "n1034_argenta-4-f" then 
	end
	if npc_talk_index == "n1034_argenta-4-g" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9298_the_power_of_the_prophet_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9298);
end

function mq34_9298_the_power_of_the_prophet_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9298);
end

function mq34_9298_the_power_of_the_prophet_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9298);
	local questID=9298;
end

function mq34_9298_the_power_of_the_prophet_OnRemoteStart( userObjID, questID )
end

function mq34_9298_the_power_of_the_prophet_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9298_the_power_of_the_prophet_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mq34_9298_the_power_of_the_prophet_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1067 then
		mq34_9298_the_power_of_the_prophet_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1090 then
		mq34_9298_the_power_of_the_prophet_OnTalk_n1090_triana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1133 then
		mq34_9298_the_power_of_the_prophet_OnTalk_n1133_rojalin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1134 then
		mq34_9298_the_power_of_the_prophet_OnTalk_n1034_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9298_the_power_of_the_prophet_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1067_elder_elf-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1067_elder_elf-6";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92981, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92981, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92981, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92981, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92983, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92983, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92984, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92984, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92985, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92985, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92986, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92986, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92987, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92987, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92988, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92988, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92989, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92989, questID, 1);
			 end 
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-!next" then
				api_quest_AddQuest( pRoom, userObjID,9298, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9298, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9298, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-party_check" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n1067_elder_elf-1-a";

				else
									npc_talk_index = "n1067_elder_elf-1-b";

				end
	end
	if npc_talk_index == "n1067_elder_elf-6-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92981, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92981, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92981, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92981, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92983, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92983, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92984, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92984, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92985, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92985, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92986, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92986, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92987, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92987, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92988, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92988, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92989, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92989, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-6-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1090_triana--------------------------------------------------------------------------------
function mq34_9298_the_power_of_the_prophet_OnTalk_n1090_triana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1090_triana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1090_triana-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1090_triana-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1090_triana-5-job5_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1090_triana-5-a";

				else
									npc_talk_index = "n1090_triana-5-b";

				end
	end
	if npc_talk_index == "n1090_triana-5-c" then 
	end
	if npc_talk_index == "n1090_triana-5-c" then 
	end
	if npc_talk_index == "n1090_triana-5-d" then 
	end
	if npc_talk_index == "n1090_triana-5-job5_3" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1090_triana-5-e";

				else
									npc_talk_index = "n1090_triana-5-f";

				end
	end
	if npc_talk_index == "n1090_triana-5-g" then 
	end
	if npc_talk_index == "n1090_triana-5-g" then 
	end
	if npc_talk_index == "n1090_triana-5-job5_4" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1090_triana-5-h";

				else
									npc_talk_index = "n1090_triana-5-i";

				end
	end
	if npc_talk_index == "n1090_triana-5-j" then 
	end
	if npc_talk_index == "n1090_triana-5-j" then 
	end
	if npc_talk_index == "n1090_triana-5-job5_5" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1090_triana-5-k";

				else
									npc_talk_index = "n1090_triana-5-m";

				end
	end
	if npc_talk_index == "n1090_triana-5-l" then 
	end
	if npc_talk_index == "n1090_triana-5-p" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end
	if npc_talk_index == "n1090_triana-5-n" then 
	end
	if npc_talk_index == "n1090_triana-5-o" then 
	end
	if npc_talk_index == "n1090_triana-5-p" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1133_rojalin--------------------------------------------------------------------------------
function mq34_9298_the_power_of_the_prophet_OnTalk_n1133_rojalin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1133_rojalin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1133_rojalin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1133_rojalin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1133_rojalin-3-b" then 
	end
	if npc_talk_index == "n1133_rojalin-3-c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1034_argenta--------------------------------------------------------------------------------
function mq34_9298_the_power_of_the_prophet_OnTalk_n1034_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1034_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1034_argenta-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1034_argenta-4-b" then 
	end
	if npc_talk_index == "n1034_argenta-4-c" then 
	end
	if npc_talk_index == "n1034_argenta-4-d" then 
	end
	if npc_talk_index == "n1034_argenta-4-e" then 
	end
	if npc_talk_index == "n1034_argenta-4-f" then 
	end
	if npc_talk_index == "n1034_argenta-4-g" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9298_the_power_of_the_prophet_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9298);
end

function mq34_9298_the_power_of_the_prophet_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9298);
end

function mq34_9298_the_power_of_the_prophet_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9298);
	local questID=9298;
end

function mq34_9298_the_power_of_the_prophet_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9298_the_power_of_the_prophet_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9298_the_power_of_the_prophet_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>