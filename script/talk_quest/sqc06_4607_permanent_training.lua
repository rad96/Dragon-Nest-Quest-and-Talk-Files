<VillageServer>

function sqc06_4607_permanent_training_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1704 then
		sqc06_4607_permanent_training_OnTalk_n1704_teramai_west(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1691 then
		sqc06_4607_permanent_training_OnTalk_n1691_eltia_lotus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1746 then
		sqc06_4607_permanent_training_OnTalk_n1746_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1747 then
		sqc06_4607_permanent_training_OnTalk_n1747_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1704_teramai_west--------------------------------------------------------------------------------
function sqc06_4607_permanent_training_OnTalk_n1704_teramai_west(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1704_teramai_west-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1691_eltia_lotus--------------------------------------------------------------------------------
function sqc06_4607_permanent_training_OnTalk_n1691_eltia_lotus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1691_eltia_lotus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1691_eltia_lotus-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1691_eltia_lotus-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1691_eltia_lotus-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46070, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46070, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46070, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46070, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46070, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46070, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46070, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46070, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46070, false);
			 end 

	end
	if npc_talk_index == "n1691_eltia_lotus-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4607, 1);
				api_quest_SetJournalStep(userObjID,4607, 1);
				api_quest_SetQuestStep(userObjID,4607, 1);
				npc_talk_index = "n1691_eltia_lotus-1";

	end
	if npc_talk_index == "n1691_eltia_lotus-5-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46070, true);
				 api_quest_RewardQuestUser(userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46070, true);
				 api_quest_RewardQuestUser(userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46070, true);
				 api_quest_RewardQuestUser(userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46070, true);
				 api_quest_RewardQuestUser(userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46070, true);
				 api_quest_RewardQuestUser(userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46070, true);
				 api_quest_RewardQuestUser(userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46070, true);
				 api_quest_RewardQuestUser(userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46070, true);
				 api_quest_RewardQuestUser(userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46070, true);
				 api_quest_RewardQuestUser(userObjID, 46070, questID, 1);
			 end 
	end
	if npc_talk_index == "n1691_eltia_lotus-5-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1746_teramai--------------------------------------------------------------------------------
function sqc06_4607_permanent_training_OnTalk_n1746_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1746_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1746_teramai-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1746_teramai-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1746_teramai-1-check_job" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									if api_user_GetUserLevel(userObjID) >= 45 then
									if api_user_GetUserJobID(userObjID) == 72   or api_user_GetUserJobID(userObjID) == 72  then
									npc_talk_index = "n1746_teramai-1-a";

				else
									if api_user_GetUserJobID(userObjID) == 73   or api_user_GetUserJobID(userObjID) == 74  then
									npc_talk_index = "n1746_teramai-1-e";

				else
									npc_talk_index = "n1746_teramai-1-d";

				end

				end

				else
									npc_talk_index = "n1746_teramai-1-d";

				end

				else
									npc_talk_index = "n1746_teramai-1-c";

				end
	end
	if npc_talk_index == "n1746_teramai-1-b" then 
	end
	if npc_talk_index == "n1746_teramai-1-move_1" then 
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13512,1);
	end
	if npc_talk_index == "n1746_teramai-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 6);
				npc_talk_index = "n1746_teramai-5";
	end
	if npc_talk_index == "n1746_teramai-2-movemap2" then 
				api_user_ChangeMap(userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1747_teramai--------------------------------------------------------------------------------
function sqc06_4607_permanent_training_OnTalk_n1747_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1747_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1747_teramai-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1747_teramai-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1747_teramai-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1747_teramai-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1747_teramai-1-b" then 
	end
	if npc_talk_index == "n1747_teramai-1-c" then 
	end
	if npc_talk_index == "n1747_teramai-1-d" then 
	end
	if npc_talk_index == "n1747_teramai-1-e" then 
	end
	if npc_talk_index == "n1747_teramai-1-f" then 
	end
	if npc_talk_index == "n1747_teramai-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1747_teramai-2-open_ui2" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1747_teramai-3-open_ui3" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1747_teramai-4-open_ui4" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1747_teramai-4-open_ui4" then 
				api_ui_OpenJobChange(userObjID)
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc06_4607_permanent_training_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4607);
end

function sqc06_4607_permanent_training_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4607);
end

function sqc06_4607_permanent_training_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4607);
	local questID=4607;
end

function sqc06_4607_permanent_training_OnRemoteStart( userObjID, questID )
end

function sqc06_4607_permanent_training_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc06_4607_permanent_training_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4607, 1);
				api_quest_SetJournalStep(userObjID,4607, 1);
				api_quest_SetQuestStep(userObjID,4607, 1);
				npc_talk_index = "n1691_eltia_lotus-1";
end

</VillageServer>

<GameServer>
function sqc06_4607_permanent_training_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1704 then
		sqc06_4607_permanent_training_OnTalk_n1704_teramai_west( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1691 then
		sqc06_4607_permanent_training_OnTalk_n1691_eltia_lotus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1746 then
		sqc06_4607_permanent_training_OnTalk_n1746_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1747 then
		sqc06_4607_permanent_training_OnTalk_n1747_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1704_teramai_west--------------------------------------------------------------------------------
function sqc06_4607_permanent_training_OnTalk_n1704_teramai_west( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1704_teramai_west-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1691_eltia_lotus--------------------------------------------------------------------------------
function sqc06_4607_permanent_training_OnTalk_n1691_eltia_lotus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1691_eltia_lotus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1691_eltia_lotus-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1691_eltia_lotus-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1691_eltia_lotus-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, false);
			 end 

	end
	if npc_talk_index == "n1691_eltia_lotus-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4607, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4607, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4607, 1);
				npc_talk_index = "n1691_eltia_lotus-1";

	end
	if npc_talk_index == "n1691_eltia_lotus-5-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46070, questID, 1);
			 end 
	end
	if npc_talk_index == "n1691_eltia_lotus-5-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1746_teramai--------------------------------------------------------------------------------
function sqc06_4607_permanent_training_OnTalk_n1746_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1746_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1746_teramai-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1746_teramai-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1746_teramai-1-check_job" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									if api_user_GetUserLevel( pRoom, userObjID) >= 45 then
									if api_user_GetUserJobID( pRoom, userObjID) == 72   or api_user_GetUserJobID( pRoom, userObjID) == 72  then
									npc_talk_index = "n1746_teramai-1-a";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 73   or api_user_GetUserJobID( pRoom, userObjID) == 74  then
									npc_talk_index = "n1746_teramai-1-e";

				else
									npc_talk_index = "n1746_teramai-1-d";

				end

				end

				else
									npc_talk_index = "n1746_teramai-1-d";

				end

				else
									npc_talk_index = "n1746_teramai-1-c";

				end
	end
	if npc_talk_index == "n1746_teramai-1-b" then 
	end
	if npc_talk_index == "n1746_teramai-1-move_1" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end
	if npc_talk_index == "n1746_teramai-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				npc_talk_index = "n1746_teramai-5";
	end
	if npc_talk_index == "n1746_teramai-2-movemap2" then 
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1747_teramai--------------------------------------------------------------------------------
function sqc06_4607_permanent_training_OnTalk_n1747_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1747_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1747_teramai-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1747_teramai-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1747_teramai-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1747_teramai-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1747_teramai-1-b" then 
	end
	if npc_talk_index == "n1747_teramai-1-c" then 
	end
	if npc_talk_index == "n1747_teramai-1-d" then 
	end
	if npc_talk_index == "n1747_teramai-1-e" then 
	end
	if npc_talk_index == "n1747_teramai-1-f" then 
	end
	if npc_talk_index == "n1747_teramai-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1747_teramai-2-open_ui2" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1747_teramai-3-open_ui3" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1747_teramai-4-open_ui4" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1747_teramai-4-open_ui4" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc06_4607_permanent_training_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4607);
end

function sqc06_4607_permanent_training_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4607);
end

function sqc06_4607_permanent_training_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4607);
	local questID=4607;
end

function sqc06_4607_permanent_training_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc06_4607_permanent_training_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc06_4607_permanent_training_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4607, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4607, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4607, 1);
				npc_talk_index = "n1691_eltia_lotus-1";
end

</GameServer>