<VillageServer>

function sqc17_4697_merchants_of_merca1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1871 then
		sqc17_4697_merchants_of_merca1_OnTalk_n1871_trader_april(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1871_trader_april--------------------------------------------------------------------------------
function sqc17_4697_merchants_of_merca1_OnTalk_n1871_trader_april(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1871_trader_april-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1871_trader_april-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1871_trader_april-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1871_trader_april-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1871_trader_april-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46970, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46970, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46970, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46970, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46970, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46970, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46970, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46970, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46970, false);
			 end 

	end
	if npc_talk_index == "n1871_trader_april-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4697, 1);
				api_quest_SetJournalStep(userObjID,4697, 1);
				api_quest_SetQuestStep(userObjID,4697, 1);
				npc_talk_index = "n1871_trader_april-1";

	end
	if npc_talk_index == "n1871_trader_april-1-b" then 
	end
	if npc_talk_index == "n1871_trader_april-1-c" then 
	end
	if npc_talk_index == "n1871_trader_april-1-d" then 
	end
	if npc_talk_index == "n1871_trader_april-1-e" then 
	end
	if npc_talk_index == "n1871_trader_april-1-f" then 
	end
	if npc_talk_index == "n1871_trader_april-1-g" then 
	end
	if npc_talk_index == "n1871_trader_april-1-h" then 
	end
	if npc_talk_index == "n1871_trader_april-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400485, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 603219, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 603220, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 603225, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 703219, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 703220, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 703225, 30000);
	end
	if npc_talk_index == "n1871_trader_april-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46970, true);
				 api_quest_RewardQuestUser(userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46970, true);
				 api_quest_RewardQuestUser(userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46970, true);
				 api_quest_RewardQuestUser(userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46970, true);
				 api_quest_RewardQuestUser(userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46970, true);
				 api_quest_RewardQuestUser(userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46970, true);
				 api_quest_RewardQuestUser(userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46970, true);
				 api_quest_RewardQuestUser(userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46970, true);
				 api_quest_RewardQuestUser(userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46970, true);
				 api_quest_RewardQuestUser(userObjID, 46970, questID, 1);
			 end 

				if api_quest_HasQuestItem(userObjID, 400485, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400485, api_quest_HasQuestItem(userObjID, 400485, 1));
				end
	end
	if npc_talk_index == "n1871_trader_april-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4698, 1);
					api_quest_SetQuestStep(userObjID, 4698, 1);
					api_quest_SetJournalStep(userObjID, 4698, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1871_trader_april-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1835_conscience_pope-1", "sqc17_4698_merchants_of_merca2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4697_merchants_of_merca1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4697);
	if qstep == 2 and CountIndex == 603219 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400485, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400485, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 603220 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400485, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400485, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 603225 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400485, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400485, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400485 then

	end
	if qstep == 2 and CountIndex == 703219 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400485, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400485, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 703220 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400485, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400485, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 703225 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400485, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400485, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sqc17_4697_merchants_of_merca1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4697);
	if qstep == 2 and CountIndex == 603219 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 603220 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 603225 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400485 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 703219 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 703220 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 703225 and Count >= TargetCount  then

	end
end

function sqc17_4697_merchants_of_merca1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4697);
	local questID=4697;
end

function sqc17_4697_merchants_of_merca1_OnRemoteStart( userObjID, questID )
end

function sqc17_4697_merchants_of_merca1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc17_4697_merchants_of_merca1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4697, 1);
				api_quest_SetJournalStep(userObjID,4697, 1);
				api_quest_SetQuestStep(userObjID,4697, 1);
				npc_talk_index = "n1871_trader_april-1";
end

</VillageServer>

<GameServer>
function sqc17_4697_merchants_of_merca1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1871 then
		sqc17_4697_merchants_of_merca1_OnTalk_n1871_trader_april( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1871_trader_april--------------------------------------------------------------------------------
function sqc17_4697_merchants_of_merca1_OnTalk_n1871_trader_april( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1871_trader_april-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1871_trader_april-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1871_trader_april-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1871_trader_april-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1871_trader_april-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, false);
			 end 

	end
	if npc_talk_index == "n1871_trader_april-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4697, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4697, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4697, 1);
				npc_talk_index = "n1871_trader_april-1";

	end
	if npc_talk_index == "n1871_trader_april-1-b" then 
	end
	if npc_talk_index == "n1871_trader_april-1-c" then 
	end
	if npc_talk_index == "n1871_trader_april-1-d" then 
	end
	if npc_talk_index == "n1871_trader_april-1-e" then 
	end
	if npc_talk_index == "n1871_trader_april-1-f" then 
	end
	if npc_talk_index == "n1871_trader_april-1-g" then 
	end
	if npc_talk_index == "n1871_trader_april-1-h" then 
	end
	if npc_talk_index == "n1871_trader_april-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400485, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 603219, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 603220, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 603225, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 703219, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 703220, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 703225, 30000);
	end
	if npc_talk_index == "n1871_trader_april-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46970, questID, 1);
			 end 

				if api_quest_HasQuestItem( pRoom, userObjID, 400485, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400485, api_quest_HasQuestItem( pRoom, userObjID, 400485, 1));
				end
	end
	if npc_talk_index == "n1871_trader_april-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4698, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4698, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4698, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1871_trader_april-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1835_conscience_pope-1", "sqc17_4698_merchants_of_merca2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4697_merchants_of_merca1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4697);
	if qstep == 2 and CountIndex == 603219 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400485, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400485, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 603220 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400485, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400485, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 603225 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400485, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400485, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400485 then

	end
	if qstep == 2 and CountIndex == 703219 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400485, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400485, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 703220 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400485, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400485, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 703225 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400485, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400485, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sqc17_4697_merchants_of_merca1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4697);
	if qstep == 2 and CountIndex == 603219 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 603220 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 603225 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400485 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 703219 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 703220 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 703225 and Count >= TargetCount  then

	end
end

function sqc17_4697_merchants_of_merca1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4697);
	local questID=4697;
end

function sqc17_4697_merchants_of_merca1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc17_4697_merchants_of_merca1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc17_4697_merchants_of_merca1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4697, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4697, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4697, 1);
				npc_talk_index = "n1871_trader_april-1";
end

</GameServer>