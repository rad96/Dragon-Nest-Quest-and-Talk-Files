<VillageServer>

function mqc15_9440_the_man_who_wish_dark_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1497 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1497_people(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1500 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1500_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1573 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1573_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1574 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1574_aruno(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1575 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1575_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1576 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1576_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1578 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1578_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1497_people--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1497_people(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1500_gaharam--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1500_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1500_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1500_gaharam-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1500_gaharam-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1573_gaharam--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1573_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1573_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1574_aruno--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1574_aruno(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1574_aruno-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1574_aruno-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1574_aruno-3-b" then 
	end
	if npc_talk_index == "n1574_aruno-3-b" then 
	end
	if npc_talk_index == "n1574_aruno-3-b" then 
	end
	if npc_talk_index == "n1574_aruno-3-c" then 
	end
	if npc_talk_index == "n1574_aruno-3-d" then 
	end
	if npc_talk_index == "n1574_aruno-3-e" then 
	end
	if npc_talk_index == "n1574_aruno-3-f" then 
	end
	if npc_talk_index == "n1574_aruno-3-g" then 
	end
	if npc_talk_index == "n1574_aruno-3-h" then 
	end
	if npc_talk_index == "n1574_aruno-3-i" then 
	end
	if npc_talk_index == "n1574_aruno-3-j" then 
	end
	if npc_talk_index == "n1574_aruno-3-k" then 
	end
	if npc_talk_index == "n1574_aruno-3-l" then 
	end
	if npc_talk_index == "n1574_aruno-3-m" then 
	end
	if npc_talk_index == "n1574_aruno-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1575_gaharam--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1575_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1575_gaharam-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1575_gaharam-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1575_gaharam-5-b" then 
	end
	if npc_talk_index == "n1575_gaharam-5-c" then 
	end
	if npc_talk_index == "n1575_gaharam-5-d" then 
	end
	if npc_talk_index == "n1575_gaharam-5-e" then 
	end
	if npc_talk_index == "n1575_gaharam-5-f" then 
	end
	if npc_talk_index == "n1575_gaharam-5-g" then 
	end
	if npc_talk_index == "n1575_gaharam-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1576_for_memory--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1576_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1578_for_memory--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1578_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1578_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1578_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1578_for_memory-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1578_for_memory-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9440, 2);
				api_quest_SetJournalStep(userObjID,9440, 1);
				api_quest_SetQuestStep(userObjID,9440, 1);
				npc_talk_index = "n1578_for_memory-1";

	end
	if npc_talk_index == "n1578_for_memory-7-b" then 
	end
	if npc_talk_index == "n1578_for_memory-7-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94400, true);
				 api_quest_RewardQuestUser(userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94400, true);
				 api_quest_RewardQuestUser(userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94400, true);
				 api_quest_RewardQuestUser(userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94400, true);
				 api_quest_RewardQuestUser(userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94400, true);
				 api_quest_RewardQuestUser(userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94400, true);
				 api_quest_RewardQuestUser(userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94400, true);
				 api_quest_RewardQuestUser(userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94400, true);
				 api_quest_RewardQuestUser(userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94400, true);
				 api_quest_RewardQuestUser(userObjID, 94400, questID, 1);
			 end 
	end
	if npc_talk_index == "n1578_for_memory-7-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9441, 2);
					api_quest_SetQuestStep(userObjID, 9441, 1);
					api_quest_SetJournalStep(userObjID, 9441, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1578_for_memory-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1578_for_memory-1", "mqc15_9441_empty_hope.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9440_the_man_who_wish_dark_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9440);
end

function mqc15_9440_the_man_who_wish_dark_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9440);
end

function mqc15_9440_the_man_who_wish_dark_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9440);
	local questID=9440;
end

function mqc15_9440_the_man_who_wish_dark_OnRemoteStart( userObjID, questID )
end

function mqc15_9440_the_man_who_wish_dark_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9440_the_man_who_wish_dark_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9440, 2);
				api_quest_SetJournalStep(userObjID,9440, 1);
				api_quest_SetQuestStep(userObjID,9440, 1);
				npc_talk_index = "n1578_for_memory-1";
end

</VillageServer>

<GameServer>
function mqc15_9440_the_man_who_wish_dark_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1497 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1497_people( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1500 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1500_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1573 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1573_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1574 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1574_aruno( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1575 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1575_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1576 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1576_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1578 then
		mqc15_9440_the_man_who_wish_dark_OnTalk_n1578_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1497_people--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1497_people( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1500_gaharam--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1500_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1500_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1500_gaharam-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1500_gaharam-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1573_gaharam--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1573_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1573_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1574_aruno--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1574_aruno( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1574_aruno-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1574_aruno-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1574_aruno-3-b" then 
	end
	if npc_talk_index == "n1574_aruno-3-b" then 
	end
	if npc_talk_index == "n1574_aruno-3-b" then 
	end
	if npc_talk_index == "n1574_aruno-3-c" then 
	end
	if npc_talk_index == "n1574_aruno-3-d" then 
	end
	if npc_talk_index == "n1574_aruno-3-e" then 
	end
	if npc_talk_index == "n1574_aruno-3-f" then 
	end
	if npc_talk_index == "n1574_aruno-3-g" then 
	end
	if npc_talk_index == "n1574_aruno-3-h" then 
	end
	if npc_talk_index == "n1574_aruno-3-i" then 
	end
	if npc_talk_index == "n1574_aruno-3-j" then 
	end
	if npc_talk_index == "n1574_aruno-3-k" then 
	end
	if npc_talk_index == "n1574_aruno-3-l" then 
	end
	if npc_talk_index == "n1574_aruno-3-m" then 
	end
	if npc_talk_index == "n1574_aruno-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1575_gaharam--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1575_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1575_gaharam-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1575_gaharam-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1575_gaharam-5-b" then 
	end
	if npc_talk_index == "n1575_gaharam-5-c" then 
	end
	if npc_talk_index == "n1575_gaharam-5-d" then 
	end
	if npc_talk_index == "n1575_gaharam-5-e" then 
	end
	if npc_talk_index == "n1575_gaharam-5-f" then 
	end
	if npc_talk_index == "n1575_gaharam-5-g" then 
	end
	if npc_talk_index == "n1575_gaharam-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1576_for_memory--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1576_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1578_for_memory--------------------------------------------------------------------------------
function mqc15_9440_the_man_who_wish_dark_OnTalk_n1578_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1578_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1578_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1578_for_memory-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1578_for_memory-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9440, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9440, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9440, 1);
				npc_talk_index = "n1578_for_memory-1";

	end
	if npc_talk_index == "n1578_for_memory-7-b" then 
	end
	if npc_talk_index == "n1578_for_memory-7-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94400, questID, 1);
			 end 
	end
	if npc_talk_index == "n1578_for_memory-7-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9441, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9441, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9441, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1578_for_memory-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1578_for_memory-1", "mqc15_9441_empty_hope.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9440_the_man_who_wish_dark_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9440);
end

function mqc15_9440_the_man_who_wish_dark_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9440);
end

function mqc15_9440_the_man_who_wish_dark_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9440);
	local questID=9440;
end

function mqc15_9440_the_man_who_wish_dark_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9440_the_man_who_wish_dark_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9440_the_man_who_wish_dark_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9440, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9440, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9440, 1);
				npc_talk_index = "n1578_for_memory-1";
end

</GameServer>