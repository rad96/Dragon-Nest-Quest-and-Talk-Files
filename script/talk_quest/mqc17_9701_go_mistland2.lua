<VillageServer>

function mqc17_9701_go_mistland2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1805 then
		mqc17_9701_go_mistland2_OnTalk_n1805_merca_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1806 then
		mqc17_9701_go_mistland2_OnTalk_n1806_merca_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1813 then
		mqc17_9701_go_mistland2_OnTalk_n1813_rubinat_box(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1839 then
		mqc17_9701_go_mistland2_OnTalk_n1839_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1840 then
		mqc17_9701_go_mistland2_OnTalk_n1840_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 775 then
		mqc17_9701_go_mistland2_OnTalk_n775_cleric_ilizer(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		mqc17_9701_go_mistland2_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1805_merca_geraint--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n1805_merca_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1805_merca_geraint-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1806_merca_rubinat--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n1806_merca_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1806_merca_rubinat-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1806_merca_rubinat-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1806_merca_rubinat-4-b" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-c" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-d" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-e" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-f" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-g" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-h" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-i" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-j" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-k" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-l" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-m" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-n" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-o" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-p" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-q" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-r" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-s" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-t" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-u" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-v" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97010, true);
				 api_quest_RewardQuestUser(userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97010, true);
				 api_quest_RewardQuestUser(userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97010, true);
				 api_quest_RewardQuestUser(userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97010, true);
				 api_quest_RewardQuestUser(userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97010, true);
				 api_quest_RewardQuestUser(userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97010, true);
				 api_quest_RewardQuestUser(userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97010, true);
				 api_quest_RewardQuestUser(userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97010, true);
				 api_quest_RewardQuestUser(userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97010, true);
				 api_quest_RewardQuestUser(userObjID, 97010, questID, 1);
			 end 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-w" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9702, 2);
					api_quest_SetQuestStep(userObjID, 9702, 1);
					api_quest_SetJournalStep(userObjID, 9702, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1806_merca_rubinat-4-x" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1806_merca_rubinat-1", "mqc17_9702_successor_of_protector.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1813_rubinat_box--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n1813_rubinat_box(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1813_rubinat_box-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1813_rubinat_box-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1813_rubinat_box-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1839_gereint_kid--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n1839_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1839_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1839_gereint_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1840_velskud--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n1840_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1840_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n775_cleric_ilizer--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n775_cleric_ilizer(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n775_cleric_ilizer-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n775_cleric_ilizer-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n775_cleric_ilizer-accepting-party_check" then
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n775_cleric_ilizer-accepting-a";

				else
									npc_talk_index = "n775_cleric_ilizer-accepting-b";

				end

	end
	if npc_talk_index == "n775_cleric_ilizer-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9701, 2);
				api_quest_SetJournalStep(userObjID,9701, 1);
				api_quest_SetQuestStep(userObjID,9701, 1);

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n776_darklair_mardlen-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n776_darklair_mardlen-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-1-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-f" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-g" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-h" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-i" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-j" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-k" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-l" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-m" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-n" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-party_check" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n776_darklair_mardlen-1-o";

				else
									npc_talk_index = "n776_darklair_mardlen-1-p";

				end
	end
	if npc_talk_index == "n776_darklair_mardlen-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13525,1);
	end
	if npc_talk_index == "n776_darklair_mardlen-2-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-party_check" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n776_darklair_mardlen-2-c";

				else
									npc_talk_index = "n776_darklair_mardlen-2-d";

				end
	end
	if npc_talk_index == "n776_darklair_mardlen-2-warp" then 
				api_user_ChangeMap(userObjID,13525,1);
	end
	if npc_talk_index == "n776_darklair_mardlen-3-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-party_check" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n776_darklair_mardlen-3-c";

				else
									npc_talk_index = "n776_darklair_mardlen-3-d";

				end
	end
	if npc_talk_index == "n776_darklair_mardlen-3-warp" then 
				api_user_ChangeMap(userObjID,13525,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc17_9701_go_mistland2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9701);
end

function mqc17_9701_go_mistland2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9701);
end

function mqc17_9701_go_mistland2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9701);
	local questID=9701;
end

function mqc17_9701_go_mistland2_OnRemoteStart( userObjID, questID )
end

function mqc17_9701_go_mistland2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc17_9701_go_mistland2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9701, 2);
				api_quest_SetJournalStep(userObjID,9701, 1);
				api_quest_SetQuestStep(userObjID,9701, 1);
end

</VillageServer>

<GameServer>
function mqc17_9701_go_mistland2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1805 then
		mqc17_9701_go_mistland2_OnTalk_n1805_merca_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1806 then
		mqc17_9701_go_mistland2_OnTalk_n1806_merca_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1813 then
		mqc17_9701_go_mistland2_OnTalk_n1813_rubinat_box( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1839 then
		mqc17_9701_go_mistland2_OnTalk_n1839_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1840 then
		mqc17_9701_go_mistland2_OnTalk_n1840_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 775 then
		mqc17_9701_go_mistland2_OnTalk_n775_cleric_ilizer( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		mqc17_9701_go_mistland2_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1805_merca_geraint--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n1805_merca_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1805_merca_geraint-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1806_merca_rubinat--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n1806_merca_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1806_merca_rubinat-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1806_merca_rubinat-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1806_merca_rubinat-4-b" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-c" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-d" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-e" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-f" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-g" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-h" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-i" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-j" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-k" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-l" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-m" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-n" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-o" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-p" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-q" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-r" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-s" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-t" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-u" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-v" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97010, questID, 1);
			 end 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-w" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9702, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9702, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9702, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1806_merca_rubinat-4-x" then 
	end
	if npc_talk_index == "n1806_merca_rubinat-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1806_merca_rubinat-1", "mqc17_9702_successor_of_protector.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1813_rubinat_box--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n1813_rubinat_box( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1813_rubinat_box-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1813_rubinat_box-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1813_rubinat_box-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1839_gereint_kid--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n1839_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1839_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1839_gereint_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1840_velskud--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n1840_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1840_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n775_cleric_ilizer--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n775_cleric_ilizer( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n775_cleric_ilizer-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n775_cleric_ilizer-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n775_cleric_ilizer-accepting-party_check" then
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n775_cleric_ilizer-accepting-a";

				else
									npc_talk_index = "n775_cleric_ilizer-accepting-b";

				end

	end
	if npc_talk_index == "n775_cleric_ilizer-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9701, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9701, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9701, 1);

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function mqc17_9701_go_mistland2_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n776_darklair_mardlen-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n776_darklair_mardlen-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-1-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-f" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-g" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-h" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-i" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-j" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-k" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-l" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-m" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-n" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-party_check" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n776_darklair_mardlen-1-o";

				else
									npc_talk_index = "n776_darklair_mardlen-1-p";

				end
	end
	if npc_talk_index == "n776_darklair_mardlen-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13525,1);
	end
	if npc_talk_index == "n776_darklair_mardlen-2-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-party_check" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n776_darklair_mardlen-2-c";

				else
									npc_talk_index = "n776_darklair_mardlen-2-d";

				end
	end
	if npc_talk_index == "n776_darklair_mardlen-2-warp" then 
				api_user_ChangeMap( pRoom, userObjID,13525,1);
	end
	if npc_talk_index == "n776_darklair_mardlen-3-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-party_check" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n776_darklair_mardlen-3-c";

				else
									npc_talk_index = "n776_darklair_mardlen-3-d";

				end
	end
	if npc_talk_index == "n776_darklair_mardlen-3-warp" then 
				api_user_ChangeMap( pRoom, userObjID,13525,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc17_9701_go_mistland2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9701);
end

function mqc17_9701_go_mistland2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9701);
end

function mqc17_9701_go_mistland2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9701);
	local questID=9701;
end

function mqc17_9701_go_mistland2_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc17_9701_go_mistland2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc17_9701_go_mistland2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9701, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9701, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9701, 1);
end

</GameServer>