<VillageServer>

function mq15_710_most_important_thing_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 485 then
		mq15_710_most_important_thing_OnTalk_n485_overhear(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 491 then
		mq15_710_most_important_thing_OnTalk_n491_overhear(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mq15_710_most_important_thing_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 706 then
		mq15_710_most_important_thing_OnTalk_n706_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_710_most_important_thing_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 724 then
		mq15_710_most_important_thing_OnTalk_n724_karahan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n485_overhear--------------------------------------------------------------------------------
function mq15_710_most_important_thing_OnTalk_n485_overhear(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n485_overhear-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n485_overhear-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n485_overhear-4-b" then 
	end
	if npc_talk_index == "n485_overhear-4-c" then 
	end
	if npc_talk_index == "n485_overhear-4-d" then 
	end
	if npc_talk_index == "n485_overhear-4-e" then 
	end
	if npc_talk_index == "n485_overhear-4-f" then 
	end
	if npc_talk_index == "n485_overhear-4-g" then 
	end
	if npc_talk_index == "n485_overhear-4-h" then 
	end
	if npc_talk_index == "n485_overhear-4-i" then 
	end
	if npc_talk_index == "n485_overhear-4-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7101, true);
				 api_quest_RewardQuestUser(userObjID, 7101, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7102, true);
				 api_quest_RewardQuestUser(userObjID, 7102, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7103, true);
				 api_quest_RewardQuestUser(userObjID, 7103, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7104, true);
				 api_quest_RewardQuestUser(userObjID, 7104, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7105, true);
				 api_quest_RewardQuestUser(userObjID, 7105, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7106, true);
				 api_quest_RewardQuestUser(userObjID, 7106, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7101, true);
				 api_quest_RewardQuestUser(userObjID, 7101, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7101, true);
				 api_quest_RewardQuestUser(userObjID, 7101, questID, 1);
			 end 
	end
	if npc_talk_index == "n485_overhear-4-k" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 711, 2);
					api_quest_SetQuestStep(userObjID, 711, 1);
					api_quest_SetJournalStep(userObjID, 711, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n491_overhear--------------------------------------------------------------------------------
function mq15_710_most_important_thing_OnTalk_n491_overhear(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n491_overhear-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n491_overhear-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n491_overhear-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n491_overhear-3-b" then 
	end
	if npc_talk_index == "n491_overhear-3-c" then 
	end
	if npc_talk_index == "n491_overhear-3-d" then 
	end
	if npc_talk_index == "n491_overhear-3-e" then 
	end
	if npc_talk_index == "n491_overhear-3-f" then 
	end
	if npc_talk_index == "n491_overhear-3-g" then 
	end
	if npc_talk_index == "n491_overhear-3-h" then 
	end
	if npc_talk_index == "n491_overhear-3-i" then 
	end
	if npc_talk_index == "n491_overhear-3-j" then 
	end
	if npc_talk_index == "n491_overhear-3-k" then 
	end
	if npc_talk_index == "n491_overhear-3-l" then 
	end
	if npc_talk_index == "n491_overhear-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_710_most_important_thing_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest(userObjID,710, 2);
				api_quest_SetJournalStep(userObjID,710, 1);
				api_quest_SetQuestStep(userObjID,710, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-1-lv_chk" then 
				if api_user_GetUserLevel(userObjID) >= 43 then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									npc_talk_index = "n701_oldkarakule-1-b";

				end
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-j" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-k" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-l" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-m" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-n" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-o" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-p" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-q" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-r" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-s" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-t" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n706_charty--------------------------------------------------------------------------------
function mq15_710_most_important_thing_OnTalk_n706_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_710_most_important_thing_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n707_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
	end
	if npc_talk_index == "n707_sidel-2-d" then 
	end
	if npc_talk_index == "n707_sidel-2-e" then 
	end
	if npc_talk_index == "n707_sidel-2-f" then 
	end
	if npc_talk_index == "n707_sidel-2-g" then 
	end
	if npc_talk_index == "n707_sidel-2-h" then 
	end
	if npc_talk_index == "n707_sidel-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n724_karahan--------------------------------------------------------------------------------
function mq15_710_most_important_thing_OnTalk_n724_karahan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_710_most_important_thing_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 710);
end

function mq15_710_most_important_thing_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 710);
end

function mq15_710_most_important_thing_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 710);
	local questID=710;
end

function mq15_710_most_important_thing_OnRemoteStart( userObjID, questID )
end

function mq15_710_most_important_thing_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_710_most_important_thing_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,710, 2);
				api_quest_SetJournalStep(userObjID,710, 1);
				api_quest_SetQuestStep(userObjID,710, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</VillageServer>

<GameServer>
function mq15_710_most_important_thing_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 485 then
		mq15_710_most_important_thing_OnTalk_n485_overhear( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 491 then
		mq15_710_most_important_thing_OnTalk_n491_overhear( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mq15_710_most_important_thing_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 706 then
		mq15_710_most_important_thing_OnTalk_n706_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_710_most_important_thing_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 724 then
		mq15_710_most_important_thing_OnTalk_n724_karahan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n485_overhear--------------------------------------------------------------------------------
function mq15_710_most_important_thing_OnTalk_n485_overhear( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n485_overhear-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n485_overhear-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n485_overhear-4-b" then 
	end
	if npc_talk_index == "n485_overhear-4-c" then 
	end
	if npc_talk_index == "n485_overhear-4-d" then 
	end
	if npc_talk_index == "n485_overhear-4-e" then 
	end
	if npc_talk_index == "n485_overhear-4-f" then 
	end
	if npc_talk_index == "n485_overhear-4-g" then 
	end
	if npc_talk_index == "n485_overhear-4-h" then 
	end
	if npc_talk_index == "n485_overhear-4-i" then 
	end
	if npc_talk_index == "n485_overhear-4-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7101, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7101, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7102, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7102, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7103, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7103, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7104, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7104, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7105, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7105, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7106, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7106, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7101, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7101, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7101, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7101, questID, 1);
			 end 
	end
	if npc_talk_index == "n485_overhear-4-k" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 711, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 711, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 711, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n491_overhear--------------------------------------------------------------------------------
function mq15_710_most_important_thing_OnTalk_n491_overhear( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n491_overhear-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n491_overhear-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n491_overhear-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n491_overhear-3-b" then 
	end
	if npc_talk_index == "n491_overhear-3-c" then 
	end
	if npc_talk_index == "n491_overhear-3-d" then 
	end
	if npc_talk_index == "n491_overhear-3-e" then 
	end
	if npc_talk_index == "n491_overhear-3-f" then 
	end
	if npc_talk_index == "n491_overhear-3-g" then 
	end
	if npc_talk_index == "n491_overhear-3-h" then 
	end
	if npc_talk_index == "n491_overhear-3-i" then 
	end
	if npc_talk_index == "n491_overhear-3-j" then 
	end
	if npc_talk_index == "n491_overhear-3-k" then 
	end
	if npc_talk_index == "n491_overhear-3-l" then 
	end
	if npc_talk_index == "n491_overhear-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_710_most_important_thing_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,710, 2);
				api_quest_SetJournalStep( pRoom, userObjID,710, 1);
				api_quest_SetQuestStep( pRoom, userObjID,710, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-1-lv_chk" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 43 then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									npc_talk_index = "n701_oldkarakule-1-b";

				end
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-j" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-k" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-l" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-m" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-n" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-o" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-p" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-q" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-r" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-s" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-t" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n706_charty--------------------------------------------------------------------------------
function mq15_710_most_important_thing_OnTalk_n706_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_710_most_important_thing_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n707_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
	end
	if npc_talk_index == "n707_sidel-2-d" then 
	end
	if npc_talk_index == "n707_sidel-2-e" then 
	end
	if npc_talk_index == "n707_sidel-2-f" then 
	end
	if npc_talk_index == "n707_sidel-2-g" then 
	end
	if npc_talk_index == "n707_sidel-2-h" then 
	end
	if npc_talk_index == "n707_sidel-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n724_karahan--------------------------------------------------------------------------------
function mq15_710_most_important_thing_OnTalk_n724_karahan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_710_most_important_thing_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 710);
end

function mq15_710_most_important_thing_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 710);
end

function mq15_710_most_important_thing_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 710);
	local questID=710;
end

function mq15_710_most_important_thing_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_710_most_important_thing_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_710_most_important_thing_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,710, 2);
				api_quest_SetJournalStep( pRoom, userObjID,710, 1);
				api_quest_SetQuestStep( pRoom, userObjID,710, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</GameServer>