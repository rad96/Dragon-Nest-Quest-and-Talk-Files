<VillageServer>

function sq11_640_like_a_dream_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 38 then
		sq11_640_like_a_dream_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 434 then
		sq11_640_like_a_dream_OnTalk_n434_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 436 then
		sq11_640_like_a_dream_OnTalk_n436_edan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function sq11_640_like_a_dream_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n038_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6400, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6400, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6400, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6400, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6400, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6400, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6400, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6400, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6400, false);
			 end 

	end
	if npc_talk_index == "n038_royal_magician_kalaen-accepting-acceptted" then
				api_quest_AddQuest(userObjID,640, 1);
				api_quest_SetJournalStep(userObjID,640, 1);
				api_quest_SetQuestStep(userObjID,640, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";

	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-e" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-g" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-h" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-i" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-i" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n434_light_pole--------------------------------------------------------------------------------
function sq11_640_like_a_dream_OnTalk_n434_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n434_light_pole-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n434_light_pole-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n434_light_pole-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n434_light_pole-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n434_light_pole-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300354, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300354, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300355, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300355, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300356, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300356, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n436_edan--------------------------------------------------------------------------------
function sq11_640_like_a_dream_OnTalk_n436_edan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n436_edan-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n436_edan-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n436_edan-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n436_edan-3";
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n436_edan-3-i";

				else
									npc_talk_index = "n436_edan-3";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n436_edan-3-b" then 
	end
	if npc_talk_index == "n436_edan-3-c" then 
	end
	if npc_talk_index == "n436_edan-3-d" then 
	end
	if npc_talk_index == "n436_edan-3-e" then 
	end
	if npc_talk_index == "n436_edan-3-e" then 
	end
	if npc_talk_index == "n436_edan-3-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 end 
	end
	if npc_talk_index == "n436_edan-3-g" then 

				if api_quest_HasQuestItem(userObjID, 300354, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300354, api_quest_HasQuestItem(userObjID, 300354, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300355, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300355, api_quest_HasQuestItem(userObjID, 300355, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300356, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300356, api_quest_HasQuestItem(userObjID, 300356, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n436_edan-3-h" then 
	end
	if npc_talk_index == "n436_edan-3-j" then 
	end
	if npc_talk_index == "n436_edan-3-k" then 
	end
	if npc_talk_index == "n436_edan-3-l" then 
	end
	if npc_talk_index == "n436_edan-3-m" then 
	end
	if npc_talk_index == "n436_edan-3-n" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6400, true);
				 api_quest_RewardQuestUser(userObjID, 6400, questID, 1);
			 end 
	end
	if npc_talk_index == "n436_edan-3-o" then 

				if api_quest_HasQuestItem(userObjID, 300354, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300354, api_quest_HasQuestItem(userObjID, 300354, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300355, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300355, api_quest_HasQuestItem(userObjID, 300355, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300356, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300356, api_quest_HasQuestItem(userObjID, 300356, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n436_edan-3-p" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_640_like_a_dream_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 640);
end

function sq11_640_like_a_dream_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 640);
end

function sq11_640_like_a_dream_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 640);
	local questID=640;
end

function sq11_640_like_a_dream_OnRemoteStart( userObjID, questID )
end

function sq11_640_like_a_dream_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_640_like_a_dream_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,640, 1);
				api_quest_SetJournalStep(userObjID,640, 1);
				api_quest_SetQuestStep(userObjID,640, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";
end

</VillageServer>

<GameServer>
function sq11_640_like_a_dream_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 38 then
		sq11_640_like_a_dream_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 434 then
		sq11_640_like_a_dream_OnTalk_n434_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 436 then
		sq11_640_like_a_dream_OnTalk_n436_edan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function sq11_640_like_a_dream_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n038_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, false);
			 end 

	end
	if npc_talk_index == "n038_royal_magician_kalaen-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,640, 1);
				api_quest_SetJournalStep( pRoom, userObjID,640, 1);
				api_quest_SetQuestStep( pRoom, userObjID,640, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";

	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-e" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-g" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-h" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-i" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-i" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n434_light_pole--------------------------------------------------------------------------------
function sq11_640_like_a_dream_OnTalk_n434_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n434_light_pole-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n434_light_pole-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n434_light_pole-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n434_light_pole-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n434_light_pole-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300354, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300354, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300355, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300355, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300356, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300356, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n436_edan--------------------------------------------------------------------------------
function sq11_640_like_a_dream_OnTalk_n436_edan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n436_edan-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n436_edan-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n436_edan-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n436_edan-3";
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n436_edan-3-i";

				else
									npc_talk_index = "n436_edan-3";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n436_edan-3-b" then 
	end
	if npc_talk_index == "n436_edan-3-c" then 
	end
	if npc_talk_index == "n436_edan-3-d" then 
	end
	if npc_talk_index == "n436_edan-3-e" then 
	end
	if npc_talk_index == "n436_edan-3-e" then 
	end
	if npc_talk_index == "n436_edan-3-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 end 
	end
	if npc_talk_index == "n436_edan-3-g" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300354, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300354, api_quest_HasQuestItem( pRoom, userObjID, 300354, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300355, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300355, api_quest_HasQuestItem( pRoom, userObjID, 300355, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300356, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300356, api_quest_HasQuestItem( pRoom, userObjID, 300356, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n436_edan-3-h" then 
	end
	if npc_talk_index == "n436_edan-3-j" then 
	end
	if npc_talk_index == "n436_edan-3-k" then 
	end
	if npc_talk_index == "n436_edan-3-l" then 
	end
	if npc_talk_index == "n436_edan-3-m" then 
	end
	if npc_talk_index == "n436_edan-3-n" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6400, questID, 1);
			 end 
	end
	if npc_talk_index == "n436_edan-3-o" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300354, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300354, api_quest_HasQuestItem( pRoom, userObjID, 300354, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300355, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300355, api_quest_HasQuestItem( pRoom, userObjID, 300355, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300356, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300356, api_quest_HasQuestItem( pRoom, userObjID, 300356, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n436_edan-3-p" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_640_like_a_dream_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 640);
end

function sq11_640_like_a_dream_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 640);
end

function sq11_640_like_a_dream_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 640);
	local questID=640;
end

function sq11_640_like_a_dream_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_640_like_a_dream_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_640_like_a_dream_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,640, 1);
				api_quest_SetJournalStep( pRoom, userObjID,640, 1);
				api_quest_SetQuestStep( pRoom, userObjID,640, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";
end

</GameServer>