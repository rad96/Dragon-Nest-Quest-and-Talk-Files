<VillageServer>

function nq28_6201_matchmaking_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 26 then
		nq28_6201_matchmaking_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 4 then
		nq28_6201_matchmaking_OnTalk_n004_guard_steave(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function nq28_6201_matchmaking_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest(userObjID,6201, 1);
				api_quest_SetJournalStep(userObjID,6201, 1);
				api_quest_SetQuestStep(userObjID,6201, 1);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-1-b" then 
	end
	if npc_talk_index == "n026_trader_may-1-c" then 
	end
	if npc_talk_index == "n026_trader_may-1-d" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n026_trader_may-3-b" then 
	end
	if npc_talk_index == "n026_trader_may-3-c" then 
	end
	if npc_talk_index == "n026_trader_may-3-d" then 
	end
	if npc_talk_index == "n026_trader_may-3-e" then 
	end
	if npc_talk_index == "n026_trader_may-3-f" then 
	end
	if npc_talk_index == "n026_trader_may-3-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 26, 300 );
				api_npc_AddFavorPoint( userObjID, 4, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n026_trader_may-3-h" then 
	end
	if npc_talk_index == "n026_trader_may-3-j" then 
	end
	if npc_talk_index == "n026_trader_may-3-k" then 
	end
	if npc_talk_index == "n026_trader_may-3-l" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n004_guard_steave--------------------------------------------------------------------------------
function nq28_6201_matchmaking_OnTalk_n004_guard_steave(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n004_guard_steave-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n004_guard_steave-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n004_guard_steave-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n004_guard_steave-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n004_guard_steave-2-b" then 
	end
	if npc_talk_index == "n004_guard_steave-2-c" then 
	end
	if npc_talk_index == "n004_guard_steave-2-d" then 
	end
	if npc_talk_index == "n004_guard_steave-2-e" then 
	end
	if npc_talk_index == "n004_guard_steave-2-f" then 
	end
	if npc_talk_index == "n004_guard_steave-2-h" then 
	end
	if npc_talk_index == "n004_guard_steave-2-g" then 
	end
	if npc_talk_index == "n004_guard_steave-2-e" then 
	end
	if npc_talk_index == "n004_guard_steave-2-e" then 
	end
	if npc_talk_index == "n004_guard_steave-2-i" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq28_6201_matchmaking_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6201);
end

function nq28_6201_matchmaking_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6201);
end

function nq28_6201_matchmaking_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6201);
	local questID=6201;
end

function nq28_6201_matchmaking_OnRemoteStart( userObjID, questID )
end

function nq28_6201_matchmaking_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq28_6201_matchmaking_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,6201, 1);
				api_quest_SetJournalStep(userObjID,6201, 1);
				api_quest_SetQuestStep(userObjID,6201, 1);
				npc_talk_index = "n026_trader_may-1";
end

</VillageServer>

<GameServer>
function nq28_6201_matchmaking_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 26 then
		nq28_6201_matchmaking_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 4 then
		nq28_6201_matchmaking_OnTalk_n004_guard_steave( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function nq28_6201_matchmaking_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,6201, 1);
				api_quest_SetJournalStep( pRoom, userObjID,6201, 1);
				api_quest_SetQuestStep( pRoom, userObjID,6201, 1);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-1-b" then 
	end
	if npc_talk_index == "n026_trader_may-1-c" then 
	end
	if npc_talk_index == "n026_trader_may-1-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n026_trader_may-3-b" then 
	end
	if npc_talk_index == "n026_trader_may-3-c" then 
	end
	if npc_talk_index == "n026_trader_may-3-d" then 
	end
	if npc_talk_index == "n026_trader_may-3-e" then 
	end
	if npc_talk_index == "n026_trader_may-3-f" then 
	end
	if npc_talk_index == "n026_trader_may-3-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 26, 300 );
				api_npc_AddFavorPoint( pRoom,  userObjID, 4, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n026_trader_may-3-h" then 
	end
	if npc_talk_index == "n026_trader_may-3-j" then 
	end
	if npc_talk_index == "n026_trader_may-3-k" then 
	end
	if npc_talk_index == "n026_trader_may-3-l" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n004_guard_steave--------------------------------------------------------------------------------
function nq28_6201_matchmaking_OnTalk_n004_guard_steave( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n004_guard_steave-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n004_guard_steave-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n004_guard_steave-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n004_guard_steave-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n004_guard_steave-2-b" then 
	end
	if npc_talk_index == "n004_guard_steave-2-c" then 
	end
	if npc_talk_index == "n004_guard_steave-2-d" then 
	end
	if npc_talk_index == "n004_guard_steave-2-e" then 
	end
	if npc_talk_index == "n004_guard_steave-2-f" then 
	end
	if npc_talk_index == "n004_guard_steave-2-h" then 
	end
	if npc_talk_index == "n004_guard_steave-2-g" then 
	end
	if npc_talk_index == "n004_guard_steave-2-e" then 
	end
	if npc_talk_index == "n004_guard_steave-2-e" then 
	end
	if npc_talk_index == "n004_guard_steave-2-i" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq28_6201_matchmaking_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6201);
end

function nq28_6201_matchmaking_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6201);
end

function nq28_6201_matchmaking_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6201);
	local questID=6201;
end

function nq28_6201_matchmaking_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq28_6201_matchmaking_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq28_6201_matchmaking_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,6201, 1);
				api_quest_SetJournalStep( pRoom, userObjID,6201, 1);
				api_quest_SetQuestStep( pRoom, userObjID,6201, 1);
				npc_talk_index = "n026_trader_may-1";
end

</GameServer>