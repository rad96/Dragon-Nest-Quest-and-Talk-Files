<VillageServer>

function sq11_473_hero_knights_weapon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 292 then
		sq11_473_hero_knights_weapon_OnTalk_n292_sword_of_velskurd(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 49 then
		sq11_473_hero_knights_weapon_OnTalk_n049_sainthaven_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		sq11_473_hero_knights_weapon_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n292_sword_of_velskurd--------------------------------------------------------------------------------
function sq11_473_hero_knights_weapon_OnTalk_n292_sword_of_velskurd(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n292_sword_of_velskurd-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n292_sword_of_velskurd-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n292_sword_of_velskurd-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n292_sword_of_velskurd-1-b" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300035, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300035, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n049_sainthaven_kid--------------------------------------------------------------------------------
function sq11_473_hero_knights_weapon_OnTalk_n049_sainthaven_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n049_sainthaven_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n049_sainthaven_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n049_sainthaven_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n049_sainthaven_kid-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n049_sainthaven_kid-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n049_sainthaven_kid-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4731, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4732, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4733, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4734, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4735, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4736, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4737, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4738, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4739, false);
			 end 

	end
	if npc_talk_index == "n049_sainthaven_kid-accepting-acceppted" then
				api_quest_AddQuest(userObjID,473, 1);
				api_quest_SetJournalStep(userObjID,473, 1);
				api_quest_SetQuestStep(userObjID,473, 1);
				npc_talk_index = "n049_sainthaven_kid-1";

	end
	if npc_talk_index == "n049_sainthaven_kid-2-b" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-c" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-d" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-e" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n049_sainthaven_kid-4-b" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-4-c" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-4-d" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-4-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4731, true);
				 api_quest_RewardQuestUser(userObjID, 4731, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4732, true);
				 api_quest_RewardQuestUser(userObjID, 4732, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4733, true);
				 api_quest_RewardQuestUser(userObjID, 4733, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4734, true);
				 api_quest_RewardQuestUser(userObjID, 4734, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4735, true);
				 api_quest_RewardQuestUser(userObjID, 4735, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4736, true);
				 api_quest_RewardQuestUser(userObjID, 4736, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4737, true);
				 api_quest_RewardQuestUser(userObjID, 4737, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4738, true);
				 api_quest_RewardQuestUser(userObjID, 4738, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4739, true);
				 api_quest_RewardQuestUser(userObjID, 4739, questID, 1);
			 end 
	end
	if npc_talk_index == "n049_sainthaven_kid-4-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_473_hero_knights_weapon_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n088_scholar_starshy-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n088_scholar_starshy-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_473_hero_knights_weapon_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 473);
end

function sq11_473_hero_knights_weapon_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 473);
end

function sq11_473_hero_knights_weapon_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 473);
	local questID=473;
end

function sq11_473_hero_knights_weapon_OnRemoteStart( userObjID, questID )
end

function sq11_473_hero_knights_weapon_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_473_hero_knights_weapon_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_473_hero_knights_weapon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 292 then
		sq11_473_hero_knights_weapon_OnTalk_n292_sword_of_velskurd( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 49 then
		sq11_473_hero_knights_weapon_OnTalk_n049_sainthaven_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		sq11_473_hero_knights_weapon_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n292_sword_of_velskurd--------------------------------------------------------------------------------
function sq11_473_hero_knights_weapon_OnTalk_n292_sword_of_velskurd( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n292_sword_of_velskurd-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n292_sword_of_velskurd-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n292_sword_of_velskurd-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n292_sword_of_velskurd-1-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300035, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300035, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n049_sainthaven_kid--------------------------------------------------------------------------------
function sq11_473_hero_knights_weapon_OnTalk_n049_sainthaven_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n049_sainthaven_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n049_sainthaven_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n049_sainthaven_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n049_sainthaven_kid-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n049_sainthaven_kid-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n049_sainthaven_kid-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4731, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4732, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4733, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4734, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4735, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4736, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4737, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4738, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4739, false);
			 end 

	end
	if npc_talk_index == "n049_sainthaven_kid-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,473, 1);
				api_quest_SetJournalStep( pRoom, userObjID,473, 1);
				api_quest_SetQuestStep( pRoom, userObjID,473, 1);
				npc_talk_index = "n049_sainthaven_kid-1";

	end
	if npc_talk_index == "n049_sainthaven_kid-2-b" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-c" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-d" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n049_sainthaven_kid-4-b" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-4-c" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-4-d" then 
	end
	if npc_talk_index == "n049_sainthaven_kid-4-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4731, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4731, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4732, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4732, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4733, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4733, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4734, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4734, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4735, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4735, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4736, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4736, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4737, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4737, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4738, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4738, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4739, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4739, questID, 1);
			 end 
	end
	if npc_talk_index == "n049_sainthaven_kid-4-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_473_hero_knights_weapon_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n088_scholar_starshy-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n088_scholar_starshy-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_473_hero_knights_weapon_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 473);
end

function sq11_473_hero_knights_weapon_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 473);
end

function sq11_473_hero_knights_weapon_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 473);
	local questID=473;
end

function sq11_473_hero_knights_weapon_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_473_hero_knights_weapon_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_473_hero_knights_weapon_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>