<VillageServer>

function sq15_4208_princess_contest_1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 737 then
		sq15_4208_princess_contest_1_OnTalk_n737_sarasa(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 999 then
		sq15_4208_princess_contest_1_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n737_sarasa--------------------------------------------------------------------------------
function sq15_4208_princess_contest_1_OnTalk_n737_sarasa(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n737_sarasa-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n737_sarasa-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n737_sarasa-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 42080, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 42080, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 42080, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 42080, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 42080, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 42080, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 42080, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 42080, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 42080, false);
			 end 

	end
	if npc_talk_index == "n737_sarasa-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4208, 1);
				api_quest_SetJournalStep(userObjID,4208, 1);
				api_quest_SetQuestStep(userObjID,4208, 1);
				npc_talk_index = "n737_sarasa-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 900201, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 900203, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 900215, 30000);

	end
	if npc_talk_index == "n737_sarasa-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n999_remote--------------------------------------------------------------------------------
function sq15_4208_princess_contest_1_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n999_remote-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4208_princess_contest_1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4208);
	if qstep == 1 and CountIndex == 900201 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 900203 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 900215 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function sq15_4208_princess_contest_1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4208);
	if qstep == 1 and CountIndex == 900201 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 900203 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 900215 and Count >= TargetCount  then

	end
end

function sq15_4208_princess_contest_1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4208);
	local questID=4208;
end

function sq15_4208_princess_contest_1_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 900201, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 900203, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 900215, 30000);
end

function sq15_4208_princess_contest_1_OnRemoteComplete( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, questID);
	if qstep == 2 then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_quest_RewardQuestUser(userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_quest_RewardQuestUser(userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_quest_RewardQuestUser(userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_quest_RewardQuestUser(userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_quest_RewardQuestUser(userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_quest_RewardQuestUser(userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_quest_RewardQuestUser(userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_quest_RewardQuestUser(userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_quest_RewardQuestUser(userObjID, 42080, questID, 1);
			 end 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
end

function sq15_4208_princess_contest_1_CanRemoteCompleteStep( userObjID, questID, questStep )
	if questStep == 2 then
		return true;
	end

	return false;
end

function sq15_4208_princess_contest_1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4208, 1);
				api_quest_SetJournalStep(userObjID,4208, 1);
				api_quest_SetQuestStep(userObjID,4208, 1);
				npc_talk_index = "n737_sarasa-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 900201, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 900203, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 900215, 30000);
end

</VillageServer>

<GameServer>
function sq15_4208_princess_contest_1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 737 then
		sq15_4208_princess_contest_1_OnTalk_n737_sarasa( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 999 then
		sq15_4208_princess_contest_1_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n737_sarasa--------------------------------------------------------------------------------
function sq15_4208_princess_contest_1_OnTalk_n737_sarasa( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n737_sarasa-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n737_sarasa-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n737_sarasa-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42080, false);
			 end 

	end
	if npc_talk_index == "n737_sarasa-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4208, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4208, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4208, 1);
				npc_talk_index = "n737_sarasa-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 900201, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 900203, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 900215, 30000);

	end
	if npc_talk_index == "n737_sarasa-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n999_remote--------------------------------------------------------------------------------
function sq15_4208_princess_contest_1_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n999_remote-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4208_princess_contest_1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4208);
	if qstep == 1 and CountIndex == 900201 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 900203 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 900215 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function sq15_4208_princess_contest_1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4208);
	if qstep == 1 and CountIndex == 900201 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 900203 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 900215 and Count >= TargetCount  then

	end
end

function sq15_4208_princess_contest_1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4208);
	local questID=4208;
end

function sq15_4208_princess_contest_1_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 900201, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 900203, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 900215, 30000);
end

function sq15_4208_princess_contest_1_OnRemoteComplete( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, questID);
	if qstep == 2 then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 42080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 42080, questID, 1);
			 end 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
end

function sq15_4208_princess_contest_1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	if questStep == 2 then
		return true;
	end

	return false;
end

function sq15_4208_princess_contest_1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4208, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4208, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4208, 1);
				npc_talk_index = "n737_sarasa-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 900201, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 900203, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 900215, 30000);
end

</GameServer>