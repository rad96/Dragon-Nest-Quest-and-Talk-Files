<VillageServer>

function eq_2443_holloween_stage_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1888 then
		eq_2443_holloween_stage_OnTalk_n1888_storyteller_kesey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1888_storyteller_kesey--------------------------------------------------------------------------------
function eq_2443_holloween_stage_OnTalk_n1888_storyteller_kesey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1888_storyteller_kesey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1888_storyteller_kesey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1888_storyteller_kesey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2443, 1);
				api_quest_SetJournalStep(userObjID,2443, 1);
				api_quest_SetQuestStep(userObjID,2443, 1);
				npc_talk_index = "n1888_storyteller_kesey-1";

	end
	if npc_talk_index == "n1888_storyteller_kesey-1-changemap" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									if api_user_IsMatching(userObjID) == 1  then
									npc_talk_index = "n1888_storyteller_kesey-1-b";

				else
					
				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_ChangeMap(userObjID,13642,1);


				else
					npc_talk_index = "_full_inventory";

				end

				end

				else
									npc_talk_index = "n1888_storyteller_kesey-1-a";

				end
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-back_1" then 
				api_npc_NextScript(userObjID, npcObjID, "001", "n1888_storyteller_kesey.xml");
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-back_2" then 
				api_npc_NextScript(userObjID, npcObjID, "001", "n1888_storyteller_kesey.xml");
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function eq_2443_holloween_stage_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2443);
end

function eq_2443_holloween_stage_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2443);
end

function eq_2443_holloween_stage_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2443);
	local questID=2443;
end

function eq_2443_holloween_stage_OnRemoteStart( userObjID, questID )
end

function eq_2443_holloween_stage_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function eq_2443_holloween_stage_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2443, 1);
				api_quest_SetJournalStep(userObjID,2443, 1);
				api_quest_SetQuestStep(userObjID,2443, 1);
				npc_talk_index = "n1888_storyteller_kesey-1";
end

</VillageServer>

<GameServer>
function eq_2443_holloween_stage_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1888 then
		eq_2443_holloween_stage_OnTalk_n1888_storyteller_kesey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1888_storyteller_kesey--------------------------------------------------------------------------------
function eq_2443_holloween_stage_OnTalk_n1888_storyteller_kesey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1888_storyteller_kesey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1888_storyteller_kesey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1888_storyteller_kesey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2443, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2443, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2443, 1);
				npc_talk_index = "n1888_storyteller_kesey-1";

	end
	if npc_talk_index == "n1888_storyteller_kesey-1-changemap" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									if api_user_IsMatching( pRoom, userObjID) == 1  then
									npc_talk_index = "n1888_storyteller_kesey-1-b";

				else
					
				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_ChangeMap( pRoom, userObjID,13642,1);


				else
					npc_talk_index = "_full_inventory";

				end

				end

				else
									npc_talk_index = "n1888_storyteller_kesey-1-a";

				end
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-back_1" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "001", "n1888_storyteller_kesey.xml");
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-back_2" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "001", "n1888_storyteller_kesey.xml");
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function eq_2443_holloween_stage_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2443);
end

function eq_2443_holloween_stage_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2443);
end

function eq_2443_holloween_stage_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2443);
	local questID=2443;
end

function eq_2443_holloween_stage_OnRemoteStart( pRoom,  userObjID, questID )
end

function eq_2443_holloween_stage_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function eq_2443_holloween_stage_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2443, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2443, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2443, 1);
				npc_talk_index = "n1888_storyteller_kesey-1";
end

</GameServer>