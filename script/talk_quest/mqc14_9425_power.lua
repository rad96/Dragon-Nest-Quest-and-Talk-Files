<VillageServer>

function mqc14_9425_power_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1511 then
		mqc14_9425_power_OnTalk_n1511_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9425_power_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1542 then
		mqc14_9425_power_OnTalk_n1542_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1543 then
		mqc14_9425_power_OnTalk_n1543_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1544 then
		mqc14_9425_power_OnTalk_n1544_rambert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1508 then
		mqc14_9425_power_OnTalk_n1508_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1537 then
		mqc14_9425_power_OnTalk_n1537_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1511_argenta--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1511_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1511_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9425, 2);
				api_quest_SetJournalStep(userObjID,9425, 1);
				api_quest_SetQuestStep(userObjID,9425, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1542_argenta--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1542_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1542_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1542_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1542_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1542_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1542_argenta-1-b" then 
	end
	if npc_talk_index == "n1542_argenta-1-c" then 
	end
	if npc_talk_index == "n1542_argenta-1-d" then 
	end
	if npc_talk_index == "n1542_argenta-1-e" then 
	end
	if npc_talk_index == "n1542_argenta-1-chk_class01" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n1542_argenta-1-f";

				else
									npc_talk_index = "n1542_argenta-1-g";

				end
	end
	if npc_talk_index == "n1542_argenta-1-k" then 
	end
	if npc_talk_index == "n1542_argenta-1-h" then 
	end
	if npc_talk_index == "n1542_argenta-1-i" then 
	end
	if npc_talk_index == "n1542_argenta-1-j" then 
	end
	if npc_talk_index == "n1542_argenta-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1542_argenta-1-l" then 
	end
	if npc_talk_index == "n1542_argenta-1-m" then 
	end
	if npc_talk_index == "n1542_argenta-1-n" then 
	end
	if npc_talk_index == "n1542_argenta-1-o" then 
	end
	if npc_talk_index == "n1542_argenta-1-p" then 
	end
	if npc_talk_index == "n1542_argenta-1-q" then 
	end
	if npc_talk_index == "n1542_argenta-1-r" then 
	end
	if npc_talk_index == "n1542_argenta-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1542_argenta-3-class_chk03" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n1542_argenta-3-d";

				else
									npc_talk_index = "n1542_argenta-3-a";

				end
	end
	if npc_talk_index == "n1542_argenta-3-b" then 
	end
	if npc_talk_index == "n1542_argenta-3-c" then 
	end
	if npc_talk_index == "n1542_argenta-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1542_argenta-3-e" then 
	end
	if npc_talk_index == "n1542_argenta-3-f" then 
	end
	if npc_talk_index == "n1542_argenta-3-g" then 
	end
	if npc_talk_index == "n1542_argenta-3-h" then 
	end
	if npc_talk_index == "n1542_argenta-3-i" then 
	end
	if npc_talk_index == "n1542_argenta-3-j" then 
	end
	if npc_talk_index == "n1542_argenta-3-k" then 
	end
	if npc_talk_index == "n1542_argenta-3-l" then 
	end
	if npc_talk_index == "n1542_argenta-3-m" then 
	end
	if npc_talk_index == "n1542_argenta-3-n" then 
	end
	if npc_talk_index == "n1542_argenta-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1543_pether--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1543_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1543_pether-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1543_pether-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1543_pether-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1544_rambert--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1544_rambert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1544_rambert-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1544_rambert-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1544_rambert-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1544_rambert-2-b" then 
	end
	if npc_talk_index == "n1544_rambert-2-c" then 
	end
	if npc_talk_index == "n1544_rambert-2-d" then 
	end
	if npc_talk_index == "n1544_rambert-2-e" then 
	end
	if npc_talk_index == "n1544_rambert-2-f" then 
	end
	if npc_talk_index == "n1544_rambert-2-g" then 
	end
	if npc_talk_index == "n1544_rambert-2-h" then 
	end
	if npc_talk_index == "n1544_rambert-2-i" then 
	end
	if npc_talk_index == "n1544_rambert-2-j" then 
	end
	if npc_talk_index == "n1544_rambert-2-k" then 
	end
	if npc_talk_index == "n1544_rambert-2-l" then 
	end
	if npc_talk_index == "n1544_rambert-2-m" then 
	end
	if npc_talk_index == "n1544_rambert-2-n" then 
	end
	if npc_talk_index == "n1544_rambert-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1508_argenta--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1508_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1537_gereint_kid--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1537_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1537_gereint_kid-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1537_gereint_kid-4-b" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-c" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-d" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-e" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-f" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-g" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-h" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-i" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-j" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-k" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-l" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-m" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-n" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-o" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-p" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-q" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-r" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-s" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94251, true);
				 api_quest_RewardQuestUser(userObjID, 94251, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94252, true);
				 api_quest_RewardQuestUser(userObjID, 94252, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94253, true);
				 api_quest_RewardQuestUser(userObjID, 94253, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94254, true);
				 api_quest_RewardQuestUser(userObjID, 94254, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94255, true);
				 api_quest_RewardQuestUser(userObjID, 94255, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94256, true);
				 api_quest_RewardQuestUser(userObjID, 94256, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94257, true);
				 api_quest_RewardQuestUser(userObjID, 94257, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94258, true);
				 api_quest_RewardQuestUser(userObjID, 94258, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94259, true);
				 api_quest_RewardQuestUser(userObjID, 94259, questID, 1);
			 end 
	end
	if npc_talk_index == "n1537_gereint_kid-4-t" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9426, 2);
					api_quest_SetQuestStep(userObjID, 9426, 1);
					api_quest_SetJournalStep(userObjID, 9426, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1537_gereint_kid-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1537_gereint_kid-1", "mqc14_9426_the_man_who_open_the_door.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9425_power_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9425);
end

function mqc14_9425_power_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9425);
end

function mqc14_9425_power_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9425);
	local questID=9425;
end

function mqc14_9425_power_OnRemoteStart( userObjID, questID )
end

function mqc14_9425_power_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc14_9425_power_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9425, 2);
				api_quest_SetJournalStep(userObjID,9425, 1);
				api_quest_SetQuestStep(userObjID,9425, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</VillageServer>

<GameServer>
function mqc14_9425_power_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1511 then
		mqc14_9425_power_OnTalk_n1511_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9425_power_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1542 then
		mqc14_9425_power_OnTalk_n1542_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1543 then
		mqc14_9425_power_OnTalk_n1543_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1544 then
		mqc14_9425_power_OnTalk_n1544_rambert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1508 then
		mqc14_9425_power_OnTalk_n1508_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1537 then
		mqc14_9425_power_OnTalk_n1537_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1511_argenta--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1511_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1511_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9425, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9425, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9425, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1542_argenta--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1542_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1542_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1542_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1542_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1542_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1542_argenta-1-b" then 
	end
	if npc_talk_index == "n1542_argenta-1-c" then 
	end
	if npc_talk_index == "n1542_argenta-1-d" then 
	end
	if npc_talk_index == "n1542_argenta-1-e" then 
	end
	if npc_talk_index == "n1542_argenta-1-chk_class01" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n1542_argenta-1-f";

				else
									npc_talk_index = "n1542_argenta-1-g";

				end
	end
	if npc_talk_index == "n1542_argenta-1-k" then 
	end
	if npc_talk_index == "n1542_argenta-1-h" then 
	end
	if npc_talk_index == "n1542_argenta-1-i" then 
	end
	if npc_talk_index == "n1542_argenta-1-j" then 
	end
	if npc_talk_index == "n1542_argenta-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1542_argenta-1-l" then 
	end
	if npc_talk_index == "n1542_argenta-1-m" then 
	end
	if npc_talk_index == "n1542_argenta-1-n" then 
	end
	if npc_talk_index == "n1542_argenta-1-o" then 
	end
	if npc_talk_index == "n1542_argenta-1-p" then 
	end
	if npc_talk_index == "n1542_argenta-1-q" then 
	end
	if npc_talk_index == "n1542_argenta-1-r" then 
	end
	if npc_talk_index == "n1542_argenta-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1542_argenta-3-class_chk03" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n1542_argenta-3-d";

				else
									npc_talk_index = "n1542_argenta-3-a";

				end
	end
	if npc_talk_index == "n1542_argenta-3-b" then 
	end
	if npc_talk_index == "n1542_argenta-3-c" then 
	end
	if npc_talk_index == "n1542_argenta-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1542_argenta-3-e" then 
	end
	if npc_talk_index == "n1542_argenta-3-f" then 
	end
	if npc_talk_index == "n1542_argenta-3-g" then 
	end
	if npc_talk_index == "n1542_argenta-3-h" then 
	end
	if npc_talk_index == "n1542_argenta-3-i" then 
	end
	if npc_talk_index == "n1542_argenta-3-j" then 
	end
	if npc_talk_index == "n1542_argenta-3-k" then 
	end
	if npc_talk_index == "n1542_argenta-3-l" then 
	end
	if npc_talk_index == "n1542_argenta-3-m" then 
	end
	if npc_talk_index == "n1542_argenta-3-n" then 
	end
	if npc_talk_index == "n1542_argenta-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1543_pether--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1543_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1543_pether-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1543_pether-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1543_pether-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1544_rambert--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1544_rambert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1544_rambert-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1544_rambert-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1544_rambert-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1544_rambert-2-b" then 
	end
	if npc_talk_index == "n1544_rambert-2-c" then 
	end
	if npc_talk_index == "n1544_rambert-2-d" then 
	end
	if npc_talk_index == "n1544_rambert-2-e" then 
	end
	if npc_talk_index == "n1544_rambert-2-f" then 
	end
	if npc_talk_index == "n1544_rambert-2-g" then 
	end
	if npc_talk_index == "n1544_rambert-2-h" then 
	end
	if npc_talk_index == "n1544_rambert-2-i" then 
	end
	if npc_talk_index == "n1544_rambert-2-j" then 
	end
	if npc_talk_index == "n1544_rambert-2-k" then 
	end
	if npc_talk_index == "n1544_rambert-2-l" then 
	end
	if npc_talk_index == "n1544_rambert-2-m" then 
	end
	if npc_talk_index == "n1544_rambert-2-n" then 
	end
	if npc_talk_index == "n1544_rambert-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1508_argenta--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1508_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1537_gereint_kid--------------------------------------------------------------------------------
function mqc14_9425_power_OnTalk_n1537_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1537_gereint_kid-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1537_gereint_kid-4-b" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-c" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-d" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-e" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-f" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-g" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-h" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-i" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-j" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-k" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-l" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-m" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-n" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-o" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-p" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-q" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-r" then 
	end
	if npc_talk_index == "n1537_gereint_kid-4-s" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94251, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94251, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94252, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94252, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94253, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94253, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94254, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94254, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94255, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94255, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94256, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94256, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94257, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94257, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94258, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94258, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94259, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94259, questID, 1);
			 end 
	end
	if npc_talk_index == "n1537_gereint_kid-4-t" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9426, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9426, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9426, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1537_gereint_kid-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1537_gereint_kid-1", "mqc14_9426_the_man_who_open_the_door.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9425_power_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9425);
end

function mqc14_9425_power_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9425);
end

function mqc14_9425_power_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9425);
	local questID=9425;
end

function mqc14_9425_power_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc14_9425_power_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc14_9425_power_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9425, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9425, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9425, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</GameServer>