<VillageServer>

function mqc06_9503_meet_the_karakule_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 213 then
		mqc06_9503_meet_the_karakule_OnTalk_n213_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mqc06_9503_meet_the_karakule_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 461 then
		mqc06_9503_meet_the_karakule_OnTalk_n461_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mqc06_9503_meet_the_karakule_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 462 then
		mqc06_9503_meet_the_karakule_OnTalk_n462_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n213_slave_david--------------------------------------------------------------------------------
function mqc06_9503_meet_the_karakule_OnTalk_n213_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n213_slave_david-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n213_slave_david-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n213_slave_david-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n213_slave_david-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9503, 2);
				api_quest_SetJournalStep(userObjID,9503, 1);
				api_quest_SetQuestStep(userObjID,9503, 1);
				npc_talk_index = "n213_slave_david-1";

	end
	if npc_talk_index == "n213_slave_david-1-b" then 
	end
	if npc_talk_index == "n213_slave_david-1-c" then 
	end
	if npc_talk_index == "n213_slave_david-1-d" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mqc06_9503_meet_the_karakule_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n701_oldkarakule-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n701_oldkarakule-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-3-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-j" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-k" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-l" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-m" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-n" then 
	end
	if npc_talk_index == "n701_oldkarakule-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n461_argenta--------------------------------------------------------------------------------
function mqc06_9503_meet_the_karakule_OnTalk_n461_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n461_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n461_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n461_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n461_argenta-2-b" then 
	end
	if npc_talk_index == "n461_argenta-2-c" then 
	end
	if npc_talk_index == "n461_argenta-2-d" then 
	end
	if npc_talk_index == "n461_argenta-2-e" then 
	end
	if npc_talk_index == "n461_argenta-2-f" then 
	end
	if npc_talk_index == "n461_argenta-2-g" then 
	end
	if npc_talk_index == "n461_argenta-2-h" then 
	end
	if npc_talk_index == "n461_argenta-2-i" then 
	end
	if npc_talk_index == "n461_argenta-2-j" then 
	end
	if npc_talk_index == "n461_argenta-2-k" then 
	end
	if npc_talk_index == "n461_argenta-2-l" then 
	end
	if npc_talk_index == "n461_argenta-2-m" then 
	end
	if npc_talk_index == "n461_argenta-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				if api_quest_HasQuestItem(userObjID, 400070, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400070, api_quest_HasQuestItem(userObjID, 400070, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mqc06_9503_meet_the_karakule_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n462_sidel--------------------------------------------------------------------------------
function mqc06_9503_meet_the_karakule_OnTalk_n462_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n462_sidel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n462_sidel-4-b" then 
	end
	if npc_talk_index == "n462_sidel-4-c" then 
	end
	if npc_talk_index == "n462_sidel-4-d" then 
	end
	if npc_talk_index == "n462_sidel-4-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95030, true);
				 api_quest_RewardQuestUser(userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95030, true);
				 api_quest_RewardQuestUser(userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95030, true);
				 api_quest_RewardQuestUser(userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95030, true);
				 api_quest_RewardQuestUser(userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95030, true);
				 api_quest_RewardQuestUser(userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95030, true);
				 api_quest_RewardQuestUser(userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95030, true);
				 api_quest_RewardQuestUser(userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95030, true);
				 api_quest_RewardQuestUser(userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95030, true);
				 api_quest_RewardQuestUser(userObjID, 95030, questID, 1);
			 end 
	end
	if npc_talk_index == "n462_sidel-4-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9504, 2);
					api_quest_SetQuestStep(userObjID, 9504, 1);
					api_quest_SetJournalStep(userObjID, 9504, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n462_sidel-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n462_sidel-1", "mqc06_9504_fairystars_remedy.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc06_9503_meet_the_karakule_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9503);
end

function mqc06_9503_meet_the_karakule_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9503);
end

function mqc06_9503_meet_the_karakule_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9503);
	local questID=9503;
end

function mqc06_9503_meet_the_karakule_OnRemoteStart( userObjID, questID )
end

function mqc06_9503_meet_the_karakule_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc06_9503_meet_the_karakule_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9503, 2);
				api_quest_SetJournalStep(userObjID,9503, 1);
				api_quest_SetQuestStep(userObjID,9503, 1);
				npc_talk_index = "n213_slave_david-1";
end

</VillageServer>

<GameServer>
function mqc06_9503_meet_the_karakule_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 213 then
		mqc06_9503_meet_the_karakule_OnTalk_n213_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mqc06_9503_meet_the_karakule_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 461 then
		mqc06_9503_meet_the_karakule_OnTalk_n461_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mqc06_9503_meet_the_karakule_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 462 then
		mqc06_9503_meet_the_karakule_OnTalk_n462_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n213_slave_david--------------------------------------------------------------------------------
function mqc06_9503_meet_the_karakule_OnTalk_n213_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n213_slave_david-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n213_slave_david-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n213_slave_david-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n213_slave_david-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9503, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9503, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9503, 1);
				npc_talk_index = "n213_slave_david-1";

	end
	if npc_talk_index == "n213_slave_david-1-b" then 
	end
	if npc_talk_index == "n213_slave_david-1-c" then 
	end
	if npc_talk_index == "n213_slave_david-1-d" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mqc06_9503_meet_the_karakule_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n701_oldkarakule-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n701_oldkarakule-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-3-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-j" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-k" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-l" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-m" then 
	end
	if npc_talk_index == "n701_oldkarakule-3-n" then 
	end
	if npc_talk_index == "n701_oldkarakule-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n461_argenta--------------------------------------------------------------------------------
function mqc06_9503_meet_the_karakule_OnTalk_n461_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n461_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n461_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n461_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n461_argenta-2-b" then 
	end
	if npc_talk_index == "n461_argenta-2-c" then 
	end
	if npc_talk_index == "n461_argenta-2-d" then 
	end
	if npc_talk_index == "n461_argenta-2-e" then 
	end
	if npc_talk_index == "n461_argenta-2-f" then 
	end
	if npc_talk_index == "n461_argenta-2-g" then 
	end
	if npc_talk_index == "n461_argenta-2-h" then 
	end
	if npc_talk_index == "n461_argenta-2-i" then 
	end
	if npc_talk_index == "n461_argenta-2-j" then 
	end
	if npc_talk_index == "n461_argenta-2-k" then 
	end
	if npc_talk_index == "n461_argenta-2-l" then 
	end
	if npc_talk_index == "n461_argenta-2-m" then 
	end
	if npc_talk_index == "n461_argenta-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				if api_quest_HasQuestItem( pRoom, userObjID, 400070, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400070, api_quest_HasQuestItem( pRoom, userObjID, 400070, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mqc06_9503_meet_the_karakule_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n462_sidel--------------------------------------------------------------------------------
function mqc06_9503_meet_the_karakule_OnTalk_n462_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n462_sidel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n462_sidel-4-b" then 
	end
	if npc_talk_index == "n462_sidel-4-c" then 
	end
	if npc_talk_index == "n462_sidel-4-d" then 
	end
	if npc_talk_index == "n462_sidel-4-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95030, questID, 1);
			 end 
	end
	if npc_talk_index == "n462_sidel-4-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9504, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9504, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9504, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n462_sidel-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n462_sidel-1", "mqc06_9504_fairystars_remedy.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc06_9503_meet_the_karakule_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9503);
end

function mqc06_9503_meet_the_karakule_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9503);
end

function mqc06_9503_meet_the_karakule_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9503);
	local questID=9503;
end

function mqc06_9503_meet_the_karakule_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc06_9503_meet_the_karakule_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc06_9503_meet_the_karakule_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9503, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9503, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9503, 1);
				npc_talk_index = "n213_slave_david-1";
end

</GameServer>