<VillageServer>

function sq15_4065_saint_challenger8_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 861 then
		sq15_4065_saint_challenger8_OnTalk_n861_orc_oneeye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 98 then
		sq15_4065_saint_challenger8_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n861_orc_oneeye--------------------------------------------------------------------------------
function sq15_4065_saint_challenger8_OnTalk_n861_orc_oneeye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n861_orc_oneeye-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n861_orc_oneeye-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n861_orc_oneeye-1-b" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-class_check" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n861_orc_oneeye-1-c";

				else
									if api_user_GetUserClassID(userObjID) == 2 then
									npc_talk_index = "n861_orc_oneeye-1-h";

				else
									npc_talk_index = "n861_orc_oneeye-1-m";

				end

				end
	end
	if npc_talk_index == "n861_orc_oneeye-1-d" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-e" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-f" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-g" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-m" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-i" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-j" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-k" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-l" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-m" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-n" then 
	end
	if npc_talk_index == "n861_orc_oneeye-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq15_4065_saint_challenger8_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n098_warrior_master_lodrigo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40650, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40650, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40650, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40650, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40650, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40650, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40650, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40650, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40650, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4065, 1);
				api_quest_SetJournalStep(userObjID,4065, 1);
				api_quest_SetQuestStep(userObjID,4065, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40650, true);
				 api_quest_RewardQuestUser(userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40650, true);
				 api_quest_RewardQuestUser(userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40650, true);
				 api_quest_RewardQuestUser(userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40650, true);
				 api_quest_RewardQuestUser(userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40650, true);
				 api_quest_RewardQuestUser(userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40650, true);
				 api_quest_RewardQuestUser(userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40650, true);
				 api_quest_RewardQuestUser(userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40650, true);
				 api_quest_RewardQuestUser(userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40650, true);
				 api_quest_RewardQuestUser(userObjID, 40650, questID, 1);
			 end 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4065_saint_challenger8_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4065);
end

function sq15_4065_saint_challenger8_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4065);
end

function sq15_4065_saint_challenger8_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4065);
	local questID=4065;
end

function sq15_4065_saint_challenger8_OnRemoteStart( userObjID, questID )
end

function sq15_4065_saint_challenger8_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4065_saint_challenger8_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4065, 1);
				api_quest_SetJournalStep(userObjID,4065, 1);
				api_quest_SetQuestStep(userObjID,4065, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
end

</VillageServer>

<GameServer>
function sq15_4065_saint_challenger8_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 861 then
		sq15_4065_saint_challenger8_OnTalk_n861_orc_oneeye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 98 then
		sq15_4065_saint_challenger8_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n861_orc_oneeye--------------------------------------------------------------------------------
function sq15_4065_saint_challenger8_OnTalk_n861_orc_oneeye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n861_orc_oneeye-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n861_orc_oneeye-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n861_orc_oneeye-1-b" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n861_orc_oneeye-1-c";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									npc_talk_index = "n861_orc_oneeye-1-h";

				else
									npc_talk_index = "n861_orc_oneeye-1-m";

				end

				end
	end
	if npc_talk_index == "n861_orc_oneeye-1-d" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-e" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-f" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-g" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-m" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-i" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-j" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-k" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-l" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-m" then 
	end
	if npc_talk_index == "n861_orc_oneeye-1-n" then 
	end
	if npc_talk_index == "n861_orc_oneeye-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq15_4065_saint_challenger8_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n098_warrior_master_lodrigo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4065, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4065, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4065, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40650, questID, 1);
			 end 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4065_saint_challenger8_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4065);
end

function sq15_4065_saint_challenger8_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4065);
end

function sq15_4065_saint_challenger8_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4065);
	local questID=4065;
end

function sq15_4065_saint_challenger8_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4065_saint_challenger8_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4065_saint_challenger8_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4065, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4065, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4065, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
end

</GameServer>