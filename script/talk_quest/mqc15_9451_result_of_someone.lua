<VillageServer>

function mqc15_9451_result_of_someone_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1067 then
		mqc15_9451_result_of_someone_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1592 then
		mqc15_9451_result_of_someone_OnTalk_n1592_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1593 then
		mqc15_9451_result_of_someone_OnTalk_n1593_rojalin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9451_result_of_someone_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9451, 2);
				api_quest_SetJournalStep(userObjID,9451, 1);
				api_quest_SetQuestStep(userObjID,9451, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1592_velskud--------------------------------------------------------------------------------
function mqc15_9451_result_of_someone_OnTalk_n1592_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1592_velskud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1592_velskud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1592_velskud-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1592_velskud-3-b" then 
	end
	if npc_talk_index == "n1592_velskud-3-b" then 
	end
	if npc_talk_index == "n1592_velskud-3-b" then 
	end
	if npc_talk_index == "n1592_velskud-3-c" then 
	end
	if npc_talk_index == "n1592_velskud-3-d" then 
	end
	if npc_talk_index == "n1592_velskud-3-e" then 
	end
	if npc_talk_index == "n1592_velskud-3-f" then 
	end
	if npc_talk_index == "n1592_velskud-3-g" then 
	end
	if npc_talk_index == "n1592_velskud-3-h" then 
	end
	if npc_talk_index == "n1592_velskud-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1592_velskud-4-b" then 
	end
	if npc_talk_index == "n1592_velskud-4-class_check_0" then 
				if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1592_velskud-4-h";

				else
									npc_talk_index = "n1592_velskud-4-c";

				end
	end
	if npc_talk_index == "n1592_velskud-4-d" then 
	end
	if npc_talk_index == "n1592_velskud-4-e" then 
	end
	if npc_talk_index == "n1592_velskud-4-f" then 
	end
	if npc_talk_index == "n1592_velskud-4-g" then 
	end
	if npc_talk_index == "n1592_velskud-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end
	if npc_talk_index == "n1592_velskud-4-i" then 
	end
	if npc_talk_index == "n1592_velskud-4-j" then 
	end
	if npc_talk_index == "n1592_velskud-4-k" then 
	end
	if npc_talk_index == "n1592_velskud-4-l" then 
	end
	if npc_talk_index == "n1592_velskud-4-m" then 
	end
	if npc_talk_index == "n1592_velskud-4-m" then 
	end
	if npc_talk_index == "n1592_velskud-4-m" then 
	end
	if npc_talk_index == "n1592_velskud-4-n" then 
	end
	if npc_talk_index == "n1592_velskud-4-o" then 
	end
	if npc_talk_index == "n1592_velskud-4-p" then 
	end
	if npc_talk_index == "n1592_velskud-4-q" then 
	end
	if npc_talk_index == "n1592_velskud-4-r" then 
	end
	if npc_talk_index == "n1592_velskud-4-s" then 
	end
	if npc_talk_index == "n1592_velskud-4-t" then 
	end
	if npc_talk_index == "n1592_velskud-4-u" then 
	end
	if npc_talk_index == "n1592_velskud-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end
	if npc_talk_index == "n1592_velskud-5-b" then 
	end
	if npc_talk_index == "n1592_velskud-5-c" then 
	end
	if npc_talk_index == "n1592_velskud-5-d" then 
	end
	if npc_talk_index == "n1592_velskud-5-e" then 
	end
	if npc_talk_index == "n1592_velskud-5-f" then 
	end
	if npc_talk_index == "n1592_velskud-5-class_check_1" then 
				npc_talk_index = "n1592_velskud-5-g";
	end
	if npc_talk_index == "n1592_velskud-5-h" then 
	end
	if npc_talk_index == "n1592_velskud-5-i" then 
	end
	if npc_talk_index == "n1592_velskud-5-p" then 
	end
	if npc_talk_index == "n1592_velskud-5-k" then 
	end
	if npc_talk_index == "n1592_velskud-5-l" then 
	end
	if npc_talk_index == "n1592_velskud-5-m" then 
	end
	if npc_talk_index == "n1592_velskud-5-n" then 
	end
	if npc_talk_index == "n1592_velskud-5-o" then 
	end
	if npc_talk_index == "n1592_velskud-5-p" then 
	end
	if npc_talk_index == "n1592_velskud-5-q" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1593_rojalin--------------------------------------------------------------------------------
function mqc15_9451_result_of_someone_OnTalk_n1593_rojalin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9451_result_of_someone_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9451);
end

function mqc15_9451_result_of_someone_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9451);
end

function mqc15_9451_result_of_someone_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9451);
	local questID=9451;
end

function mqc15_9451_result_of_someone_OnRemoteStart( userObjID, questID )
end

function mqc15_9451_result_of_someone_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9451_result_of_someone_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9451, 2);
				api_quest_SetJournalStep(userObjID,9451, 1);
				api_quest_SetQuestStep(userObjID,9451, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</VillageServer>

<GameServer>
function mqc15_9451_result_of_someone_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1067 then
		mqc15_9451_result_of_someone_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1592 then
		mqc15_9451_result_of_someone_OnTalk_n1592_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1593 then
		mqc15_9451_result_of_someone_OnTalk_n1593_rojalin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9451_result_of_someone_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9451, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9451, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9451, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1592_velskud--------------------------------------------------------------------------------
function mqc15_9451_result_of_someone_OnTalk_n1592_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1592_velskud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1592_velskud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1592_velskud-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1592_velskud-3-b" then 
	end
	if npc_talk_index == "n1592_velskud-3-b" then 
	end
	if npc_talk_index == "n1592_velskud-3-b" then 
	end
	if npc_talk_index == "n1592_velskud-3-c" then 
	end
	if npc_talk_index == "n1592_velskud-3-d" then 
	end
	if npc_talk_index == "n1592_velskud-3-e" then 
	end
	if npc_talk_index == "n1592_velskud-3-f" then 
	end
	if npc_talk_index == "n1592_velskud-3-g" then 
	end
	if npc_talk_index == "n1592_velskud-3-h" then 
	end
	if npc_talk_index == "n1592_velskud-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1592_velskud-4-b" then 
	end
	if npc_talk_index == "n1592_velskud-4-class_check_0" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1592_velskud-4-h";

				else
									npc_talk_index = "n1592_velskud-4-c";

				end
	end
	if npc_talk_index == "n1592_velskud-4-d" then 
	end
	if npc_talk_index == "n1592_velskud-4-e" then 
	end
	if npc_talk_index == "n1592_velskud-4-f" then 
	end
	if npc_talk_index == "n1592_velskud-4-g" then 
	end
	if npc_talk_index == "n1592_velskud-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end
	if npc_talk_index == "n1592_velskud-4-i" then 
	end
	if npc_talk_index == "n1592_velskud-4-j" then 
	end
	if npc_talk_index == "n1592_velskud-4-k" then 
	end
	if npc_talk_index == "n1592_velskud-4-l" then 
	end
	if npc_talk_index == "n1592_velskud-4-m" then 
	end
	if npc_talk_index == "n1592_velskud-4-m" then 
	end
	if npc_talk_index == "n1592_velskud-4-m" then 
	end
	if npc_talk_index == "n1592_velskud-4-n" then 
	end
	if npc_talk_index == "n1592_velskud-4-o" then 
	end
	if npc_talk_index == "n1592_velskud-4-p" then 
	end
	if npc_talk_index == "n1592_velskud-4-q" then 
	end
	if npc_talk_index == "n1592_velskud-4-r" then 
	end
	if npc_talk_index == "n1592_velskud-4-s" then 
	end
	if npc_talk_index == "n1592_velskud-4-t" then 
	end
	if npc_talk_index == "n1592_velskud-4-u" then 
	end
	if npc_talk_index == "n1592_velskud-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end
	if npc_talk_index == "n1592_velskud-5-b" then 
	end
	if npc_talk_index == "n1592_velskud-5-c" then 
	end
	if npc_talk_index == "n1592_velskud-5-d" then 
	end
	if npc_talk_index == "n1592_velskud-5-e" then 
	end
	if npc_talk_index == "n1592_velskud-5-f" then 
	end
	if npc_talk_index == "n1592_velskud-5-class_check_1" then 
				npc_talk_index = "n1592_velskud-5-g";
	end
	if npc_talk_index == "n1592_velskud-5-h" then 
	end
	if npc_talk_index == "n1592_velskud-5-i" then 
	end
	if npc_talk_index == "n1592_velskud-5-p" then 
	end
	if npc_talk_index == "n1592_velskud-5-k" then 
	end
	if npc_talk_index == "n1592_velskud-5-l" then 
	end
	if npc_talk_index == "n1592_velskud-5-m" then 
	end
	if npc_talk_index == "n1592_velskud-5-n" then 
	end
	if npc_talk_index == "n1592_velskud-5-o" then 
	end
	if npc_talk_index == "n1592_velskud-5-p" then 
	end
	if npc_talk_index == "n1592_velskud-5-q" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1593_rojalin--------------------------------------------------------------------------------
function mqc15_9451_result_of_someone_OnTalk_n1593_rojalin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9451_result_of_someone_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9451);
end

function mqc15_9451_result_of_someone_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9451);
end

function mqc15_9451_result_of_someone_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9451);
	local questID=9451;
end

function mqc15_9451_result_of_someone_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9451_result_of_someone_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9451_result_of_someone_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9451, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9451, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9451, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</GameServer>