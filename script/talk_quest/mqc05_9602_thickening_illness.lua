<VillageServer>

function mqc05_9602_thickening_illness_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1861 then
		mqc05_9602_thickening_illness_OnTalk_n1861_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1893 then
		mqc05_9602_thickening_illness_OnTalk_n1893_suriya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mqc05_9602_thickening_illness_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1861_shaolong--------------------------------------------------------------------------------
function mqc05_9602_thickening_illness_OnTalk_n1861_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1861_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1861_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1861_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1861_shaolong-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1861_shaolong-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1861_shaolong-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9602, 2);
				api_quest_SetJournalStep(userObjID,9602, 1);
				api_quest_SetQuestStep(userObjID,9602, 1);
				npc_talk_index = "n1861_shaolong-1";

	end
	if npc_talk_index == "n1861_shaolong-1-b" then 
	end
	if npc_talk_index == "n1861_shaolong-1-c" then 
	end
	if npc_talk_index == "n1861_shaolong-1-d" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1861_shaolong-4-b" then 
	end
	if npc_talk_index == "n1861_shaolong-4-c" then 
	end
	if npc_talk_index == "n1861_shaolong-4-d" then 
	end
	if npc_talk_index == "n1861_shaolong-4-e" then 
	end
	if npc_talk_index == "n1861_shaolong-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1893_suriya--------------------------------------------------------------------------------
function mqc05_9602_thickening_illness_OnTalk_n1893_suriya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1893_suriya-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1893_suriya-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1893_suriya-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1893_suriya-2-b" then 
	end
	if npc_talk_index == "n1893_suriya-2-c" then 
	end
	if npc_talk_index == "n1893_suriya-2-d" then 
	end
	if npc_talk_index == "n1893_suriya-2-e" then 
	end
	if npc_talk_index == "n1893_suriya-2-f" then 
	end
	if npc_talk_index == "n1893_suriya-2-g" then 
	end
	if npc_talk_index == "n1893_suriya-2-h" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n1893_suriya-3-b" then 
	end
	if npc_talk_index == "n1893_suriya-3-c" then 
	end
	if npc_talk_index == "n1893_suriya-3-d" then 
	end
	if npc_talk_index == "n1893_suriya-3-e" then 
	end
	if npc_talk_index == "n1893_suriya-3-f" then 
	end
	if npc_talk_index == "n1893_suriya-3-g" then 
	end
	if npc_talk_index == "n1893_suriya-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc05_9602_thickening_illness_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n039_bishop_ignasio-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-5-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 96020, true);
				 api_quest_RewardQuestUser(userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 96020, true);
				 api_quest_RewardQuestUser(userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 96020, true);
				 api_quest_RewardQuestUser(userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 96020, true);
				 api_quest_RewardQuestUser(userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 96020, true);
				 api_quest_RewardQuestUser(userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 96020, true);
				 api_quest_RewardQuestUser(userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 96020, true);
				 api_quest_RewardQuestUser(userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 96020, true);
				 api_quest_RewardQuestUser(userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 96020, true);
				 api_quest_RewardQuestUser(userObjID, 96020, questID, 1);
			 end 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9603, 2);
					api_quest_SetQuestStep(userObjID, 9603, 1);
					api_quest_SetJournalStep(userObjID, 9603, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n039_bishop_ignasio-1", "mqc05_9603_unusual_disease.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9602_thickening_illness_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9602);
end

function mqc05_9602_thickening_illness_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9602);
end

function mqc05_9602_thickening_illness_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9602);
	local questID=9602;
end

function mqc05_9602_thickening_illness_OnRemoteStart( userObjID, questID )
end

function mqc05_9602_thickening_illness_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc05_9602_thickening_illness_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9602, 2);
				api_quest_SetJournalStep(userObjID,9602, 1);
				api_quest_SetQuestStep(userObjID,9602, 1);
				npc_talk_index = "n1861_shaolong-1";
end

</VillageServer>

<GameServer>
function mqc05_9602_thickening_illness_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1861 then
		mqc05_9602_thickening_illness_OnTalk_n1861_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1893 then
		mqc05_9602_thickening_illness_OnTalk_n1893_suriya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mqc05_9602_thickening_illness_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1861_shaolong--------------------------------------------------------------------------------
function mqc05_9602_thickening_illness_OnTalk_n1861_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1861_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1861_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1861_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1861_shaolong-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1861_shaolong-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1861_shaolong-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9602, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9602, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9602, 1);
				npc_talk_index = "n1861_shaolong-1";

	end
	if npc_talk_index == "n1861_shaolong-1-b" then 
	end
	if npc_talk_index == "n1861_shaolong-1-c" then 
	end
	if npc_talk_index == "n1861_shaolong-1-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1861_shaolong-4-b" then 
	end
	if npc_talk_index == "n1861_shaolong-4-c" then 
	end
	if npc_talk_index == "n1861_shaolong-4-d" then 
	end
	if npc_talk_index == "n1861_shaolong-4-e" then 
	end
	if npc_talk_index == "n1861_shaolong-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1893_suriya--------------------------------------------------------------------------------
function mqc05_9602_thickening_illness_OnTalk_n1893_suriya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1893_suriya-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1893_suriya-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1893_suriya-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1893_suriya-2-b" then 
	end
	if npc_talk_index == "n1893_suriya-2-c" then 
	end
	if npc_talk_index == "n1893_suriya-2-d" then 
	end
	if npc_talk_index == "n1893_suriya-2-e" then 
	end
	if npc_talk_index == "n1893_suriya-2-f" then 
	end
	if npc_talk_index == "n1893_suriya-2-g" then 
	end
	if npc_talk_index == "n1893_suriya-2-h" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n1893_suriya-3-b" then 
	end
	if npc_talk_index == "n1893_suriya-3-c" then 
	end
	if npc_talk_index == "n1893_suriya-3-d" then 
	end
	if npc_talk_index == "n1893_suriya-3-e" then 
	end
	if npc_talk_index == "n1893_suriya-3-f" then 
	end
	if npc_talk_index == "n1893_suriya-3-g" then 
	end
	if npc_talk_index == "n1893_suriya-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc05_9602_thickening_illness_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n039_bishop_ignasio-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-5-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96020, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96020, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96020, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96020, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96020, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96020, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96020, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96020, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96020, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96020, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96020, questID, 1);
			 end 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9603, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9603, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9603, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n039_bishop_ignasio-1", "mqc05_9603_unusual_disease.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9602_thickening_illness_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9602);
end

function mqc05_9602_thickening_illness_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9602);
end

function mqc05_9602_thickening_illness_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9602);
	local questID=9602;
end

function mqc05_9602_thickening_illness_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc05_9602_thickening_illness_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc05_9602_thickening_illness_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9602, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9602, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9602, 1);
				npc_talk_index = "n1861_shaolong-1";
end

</GameServer>