<VillageServer>

function sq05_163_lets_party_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 15 then
		sq05_163_lets_party_OnTalk_n015_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n015_sorceress_tara--------------------------------------------------------------------------------
function sq05_163_lets_party_OnTalk_n015_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n015_sorceress_tara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n015_sorceress_tara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n015_sorceress_tara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n015_sorceress_tara-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n015_sorceress_tara-accepting-class_check" then
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n015_sorceress_tara-accepting-a";

				else
									npc_talk_index = "n015_sorceress_tara-accepting-c";

				end

	end
	if npc_talk_index == "n015_sorceress_tara-accepting-set_reward1" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1632, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1633, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 end 
				npc_talk_index = "n015_sorceress_tara-accepting-e";

	end
	if npc_talk_index == "n015_sorceress_tara-accepting-set_reward2" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1632, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1633, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1631, false);
			 end 
				npc_talk_index = "n015_sorceress_tara-accepting-e";

	end
	if npc_talk_index == "n015_sorceress_tara-accepting-acceptted" then
				api_quest_AddQuest(userObjID,163, 1);
				api_quest_SetJournalStep(userObjID,163, 1);
				api_quest_SetQuestStep(userObjID,163, 1);
				npc_talk_index = "n015_sorceress_tara-1";

	end
	if npc_talk_index == "n015_sorceress_tara-1-b" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-jobconv" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n015_sorceress_tara-1-c";

				else
									npc_talk_index = "n015_sorceress_tara-1-d";

				end
	end
	if npc_talk_index == "n015_sorceress_tara-1-e" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-e" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-f" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n015_sorceress_tara-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1631, true);
				 api_quest_RewardQuestUser(userObjID, 1631, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1631, true);
				 api_quest_RewardQuestUser(userObjID, 1631, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1631, true);
				 api_quest_RewardQuestUser(userObjID, 1631, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1632, true);
				 api_quest_RewardQuestUser(userObjID, 1632, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1633, true);
				 api_quest_RewardQuestUser(userObjID, 1633, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1631, true);
				 api_quest_RewardQuestUser(userObjID, 1631, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1631, true);
				 api_quest_RewardQuestUser(userObjID, 1631, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1631, true);
				 api_quest_RewardQuestUser(userObjID, 1631, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1631, true);
				 api_quest_RewardQuestUser(userObjID, 1631, questID, 1);
			 end 
	end
	if npc_talk_index == "n015_sorceress_tara-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300202, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300202, api_quest_HasQuestItem(userObjID, 300202, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_163_lets_party_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 163);
end

function sq05_163_lets_party_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 163);
end

function sq05_163_lets_party_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 163);
	local questID=163;
end

function sq05_163_lets_party_OnRemoteStart( userObjID, questID )
end

function sq05_163_lets_party_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq05_163_lets_party_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,163, 1);
				api_quest_SetJournalStep(userObjID,163, 1);
				api_quest_SetQuestStep(userObjID,163, 1);
				npc_talk_index = "n015_sorceress_tara-1";
end

</VillageServer>

<GameServer>
function sq05_163_lets_party_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 15 then
		sq05_163_lets_party_OnTalk_n015_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n015_sorceress_tara--------------------------------------------------------------------------------
function sq05_163_lets_party_OnTalk_n015_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n015_sorceress_tara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n015_sorceress_tara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n015_sorceress_tara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n015_sorceress_tara-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n015_sorceress_tara-accepting-class_check" then
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n015_sorceress_tara-accepting-a";

				else
									npc_talk_index = "n015_sorceress_tara-accepting-c";

				end

	end
	if npc_talk_index == "n015_sorceress_tara-accepting-set_reward1" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1632, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1633, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 end 
				npc_talk_index = "n015_sorceress_tara-accepting-e";

	end
	if npc_talk_index == "n015_sorceress_tara-accepting-set_reward2" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1632, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1633, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, false);
			 end 
				npc_talk_index = "n015_sorceress_tara-accepting-e";

	end
	if npc_talk_index == "n015_sorceress_tara-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,163, 1);
				api_quest_SetJournalStep( pRoom, userObjID,163, 1);
				api_quest_SetQuestStep( pRoom, userObjID,163, 1);
				npc_talk_index = "n015_sorceress_tara-1";

	end
	if npc_talk_index == "n015_sorceress_tara-1-b" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-jobconv" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n015_sorceress_tara-1-c";

				else
									npc_talk_index = "n015_sorceress_tara-1-d";

				end
	end
	if npc_talk_index == "n015_sorceress_tara-1-e" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-e" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n015_sorceress_tara-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1631, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1631, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1631, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1632, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1632, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1633, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1633, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1631, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1631, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1631, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1631, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1631, questID, 1);
			 end 
	end
	if npc_talk_index == "n015_sorceress_tara-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300202, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300202, api_quest_HasQuestItem( pRoom, userObjID, 300202, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_163_lets_party_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 163);
end

function sq05_163_lets_party_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 163);
end

function sq05_163_lets_party_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 163);
	local questID=163;
end

function sq05_163_lets_party_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq05_163_lets_party_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq05_163_lets_party_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,163, 1);
				api_quest_SetJournalStep( pRoom, userObjID,163, 1);
				api_quest_SetQuestStep( pRoom, userObjID,163, 1);
				npc_talk_index = "n015_sorceress_tara-1";
end

</GameServer>