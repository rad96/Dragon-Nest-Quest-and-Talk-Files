<VillageServer>

function sqc16_4686_red_dragon_memoria_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1796 then
		sqc16_4686_red_dragon_memoria_OnTalk_n1796_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		sqc16_4686_red_dragon_memoria_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 999 then
		sqc16_4686_red_dragon_memoria_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 954 then
		sqc16_4686_red_dragon_memoria_OnTalk_n954_darklair_rosetta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1796_rubinat--------------------------------------------------------------------------------
function sqc16_4686_red_dragon_memoria_OnTalk_n1796_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function sqc16_4686_red_dragon_memoria_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n999_remote--------------------------------------------------------------------------------
function sqc16_4686_red_dragon_memoria_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n999_remote-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n999_remote-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4686, 1);
				api_quest_SetJournalStep(userObjID,4686, 1);
				api_quest_SetQuestStep(userObjID,4686, 1);

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n954_darklair_rosetta--------------------------------------------------------------------------------
function sqc16_4686_red_dragon_memoria_OnTalk_n954_darklair_rosetta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n954_darklair_rosetta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n954_darklair_rosetta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n954_darklair_rosetta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n954_darklair_rosetta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n954_darklair_rosetta-2" then 
				api_quest_SetQuestStep(userObjID, questID,0);
	end
	if npc_talk_index == "n954_darklair_rosetta-1-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3" then 
				api_quest_SetQuestStep(userObjID, questID,1);
	end
	if npc_talk_index == "n954_darklair_rosetta-2" then 
				api_quest_SetQuestStep(userObjID, questID,0);
	end
	if npc_talk_index == "n954_darklair_rosetta-2-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-d" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-e" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-f" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-g" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-h" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-i" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-j" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-k" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-l" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n954_darklair_rosetta-3-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-d" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-e" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-f" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-f" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-g" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-g" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-h" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-i" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-j" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-k" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-l" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-l" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-m" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-n" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-o" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n954_darklair_rosetta-4-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46860, true);
				 api_quest_RewardQuestUser(userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46860, true);
				 api_quest_RewardQuestUser(userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46860, true);
				 api_quest_RewardQuestUser(userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46860, true);
				 api_quest_RewardQuestUser(userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46860, true);
				 api_quest_RewardQuestUser(userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46860, true);
				 api_quest_RewardQuestUser(userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46860, true);
				 api_quest_RewardQuestUser(userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46860, true);
				 api_quest_RewardQuestUser(userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46860, true);
				 api_quest_RewardQuestUser(userObjID, 46860, questID, 1);
			 end 
	end
	if npc_talk_index == "n954_darklair_rosetta-4-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n954_darklair_rosetta-4-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-4-talk_back" then 
				api_npc_NextScript(userObjID, npcObjID, "001", "n954_darklair_rosetta.xml");
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4686_red_dragon_memoria_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4686);
end

function sqc16_4686_red_dragon_memoria_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4686);
end

function sqc16_4686_red_dragon_memoria_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4686);
	local questID=4686;
end

function sqc16_4686_red_dragon_memoria_OnRemoteStart( userObjID, questID )
end

function sqc16_4686_red_dragon_memoria_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc16_4686_red_dragon_memoria_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4686, 1);
				api_quest_SetJournalStep(userObjID,4686, 1);
				api_quest_SetQuestStep(userObjID,4686, 1);
end

</VillageServer>

<GameServer>
function sqc16_4686_red_dragon_memoria_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1796 then
		sqc16_4686_red_dragon_memoria_OnTalk_n1796_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		sqc16_4686_red_dragon_memoria_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 999 then
		sqc16_4686_red_dragon_memoria_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 954 then
		sqc16_4686_red_dragon_memoria_OnTalk_n954_darklair_rosetta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1796_rubinat--------------------------------------------------------------------------------
function sqc16_4686_red_dragon_memoria_OnTalk_n1796_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function sqc16_4686_red_dragon_memoria_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n999_remote--------------------------------------------------------------------------------
function sqc16_4686_red_dragon_memoria_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n999_remote-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n999_remote-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4686, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4686, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4686, 1);

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n954_darklair_rosetta--------------------------------------------------------------------------------
function sqc16_4686_red_dragon_memoria_OnTalk_n954_darklair_rosetta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n954_darklair_rosetta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n954_darklair_rosetta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n954_darklair_rosetta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n954_darklair_rosetta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n954_darklair_rosetta-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,0);
	end
	if npc_talk_index == "n954_darklair_rosetta-1-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
	end
	if npc_talk_index == "n954_darklair_rosetta-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,0);
	end
	if npc_talk_index == "n954_darklair_rosetta-2-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-d" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-e" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-f" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-g" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-h" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-i" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-j" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-k" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-l" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n954_darklair_rosetta-3-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-d" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-e" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-f" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-f" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-g" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-g" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-h" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-i" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-j" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-k" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-l" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-l" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-m" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-n" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-3-o" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n954_darklair_rosetta-4-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46860, questID, 1);
			 end 
	end
	if npc_talk_index == "n954_darklair_rosetta-4-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n954_darklair_rosetta-4-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-4-talk_back" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "001", "n954_darklair_rosetta.xml");
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4686_red_dragon_memoria_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4686);
end

function sqc16_4686_red_dragon_memoria_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4686);
end

function sqc16_4686_red_dragon_memoria_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4686);
	local questID=4686;
end

function sqc16_4686_red_dragon_memoria_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc16_4686_red_dragon_memoria_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc16_4686_red_dragon_memoria_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4686, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4686, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4686, 1);
end

</GameServer>