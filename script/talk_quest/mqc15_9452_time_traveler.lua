<VillageServer>

function mqc15_9452_time_traveler_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1067 then
		mqc15_9452_time_traveler_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1594 then
		mqc15_9452_time_traveler_OnTalk_n1594_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1561 then
		mqc15_9452_time_traveler_OnTalk_n1561_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1592 then
		mqc15_9452_time_traveler_OnTalk_n1592_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9452_time_traveler_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted_1" then
				api_quest_AddQuest(userObjID,9452, 2);
				api_quest_SetJournalStep(userObjID,9452, 1);
				api_quest_SetQuestStep(userObjID,9452, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1594_lunaria--------------------------------------------------------------------------------
function mqc15_9452_time_traveler_OnTalk_n1594_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1594_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1594_lunaria-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1594_lunaria-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1594_lunaria-2-b" then 
	end
	if npc_talk_index == "n1594_lunaria-2-c" then 
	end
	if npc_talk_index == "n1594_lunaria-2-d" then 
	end
	if npc_talk_index == "n1594_lunaria-2-e" then 
	end
	if npc_talk_index == "n1594_lunaria-2-f" then 
	end
	if npc_talk_index == "n1594_lunaria-2-g" then 
	end
	if npc_talk_index == "n1594_lunaria-2-h" then 
	end
	if npc_talk_index == "n1594_lunaria-2-i" then 
	end
	if npc_talk_index == "n1594_lunaria-2-j" then 
	end
	if npc_talk_index == "n1594_lunaria-2-k" then 
	end
	if npc_talk_index == "n1594_lunaria-2-l" then 
	end
	if npc_talk_index == "n1594_lunaria-2-m" then 
	end
	if npc_talk_index == "n1594_lunaria-2-n" then 
	end
	if npc_talk_index == "n1594_lunaria-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n1594_lunaria-3-b" then 
	end
	if npc_talk_index == "n1594_lunaria-3-c" then 
	end
	if npc_talk_index == "n1594_lunaria-3-d" then 
	end
	if npc_talk_index == "n1594_lunaria-3-e" then 
	end
	if npc_talk_index == "n1594_lunaria-3-f" then 
	end
	if npc_talk_index == "n1594_lunaria-3-g" then 
	end
	if npc_talk_index == "n1594_lunaria-3-h" then 
	end
	if npc_talk_index == "n1594_lunaria-3-i" then 
	end
	if npc_talk_index == "n1594_lunaria-3-j" then 
	end
	if npc_talk_index == "n1594_lunaria-3-k" then 
	end
	if npc_talk_index == "n1594_lunaria-3-l" then 
	end
	if npc_talk_index == "n1594_lunaria-3-m" then 
	end
	if npc_talk_index == "n1594_lunaria-3-n" then 
	end
	if npc_talk_index == "n1594_lunaria-3-o" then 
	end
	if npc_talk_index == "n1594_lunaria-3-p" then 
	end
	if npc_talk_index == "n1594_lunaria-3-q" then 
	end
	if npc_talk_index == "n1594_lunaria-3-r" then 
	end
	if npc_talk_index == "n1594_lunaria-3-s" then 
	end
	if npc_talk_index == "n1594_lunaria-3-t" then 
	end
	if npc_talk_index == "n1594_lunaria-3-u" then 
	end
	if npc_talk_index == "n1594_lunaria-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1561_for_memory--------------------------------------------------------------------------------
function mqc15_9452_time_traveler_OnTalk_n1561_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1561_for_memory-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1561_for_memory-5-b" then 
	end
	if npc_talk_index == "n1561_for_memory-5-c" then 
	end
	if npc_talk_index == "n1561_for_memory-5-d" then 
	end
	if npc_talk_index == "n1561_for_memory-5-e" then 
	end
	if npc_talk_index == "n1561_for_memory-5-f" then 
	end
	if npc_talk_index == "n1561_for_memory-5-g" then 
	end
	if npc_talk_index == "n1561_for_memory-5-h" then 
	end
	if npc_talk_index == "n1561_for_memory-5-i" then 
	end
	if npc_talk_index == "n1561_for_memory-5-j" then 
	end
	if npc_talk_index == "n1561_for_memory-5-k" then 
	end
	if npc_talk_index == "n1561_for_memory-5-l" then 
	end
	if npc_talk_index == "n1561_for_memory-5-m" then 
	end
	if npc_talk_index == "n1561_for_memory-5-n" then 
	end
	if npc_talk_index == "n1561_for_memory-5-o" then 
	end
	if npc_talk_index == "n1561_for_memory-5-p" then 
	end
	if npc_talk_index == "n1561_for_memory-5-q" then 
	end
	if npc_talk_index == "n1561_for_memory-5-r" then 
	end
	if npc_talk_index == "n1561_for_memory-5-s" then 
	end
	if npc_talk_index == "n1561_for_memory-5-t" then 
	end
	if npc_talk_index == "n1561_for_memory-5-u" then 
	end
	if npc_talk_index == "n1561_for_memory-5-v" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94520, true);
				 api_quest_RewardQuestUser(userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94520, true);
				 api_quest_RewardQuestUser(userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94520, true);
				 api_quest_RewardQuestUser(userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94520, true);
				 api_quest_RewardQuestUser(userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94520, true);
				 api_quest_RewardQuestUser(userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94520, true);
				 api_quest_RewardQuestUser(userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94520, true);
				 api_quest_RewardQuestUser(userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94520, true);
				 api_quest_RewardQuestUser(userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94520, true);
				 api_quest_RewardQuestUser(userObjID, 94520, questID, 1);
			 end 
	end
	if npc_talk_index == "n1561_for_memory-5-w" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9454, 2);
					api_quest_SetQuestStep(userObjID, 9454, 1);
					api_quest_SetJournalStep(userObjID, 9454, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1561_for_memory-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1561_for_memory-1", "mqc15_9454_hate_and_role.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1592_velskud--------------------------------------------------------------------------------
function mqc15_9452_time_traveler_OnTalk_n1592_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1592_velskud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1592_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1592_velskud-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,9452, 2);
				api_quest_SetJournalStep(userObjID,9452, 1);
				api_quest_SetQuestStep(userObjID,9452, 1);
				npc_talk_index = "n1592_velskud-1";

	end
	if npc_talk_index == "n1592_velskud-1-b" then 
	end
	if npc_talk_index == "n1592_velskud-1-c" then 
	end
	if npc_talk_index == "n1592_velskud-1-d" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9452_time_traveler_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9452);
end

function mqc15_9452_time_traveler_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9452);
end

function mqc15_9452_time_traveler_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9452);
	local questID=9452;
end

function mqc15_9452_time_traveler_OnRemoteStart( userObjID, questID )
end

function mqc15_9452_time_traveler_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9452_time_traveler_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mqc15_9452_time_traveler_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1067 then
		mqc15_9452_time_traveler_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1594 then
		mqc15_9452_time_traveler_OnTalk_n1594_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1561 then
		mqc15_9452_time_traveler_OnTalk_n1561_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1592 then
		mqc15_9452_time_traveler_OnTalk_n1592_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9452_time_traveler_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted_1" then
				api_quest_AddQuest( pRoom, userObjID,9452, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9452, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9452, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1594_lunaria--------------------------------------------------------------------------------
function mqc15_9452_time_traveler_OnTalk_n1594_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1594_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1594_lunaria-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1594_lunaria-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1594_lunaria-2-b" then 
	end
	if npc_talk_index == "n1594_lunaria-2-c" then 
	end
	if npc_talk_index == "n1594_lunaria-2-d" then 
	end
	if npc_talk_index == "n1594_lunaria-2-e" then 
	end
	if npc_talk_index == "n1594_lunaria-2-f" then 
	end
	if npc_talk_index == "n1594_lunaria-2-g" then 
	end
	if npc_talk_index == "n1594_lunaria-2-h" then 
	end
	if npc_talk_index == "n1594_lunaria-2-i" then 
	end
	if npc_talk_index == "n1594_lunaria-2-j" then 
	end
	if npc_talk_index == "n1594_lunaria-2-k" then 
	end
	if npc_talk_index == "n1594_lunaria-2-l" then 
	end
	if npc_talk_index == "n1594_lunaria-2-m" then 
	end
	if npc_talk_index == "n1594_lunaria-2-n" then 
	end
	if npc_talk_index == "n1594_lunaria-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n1594_lunaria-3-b" then 
	end
	if npc_talk_index == "n1594_lunaria-3-c" then 
	end
	if npc_talk_index == "n1594_lunaria-3-d" then 
	end
	if npc_talk_index == "n1594_lunaria-3-e" then 
	end
	if npc_talk_index == "n1594_lunaria-3-f" then 
	end
	if npc_talk_index == "n1594_lunaria-3-g" then 
	end
	if npc_talk_index == "n1594_lunaria-3-h" then 
	end
	if npc_talk_index == "n1594_lunaria-3-i" then 
	end
	if npc_talk_index == "n1594_lunaria-3-j" then 
	end
	if npc_talk_index == "n1594_lunaria-3-k" then 
	end
	if npc_talk_index == "n1594_lunaria-3-l" then 
	end
	if npc_talk_index == "n1594_lunaria-3-m" then 
	end
	if npc_talk_index == "n1594_lunaria-3-n" then 
	end
	if npc_talk_index == "n1594_lunaria-3-o" then 
	end
	if npc_talk_index == "n1594_lunaria-3-p" then 
	end
	if npc_talk_index == "n1594_lunaria-3-q" then 
	end
	if npc_talk_index == "n1594_lunaria-3-r" then 
	end
	if npc_talk_index == "n1594_lunaria-3-s" then 
	end
	if npc_talk_index == "n1594_lunaria-3-t" then 
	end
	if npc_talk_index == "n1594_lunaria-3-u" then 
	end
	if npc_talk_index == "n1594_lunaria-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1561_for_memory--------------------------------------------------------------------------------
function mqc15_9452_time_traveler_OnTalk_n1561_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1561_for_memory-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1561_for_memory-5-b" then 
	end
	if npc_talk_index == "n1561_for_memory-5-c" then 
	end
	if npc_talk_index == "n1561_for_memory-5-d" then 
	end
	if npc_talk_index == "n1561_for_memory-5-e" then 
	end
	if npc_talk_index == "n1561_for_memory-5-f" then 
	end
	if npc_talk_index == "n1561_for_memory-5-g" then 
	end
	if npc_talk_index == "n1561_for_memory-5-h" then 
	end
	if npc_talk_index == "n1561_for_memory-5-i" then 
	end
	if npc_talk_index == "n1561_for_memory-5-j" then 
	end
	if npc_talk_index == "n1561_for_memory-5-k" then 
	end
	if npc_talk_index == "n1561_for_memory-5-l" then 
	end
	if npc_talk_index == "n1561_for_memory-5-m" then 
	end
	if npc_talk_index == "n1561_for_memory-5-n" then 
	end
	if npc_talk_index == "n1561_for_memory-5-o" then 
	end
	if npc_talk_index == "n1561_for_memory-5-p" then 
	end
	if npc_talk_index == "n1561_for_memory-5-q" then 
	end
	if npc_talk_index == "n1561_for_memory-5-r" then 
	end
	if npc_talk_index == "n1561_for_memory-5-s" then 
	end
	if npc_talk_index == "n1561_for_memory-5-t" then 
	end
	if npc_talk_index == "n1561_for_memory-5-u" then 
	end
	if npc_talk_index == "n1561_for_memory-5-v" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94520, questID, 1);
			 end 
	end
	if npc_talk_index == "n1561_for_memory-5-w" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9454, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9454, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9454, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1561_for_memory-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1561_for_memory-1", "mqc15_9454_hate_and_role.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1592_velskud--------------------------------------------------------------------------------
function mqc15_9452_time_traveler_OnTalk_n1592_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1592_velskud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1592_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1592_velskud-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,9452, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9452, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9452, 1);
				npc_talk_index = "n1592_velskud-1";

	end
	if npc_talk_index == "n1592_velskud-1-b" then 
	end
	if npc_talk_index == "n1592_velskud-1-c" then 
	end
	if npc_talk_index == "n1592_velskud-1-d" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9452_time_traveler_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9452);
end

function mqc15_9452_time_traveler_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9452);
end

function mqc15_9452_time_traveler_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9452);
	local questID=9452;
end

function mqc15_9452_time_traveler_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9452_time_traveler_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9452_time_traveler_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>