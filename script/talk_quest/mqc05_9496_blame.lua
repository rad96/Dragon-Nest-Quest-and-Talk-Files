<VillageServer>

function mqc05_9496_blame_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 39 then
		mqc05_9496_blame_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mqc05_9496_blame_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 40 then
		mqc05_9496_blame_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mqc05_9496_blame_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1737 then
		mqc05_9496_blame_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 523 then
		mqc05_9496_blame_OnTalk_n523_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1751 then
		mqc05_9496_blame_OnTalk_n1751_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n039_bishop_ignasio-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9496, 2);
				api_quest_SetJournalStep(userObjID,9496, 1);
				api_quest_SetQuestStep(userObjID,9496, 1);
				npc_talk_index = "n039_bishop_ignasio-1";

	end
	if npc_talk_index == "n039_bishop_ignasio-1-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-g" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-h" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n040_king_casius--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1737_eltia--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1737_eltia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1737_eltia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-2-b" then 
	end
	if npc_talk_index == "n1737_eltia-2-c" then 
	end
	if npc_talk_index == "n1737_eltia-2-d" then 
	end
	if npc_talk_index == "n1737_eltia-2-e" then 
	end
	if npc_talk_index == "n1737_eltia-2-f" then 
	end
	if npc_talk_index == "n1737_eltia-2-g" then 
	end
	if npc_talk_index == "n1737_eltia-2-h" then 
	end
	if npc_talk_index == "n1737_eltia-2-i" then 
	end
	if npc_talk_index == "n1737_eltia-2-j" then 
	end
	if npc_talk_index == "n1737_eltia-2-k" then 
	end
	if npc_talk_index == "n1737_eltia-2-l" then 
	end
	if npc_talk_index == "n1737_eltia-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n523_velskud--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n523_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n523_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n523_velskud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n523_velskud-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94960, true);
				 api_quest_RewardQuestUser(userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94960, true);
				 api_quest_RewardQuestUser(userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94960, true);
				 api_quest_RewardQuestUser(userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94960, true);
				 api_quest_RewardQuestUser(userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94960, true);
				 api_quest_RewardQuestUser(userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94960, true);
				 api_quest_RewardQuestUser(userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94960, true);
				 api_quest_RewardQuestUser(userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94960, true);
				 api_quest_RewardQuestUser(userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94960, true);
				 api_quest_RewardQuestUser(userObjID, 94960, questID, 1);
			 end 
	end
	if npc_talk_index == "n523_velskud-4-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9497, 2);
					api_quest_SetQuestStep(userObjID, 9497, 1);
					api_quest_SetJournalStep(userObjID, 9497, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n523_velskud-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n523_velskud-1", "mqc05_9497_enough_reason.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1751_kaye--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n1751_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1751_kaye-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1751_kaye-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1751_kaye-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1751_kaye-3-b" then 
	end
	if npc_talk_index == "n1751_kaye-3-c" then 
	end
	if npc_talk_index == "n1751_kaye-3-d" then 
	end
	if npc_talk_index == "n1751_kaye-3-e" then 
	end
	if npc_talk_index == "n1751_kaye-3-f" then 
	end
	if npc_talk_index == "n1751_kaye-3-g" then 
	end
	if npc_talk_index == "n1751_kaye-3-h" then 
	end
	if npc_talk_index == "n1751_kaye-3-i" then 
	end
	if npc_talk_index == "n1751_kaye-3-j" then 
	end
	if npc_talk_index == "n1751_kaye-3-k" then 
	end
	if npc_talk_index == "n1751_kaye-3-l" then 
	end
	if npc_talk_index == "n1751_kaye-3-m" then 
	end
	if npc_talk_index == "n1751_kaye-3-n" then 
	end
	if npc_talk_index == "n1751_kaye-3-o" then 
	end
	if npc_talk_index == "n1751_kaye-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9496_blame_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9496);
end

function mqc05_9496_blame_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9496);
end

function mqc05_9496_blame_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9496);
	local questID=9496;
end

function mqc05_9496_blame_OnRemoteStart( userObjID, questID )
end

function mqc05_9496_blame_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc05_9496_blame_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9496, 2);
				api_quest_SetJournalStep(userObjID,9496, 1);
				api_quest_SetQuestStep(userObjID,9496, 1);
				npc_talk_index = "n039_bishop_ignasio-1";
end

</VillageServer>

<GameServer>
function mqc05_9496_blame_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 39 then
		mqc05_9496_blame_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mqc05_9496_blame_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 40 then
		mqc05_9496_blame_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mqc05_9496_blame_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1737 then
		mqc05_9496_blame_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 523 then
		mqc05_9496_blame_OnTalk_n523_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1751 then
		mqc05_9496_blame_OnTalk_n1751_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n039_bishop_ignasio-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9496, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9496, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9496, 1);
				npc_talk_index = "n039_bishop_ignasio-1";

	end
	if npc_talk_index == "n039_bishop_ignasio-1-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-g" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-h" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n040_king_casius--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1737_eltia--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1737_eltia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1737_eltia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-2-b" then 
	end
	if npc_talk_index == "n1737_eltia-2-c" then 
	end
	if npc_talk_index == "n1737_eltia-2-d" then 
	end
	if npc_talk_index == "n1737_eltia-2-e" then 
	end
	if npc_talk_index == "n1737_eltia-2-f" then 
	end
	if npc_talk_index == "n1737_eltia-2-g" then 
	end
	if npc_talk_index == "n1737_eltia-2-h" then 
	end
	if npc_talk_index == "n1737_eltia-2-i" then 
	end
	if npc_talk_index == "n1737_eltia-2-j" then 
	end
	if npc_talk_index == "n1737_eltia-2-k" then 
	end
	if npc_talk_index == "n1737_eltia-2-l" then 
	end
	if npc_talk_index == "n1737_eltia-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n523_velskud--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n523_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n523_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n523_velskud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n523_velskud-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94960, questID, 1);
			 end 
	end
	if npc_talk_index == "n523_velskud-4-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9497, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9497, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9497, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n523_velskud-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n523_velskud-1", "mqc05_9497_enough_reason.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1751_kaye--------------------------------------------------------------------------------
function mqc05_9496_blame_OnTalk_n1751_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1751_kaye-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1751_kaye-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1751_kaye-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1751_kaye-3-b" then 
	end
	if npc_talk_index == "n1751_kaye-3-c" then 
	end
	if npc_talk_index == "n1751_kaye-3-d" then 
	end
	if npc_talk_index == "n1751_kaye-3-e" then 
	end
	if npc_talk_index == "n1751_kaye-3-f" then 
	end
	if npc_talk_index == "n1751_kaye-3-g" then 
	end
	if npc_talk_index == "n1751_kaye-3-h" then 
	end
	if npc_talk_index == "n1751_kaye-3-i" then 
	end
	if npc_talk_index == "n1751_kaye-3-j" then 
	end
	if npc_talk_index == "n1751_kaye-3-k" then 
	end
	if npc_talk_index == "n1751_kaye-3-l" then 
	end
	if npc_talk_index == "n1751_kaye-3-m" then 
	end
	if npc_talk_index == "n1751_kaye-3-n" then 
	end
	if npc_talk_index == "n1751_kaye-3-o" then 
	end
	if npc_talk_index == "n1751_kaye-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9496_blame_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9496);
end

function mqc05_9496_blame_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9496);
end

function mqc05_9496_blame_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9496);
	local questID=9496;
end

function mqc05_9496_blame_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc05_9496_blame_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc05_9496_blame_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9496, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9496, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9496, 1);
				npc_talk_index = "n039_bishop_ignasio-1";
end

</GameServer>