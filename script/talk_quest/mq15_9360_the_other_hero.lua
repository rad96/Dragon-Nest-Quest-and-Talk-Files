<VillageServer>

function mq15_9360_the_other_hero_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 707 then
		mq15_9360_the_other_hero_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 811 then
		mq15_9360_the_other_hero_OnTalk_n811_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mq15_9360_the_other_hero_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9360, 2);
				api_quest_SetJournalStep(userObjID,9360, 1);
				api_quest_SetQuestStep(userObjID,9360, 1);
				npc_talk_index = "n707_sidel-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n811_teramai--------------------------------------------------------------------------------
function mq15_9360_the_other_hero_OnTalk_n811_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n811_teramai-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n811_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n811_teramai-1-b" then 
	end
	if npc_talk_index == "n811_teramai-1-c" then 
	end
	if npc_talk_index == "n811_teramai-1-d" then 
	end
	if npc_talk_index == "n811_teramai-1-e" then 
	end
	if npc_talk_index == "n811_teramai-1-f" then 
	end
	if npc_talk_index == "n811_teramai-1-g" then 
	end
	if npc_talk_index == "n811_teramai-1-h" then 
	end
	if npc_talk_index == "n811_teramai-1-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93600, true);
				 api_quest_RewardQuestUser(userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93600, true);
				 api_quest_RewardQuestUser(userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93600, true);
				 api_quest_RewardQuestUser(userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93600, true);
				 api_quest_RewardQuestUser(userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93600, true);
				 api_quest_RewardQuestUser(userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93600, true);
				 api_quest_RewardQuestUser(userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93600, true);
				 api_quest_RewardQuestUser(userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93600, true);
				 api_quest_RewardQuestUser(userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93600, true);
				 api_quest_RewardQuestUser(userObjID, 93600, questID, 1);
			 end 
	end
	if npc_talk_index == "n811_teramai-1-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9361, 2);
					api_quest_SetQuestStep(userObjID, 9361, 1);
					api_quest_SetJournalStep(userObjID, 9361, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n811_teramai-1-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n707_sidel-1", "mq15_9361_that_girl.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9360_the_other_hero_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9360);
end

function mq15_9360_the_other_hero_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9360);
end

function mq15_9360_the_other_hero_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9360);
	local questID=9360;
end

function mq15_9360_the_other_hero_OnRemoteStart( userObjID, questID )
end

function mq15_9360_the_other_hero_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9360_the_other_hero_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9360, 2);
				api_quest_SetJournalStep(userObjID,9360, 1);
				api_quest_SetQuestStep(userObjID,9360, 1);
				npc_talk_index = "n707_sidel-1";
end

</VillageServer>

<GameServer>
function mq15_9360_the_other_hero_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 707 then
		mq15_9360_the_other_hero_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 811 then
		mq15_9360_the_other_hero_OnTalk_n811_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mq15_9360_the_other_hero_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9360, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9360, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9360, 1);
				npc_talk_index = "n707_sidel-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n811_teramai--------------------------------------------------------------------------------
function mq15_9360_the_other_hero_OnTalk_n811_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n811_teramai-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n811_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n811_teramai-1-b" then 
	end
	if npc_talk_index == "n811_teramai-1-c" then 
	end
	if npc_talk_index == "n811_teramai-1-d" then 
	end
	if npc_talk_index == "n811_teramai-1-e" then 
	end
	if npc_talk_index == "n811_teramai-1-f" then 
	end
	if npc_talk_index == "n811_teramai-1-g" then 
	end
	if npc_talk_index == "n811_teramai-1-h" then 
	end
	if npc_talk_index == "n811_teramai-1-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93600, questID, 1);
			 end 
	end
	if npc_talk_index == "n811_teramai-1-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9361, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9361, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9361, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n811_teramai-1-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n707_sidel-1", "mq15_9361_that_girl.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9360_the_other_hero_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9360);
end

function mq15_9360_the_other_hero_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9360);
end

function mq15_9360_the_other_hero_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9360);
	local questID=9360;
end

function mq15_9360_the_other_hero_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9360_the_other_hero_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9360_the_other_hero_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9360, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9360, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9360, 1);
				npc_talk_index = "n707_sidel-1";
end

</GameServer>