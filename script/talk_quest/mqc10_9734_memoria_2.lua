<VillageServer>

function mqc10_9734_memoria_2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 735 then
		mqc10_9734_memoria_2_OnTalk_n735_darklair_mocha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 748 then
		mqc10_9734_memoria_2_OnTalk_n748_night_kasius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 749 then
		mqc10_9734_memoria_2_OnTalk_n749_night_kasius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n735_darklair_mocha--------------------------------------------------------------------------------
function mqc10_9734_memoria_2_OnTalk_n735_darklair_mocha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n735_darklair_mocha-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n735_darklair_mocha-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n735_darklair_mocha-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9734, 2);
				api_quest_SetJournalStep(userObjID,9734, 1);
				api_quest_SetQuestStep(userObjID,9734, 1);
				npc_talk_index = "n735_darklair_mocha-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n748_night_kasius--------------------------------------------------------------------------------
function mqc10_9734_memoria_2_OnTalk_n748_night_kasius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n748_night_kasius-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n749_night_kasius--------------------------------------------------------------------------------
function mqc10_9734_memoria_2_OnTalk_n749_night_kasius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n749_night_kasius-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n749_night_kasius-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n749_night_kasius-2-b" then 
	end
	if npc_talk_index == "n749_night_kasius-2-c" then 
	end
	if npc_talk_index == "n749_night_kasius-2-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97340, true);
				 api_quest_RewardQuestUser(userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97340, true);
				 api_quest_RewardQuestUser(userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97340, true);
				 api_quest_RewardQuestUser(userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97340, true);
				 api_quest_RewardQuestUser(userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97340, true);
				 api_quest_RewardQuestUser(userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97340, true);
				 api_quest_RewardQuestUser(userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97340, true);
				 api_quest_RewardQuestUser(userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97340, true);
				 api_quest_RewardQuestUser(userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97340, true);
				 api_quest_RewardQuestUser(userObjID, 97340, questID, 1);
			 end 
	end
	if npc_talk_index == "n749_night_kasius-2-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9735, 2);
					api_quest_SetQuestStep(userObjID, 9735, 1);
					api_quest_SetJournalStep(userObjID, 9735, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n749_night_kasius-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n749_night_kasius-1", "mqc10_9735_memoria_3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc10_9734_memoria_2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9734);
end

function mqc10_9734_memoria_2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9734);
end

function mqc10_9734_memoria_2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9734);
	local questID=9734;
end

function mqc10_9734_memoria_2_OnRemoteStart( userObjID, questID )
end

function mqc10_9734_memoria_2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc10_9734_memoria_2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9734, 2);
				api_quest_SetJournalStep(userObjID,9734, 1);
				api_quest_SetQuestStep(userObjID,9734, 1);
				npc_talk_index = "n735_darklair_mocha-1";
end

</VillageServer>

<GameServer>
function mqc10_9734_memoria_2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 735 then
		mqc10_9734_memoria_2_OnTalk_n735_darklair_mocha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 748 then
		mqc10_9734_memoria_2_OnTalk_n748_night_kasius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 749 then
		mqc10_9734_memoria_2_OnTalk_n749_night_kasius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n735_darklair_mocha--------------------------------------------------------------------------------
function mqc10_9734_memoria_2_OnTalk_n735_darklair_mocha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n735_darklair_mocha-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n735_darklair_mocha-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n735_darklair_mocha-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9734, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9734, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9734, 1);
				npc_talk_index = "n735_darklair_mocha-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n748_night_kasius--------------------------------------------------------------------------------
function mqc10_9734_memoria_2_OnTalk_n748_night_kasius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n748_night_kasius-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n749_night_kasius--------------------------------------------------------------------------------
function mqc10_9734_memoria_2_OnTalk_n749_night_kasius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n749_night_kasius-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n749_night_kasius-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n749_night_kasius-2-b" then 
	end
	if npc_talk_index == "n749_night_kasius-2-c" then 
	end
	if npc_talk_index == "n749_night_kasius-2-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97340, questID, 1);
			 end 
	end
	if npc_talk_index == "n749_night_kasius-2-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9735, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9735, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9735, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n749_night_kasius-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n749_night_kasius-1", "mqc10_9735_memoria_3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc10_9734_memoria_2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9734);
end

function mqc10_9734_memoria_2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9734);
end

function mqc10_9734_memoria_2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9734);
	local questID=9734;
end

function mqc10_9734_memoria_2_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc10_9734_memoria_2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc10_9734_memoria_2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9734, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9734, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9734, 1);
				npc_talk_index = "n735_darklair_mocha-1";
end

</GameServer>