<VillageServer>

function sq11_584_unfinished_war_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 42 then
		sq11_584_unfinished_war_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function sq11_584_unfinished_war_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n042_general_duglars-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5841, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5842, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5843, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5844, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5845, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5846, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5847, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5848, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5849, false);
			 end 

	end
	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest(userObjID,584, 1);
				api_quest_SetJournalStep(userObjID,584, 1);
				api_quest_SetQuestStep(userObjID,584, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1034, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201034, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400187, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_DelQuestItem(userObjID, 400187, 1);
	end
	if npc_talk_index == "n042_general_duglars-3-b" then 
	end
	if npc_talk_index == "n042_general_duglars-3-c" then 
	end
	if npc_talk_index == "n042_general_duglars-3-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5841, true);
				 api_quest_RewardQuestUser(userObjID, 5841, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5842, true);
				 api_quest_RewardQuestUser(userObjID, 5842, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5843, true);
				 api_quest_RewardQuestUser(userObjID, 5843, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5844, true);
				 api_quest_RewardQuestUser(userObjID, 5844, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5845, true);
				 api_quest_RewardQuestUser(userObjID, 5845, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5846, true);
				 api_quest_RewardQuestUser(userObjID, 5846, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5847, true);
				 api_quest_RewardQuestUser(userObjID, 5847, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5848, true);
				 api_quest_RewardQuestUser(userObjID, 5848, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5849, true);
				 api_quest_RewardQuestUser(userObjID, 5849, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-3-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_584_unfinished_war_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 584);
	if qstep == 1 and CountIndex == 1034 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400187, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400187, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400187 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201034 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400187, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400187, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_584_unfinished_war_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 584);
	if qstep == 1 and CountIndex == 1034 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400187 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201034 and Count >= TargetCount  then

	end
end

function sq11_584_unfinished_war_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 584);
	local questID=584;
end

function sq11_584_unfinished_war_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1034, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201034, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400187, 1);
end

function sq11_584_unfinished_war_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_584_unfinished_war_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,584, 1);
				api_quest_SetJournalStep(userObjID,584, 1);
				api_quest_SetQuestStep(userObjID,584, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1034, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201034, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400187, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</VillageServer>

<GameServer>
function sq11_584_unfinished_war_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 42 then
		sq11_584_unfinished_war_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function sq11_584_unfinished_war_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n042_general_duglars-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5841, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5842, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5843, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5844, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5845, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5846, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5847, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5848, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5849, false);
			 end 

	end
	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,584, 1);
				api_quest_SetJournalStep( pRoom, userObjID,584, 1);
				api_quest_SetQuestStep( pRoom, userObjID,584, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1034, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201034, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400187, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_DelQuestItem( pRoom, userObjID, 400187, 1);
	end
	if npc_talk_index == "n042_general_duglars-3-b" then 
	end
	if npc_talk_index == "n042_general_duglars-3-c" then 
	end
	if npc_talk_index == "n042_general_duglars-3-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5841, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5841, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5842, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5842, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5843, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5843, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5844, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5844, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5845, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5845, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5846, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5846, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5847, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5847, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5848, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5848, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5849, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5849, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-3-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_584_unfinished_war_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 584);
	if qstep == 1 and CountIndex == 1034 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400187, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400187, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400187 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201034 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400187, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400187, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_584_unfinished_war_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 584);
	if qstep == 1 and CountIndex == 1034 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400187 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201034 and Count >= TargetCount  then

	end
end

function sq11_584_unfinished_war_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 584);
	local questID=584;
end

function sq11_584_unfinished_war_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1034, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201034, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400187, 1);
end

function sq11_584_unfinished_war_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_584_unfinished_war_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,584, 1);
				api_quest_SetJournalStep( pRoom, userObjID,584, 1);
				api_quest_SetQuestStep( pRoom, userObjID,584, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1034, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201034, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400187, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</GameServer>