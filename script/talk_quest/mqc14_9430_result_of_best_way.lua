<VillageServer>

function mqc14_9430_result_of_best_way_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1556 then
		mqc14_9430_result_of_best_way_OnTalk_n1556_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1557 then
		mqc14_9430_result_of_best_way_OnTalk_n1557_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1138 then
		mqc14_9430_result_of_best_way_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1555 then
		mqc14_9430_result_of_best_way_OnTalk_n1555_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1516 then
		mqc14_9430_result_of_best_way_OnTalk_n1516_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1529 then
		mqc14_9430_result_of_best_way_OnTalk_n1529_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1556_gereint_kid--------------------------------------------------------------------------------
function mqc14_9430_result_of_best_way_OnTalk_n1556_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1556_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1557_argenta--------------------------------------------------------------------------------
function mqc14_9430_result_of_best_way_OnTalk_n1557_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1557_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1557_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1557_argenta-1-b" then 
	end
	if npc_talk_index == "n1557_argenta-1-c" then 
	end
	if npc_talk_index == "n1557_argenta-1-d" then 
	end
	if npc_talk_index == "n1557_argenta-1-e" then 
	end
	if npc_talk_index == "n1557_argenta-1-f" then 
	end
	if npc_talk_index == "n1557_argenta-1-g" then 
	end
	if npc_talk_index == "n1557_argenta-1-h" then 
	end
	if npc_talk_index == "n1557_argenta-1-h" then 
	end
	if npc_talk_index == "n1557_argenta-1-h" then 
	end
	if npc_talk_index == "n1557_argenta-1-i" then 
	end
	if npc_talk_index == "n1557_argenta-1-i" then 
	end
	if npc_talk_index == "n1557_argenta-1-i" then 
	end
	if npc_talk_index == "n1557_argenta-1-j" then 
	end
	if npc_talk_index == "n1557_argenta-1-k" then 
	end
	if npc_talk_index == "n1557_argenta-1-l" then 
	end
	if npc_talk_index == "n1557_argenta-1-m" then 
	end
	if npc_talk_index == "n1557_argenta-1-n" then 
	end
	if npc_talk_index == "n1557_argenta-1-o" then 
	end
	if npc_talk_index == "n1557_argenta-1-p" then 
	end
	if npc_talk_index == "n1557_argenta-1-q" then 
	end
	if npc_talk_index == "n1557_argenta-1-r" then 
	end
	if npc_talk_index == "n1557_argenta-1-s" then 
	end
	if npc_talk_index == "n1557_argenta-1-t" then 
	end
	if npc_talk_index == "n1557_argenta-1-u" then 
	end
	if npc_talk_index == "n1557_argenta-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function mqc14_9430_result_of_best_way_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1138_elf_guard_sitredel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9430, 2);
				api_quest_SetJournalStep(userObjID,9430, 1);
				api_quest_SetQuestStep(userObjID,9430, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1555_for_memory--------------------------------------------------------------------------------
function mqc14_9430_result_of_best_way_OnTalk_n1555_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1555_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1516_argenta--------------------------------------------------------------------------------
function mqc14_9430_result_of_best_way_OnTalk_n1516_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1516_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1516_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1516_argenta-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1516_argenta-3-b" then 
	end
	if npc_talk_index == "n1516_argenta-3-c" then 
	end
	if npc_talk_index == "n1516_argenta-3-d" then 
	end
	if npc_talk_index == "n1516_argenta-3-e" then 
	end
	if npc_talk_index == "n1516_argenta-3-e" then 
	end
	if npc_talk_index == "n1516_argenta-3-e" then 
	end
	if npc_talk_index == "n1516_argenta-3-f" then 
	end
	if npc_talk_index == "n1516_argenta-3-g" then 
	end
	if npc_talk_index == "n1516_argenta-3-h" then 
	end
	if npc_talk_index == "n1516_argenta-3-i" then 
	end
	if npc_talk_index == "n1516_argenta-3-j" then 
	end
	if npc_talk_index == "n1516_argenta-3-k" then 
	end
	if npc_talk_index == "n1516_argenta-3-l" then 
	end
	if npc_talk_index == "n1516_argenta-3-m" then 
	end
	if npc_talk_index == "n1516_argenta-3-n" then 
	end
	if npc_talk_index == "n1516_argenta-3-o" then 
	end
	if npc_talk_index == "n1516_argenta-3-p" then 
	end
	if npc_talk_index == "n1516_argenta-3-q" then 
	end
	if npc_talk_index == "n1516_argenta-3-r" then 
	end
	if npc_talk_index == "n1516_argenta-3-s" then 
	end
	if npc_talk_index == "n1516_argenta-3-t" then 
	end
	if npc_talk_index == "n1516_argenta-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1516_argenta-4-chk_class" then 
				if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1516_argenta-4-s";

				else
									if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1516_argenta-4-k";

				else
									npc_talk_index = "n1516_argenta-4-a";

				end

				end
	end
	if npc_talk_index == "n1516_argenta-4-b" then 
	end
	if npc_talk_index == "n1516_argenta-4-c" then 
	end
	if npc_talk_index == "n1516_argenta-4-anysaychk" then 
				if api_user_GetUserClassID(userObjID) == 4 then
									npc_talk_index = "n1516_argenta-4-g";

				else
									if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1516_argenta-4-h";

				else
									if api_user_GetUserClassID(userObjID) == 2 then
									npc_talk_index = "n1516_argenta-4-j";

				else
									npc_talk_index = "n1516_argenta-4-d";

				end

				end

				end
	end
	if npc_talk_index == "n1516_argenta-4-e" then 
	end
	if npc_talk_index == "n1516_argenta-4-f" then 
	end
	if npc_talk_index == "n1516_argenta-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1516_argenta-4-e" then 
	end
	if npc_talk_index == "n1516_argenta-4-i" then 
	end
	if npc_talk_index == "n1516_argenta-4-e" then 
	end
	if npc_talk_index == "n1516_argenta-4-e" then 
	end
	if npc_talk_index == "n1516_argenta-4-l" then 
	end
	if npc_talk_index == "n1516_argenta-4-m" then 
	end
	if npc_talk_index == "n1516_argenta-4-n" then 
	end
	if npc_talk_index == "n1516_argenta-4-o" then 
	end
	if npc_talk_index == "n1516_argenta-4-p" then 
	end
	if npc_talk_index == "n1516_argenta-4-q" then 
	end
	if npc_talk_index == "n1516_argenta-4-q" then 
	end
	if npc_talk_index == "n1516_argenta-4-q" then 
	end
	if npc_talk_index == "n1516_argenta-4-r" then 
	end
	if npc_talk_index == "n1516_argenta-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1516_argenta-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1516_argenta-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1516_argenta-4-t" then 
	end
	if npc_talk_index == "n1516_argenta-4-u" then 
	end
	if npc_talk_index == "n1516_argenta-4-v" then 
	end
	if npc_talk_index == "n1516_argenta-4-w" then 
	end
	if npc_talk_index == "n1516_argenta-4-x" then 
	end
	if npc_talk_index == "n1516_argenta-4-y" then 
	end
	if npc_talk_index == "n1516_argenta-4-y" then 
	end
	if npc_talk_index == "n1516_argenta-4-y" then 
	end
	if npc_talk_index == "n1516_argenta-4-z" then 
	end
	if npc_talk_index == "n1516_argenta-4-z" then 
	end
	if npc_talk_index == "n1516_argenta-4-z" then 
	end
	if npc_talk_index == "n1516_argenta-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1516_argenta-5-b" then 
	end
	if npc_talk_index == "n1516_argenta-5-c" then 
	end
	if npc_talk_index == "n1516_argenta-5-d" then 
	end
	if npc_talk_index == "n1516_argenta-5-e" then 
	end
	if npc_talk_index == "n1516_argenta-5-e" then 
	end
	if npc_talk_index == "n1516_argenta-5-f" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1529_gaharam--------------------------------------------------------------------------------
function mqc14_9430_result_of_best_way_OnTalk_n1529_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1529_gaharam-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1529_gaharam-6-b" then 
				if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n1529_gaharam-6-b";

				else
									npc_talk_index = "n1529_gaharam-6-a";

				end
	end
	if npc_talk_index == "n1529_gaharam-6-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 end 
	end
	if npc_talk_index == "n1529_gaharam-6-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94300, true);
				 api_quest_RewardQuestUser(userObjID, 94300, questID, 1);
			 end 
	end
	if npc_talk_index == "n1529_gaharam-6-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9431, 2);
					api_quest_SetQuestStep(userObjID, 9431, 1);
					api_quest_SetJournalStep(userObjID, 9431, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1529_gaharam-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1529_gaharam-1", "mqc15_9431_start_of_journey.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9430_result_of_best_way_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9430);
end

function mqc14_9430_result_of_best_way_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9430);
end

function mqc14_9430_result_of_best_way_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9430);
	local questID=9430;
end

function mqc14_9430_result_of_best_way_OnRemoteStart( userObjID, questID )
end

function mqc14_9430_result_of_best_way_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc14_9430_result_of_best_way_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9430, 2);
				api_quest_SetJournalStep(userObjID,9430, 1);
				api_quest_SetQuestStep(userObjID,9430, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";
end

</VillageServer>

<GameServer>
function mqc14_9430_result_of_best_way_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1556 then
		mqc14_9430_result_of_best_way_OnTalk_n1556_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1557 then
		mqc14_9430_result_of_best_way_OnTalk_n1557_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1138 then
		mqc14_9430_result_of_best_way_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1555 then
		mqc14_9430_result_of_best_way_OnTalk_n1555_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1516 then
		mqc14_9430_result_of_best_way_OnTalk_n1516_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1529 then
		mqc14_9430_result_of_best_way_OnTalk_n1529_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1556_gereint_kid--------------------------------------------------------------------------------
function mqc14_9430_result_of_best_way_OnTalk_n1556_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1556_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1557_argenta--------------------------------------------------------------------------------
function mqc14_9430_result_of_best_way_OnTalk_n1557_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1557_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1557_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1557_argenta-1-b" then 
	end
	if npc_talk_index == "n1557_argenta-1-c" then 
	end
	if npc_talk_index == "n1557_argenta-1-d" then 
	end
	if npc_talk_index == "n1557_argenta-1-e" then 
	end
	if npc_talk_index == "n1557_argenta-1-f" then 
	end
	if npc_talk_index == "n1557_argenta-1-g" then 
	end
	if npc_talk_index == "n1557_argenta-1-h" then 
	end
	if npc_talk_index == "n1557_argenta-1-h" then 
	end
	if npc_talk_index == "n1557_argenta-1-h" then 
	end
	if npc_talk_index == "n1557_argenta-1-i" then 
	end
	if npc_talk_index == "n1557_argenta-1-i" then 
	end
	if npc_talk_index == "n1557_argenta-1-i" then 
	end
	if npc_talk_index == "n1557_argenta-1-j" then 
	end
	if npc_talk_index == "n1557_argenta-1-k" then 
	end
	if npc_talk_index == "n1557_argenta-1-l" then 
	end
	if npc_talk_index == "n1557_argenta-1-m" then 
	end
	if npc_talk_index == "n1557_argenta-1-n" then 
	end
	if npc_talk_index == "n1557_argenta-1-o" then 
	end
	if npc_talk_index == "n1557_argenta-1-p" then 
	end
	if npc_talk_index == "n1557_argenta-1-q" then 
	end
	if npc_talk_index == "n1557_argenta-1-r" then 
	end
	if npc_talk_index == "n1557_argenta-1-s" then 
	end
	if npc_talk_index == "n1557_argenta-1-t" then 
	end
	if npc_talk_index == "n1557_argenta-1-u" then 
	end
	if npc_talk_index == "n1557_argenta-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function mqc14_9430_result_of_best_way_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1138_elf_guard_sitredel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9430, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9430, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9430, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1555_for_memory--------------------------------------------------------------------------------
function mqc14_9430_result_of_best_way_OnTalk_n1555_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1555_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1516_argenta--------------------------------------------------------------------------------
function mqc14_9430_result_of_best_way_OnTalk_n1516_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1516_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1516_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1516_argenta-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1516_argenta-3-b" then 
	end
	if npc_talk_index == "n1516_argenta-3-c" then 
	end
	if npc_talk_index == "n1516_argenta-3-d" then 
	end
	if npc_talk_index == "n1516_argenta-3-e" then 
	end
	if npc_talk_index == "n1516_argenta-3-e" then 
	end
	if npc_talk_index == "n1516_argenta-3-e" then 
	end
	if npc_talk_index == "n1516_argenta-3-f" then 
	end
	if npc_talk_index == "n1516_argenta-3-g" then 
	end
	if npc_talk_index == "n1516_argenta-3-h" then 
	end
	if npc_talk_index == "n1516_argenta-3-i" then 
	end
	if npc_talk_index == "n1516_argenta-3-j" then 
	end
	if npc_talk_index == "n1516_argenta-3-k" then 
	end
	if npc_talk_index == "n1516_argenta-3-l" then 
	end
	if npc_talk_index == "n1516_argenta-3-m" then 
	end
	if npc_talk_index == "n1516_argenta-3-n" then 
	end
	if npc_talk_index == "n1516_argenta-3-o" then 
	end
	if npc_talk_index == "n1516_argenta-3-p" then 
	end
	if npc_talk_index == "n1516_argenta-3-q" then 
	end
	if npc_talk_index == "n1516_argenta-3-r" then 
	end
	if npc_talk_index == "n1516_argenta-3-s" then 
	end
	if npc_talk_index == "n1516_argenta-3-t" then 
	end
	if npc_talk_index == "n1516_argenta-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1516_argenta-4-chk_class" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1516_argenta-4-s";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1516_argenta-4-k";

				else
									npc_talk_index = "n1516_argenta-4-a";

				end

				end
	end
	if npc_talk_index == "n1516_argenta-4-b" then 
	end
	if npc_talk_index == "n1516_argenta-4-c" then 
	end
	if npc_talk_index == "n1516_argenta-4-anysaychk" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									npc_talk_index = "n1516_argenta-4-g";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1516_argenta-4-h";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									npc_talk_index = "n1516_argenta-4-j";

				else
									npc_talk_index = "n1516_argenta-4-d";

				end

				end

				end
	end
	if npc_talk_index == "n1516_argenta-4-e" then 
	end
	if npc_talk_index == "n1516_argenta-4-f" then 
	end
	if npc_talk_index == "n1516_argenta-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1516_argenta-4-e" then 
	end
	if npc_talk_index == "n1516_argenta-4-i" then 
	end
	if npc_talk_index == "n1516_argenta-4-e" then 
	end
	if npc_talk_index == "n1516_argenta-4-e" then 
	end
	if npc_talk_index == "n1516_argenta-4-l" then 
	end
	if npc_talk_index == "n1516_argenta-4-m" then 
	end
	if npc_talk_index == "n1516_argenta-4-n" then 
	end
	if npc_talk_index == "n1516_argenta-4-o" then 
	end
	if npc_talk_index == "n1516_argenta-4-p" then 
	end
	if npc_talk_index == "n1516_argenta-4-q" then 
	end
	if npc_talk_index == "n1516_argenta-4-q" then 
	end
	if npc_talk_index == "n1516_argenta-4-q" then 
	end
	if npc_talk_index == "n1516_argenta-4-r" then 
	end
	if npc_talk_index == "n1516_argenta-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1516_argenta-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1516_argenta-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1516_argenta-4-t" then 
	end
	if npc_talk_index == "n1516_argenta-4-u" then 
	end
	if npc_talk_index == "n1516_argenta-4-v" then 
	end
	if npc_talk_index == "n1516_argenta-4-w" then 
	end
	if npc_talk_index == "n1516_argenta-4-x" then 
	end
	if npc_talk_index == "n1516_argenta-4-y" then 
	end
	if npc_talk_index == "n1516_argenta-4-y" then 
	end
	if npc_talk_index == "n1516_argenta-4-y" then 
	end
	if npc_talk_index == "n1516_argenta-4-z" then 
	end
	if npc_talk_index == "n1516_argenta-4-z" then 
	end
	if npc_talk_index == "n1516_argenta-4-z" then 
	end
	if npc_talk_index == "n1516_argenta-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1516_argenta-5-b" then 
	end
	if npc_talk_index == "n1516_argenta-5-c" then 
	end
	if npc_talk_index == "n1516_argenta-5-d" then 
	end
	if npc_talk_index == "n1516_argenta-5-e" then 
	end
	if npc_talk_index == "n1516_argenta-5-e" then 
	end
	if npc_talk_index == "n1516_argenta-5-f" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1529_gaharam--------------------------------------------------------------------------------
function mqc14_9430_result_of_best_way_OnTalk_n1529_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1529_gaharam-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1529_gaharam-6-b" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n1529_gaharam-6-b";

				else
									npc_talk_index = "n1529_gaharam-6-a";

				end
	end
	if npc_talk_index == "n1529_gaharam-6-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 end 
	end
	if npc_talk_index == "n1529_gaharam-6-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94300, questID, 1);
			 end 
	end
	if npc_talk_index == "n1529_gaharam-6-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9431, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9431, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9431, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1529_gaharam-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1529_gaharam-1", "mqc15_9431_start_of_journey.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9430_result_of_best_way_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9430);
end

function mqc14_9430_result_of_best_way_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9430);
end

function mqc14_9430_result_of_best_way_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9430);
	local questID=9430;
end

function mqc14_9430_result_of_best_way_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc14_9430_result_of_best_way_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc14_9430_result_of_best_way_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9430, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9430, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9430, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";
end

</GameServer>