<VillageServer>

function sq11_549_jewelry_black_sigh_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 277 then
		sq11_549_jewelry_black_sigh_OnTalk_n277_bluff_dealers_popo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n277_bluff_dealers_popo--------------------------------------------------------------------------------
function sq11_549_jewelry_black_sigh_OnTalk_n277_bluff_dealers_popo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n277_bluff_dealers_popo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n277_bluff_dealers_popo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n277_bluff_dealers_popo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n277_bluff_dealers_popo-accepting-h" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5491, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5492, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5493, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5494, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5495, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5496, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5497, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5498, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5499, false);
			 end 

	end
	if npc_talk_index == "n277_bluff_dealers_popo-accepting-acceptted" then
				api_quest_AddQuest(userObjID,549, 1);
				api_quest_SetJournalStep(userObjID,549, 1);
				api_quest_SetQuestStep(userObjID,549, 1);
				npc_talk_index = "n277_bluff_dealers_popo-1";

	end
	if npc_talk_index == "n277_bluff_dealers_popo-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1159, 30001);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400155, 30001);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestMemo(userObjID, questID, 1, 1);
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-haveitem1" then 
				if api_quest_HasQuestItem(userObjID, 400155, 1) >= 1 then
									npc_talk_index = "n277_bluff_dealers_popo-2-a";

				else
									npc_talk_index = "n277_bluff_dealers_popo-2-f";

				end
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-b" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-c" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-g" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-haveitem2" then 
				if api_quest_HasQuestItem(userObjID, 400155, 1) >= 1 then
									npc_talk_index = "n277_bluff_dealers_popo-2-a";

				else
									npc_talk_index = "n277_bluff_dealers_popo-2-f";

				end
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-e" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-h" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5491, true);
				 api_quest_RewardQuestUser(userObjID, 5491, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5492, true);
				 api_quest_RewardQuestUser(userObjID, 5492, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5493, true);
				 api_quest_RewardQuestUser(userObjID, 5493, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5494, true);
				 api_quest_RewardQuestUser(userObjID, 5494, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5495, true);
				 api_quest_RewardQuestUser(userObjID, 5495, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5496, true);
				 api_quest_RewardQuestUser(userObjID, 5496, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5497, true);
				 api_quest_RewardQuestUser(userObjID, 5497, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5498, true);
				 api_quest_RewardQuestUser(userObjID, 5498, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5499, true);
				 api_quest_RewardQuestUser(userObjID, 5499, questID, 1);
			 end 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400155, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400155, api_quest_HasQuestItem(userObjID, 400155, 1));
				end
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-k" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_549_jewelry_black_sigh_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 549);
	if qstep == 2 and CountIndex == 1159 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400155, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400155, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400155 then
				if api_quest_HasQuestItem(userObjID, 400155, 1) >= 1 then
									api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 201159 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400155, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400155, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_549_jewelry_black_sigh_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 549);
	if qstep == 2 and CountIndex == 1159 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400155 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201159 and Count >= TargetCount  then

	end
end

function sq11_549_jewelry_black_sigh_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 549);
	local questID=549;
end

function sq11_549_jewelry_black_sigh_OnRemoteStart( userObjID, questID )
end

function sq11_549_jewelry_black_sigh_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_549_jewelry_black_sigh_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,549, 1);
				api_quest_SetJournalStep(userObjID,549, 1);
				api_quest_SetQuestStep(userObjID,549, 1);
				npc_talk_index = "n277_bluff_dealers_popo-1";
end

</VillageServer>

<GameServer>
function sq11_549_jewelry_black_sigh_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 277 then
		sq11_549_jewelry_black_sigh_OnTalk_n277_bluff_dealers_popo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n277_bluff_dealers_popo--------------------------------------------------------------------------------
function sq11_549_jewelry_black_sigh_OnTalk_n277_bluff_dealers_popo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n277_bluff_dealers_popo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n277_bluff_dealers_popo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n277_bluff_dealers_popo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n277_bluff_dealers_popo-accepting-h" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5491, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5492, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5493, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5494, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5495, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5496, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5497, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5498, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5499, false);
			 end 

	end
	if npc_talk_index == "n277_bluff_dealers_popo-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,549, 1);
				api_quest_SetJournalStep( pRoom, userObjID,549, 1);
				api_quest_SetQuestStep( pRoom, userObjID,549, 1);
				npc_talk_index = "n277_bluff_dealers_popo-1";

	end
	if npc_talk_index == "n277_bluff_dealers_popo-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1159, 30001);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400155, 30001);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 1);
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-haveitem1" then 
				if api_quest_HasQuestItem( pRoom, userObjID, 400155, 1) >= 1 then
									npc_talk_index = "n277_bluff_dealers_popo-2-a";

				else
									npc_talk_index = "n277_bluff_dealers_popo-2-f";

				end
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-b" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-c" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-g" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-haveitem2" then 
				if api_quest_HasQuestItem( pRoom, userObjID, 400155, 1) >= 1 then
									npc_talk_index = "n277_bluff_dealers_popo-2-a";

				else
									npc_talk_index = "n277_bluff_dealers_popo-2-f";

				end
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-e" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-h" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5491, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5491, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5492, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5492, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5493, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5493, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5494, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5494, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5495, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5495, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5496, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5496, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5497, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5497, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5498, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5498, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5499, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5499, questID, 1);
			 end 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400155, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400155, api_quest_HasQuestItem( pRoom, userObjID, 400155, 1));
				end
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-k" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_549_jewelry_black_sigh_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 549);
	if qstep == 2 and CountIndex == 1159 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400155, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400155, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400155 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400155, 1) >= 1 then
									api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 201159 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400155, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400155, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_549_jewelry_black_sigh_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 549);
	if qstep == 2 and CountIndex == 1159 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400155 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201159 and Count >= TargetCount  then

	end
end

function sq11_549_jewelry_black_sigh_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 549);
	local questID=549;
end

function sq11_549_jewelry_black_sigh_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_549_jewelry_black_sigh_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_549_jewelry_black_sigh_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,549, 1);
				api_quest_SetJournalStep( pRoom, userObjID,549, 1);
				api_quest_SetQuestStep( pRoom, userObjID,549, 1);
				npc_talk_index = "n277_bluff_dealers_popo-1";
end

</GameServer>