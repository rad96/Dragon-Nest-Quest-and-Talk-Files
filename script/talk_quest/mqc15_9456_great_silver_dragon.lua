<VillageServer>

function mqc15_9456_great_silver_dragon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1067 then
		mqc15_9456_great_silver_dragon_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1503 then
		mqc15_9456_great_silver_dragon_OnTalk_n1503_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1598 then
		mqc15_9456_great_silver_dragon_OnTalk_n1598_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1587 then
		mqc15_9456_great_silver_dragon_OnTalk_n1587_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9456_great_silver_dragon_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9456, 2);
				api_quest_SetJournalStep(userObjID,9456, 1);
				api_quest_SetQuestStep(userObjID,9456, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1503_gereint_kid--------------------------------------------------------------------------------
function mqc15_9456_great_silver_dragon_OnTalk_n1503_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1503_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1598_velskud--------------------------------------------------------------------------------
function mqc15_9456_great_silver_dragon_OnTalk_n1598_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1598_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1598_velskud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1598_velskud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1598_velskud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1598_velskud-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1598_velskud-1-b" then 
	end
	if npc_talk_index == "n1598_velskud-1-c" then 
	end
	if npc_talk_index == "n1598_velskud-1-d" then 
	end
	if npc_talk_index == "n1598_velskud-1-e" then 
	end
	if npc_talk_index == "n1598_velskud-1-f" then 
	end
	if npc_talk_index == "n1598_velskud-1-g" then 
	end
	if npc_talk_index == "n1598_velskud-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n1598_velskud-3-b" then 
	end
	if npc_talk_index == "n1598_velskud-3-c" then 
	end
	if npc_talk_index == "n1598_velskud-3-d" then 
	end
	if npc_talk_index == "n1598_velskud-3-e" then 
	end
	if npc_talk_index == "n1598_velskud-3-f" then 
	end
	if npc_talk_index == "n1598_velskud-3-g" then 
	end
	if npc_talk_index == "n1598_velskud-3-h" then 
	end
	if npc_talk_index == "n1598_velskud-3-i" then 
	end
	if npc_talk_index == "n1598_velskud-3-j" then 
	end
	if npc_talk_index == "n1598_velskud-3-k" then 
	end
	if npc_talk_index == "n1598_velskud-3-k" then 
	end
	if npc_talk_index == "n1598_velskud-3-k" then 
	end
	if npc_talk_index == "n1598_velskud-3-l" then 
	end
	if npc_talk_index == "n1598_velskud-3-m" then 
	end
	if npc_talk_index == "n1598_velskud-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1598_velskud-4-b" then 
	end
	if npc_talk_index == "n1598_velskud-4-c" then 
	end
	if npc_talk_index == "n1598_velskud-4-d" then 
	end
	if npc_talk_index == "n1598_velskud-4-e" then 
	end
	if npc_talk_index == "n1598_velskud-4-e" then 
	end
	if npc_talk_index == "n1598_velskud-4-e" then 
	end
	if npc_talk_index == "n1598_velskud-4-f" then 
	end
	if npc_talk_index == "n1598_velskud-4-g" then 
	end
	if npc_talk_index == "n1598_velskud-4-h" then 
	end
	if npc_talk_index == "n1598_velskud-4-h" then 
	end
	if npc_talk_index == "n1598_velskud-4-h" then 
	end
	if npc_talk_index == "n1598_velskud-4-j" then 
	end
	if npc_talk_index == "n1598_velskud-4-j" then 
	end
	if npc_talk_index == "n1598_velskud-4-k" then 
	end
	if npc_talk_index == "n1598_velskud-4-l" then 
	end
	if npc_talk_index == "n1598_velskud-4-m" then 
	end
	if npc_talk_index == "n1598_velskud-4-n" then 
	end
	if npc_talk_index == "n1598_velskud-4-o" then 
	end
	if npc_talk_index == "n1598_velskud-4-p" then 
	end
	if npc_talk_index == "n1598_velskud-4-p" then 
	end
	if npc_talk_index == "n1598_velskud-4-q" then 
	end
	if npc_talk_index == "n1598_velskud-4-q" then 
	end
	if npc_talk_index == "n1598_velskud-4-q" then 
	end
	if npc_talk_index == "n1598_velskud-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1587_for_memory--------------------------------------------------------------------------------
function mqc15_9456_great_silver_dragon_OnTalk_n1587_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1587_for_memory-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1587_for_memory-6-b" then 
	end
	if npc_talk_index == "n1587_for_memory-6-c" then 
	end
	if npc_talk_index == "n1587_for_memory-6-d" then 
	end
	if npc_talk_index == "n1587_for_memory-6-d" then 
	end
	if npc_talk_index == "n1587_for_memory-6-d" then 
	end
	if npc_talk_index == "n1587_for_memory-6-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94561, true);
				 api_quest_RewardQuestUser(userObjID, 94561, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94562, true);
				 api_quest_RewardQuestUser(userObjID, 94562, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94563, true);
				 api_quest_RewardQuestUser(userObjID, 94563, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94564, true);
				 api_quest_RewardQuestUser(userObjID, 94564, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94565, true);
				 api_quest_RewardQuestUser(userObjID, 94565, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94566, true);
				 api_quest_RewardQuestUser(userObjID, 94566, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94567, true);
				 api_quest_RewardQuestUser(userObjID, 94567, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94568, true);
				 api_quest_RewardQuestUser(userObjID, 94568, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94569, true);
				 api_quest_RewardQuestUser(userObjID, 94569, questID, 1);
			 end 
	end
	if npc_talk_index == "n1587_for_memory-6-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9457, 2);
					api_quest_SetQuestStep(userObjID, 9457, 1);
					api_quest_SetJournalStep(userObjID, 9457, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1587_for_memory-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1587_for_memory-1", "mqc15_9457_say_goodbye.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9456_great_silver_dragon_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9456);
end

function mqc15_9456_great_silver_dragon_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9456);
end

function mqc15_9456_great_silver_dragon_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9456);
	local questID=9456;
end

function mqc15_9456_great_silver_dragon_OnRemoteStart( userObjID, questID )
end

function mqc15_9456_great_silver_dragon_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9456_great_silver_dragon_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9456, 2);
				api_quest_SetJournalStep(userObjID,9456, 1);
				api_quest_SetQuestStep(userObjID,9456, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</VillageServer>

<GameServer>
function mqc15_9456_great_silver_dragon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1067 then
		mqc15_9456_great_silver_dragon_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1503 then
		mqc15_9456_great_silver_dragon_OnTalk_n1503_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1598 then
		mqc15_9456_great_silver_dragon_OnTalk_n1598_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1587 then
		mqc15_9456_great_silver_dragon_OnTalk_n1587_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9456_great_silver_dragon_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9456, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9456, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9456, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1503_gereint_kid--------------------------------------------------------------------------------
function mqc15_9456_great_silver_dragon_OnTalk_n1503_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1503_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1598_velskud--------------------------------------------------------------------------------
function mqc15_9456_great_silver_dragon_OnTalk_n1598_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1598_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1598_velskud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1598_velskud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1598_velskud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1598_velskud-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1598_velskud-1-b" then 
	end
	if npc_talk_index == "n1598_velskud-1-c" then 
	end
	if npc_talk_index == "n1598_velskud-1-d" then 
	end
	if npc_talk_index == "n1598_velskud-1-e" then 
	end
	if npc_talk_index == "n1598_velskud-1-f" then 
	end
	if npc_talk_index == "n1598_velskud-1-g" then 
	end
	if npc_talk_index == "n1598_velskud-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n1598_velskud-3-b" then 
	end
	if npc_talk_index == "n1598_velskud-3-c" then 
	end
	if npc_talk_index == "n1598_velskud-3-d" then 
	end
	if npc_talk_index == "n1598_velskud-3-e" then 
	end
	if npc_talk_index == "n1598_velskud-3-f" then 
	end
	if npc_talk_index == "n1598_velskud-3-g" then 
	end
	if npc_talk_index == "n1598_velskud-3-h" then 
	end
	if npc_talk_index == "n1598_velskud-3-i" then 
	end
	if npc_talk_index == "n1598_velskud-3-j" then 
	end
	if npc_talk_index == "n1598_velskud-3-k" then 
	end
	if npc_talk_index == "n1598_velskud-3-k" then 
	end
	if npc_talk_index == "n1598_velskud-3-k" then 
	end
	if npc_talk_index == "n1598_velskud-3-l" then 
	end
	if npc_talk_index == "n1598_velskud-3-m" then 
	end
	if npc_talk_index == "n1598_velskud-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1598_velskud-4-b" then 
	end
	if npc_talk_index == "n1598_velskud-4-c" then 
	end
	if npc_talk_index == "n1598_velskud-4-d" then 
	end
	if npc_talk_index == "n1598_velskud-4-e" then 
	end
	if npc_talk_index == "n1598_velskud-4-e" then 
	end
	if npc_talk_index == "n1598_velskud-4-e" then 
	end
	if npc_talk_index == "n1598_velskud-4-f" then 
	end
	if npc_talk_index == "n1598_velskud-4-g" then 
	end
	if npc_talk_index == "n1598_velskud-4-h" then 
	end
	if npc_talk_index == "n1598_velskud-4-h" then 
	end
	if npc_talk_index == "n1598_velskud-4-h" then 
	end
	if npc_talk_index == "n1598_velskud-4-j" then 
	end
	if npc_talk_index == "n1598_velskud-4-j" then 
	end
	if npc_talk_index == "n1598_velskud-4-k" then 
	end
	if npc_talk_index == "n1598_velskud-4-l" then 
	end
	if npc_talk_index == "n1598_velskud-4-m" then 
	end
	if npc_talk_index == "n1598_velskud-4-n" then 
	end
	if npc_talk_index == "n1598_velskud-4-o" then 
	end
	if npc_talk_index == "n1598_velskud-4-p" then 
	end
	if npc_talk_index == "n1598_velskud-4-p" then 
	end
	if npc_talk_index == "n1598_velskud-4-q" then 
	end
	if npc_talk_index == "n1598_velskud-4-q" then 
	end
	if npc_talk_index == "n1598_velskud-4-q" then 
	end
	if npc_talk_index == "n1598_velskud-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1587_for_memory--------------------------------------------------------------------------------
function mqc15_9456_great_silver_dragon_OnTalk_n1587_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1587_for_memory-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1587_for_memory-6-b" then 
	end
	if npc_talk_index == "n1587_for_memory-6-c" then 
	end
	if npc_talk_index == "n1587_for_memory-6-d" then 
	end
	if npc_talk_index == "n1587_for_memory-6-d" then 
	end
	if npc_talk_index == "n1587_for_memory-6-d" then 
	end
	if npc_talk_index == "n1587_for_memory-6-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94561, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94561, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94562, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94562, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94563, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94563, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94564, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94564, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94565, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94565, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94566, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94566, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94567, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94567, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94568, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94568, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94569, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94569, questID, 1);
			 end 
	end
	if npc_talk_index == "n1587_for_memory-6-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9457, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9457, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9457, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1587_for_memory-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1587_for_memory-1", "mqc15_9457_say_goodbye.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9456_great_silver_dragon_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9456);
end

function mqc15_9456_great_silver_dragon_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9456);
end

function mqc15_9456_great_silver_dragon_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9456);
	local questID=9456;
end

function mqc15_9456_great_silver_dragon_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9456_great_silver_dragon_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9456_great_silver_dragon_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9456, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9456, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9456, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</GameServer>