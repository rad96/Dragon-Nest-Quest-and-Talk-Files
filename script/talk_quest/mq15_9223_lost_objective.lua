<VillageServer>

function mq15_9223_lost_objective_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 822 then
		mq15_9223_lost_objective_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 837 then
		mq15_9223_lost_objective_OnTalk_n837_rambert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n822_teramai--------------------------------------------------------------------------------
function mq15_9223_lost_objective_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n822_teramai-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9223, 2);
				api_quest_SetJournalStep(userObjID,9223, 1);
				api_quest_SetQuestStep(userObjID,9223, 1);
				npc_talk_index = "n822_teramai-1";

	end
	if npc_talk_index == "n822_teramai-3-b" then 
	end
	if npc_talk_index == "n822_teramai-3-c" then 
	end
	if npc_talk_index == "n822_teramai-3-d" then 
	end
	if npc_talk_index == "n822_teramai-3-d" then 
	end
	if npc_talk_index == "n822_teramai-3-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92231, true);
				 api_quest_RewardQuestUser(userObjID, 92231, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92232, true);
				 api_quest_RewardQuestUser(userObjID, 92232, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92233, true);
				 api_quest_RewardQuestUser(userObjID, 92233, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92234, true);
				 api_quest_RewardQuestUser(userObjID, 92234, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92235, true);
				 api_quest_RewardQuestUser(userObjID, 92235, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92236, true);
				 api_quest_RewardQuestUser(userObjID, 92236, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92231, true);
				 api_quest_RewardQuestUser(userObjID, 92231, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92231, true);
				 api_quest_RewardQuestUser(userObjID, 92231, questID, 1);
			 end 
	end
	if npc_talk_index == "n822_teramai-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9224, 2);
					api_quest_SetQuestStep(userObjID, 9224, 1);
					api_quest_SetJournalStep(userObjID, 9224, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n822_teramai-1", "mq15_9224_resurrection.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n837_rambert--------------------------------------------------------------------------------
function mq15_9223_lost_objective_OnTalk_n837_rambert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n837_rambert-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n837_rambert-2";
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n837_rambert-2";

				else
									if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n837_rambert-2-o";

				else
									if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n837_rambert-2-x";

				else
									npc_talk_index = "n837_rambert-2-t";

				end

				end

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n837_rambert-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n837_rambert-2-b" then 
	end
	if npc_talk_index == "n837_rambert-2-c" then 
	end
	if npc_talk_index == "n837_rambert-2-d" then 
	end
	if npc_talk_index == "n837_rambert-2-e" then 
	end
	if npc_talk_index == "n837_rambert-2-f" then 
	end
	if npc_talk_index == "n837_rambert-2-g" then 
	end
	if npc_talk_index == "n837_rambert-2-h" then 
	end
	if npc_talk_index == "n837_rambert-2-i" then 
	end
	if npc_talk_index == "n837_rambert-2-j" then 
	end
	if npc_talk_index == "n837_rambert-2-k" then 
	end
	if npc_talk_index == "n837_rambert-2-l" then 
	end
	if npc_talk_index == "n837_rambert-2-m" then 
	end
	if npc_talk_index == "n837_rambert-2-n" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n837_rambert-2-p" then 
	end
	if npc_talk_index == "n837_rambert-2-q" then 
	end
	if npc_talk_index == "n837_rambert-2-r" then 
	end
	if npc_talk_index == "n837_rambert-2-s" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n837_rambert-2-u" then 
	end
	if npc_talk_index == "n837_rambert-2-v" then 
	end
	if npc_talk_index == "n837_rambert-2-w" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n837_rambert-2-y" then 
	end
	if npc_talk_index == "n837_rambert-2-z" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9223_lost_objective_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9223);
end

function mq15_9223_lost_objective_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9223);
end

function mq15_9223_lost_objective_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9223);
	local questID=9223;
end

function mq15_9223_lost_objective_OnRemoteStart( userObjID, questID )
end

function mq15_9223_lost_objective_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9223_lost_objective_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9223, 2);
				api_quest_SetJournalStep(userObjID,9223, 1);
				api_quest_SetQuestStep(userObjID,9223, 1);
				npc_talk_index = "n822_teramai-1";
end

</VillageServer>

<GameServer>
function mq15_9223_lost_objective_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 822 then
		mq15_9223_lost_objective_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 837 then
		mq15_9223_lost_objective_OnTalk_n837_rambert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n822_teramai--------------------------------------------------------------------------------
function mq15_9223_lost_objective_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n822_teramai-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9223, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9223, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9223, 1);
				npc_talk_index = "n822_teramai-1";

	end
	if npc_talk_index == "n822_teramai-3-b" then 
	end
	if npc_talk_index == "n822_teramai-3-c" then 
	end
	if npc_talk_index == "n822_teramai-3-d" then 
	end
	if npc_talk_index == "n822_teramai-3-d" then 
	end
	if npc_talk_index == "n822_teramai-3-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92231, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92231, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92232, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92232, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92233, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92233, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92234, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92234, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92235, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92235, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92236, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92236, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92231, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92231, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92231, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92231, questID, 1);
			 end 
	end
	if npc_talk_index == "n822_teramai-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9224, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9224, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9224, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n822_teramai-1", "mq15_9224_resurrection.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n837_rambert--------------------------------------------------------------------------------
function mq15_9223_lost_objective_OnTalk_n837_rambert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n837_rambert-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n837_rambert-2";
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n837_rambert-2";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n837_rambert-2-o";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n837_rambert-2-x";

				else
									npc_talk_index = "n837_rambert-2-t";

				end

				end

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n837_rambert-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n837_rambert-2-b" then 
	end
	if npc_talk_index == "n837_rambert-2-c" then 
	end
	if npc_talk_index == "n837_rambert-2-d" then 
	end
	if npc_talk_index == "n837_rambert-2-e" then 
	end
	if npc_talk_index == "n837_rambert-2-f" then 
	end
	if npc_talk_index == "n837_rambert-2-g" then 
	end
	if npc_talk_index == "n837_rambert-2-h" then 
	end
	if npc_talk_index == "n837_rambert-2-i" then 
	end
	if npc_talk_index == "n837_rambert-2-j" then 
	end
	if npc_talk_index == "n837_rambert-2-k" then 
	end
	if npc_talk_index == "n837_rambert-2-l" then 
	end
	if npc_talk_index == "n837_rambert-2-m" then 
	end
	if npc_talk_index == "n837_rambert-2-n" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n837_rambert-2-p" then 
	end
	if npc_talk_index == "n837_rambert-2-q" then 
	end
	if npc_talk_index == "n837_rambert-2-r" then 
	end
	if npc_talk_index == "n837_rambert-2-s" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n837_rambert-2-u" then 
	end
	if npc_talk_index == "n837_rambert-2-v" then 
	end
	if npc_talk_index == "n837_rambert-2-w" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n837_rambert-2-y" then 
	end
	if npc_talk_index == "n837_rambert-2-z" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9223_lost_objective_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9223);
end

function mq15_9223_lost_objective_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9223);
end

function mq15_9223_lost_objective_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9223);
	local questID=9223;
end

function mq15_9223_lost_objective_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9223_lost_objective_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9223_lost_objective_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9223, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9223, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9223, 1);
				npc_talk_index = "n822_teramai-1";
end

</GameServer>