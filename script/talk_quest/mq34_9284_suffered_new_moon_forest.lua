<VillageServer>

function mq34_9284_suffered_new_moon_forest_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9284_suffered_new_moon_forest_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9284_suffered_new_moon_forest_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1047_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9284, 2);
				api_quest_SetJournalStep(userObjID,9284, 1);
				api_quest_SetQuestStep(userObjID,9284, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
	end
	if npc_talk_index == "n1047_argenta-1-f" then 
	end
	if npc_talk_index == "n1047_argenta-1-g" then 
	end
	if npc_talk_index == "n1047_argenta-1-h" then 
	end
	if npc_talk_index == "n1047_argenta-1-i" then 
	end
	if npc_talk_index == "n1047_argenta-1-j" then 
	end
	if npc_talk_index == "n1047_argenta-1-k" then 
	end
	if npc_talk_index == "n1047_argenta-1-l" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400382, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 701019, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 601019, 30000);
	end
	if npc_talk_index == "n1047_argenta-3-b" then 
	end
	if npc_talk_index == "n1047_argenta-3-c" then 
	end
	if npc_talk_index == "n1047_argenta-3-d" then 
	end
	if npc_talk_index == "n1047_argenta-3-e" then 
	end
	if npc_talk_index == "n1047_argenta-3-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92841, true);
				 api_quest_RewardQuestUser(userObjID, 92841, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92840, true);
				 api_quest_RewardQuestUser(userObjID, 92840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92843, true);
				 api_quest_RewardQuestUser(userObjID, 92843, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92844, true);
				 api_quest_RewardQuestUser(userObjID, 92844, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92845, true);
				 api_quest_RewardQuestUser(userObjID, 92845, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92846, true);
				 api_quest_RewardQuestUser(userObjID, 92846, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92847, true);
				 api_quest_RewardQuestUser(userObjID, 92847, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92848, true);
				 api_quest_RewardQuestUser(userObjID, 92848, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92849, true);
				 api_quest_RewardQuestUser(userObjID, 92849, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9285, 2);
					api_quest_SetQuestStep(userObjID, 9285, 1);
					api_quest_SetJournalStep(userObjID, 9285, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1047_argenta-1", "mq34_9285_doubtful_encounter.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9284_suffered_new_moon_forest_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9284);
	if qstep == 2 and CountIndex == 400382 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 701019 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400382, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 601019 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400382, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq34_9284_suffered_new_moon_forest_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9284);
	if qstep == 2 and CountIndex == 400382 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 701019 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 601019 and Count >= TargetCount  then

	end
end

function mq34_9284_suffered_new_moon_forest_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9284);
	local questID=9284;
end

function mq34_9284_suffered_new_moon_forest_OnRemoteStart( userObjID, questID )
end

function mq34_9284_suffered_new_moon_forest_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9284_suffered_new_moon_forest_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9284, 2);
				api_quest_SetJournalStep(userObjID,9284, 1);
				api_quest_SetQuestStep(userObjID,9284, 1);
				npc_talk_index = "n1047_argenta-1";
end

</VillageServer>

<GameServer>
function mq34_9284_suffered_new_moon_forest_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9284_suffered_new_moon_forest_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9284_suffered_new_moon_forest_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1047_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9284, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9284, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9284, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
	end
	if npc_talk_index == "n1047_argenta-1-f" then 
	end
	if npc_talk_index == "n1047_argenta-1-g" then 
	end
	if npc_talk_index == "n1047_argenta-1-h" then 
	end
	if npc_talk_index == "n1047_argenta-1-i" then 
	end
	if npc_talk_index == "n1047_argenta-1-j" then 
	end
	if npc_talk_index == "n1047_argenta-1-k" then 
	end
	if npc_talk_index == "n1047_argenta-1-l" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400382, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 701019, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 601019, 30000);
	end
	if npc_talk_index == "n1047_argenta-3-b" then 
	end
	if npc_talk_index == "n1047_argenta-3-c" then 
	end
	if npc_talk_index == "n1047_argenta-3-d" then 
	end
	if npc_talk_index == "n1047_argenta-3-e" then 
	end
	if npc_talk_index == "n1047_argenta-3-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92841, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92841, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92843, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92843, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92844, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92844, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92845, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92845, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92846, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92846, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92847, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92847, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92848, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92848, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92849, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92849, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9285, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9285, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9285, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1047_argenta-1", "mq34_9285_doubtful_encounter.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9284_suffered_new_moon_forest_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9284);
	if qstep == 2 and CountIndex == 400382 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 701019 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400382, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 601019 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400382, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq34_9284_suffered_new_moon_forest_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9284);
	if qstep == 2 and CountIndex == 400382 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 701019 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 601019 and Count >= TargetCount  then

	end
end

function mq34_9284_suffered_new_moon_forest_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9284);
	local questID=9284;
end

function mq34_9284_suffered_new_moon_forest_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9284_suffered_new_moon_forest_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9284_suffered_new_moon_forest_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9284, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9284, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9284, 1);
				npc_talk_index = "n1047_argenta-1";
end

</GameServer>