<VillageServer>

function mq11_9324_the_poison_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1160 then
		mq11_9324_the_poison_OnTalk_n1160_eclair(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1161 then
		mq11_9324_the_poison_OnTalk_n1161_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1162 then
		mq11_9324_the_poison_OnTalk_n1162_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1193 then
		mq11_9324_the_poison_OnTalk_n1193_illusion(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		mq11_9324_the_poison_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1160_eclair--------------------------------------------------------------------------------
function mq11_9324_the_poison_OnTalk_n1160_eclair(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1160_eclair-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1161_lunaria--------------------------------------------------------------------------------
function mq11_9324_the_poison_OnTalk_n1161_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1161_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1162_lunaria--------------------------------------------------------------------------------
function mq11_9324_the_poison_OnTalk_n1162_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1162_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1162_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1162_lunaria-1-b" then 
	end
	if npc_talk_index == "n1162_lunaria-1-c" then 
	end
	if npc_talk_index == "n1162_lunaria-1-d" then 
	end
	if npc_talk_index == "n1162_lunaria-1-e" then 
	end
	if npc_talk_index == "n1162_lunaria-1-f" then 
	end
	if npc_talk_index == "n1162_lunaria-1-g" then 
	end
	if npc_talk_index == "n1162_lunaria-1-h" then 
	end
	if npc_talk_index == "n1162_lunaria-1-i" then 
	end
	if npc_talk_index == "n1162_lunaria-1-j" then 
	end
	if npc_talk_index == "n1162_lunaria-1-k" then 
	end
	if npc_talk_index == "n1162_lunaria-1-l" then 
	end
	if npc_talk_index == "n1162_lunaria-1-m" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300575, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300575, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1193_illusion--------------------------------------------------------------------------------
function mq11_9324_the_poison_OnTalk_n1193_illusion(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function mq11_9324_the_poison_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n776_darklair_mardlen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9324, 2);
				api_quest_SetJournalStep(userObjID,9324, 1);
				api_quest_SetQuestStep(userObjID,9324, 1);
				npc_talk_index = "n776_darklair_mardlen-1";

	end
	if npc_talk_index == "n776_darklair_mardlen-2-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93240, true);
				 api_quest_RewardQuestUser(userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93240, true);
				 api_quest_RewardQuestUser(userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93240, true);
				 api_quest_RewardQuestUser(userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93240, true);
				 api_quest_RewardQuestUser(userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93240, true);
				 api_quest_RewardQuestUser(userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93240, true);
				 api_quest_RewardQuestUser(userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93240, true);
				 api_quest_RewardQuestUser(userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93240, true);
				 api_quest_RewardQuestUser(userObjID, 93240, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9325, 2);
					api_quest_SetQuestStep(userObjID, 9325, 1);
					api_quest_SetJournalStep(userObjID, 9325, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300575, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300575, api_quest_HasQuestItem(userObjID, 300575, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300574, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300574, api_quest_HasQuestItem(userObjID, 300574, 1));
				end
	end
	if npc_talk_index == "n776_darklair_mardlen-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n776_darklair_mardlen-1", "mq11_9325_word_of_promise.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_9324_the_poison_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9324);
end

function mq11_9324_the_poison_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9324);
end

function mq11_9324_the_poison_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9324);
	local questID=9324;
end

function mq11_9324_the_poison_OnRemoteStart( userObjID, questID )
end

function mq11_9324_the_poison_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq11_9324_the_poison_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9324, 2);
				api_quest_SetJournalStep(userObjID,9324, 1);
				api_quest_SetQuestStep(userObjID,9324, 1);
				npc_talk_index = "n776_darklair_mardlen-1";
end

</VillageServer>

<GameServer>
function mq11_9324_the_poison_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1160 then
		mq11_9324_the_poison_OnTalk_n1160_eclair( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1161 then
		mq11_9324_the_poison_OnTalk_n1161_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1162 then
		mq11_9324_the_poison_OnTalk_n1162_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1193 then
		mq11_9324_the_poison_OnTalk_n1193_illusion( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		mq11_9324_the_poison_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1160_eclair--------------------------------------------------------------------------------
function mq11_9324_the_poison_OnTalk_n1160_eclair( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1160_eclair-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1161_lunaria--------------------------------------------------------------------------------
function mq11_9324_the_poison_OnTalk_n1161_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1161_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1162_lunaria--------------------------------------------------------------------------------
function mq11_9324_the_poison_OnTalk_n1162_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1162_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1162_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1162_lunaria-1-b" then 
	end
	if npc_talk_index == "n1162_lunaria-1-c" then 
	end
	if npc_talk_index == "n1162_lunaria-1-d" then 
	end
	if npc_talk_index == "n1162_lunaria-1-e" then 
	end
	if npc_talk_index == "n1162_lunaria-1-f" then 
	end
	if npc_talk_index == "n1162_lunaria-1-g" then 
	end
	if npc_talk_index == "n1162_lunaria-1-h" then 
	end
	if npc_talk_index == "n1162_lunaria-1-i" then 
	end
	if npc_talk_index == "n1162_lunaria-1-j" then 
	end
	if npc_talk_index == "n1162_lunaria-1-k" then 
	end
	if npc_talk_index == "n1162_lunaria-1-l" then 
	end
	if npc_talk_index == "n1162_lunaria-1-m" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300575, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300575, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1193_illusion--------------------------------------------------------------------------------
function mq11_9324_the_poison_OnTalk_n1193_illusion( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function mq11_9324_the_poison_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n776_darklair_mardlen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9324, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9324, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9324, 1);
				npc_talk_index = "n776_darklair_mardlen-1";

	end
	if npc_talk_index == "n776_darklair_mardlen-2-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93240, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9325, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9325, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9325, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300575, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300575, api_quest_HasQuestItem( pRoom, userObjID, 300575, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300574, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300574, api_quest_HasQuestItem( pRoom, userObjID, 300574, 1));
				end
	end
	if npc_talk_index == "n776_darklair_mardlen-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n776_darklair_mardlen-1", "mq11_9325_word_of_promise.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_9324_the_poison_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9324);
end

function mq11_9324_the_poison_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9324);
end

function mq11_9324_the_poison_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9324);
	local questID=9324;
end

function mq11_9324_the_poison_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq11_9324_the_poison_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq11_9324_the_poison_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9324, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9324, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9324, 1);
				npc_talk_index = "n776_darklair_mardlen-1";
end

</GameServer>