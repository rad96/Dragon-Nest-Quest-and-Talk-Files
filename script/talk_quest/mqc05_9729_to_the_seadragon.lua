<VillageServer>

function mqc05_9729_to_the_seadragon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 42 then
		mqc05_9729_to_the_seadragon_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mqc05_9729_to_the_seadragon_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		mqc05_9729_to_the_seadragon_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function mqc05_9729_to_the_seadragon_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n042_general_duglars-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n042_general_duglars-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9729, 2);
				api_quest_SetJournalStep(userObjID,9729, 1);
				api_quest_SetQuestStep(userObjID,9729, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-1-b" then 
	end
	if npc_talk_index == "n042_general_duglars-1-c" then 
	end
	if npc_talk_index == "n042_general_duglars-1-d" then 
	end
	if npc_talk_index == "n042_general_duglars-1-e" then 
	end
	if npc_talk_index == "n042_general_duglars-1-f" then 
	end
	if npc_talk_index == "n042_general_duglars-1-g" then 
	end
	if npc_talk_index == "n042_general_duglars-1-h" then 
	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n042_general_duglars-8-b" then 
	end
	if npc_talk_index == "n042_general_duglars-8-c" then 
	end
	if npc_talk_index == "n042_general_duglars-8-d" then 
	end
	if npc_talk_index == "n042_general_duglars-8-e" then 
	end
	if npc_talk_index == "n042_general_duglars-8-f" then 
	end
	if npc_talk_index == "n042_general_duglars-8-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97290, true);
				 api_quest_RewardQuestUser(userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97290, true);
				 api_quest_RewardQuestUser(userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97290, true);
				 api_quest_RewardQuestUser(userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97290, true);
				 api_quest_RewardQuestUser(userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97290, true);
				 api_quest_RewardQuestUser(userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97290, true);
				 api_quest_RewardQuestUser(userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97290, true);
				 api_quest_RewardQuestUser(userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97290, true);
				 api_quest_RewardQuestUser(userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97290, true);
				 api_quest_RewardQuestUser(userObjID, 97290, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-8-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									if api_quest_IsMarkingCompleteQuest(userObjID, 701) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 724) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 9330) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 9503) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 9615) == 1 then
				else
									if api_user_GetUserClassID(userObjID) == 1 then
									api_quest_AddQuest(userObjID,701, 2);
				api_quest_SetJournalStep(userObjID,701, 1);
				api_quest_SetQuestStep(userObjID,701, 1);

				else
									if api_user_GetUserClassID(userObjID) == 2 then
									api_quest_AddQuest(userObjID,701, 2);
				api_quest_SetJournalStep(userObjID,701, 1);
				api_quest_SetQuestStep(userObjID,701, 1);

				else
									if api_user_GetUserClassID(userObjID) == 3 then
									api_quest_AddQuest(userObjID,701, 2);
				api_quest_SetJournalStep(userObjID,701, 1);
				api_quest_SetQuestStep(userObjID,701, 1);

				else
									if api_user_GetUserClassID(userObjID) == 4 then
									api_quest_AddQuest(userObjID,701, 2);
				api_quest_SetJournalStep(userObjID,701, 1);
				api_quest_SetQuestStep(userObjID,701, 1);

				else
									if api_user_GetUserClassID(userObjID) == 5 then
									api_quest_AddQuest(userObjID,724, 2);
				api_quest_SetJournalStep(userObjID,724, 1);
				api_quest_SetQuestStep(userObjID,724, 1);

				else
									if api_user_GetUserClassID(userObjID) == 6 then
									api_quest_AddQuest(userObjID,701, 2);
				api_quest_SetJournalStep(userObjID,701, 1);
				api_quest_SetQuestStep(userObjID,701, 1);

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									api_quest_AddQuest(userObjID,9330, 2);
				api_quest_SetJournalStep(userObjID,9330, 1);
				api_quest_SetQuestStep(userObjID,9330, 1);

				else
									if api_user_GetUserClassID(userObjID) == 8 then
									api_quest_AddQuest(userObjID,9503, 2);
				api_quest_SetJournalStep(userObjID,9503, 1);
				api_quest_SetQuestStep(userObjID,9503, 1);

				else
									if api_user_GetUserClassID(userObjID) == 9 then
									api_quest_AddQuest(userObjID,9615, 2);
				api_quest_SetJournalStep(userObjID,9615, 1);
				api_quest_SetQuestStep(userObjID,9615, 1);

				else
				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n042_general_duglars-8-i" then 
	end
	if npc_talk_index == "n042_general_duglars-8-class_chk" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n042_general_duglars-8-k";

				else
									if api_user_GetUserClassID(userObjID) == 2 then
									npc_talk_index = "n042_general_duglars-8-l";

				else
									if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n042_general_duglars-8-m";

				else
									if api_user_GetUserClassID(userObjID) == 4 then
									npc_talk_index = "n042_general_duglars-8-n";

				else
									if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n042_general_duglars-8-o";

				else
									if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n042_general_duglars-8-p";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n042_general_duglars-8-p";

				else
									npc_talk_index = "n042_general_duglars-8-j";

				end

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mqc05_9729_to_the_seadragon_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n088_scholar_starshy-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n088_scholar_starshy-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n088_scholar_starshy-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n088_scholar_starshy-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n088_scholar_starshy-5-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n088_scholar_starshy-7-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-k" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-l" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-m" then 
	end
	if npc_talk_index == "n088_scholar_starshy-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function mqc05_9729_to_the_seadragon_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n045_soceress_master_stella-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n045_soceress_master_stella-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n045_soceress_master_stella-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-3-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-j" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-k" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-l" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-m" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-n" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-o" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-p" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-q" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-r" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-s" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-t" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n045_soceress_master_stella-4-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9729_to_the_seadragon_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9729);
end

function mqc05_9729_to_the_seadragon_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9729);
end

function mqc05_9729_to_the_seadragon_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9729);
	local questID=9729;
end

function mqc05_9729_to_the_seadragon_OnRemoteStart( userObjID, questID )
end

function mqc05_9729_to_the_seadragon_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc05_9729_to_the_seadragon_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9729, 2);
				api_quest_SetJournalStep(userObjID,9729, 1);
				api_quest_SetQuestStep(userObjID,9729, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</VillageServer>

<GameServer>
function mqc05_9729_to_the_seadragon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 42 then
		mqc05_9729_to_the_seadragon_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mqc05_9729_to_the_seadragon_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		mqc05_9729_to_the_seadragon_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function mqc05_9729_to_the_seadragon_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n042_general_duglars-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n042_general_duglars-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9729, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9729, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9729, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-1-b" then 
	end
	if npc_talk_index == "n042_general_duglars-1-c" then 
	end
	if npc_talk_index == "n042_general_duglars-1-d" then 
	end
	if npc_talk_index == "n042_general_duglars-1-e" then 
	end
	if npc_talk_index == "n042_general_duglars-1-f" then 
	end
	if npc_talk_index == "n042_general_duglars-1-g" then 
	end
	if npc_talk_index == "n042_general_duglars-1-h" then 
	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n042_general_duglars-8-b" then 
	end
	if npc_talk_index == "n042_general_duglars-8-c" then 
	end
	if npc_talk_index == "n042_general_duglars-8-d" then 
	end
	if npc_talk_index == "n042_general_duglars-8-e" then 
	end
	if npc_talk_index == "n042_general_duglars-8-f" then 
	end
	if npc_talk_index == "n042_general_duglars-8-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97290, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-8-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 701) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 724) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9330) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9503) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9615) == 1 then
				else
									if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									api_quest_AddQuest( pRoom, userObjID,701, 2);
				api_quest_SetJournalStep( pRoom, userObjID,701, 1);
				api_quest_SetQuestStep( pRoom, userObjID,701, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									api_quest_AddQuest( pRoom, userObjID,701, 2);
				api_quest_SetJournalStep( pRoom, userObjID,701, 1);
				api_quest_SetQuestStep( pRoom, userObjID,701, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									api_quest_AddQuest( pRoom, userObjID,701, 2);
				api_quest_SetJournalStep( pRoom, userObjID,701, 1);
				api_quest_SetQuestStep( pRoom, userObjID,701, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									api_quest_AddQuest( pRoom, userObjID,701, 2);
				api_quest_SetJournalStep( pRoom, userObjID,701, 1);
				api_quest_SetQuestStep( pRoom, userObjID,701, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									api_quest_AddQuest( pRoom, userObjID,724, 2);
				api_quest_SetJournalStep( pRoom, userObjID,724, 1);
				api_quest_SetQuestStep( pRoom, userObjID,724, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									api_quest_AddQuest( pRoom, userObjID,701, 2);
				api_quest_SetJournalStep( pRoom, userObjID,701, 1);
				api_quest_SetQuestStep( pRoom, userObjID,701, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									api_quest_AddQuest( pRoom, userObjID,9330, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9330, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9330, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 8 then
									api_quest_AddQuest( pRoom, userObjID,9503, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9503, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9503, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 9 then
									api_quest_AddQuest( pRoom, userObjID,9615, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9615, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9615, 1);

				else
				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n042_general_duglars-8-i" then 
	end
	if npc_talk_index == "n042_general_duglars-8-class_chk" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n042_general_duglars-8-k";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									npc_talk_index = "n042_general_duglars-8-l";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n042_general_duglars-8-m";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									npc_talk_index = "n042_general_duglars-8-n";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n042_general_duglars-8-o";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n042_general_duglars-8-p";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n042_general_duglars-8-p";

				else
									npc_talk_index = "n042_general_duglars-8-j";

				end

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end
	if npc_talk_index == "n042_general_duglars-8-q" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mqc05_9729_to_the_seadragon_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n088_scholar_starshy-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n088_scholar_starshy-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n088_scholar_starshy-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n088_scholar_starshy-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n088_scholar_starshy-5-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n088_scholar_starshy-7-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-k" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-l" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-m" then 
	end
	if npc_talk_index == "n088_scholar_starshy-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function mqc05_9729_to_the_seadragon_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n045_soceress_master_stella-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n045_soceress_master_stella-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n045_soceress_master_stella-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-3-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-j" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-k" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-l" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-m" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-n" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-o" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-p" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-q" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-r" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-s" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-t" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n045_soceress_master_stella-4-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9729_to_the_seadragon_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9729);
end

function mqc05_9729_to_the_seadragon_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9729);
end

function mqc05_9729_to_the_seadragon_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9729);
	local questID=9729;
end

function mqc05_9729_to_the_seadragon_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc05_9729_to_the_seadragon_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc05_9729_to_the_seadragon_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9729, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9729, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9729, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</GameServer>