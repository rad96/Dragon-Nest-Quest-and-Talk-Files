<VillageServer>

function mq15_9351_stray_sheep_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1179 then
		mq15_9351_stray_sheep_OnTalk_n1179_sorceress_karakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 575 then
		mq15_9351_stray_sheep_OnTalk_n575_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mq15_9351_stray_sheep_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_9351_stray_sheep_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1179_sorceress_karakule--------------------------------------------------------------------------------
function mq15_9351_stray_sheep_OnTalk_n1179_sorceress_karakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1179_sorceress_karakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1179_sorceress_karakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1179_sorceress_karakule-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1179_sorceress_karakule-4-b" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-c" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-d" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-e" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-f" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-g" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-h" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-i" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-j" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93510, true);
				 api_quest_RewardQuestUser(userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93510, true);
				 api_quest_RewardQuestUser(userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93510, true);
				 api_quest_RewardQuestUser(userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93510, true);
				 api_quest_RewardQuestUser(userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93510, true);
				 api_quest_RewardQuestUser(userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93510, true);
				 api_quest_RewardQuestUser(userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93510, true);
				 api_quest_RewardQuestUser(userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93510, true);
				 api_quest_RewardQuestUser(userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93510, true);
				 api_quest_RewardQuestUser(userObjID, 93510, questID, 1);
			 end 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-l" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9352, 2);
					api_quest_SetQuestStep(userObjID, 9352, 1);
					api_quest_SetJournalStep(userObjID, 9352, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1179_sorceress_karakule-1", "mq15_9352_i_need_you.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n575_shadow_meow--------------------------------------------------------------------------------
function mq15_9351_stray_sheep_OnTalk_n575_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n575_shadow_meow-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n575_shadow_meow-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n575_shadow_meow-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9351, 2);
				api_quest_SetJournalStep(userObjID,9351, 1);
				api_quest_SetQuestStep(userObjID,9351, 1);
				npc_talk_index = "n575_shadow_meow-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_9351_stray_sheep_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-j" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-k" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-l" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-m" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-n" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-o" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_9351_stray_sheep_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
	end
	if npc_talk_index == "n707_sidel-2-d" then 
	end
	if npc_talk_index == "n707_sidel-2-e" then 
	end
	if npc_talk_index == "n707_sidel-2-f" then 
	end
	if npc_talk_index == "n707_sidel-2-g" then 
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300382, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1576, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201576, 30000);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9351_stray_sheep_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9351);
	if qstep == 3 and CountIndex == 300382 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

	end
	if qstep == 3 and CountIndex == 1576 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300382, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 201576 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300382, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq15_9351_stray_sheep_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9351);
	if qstep == 3 and CountIndex == 300382 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 1576 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 201576 and Count >= TargetCount  then

	end
end

function mq15_9351_stray_sheep_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9351);
	local questID=9351;
end

function mq15_9351_stray_sheep_OnRemoteStart( userObjID, questID )
end

function mq15_9351_stray_sheep_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9351_stray_sheep_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9351, 2);
				api_quest_SetJournalStep(userObjID,9351, 1);
				api_quest_SetQuestStep(userObjID,9351, 1);
				npc_talk_index = "n575_shadow_meow-1";
end

</VillageServer>

<GameServer>
function mq15_9351_stray_sheep_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1179 then
		mq15_9351_stray_sheep_OnTalk_n1179_sorceress_karakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 575 then
		mq15_9351_stray_sheep_OnTalk_n575_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mq15_9351_stray_sheep_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_9351_stray_sheep_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1179_sorceress_karakule--------------------------------------------------------------------------------
function mq15_9351_stray_sheep_OnTalk_n1179_sorceress_karakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1179_sorceress_karakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1179_sorceress_karakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1179_sorceress_karakule-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1179_sorceress_karakule-4-b" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-c" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-d" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-e" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-f" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-g" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-h" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-i" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-j" then 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93510, questID, 1);
			 end 
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-l" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9352, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9352, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9352, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1179_sorceress_karakule-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1179_sorceress_karakule-1", "mq15_9352_i_need_you.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n575_shadow_meow--------------------------------------------------------------------------------
function mq15_9351_stray_sheep_OnTalk_n575_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n575_shadow_meow-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n575_shadow_meow-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n575_shadow_meow-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9351, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9351, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9351, 1);
				npc_talk_index = "n575_shadow_meow-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_9351_stray_sheep_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-j" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-k" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-l" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-m" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-n" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-o" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_9351_stray_sheep_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
	end
	if npc_talk_index == "n707_sidel-2-d" then 
	end
	if npc_talk_index == "n707_sidel-2-e" then 
	end
	if npc_talk_index == "n707_sidel-2-f" then 
	end
	if npc_talk_index == "n707_sidel-2-g" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300382, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1576, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201576, 30000);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9351_stray_sheep_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9351);
	if qstep == 3 and CountIndex == 300382 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

	end
	if qstep == 3 and CountIndex == 1576 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300382, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 201576 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300382, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq15_9351_stray_sheep_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9351);
	if qstep == 3 and CountIndex == 300382 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 1576 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 201576 and Count >= TargetCount  then

	end
end

function mq15_9351_stray_sheep_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9351);
	local questID=9351;
end

function mq15_9351_stray_sheep_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9351_stray_sheep_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9351_stray_sheep_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9351, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9351, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9351, 1);
				npc_talk_index = "n575_shadow_meow-1";
end

</GameServer>