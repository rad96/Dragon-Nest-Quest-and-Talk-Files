<VillageServer>

function gqc14_3524_n1407_mist_ruin_boss_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1407 then
		gqc14_3524_n1407_mist_ruin_boss_OnTalk_n1407_guild_board(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1407_guild_board--------------------------------------------------------------------------------
function gqc14_3524_n1407_mist_ruin_boss_OnTalk_n1407_guild_board(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1407_guild_board-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1407_guild_board-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1407_guild_board-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1407_guild_board-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 35240, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 35240, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 35240, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 35240, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 35240, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 35240, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 35240, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 35240, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 35240, false);
			 end 

	end
	if npc_talk_index == "n1407_guild_board-accepting-c" then
				api_quest_AddQuest(userObjID,3524, 1);
				api_quest_SetJournalStep(userObjID,3524, 1);
				api_quest_SetQuestStep(userObjID,3524, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 602002, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 702002, 30000);

	end
	if npc_talk_index == "n1407_guild_board-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 35240, true);
				 api_quest_RewardQuestUser(userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 35240, true);
				 api_quest_RewardQuestUser(userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 35240, true);
				 api_quest_RewardQuestUser(userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 35240, true);
				 api_quest_RewardQuestUser(userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 35240, true);
				 api_quest_RewardQuestUser(userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 35240, true);
				 api_quest_RewardQuestUser(userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 35240, true);
				 api_quest_RewardQuestUser(userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 35240, true);
				 api_quest_RewardQuestUser(userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 35240, true);
				 api_quest_RewardQuestUser(userObjID, 35240, questID, 1);
			 end 
	end
	if npc_talk_index == "n1407_guild_board-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function gqc14_3524_n1407_mist_ruin_boss_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 3524);
	if qstep == 1 and CountIndex == 602002 then

	end
	if qstep == 1 and CountIndex == 702002 then
				api_quest_IncCounting(userObjID, 2, 602002);

	end
end

function gqc14_3524_n1407_mist_ruin_boss_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 3524);
	if qstep == 1 and CountIndex == 602002 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 702002 and Count >= TargetCount  then

	end
end

function gqc14_3524_n1407_mist_ruin_boss_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 3524);
	local questID=3524;
end

function gqc14_3524_n1407_mist_ruin_boss_OnRemoteStart( userObjID, questID )
end

function gqc14_3524_n1407_mist_ruin_boss_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function gqc14_3524_n1407_mist_ruin_boss_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function gqc14_3524_n1407_mist_ruin_boss_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1407 then
		gqc14_3524_n1407_mist_ruin_boss_OnTalk_n1407_guild_board( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1407_guild_board--------------------------------------------------------------------------------
function gqc14_3524_n1407_mist_ruin_boss_OnTalk_n1407_guild_board( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1407_guild_board-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1407_guild_board-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1407_guild_board-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1407_guild_board-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, false);
			 end 

	end
	if npc_talk_index == "n1407_guild_board-accepting-c" then
				api_quest_AddQuest( pRoom, userObjID,3524, 1);
				api_quest_SetJournalStep( pRoom, userObjID,3524, 1);
				api_quest_SetQuestStep( pRoom, userObjID,3524, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 602002, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 702002, 30000);

	end
	if npc_talk_index == "n1407_guild_board-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35240, questID, 1);
			 end 
	end
	if npc_talk_index == "n1407_guild_board-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function gqc14_3524_n1407_mist_ruin_boss_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 3524);
	if qstep == 1 and CountIndex == 602002 then

	end
	if qstep == 1 and CountIndex == 702002 then
				api_quest_IncCounting( pRoom, userObjID, 2, 602002);

	end
end

function gqc14_3524_n1407_mist_ruin_boss_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 3524);
	if qstep == 1 and CountIndex == 602002 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 702002 and Count >= TargetCount  then

	end
end

function gqc14_3524_n1407_mist_ruin_boss_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 3524);
	local questID=3524;
end

function gqc14_3524_n1407_mist_ruin_boss_OnRemoteStart( pRoom,  userObjID, questID )
end

function gqc14_3524_n1407_mist_ruin_boss_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function gqc14_3524_n1407_mist_ruin_boss_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>