<VillageServer>

function sq_2439_kor_crazypang_2015_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1888 then
		sq_2439_kor_crazypang_2015_OnTalk_n1888_storyteller_kesey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1888_storyteller_kesey--------------------------------------------------------------------------------
function sq_2439_kor_crazypang_2015_OnTalk_n1888_storyteller_kesey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1888_storyteller_kesey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1888_storyteller_kesey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1888_storyteller_kesey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2439, 1);
				api_quest_SetJournalStep(userObjID,2439, 1);
				api_quest_SetQuestStep(userObjID,2439, 1);
				npc_talk_index = "n1888_storyteller_kesey-1";

	end
	if npc_talk_index == "n1888_storyteller_kesey-1-b" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-c" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-c" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-party_check" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n1888_storyteller_kesey-1-d";

				else
									npc_talk_index = "n1888_storyteller_kesey-1-e";

				end
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-changemap" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
					
				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_ChangeMap(userObjID,13637,1);


				else
					npc_talk_index = "_full_inventory";

				end

				else
									npc_talk_index = "n1888_storyteller_kesey-1-e";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2439_kor_crazypang_2015_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2439);
end

function sq_2439_kor_crazypang_2015_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2439);
end

function sq_2439_kor_crazypang_2015_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2439);
	local questID=2439;
end

function sq_2439_kor_crazypang_2015_OnRemoteStart( userObjID, questID )
end

function sq_2439_kor_crazypang_2015_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_2439_kor_crazypang_2015_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2439, 1);
				api_quest_SetJournalStep(userObjID,2439, 1);
				api_quest_SetQuestStep(userObjID,2439, 1);
				npc_talk_index = "n1888_storyteller_kesey-1";
end

</VillageServer>

<GameServer>
function sq_2439_kor_crazypang_2015_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1888 then
		sq_2439_kor_crazypang_2015_OnTalk_n1888_storyteller_kesey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1888_storyteller_kesey--------------------------------------------------------------------------------
function sq_2439_kor_crazypang_2015_OnTalk_n1888_storyteller_kesey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1888_storyteller_kesey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1888_storyteller_kesey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1888_storyteller_kesey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2439, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2439, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2439, 1);
				npc_talk_index = "n1888_storyteller_kesey-1";

	end
	if npc_talk_index == "n1888_storyteller_kesey-1-b" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-c" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-c" then 
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-party_check" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n1888_storyteller_kesey-1-d";

				else
									npc_talk_index = "n1888_storyteller_kesey-1-e";

				end
	end
	if npc_talk_index == "n1888_storyteller_kesey-1-changemap" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
					
				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_ChangeMap( pRoom, userObjID,13637,1);


				else
					npc_talk_index = "_full_inventory";

				end

				else
									npc_talk_index = "n1888_storyteller_kesey-1-e";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2439_kor_crazypang_2015_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2439);
end

function sq_2439_kor_crazypang_2015_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2439);
end

function sq_2439_kor_crazypang_2015_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2439);
	local questID=2439;
end

function sq_2439_kor_crazypang_2015_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_2439_kor_crazypang_2015_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_2439_kor_crazypang_2015_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2439, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2439, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2439, 1);
				npc_talk_index = "n1888_storyteller_kesey-1";
end

</GameServer>