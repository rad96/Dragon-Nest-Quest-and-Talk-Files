<VillageServer>

function sq08_256_ashes_of_saint2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 34 then
		sq08_256_ashes_of_saint2_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n034_cleric_master_germain--------------------------------------------------------------------------------
function sq08_256_ashes_of_saint2_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n034_cleric_master_germain-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n034_cleric_master_germain-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2561, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2562, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2563, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2564, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2565, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2566, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2567, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2568, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2569, false);
			 end 

	end
	if npc_talk_index == "n034_cleric_master_germain-accepting-acceptted" then
				api_quest_AddQuest(userObjID,256, 1);
				api_quest_SetJournalStep(userObjID,256, 1);
				api_quest_SetQuestStep(userObjID,256, 1);
				npc_talk_index = "n034_cleric_master_germain-1";

	end
	if npc_talk_index == "n034_cleric_master_germain-1-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-f" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-g" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-h" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-i" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-j" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-k" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-l" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-m" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-n" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300014, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200413, 30000);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n034_cleric_master_germain-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2561, true);
				 api_quest_RewardQuestUser(userObjID, 2561, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2562, true);
				 api_quest_RewardQuestUser(userObjID, 2562, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2563, true);
				 api_quest_RewardQuestUser(userObjID, 2563, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2564, true);
				 api_quest_RewardQuestUser(userObjID, 2564, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2565, true);
				 api_quest_RewardQuestUser(userObjID, 2565, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2566, true);
				 api_quest_RewardQuestUser(userObjID, 2566, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2567, true);
				 api_quest_RewardQuestUser(userObjID, 2567, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2568, true);
				 api_quest_RewardQuestUser(userObjID, 2568, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2569, true);
				 api_quest_RewardQuestUser(userObjID, 2569, questID, 1);
			 end 
	end
	if npc_talk_index == "n034_cleric_master_germain-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 257, 1);
					api_quest_SetQuestStep(userObjID, 257, 1);
					api_quest_SetJournalStep(userObjID, 257, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-3-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem(userObjID, 300014, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300014, api_quest_HasQuestItem(userObjID, 300014, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300008, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300008, api_quest_HasQuestItem(userObjID, 300008, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300007, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300007, api_quest_HasQuestItem(userObjID, 300007, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n034_cleric_master_germain-1", "sq08_257_ashes_of_saint3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_256_ashes_of_saint2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 256);
	if qstep == 2 and CountIndex == 300014 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300014, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300014, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300014, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300014, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_256_ashes_of_saint2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 256);
	if qstep == 2 and CountIndex == 300014 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 413 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200413 and Count >= TargetCount  then

	end
end

function sq08_256_ashes_of_saint2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 256);
	local questID=256;
end

function sq08_256_ashes_of_saint2_OnRemoteStart( userObjID, questID )
end

function sq08_256_ashes_of_saint2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_256_ashes_of_saint2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,256, 1);
				api_quest_SetJournalStep(userObjID,256, 1);
				api_quest_SetQuestStep(userObjID,256, 1);
				npc_talk_index = "n034_cleric_master_germain-1";
end

</VillageServer>

<GameServer>
function sq08_256_ashes_of_saint2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 34 then
		sq08_256_ashes_of_saint2_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n034_cleric_master_germain--------------------------------------------------------------------------------
function sq08_256_ashes_of_saint2_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n034_cleric_master_germain-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n034_cleric_master_germain-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2561, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2562, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2563, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2564, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2565, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2566, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2567, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2568, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2569, false);
			 end 

	end
	if npc_talk_index == "n034_cleric_master_germain-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,256, 1);
				api_quest_SetJournalStep( pRoom, userObjID,256, 1);
				api_quest_SetQuestStep( pRoom, userObjID,256, 1);
				npc_talk_index = "n034_cleric_master_germain-1";

	end
	if npc_talk_index == "n034_cleric_master_germain-1-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-f" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-g" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-h" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-i" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-j" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-k" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-l" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-m" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-n" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300014, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200413, 30000);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n034_cleric_master_germain-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2561, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2561, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2562, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2562, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2563, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2563, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2564, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2564, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2565, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2565, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2566, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2566, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2567, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2567, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2568, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2568, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2569, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2569, questID, 1);
			 end 
	end
	if npc_talk_index == "n034_cleric_master_germain-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 257, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 257, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 257, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-3-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem( pRoom, userObjID, 300014, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300014, api_quest_HasQuestItem( pRoom, userObjID, 300014, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300008, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300008, api_quest_HasQuestItem( pRoom, userObjID, 300008, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300007, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300007, api_quest_HasQuestItem( pRoom, userObjID, 300007, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n034_cleric_master_germain-1", "sq08_257_ashes_of_saint3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_256_ashes_of_saint2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 256);
	if qstep == 2 and CountIndex == 300014 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300014, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300014, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300014, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300014, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_256_ashes_of_saint2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 256);
	if qstep == 2 and CountIndex == 300014 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 413 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200413 and Count >= TargetCount  then

	end
end

function sq08_256_ashes_of_saint2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 256);
	local questID=256;
end

function sq08_256_ashes_of_saint2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_256_ashes_of_saint2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_256_ashes_of_saint2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,256, 1);
				api_quest_SetJournalStep( pRoom, userObjID,256, 1);
				api_quest_SetQuestStep( pRoom, userObjID,256, 1);
				npc_talk_index = "n034_cleric_master_germain-1";
end

</GameServer>