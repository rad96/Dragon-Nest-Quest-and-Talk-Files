<VillageServer>

function nq25_5001_lets_be_a_friend_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 100 then
		nq25_5001_lets_be_a_friend_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function nq25_5001_lets_be_a_friend_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n100_event_ilyn-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest(userObjID,5001, 1);
				api_quest_SetJournalStep(userObjID,5001, 1);
				api_quest_SetQuestStep(userObjID,5001, 1);
				npc_talk_index = "n100_event_ilyn-1";

	end
	if npc_talk_index == "n100_event_ilyn-1-b" then 
	end
	if npc_talk_index == "n100_event_ilyn-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n100_event_ilyn-2" then 
	end
	if npc_talk_index == "n100_event_ilyn-2" then 
	end
	if npc_talk_index == "n100_event_ilyn-2-d" then 
	end
	if npc_talk_index == "n100_event_ilyn-2-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 100, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq25_5001_lets_be_a_friend_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5001);
end

function nq25_5001_lets_be_a_friend_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5001);
end

function nq25_5001_lets_be_a_friend_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5001);
	local questID=5001;
end

function nq25_5001_lets_be_a_friend_OnRemoteStart( userObjID, questID )
end

function nq25_5001_lets_be_a_friend_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq25_5001_lets_be_a_friend_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,5001, 1);
				api_quest_SetJournalStep(userObjID,5001, 1);
				api_quest_SetQuestStep(userObjID,5001, 1);
				npc_talk_index = "n100_event_ilyn-1";
end

</VillageServer>

<GameServer>
function nq25_5001_lets_be_a_friend_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 100 then
		nq25_5001_lets_be_a_friend_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function nq25_5001_lets_be_a_friend_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n100_event_ilyn-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,5001, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5001, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5001, 1);
				npc_talk_index = "n100_event_ilyn-1";

	end
	if npc_talk_index == "n100_event_ilyn-1-b" then 
	end
	if npc_talk_index == "n100_event_ilyn-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n100_event_ilyn-2" then 
	end
	if npc_talk_index == "n100_event_ilyn-2" then 
	end
	if npc_talk_index == "n100_event_ilyn-2-d" then 
	end
	if npc_talk_index == "n100_event_ilyn-2-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 100, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq25_5001_lets_be_a_friend_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5001);
end

function nq25_5001_lets_be_a_friend_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5001);
end

function nq25_5001_lets_be_a_friend_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5001);
	local questID=5001;
end

function nq25_5001_lets_be_a_friend_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq25_5001_lets_be_a_friend_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq25_5001_lets_be_a_friend_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,5001, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5001, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5001, 1);
				npc_talk_index = "n100_event_ilyn-1";
end

</GameServer>