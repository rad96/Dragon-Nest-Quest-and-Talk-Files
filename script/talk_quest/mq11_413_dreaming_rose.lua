<VillageServer>

function mq11_413_dreaming_rose_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 216 then
		mq11_413_dreaming_rose_OnTalk_n216_geraint_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 251 then
		mq11_413_dreaming_rose_OnTalk_n251_rose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mq11_413_dreaming_rose_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 76 then
		mq11_413_dreaming_rose_OnTalk_n076_dream_rose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n216_geraint_wounded--------------------------------------------------------------------------------
function mq11_413_dreaming_rose_OnTalk_n216_geraint_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n216_geraint_wounded-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n216_geraint_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n216_geraint_wounded-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n216_geraint_wounded-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n216_geraint_wounded-3-b" then 
	end
	if npc_talk_index == "n216_geraint_wounded-3-c" then 
	end
	if npc_talk_index == "n216_geraint_wounded-3-d" then 
	end
	if npc_talk_index == "n216_geraint_wounded-3-e" then 
	end
	if npc_talk_index == "n216_geraint_wounded-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n251_rose--------------------------------------------------------------------------------
function mq11_413_dreaming_rose_OnTalk_n251_rose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n251_rose-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n251_rose-1";
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n251_rose-1";

				else
									npc_talk_index = "n251_rose-1-a";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n251_rose-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n251_rose-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n251_rose-1-changemap_1" then 
				api_user_ChangeMap(userObjID,13016,1);
	end
	if npc_talk_index == "n251_rose-2-changemap_again" then 
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetJournalStep(userObjID, questID, 1);
				api_user_ChangeMap(userObjID,13016,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mq11_413_dreaming_rose_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n038_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n038_royal_magician_kalaen-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-accepting-acceptted" then
				api_quest_AddQuest(userObjID,413, 2);
				api_quest_SetJournalStep(userObjID,413, 1);
				api_quest_SetQuestStep(userObjID,413, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";

	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-e" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4131, true);
				 api_quest_RewardQuestUser(userObjID, 4131, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4132, true);
				 api_quest_RewardQuestUser(userObjID, 4132, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4133, true);
				 api_quest_RewardQuestUser(userObjID, 4133, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4134, true);
				 api_quest_RewardQuestUser(userObjID, 4134, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4135, true);
				 api_quest_RewardQuestUser(userObjID, 4135, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4136, true);
				 api_quest_RewardQuestUser(userObjID, 4136, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4131, true);
				 api_quest_RewardQuestUser(userObjID, 4131, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4138, true);
				 api_quest_RewardQuestUser(userObjID, 4138, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4131, true);
				 api_quest_RewardQuestUser(userObjID, 4131, questID, 1);
			 end 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-!next" then 
		local cqresult = 1
				api_quest_AddQuest(userObjID,414, 2);
				api_quest_SetJournalStep(userObjID,414, 1);
				api_quest_SetQuestStep(userObjID,414, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "-1", ".xml");
			return;
		end
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-i" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-!next2" then 
				api_quest_AddQuest(userObjID,9492, 2);
				api_quest_SetJournalStep(userObjID,9492, 1);
				api_quest_SetQuestStep(userObjID,9492, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-k" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-!next2" then 

		local cqresult = 1				api_quest_AddQuest(userObjID,414, 2);
				api_quest_SetJournalStep(userObjID,414, 1);
				api_quest_SetQuestStep(userObjID,414, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "-1", ".xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n076_dream_rose--------------------------------------------------------------------------------
function mq11_413_dreaming_rose_OnTalk_n076_dream_rose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n076_dream_rose-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n076_dream_rose-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n076_dream_rose-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n076_dream_rose-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n076_dream_rose-2-b" then 
	end
	if npc_talk_index == "n076_dream_rose-2-b" then 
	end
	if npc_talk_index == "n076_dream_rose-2-b" then 
	end
	if npc_talk_index == "n076_dream_rose-2-c" then 
	end
	if npc_talk_index == "n076_dream_rose-2-d" then 
	end
	if npc_talk_index == "n076_dream_rose-2-d" then 
	end
	if npc_talk_index == "n076_dream_rose-2-d" then 
	end
	if npc_talk_index == "n076_dream_rose-2-e" then 
	end
	if npc_talk_index == "n076_dream_rose-2-changemap_2" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				if api_quest_HasQuestItem(userObjID, 300083, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300083, api_quest_HasQuestItem(userObjID, 300083, 1));
				end
				api_user_ChangeMap(userObjID,13014,1);
	end
	if npc_talk_index == "n076_dream_rose-2-changemap_2" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				if api_quest_HasQuestItem(userObjID, 300083, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300083, api_quest_HasQuestItem(userObjID, 300083, 1));
				end
				api_user_ChangeMap(userObjID,13014,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_413_dreaming_rose_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 413);
end

function mq11_413_dreaming_rose_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 413);
end

function mq11_413_dreaming_rose_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 413);
	local questID=413;
end

function mq11_413_dreaming_rose_OnRemoteStart( userObjID, questID )
end

function mq11_413_dreaming_rose_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq11_413_dreaming_rose_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,413, 2);
				api_quest_SetJournalStep(userObjID,413, 1);
				api_quest_SetQuestStep(userObjID,413, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";
end

</VillageServer>

<GameServer>
function mq11_413_dreaming_rose_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 216 then
		mq11_413_dreaming_rose_OnTalk_n216_geraint_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 251 then
		mq11_413_dreaming_rose_OnTalk_n251_rose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mq11_413_dreaming_rose_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 76 then
		mq11_413_dreaming_rose_OnTalk_n076_dream_rose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n216_geraint_wounded--------------------------------------------------------------------------------
function mq11_413_dreaming_rose_OnTalk_n216_geraint_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n216_geraint_wounded-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n216_geraint_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n216_geraint_wounded-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n216_geraint_wounded-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n216_geraint_wounded-3-b" then 
	end
	if npc_talk_index == "n216_geraint_wounded-3-c" then 
	end
	if npc_talk_index == "n216_geraint_wounded-3-d" then 
	end
	if npc_talk_index == "n216_geraint_wounded-3-e" then 
	end
	if npc_talk_index == "n216_geraint_wounded-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n251_rose--------------------------------------------------------------------------------
function mq11_413_dreaming_rose_OnTalk_n251_rose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n251_rose-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n251_rose-1";
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n251_rose-1";

				else
									npc_talk_index = "n251_rose-1-a";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n251_rose-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n251_rose-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n251_rose-1-changemap_1" then 
				api_user_ChangeMap( pRoom, userObjID,13016,1);
	end
	if npc_talk_index == "n251_rose-2-changemap_again" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				api_user_ChangeMap( pRoom, userObjID,13016,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mq11_413_dreaming_rose_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n038_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n038_royal_magician_kalaen-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,413, 2);
				api_quest_SetJournalStep( pRoom, userObjID,413, 1);
				api_quest_SetQuestStep( pRoom, userObjID,413, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";

	end
	if npc_talk_index == "n038_royal_magician_kalaen-1-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-e" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4131, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4131, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4132, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4132, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4133, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4133, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4134, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4134, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4135, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4135, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4136, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4136, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4131, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4131, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4138, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4138, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4131, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4131, questID, 1);
			 end 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-!next" then 
		local cqresult = 1
				api_quest_AddQuest( pRoom, userObjID,414, 2);
				api_quest_SetJournalStep( pRoom, userObjID,414, 1);
				api_quest_SetQuestStep( pRoom, userObjID,414, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "-1", ".xml");
			return;
		end
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-i" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-!next2" then 
				api_quest_AddQuest( pRoom, userObjID,9492, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9492, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9492, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-k" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n038_royal_magician_kalaen-4-!next2" then 

		local cqresult = 1				api_quest_AddQuest( pRoom, userObjID,414, 2);
				api_quest_SetJournalStep( pRoom, userObjID,414, 1);
				api_quest_SetQuestStep( pRoom, userObjID,414, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "-1", ".xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n076_dream_rose--------------------------------------------------------------------------------
function mq11_413_dreaming_rose_OnTalk_n076_dream_rose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n076_dream_rose-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n076_dream_rose-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n076_dream_rose-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n076_dream_rose-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n076_dream_rose-2-b" then 
	end
	if npc_talk_index == "n076_dream_rose-2-b" then 
	end
	if npc_talk_index == "n076_dream_rose-2-b" then 
	end
	if npc_talk_index == "n076_dream_rose-2-c" then 
	end
	if npc_talk_index == "n076_dream_rose-2-d" then 
	end
	if npc_talk_index == "n076_dream_rose-2-d" then 
	end
	if npc_talk_index == "n076_dream_rose-2-d" then 
	end
	if npc_talk_index == "n076_dream_rose-2-e" then 
	end
	if npc_talk_index == "n076_dream_rose-2-changemap_2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				if api_quest_HasQuestItem( pRoom, userObjID, 300083, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300083, api_quest_HasQuestItem( pRoom, userObjID, 300083, 1));
				end
				api_user_ChangeMap( pRoom, userObjID,13014,1);
	end
	if npc_talk_index == "n076_dream_rose-2-changemap_2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				if api_quest_HasQuestItem( pRoom, userObjID, 300083, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300083, api_quest_HasQuestItem( pRoom, userObjID, 300083, 1));
				end
				api_user_ChangeMap( pRoom, userObjID,13014,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_413_dreaming_rose_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 413);
end

function mq11_413_dreaming_rose_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 413);
end

function mq11_413_dreaming_rose_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 413);
	local questID=413;
end

function mq11_413_dreaming_rose_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq11_413_dreaming_rose_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq11_413_dreaming_rose_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,413, 2);
				api_quest_SetJournalStep( pRoom, userObjID,413, 1);
				api_quest_SetQuestStep( pRoom, userObjID,413, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";
end

</GameServer>