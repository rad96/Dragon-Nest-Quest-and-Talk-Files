<VillageServer>

function dq15_1045_left_monster_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 801 then
		dq15_1045_left_monster_OnTalk_n801_sorceress_tessa(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n801_sorceress_tessa--------------------------------------------------------------------------------
function dq15_1045_left_monster_OnTalk_n801_sorceress_tessa(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n801_sorceress_tessa-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n801_sorceress_tessa-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n801_sorceress_tessa-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n801_sorceress_tessa-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 10450, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 10450, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 10450, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 10450, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 10450, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 10450, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 10450, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 10450, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 10450, false);
			 end 

	end
	if npc_talk_index == "n801_sorceress_tessa-accepting-acceptted" then
				api_quest_AddQuest(userObjID,1045, 1);
				api_quest_SetJournalStep(userObjID,1045, 1);
				api_quest_SetQuestStep(userObjID,1045, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 2043, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 202043, 30000);
				npc_talk_index = "n801_sorceress_tessa-1";

	end
	if npc_talk_index == "n801_sorceress_tessa-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 10450, true);
				 api_quest_RewardQuestUser(userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 10450, true);
				 api_quest_RewardQuestUser(userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 10450, true);
				 api_quest_RewardQuestUser(userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 10450, true);
				 api_quest_RewardQuestUser(userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 10450, true);
				 api_quest_RewardQuestUser(userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 10450, true);
				 api_quest_RewardQuestUser(userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 10450, true);
				 api_quest_RewardQuestUser(userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 10450, true);
				 api_quest_RewardQuestUser(userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 10450, true);
				 api_quest_RewardQuestUser(userObjID, 10450, questID, 1);
			 end 
	end
	if npc_talk_index == "n801_sorceress_tessa-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dq15_1045_left_monster_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1045);
	if qstep == 1 and CountIndex == 2043 then

	end
	if qstep == 1 and CountIndex == 202043 then
				api_quest_IncCounting(userObjID, 2, 2043);

	end
end

function dq15_1045_left_monster_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1045);
	if qstep == 1 and CountIndex == 2043 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 202043 and Count >= TargetCount  then

	end
end

function dq15_1045_left_monster_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1045);
	local questID=1045;
end

function dq15_1045_left_monster_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 2043, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 202043, 30000);
end

function dq15_1045_left_monster_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function dq15_1045_left_monster_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,1045, 1);
				api_quest_SetJournalStep(userObjID,1045, 1);
				api_quest_SetQuestStep(userObjID,1045, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 2043, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 202043, 30000);
				npc_talk_index = "n801_sorceress_tessa-1";
end

</VillageServer>

<GameServer>
function dq15_1045_left_monster_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 801 then
		dq15_1045_left_monster_OnTalk_n801_sorceress_tessa( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n801_sorceress_tessa--------------------------------------------------------------------------------
function dq15_1045_left_monster_OnTalk_n801_sorceress_tessa( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n801_sorceress_tessa-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n801_sorceress_tessa-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n801_sorceress_tessa-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n801_sorceress_tessa-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, false);
			 end 

	end
	if npc_talk_index == "n801_sorceress_tessa-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,1045, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1045, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1045, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 2043, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 202043, 30000);
				npc_talk_index = "n801_sorceress_tessa-1";

	end
	if npc_talk_index == "n801_sorceress_tessa-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 10450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 10450, questID, 1);
			 end 
	end
	if npc_talk_index == "n801_sorceress_tessa-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dq15_1045_left_monster_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1045);
	if qstep == 1 and CountIndex == 2043 then

	end
	if qstep == 1 and CountIndex == 202043 then
				api_quest_IncCounting( pRoom, userObjID, 2, 2043);

	end
end

function dq15_1045_left_monster_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1045);
	if qstep == 1 and CountIndex == 2043 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 202043 and Count >= TargetCount  then

	end
end

function dq15_1045_left_monster_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1045);
	local questID=1045;
end

function dq15_1045_left_monster_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 2043, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 202043, 30000);
end

function dq15_1045_left_monster_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function dq15_1045_left_monster_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,1045, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1045, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1045, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 2043, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 202043, 30000);
				npc_talk_index = "n801_sorceress_tessa-1";
end

</GameServer>