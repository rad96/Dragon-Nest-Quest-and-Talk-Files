<VillageServer>

function mq34_9289_gold_dragon_returns_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9289_gold_dragon_returns_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mq34_9289_gold_dragon_returns_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9289_gold_dragon_returns_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1047_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1047_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9289, 2);
				api_quest_SetJournalStep(userObjID,9289, 1);
				api_quest_SetQuestStep(userObjID,9289, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-1-levelcheck" then 
				if api_user_GetUserLevel(userObjID) >= 65 then
									npc_talk_index = "n1047_argenta-1-a";

				else
									npc_talk_index = "n1047_argenta-1-p";

				end
	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
	end
	if npc_talk_index == "n1047_argenta-1-f" then 
	end
	if npc_talk_index == "n1047_argenta-1-g" then 
	end
	if npc_talk_index == "n1047_argenta-1-h" then 
	end
	if npc_talk_index == "n1047_argenta-1-i" then 
	end
	if npc_talk_index == "n1047_argenta-1-j" then 
	end
	if npc_talk_index == "n1047_argenta-1-j" then 
	end
	if npc_talk_index == "n1047_argenta-1-j" then 
	end
	if npc_talk_index == "n1047_argenta-1-k" then 
	end
	if npc_talk_index == "n1047_argenta-1-l" then 
	end
	if npc_talk_index == "n1047_argenta-1-m" then 
	end
	if npc_talk_index == "n1047_argenta-1-n" then 
	end
	if npc_talk_index == "n1047_argenta-1-o" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1047_argenta-4-b" then 
	end
	if npc_talk_index == "n1047_argenta-4-c" then 
	end
	if npc_talk_index == "n1047_argenta-4-d" then 
	end
	if npc_talk_index == "n1047_argenta-4-e" then 
	end
	if npc_talk_index == "n1047_argenta-4-f" then 
	end
	if npc_talk_index == "n1047_argenta-4-chk_j_04" then 
				if api_user_GetUserClassID(userObjID) == 4 then
									npc_talk_index = "n1047_argenta-4-j";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1047_argenta-4-j";

				else
									npc_talk_index = "n1047_argenta-4-g";

				end

				end
	end
	if npc_talk_index == "n1047_argenta-4-h" then 
	end
	if npc_talk_index == "n1047_argenta-4-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92891, true);
				 api_quest_RewardQuestUser(userObjID, 92891, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92891, true);
				 api_quest_RewardQuestUser(userObjID, 92891, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92893, true);
				 api_quest_RewardQuestUser(userObjID, 92893, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92894, true);
				 api_quest_RewardQuestUser(userObjID, 92894, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92895, true);
				 api_quest_RewardQuestUser(userObjID, 92895, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92896, true);
				 api_quest_RewardQuestUser(userObjID, 92896, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92897, true);
				 api_quest_RewardQuestUser(userObjID, 92897, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92898, true);
				 api_quest_RewardQuestUser(userObjID, 92898, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92899, true);
				 api_quest_RewardQuestUser(userObjID, 92899, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9290, 2);
					api_quest_SetQuestStep(userObjID, 9290, 1);
					api_quest_SetJournalStep(userObjID, 9290, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1047_argenta-1", "mq34_9290_the_secret_of_anu_arendel.xml");
			return;
		end
	end
	if npc_talk_index == "n1047_argenta-4-h" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mq34_9289_gold_dragon_returns_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1065_geraint_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1065_geraint_kid-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9289_gold_dragon_returns_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9289);
end

function mq34_9289_gold_dragon_returns_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9289);
end

function mq34_9289_gold_dragon_returns_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9289);
	local questID=9289;
end

function mq34_9289_gold_dragon_returns_OnRemoteStart( userObjID, questID )
end

function mq34_9289_gold_dragon_returns_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9289_gold_dragon_returns_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9289, 2);
				api_quest_SetJournalStep(userObjID,9289, 1);
				api_quest_SetQuestStep(userObjID,9289, 1);
				npc_talk_index = "n1047_argenta-1";
end

</VillageServer>

<GameServer>
function mq34_9289_gold_dragon_returns_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9289_gold_dragon_returns_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mq34_9289_gold_dragon_returns_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9289_gold_dragon_returns_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1047_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1047_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9289, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9289, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9289, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-1-levelcheck" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 65 then
									npc_talk_index = "n1047_argenta-1-a";

				else
									npc_talk_index = "n1047_argenta-1-p";

				end
	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
	end
	if npc_talk_index == "n1047_argenta-1-f" then 
	end
	if npc_talk_index == "n1047_argenta-1-g" then 
	end
	if npc_talk_index == "n1047_argenta-1-h" then 
	end
	if npc_talk_index == "n1047_argenta-1-i" then 
	end
	if npc_talk_index == "n1047_argenta-1-j" then 
	end
	if npc_talk_index == "n1047_argenta-1-j" then 
	end
	if npc_talk_index == "n1047_argenta-1-j" then 
	end
	if npc_talk_index == "n1047_argenta-1-k" then 
	end
	if npc_talk_index == "n1047_argenta-1-l" then 
	end
	if npc_talk_index == "n1047_argenta-1-m" then 
	end
	if npc_talk_index == "n1047_argenta-1-n" then 
	end
	if npc_talk_index == "n1047_argenta-1-o" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1047_argenta-4-b" then 
	end
	if npc_talk_index == "n1047_argenta-4-c" then 
	end
	if npc_talk_index == "n1047_argenta-4-d" then 
	end
	if npc_talk_index == "n1047_argenta-4-e" then 
	end
	if npc_talk_index == "n1047_argenta-4-f" then 
	end
	if npc_talk_index == "n1047_argenta-4-chk_j_04" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									npc_talk_index = "n1047_argenta-4-j";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1047_argenta-4-j";

				else
									npc_talk_index = "n1047_argenta-4-g";

				end

				end
	end
	if npc_talk_index == "n1047_argenta-4-h" then 
	end
	if npc_talk_index == "n1047_argenta-4-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92891, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92891, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92891, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92891, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92893, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92893, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92894, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92894, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92895, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92895, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92896, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92896, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92897, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92897, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92898, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92898, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92899, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92899, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9290, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9290, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9290, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1047_argenta-1", "mq34_9290_the_secret_of_anu_arendel.xml");
			return;
		end
	end
	if npc_talk_index == "n1047_argenta-4-h" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mq34_9289_gold_dragon_returns_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1065_geraint_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1065_geraint_kid-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9289_gold_dragon_returns_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9289);
end

function mq34_9289_gold_dragon_returns_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9289);
end

function mq34_9289_gold_dragon_returns_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9289);
	local questID=9289;
end

function mq34_9289_gold_dragon_returns_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9289_gold_dragon_returns_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9289_gold_dragon_returns_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9289, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9289, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9289, 1);
				npc_talk_index = "n1047_argenta-1";
end

</GameServer>