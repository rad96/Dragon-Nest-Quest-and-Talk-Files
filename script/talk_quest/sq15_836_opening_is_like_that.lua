<VillageServer>

function sq15_836_opening_is_like_that_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 710 then
		sq15_836_opening_is_like_that_OnTalk_n710_scholar_hancock(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 711 then
		sq15_836_opening_is_like_that_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n710_scholar_hancock--------------------------------------------------------------------------------
function sq15_836_opening_is_like_that_OnTalk_n710_scholar_hancock(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n710_scholar_hancock-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n710_scholar_hancock-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n710_scholar_hancock-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n710_scholar_hancock-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n710_scholar_hancock-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n710_scholar_hancock-2-b" then 

				if api_quest_HasQuestItem(userObjID, 300461, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300461, api_quest_HasQuestItem(userObjID, 300461, 1));
				end
	end
	if npc_talk_index == "n710_scholar_hancock-2-c" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2-d" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2-e" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2-e" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2-f" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2-g" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2-h" then 
	end
	if npc_talk_index == "n710_scholar_hancock-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1451, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201451, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300462, 1);
	end
	if npc_talk_index == "n710_scholar_hancock-4-a" then 

				if api_quest_HasQuestItem(userObjID, 300462, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300462, api_quest_HasQuestItem(userObjID, 300462, 1));
				end
	end
	if npc_talk_index == "n710_scholar_hancock-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8360, true);
				 api_quest_RewardQuestUser(userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8360, true);
				 api_quest_RewardQuestUser(userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8360, true);
				 api_quest_RewardQuestUser(userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8360, true);
				 api_quest_RewardQuestUser(userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8360, true);
				 api_quest_RewardQuestUser(userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8360, true);
				 api_quest_RewardQuestUser(userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8360, true);
				 api_quest_RewardQuestUser(userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8360, true);
				 api_quest_RewardQuestUser(userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8360, true);
				 api_quest_RewardQuestUser(userObjID, 8360, questID, 1);
			 end 
	end
	if npc_talk_index == "n710_scholar_hancock-4-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 837, 1);
					api_quest_SetQuestStep(userObjID, 837, 1);
					api_quest_SetJournalStep(userObjID, 837, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n710_scholar_hancock-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n710_scholar_hancock-1", "sq15_837_shameful_past.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n711_warrior_fedro--------------------------------------------------------------------------------
function sq15_836_opening_is_like_that_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n711_warrior_fedro-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n711_warrior_fedro-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8360, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8360, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8360, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8360, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8360, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8360, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8360, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8360, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8360, false);
			 end 

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted" then
				api_quest_AddQuest(userObjID,836, 1);
				api_quest_SetJournalStep(userObjID,836, 1);
				api_quest_SetQuestStep(userObjID,836, 1);
				npc_talk_index = "n711_warrior_fedro-1";

	end
	if npc_talk_index == "n711_warrior_fedro-2" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300461, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300461, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_836_opening_is_like_that_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 836);
	if qstep == 3 and CountIndex == 1451 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300462, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300462, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 201451 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300462, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300462, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300462 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

	end
end

function sq15_836_opening_is_like_that_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 836);
	if qstep == 3 and CountIndex == 1451 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 201451 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300462 and Count >= TargetCount  then

	end
end

function sq15_836_opening_is_like_that_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 836);
	local questID=836;
end

function sq15_836_opening_is_like_that_OnRemoteStart( userObjID, questID )
end

function sq15_836_opening_is_like_that_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_836_opening_is_like_that_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,836, 1);
				api_quest_SetJournalStep(userObjID,836, 1);
				api_quest_SetQuestStep(userObjID,836, 1);
				npc_talk_index = "n711_warrior_fedro-1";
end

</VillageServer>

<GameServer>
function sq15_836_opening_is_like_that_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 710 then
		sq15_836_opening_is_like_that_OnTalk_n710_scholar_hancock( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 711 then
		sq15_836_opening_is_like_that_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n710_scholar_hancock--------------------------------------------------------------------------------
function sq15_836_opening_is_like_that_OnTalk_n710_scholar_hancock( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n710_scholar_hancock-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n710_scholar_hancock-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n710_scholar_hancock-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n710_scholar_hancock-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n710_scholar_hancock-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n710_scholar_hancock-2-b" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300461, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300461, api_quest_HasQuestItem( pRoom, userObjID, 300461, 1));
				end
	end
	if npc_talk_index == "n710_scholar_hancock-2-c" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2-d" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2-e" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2-e" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2-f" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2-g" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2-h" then 
	end
	if npc_talk_index == "n710_scholar_hancock-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1451, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201451, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300462, 1);
	end
	if npc_talk_index == "n710_scholar_hancock-4-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300462, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300462, api_quest_HasQuestItem( pRoom, userObjID, 300462, 1));
				end
	end
	if npc_talk_index == "n710_scholar_hancock-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8360, questID, 1);
			 end 
	end
	if npc_talk_index == "n710_scholar_hancock-4-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 837, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 837, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 837, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n710_scholar_hancock-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n710_scholar_hancock-1", "sq15_837_shameful_past.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n711_warrior_fedro--------------------------------------------------------------------------------
function sq15_836_opening_is_like_that_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n711_warrior_fedro-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n711_warrior_fedro-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8360, false);
			 end 

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,836, 1);
				api_quest_SetJournalStep( pRoom, userObjID,836, 1);
				api_quest_SetQuestStep( pRoom, userObjID,836, 1);
				npc_talk_index = "n711_warrior_fedro-1";

	end
	if npc_talk_index == "n711_warrior_fedro-2" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300461, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300461, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_836_opening_is_like_that_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 836);
	if qstep == 3 and CountIndex == 1451 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300462, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300462, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 201451 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300462, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300462, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300462 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

	end
end

function sq15_836_opening_is_like_that_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 836);
	if qstep == 3 and CountIndex == 1451 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 201451 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300462 and Count >= TargetCount  then

	end
end

function sq15_836_opening_is_like_that_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 836);
	local questID=836;
end

function sq15_836_opening_is_like_that_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_836_opening_is_like_that_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_836_opening_is_like_that_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,836, 1);
				api_quest_SetJournalStep( pRoom, userObjID,836, 1);
				api_quest_SetQuestStep( pRoom, userObjID,836, 1);
				npc_talk_index = "n711_warrior_fedro-1";
end

</GameServer>