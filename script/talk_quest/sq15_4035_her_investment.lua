<VillageServer>

function sq15_4035_her_investment_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 45 then
		sq15_4035_her_investment_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq15_4035_her_investment_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40350, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40350, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40350, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40350, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40350, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40350, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40350, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40350, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40350, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4035, 1);
				api_quest_SetJournalStep(userObjID,4035, 1);
				api_quest_SetQuestStep(userObjID,4035, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 600057, 5);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700057, 30000);

	end
	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40350, true);
				 api_quest_RewardQuestUser(userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40350, true);
				 api_quest_RewardQuestUser(userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40350, true);
				 api_quest_RewardQuestUser(userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40350, true);
				 api_quest_RewardQuestUser(userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40350, true);
				 api_quest_RewardQuestUser(userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40350, true);
				 api_quest_RewardQuestUser(userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40350, true);
				 api_quest_RewardQuestUser(userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40350, true);
				 api_quest_RewardQuestUser(userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40350, true);
				 api_quest_RewardQuestUser(userObjID, 40350, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4035_her_investment_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4035);
	if qstep == 1 and CountIndex == 600057 then

	end
	if qstep == 1 and CountIndex == 700057 then
				api_quest_IncCounting(userObjID, 2, 600057);

	end
end

function sq15_4035_her_investment_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4035);
	if qstep == 1 and CountIndex == 600057 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 700057 and Count >= TargetCount  then

	end
end

function sq15_4035_her_investment_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4035);
	local questID=4035;
end

function sq15_4035_her_investment_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 600057, 5);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700057, 30000);
end

function sq15_4035_her_investment_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4035_her_investment_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4035, 1);
				api_quest_SetJournalStep(userObjID,4035, 1);
				api_quest_SetQuestStep(userObjID,4035, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 600057, 5);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700057, 30000);
end

</VillageServer>

<GameServer>
function sq15_4035_her_investment_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 45 then
		sq15_4035_her_investment_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq15_4035_her_investment_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4035, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4035, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4035, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 600057, 5);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700057, 30000);

	end
	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40350, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4035_her_investment_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4035);
	if qstep == 1 and CountIndex == 600057 then

	end
	if qstep == 1 and CountIndex == 700057 then
				api_quest_IncCounting( pRoom, userObjID, 2, 600057);

	end
end

function sq15_4035_her_investment_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4035);
	if qstep == 1 and CountIndex == 600057 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 700057 and Count >= TargetCount  then

	end
end

function sq15_4035_her_investment_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4035);
	local questID=4035;
end

function sq15_4035_her_investment_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 600057, 5);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700057, 30000);
end

function sq15_4035_her_investment_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4035_her_investment_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4035, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4035, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4035, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 600057, 5);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700057, 30000);
end

</GameServer>