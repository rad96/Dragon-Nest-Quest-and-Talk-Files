<VillageServer>

function mqc01_9461_proof_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 158 then
		mqc01_9461_proof_OnTalk_n158_point_mark(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1686 then
		mqc01_9461_proof_OnTalk_n1686_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1687 then
		mqc01_9461_proof_OnTalk_n1687_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n158_point_mark--------------------------------------------------------------------------------
function mqc01_9461_proof_OnTalk_n158_point_mark(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n158_point_mark-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n158_point_mark-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n158_point_mark-3-a" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400453, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400453, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1686_eltia--------------------------------------------------------------------------------
function mqc01_9461_proof_OnTalk_n1686_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1686_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1686_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1686_eltia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1686_eltia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1686_eltia-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1686_eltia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9461, 2);
				api_quest_SetJournalStep(userObjID,9461, 1);
				api_quest_SetQuestStep(userObjID,9461, 1);
				npc_talk_index = "n1686_eltia-1";

	end
	if npc_talk_index == "n1686_eltia-1-b" then 
	end
	if npc_talk_index == "n1686_eltia-1-c" then 
	end
	if npc_talk_index == "n1686_eltia-1-d" then 
	end
	if npc_talk_index == "n1686_eltia-1-e" then 
	end
	if npc_talk_index == "n1686_eltia-1-f" then 
	end
	if npc_talk_index == "n1686_eltia-1-g" then 
	end
	if npc_talk_index == "n1686_eltia-1-h" then 
	end
	if npc_talk_index == "n1686_eltia-1-h" then 
	end
	if npc_talk_index == "n1686_eltia-1-h" then 
	end
	if npc_talk_index == "n1686_eltia-1-i" then 
	end
	if npc_talk_index == "n1686_eltia-1-j" then 
	end
	if npc_talk_index == "n1686_eltia-1-k" then 
	end
	if npc_talk_index == "n1686_eltia-1-l" then 
	end
	if npc_talk_index == "n1686_eltia-1-m" then 
	end
	if npc_talk_index == "n1686_eltia-1-n" then 
	end
	if npc_talk_index == "n1686_eltia-1-o" then 
	end
	if npc_talk_index == "n1686_eltia-1-p" then 
	end
	if npc_talk_index == "n1686_eltia-1-q" then 
	end
	if npc_talk_index == "n1686_eltia-1-r" then 
	end
	if npc_talk_index == "n1686_eltia-1-s" then 
	end
	if npc_talk_index == "n1686_eltia-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1686_eltia-4-c" then 
	end
	if npc_talk_index == "n1686_eltia-4-c" then 
	end
	if npc_talk_index == "n1686_eltia-4-d" then 
	end
	if npc_talk_index == "n1686_eltia-4-e" then 
	end
	if npc_talk_index == "n1686_eltia-4-f" then 
	end
	if npc_talk_index == "n1686_eltia-4-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94610, true);
				 api_quest_RewardQuestUser(userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94610, true);
				 api_quest_RewardQuestUser(userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94610, true);
				 api_quest_RewardQuestUser(userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94610, true);
				 api_quest_RewardQuestUser(userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94610, true);
				 api_quest_RewardQuestUser(userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94610, true);
				 api_quest_RewardQuestUser(userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94610, true);
				 api_quest_RewardQuestUser(userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94610, true);
				 api_quest_RewardQuestUser(userObjID, 94610, questID, 1);
			 end 
	end
	if npc_talk_index == "n1686_eltia-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9462, 2);
					api_quest_SetQuestStep(userObjID, 9462, 1);
					api_quest_SetJournalStep(userObjID, 9462, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1686_eltia-1", "mqc01_9462_doubtful_motion.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1687_kaye--------------------------------------------------------------------------------
function mqc01_9461_proof_OnTalk_n1687_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1687_kaye-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1687_kaye-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1687_kaye-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1687_kaye-2-b" then 
	end
	if npc_talk_index == "n1687_kaye-2-c" then 
	end
	if npc_talk_index == "n1687_kaye-2-d" then 
	end
	if npc_talk_index == "n1687_kaye-2-e" then 
	end
	if npc_talk_index == "n1687_kaye-2-f" then 
	end
	if npc_talk_index == "n1687_kaye-2-g" then 
	end
	if npc_talk_index == "n1687_kaye-2-h" then 
	end
	if npc_talk_index == "n1687_kaye-2-i" then 
	end
	if npc_talk_index == "n1687_kaye-2-j" then 
	end
	if npc_talk_index == "n1687_kaye-2-k" then 
	end
	if npc_talk_index == "n1687_kaye-2-l" then 
	end
	if npc_talk_index == "n1687_kaye-2-m" then 
	end
	if npc_talk_index == "n1687_kaye-2-n" then 
	end
	if npc_talk_index == "n1687_kaye-2-o" then 
	end
	if npc_talk_index == "n1687_kaye-2-p" then 
	end
	if npc_talk_index == "n1687_kaye-2-q" then 
	end
	if npc_talk_index == "n1687_kaye-2-r" then 
	end
	if npc_talk_index == "n1687_kaye-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9461_proof_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9461);
end

function mqc01_9461_proof_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9461);
end

function mqc01_9461_proof_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9461);
	local questID=9461;
end

function mqc01_9461_proof_OnRemoteStart( userObjID, questID )
end

function mqc01_9461_proof_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc01_9461_proof_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9461, 2);
				api_quest_SetJournalStep(userObjID,9461, 1);
				api_quest_SetQuestStep(userObjID,9461, 1);
				npc_talk_index = "n1686_eltia-1";
end

</VillageServer>

<GameServer>
function mqc01_9461_proof_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 158 then
		mqc01_9461_proof_OnTalk_n158_point_mark( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1686 then
		mqc01_9461_proof_OnTalk_n1686_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1687 then
		mqc01_9461_proof_OnTalk_n1687_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n158_point_mark--------------------------------------------------------------------------------
function mqc01_9461_proof_OnTalk_n158_point_mark( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n158_point_mark-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n158_point_mark-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n158_point_mark-3-a" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400453, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400453, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1686_eltia--------------------------------------------------------------------------------
function mqc01_9461_proof_OnTalk_n1686_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1686_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1686_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1686_eltia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1686_eltia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1686_eltia-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1686_eltia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9461, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9461, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9461, 1);
				npc_talk_index = "n1686_eltia-1";

	end
	if npc_talk_index == "n1686_eltia-1-b" then 
	end
	if npc_talk_index == "n1686_eltia-1-c" then 
	end
	if npc_talk_index == "n1686_eltia-1-d" then 
	end
	if npc_talk_index == "n1686_eltia-1-e" then 
	end
	if npc_talk_index == "n1686_eltia-1-f" then 
	end
	if npc_talk_index == "n1686_eltia-1-g" then 
	end
	if npc_talk_index == "n1686_eltia-1-h" then 
	end
	if npc_talk_index == "n1686_eltia-1-h" then 
	end
	if npc_talk_index == "n1686_eltia-1-h" then 
	end
	if npc_talk_index == "n1686_eltia-1-i" then 
	end
	if npc_talk_index == "n1686_eltia-1-j" then 
	end
	if npc_talk_index == "n1686_eltia-1-k" then 
	end
	if npc_talk_index == "n1686_eltia-1-l" then 
	end
	if npc_talk_index == "n1686_eltia-1-m" then 
	end
	if npc_talk_index == "n1686_eltia-1-n" then 
	end
	if npc_talk_index == "n1686_eltia-1-o" then 
	end
	if npc_talk_index == "n1686_eltia-1-p" then 
	end
	if npc_talk_index == "n1686_eltia-1-q" then 
	end
	if npc_talk_index == "n1686_eltia-1-r" then 
	end
	if npc_talk_index == "n1686_eltia-1-s" then 
	end
	if npc_talk_index == "n1686_eltia-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1686_eltia-4-c" then 
	end
	if npc_talk_index == "n1686_eltia-4-c" then 
	end
	if npc_talk_index == "n1686_eltia-4-d" then 
	end
	if npc_talk_index == "n1686_eltia-4-e" then 
	end
	if npc_talk_index == "n1686_eltia-4-f" then 
	end
	if npc_talk_index == "n1686_eltia-4-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94610, questID, 1);
			 end 
	end
	if npc_talk_index == "n1686_eltia-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9462, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9462, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9462, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1686_eltia-1", "mqc01_9462_doubtful_motion.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1687_kaye--------------------------------------------------------------------------------
function mqc01_9461_proof_OnTalk_n1687_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1687_kaye-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1687_kaye-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1687_kaye-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1687_kaye-2-b" then 
	end
	if npc_talk_index == "n1687_kaye-2-c" then 
	end
	if npc_talk_index == "n1687_kaye-2-d" then 
	end
	if npc_talk_index == "n1687_kaye-2-e" then 
	end
	if npc_talk_index == "n1687_kaye-2-f" then 
	end
	if npc_talk_index == "n1687_kaye-2-g" then 
	end
	if npc_talk_index == "n1687_kaye-2-h" then 
	end
	if npc_talk_index == "n1687_kaye-2-i" then 
	end
	if npc_talk_index == "n1687_kaye-2-j" then 
	end
	if npc_talk_index == "n1687_kaye-2-k" then 
	end
	if npc_talk_index == "n1687_kaye-2-l" then 
	end
	if npc_talk_index == "n1687_kaye-2-m" then 
	end
	if npc_talk_index == "n1687_kaye-2-n" then 
	end
	if npc_talk_index == "n1687_kaye-2-o" then 
	end
	if npc_talk_index == "n1687_kaye-2-p" then 
	end
	if npc_talk_index == "n1687_kaye-2-q" then 
	end
	if npc_talk_index == "n1687_kaye-2-r" then 
	end
	if npc_talk_index == "n1687_kaye-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9461_proof_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9461);
end

function mqc01_9461_proof_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9461);
end

function mqc01_9461_proof_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9461);
	local questID=9461;
end

function mqc01_9461_proof_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc01_9461_proof_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc01_9461_proof_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9461, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9461, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9461, 1);
				npc_talk_index = "n1686_eltia-1";
end

</GameServer>