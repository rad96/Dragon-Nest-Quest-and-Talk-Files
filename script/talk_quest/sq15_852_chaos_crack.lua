<VillageServer>

function sq15_852_chaos_crack_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 714 then
		sq15_852_chaos_crack_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 732 then
		sq15_852_chaos_crack_OnTalk_n732_beggar_rabbit(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n714_cleric_yohan--------------------------------------------------------------------------------
function sq15_852_chaos_crack_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n714_cleric_yohan-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n714_cleric_yohan-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n714_cleric_yohan-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8520, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8520, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8520, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8520, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8520, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8520, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8520, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8520, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8520, false);
			 end 

	end
	if npc_talk_index == "n714_cleric_yohan-accepting-acceptted" then
				api_quest_AddQuest(userObjID,852, 1);
				api_quest_SetJournalStep(userObjID,852, 1);
				api_quest_SetQuestStep(userObjID,852, 1);
				npc_talk_index = "n714_cleric_yohan-2";
				api_quest_SetQuestStep(userObjID, questID,2);

	end
	if npc_talk_index == "n714_cleric_yohan-2-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-2-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-2-d" then 
	end
	if npc_talk_index == "n714_cleric_yohan-2-e" then 
	end
	if npc_talk_index == "n714_cleric_yohan-2-f" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1" then 
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n732_beggar_rabbit--------------------------------------------------------------------------------
function sq15_852_chaos_crack_OnTalk_n732_beggar_rabbit(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n732_beggar_rabbit-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n732_beggar_rabbit-1-b" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-c" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-d" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-e" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-f" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-g" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-h" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-i" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-j" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8520, true);
				 api_quest_RewardQuestUser(userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8520, true);
				 api_quest_RewardQuestUser(userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8520, true);
				 api_quest_RewardQuestUser(userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8520, true);
				 api_quest_RewardQuestUser(userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8520, true);
				 api_quest_RewardQuestUser(userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8520, true);
				 api_quest_RewardQuestUser(userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8520, true);
				 api_quest_RewardQuestUser(userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8520, true);
				 api_quest_RewardQuestUser(userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8520, true);
				 api_quest_RewardQuestUser(userObjID, 8520, questID, 1);
			 end 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-l" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_852_chaos_crack_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 852);
end

function sq15_852_chaos_crack_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 852);
end

function sq15_852_chaos_crack_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 852);
	local questID=852;
end

function sq15_852_chaos_crack_OnRemoteStart( userObjID, questID )
				api_quest_SetQuestStep(userObjID, questID,2);
end

function sq15_852_chaos_crack_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_852_chaos_crack_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,852, 1);
				api_quest_SetJournalStep(userObjID,852, 1);
				api_quest_SetQuestStep(userObjID,852, 1);
				npc_talk_index = "n714_cleric_yohan-2";
				api_quest_SetQuestStep(userObjID, questID,2);
end

</VillageServer>

<GameServer>
function sq15_852_chaos_crack_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 714 then
		sq15_852_chaos_crack_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 732 then
		sq15_852_chaos_crack_OnTalk_n732_beggar_rabbit( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n714_cleric_yohan--------------------------------------------------------------------------------
function sq15_852_chaos_crack_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n714_cleric_yohan-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n714_cleric_yohan-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n714_cleric_yohan-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, false);
			 end 

	end
	if npc_talk_index == "n714_cleric_yohan-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,852, 1);
				api_quest_SetJournalStep( pRoom, userObjID,852, 1);
				api_quest_SetQuestStep( pRoom, userObjID,852, 1);
				npc_talk_index = "n714_cleric_yohan-2";
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);

	end
	if npc_talk_index == "n714_cleric_yohan-2-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-2-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-2-d" then 
	end
	if npc_talk_index == "n714_cleric_yohan-2-e" then 
	end
	if npc_talk_index == "n714_cleric_yohan-2-f" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n732_beggar_rabbit--------------------------------------------------------------------------------
function sq15_852_chaos_crack_OnTalk_n732_beggar_rabbit( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n732_beggar_rabbit-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n732_beggar_rabbit-1-b" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-c" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-d" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-e" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-f" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-g" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-h" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-i" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-j" then 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8520, questID, 1);
			 end 
	end
	if npc_talk_index == "n732_beggar_rabbit-1-l" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_852_chaos_crack_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 852);
end

function sq15_852_chaos_crack_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 852);
end

function sq15_852_chaos_crack_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 852);
	local questID=852;
end

function sq15_852_chaos_crack_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
end

function sq15_852_chaos_crack_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_852_chaos_crack_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,852, 1);
				api_quest_SetJournalStep( pRoom, userObjID,852, 1);
				api_quest_SetQuestStep( pRoom, userObjID,852, 1);
				npc_talk_index = "n714_cleric_yohan-2";
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
end

</GameServer>