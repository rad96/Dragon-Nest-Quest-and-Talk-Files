<VillageServer>

function sq08_329_unknown_order_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 26 then
		sq08_329_unknown_order_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_329_unknown_order_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3291, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3292, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3293, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3294, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3295, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3296, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3297, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3298, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3299, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest(userObjID,329, 1);
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetJournalStep(userObjID, questID, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400110, 20);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 507, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 508, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 510, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200507, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200508, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 200509, 30000);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3291, true);
				 api_quest_RewardQuestUser(userObjID, 3291, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3292, true);
				 api_quest_RewardQuestUser(userObjID, 3292, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3293, true);
				 api_quest_RewardQuestUser(userObjID, 3293, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3294, true);
				 api_quest_RewardQuestUser(userObjID, 3294, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3295, true);
				 api_quest_RewardQuestUser(userObjID, 3295, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3296, true);
				 api_quest_RewardQuestUser(userObjID, 3296, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3297, true);
				 api_quest_RewardQuestUser(userObjID, 3297, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3298, true);
				 api_quest_RewardQuestUser(userObjID, 3298, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3299, true);
				 api_quest_RewardQuestUser(userObjID, 3299, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-2-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_329_unknown_order_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 329);
	if qstep == 1 and CountIndex == 507 then
				if api_quest_HasQuestItem(userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400110, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400110, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 508 then
				if api_quest_HasQuestItem(userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400110, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400110, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 510 then
				if api_quest_HasQuestItem(userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400110, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400110, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 400110 then
				if api_quest_HasQuestItem(userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

	end
	if qstep == 1 and CountIndex == 200507 then
				if api_quest_HasQuestItem(userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400110, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400110, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 200508 then
				if api_quest_HasQuestItem(userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400110, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400110, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 200509 then
				if api_quest_HasQuestItem(userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400110, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400110, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
end

function sq08_329_unknown_order_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 329);
	if qstep == 1 and CountIndex == 507 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 508 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 510 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400110 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200507 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200508 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200509 and Count >= TargetCount  then

	end
end

function sq08_329_unknown_order_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 329);
	local questID=329;
end

function sq08_329_unknown_order_OnRemoteStart( userObjID, questID )
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetJournalStep(userObjID, questID, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400110, 20);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 507, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 508, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 510, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200507, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200508, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 200509, 30000);
end

function sq08_329_unknown_order_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_329_unknown_order_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,329, 1);
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetJournalStep(userObjID, questID, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400110, 20);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 507, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 508, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 510, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200507, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200508, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 200509, 30000);
				npc_talk_index = "n026_trader_may-1";
end

</VillageServer>

<GameServer>
function sq08_329_unknown_order_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 26 then
		sq08_329_unknown_order_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_329_unknown_order_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3291, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3292, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3293, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3294, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3295, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3296, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3297, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3298, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3299, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,329, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400110, 20);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 507, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 508, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 510, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200507, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200508, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 200509, 30000);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3291, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3291, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3292, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3292, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3293, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3293, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3294, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3294, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3295, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3295, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3296, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3296, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3297, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3297, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3298, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3298, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3299, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3299, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-2-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_329_unknown_order_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 329);
	if qstep == 1 and CountIndex == 507 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400110, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400110, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 508 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400110, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400110, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 510 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400110, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400110, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 400110 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

	end
	if qstep == 1 and CountIndex == 200507 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400110, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400110, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 200508 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400110, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400110, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 1 and CountIndex == 200509 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400110, 20) >= 20 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400110, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400110, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
end

function sq08_329_unknown_order_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 329);
	if qstep == 1 and CountIndex == 507 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 508 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 510 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400110 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200507 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200508 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200509 and Count >= TargetCount  then

	end
end

function sq08_329_unknown_order_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 329);
	local questID=329;
end

function sq08_329_unknown_order_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400110, 20);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 507, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 508, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 510, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200507, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200508, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 200509, 30000);
end

function sq08_329_unknown_order_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_329_unknown_order_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,329, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400110, 20);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 507, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 508, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 510, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200507, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200508, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 200509, 30000);
				npc_talk_index = "n026_trader_may-1";
end

</GameServer>