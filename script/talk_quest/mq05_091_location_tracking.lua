<VillageServer>

function mq05_091_location_tracking_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 16 then
		mq05_091_location_tracking_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 391 then
		mq05_091_location_tracking_OnTalk_n391_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n016_trader_borin--------------------------------------------------------------------------------
function mq05_091_location_tracking_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-2-b" then 
	end
	if npc_talk_index == "n016_trader_borin-2-c" then 
	end
	if npc_talk_index == "n016_trader_borin-2-d" then 
	end
	if npc_talk_index == "n016_trader_borin-2-e" then 
	end
	if npc_talk_index == "n016_trader_borin-2-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 910, true);
				 api_quest_RewardQuestUser(userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 910, true);
				 api_quest_RewardQuestUser(userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 910, true);
				 api_quest_RewardQuestUser(userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 910, true);
				 api_quest_RewardQuestUser(userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 910, true);
				 api_quest_RewardQuestUser(userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 910, true);
				 api_quest_RewardQuestUser(userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 910, true);
				 api_quest_RewardQuestUser(userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 910, true);
				 api_quest_RewardQuestUser(userObjID, 910, questID, 1);
			 end 
	end
	if npc_talk_index == "n016_trader_borin-2-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 92, 2);
					api_quest_SetQuestStep(userObjID, 92, 1);
					api_quest_SetJournalStep(userObjID, 92, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n016_trader_borin-2-h" then 
	end
	if npc_talk_index == "n016_trader_borin-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n016_trader_borin-1", "mq05_092_tools_to_rebuild.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n391_academic_station--------------------------------------------------------------------------------
function mq05_091_location_tracking_OnTalk_n391_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n391_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n391_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n391_academic_station-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n391_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,91, 2);
				api_quest_SetJournalStep(userObjID,91, 1);
				api_quest_SetQuestStep(userObjID,91, 1);
				npc_talk_index = "n391_academic_station-1";

	end
	if npc_talk_index == "n391_academic_station-1-b" then 
	end
	if npc_talk_index == "n391_academic_station-1-c" then 
	end
	if npc_talk_index == "n391_academic_station-1-d" then 
	end
	if npc_talk_index == "n391_academic_station-1-e" then 
	end
	if npc_talk_index == "n391_academic_station-1-f" then 
	end
	if npc_talk_index == "n391_academic_station-1-g" then 
	end
	if npc_talk_index == "n391_academic_station-1-h" then 
	end
	if npc_talk_index == "n391_academic_station-1-h" then 
	end
	if npc_talk_index == "n391_academic_station-1-i" then 
	end
	if npc_talk_index == "n391_academic_station-1-j" then 
	end
	if npc_talk_index == "n391_academic_station-1-k" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_091_location_tracking_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 91);
end

function mq05_091_location_tracking_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 91);
end

function mq05_091_location_tracking_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 91);
	local questID=91;
end

function mq05_091_location_tracking_OnRemoteStart( userObjID, questID )
end

function mq05_091_location_tracking_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq05_091_location_tracking_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,91, 2);
				api_quest_SetJournalStep(userObjID,91, 1);
				api_quest_SetQuestStep(userObjID,91, 1);
				npc_talk_index = "n391_academic_station-1";
end

</VillageServer>

<GameServer>
function mq05_091_location_tracking_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 16 then
		mq05_091_location_tracking_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 391 then
		mq05_091_location_tracking_OnTalk_n391_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n016_trader_borin--------------------------------------------------------------------------------
function mq05_091_location_tracking_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-2-b" then 
	end
	if npc_talk_index == "n016_trader_borin-2-c" then 
	end
	if npc_talk_index == "n016_trader_borin-2-d" then 
	end
	if npc_talk_index == "n016_trader_borin-2-e" then 
	end
	if npc_talk_index == "n016_trader_borin-2-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 910, questID, 1);
			 end 
	end
	if npc_talk_index == "n016_trader_borin-2-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 92, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 92, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 92, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n016_trader_borin-2-h" then 
	end
	if npc_talk_index == "n016_trader_borin-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n016_trader_borin-1", "mq05_092_tools_to_rebuild.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n391_academic_station--------------------------------------------------------------------------------
function mq05_091_location_tracking_OnTalk_n391_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n391_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n391_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n391_academic_station-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n391_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,91, 2);
				api_quest_SetJournalStep( pRoom, userObjID,91, 1);
				api_quest_SetQuestStep( pRoom, userObjID,91, 1);
				npc_talk_index = "n391_academic_station-1";

	end
	if npc_talk_index == "n391_academic_station-1-b" then 
	end
	if npc_talk_index == "n391_academic_station-1-c" then 
	end
	if npc_talk_index == "n391_academic_station-1-d" then 
	end
	if npc_talk_index == "n391_academic_station-1-e" then 
	end
	if npc_talk_index == "n391_academic_station-1-f" then 
	end
	if npc_talk_index == "n391_academic_station-1-g" then 
	end
	if npc_talk_index == "n391_academic_station-1-h" then 
	end
	if npc_talk_index == "n391_academic_station-1-h" then 
	end
	if npc_talk_index == "n391_academic_station-1-i" then 
	end
	if npc_talk_index == "n391_academic_station-1-j" then 
	end
	if npc_talk_index == "n391_academic_station-1-k" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_091_location_tracking_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 91);
end

function mq05_091_location_tracking_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 91);
end

function mq05_091_location_tracking_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 91);
	local questID=91;
end

function mq05_091_location_tracking_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq05_091_location_tracking_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq05_091_location_tracking_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,91, 2);
				api_quest_SetJournalStep( pRoom, userObjID,91, 1);
				api_quest_SetQuestStep( pRoom, userObjID,91, 1);
				npc_talk_index = "n391_academic_station-1";
end

</GameServer>