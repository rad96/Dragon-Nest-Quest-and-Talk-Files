<VillageServer>

function mq08_191_the_illusion_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1121 then
		mq08_191_the_illusion_OnTalk_n1121_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1122 then
		mq08_191_the_illusion_OnTalk_n1122_darkelf_elena(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1252 then
		mq08_191_the_illusion_OnTalk_n1252_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mq08_191_the_illusion_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1121_lunaria--------------------------------------------------------------------------------
function mq08_191_the_illusion_OnTalk_n1121_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1121_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1121_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1121_lunaria-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1121_lunaria-3-b" then 
	end
	if npc_talk_index == "n1121_lunaria-3-c" then 
	end
	if npc_talk_index == "n1121_lunaria-3-d" then 
	end
	if npc_talk_index == "n1121_lunaria-3-e" then 
	end
	if npc_talk_index == "n1121_lunaria-3-f" then 
	end
	if npc_talk_index == "n1121_lunaria-3-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1910, true);
				 api_quest_RewardQuestUser(userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1910, true);
				 api_quest_RewardQuestUser(userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1910, true);
				 api_quest_RewardQuestUser(userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1910, true);
				 api_quest_RewardQuestUser(userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1910, true);
				 api_quest_RewardQuestUser(userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1910, true);
				 api_quest_RewardQuestUser(userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1910, true);
				 api_quest_RewardQuestUser(userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1910, true);
				 api_quest_RewardQuestUser(userObjID, 1910, questID, 1);
			 end 
	end
	if npc_talk_index == "n1121_lunaria-3-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 192, 2);
					api_quest_SetQuestStep(userObjID, 192, 1);
					api_quest_SetJournalStep(userObjID, 192, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1121_lunaria-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1121_lunaria-1", "mq08_192_rationality.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1122_darkelf_elena--------------------------------------------------------------------------------
function mq08_191_the_illusion_OnTalk_n1122_darkelf_elena(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1122_darkelf_elena-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1122_darkelf_elena-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1122_darkelf_elena-1-b" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-c" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-d" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-e" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-f" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-g" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-h" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-i" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-j" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-k" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-k" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-l" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-m" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1252_lunaria--------------------------------------------------------------------------------
function mq08_191_the_illusion_OnTalk_n1252_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1252_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mq08_191_the_illusion_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest(userObjID,191, 2);
				api_quest_SetJournalStep(userObjID,191, 1);
				api_quest_SetQuestStep(userObjID,191, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_191_the_illusion_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 191);
end

function mq08_191_the_illusion_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 191);
end

function mq08_191_the_illusion_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 191);
	local questID=191;
end

function mq08_191_the_illusion_OnRemoteStart( userObjID, questID )
end

function mq08_191_the_illusion_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq08_191_the_illusion_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,191, 2);
				api_quest_SetJournalStep(userObjID,191, 1);
				api_quest_SetQuestStep(userObjID,191, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
end

</VillageServer>

<GameServer>
function mq08_191_the_illusion_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1121 then
		mq08_191_the_illusion_OnTalk_n1121_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1122 then
		mq08_191_the_illusion_OnTalk_n1122_darkelf_elena( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1252 then
		mq08_191_the_illusion_OnTalk_n1252_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mq08_191_the_illusion_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1121_lunaria--------------------------------------------------------------------------------
function mq08_191_the_illusion_OnTalk_n1121_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1121_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1121_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1121_lunaria-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1121_lunaria-3-b" then 
	end
	if npc_talk_index == "n1121_lunaria-3-c" then 
	end
	if npc_talk_index == "n1121_lunaria-3-d" then 
	end
	if npc_talk_index == "n1121_lunaria-3-e" then 
	end
	if npc_talk_index == "n1121_lunaria-3-f" then 
	end
	if npc_talk_index == "n1121_lunaria-3-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1910, questID, 1);
			 end 
	end
	if npc_talk_index == "n1121_lunaria-3-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 192, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 192, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 192, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1121_lunaria-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1121_lunaria-1", "mq08_192_rationality.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1122_darkelf_elena--------------------------------------------------------------------------------
function mq08_191_the_illusion_OnTalk_n1122_darkelf_elena( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1122_darkelf_elena-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1122_darkelf_elena-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1122_darkelf_elena-1-b" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-c" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-d" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-e" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-f" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-g" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-h" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-i" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-j" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-k" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-k" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-l" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-1-m" then 
	end
	if npc_talk_index == "n1122_darkelf_elena-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1252_lunaria--------------------------------------------------------------------------------
function mq08_191_the_illusion_OnTalk_n1252_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1252_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mq08_191_the_illusion_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,191, 2);
				api_quest_SetJournalStep( pRoom, userObjID,191, 1);
				api_quest_SetQuestStep( pRoom, userObjID,191, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_191_the_illusion_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 191);
end

function mq08_191_the_illusion_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 191);
end

function mq08_191_the_illusion_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 191);
	local questID=191;
end

function mq08_191_the_illusion_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq08_191_the_illusion_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq08_191_the_illusion_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,191, 2);
				api_quest_SetJournalStep( pRoom, userObjID,191, 1);
				api_quest_SetQuestStep( pRoom, userObjID,191, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
end

</GameServer>