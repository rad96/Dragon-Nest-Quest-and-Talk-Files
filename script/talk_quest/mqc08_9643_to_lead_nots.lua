<VillageServer>

function mqc08_9643_to_lead_nots_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1897 then
		mqc08_9643_to_lead_nots_OnTalk_n1897_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1912 then
		mqc08_9643_to_lead_nots_OnTalk_n1912_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1916 then
		mqc08_9643_to_lead_nots_OnTalk_n1916_sorceress_lantana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1897_shaolong--------------------------------------------------------------------------------
function mqc08_9643_to_lead_nots_OnTalk_n1897_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1897_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1897_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1897_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1897_shaolong-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1897_shaolong-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9643, 2);
				api_quest_SetJournalStep(userObjID,9643, 1);
				api_quest_SetQuestStep(userObjID,9643, 1);
				npc_talk_index = "n1897_shaolong-1";

	end
	if npc_talk_index == "n1897_shaolong-1-b" then 
	end
	if npc_talk_index == "n1897_shaolong-1-c" then 
	end
	if npc_talk_index == "n1897_shaolong-1-d" then 
	end
	if npc_talk_index == "n1897_shaolong-1-e" then 
	end
	if npc_talk_index == "n1897_shaolong-1-f" then 
	end
	if npc_talk_index == "n1897_shaolong-1-g" then 
	end
	if npc_talk_index == "n1897_shaolong-1-h" then 
	end
	if npc_talk_index == "n1897_shaolong-1-i" then 
	end
	if npc_talk_index == "n1897_shaolong-1-j" then 
	end
	if npc_talk_index == "n1897_shaolong-1-k" then 
	end
	if npc_talk_index == "n1897_shaolong-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1897_shaolong-5-levelchk" then 
				if api_user_GetUserLevel(userObjID) >= 50 then
									npc_talk_index = "n1897_shaolong-5-b";

				else
									npc_talk_index = "n1897_shaolong-5-q";

				end
	end
	if npc_talk_index == "n1897_shaolong-5-c" then 
	end
	if npc_talk_index == "n1897_shaolong-5-d" then 
	end
	if npc_talk_index == "n1897_shaolong-5-e" then 
	end
	if npc_talk_index == "n1897_shaolong-5-f" then 
	end
	if npc_talk_index == "n1897_shaolong-5-g" then 
	end
	if npc_talk_index == "n1897_shaolong-5-h" then 
	end
	if npc_talk_index == "n1897_shaolong-5-i" then 
	end
	if npc_talk_index == "n1897_shaolong-5-j" then 
	end
	if npc_talk_index == "n1897_shaolong-5-k" then 
	end
	if npc_talk_index == "n1897_shaolong-5-l" then 
	end
	if npc_talk_index == "n1897_shaolong-5-m" then 
	end
	if npc_talk_index == "n1897_shaolong-5-n" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 96430, true);
				 api_quest_RewardQuestUser(userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 96430, true);
				 api_quest_RewardQuestUser(userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 96430, true);
				 api_quest_RewardQuestUser(userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 96430, true);
				 api_quest_RewardQuestUser(userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 96430, true);
				 api_quest_RewardQuestUser(userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 96430, true);
				 api_quest_RewardQuestUser(userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 96430, true);
				 api_quest_RewardQuestUser(userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 96430, true);
				 api_quest_RewardQuestUser(userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 96430, true);
				 api_quest_RewardQuestUser(userObjID, 96430, questID, 1);
			 end 
	end
	if npc_talk_index == "n1897_shaolong-5-o" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9730, 2);
					api_quest_SetQuestStep(userObjID, 9730, 1);
					api_quest_SetJournalStep(userObjID, 9730, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1897_shaolong-5-p" then 
	end
	if npc_talk_index == "n1897_shaolong-5-q" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1912_shaolong--------------------------------------------------------------------------------
function mqc08_9643_to_lead_nots_OnTalk_n1912_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1912_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1912_shaolong-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1912_shaolong-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1912_shaolong-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1916_sorceress_lantana--------------------------------------------------------------------------------
function mqc08_9643_to_lead_nots_OnTalk_n1916_sorceress_lantana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1916_sorceress_lantana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1916_sorceress_lantana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1916_sorceress_lantana-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1916_sorceress_lantana-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1916_sorceress_lantana-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1916_sorceress_lantana-2-b" then 
	end
	if npc_talk_index == "n1916_sorceress_lantana-2-c" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1916_sorceress_lantana-4-b" then 
	end
	if npc_talk_index == "n1916_sorceress_lantana-4-c" then 
	end
	if npc_talk_index == "n1916_sorceress_lantana-4-d" then 
	end
	if npc_talk_index == "n1916_sorceress_lantana-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc08_9643_to_lead_nots_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9643);
end

function mqc08_9643_to_lead_nots_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9643);
end

function mqc08_9643_to_lead_nots_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9643);
	local questID=9643;
end

function mqc08_9643_to_lead_nots_OnRemoteStart( userObjID, questID )
end

function mqc08_9643_to_lead_nots_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc08_9643_to_lead_nots_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9643, 2);
				api_quest_SetJournalStep(userObjID,9643, 1);
				api_quest_SetQuestStep(userObjID,9643, 1);
				npc_talk_index = "n1897_shaolong-1";
end

</VillageServer>

<GameServer>
function mqc08_9643_to_lead_nots_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1897 then
		mqc08_9643_to_lead_nots_OnTalk_n1897_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1912 then
		mqc08_9643_to_lead_nots_OnTalk_n1912_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1916 then
		mqc08_9643_to_lead_nots_OnTalk_n1916_sorceress_lantana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1897_shaolong--------------------------------------------------------------------------------
function mqc08_9643_to_lead_nots_OnTalk_n1897_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1897_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1897_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1897_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1897_shaolong-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1897_shaolong-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9643, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9643, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9643, 1);
				npc_talk_index = "n1897_shaolong-1";

	end
	if npc_talk_index == "n1897_shaolong-1-b" then 
	end
	if npc_talk_index == "n1897_shaolong-1-c" then 
	end
	if npc_talk_index == "n1897_shaolong-1-d" then 
	end
	if npc_talk_index == "n1897_shaolong-1-e" then 
	end
	if npc_talk_index == "n1897_shaolong-1-f" then 
	end
	if npc_talk_index == "n1897_shaolong-1-g" then 
	end
	if npc_talk_index == "n1897_shaolong-1-h" then 
	end
	if npc_talk_index == "n1897_shaolong-1-i" then 
	end
	if npc_talk_index == "n1897_shaolong-1-j" then 
	end
	if npc_talk_index == "n1897_shaolong-1-k" then 
	end
	if npc_talk_index == "n1897_shaolong-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1897_shaolong-5-levelchk" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 50 then
									npc_talk_index = "n1897_shaolong-5-b";

				else
									npc_talk_index = "n1897_shaolong-5-q";

				end
	end
	if npc_talk_index == "n1897_shaolong-5-c" then 
	end
	if npc_talk_index == "n1897_shaolong-5-d" then 
	end
	if npc_talk_index == "n1897_shaolong-5-e" then 
	end
	if npc_talk_index == "n1897_shaolong-5-f" then 
	end
	if npc_talk_index == "n1897_shaolong-5-g" then 
	end
	if npc_talk_index == "n1897_shaolong-5-h" then 
	end
	if npc_talk_index == "n1897_shaolong-5-i" then 
	end
	if npc_talk_index == "n1897_shaolong-5-j" then 
	end
	if npc_talk_index == "n1897_shaolong-5-k" then 
	end
	if npc_talk_index == "n1897_shaolong-5-l" then 
	end
	if npc_talk_index == "n1897_shaolong-5-m" then 
	end
	if npc_talk_index == "n1897_shaolong-5-n" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96430, questID, 1);
			 end 
	end
	if npc_talk_index == "n1897_shaolong-5-o" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9730, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9730, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9730, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1897_shaolong-5-p" then 
	end
	if npc_talk_index == "n1897_shaolong-5-q" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1912_shaolong--------------------------------------------------------------------------------
function mqc08_9643_to_lead_nots_OnTalk_n1912_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1912_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1912_shaolong-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1912_shaolong-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1912_shaolong-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1916_sorceress_lantana--------------------------------------------------------------------------------
function mqc08_9643_to_lead_nots_OnTalk_n1916_sorceress_lantana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1916_sorceress_lantana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1916_sorceress_lantana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1916_sorceress_lantana-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1916_sorceress_lantana-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1916_sorceress_lantana-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1916_sorceress_lantana-2-b" then 
	end
	if npc_talk_index == "n1916_sorceress_lantana-2-c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1916_sorceress_lantana-4-b" then 
	end
	if npc_talk_index == "n1916_sorceress_lantana-4-c" then 
	end
	if npc_talk_index == "n1916_sorceress_lantana-4-d" then 
	end
	if npc_talk_index == "n1916_sorceress_lantana-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc08_9643_to_lead_nots_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9643);
end

function mqc08_9643_to_lead_nots_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9643);
end

function mqc08_9643_to_lead_nots_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9643);
	local questID=9643;
end

function mqc08_9643_to_lead_nots_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc08_9643_to_lead_nots_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc08_9643_to_lead_nots_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9643, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9643, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9643, 1);
				npc_talk_index = "n1897_shaolong-1";
end

</GameServer>