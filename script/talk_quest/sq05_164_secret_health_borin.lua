<VillageServer>

function sq05_164_secret_health_borin_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 16 then
		sq05_164_secret_health_borin_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n016_trader_borin--------------------------------------------------------------------------------
function sq05_164_secret_health_borin_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n016_trader_borin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n016_trader_borin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-accepting-check_quest" then
				if api_quest_UserHasQuest(userObjID,62) > -1 then
									npc_talk_index = "n016_trader_borin-accepting-c";

				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 62 ) == 1  then
									npc_talk_index = "n016_trader_borin-accepting-c";

				else
									npc_talk_index = "n016_trader_borin-accepting-a";

				end

				end

	end
	if npc_talk_index == "n016_trader_borin-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1640, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1640, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1640, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1640, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1640, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1640, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1640, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1640, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1640, false);
			 end 

	end
	if npc_talk_index == "n016_trader_borin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,164, 1);
				api_quest_SetJournalStep(userObjID,164, 1);
				api_quest_SetQuestStep(userObjID,164, 1);
				npc_talk_index = "n016_trader_borin-1";

	end
	if npc_talk_index == "n016_trader_borin-1-b" then 
	end
	if npc_talk_index == "n016_trader_borin-1-c" then 
	end
	if npc_talk_index == "n016_trader_borin-1-g" then 
	end
	if npc_talk_index == "n016_trader_borin-1-f" then 
	end
	if npc_talk_index == "n016_trader_borin-1-e" then 
	end
	if npc_talk_index == "n016_trader_borin-1-f" then 
	end
	if npc_talk_index == "n016_trader_borin-1-g" then 
	end
	if npc_talk_index == "n016_trader_borin-1-c" then 
	end
	if npc_talk_index == "n016_trader_borin-1-h" then 
	end
	if npc_talk_index == "n016_trader_borin-1-i" then 
	end
	if npc_talk_index == "n016_trader_borin-1-j" then 
	end
	if npc_talk_index == "n016_trader_borin-1-k" then 
	end
	if npc_talk_index == "n016_trader_borin-1-l" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 306, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300076, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 311, 30000);
	end
	if npc_talk_index == "n016_trader_borin-3-a" then 
				api_quest_DelQuestItem(userObjID, 300076, 1);
	end
	if npc_talk_index == "n016_trader_borin-3-b" then 
	end
	if npc_talk_index == "n016_trader_borin-3-c" then 
	end
	if npc_talk_index == "n016_trader_borin-3-d" then 
	end
	if npc_talk_index == "n016_trader_borin-3-e" then 
	end
	if npc_talk_index == "n016_trader_borin-3-f" then 
	end
	if npc_talk_index == "n016_trader_borin-3-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1640, true);
				 api_quest_RewardQuestUser(userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1640, true);
				 api_quest_RewardQuestUser(userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1640, true);
				 api_quest_RewardQuestUser(userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1640, true);
				 api_quest_RewardQuestUser(userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1640, true);
				 api_quest_RewardQuestUser(userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1640, true);
				 api_quest_RewardQuestUser(userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1640, true);
				 api_quest_RewardQuestUser(userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1640, true);
				 api_quest_RewardQuestUser(userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1640, true);
				 api_quest_RewardQuestUser(userObjID, 1640, questID, 1);
			 end 
	end
	if npc_talk_index == "n016_trader_borin-3-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n016_trader_borin-3-i" then 
	end
	if npc_talk_index == "n016_trader_borin-3-j" then 
	end
	if npc_talk_index == "n016_trader_borin-3-k" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_164_secret_health_borin_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 164);
	if qstep == 2 and CountIndex == 306 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300076, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300076, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 300076 then

	end
	if qstep == 2 and CountIndex == 311 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300076, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300076, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function sq05_164_secret_health_borin_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 164);
	if qstep == 2 and CountIndex == 306 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300076 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 311 and Count >= TargetCount  then

	end
end

function sq05_164_secret_health_borin_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 164);
	local questID=164;
end

function sq05_164_secret_health_borin_OnRemoteStart( userObjID, questID )
end

function sq05_164_secret_health_borin_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq05_164_secret_health_borin_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,164, 1);
				api_quest_SetJournalStep(userObjID,164, 1);
				api_quest_SetQuestStep(userObjID,164, 1);
				npc_talk_index = "n016_trader_borin-1";
end

</VillageServer>

<GameServer>
function sq05_164_secret_health_borin_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 16 then
		sq05_164_secret_health_borin_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n016_trader_borin--------------------------------------------------------------------------------
function sq05_164_secret_health_borin_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n016_trader_borin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n016_trader_borin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-accepting-check_quest" then
				if api_quest_UserHasQuest( pRoom, userObjID,62) > -1 then
									npc_talk_index = "n016_trader_borin-accepting-c";

				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 62 ) == 1  then
									npc_talk_index = "n016_trader_borin-accepting-c";

				else
									npc_talk_index = "n016_trader_borin-accepting-a";

				end

				end

	end
	if npc_talk_index == "n016_trader_borin-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, false);
			 end 

	end
	if npc_talk_index == "n016_trader_borin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,164, 1);
				api_quest_SetJournalStep( pRoom, userObjID,164, 1);
				api_quest_SetQuestStep( pRoom, userObjID,164, 1);
				npc_talk_index = "n016_trader_borin-1";

	end
	if npc_talk_index == "n016_trader_borin-1-b" then 
	end
	if npc_talk_index == "n016_trader_borin-1-c" then 
	end
	if npc_talk_index == "n016_trader_borin-1-g" then 
	end
	if npc_talk_index == "n016_trader_borin-1-f" then 
	end
	if npc_talk_index == "n016_trader_borin-1-e" then 
	end
	if npc_talk_index == "n016_trader_borin-1-f" then 
	end
	if npc_talk_index == "n016_trader_borin-1-g" then 
	end
	if npc_talk_index == "n016_trader_borin-1-c" then 
	end
	if npc_talk_index == "n016_trader_borin-1-h" then 
	end
	if npc_talk_index == "n016_trader_borin-1-i" then 
	end
	if npc_talk_index == "n016_trader_borin-1-j" then 
	end
	if npc_talk_index == "n016_trader_borin-1-k" then 
	end
	if npc_talk_index == "n016_trader_borin-1-l" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 306, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300076, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 311, 30000);
	end
	if npc_talk_index == "n016_trader_borin-3-a" then 
				api_quest_DelQuestItem( pRoom, userObjID, 300076, 1);
	end
	if npc_talk_index == "n016_trader_borin-3-b" then 
	end
	if npc_talk_index == "n016_trader_borin-3-c" then 
	end
	if npc_talk_index == "n016_trader_borin-3-d" then 
	end
	if npc_talk_index == "n016_trader_borin-3-e" then 
	end
	if npc_talk_index == "n016_trader_borin-3-f" then 
	end
	if npc_talk_index == "n016_trader_borin-3-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1640, questID, 1);
			 end 
	end
	if npc_talk_index == "n016_trader_borin-3-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n016_trader_borin-3-i" then 
	end
	if npc_talk_index == "n016_trader_borin-3-j" then 
	end
	if npc_talk_index == "n016_trader_borin-3-k" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_164_secret_health_borin_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 164);
	if qstep == 2 and CountIndex == 306 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300076, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300076, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 300076 then

	end
	if qstep == 2 and CountIndex == 311 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300076, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300076, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function sq05_164_secret_health_borin_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 164);
	if qstep == 2 and CountIndex == 306 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300076 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 311 and Count >= TargetCount  then

	end
end

function sq05_164_secret_health_borin_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 164);
	local questID=164;
end

function sq05_164_secret_health_borin_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq05_164_secret_health_borin_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq05_164_secret_health_borin_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,164, 1);
				api_quest_SetJournalStep( pRoom, userObjID,164, 1);
				api_quest_SetQuestStep( pRoom, userObjID,164, 1);
				npc_talk_index = "n016_trader_borin-1";
end

</GameServer>