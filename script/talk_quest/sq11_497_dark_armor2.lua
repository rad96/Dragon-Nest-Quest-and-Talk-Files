<VillageServer>

function sq11_497_dark_armor2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 46 then
		sq11_497_dark_armor2_OnTalk_n046_cleric_master_enoc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 91 then
		sq11_497_dark_armor2_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n046_cleric_master_enoc--------------------------------------------------------------------------------
function sq11_497_dark_armor2_OnTalk_n046_cleric_master_enoc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n046_cleric_master_enoc-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n046_cleric_master_enoc-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n046_cleric_master_enoc-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n046_cleric_master_enoc-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n046_cleric_master_enoc-4-b" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-4-c" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300093, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300093, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n046_cleric_master_enoc-4-d" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n091_trader_bellin--------------------------------------------------------------------------------
function sq11_497_dark_armor2_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n091_trader_bellin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n091_trader_bellin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n091_trader_bellin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n091_trader_bellin-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4970, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4970, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4970, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4970, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4970, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4970, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4970, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4970, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4970, false);
			 end 

	end
	if npc_talk_index == "n091_trader_bellin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,497, 1);
				api_quest_SetJournalStep(userObjID,497, 1);
				api_quest_SetQuestStep(userObjID,497, 1);
				npc_talk_index = "n091_trader_bellin-1";

	end
	if npc_talk_index == "n091_trader_bellin-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300079, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 973, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200973, 30000);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n091_trader_bellin-1-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-a" then 
				api_quest_DelQuestItem(userObjID, 300081, 1);
	end
	if npc_talk_index == "n091_trader_bellin-3-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-c" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-d" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-e" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-f" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n091_trader_bellin-5-a" then 
				api_quest_DelQuestItem(userObjID, 300093, 1);
	end
	if npc_talk_index == "n091_trader_bellin-5-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-5-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4970, true);
				 api_quest_RewardQuestUser(userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4970, true);
				 api_quest_RewardQuestUser(userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4970, true);
				 api_quest_RewardQuestUser(userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4970, true);
				 api_quest_RewardQuestUser(userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4970, true);
				 api_quest_RewardQuestUser(userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4970, true);
				 api_quest_RewardQuestUser(userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4970, true);
				 api_quest_RewardQuestUser(userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4970, true);
				 api_quest_RewardQuestUser(userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4970, true);
				 api_quest_RewardQuestUser(userObjID, 4970, questID, 1);
			 end 
	end
	if npc_talk_index == "n091_trader_bellin-5-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 498, 1);
					api_quest_SetQuestStep(userObjID, 498, 1);
					api_quest_SetJournalStep(userObjID, 498, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n091_trader_bellin-5-e" then 
	end
	if npc_talk_index == "n091_trader_bellin-5-f" then 
	end
	if npc_talk_index == "n091_trader_bellin-5-g" then 
	end
	if npc_talk_index == "n091_trader_bellin-5-h" then 
	end
	if npc_talk_index == "n091_trader_bellin-5-i" then 
	end
	if npc_talk_index == "n091_trader_bellin-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n091_trader_bellin-1", "sq11_498_dark_armor3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_497_dark_armor2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 497);
	if qstep == 2 and CountIndex == 973 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300079, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300079, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300079 then

	end
	if qstep == 2 and CountIndex == 200973 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300079, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300079, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_497_dark_armor2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 497);
	if qstep == 2 and CountIndex == 973 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300079 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 200973 and Count >= TargetCount  then

	end
end

function sq11_497_dark_armor2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 497);
	local questID=497;
end

function sq11_497_dark_armor2_OnRemoteStart( userObjID, questID )
end

function sq11_497_dark_armor2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_497_dark_armor2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,497, 1);
				api_quest_SetJournalStep(userObjID,497, 1);
				api_quest_SetQuestStep(userObjID,497, 1);
				npc_talk_index = "n091_trader_bellin-1";
end

</VillageServer>

<GameServer>
function sq11_497_dark_armor2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 46 then
		sq11_497_dark_armor2_OnTalk_n046_cleric_master_enoc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 91 then
		sq11_497_dark_armor2_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n046_cleric_master_enoc--------------------------------------------------------------------------------
function sq11_497_dark_armor2_OnTalk_n046_cleric_master_enoc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n046_cleric_master_enoc-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n046_cleric_master_enoc-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n046_cleric_master_enoc-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n046_cleric_master_enoc-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n046_cleric_master_enoc-4-b" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-4-c" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300093, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300093, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n046_cleric_master_enoc-4-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n091_trader_bellin--------------------------------------------------------------------------------
function sq11_497_dark_armor2_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n091_trader_bellin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n091_trader_bellin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n091_trader_bellin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n091_trader_bellin-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, false);
			 end 

	end
	if npc_talk_index == "n091_trader_bellin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,497, 1);
				api_quest_SetJournalStep( pRoom, userObjID,497, 1);
				api_quest_SetQuestStep( pRoom, userObjID,497, 1);
				npc_talk_index = "n091_trader_bellin-1";

	end
	if npc_talk_index == "n091_trader_bellin-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300079, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 973, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200973, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n091_trader_bellin-1-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-a" then 
				api_quest_DelQuestItem( pRoom, userObjID, 300081, 1);
	end
	if npc_talk_index == "n091_trader_bellin-3-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-c" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-d" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-e" then 
	end
	if npc_talk_index == "n091_trader_bellin-3-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n091_trader_bellin-5-a" then 
				api_quest_DelQuestItem( pRoom, userObjID, 300093, 1);
	end
	if npc_talk_index == "n091_trader_bellin-5-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-5-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4970, questID, 1);
			 end 
	end
	if npc_talk_index == "n091_trader_bellin-5-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 498, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 498, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 498, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n091_trader_bellin-5-e" then 
	end
	if npc_talk_index == "n091_trader_bellin-5-f" then 
	end
	if npc_talk_index == "n091_trader_bellin-5-g" then 
	end
	if npc_talk_index == "n091_trader_bellin-5-h" then 
	end
	if npc_talk_index == "n091_trader_bellin-5-i" then 
	end
	if npc_talk_index == "n091_trader_bellin-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n091_trader_bellin-1", "sq11_498_dark_armor3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_497_dark_armor2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 497);
	if qstep == 2 and CountIndex == 973 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300079, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300079, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300079 then

	end
	if qstep == 2 and CountIndex == 200973 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300079, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300079, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_497_dark_armor2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 497);
	if qstep == 2 and CountIndex == 973 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300079 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 200973 and Count >= TargetCount  then

	end
end

function sq11_497_dark_armor2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 497);
	local questID=497;
end

function sq11_497_dark_armor2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_497_dark_armor2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_497_dark_armor2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,497, 1);
				api_quest_SetJournalStep( pRoom, userObjID,497, 1);
				api_quest_SetQuestStep( pRoom, userObjID,497, 1);
				npc_talk_index = "n091_trader_bellin-1";
end

</GameServer>