<VillageServer>

function mqc15_9438_second_desperation_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1498 then
		mqc15_9438_second_desperation_OnTalk_n1498_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1558 then
		mqc15_9438_second_desperation_OnTalk_n1558_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1566 then
		mqc15_9438_second_desperation_OnTalk_n1566_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1571 then
		mqc15_9438_second_desperation_OnTalk_n1571_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1572 then
		mqc15_9438_second_desperation_OnTalk_n1572_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1498_meriendel--------------------------------------------------------------------------------
function mqc15_9438_second_desperation_OnTalk_n1498_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1498_meriendel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1558_gaharam--------------------------------------------------------------------------------
function mqc15_9438_second_desperation_OnTalk_n1558_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1558_gaharam-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1558_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1558_gaharam-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9438, 2);
				api_quest_SetJournalStep(userObjID,9438, 1);
				api_quest_SetQuestStep(userObjID,9438, 1);
				npc_talk_index = "n1558_gaharam-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1566_pether--------------------------------------------------------------------------------
function mqc15_9438_second_desperation_OnTalk_n1566_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1566_pether-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1566_pether-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1566_pether-1-b" then 
	end
	if npc_talk_index == "n1566_pether-1-c" then 
	end
	if npc_talk_index == "n1566_pether-1-d" then 
	end
	if npc_talk_index == "n1566_pether-1-e" then 
	end
	if npc_talk_index == "n1566_pether-1-f" then 
	end
	if npc_talk_index == "n1566_pether-1-g" then 
	end
	if npc_talk_index == "n1566_pether-1-h" then 
	end
	if npc_talk_index == "n1566_pether-1-i" then 
	end
	if npc_talk_index == "n1566_pether-1-j" then 
	end
	if npc_talk_index == "n1566_pether-1-k" then 
	end
	if npc_talk_index == "n1566_pether-1-l" then 
	end
	if npc_talk_index == "n1566_pether-1-m" then 
	end
	if npc_talk_index == "n1566_pether-1-n" then 
	end
	if npc_talk_index == "n1566_pether-1-o" then 
	end
	if npc_talk_index == "n1566_pether-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1571_meriendel--------------------------------------------------------------------------------
function mqc15_9438_second_desperation_OnTalk_n1571_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1571_meriendel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1572_gaharam--------------------------------------------------------------------------------
function mqc15_9438_second_desperation_OnTalk_n1572_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1572_gaharam-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1572_gaharam-3-b" then 
	end
	if npc_talk_index == "n1572_gaharam-3-c" then 
	end
	if npc_talk_index == "n1572_gaharam-3-d" then 
	end
	if npc_talk_index == "n1572_gaharam-3-e" then 
	end
	if npc_talk_index == "n1572_gaharam-3-f" then 
	end
	if npc_talk_index == "n1572_gaharam-3-g" then 
	end
	if npc_talk_index == "n1572_gaharam-3-h" then 
	end
	if npc_talk_index == "n1572_gaharam-3-i" then 
	end
	if npc_talk_index == "n1572_gaharam-3-j" then 
	end
	if npc_talk_index == "n1572_gaharam-3-k" then 
	end
	if npc_talk_index == "n1572_gaharam-3-l" then 
	end
	if npc_talk_index == "n1572_gaharam-3-m" then 
	end
	if npc_talk_index == "n1572_gaharam-3-n" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94380, true);
				 api_quest_RewardQuestUser(userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94380, true);
				 api_quest_RewardQuestUser(userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94380, true);
				 api_quest_RewardQuestUser(userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94380, true);
				 api_quest_RewardQuestUser(userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94380, true);
				 api_quest_RewardQuestUser(userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94380, true);
				 api_quest_RewardQuestUser(userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94380, true);
				 api_quest_RewardQuestUser(userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94380, true);
				 api_quest_RewardQuestUser(userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94380, true);
				 api_quest_RewardQuestUser(userObjID, 94380, questID, 1);
			 end 
	end
	if npc_talk_index == "n1572_gaharam-3-o" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9439, 2);
					api_quest_SetQuestStep(userObjID, 9439, 1);
					api_quest_SetJournalStep(userObjID, 9439, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1572_gaharam-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1572_gaharam-1", "mqc15_9439_corrupt_people.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9438_second_desperation_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9438);
end

function mqc15_9438_second_desperation_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9438);
end

function mqc15_9438_second_desperation_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9438);
	local questID=9438;
end

function mqc15_9438_second_desperation_OnRemoteStart( userObjID, questID )
end

function mqc15_9438_second_desperation_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9438_second_desperation_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9438, 2);
				api_quest_SetJournalStep(userObjID,9438, 1);
				api_quest_SetQuestStep(userObjID,9438, 1);
				npc_talk_index = "n1558_gaharam-1";
end

</VillageServer>

<GameServer>
function mqc15_9438_second_desperation_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1498 then
		mqc15_9438_second_desperation_OnTalk_n1498_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1558 then
		mqc15_9438_second_desperation_OnTalk_n1558_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1566 then
		mqc15_9438_second_desperation_OnTalk_n1566_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1571 then
		mqc15_9438_second_desperation_OnTalk_n1571_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1572 then
		mqc15_9438_second_desperation_OnTalk_n1572_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1498_meriendel--------------------------------------------------------------------------------
function mqc15_9438_second_desperation_OnTalk_n1498_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1498_meriendel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1558_gaharam--------------------------------------------------------------------------------
function mqc15_9438_second_desperation_OnTalk_n1558_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1558_gaharam-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1558_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1558_gaharam-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9438, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9438, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9438, 1);
				npc_talk_index = "n1558_gaharam-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1566_pether--------------------------------------------------------------------------------
function mqc15_9438_second_desperation_OnTalk_n1566_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1566_pether-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1566_pether-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1566_pether-1-b" then 
	end
	if npc_talk_index == "n1566_pether-1-c" then 
	end
	if npc_talk_index == "n1566_pether-1-d" then 
	end
	if npc_talk_index == "n1566_pether-1-e" then 
	end
	if npc_talk_index == "n1566_pether-1-f" then 
	end
	if npc_talk_index == "n1566_pether-1-g" then 
	end
	if npc_talk_index == "n1566_pether-1-h" then 
	end
	if npc_talk_index == "n1566_pether-1-i" then 
	end
	if npc_talk_index == "n1566_pether-1-j" then 
	end
	if npc_talk_index == "n1566_pether-1-k" then 
	end
	if npc_talk_index == "n1566_pether-1-l" then 
	end
	if npc_talk_index == "n1566_pether-1-m" then 
	end
	if npc_talk_index == "n1566_pether-1-n" then 
	end
	if npc_talk_index == "n1566_pether-1-o" then 
	end
	if npc_talk_index == "n1566_pether-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1571_meriendel--------------------------------------------------------------------------------
function mqc15_9438_second_desperation_OnTalk_n1571_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1571_meriendel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1572_gaharam--------------------------------------------------------------------------------
function mqc15_9438_second_desperation_OnTalk_n1572_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1572_gaharam-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1572_gaharam-3-b" then 
	end
	if npc_talk_index == "n1572_gaharam-3-c" then 
	end
	if npc_talk_index == "n1572_gaharam-3-d" then 
	end
	if npc_talk_index == "n1572_gaharam-3-e" then 
	end
	if npc_talk_index == "n1572_gaharam-3-f" then 
	end
	if npc_talk_index == "n1572_gaharam-3-g" then 
	end
	if npc_talk_index == "n1572_gaharam-3-h" then 
	end
	if npc_talk_index == "n1572_gaharam-3-i" then 
	end
	if npc_talk_index == "n1572_gaharam-3-j" then 
	end
	if npc_talk_index == "n1572_gaharam-3-k" then 
	end
	if npc_talk_index == "n1572_gaharam-3-l" then 
	end
	if npc_talk_index == "n1572_gaharam-3-m" then 
	end
	if npc_talk_index == "n1572_gaharam-3-n" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94380, questID, 1);
			 end 
	end
	if npc_talk_index == "n1572_gaharam-3-o" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9439, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9439, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9439, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1572_gaharam-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1572_gaharam-1", "mqc15_9439_corrupt_people.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9438_second_desperation_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9438);
end

function mqc15_9438_second_desperation_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9438);
end

function mqc15_9438_second_desperation_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9438);
	local questID=9438;
end

function mqc15_9438_second_desperation_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9438_second_desperation_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9438_second_desperation_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9438, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9438, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9438, 1);
				npc_talk_index = "n1558_gaharam-1";
end

</GameServer>