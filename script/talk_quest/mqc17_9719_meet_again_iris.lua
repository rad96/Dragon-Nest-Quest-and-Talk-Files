<VillageServer>

function mqc17_9719_meet_again_iris_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1816 then
		mqc17_9719_meet_again_iris_OnTalk_n1816_vernicka(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1829 then
		mqc17_9719_meet_again_iris_OnTalk_n1829_tryan1_iris(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1830 then
		mqc17_9719_meet_again_iris_OnTalk_n1830_tryan2_iris(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1816_vernicka--------------------------------------------------------------------------------
function mqc17_9719_meet_again_iris_OnTalk_n1816_vernicka(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1816_vernicka-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1816_vernicka-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1816_vernicka-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1816_vernicka-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1816_vernicka-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9719, 2);
				api_quest_SetJournalStep(userObjID,9719, 1);
				api_quest_SetQuestStep(userObjID,9719, 1);
				npc_talk_index = "n1816_vernicka-1";

	end
	if npc_talk_index == "n1816_vernicka-1-b" then 
	end
	if npc_talk_index == "n1816_vernicka-1-c" then 
	end
	if npc_talk_index == "n1816_vernicka-1-d" then 
	end
	if npc_talk_index == "n1816_vernicka-1-d" then 
	end
	if npc_talk_index == "n1816_vernicka-1-e" then 
	end
	if npc_talk_index == "n1816_vernicka-1-f" then 
	end
	if npc_talk_index == "n1816_vernicka-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1816_vernicka-4-b" then 
	end
	if npc_talk_index == "n1816_vernicka-4-c" then 
	end
	if npc_talk_index == "n1816_vernicka-4-d" then 
	end
	if npc_talk_index == "n1816_vernicka-4-e" then 
	end
	if npc_talk_index == "n1816_vernicka-4-f" then 
	end
	if npc_talk_index == "n1816_vernicka-4-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97190, true);
				 api_quest_RewardQuestUser(userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97190, true);
				 api_quest_RewardQuestUser(userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97190, true);
				 api_quest_RewardQuestUser(userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97190, true);
				 api_quest_RewardQuestUser(userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97190, true);
				 api_quest_RewardQuestUser(userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97190, true);
				 api_quest_RewardQuestUser(userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97190, true);
				 api_quest_RewardQuestUser(userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97190, true);
				 api_quest_RewardQuestUser(userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97190, true);
				 api_quest_RewardQuestUser(userObjID, 97190, questID, 1);
			 end 
	end
	if npc_talk_index == "n1816_vernicka-4-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9720, 2);
					api_quest_SetQuestStep(userObjID, 9720, 1);
					api_quest_SetJournalStep(userObjID, 9720, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1816_vernicka-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1816_vernicka-1", "mqc17_9720_disappeared_grele.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1829_tryan1_iris--------------------------------------------------------------------------------
function mqc17_9719_meet_again_iris_OnTalk_n1829_tryan1_iris(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1829_tryan1_iris-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1829_tryan1_iris-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1829_tryan1_iris-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1829_tryan1_iris-2-b" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-c" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-d" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-e" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-f" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-g" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-h" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-i" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-j" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-k" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-l" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1830_tryan2_iris--------------------------------------------------------------------------------
function mqc17_9719_meet_again_iris_OnTalk_n1830_tryan2_iris(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1830_tryan2_iris-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1830_tryan2_iris-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1830_tryan2_iris-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1830_tryan2_iris-3-b" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-c" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-d" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-e" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-f" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-g" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-h" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-i" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-j" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-k" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-l" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc17_9719_meet_again_iris_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9719);
end

function mqc17_9719_meet_again_iris_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9719);
end

function mqc17_9719_meet_again_iris_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9719);
	local questID=9719;
end

function mqc17_9719_meet_again_iris_OnRemoteStart( userObjID, questID )
end

function mqc17_9719_meet_again_iris_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc17_9719_meet_again_iris_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9719, 2);
				api_quest_SetJournalStep(userObjID,9719, 1);
				api_quest_SetQuestStep(userObjID,9719, 1);
				npc_talk_index = "n1816_vernicka-1";
end

</VillageServer>

<GameServer>
function mqc17_9719_meet_again_iris_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1816 then
		mqc17_9719_meet_again_iris_OnTalk_n1816_vernicka( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1829 then
		mqc17_9719_meet_again_iris_OnTalk_n1829_tryan1_iris( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1830 then
		mqc17_9719_meet_again_iris_OnTalk_n1830_tryan2_iris( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1816_vernicka--------------------------------------------------------------------------------
function mqc17_9719_meet_again_iris_OnTalk_n1816_vernicka( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1816_vernicka-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1816_vernicka-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1816_vernicka-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1816_vernicka-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1816_vernicka-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9719, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9719, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9719, 1);
				npc_talk_index = "n1816_vernicka-1";

	end
	if npc_talk_index == "n1816_vernicka-1-b" then 
	end
	if npc_talk_index == "n1816_vernicka-1-c" then 
	end
	if npc_talk_index == "n1816_vernicka-1-d" then 
	end
	if npc_talk_index == "n1816_vernicka-1-d" then 
	end
	if npc_talk_index == "n1816_vernicka-1-e" then 
	end
	if npc_talk_index == "n1816_vernicka-1-f" then 
	end
	if npc_talk_index == "n1816_vernicka-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1816_vernicka-4-b" then 
	end
	if npc_talk_index == "n1816_vernicka-4-c" then 
	end
	if npc_talk_index == "n1816_vernicka-4-d" then 
	end
	if npc_talk_index == "n1816_vernicka-4-e" then 
	end
	if npc_talk_index == "n1816_vernicka-4-f" then 
	end
	if npc_talk_index == "n1816_vernicka-4-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97190, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97190, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97190, questID, 1);
			 end 
	end
	if npc_talk_index == "n1816_vernicka-4-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9720, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9720, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9720, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1816_vernicka-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1816_vernicka-1", "mqc17_9720_disappeared_grele.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1829_tryan1_iris--------------------------------------------------------------------------------
function mqc17_9719_meet_again_iris_OnTalk_n1829_tryan1_iris( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1829_tryan1_iris-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1829_tryan1_iris-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1829_tryan1_iris-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1829_tryan1_iris-2-b" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-c" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-d" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-e" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-f" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-g" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-h" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-i" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-j" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-k" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-2-l" then 
	end
	if npc_talk_index == "n1829_tryan1_iris-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1830_tryan2_iris--------------------------------------------------------------------------------
function mqc17_9719_meet_again_iris_OnTalk_n1830_tryan2_iris( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1830_tryan2_iris-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1830_tryan2_iris-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1830_tryan2_iris-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1830_tryan2_iris-3-b" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-c" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-d" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-e" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-f" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-g" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-h" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-i" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-j" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-k" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-l" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc17_9719_meet_again_iris_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9719);
end

function mqc17_9719_meet_again_iris_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9719);
end

function mqc17_9719_meet_again_iris_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9719);
	local questID=9719;
end

function mqc17_9719_meet_again_iris_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc17_9719_meet_again_iris_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc17_9719_meet_again_iris_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9719, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9719, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9719, 1);
				npc_talk_index = "n1816_vernicka-1";
end

</GameServer>