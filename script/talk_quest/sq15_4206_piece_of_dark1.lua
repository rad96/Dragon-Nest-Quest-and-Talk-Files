<VillageServer>

function sq15_4206_piece_of_dark1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 776 then
		sq15_4206_piece_of_dark1_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n776_darklair_mardlen--------------------------------------------------------------------------------
function sq15_4206_piece_of_dark1_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n776_darklair_mardlen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 42060, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 42060, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 42060, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 42060, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 42060, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 42060, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 42060, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 42060, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 42060, false);
			 end 

	end
	if npc_talk_index == "n776_darklair_mardlen-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4206, 1);
				api_quest_SetJournalStep(userObjID,4206, 1);
				api_quest_SetQuestStep(userObjID,4206, 1);
				npc_talk_index = "n776_darklair_mardlen-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 14005, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 1, 5, 14024, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 2, 5, 14040, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 3, 5, 14046, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 4, 5, 14199, 1 );

	end
	if npc_talk_index == "n776_darklair_mardlen-2-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-f" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-g" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-h" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-i" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-j" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-k" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-l" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 42060, true);
				 api_quest_RewardQuestUser(userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 42060, true);
				 api_quest_RewardQuestUser(userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 42060, true);
				 api_quest_RewardQuestUser(userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 42060, true);
				 api_quest_RewardQuestUser(userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 42060, true);
				 api_quest_RewardQuestUser(userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 42060, true);
				 api_quest_RewardQuestUser(userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 42060, true);
				 api_quest_RewardQuestUser(userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 42060, true);
				 api_quest_RewardQuestUser(userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 42060, true);
				 api_quest_RewardQuestUser(userObjID, 42060, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-m" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4206_piece_of_dark1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4206);
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
		 end 
	 end 
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
		 end 
	 end 
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
		 end 
	 end 
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
		 end 
	 end 
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
		 end 
	 end 
end

function sq15_4206_piece_of_dark1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4206);
end

function sq15_4206_piece_of_dark1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4206);
	local questID=4206;
end

function sq15_4206_piece_of_dark1_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 14005, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 1, 5, 14024, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 2, 5, 14040, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 3, 5, 14046, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 4, 5, 14199, 1 );
end

function sq15_4206_piece_of_dark1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4206_piece_of_dark1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4206, 1);
				api_quest_SetJournalStep(userObjID,4206, 1);
				api_quest_SetQuestStep(userObjID,4206, 1);
				npc_talk_index = "n776_darklair_mardlen-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 14005, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 1, 5, 14024, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 2, 5, 14040, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 3, 5, 14046, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 4, 5, 14199, 1 );
end

</VillageServer>

<GameServer>
function sq15_4206_piece_of_dark1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 776 then
		sq15_4206_piece_of_dark1_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n776_darklair_mardlen--------------------------------------------------------------------------------
function sq15_4206_piece_of_dark1_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n776_darklair_mardlen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, false);
			 end 

	end
	if npc_talk_index == "n776_darklair_mardlen-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4206, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4206, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4206, 1);
				npc_talk_index = "n776_darklair_mardlen-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 14005, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 5, 14024, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 5, 14040, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 5, 14046, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 5, 14199, 1 );

	end
	if npc_talk_index == "n776_darklair_mardlen-2-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-f" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-g" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-h" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-i" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-j" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-k" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-l" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42060, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-2-m" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4206_piece_of_dark1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4206);
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
		 end 
	 end 
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
		 end 
	 end 
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
		 end 
	 end 
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
		 end 
	 end 
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
		 end 
	 end 
end

function sq15_4206_piece_of_dark1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4206);
end

function sq15_4206_piece_of_dark1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4206);
	local questID=4206;
end

function sq15_4206_piece_of_dark1_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 14005, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 5, 14024, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 5, 14040, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 5, 14046, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 5, 14199, 1 );
end

function sq15_4206_piece_of_dark1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4206_piece_of_dark1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4206, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4206, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4206, 1);
				npc_talk_index = "n776_darklair_mardlen-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 14005, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 5, 14024, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 5, 14040, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 5, 14046, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 5, 14199, 1 );
end

</GameServer>