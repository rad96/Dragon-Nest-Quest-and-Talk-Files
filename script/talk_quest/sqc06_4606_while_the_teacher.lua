<VillageServer>

function sqc06_4606_while_the_teacher_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1691 then
		sqc06_4606_while_the_teacher_OnTalk_n1691_eltia_lotus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1694 then
		sqc06_4606_while_the_teacher_OnTalk_n1694_teramai_library(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1704 then
		sqc06_4606_while_the_teacher_OnTalk_n1704_teramai_west(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1691_eltia_lotus--------------------------------------------------------------------------------
function sqc06_4606_while_the_teacher_OnTalk_n1691_eltia_lotus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1691_eltia_lotus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1691_eltia_lotus-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1691_eltia_lotus-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1691_eltia_lotus-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46060, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46060, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46060, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46060, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46060, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46060, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46060, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46060, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46060, false);
			 end 

	end
	if npc_talk_index == "n1691_eltia_lotus-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4606, 1);
				api_quest_SetJournalStep(userObjID,4606, 1);
				api_quest_SetQuestStep(userObjID,4606, 1);
				npc_talk_index = "n1691_eltia_lotus-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1694_teramai_library--------------------------------------------------------------------------------
function sqc06_4606_while_the_teacher_OnTalk_n1694_teramai_library(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1694_teramai_library-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1694_teramai_library-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1694_teramai_library-1-b" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1704_teramai_west--------------------------------------------------------------------------------
function sqc06_4606_while_the_teacher_OnTalk_n1704_teramai_west(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1704_teramai_west-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1704_teramai_west-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1704_teramai_west-2-b" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-c" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-c" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-c" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-d" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-e" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-f" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-g" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-h" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46060, true);
				 api_quest_RewardQuestUser(userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46060, true);
				 api_quest_RewardQuestUser(userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46060, true);
				 api_quest_RewardQuestUser(userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46060, true);
				 api_quest_RewardQuestUser(userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46060, true);
				 api_quest_RewardQuestUser(userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46060, true);
				 api_quest_RewardQuestUser(userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46060, true);
				 api_quest_RewardQuestUser(userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46060, true);
				 api_quest_RewardQuestUser(userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46060, true);
				 api_quest_RewardQuestUser(userObjID, 46060, questID, 1);
			 end 
	end
	if npc_talk_index == "n1704_teramai_west-2-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4607, 1);
					api_quest_SetQuestStep(userObjID, 4607, 1);
					api_quest_SetJournalStep(userObjID, 4607, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1704_teramai_west-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1704_teramai_west-1", "sqc06_4607_permanent_training.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc06_4606_while_the_teacher_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4606);
end

function sqc06_4606_while_the_teacher_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4606);
end

function sqc06_4606_while_the_teacher_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4606);
	local questID=4606;
end

function sqc06_4606_while_the_teacher_OnRemoteStart( userObjID, questID )
end

function sqc06_4606_while_the_teacher_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc06_4606_while_the_teacher_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4606, 1);
				api_quest_SetJournalStep(userObjID,4606, 1);
				api_quest_SetQuestStep(userObjID,4606, 1);
				npc_talk_index = "n1691_eltia_lotus-1";
end

</VillageServer>

<GameServer>
function sqc06_4606_while_the_teacher_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1691 then
		sqc06_4606_while_the_teacher_OnTalk_n1691_eltia_lotus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1694 then
		sqc06_4606_while_the_teacher_OnTalk_n1694_teramai_library( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1704 then
		sqc06_4606_while_the_teacher_OnTalk_n1704_teramai_west( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1691_eltia_lotus--------------------------------------------------------------------------------
function sqc06_4606_while_the_teacher_OnTalk_n1691_eltia_lotus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1691_eltia_lotus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1691_eltia_lotus-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1691_eltia_lotus-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1691_eltia_lotus-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, false);
			 end 

	end
	if npc_talk_index == "n1691_eltia_lotus-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4606, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4606, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4606, 1);
				npc_talk_index = "n1691_eltia_lotus-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1694_teramai_library--------------------------------------------------------------------------------
function sqc06_4606_while_the_teacher_OnTalk_n1694_teramai_library( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1694_teramai_library-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1694_teramai_library-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1694_teramai_library-1-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1704_teramai_west--------------------------------------------------------------------------------
function sqc06_4606_while_the_teacher_OnTalk_n1704_teramai_west( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1704_teramai_west-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1704_teramai_west-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1704_teramai_west-2-b" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-c" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-c" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-c" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-d" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-e" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-f" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-g" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-h" then 
	end
	if npc_talk_index == "n1704_teramai_west-2-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46060, questID, 1);
			 end 
	end
	if npc_talk_index == "n1704_teramai_west-2-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4607, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4607, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4607, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1704_teramai_west-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1704_teramai_west-1", "sqc06_4607_permanent_training.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc06_4606_while_the_teacher_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4606);
end

function sqc06_4606_while_the_teacher_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4606);
end

function sqc06_4606_while_the_teacher_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4606);
	local questID=4606;
end

function sqc06_4606_while_the_teacher_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc06_4606_while_the_teacher_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc06_4606_while_the_teacher_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4606, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4606, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4606, 1);
				npc_talk_index = "n1691_eltia_lotus-1";
end

</GameServer>