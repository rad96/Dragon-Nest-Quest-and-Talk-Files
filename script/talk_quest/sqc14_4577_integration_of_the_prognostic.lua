<VillageServer>

function sqc14_4577_integration_of_the_prognostic_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1609 then
		sqc14_4577_integration_of_the_prognostic_OnTalk_n1609_sorceress_pochetel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1621 then
		sqc14_4577_integration_of_the_prognostic_OnTalk_n1621_sorceress_beriel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1623 then
		sqc14_4577_integration_of_the_prognostic_OnTalk_n1623_sorceress_yutz(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1610 then
		sqc14_4577_integration_of_the_prognostic_OnTalk_n1610_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1620 then
		sqc14_4577_integration_of_the_prognostic_OnTalk_n1620_side_kalaenl(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1609_sorceress_pochetel--------------------------------------------------------------------------------
function sqc14_4577_integration_of_the_prognostic_OnTalk_n1609_sorceress_pochetel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1609_sorceress_pochetel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1609_sorceress_pochetel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1609_sorceress_pochetel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1609_sorceress_pochetel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1609_sorceress_pochetel-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45770, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45770, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45770, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45770, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45770, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45770, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45770, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45770, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45770, false);
			 end 

	end
	if npc_talk_index == "n1609_sorceress_pochetel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4577, 1);
				api_quest_SetJournalStep(userObjID,4577, 1);
				api_quest_SetQuestStep(userObjID,4577, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 602045, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 702045, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400441, 3);
				npc_talk_index = "n1609_sorceress_pochetel-1";

	end
	if npc_talk_index == "n1609_sorceress_pochetel-2-a" then 

				if api_quest_HasQuestItem(userObjID, 400441, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400441, api_quest_HasQuestItem(userObjID, 400441, 1));
				end
	end
	if npc_talk_index == "n1609_sorceress_pochetel-2-b" then 
	end
	if npc_talk_index == "n1609_sorceress_pochetel-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1621_sorceress_beriel--------------------------------------------------------------------------------
function sqc14_4577_integration_of_the_prognostic_OnTalk_n1621_sorceress_beriel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1621_sorceress_beriel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1621_sorceress_beriel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1621_sorceress_beriel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1621_sorceress_beriel-3-a" then 
				api_quest_DelQuestItem(userObjID, 400441, 1);
	end
	if npc_talk_index == "n1621_sorceress_beriel-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1623_sorceress_yutz--------------------------------------------------------------------------------
function sqc14_4577_integration_of_the_prognostic_OnTalk_n1623_sorceress_yutz(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1623_sorceress_yutz-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1623_sorceress_yutz-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1623_sorceress_yutz-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1623_sorceress_yutz-4-a" then 

				if api_quest_HasQuestItem(userObjID, 400441, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400441, api_quest_HasQuestItem(userObjID, 400441, 1));
				end
	end
	if npc_talk_index == "n1623_sorceress_yutz-4-b" then 
	end
	if npc_talk_index == "n1623_sorceress_yutz-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1610_royal_magician_kalaen--------------------------------------------------------------------------------
function sqc14_4577_integration_of_the_prognostic_OnTalk_n1610_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1610_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1610_royal_magician_kalaen-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1610_royal_magician_kalaen-5-b" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-5-c" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-5-d" then 

				if api_quest_HasQuestItem(userObjID, 400441, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400441, api_quest_HasQuestItem(userObjID, 400441, 1));
				end
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45770, true);
				 api_quest_RewardQuestUser(userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45770, true);
				 api_quest_RewardQuestUser(userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45770, true);
				 api_quest_RewardQuestUser(userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45770, true);
				 api_quest_RewardQuestUser(userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45770, true);
				 api_quest_RewardQuestUser(userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45770, true);
				 api_quest_RewardQuestUser(userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45770, true);
				 api_quest_RewardQuestUser(userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45770, true);
				 api_quest_RewardQuestUser(userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45770, true);
				 api_quest_RewardQuestUser(userObjID, 45770, questID, 1);
			 end 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-5-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-5-f" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1620_side_kalaenl--------------------------------------------------------------------------------
function sqc14_4577_integration_of_the_prognostic_OnTalk_n1620_side_kalaenl(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1620_side_kalaenl-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4577_integration_of_the_prognostic_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4577);
	if qstep == 1 and CountIndex == 400441 then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 602045 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400441, 3) == 1 then 
					api_quest_AddQuestItem(userObjID, 400441, 3, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 702045 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400441, 3) == 1 then 
					api_quest_AddQuestItem(userObjID, 400441, 3, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sqc14_4577_integration_of_the_prognostic_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4577);
	if qstep == 1 and CountIndex == 400441 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 602045 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 702045 and Count >= TargetCount  then

	end
end

function sqc14_4577_integration_of_the_prognostic_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4577);
	local questID=4577;
end

function sqc14_4577_integration_of_the_prognostic_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 602045, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 702045, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400441, 3);
end

function sqc14_4577_integration_of_the_prognostic_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc14_4577_integration_of_the_prognostic_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4577, 1);
				api_quest_SetJournalStep(userObjID,4577, 1);
				api_quest_SetQuestStep(userObjID,4577, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 602045, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 702045, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400441, 3);
				npc_talk_index = "n1609_sorceress_pochetel-1";
end

</VillageServer>

<GameServer>
function sqc14_4577_integration_of_the_prognostic_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1609 then
		sqc14_4577_integration_of_the_prognostic_OnTalk_n1609_sorceress_pochetel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1621 then
		sqc14_4577_integration_of_the_prognostic_OnTalk_n1621_sorceress_beriel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1623 then
		sqc14_4577_integration_of_the_prognostic_OnTalk_n1623_sorceress_yutz( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1610 then
		sqc14_4577_integration_of_the_prognostic_OnTalk_n1610_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1620 then
		sqc14_4577_integration_of_the_prognostic_OnTalk_n1620_side_kalaenl( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1609_sorceress_pochetel--------------------------------------------------------------------------------
function sqc14_4577_integration_of_the_prognostic_OnTalk_n1609_sorceress_pochetel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1609_sorceress_pochetel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1609_sorceress_pochetel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1609_sorceress_pochetel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1609_sorceress_pochetel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1609_sorceress_pochetel-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, false);
			 end 

	end
	if npc_talk_index == "n1609_sorceress_pochetel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4577, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4577, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4577, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 602045, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 702045, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400441, 3);
				npc_talk_index = "n1609_sorceress_pochetel-1";

	end
	if npc_talk_index == "n1609_sorceress_pochetel-2-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400441, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400441, api_quest_HasQuestItem( pRoom, userObjID, 400441, 1));
				end
	end
	if npc_talk_index == "n1609_sorceress_pochetel-2-b" then 
	end
	if npc_talk_index == "n1609_sorceress_pochetel-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1621_sorceress_beriel--------------------------------------------------------------------------------
function sqc14_4577_integration_of_the_prognostic_OnTalk_n1621_sorceress_beriel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1621_sorceress_beriel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1621_sorceress_beriel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1621_sorceress_beriel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1621_sorceress_beriel-3-a" then 
				api_quest_DelQuestItem( pRoom, userObjID, 400441, 1);
	end
	if npc_talk_index == "n1621_sorceress_beriel-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1623_sorceress_yutz--------------------------------------------------------------------------------
function sqc14_4577_integration_of_the_prognostic_OnTalk_n1623_sorceress_yutz( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1623_sorceress_yutz-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1623_sorceress_yutz-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1623_sorceress_yutz-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1623_sorceress_yutz-4-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400441, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400441, api_quest_HasQuestItem( pRoom, userObjID, 400441, 1));
				end
	end
	if npc_talk_index == "n1623_sorceress_yutz-4-b" then 
	end
	if npc_talk_index == "n1623_sorceress_yutz-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1610_royal_magician_kalaen--------------------------------------------------------------------------------
function sqc14_4577_integration_of_the_prognostic_OnTalk_n1610_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1610_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1610_royal_magician_kalaen-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1610_royal_magician_kalaen-5-b" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-5-c" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-5-d" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400441, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400441, api_quest_HasQuestItem( pRoom, userObjID, 400441, 1));
				end
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45770, questID, 1);
			 end 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-5-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-5-f" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1620_side_kalaenl--------------------------------------------------------------------------------
function sqc14_4577_integration_of_the_prognostic_OnTalk_n1620_side_kalaenl( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1620_side_kalaenl-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4577_integration_of_the_prognostic_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4577);
	if qstep == 1 and CountIndex == 400441 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 602045 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400441, 3) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400441, 3, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 702045 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400441, 3) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400441, 3, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sqc14_4577_integration_of_the_prognostic_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4577);
	if qstep == 1 and CountIndex == 400441 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 602045 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 702045 and Count >= TargetCount  then

	end
end

function sqc14_4577_integration_of_the_prognostic_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4577);
	local questID=4577;
end

function sqc14_4577_integration_of_the_prognostic_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 602045, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 702045, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400441, 3);
end

function sqc14_4577_integration_of_the_prognostic_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc14_4577_integration_of_the_prognostic_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4577, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4577, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4577, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 602045, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 702045, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400441, 3);
				npc_talk_index = "n1609_sorceress_pochetel-1";
end

</GameServer>