<VillageServer>

function sq11_507_ancient_name_typhoon3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 88 then
		sq11_507_ancient_name_typhoon3_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_507_ancient_name_typhoon3_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-l" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5070, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5070, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5070, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5070, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5070, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5070, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5070, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5070, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5070, false);
			 end 

	end
	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest(userObjID,507, 1);
				api_quest_SetJournalStep(userObjID,507, 1);
				api_quest_SetQuestStep(userObjID,507, 1);
				npc_talk_index = "n088_scholar_starshy-1";

	end
	if npc_talk_index == "n088_scholar_starshy-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1168, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201168, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400152, 1);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-k" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-l" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-m" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-n" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-o" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-p" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-q" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5070, true);
				 api_quest_RewardQuestUser(userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5070, true);
				 api_quest_RewardQuestUser(userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5070, true);
				 api_quest_RewardQuestUser(userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5070, true);
				 api_quest_RewardQuestUser(userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5070, true);
				 api_quest_RewardQuestUser(userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5070, true);
				 api_quest_RewardQuestUser(userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5070, true);
				 api_quest_RewardQuestUser(userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5070, true);
				 api_quest_RewardQuestUser(userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5070, true);
				 api_quest_RewardQuestUser(userObjID, 5070, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-3-r" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n088_scholar_starshy-3-s" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_507_ancient_name_typhoon3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 507);
	if qstep == 2 and CountIndex == 1168 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400152, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400152, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400152 then

	end
	if qstep == 2 and CountIndex == 201168 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400152, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400152, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_507_ancient_name_typhoon3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 507);
	if qstep == 2 and CountIndex == 1168 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400152 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 201168 and Count >= TargetCount  then

	end
end

function sq11_507_ancient_name_typhoon3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 507);
	local questID=507;
end

function sq11_507_ancient_name_typhoon3_OnRemoteStart( userObjID, questID )
end

function sq11_507_ancient_name_typhoon3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_507_ancient_name_typhoon3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,507, 1);
				api_quest_SetJournalStep(userObjID,507, 1);
				api_quest_SetQuestStep(userObjID,507, 1);
				npc_talk_index = "n088_scholar_starshy-1";
end

</VillageServer>

<GameServer>
function sq11_507_ancient_name_typhoon3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 88 then
		sq11_507_ancient_name_typhoon3_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_507_ancient_name_typhoon3_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-l" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, false);
			 end 

	end
	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,507, 1);
				api_quest_SetJournalStep( pRoom, userObjID,507, 1);
				api_quest_SetQuestStep( pRoom, userObjID,507, 1);
				npc_talk_index = "n088_scholar_starshy-1";

	end
	if npc_talk_index == "n088_scholar_starshy-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1168, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201168, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400152, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-k" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-l" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-m" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-n" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-o" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-p" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-q" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5070, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-3-r" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n088_scholar_starshy-3-s" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_507_ancient_name_typhoon3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 507);
	if qstep == 2 and CountIndex == 1168 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400152, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400152, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400152 then

	end
	if qstep == 2 and CountIndex == 201168 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400152, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400152, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_507_ancient_name_typhoon3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 507);
	if qstep == 2 and CountIndex == 1168 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400152 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 201168 and Count >= TargetCount  then

	end
end

function sq11_507_ancient_name_typhoon3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 507);
	local questID=507;
end

function sq11_507_ancient_name_typhoon3_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_507_ancient_name_typhoon3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_507_ancient_name_typhoon3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,507, 1);
				api_quest_SetJournalStep( pRoom, userObjID,507, 1);
				api_quest_SetQuestStep( pRoom, userObjID,507, 1);
				npc_talk_index = "n088_scholar_starshy-1";
end

</GameServer>