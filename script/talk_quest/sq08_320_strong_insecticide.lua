<VillageServer>

function sq08_320_strong_insecticide_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 26 then
		sq08_320_strong_insecticide_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_320_strong_insecticide_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3201, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3202, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3203, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3204, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3205, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3206, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3207, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3208, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3209, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest(userObjID,320, 1);
				api_quest_SetJournalStep(userObjID,320, 1);
				api_quest_SetQuestStep(userObjID,320, 1);
				npc_talk_index = "n026_trader_may-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 659, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200659, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400125, 10);

	end
	if npc_talk_index == "n026_trader_may-2-b" then 
	end
	if npc_talk_index == "n026_trader_may-2-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3201, true);
				 api_quest_RewardQuestUser(userObjID, 3201, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3202, true);
				 api_quest_RewardQuestUser(userObjID, 3202, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3203, true);
				 api_quest_RewardQuestUser(userObjID, 3203, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3204, true);
				 api_quest_RewardQuestUser(userObjID, 3204, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3205, true);
				 api_quest_RewardQuestUser(userObjID, 3205, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3206, true);
				 api_quest_RewardQuestUser(userObjID, 3206, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3207, true);
				 api_quest_RewardQuestUser(userObjID, 3207, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3208, true);
				 api_quest_RewardQuestUser(userObjID, 3208, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3209, true);
				 api_quest_RewardQuestUser(userObjID, 3209, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-2-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400125, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400125, api_quest_HasQuestItem(userObjID, 400125, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_320_strong_insecticide_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 320);
	if qstep == 1 and CountIndex == 659 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400125, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400125, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400125 then

	end
	if qstep == 1 and CountIndex == 200659 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400125, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400125, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_320_strong_insecticide_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 320);
	if qstep == 1 and CountIndex == 659 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400125 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 200659 and Count >= TargetCount  then

	end
end

function sq08_320_strong_insecticide_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 320);
	local questID=320;
end

function sq08_320_strong_insecticide_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 659, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200659, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400125, 10);
end

function sq08_320_strong_insecticide_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_320_strong_insecticide_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,320, 1);
				api_quest_SetJournalStep(userObjID,320, 1);
				api_quest_SetQuestStep(userObjID,320, 1);
				npc_talk_index = "n026_trader_may-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 659, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200659, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400125, 10);
end

</VillageServer>

<GameServer>
function sq08_320_strong_insecticide_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 26 then
		sq08_320_strong_insecticide_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_320_strong_insecticide_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3201, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3202, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3203, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3204, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3205, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3206, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3207, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3208, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3209, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,320, 1);
				api_quest_SetJournalStep( pRoom, userObjID,320, 1);
				api_quest_SetQuestStep( pRoom, userObjID,320, 1);
				npc_talk_index = "n026_trader_may-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 659, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200659, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400125, 10);

	end
	if npc_talk_index == "n026_trader_may-2-b" then 
	end
	if npc_talk_index == "n026_trader_may-2-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3201, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3201, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3202, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3202, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3203, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3203, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3204, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3204, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3205, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3205, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3206, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3206, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3207, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3207, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3208, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3208, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3209, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3209, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-2-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400125, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400125, api_quest_HasQuestItem( pRoom, userObjID, 400125, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_320_strong_insecticide_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 320);
	if qstep == 1 and CountIndex == 659 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400125, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400125, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400125 then

	end
	if qstep == 1 and CountIndex == 200659 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400125, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400125, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_320_strong_insecticide_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 320);
	if qstep == 1 and CountIndex == 659 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400125 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 200659 and Count >= TargetCount  then

	end
end

function sq08_320_strong_insecticide_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 320);
	local questID=320;
end

function sq08_320_strong_insecticide_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 659, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200659, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400125, 10);
end

function sq08_320_strong_insecticide_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_320_strong_insecticide_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,320, 1);
				api_quest_SetJournalStep( pRoom, userObjID,320, 1);
				api_quest_SetQuestStep( pRoom, userObjID,320, 1);
				npc_talk_index = "n026_trader_may-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 659, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200659, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400125, 10);
end

</GameServer>