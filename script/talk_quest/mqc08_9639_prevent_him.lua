<VillageServer>

function mqc08_9639_prevent_him_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 707 then
		mqc08_9639_prevent_him_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 800 then
		mqc08_9639_prevent_him_OnTalk_n800_native_taruun(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mqc08_9639_prevent_him_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9639, 2);
				api_quest_SetJournalStep(userObjID,9639, 1);
				api_quest_SetQuestStep(userObjID,9639, 1);
				npc_talk_index = "n707_sidel-1";

	end
	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
	end
	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 96390, true);
				 api_quest_RewardQuestUser(userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 96390, true);
				 api_quest_RewardQuestUser(userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 96390, true);
				 api_quest_RewardQuestUser(userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 96390, true);
				 api_quest_RewardQuestUser(userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 96390, true);
				 api_quest_RewardQuestUser(userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 96390, true);
				 api_quest_RewardQuestUser(userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 96390, true);
				 api_quest_RewardQuestUser(userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 96390, true);
				 api_quest_RewardQuestUser(userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 96390, true);
				 api_quest_RewardQuestUser(userObjID, 96390, questID, 1);
			 end 
	end
	if npc_talk_index == "n707_sidel-2-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9640, 2);
					api_quest_SetQuestStep(userObjID, 9640, 1);
					api_quest_SetJournalStep(userObjID, 9640, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n707_sidel-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n707_sidel-1", "mqc08_9640_ancient_artifacts.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n800_native_taruun--------------------------------------------------------------------------------
function mqc08_9639_prevent_him_OnTalk_n800_native_taruun(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n800_native_taruun-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n800_native_taruun-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n800_native_taruun-1-b" then 
	end
	if npc_talk_index == "n800_native_taruun-1-c" then 
	end
	if npc_talk_index == "n800_native_taruun-1-d" then 
	end
	if npc_talk_index == "n800_native_taruun-1-e" then 
	end
	if npc_talk_index == "n800_native_taruun-1-f" then 
	end
	if npc_talk_index == "n800_native_taruun-1-g" then 
	end
	if npc_talk_index == "n800_native_taruun-1-h" then 
	end
	if npc_talk_index == "n800_native_taruun-1-i" then 
	end
	if npc_talk_index == "n800_native_taruun-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc08_9639_prevent_him_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9639);
end

function mqc08_9639_prevent_him_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9639);
end

function mqc08_9639_prevent_him_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9639);
	local questID=9639;
end

function mqc08_9639_prevent_him_OnRemoteStart( userObjID, questID )
end

function mqc08_9639_prevent_him_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc08_9639_prevent_him_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9639, 2);
				api_quest_SetJournalStep(userObjID,9639, 1);
				api_quest_SetQuestStep(userObjID,9639, 1);
				npc_talk_index = "n707_sidel-1";
end

</VillageServer>

<GameServer>
function mqc08_9639_prevent_him_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 707 then
		mqc08_9639_prevent_him_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 800 then
		mqc08_9639_prevent_him_OnTalk_n800_native_taruun( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mqc08_9639_prevent_him_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9639, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9639, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9639, 1);
				npc_talk_index = "n707_sidel-1";

	end
	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
	end
	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96390, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96390, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96390, questID, 1);
			 end 
	end
	if npc_talk_index == "n707_sidel-2-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9640, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9640, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9640, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n707_sidel-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n707_sidel-1", "mqc08_9640_ancient_artifacts.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n800_native_taruun--------------------------------------------------------------------------------
function mqc08_9639_prevent_him_OnTalk_n800_native_taruun( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n800_native_taruun-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n800_native_taruun-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n800_native_taruun-1-b" then 
	end
	if npc_talk_index == "n800_native_taruun-1-c" then 
	end
	if npc_talk_index == "n800_native_taruun-1-d" then 
	end
	if npc_talk_index == "n800_native_taruun-1-e" then 
	end
	if npc_talk_index == "n800_native_taruun-1-f" then 
	end
	if npc_talk_index == "n800_native_taruun-1-g" then 
	end
	if npc_talk_index == "n800_native_taruun-1-h" then 
	end
	if npc_talk_index == "n800_native_taruun-1-i" then 
	end
	if npc_talk_index == "n800_native_taruun-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc08_9639_prevent_him_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9639);
end

function mqc08_9639_prevent_him_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9639);
end

function mqc08_9639_prevent_him_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9639);
	local questID=9639;
end

function mqc08_9639_prevent_him_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc08_9639_prevent_him_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc08_9639_prevent_him_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9639, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9639, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9639, 1);
				npc_talk_index = "n707_sidel-1";
end

</GameServer>