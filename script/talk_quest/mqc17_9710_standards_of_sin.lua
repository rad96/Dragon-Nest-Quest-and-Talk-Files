<VillageServer>

function mqc17_9710_standards_of_sin_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1823 then
		mqc17_9710_standards_of_sin_OnTalk_n1823_merca_fargeau(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1825 then
		mqc17_9710_standards_of_sin_OnTalk_n1825_wind_grele(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1816 then
		mqc17_9710_standards_of_sin_OnTalk_n1816_vernicka(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1823_merca_fargeau--------------------------------------------------------------------------------
function mqc17_9710_standards_of_sin_OnTalk_n1823_merca_fargeau(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1823_merca_fargeau-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1823_merca_fargeau-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1823_merca_fargeau-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1823_merca_fargeau-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9710, 2);
				api_quest_SetJournalStep(userObjID,9710, 1);
				api_quest_SetQuestStep(userObjID,9710, 1);
				npc_talk_index = "n1823_merca_fargeau-1";

	end
	if npc_talk_index == "n1823_merca_fargeau-1-b" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400483, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400483, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1823_merca_fargeau-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1825_wind_grele--------------------------------------------------------------------------------
function mqc17_9710_standards_of_sin_OnTalk_n1825_wind_grele(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1825_wind_grele-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1825_wind_grele-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1825_wind_grele-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1825_wind_grele-2-a" then 

				if api_quest_HasQuestItem(userObjID, 400483, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400483, api_quest_HasQuestItem(userObjID, 400483, 1));
				end
	end
	if npc_talk_index == "n1825_wind_grele-2-b" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-c" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-d" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-e" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-f" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-g" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-h" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-i" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-j" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-k" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-l" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-m" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-n" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-o" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-p" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-q" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-r" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-s" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-t" then 
	end
	if npc_talk_index == "n1825_wind_grele-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1816_vernicka--------------------------------------------------------------------------------
function mqc17_9710_standards_of_sin_OnTalk_n1816_vernicka(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1816_vernicka-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1816_vernicka-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1816_vernicka-3-b" then 
	end
	if npc_talk_index == "n1816_vernicka-3-e" then 
	end
	if npc_talk_index == "n1816_vernicka-3-c" then 
	end
	if npc_talk_index == "n1816_vernicka-3-d" then 
	end
	if npc_talk_index == "n1816_vernicka-3-d" then 
	end
	if npc_talk_index == "n1816_vernicka-3-e" then 
	end
	if npc_talk_index == "n1816_vernicka-3-e" then 
	end
	if npc_talk_index == "n1816_vernicka-3-f" then 
	end
	if npc_talk_index == "n1816_vernicka-3-g" then 
	end
	if npc_talk_index == "n1816_vernicka-3-h" then 
	end
	if npc_talk_index == "n1816_vernicka-3-i" then 
	end
	if npc_talk_index == "n1816_vernicka-3-j" then 
	end
	if npc_talk_index == "n1816_vernicka-3-k" then 
	end
	if npc_talk_index == "n1816_vernicka-3-l" then 
	end
	if npc_talk_index == "n1816_vernicka-3-m" then 
	end
	if npc_talk_index == "n1816_vernicka-3-n" then 
	end
	if npc_talk_index == "n1816_vernicka-3-o" then 
	end
	if npc_talk_index == "n1816_vernicka-3-p" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97100, true);
				 api_quest_RewardQuestUser(userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97100, true);
				 api_quest_RewardQuestUser(userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97100, true);
				 api_quest_RewardQuestUser(userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97100, true);
				 api_quest_RewardQuestUser(userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97100, true);
				 api_quest_RewardQuestUser(userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97100, true);
				 api_quest_RewardQuestUser(userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97100, true);
				 api_quest_RewardQuestUser(userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97100, true);
				 api_quest_RewardQuestUser(userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97100, true);
				 api_quest_RewardQuestUser(userObjID, 97100, questID, 1);
			 end 
	end
	if npc_talk_index == "n1816_vernicka-3-q" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9711, 2);
					api_quest_SetQuestStep(userObjID, 9711, 1);
					api_quest_SetJournalStep(userObjID, 9711, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1816_vernicka-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1816_vernicka-1", "mqc17_9711_embers_of_barn.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc17_9710_standards_of_sin_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9710);
end

function mqc17_9710_standards_of_sin_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9710);
end

function mqc17_9710_standards_of_sin_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9710);
	local questID=9710;
end

function mqc17_9710_standards_of_sin_OnRemoteStart( userObjID, questID )
end

function mqc17_9710_standards_of_sin_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc17_9710_standards_of_sin_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9710, 2);
				api_quest_SetJournalStep(userObjID,9710, 1);
				api_quest_SetQuestStep(userObjID,9710, 1);
				npc_talk_index = "n1823_merca_fargeau-1";
end

</VillageServer>

<GameServer>
function mqc17_9710_standards_of_sin_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1823 then
		mqc17_9710_standards_of_sin_OnTalk_n1823_merca_fargeau( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1825 then
		mqc17_9710_standards_of_sin_OnTalk_n1825_wind_grele( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1816 then
		mqc17_9710_standards_of_sin_OnTalk_n1816_vernicka( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1823_merca_fargeau--------------------------------------------------------------------------------
function mqc17_9710_standards_of_sin_OnTalk_n1823_merca_fargeau( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1823_merca_fargeau-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1823_merca_fargeau-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1823_merca_fargeau-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1823_merca_fargeau-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9710, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9710, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9710, 1);
				npc_talk_index = "n1823_merca_fargeau-1";

	end
	if npc_talk_index == "n1823_merca_fargeau-1-b" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400483, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400483, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1823_merca_fargeau-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1825_wind_grele--------------------------------------------------------------------------------
function mqc17_9710_standards_of_sin_OnTalk_n1825_wind_grele( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1825_wind_grele-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1825_wind_grele-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1825_wind_grele-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1825_wind_grele-2-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400483, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400483, api_quest_HasQuestItem( pRoom, userObjID, 400483, 1));
				end
	end
	if npc_talk_index == "n1825_wind_grele-2-b" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-c" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-d" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-e" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-f" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-g" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-h" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-i" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-j" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-k" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-l" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-m" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-n" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-o" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-p" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-q" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-r" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-s" then 
	end
	if npc_talk_index == "n1825_wind_grele-2-t" then 
	end
	if npc_talk_index == "n1825_wind_grele-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1816_vernicka--------------------------------------------------------------------------------
function mqc17_9710_standards_of_sin_OnTalk_n1816_vernicka( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1816_vernicka-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1816_vernicka-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1816_vernicka-3-b" then 
	end
	if npc_talk_index == "n1816_vernicka-3-e" then 
	end
	if npc_talk_index == "n1816_vernicka-3-c" then 
	end
	if npc_talk_index == "n1816_vernicka-3-d" then 
	end
	if npc_talk_index == "n1816_vernicka-3-d" then 
	end
	if npc_talk_index == "n1816_vernicka-3-e" then 
	end
	if npc_talk_index == "n1816_vernicka-3-e" then 
	end
	if npc_talk_index == "n1816_vernicka-3-f" then 
	end
	if npc_talk_index == "n1816_vernicka-3-g" then 
	end
	if npc_talk_index == "n1816_vernicka-3-h" then 
	end
	if npc_talk_index == "n1816_vernicka-3-i" then 
	end
	if npc_talk_index == "n1816_vernicka-3-j" then 
	end
	if npc_talk_index == "n1816_vernicka-3-k" then 
	end
	if npc_talk_index == "n1816_vernicka-3-l" then 
	end
	if npc_talk_index == "n1816_vernicka-3-m" then 
	end
	if npc_talk_index == "n1816_vernicka-3-n" then 
	end
	if npc_talk_index == "n1816_vernicka-3-o" then 
	end
	if npc_talk_index == "n1816_vernicka-3-p" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97100, questID, 1);
			 end 
	end
	if npc_talk_index == "n1816_vernicka-3-q" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9711, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9711, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9711, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1816_vernicka-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1816_vernicka-1", "mqc17_9711_embers_of_barn.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc17_9710_standards_of_sin_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9710);
end

function mqc17_9710_standards_of_sin_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9710);
end

function mqc17_9710_standards_of_sin_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9710);
	local questID=9710;
end

function mqc17_9710_standards_of_sin_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc17_9710_standards_of_sin_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc17_9710_standards_of_sin_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9710, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9710, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9710, 1);
				npc_talk_index = "n1823_merca_fargeau-1";
end

</GameServer>