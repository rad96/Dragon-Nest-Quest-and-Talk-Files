<VillageServer>

function mq15_9370_testimony_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 707 then
		mq15_9370_testimony_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 754 then
		mq15_9370_testimony_OnTalk_n754_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 831 then
		mq15_9370_testimony_OnTalk_n831_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 858 then
		mq15_9370_testimony_OnTalk_n858_place_of_trail(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mq15_9370_testimony_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9370, 2);
				api_quest_SetJournalStep(userObjID,9370, 1);
				api_quest_SetQuestStep(userObjID,9370, 1);
				npc_talk_index = "n707_sidel-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n754_cian--------------------------------------------------------------------------------
function mq15_9370_testimony_OnTalk_n754_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n754_cian-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n754_cian-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n754_cian-2-b" then 
	end
	if npc_talk_index == "n754_cian-2-c" then 
	end
	if npc_talk_index == "n754_cian-2-d" then 
	end
	if npc_talk_index == "n754_cian-2-e" then 
	end
	if npc_talk_index == "n754_cian-2-f" then 
	end
	if npc_talk_index == "n754_cian-2-lv_check" then 
				if api_user_GetUserLevel(userObjID) >= 55 then
									npc_talk_index = "n754_cian-2-h";

				else
									npc_talk_index = "n754_cian-2-g";

				end
	end
	if npc_talk_index == "n754_cian-2-i" then 
	end
	if npc_talk_index == "n754_cian-2-j" then 
	end
	if npc_talk_index == "n754_cian-2-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93700, true);
				 api_quest_RewardQuestUser(userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93700, true);
				 api_quest_RewardQuestUser(userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93700, true);
				 api_quest_RewardQuestUser(userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93700, true);
				 api_quest_RewardQuestUser(userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93700, true);
				 api_quest_RewardQuestUser(userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93700, true);
				 api_quest_RewardQuestUser(userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93700, true);
				 api_quest_RewardQuestUser(userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93700, true);
				 api_quest_RewardQuestUser(userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93700, true);
				 api_quest_RewardQuestUser(userObjID, 93700, questID, 1);
			 end 
	end
	if npc_talk_index == "n754_cian-2-l" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9371, 2);
					api_quest_SetQuestStep(userObjID, 9371, 1);
					api_quest_SetJournalStep(userObjID, 9371, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n754_cian-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n754_cian-1", "mq15_9371_double_or_quits.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n831_yuvenciel--------------------------------------------------------------------------------
function mq15_9370_testimony_OnTalk_n831_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n831_yuvenciel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n831_yuvenciel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n831_yuvenciel-1-b" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-c" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-d" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-e" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-f" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-g" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-h" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-i" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-j" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-k" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-l" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-m" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-n" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-o" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-p" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-q" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-r" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-s" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-t" then 
	end
	if npc_talk_index == "n831_yuvenciel-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n831_yuvenciel-2-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n858_place_of_trail--------------------------------------------------------------------------------
function mq15_9370_testimony_OnTalk_n858_place_of_trail(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n858_place_of_trail-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9370_testimony_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9370);
end

function mq15_9370_testimony_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9370);
end

function mq15_9370_testimony_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9370);
	local questID=9370;
end

function mq15_9370_testimony_OnRemoteStart( userObjID, questID )
end

function mq15_9370_testimony_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9370_testimony_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9370, 2);
				api_quest_SetJournalStep(userObjID,9370, 1);
				api_quest_SetQuestStep(userObjID,9370, 1);
				npc_talk_index = "n707_sidel-1";
end

</VillageServer>

<GameServer>
function mq15_9370_testimony_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 707 then
		mq15_9370_testimony_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 754 then
		mq15_9370_testimony_OnTalk_n754_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 831 then
		mq15_9370_testimony_OnTalk_n831_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 858 then
		mq15_9370_testimony_OnTalk_n858_place_of_trail( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mq15_9370_testimony_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9370, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9370, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9370, 1);
				npc_talk_index = "n707_sidel-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n754_cian--------------------------------------------------------------------------------
function mq15_9370_testimony_OnTalk_n754_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n754_cian-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n754_cian-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n754_cian-2-b" then 
	end
	if npc_talk_index == "n754_cian-2-c" then 
	end
	if npc_talk_index == "n754_cian-2-d" then 
	end
	if npc_talk_index == "n754_cian-2-e" then 
	end
	if npc_talk_index == "n754_cian-2-f" then 
	end
	if npc_talk_index == "n754_cian-2-lv_check" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 55 then
									npc_talk_index = "n754_cian-2-h";

				else
									npc_talk_index = "n754_cian-2-g";

				end
	end
	if npc_talk_index == "n754_cian-2-i" then 
	end
	if npc_talk_index == "n754_cian-2-j" then 
	end
	if npc_talk_index == "n754_cian-2-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93700, questID, 1);
			 end 
	end
	if npc_talk_index == "n754_cian-2-l" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9371, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9371, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9371, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n754_cian-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n754_cian-1", "mq15_9371_double_or_quits.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n831_yuvenciel--------------------------------------------------------------------------------
function mq15_9370_testimony_OnTalk_n831_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n831_yuvenciel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n831_yuvenciel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n831_yuvenciel-1-b" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-c" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-d" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-e" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-f" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-g" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-h" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-i" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-j" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-k" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-l" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-m" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-n" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-o" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-p" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-q" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-r" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-s" then 
	end
	if npc_talk_index == "n831_yuvenciel-1-t" then 
	end
	if npc_talk_index == "n831_yuvenciel-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n831_yuvenciel-2-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n858_place_of_trail--------------------------------------------------------------------------------
function mq15_9370_testimony_OnTalk_n858_place_of_trail( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n858_place_of_trail-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9370_testimony_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9370);
end

function mq15_9370_testimony_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9370);
end

function mq15_9370_testimony_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9370);
	local questID=9370;
end

function mq15_9370_testimony_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9370_testimony_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9370_testimony_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9370, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9370, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9370, 1);
				npc_talk_index = "n707_sidel-1";
end

</GameServer>