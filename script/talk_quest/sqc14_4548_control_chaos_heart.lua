<VillageServer>

function sqc14_4548_control_chaos_heart_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1502 then
		sqc14_4548_control_chaos_heart_OnTalk_n1502_ifuture3(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1502_ifuture3--------------------------------------------------------------------------------
function sqc14_4548_control_chaos_heart_OnTalk_n1502_ifuture3(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1502_ifuture3-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1502_ifuture3-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1502_ifuture3-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1502_ifuture3-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45480, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45480, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45480, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45480, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45480, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45480, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45480, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45480, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45480, false);
			 end 

	end
	if npc_talk_index == "n1502_ifuture3-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4548, 1);
				api_quest_SetJournalStep(userObjID,4548, 1);
				api_quest_SetQuestStep(userObjID,4548, 1);
				npc_talk_index = "n1502_ifuture3-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 1072, 1 );

	end
	if npc_talk_index == "n1502_ifuture3-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45480, true);
				 api_quest_RewardQuestUser(userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45480, true);
				 api_quest_RewardQuestUser(userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45480, true);
				 api_quest_RewardQuestUser(userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45480, true);
				 api_quest_RewardQuestUser(userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45480, true);
				 api_quest_RewardQuestUser(userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45480, true);
				 api_quest_RewardQuestUser(userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45480, true);
				 api_quest_RewardQuestUser(userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45480, true);
				 api_quest_RewardQuestUser(userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45480, true);
				 api_quest_RewardQuestUser(userObjID, 45480, questID, 1);
			 end 
	end
	if npc_talk_index == "n1502_ifuture3-2-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, true);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4548_control_chaos_heart_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4548);
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				if  api_user_GetStageConstructionLevel( userObjID ) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end
		 end 
	 end 
end

function sqc14_4548_control_chaos_heart_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4548);
end

function sqc14_4548_control_chaos_heart_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4548);
	local questID=4548;
end

function sqc14_4548_control_chaos_heart_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 1072, 1 );
end

function sqc14_4548_control_chaos_heart_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc14_4548_control_chaos_heart_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4548, 1);
				api_quest_SetJournalStep(userObjID,4548, 1);
				api_quest_SetQuestStep(userObjID,4548, 1);
				npc_talk_index = "n1502_ifuture3-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 1072, 1 );
end

</VillageServer>

<GameServer>
function sqc14_4548_control_chaos_heart_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1502 then
		sqc14_4548_control_chaos_heart_OnTalk_n1502_ifuture3( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1502_ifuture3--------------------------------------------------------------------------------
function sqc14_4548_control_chaos_heart_OnTalk_n1502_ifuture3( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1502_ifuture3-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1502_ifuture3-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1502_ifuture3-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1502_ifuture3-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, false);
			 end 

	end
	if npc_talk_index == "n1502_ifuture3-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4548, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4548, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4548, 1);
				npc_talk_index = "n1502_ifuture3-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 1072, 1 );

	end
	if npc_talk_index == "n1502_ifuture3-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45480, questID, 1);
			 end 
	end
	if npc_talk_index == "n1502_ifuture3-2-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, true);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4548_control_chaos_heart_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4548);
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				if  api_user_GetStageConstructionLevel( pRoom,  userObjID ) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end
		 end 
	 end 
end

function sqc14_4548_control_chaos_heart_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4548);
end

function sqc14_4548_control_chaos_heart_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4548);
	local questID=4548;
end

function sqc14_4548_control_chaos_heart_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 1072, 1 );
end

function sqc14_4548_control_chaos_heart_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc14_4548_control_chaos_heart_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4548, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4548, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4548, 1);
				npc_talk_index = "n1502_ifuture3-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 1072, 1 );
end

</GameServer>