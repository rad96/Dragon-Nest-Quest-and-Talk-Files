<VillageServer>

function sq15_4094_fantasy_of_stage_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 44 then
		sq15_4094_fantasy_of_stage_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n044_archer_master_ishilien--------------------------------------------------------------------------------
function sq15_4094_fantasy_of_stage_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n044_archer_master_ishilien-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40940, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40940, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40940, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40940, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40940, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40940, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40940, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40940, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40940, false);
			 end 

	end
	if npc_talk_index == "n044_archer_master_ishilien-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4094, 1);
				api_quest_SetJournalStep(userObjID,4094, 1);
				api_quest_SetQuestStep(userObjID,4094, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 700201, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 600201, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700214, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 600214, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300554, 1);

	end
	if npc_talk_index == "n044_archer_master_ishilien-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40940, true);
				 api_quest_RewardQuestUser(userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40940, true);
				 api_quest_RewardQuestUser(userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40940, true);
				 api_quest_RewardQuestUser(userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40940, true);
				 api_quest_RewardQuestUser(userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40940, true);
				 api_quest_RewardQuestUser(userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40940, true);
				 api_quest_RewardQuestUser(userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40940, true);
				 api_quest_RewardQuestUser(userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40940, true);
				 api_quest_RewardQuestUser(userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40940, true);
				 api_quest_RewardQuestUser(userObjID, 40940, questID, 1);
			 end 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300554, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300554, api_quest_HasQuestItem(userObjID, 300554, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4094_fantasy_of_stage_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4094);
	if qstep == 1 and CountIndex == 700201 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300554, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300554, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600201 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300554, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300554, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700214 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300554, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300554, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600214 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300554, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300554, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300554 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function sq15_4094_fantasy_of_stage_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4094);
	if qstep == 1 and CountIndex == 700201 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600201 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700214 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600214 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300554 and Count >= TargetCount  then

	end
end

function sq15_4094_fantasy_of_stage_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4094);
	local questID=4094;
end

function sq15_4094_fantasy_of_stage_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 700201, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 600201, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700214, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 600214, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300554, 1);
end

function sq15_4094_fantasy_of_stage_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4094_fantasy_of_stage_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4094, 1);
				api_quest_SetJournalStep(userObjID,4094, 1);
				api_quest_SetQuestStep(userObjID,4094, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 700201, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 600201, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700214, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 600214, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300554, 1);
end

</VillageServer>

<GameServer>
function sq15_4094_fantasy_of_stage_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 44 then
		sq15_4094_fantasy_of_stage_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n044_archer_master_ishilien--------------------------------------------------------------------------------
function sq15_4094_fantasy_of_stage_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n044_archer_master_ishilien-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, false);
			 end 

	end
	if npc_talk_index == "n044_archer_master_ishilien-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4094, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4094, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4094, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 700201, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 600201, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700214, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 600214, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300554, 1);

	end
	if npc_talk_index == "n044_archer_master_ishilien-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40940, questID, 1);
			 end 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300554, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300554, api_quest_HasQuestItem( pRoom, userObjID, 300554, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4094_fantasy_of_stage_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4094);
	if qstep == 1 and CountIndex == 700201 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300554, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300554, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600201 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300554, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300554, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700214 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300554, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300554, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600214 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300554, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300554, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300554 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function sq15_4094_fantasy_of_stage_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4094);
	if qstep == 1 and CountIndex == 700201 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600201 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700214 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600214 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300554 and Count >= TargetCount  then

	end
end

function sq15_4094_fantasy_of_stage_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4094);
	local questID=4094;
end

function sq15_4094_fantasy_of_stage_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 700201, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 600201, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700214, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 600214, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300554, 1);
end

function sq15_4094_fantasy_of_stage_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4094_fantasy_of_stage_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4094, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4094, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4094, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 700201, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 600201, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700214, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 600214, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300554, 1);
end

</GameServer>