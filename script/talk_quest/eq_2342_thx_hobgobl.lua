<VillageServer>

function eq_2342_thx_hobgobl_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1268 then
		eq_2342_thx_hobgobl_OnTalk_n1268_thx_godol(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1268_thx_godol--------------------------------------------------------------------------------
function eq_2342_thx_hobgobl_OnTalk_n1268_thx_godol(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1268_thx_godol-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1268_thx_godol-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1268_thx_godol-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23420, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23420, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23420, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23420, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23420, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23420, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23420, false);
			 end 

	end
	if npc_talk_index == "n1268_thx_godol-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2342, 1);
				api_quest_SetJournalStep(userObjID,2342, 1);
				api_quest_SetQuestStep(userObjID,2342, 1);
				npc_talk_index = "n1268_thx_godol-1";

	end
	if npc_talk_index == "n1268_thx_godol-1-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23420, true);
				 api_quest_RewardQuestUser(userObjID, 23420, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23420, true);
				 api_quest_RewardQuestUser(userObjID, 23420, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23420, true);
				 api_quest_RewardQuestUser(userObjID, 23420, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23420, true);
				 api_quest_RewardQuestUser(userObjID, 23420, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23420, true);
				 api_quest_RewardQuestUser(userObjID, 23420, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23420, true);
				 api_quest_RewardQuestUser(userObjID, 23420, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23420, true);
				 api_quest_RewardQuestUser(userObjID, 23420, questID, 1);
			 end 
	end
	if npc_talk_index == "n1268_thx_godol-1-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function eq_2342_thx_hobgobl_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2342);
end

function eq_2342_thx_hobgobl_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2342);
end

function eq_2342_thx_hobgobl_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2342);
	local questID=2342;
end

function eq_2342_thx_hobgobl_OnRemoteStart( userObjID, questID )
end

function eq_2342_thx_hobgobl_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function eq_2342_thx_hobgobl_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2342, 1);
				api_quest_SetJournalStep(userObjID,2342, 1);
				api_quest_SetQuestStep(userObjID,2342, 1);
				npc_talk_index = "n1268_thx_godol-1";
end

</VillageServer>

<GameServer>
function eq_2342_thx_hobgobl_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1268 then
		eq_2342_thx_hobgobl_OnTalk_n1268_thx_godol( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1268_thx_godol--------------------------------------------------------------------------------
function eq_2342_thx_hobgobl_OnTalk_n1268_thx_godol( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1268_thx_godol-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1268_thx_godol-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1268_thx_godol-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, false);
			 end 

	end
	if npc_talk_index == "n1268_thx_godol-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2342, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2342, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2342, 1);
				npc_talk_index = "n1268_thx_godol-1";

	end
	if npc_talk_index == "n1268_thx_godol-1-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23420, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23420, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23420, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23420, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23420, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23420, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23420, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23420, questID, 1);
			 end 
	end
	if npc_talk_index == "n1268_thx_godol-1-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function eq_2342_thx_hobgobl_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2342);
end

function eq_2342_thx_hobgobl_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2342);
end

function eq_2342_thx_hobgobl_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2342);
	local questID=2342;
end

function eq_2342_thx_hobgobl_OnRemoteStart( pRoom,  userObjID, questID )
end

function eq_2342_thx_hobgobl_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function eq_2342_thx_hobgobl_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2342, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2342, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2342, 1);
				npc_talk_index = "n1268_thx_godol-1";
end

</GameServer>