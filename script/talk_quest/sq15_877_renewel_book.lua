<VillageServer>

function sq15_877_renewel_book_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 44 then
		sq15_877_renewel_book_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n044_archer_master_ishilien--------------------------------------------------------------------------------
function sq15_877_renewel_book_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n044_archer_master_ishilien-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n044_archer_master_ishilien-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8770, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8770, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8770, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8770, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8770, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8770, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8770, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8770, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8770, false);
			 end 

	end
	if npc_talk_index == "n044_archer_master_ishilien-accepting-acceptted" then
				api_quest_AddQuest(userObjID,877, 1);
				api_quest_SetJournalStep(userObjID,877, 1);
				api_quest_SetQuestStep(userObjID,877, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";

	end
	if npc_talk_index == "n044_archer_master_ishilien-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 600161, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700161, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 600165, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 700165, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300517, 3);
				api_quest_SetCountingInfo(userObjID, questID, 5, 3, 300518, 3);
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8770, true);
				 api_quest_RewardQuestUser(userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8770, true);
				 api_quest_RewardQuestUser(userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8770, true);
				 api_quest_RewardQuestUser(userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8770, true);
				 api_quest_RewardQuestUser(userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8770, true);
				 api_quest_RewardQuestUser(userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8770, true);
				 api_quest_RewardQuestUser(userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8770, true);
				 api_quest_RewardQuestUser(userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8770, true);
				 api_quest_RewardQuestUser(userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8770, true);
				 api_quest_RewardQuestUser(userObjID, 8770, questID, 1);
			 end 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300517, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300517, api_quest_HasQuestItem(userObjID, 300517, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300518, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300518, api_quest_HasQuestItem(userObjID, 300518, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_877_renewel_book_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 877);
	if qstep == 2 and CountIndex == 600161 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300517, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300517, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 700161 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300517, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300517, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 600165 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300518, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300518, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 700165 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300518, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300518, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300517 then
				if api_quest_HasQuestItem(userObjID, 300517, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300518, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 300518 then
				if api_quest_HasQuestItem(userObjID, 300517, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300518, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
end

function sq15_877_renewel_book_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 877);
	if qstep == 2 and CountIndex == 600161 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700161 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 600165 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700165 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300517 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300518 and Count >= TargetCount  then

	end
end

function sq15_877_renewel_book_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 877);
	local questID=877;
end

function sq15_877_renewel_book_OnRemoteStart( userObjID, questID )
end

function sq15_877_renewel_book_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_877_renewel_book_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,877, 1);
				api_quest_SetJournalStep(userObjID,877, 1);
				api_quest_SetQuestStep(userObjID,877, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
end

</VillageServer>

<GameServer>
function sq15_877_renewel_book_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 44 then
		sq15_877_renewel_book_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n044_archer_master_ishilien--------------------------------------------------------------------------------
function sq15_877_renewel_book_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n044_archer_master_ishilien-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n044_archer_master_ishilien-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, false);
			 end 

	end
	if npc_talk_index == "n044_archer_master_ishilien-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,877, 1);
				api_quest_SetJournalStep( pRoom, userObjID,877, 1);
				api_quest_SetQuestStep( pRoom, userObjID,877, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";

	end
	if npc_talk_index == "n044_archer_master_ishilien-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 600161, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700161, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 600165, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 700165, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300517, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 3, 300518, 3);
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8770, questID, 1);
			 end 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300517, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300517, api_quest_HasQuestItem( pRoom, userObjID, 300517, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300518, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300518, api_quest_HasQuestItem( pRoom, userObjID, 300518, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_877_renewel_book_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 877);
	if qstep == 2 and CountIndex == 600161 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300517, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300517, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 700161 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300517, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300517, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 600165 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300518, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300518, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 700165 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300518, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300518, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300517 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300517, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300518, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 300518 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300517, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300518, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
end

function sq15_877_renewel_book_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 877);
	if qstep == 2 and CountIndex == 600161 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700161 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 600165 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700165 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300517 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300518 and Count >= TargetCount  then

	end
end

function sq15_877_renewel_book_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 877);
	local questID=877;
end

function sq15_877_renewel_book_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_877_renewel_book_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_877_renewel_book_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,877, 1);
				api_quest_SetJournalStep( pRoom, userObjID,877, 1);
				api_quest_SetQuestStep( pRoom, userObjID,877, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
end

</GameServer>