<VillageServer>

function mq11_411_rose_in_nightmare_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 216 then
		mq11_411_rose_in_nightmare_OnTalk_n216_geraint_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 251 then
		mq11_411_rose_in_nightmare_OnTalk_n251_rose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mq11_411_rose_in_nightmare_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mq11_411_rose_in_nightmare_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n216_geraint_wounded--------------------------------------------------------------------------------
function mq11_411_rose_in_nightmare_OnTalk_n216_geraint_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n216_geraint_wounded-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n216_geraint_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n216_geraint_wounded-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n216_geraint_wounded-1-b" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-c" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-d" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-e" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-f" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-g" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-h" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-i" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n251_rose--------------------------------------------------------------------------------
function mq11_411_rose_in_nightmare_OnTalk_n251_rose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n251_rose-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n251_rose-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mq11_411_rose_in_nightmare_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n038_royal_magician_kalaen-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n038_royal_magician_kalaen-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n038_royal_magician_kalaen-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-3-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-3-c" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300251, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 931, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200931, 30000);
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4111, true);
				 api_quest_RewardQuestUser(userObjID, 4111, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4112, true);
				 api_quest_RewardQuestUser(userObjID, 4112, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4113, true);
				 api_quest_RewardQuestUser(userObjID, 4113, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4114, true);
				 api_quest_RewardQuestUser(userObjID, 4114, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4111, true);
				 api_quest_RewardQuestUser(userObjID, 4111, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4116, true);
				 api_quest_RewardQuestUser(userObjID, 4116, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4111, true);
				 api_quest_RewardQuestUser(userObjID, 4111, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4111, true);
				 api_quest_RewardQuestUser(userObjID, 4111, questID, 1);
			 end 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 413, 2);
					api_quest_SetQuestStep(userObjID, 413, 1);
					api_quest_SetJournalStep(userObjID, 413, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300251, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300251, api_quest_HasQuestItem(userObjID, 300251, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300083, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300083, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n038_royal_magician_kalaen-1", "mq11_413_dreaming_rose.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mq11_411_rose_in_nightmare_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n039_bishop_ignasio-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n039_bishop_ignasio-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-accepting-acceptted" then
				api_quest_AddQuest(userObjID,411, 2);
				api_quest_SetJournalStep(userObjID,411, 1);
				api_quest_SetQuestStep(userObjID,411, 1);
				npc_talk_index = "n039_bishop_ignasio-1";

	end
	if npc_talk_index == "n039_bishop_ignasio-2-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-class1" then 
				if api_user_GetUserClassID(userObjID) == 4 then
									npc_talk_index = "n039_bishop_ignasio-2-c";

				else
									npc_talk_index = "n039_bishop_ignasio-2-e";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-2-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-g" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-g" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-h" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-levelc1" then 
				if api_user_GetUserLevel(userObjID) >= 32 then
									npc_talk_index = "n039_bishop_ignasio-2-j";

				else
									npc_talk_index = "n039_bishop_ignasio-2-i";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-2-k" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-k" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_411_rose_in_nightmare_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 411);
	if qstep == 4 and CountIndex == 300251 then
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 4 and CountIndex == 931 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300251, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 4 and CountIndex == 200931 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300251, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq11_411_rose_in_nightmare_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 411);
	if qstep == 4 and CountIndex == 300251 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 931 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 200931 and Count >= TargetCount  then

	end
end

function mq11_411_rose_in_nightmare_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 411);
	local questID=411;
end

function mq11_411_rose_in_nightmare_OnRemoteStart( userObjID, questID )
end

function mq11_411_rose_in_nightmare_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq11_411_rose_in_nightmare_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,411, 2);
				api_quest_SetJournalStep(userObjID,411, 1);
				api_quest_SetQuestStep(userObjID,411, 1);
				npc_talk_index = "n039_bishop_ignasio-1";
end

</VillageServer>

<GameServer>
function mq11_411_rose_in_nightmare_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 216 then
		mq11_411_rose_in_nightmare_OnTalk_n216_geraint_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 251 then
		mq11_411_rose_in_nightmare_OnTalk_n251_rose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mq11_411_rose_in_nightmare_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mq11_411_rose_in_nightmare_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n216_geraint_wounded--------------------------------------------------------------------------------
function mq11_411_rose_in_nightmare_OnTalk_n216_geraint_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n216_geraint_wounded-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n216_geraint_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n216_geraint_wounded-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n216_geraint_wounded-1-b" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-c" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-d" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-e" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-f" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-g" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-h" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-i" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n251_rose--------------------------------------------------------------------------------
function mq11_411_rose_in_nightmare_OnTalk_n251_rose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n251_rose-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n251_rose-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mq11_411_rose_in_nightmare_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n038_royal_magician_kalaen-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n038_royal_magician_kalaen-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n038_royal_magician_kalaen-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-3-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-3-c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300251, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 931, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200931, 30000);
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4111, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4111, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4112, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4112, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4113, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4113, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4114, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4114, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4111, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4111, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4116, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4116, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4111, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4111, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4111, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4111, questID, 1);
			 end 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 413, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 413, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 413, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300251, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300251, api_quest_HasQuestItem( pRoom, userObjID, 300251, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300083, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300083, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n038_royal_magician_kalaen-1", "mq11_413_dreaming_rose.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mq11_411_rose_in_nightmare_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n039_bishop_ignasio-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n039_bishop_ignasio-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,411, 2);
				api_quest_SetJournalStep( pRoom, userObjID,411, 1);
				api_quest_SetQuestStep( pRoom, userObjID,411, 1);
				npc_talk_index = "n039_bishop_ignasio-1";

	end
	if npc_talk_index == "n039_bishop_ignasio-2-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-class1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									npc_talk_index = "n039_bishop_ignasio-2-c";

				else
									npc_talk_index = "n039_bishop_ignasio-2-e";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-2-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-g" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-g" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-h" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-levelc1" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 32 then
									npc_talk_index = "n039_bishop_ignasio-2-j";

				else
									npc_talk_index = "n039_bishop_ignasio-2-i";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-2-k" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-k" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_411_rose_in_nightmare_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 411);
	if qstep == 4 and CountIndex == 300251 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 4 and CountIndex == 931 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300251, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 4 and CountIndex == 200931 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300251, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq11_411_rose_in_nightmare_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 411);
	if qstep == 4 and CountIndex == 300251 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 931 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 200931 and Count >= TargetCount  then

	end
end

function mq11_411_rose_in_nightmare_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 411);
	local questID=411;
end

function mq11_411_rose_in_nightmare_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq11_411_rose_in_nightmare_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq11_411_rose_in_nightmare_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,411, 2);
				api_quest_SetJournalStep( pRoom, userObjID,411, 1);
				api_quest_SetQuestStep( pRoom, userObjID,411, 1);
				npc_talk_index = "n039_bishop_ignasio-1";
end

</GameServer>