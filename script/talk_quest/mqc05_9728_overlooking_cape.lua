<VillageServer>

function mqc05_9728_overlooking_cape_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 42 then
		mqc05_9728_overlooking_cape_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mqc05_9728_overlooking_cape_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 312 then
		mqc05_9728_overlooking_cape_OnTalk_n312_velskud_injured_wing(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function mqc05_9728_overlooking_cape_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n042_general_duglars-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9728, 2);
				api_quest_SetJournalStep(userObjID,9728, 1);
				api_quest_SetQuestStep(userObjID,9728, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-1-b" then 
	end
	if npc_talk_index == "n042_general_duglars-1-c" then 
	end
	if npc_talk_index == "n042_general_duglars-1-d" then 
	end
	if npc_talk_index == "n042_general_duglars-1-e" then 
	end
	if npc_talk_index == "n042_general_duglars-1-f" then 
	end
	if npc_talk_index == "n042_general_duglars-1-g" then 
	end
	if npc_talk_index == "n042_general_duglars-1-h" then 
	end
	if npc_talk_index == "n042_general_duglars-1-i" then 
	end
	if npc_talk_index == "n042_general_duglars-1-j" then 
	end
	if npc_talk_index == "n042_general_duglars-1-k" then 
	end
	if npc_talk_index == "n042_general_duglars-1-l" then 
	end
	if npc_talk_index == "n042_general_duglars-1-m" then 
	end
	if npc_talk_index == "n042_general_duglars-1-n" then 
	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n042_general_duglars-5-b" then 
	end
	if npc_talk_index == "n042_general_duglars-5-c" then 
	end
	if npc_talk_index == "n042_general_duglars-5-d" then 
	end
	if npc_talk_index == "n042_general_duglars-5-e" then 
	end
	if npc_talk_index == "n042_general_duglars-5-f" then 
	end
	if npc_talk_index == "n042_general_duglars-5-g" then 
	end
	if npc_talk_index == "n042_general_duglars-5-h" then 
	end
	if npc_talk_index == "n042_general_duglars-5-i" then 
	end
	if npc_talk_index == "n042_general_duglars-5-j" then 
	end
	if npc_talk_index == "n042_general_duglars-5-k" then 
	end
	if npc_talk_index == "n042_general_duglars-5-l" then 
	end
	if npc_talk_index == "n042_general_duglars-5-m" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97280, true);
				 api_quest_RewardQuestUser(userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97280, true);
				 api_quest_RewardQuestUser(userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97280, true);
				 api_quest_RewardQuestUser(userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97280, true);
				 api_quest_RewardQuestUser(userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97280, true);
				 api_quest_RewardQuestUser(userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97280, true);
				 api_quest_RewardQuestUser(userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97280, true);
				 api_quest_RewardQuestUser(userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97280, true);
				 api_quest_RewardQuestUser(userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97280, true);
				 api_quest_RewardQuestUser(userObjID, 97280, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-5-n" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9729, 2);
					api_quest_SetQuestStep(userObjID, 9729, 1);
					api_quest_SetJournalStep(userObjID, 9729, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n042_general_duglars-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n042_general_duglars-1", "mqc05_9729_to_the_seadragon.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mqc05_9728_overlooking_cape_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n088_scholar_starshy-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-k" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-l" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n312_velskud_injured_wing--------------------------------------------------------------------------------
function mqc05_9728_overlooking_cape_OnTalk_n312_velskud_injured_wing(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n312_velskud_injured_wing-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n312_velskud_injured_wing-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n312_velskud_injured_wing-4-b" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-c" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-d" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-e" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-f" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-g" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-h" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-i" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-j" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-k" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-l" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-m" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-n" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-o" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-p" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-q" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-r" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-s" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-t" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-u" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-v" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-w" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-x" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9728_overlooking_cape_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9728);
end

function mqc05_9728_overlooking_cape_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9728);
end

function mqc05_9728_overlooking_cape_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9728);
	local questID=9728;
end

function mqc05_9728_overlooking_cape_OnRemoteStart( userObjID, questID )
end

function mqc05_9728_overlooking_cape_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc05_9728_overlooking_cape_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9728, 2);
				api_quest_SetJournalStep(userObjID,9728, 1);
				api_quest_SetQuestStep(userObjID,9728, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</VillageServer>

<GameServer>
function mqc05_9728_overlooking_cape_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 42 then
		mqc05_9728_overlooking_cape_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mqc05_9728_overlooking_cape_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 312 then
		mqc05_9728_overlooking_cape_OnTalk_n312_velskud_injured_wing( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function mqc05_9728_overlooking_cape_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n042_general_duglars-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9728, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9728, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9728, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-1-b" then 
	end
	if npc_talk_index == "n042_general_duglars-1-c" then 
	end
	if npc_talk_index == "n042_general_duglars-1-d" then 
	end
	if npc_talk_index == "n042_general_duglars-1-e" then 
	end
	if npc_talk_index == "n042_general_duglars-1-f" then 
	end
	if npc_talk_index == "n042_general_duglars-1-g" then 
	end
	if npc_talk_index == "n042_general_duglars-1-h" then 
	end
	if npc_talk_index == "n042_general_duglars-1-i" then 
	end
	if npc_talk_index == "n042_general_duglars-1-j" then 
	end
	if npc_talk_index == "n042_general_duglars-1-k" then 
	end
	if npc_talk_index == "n042_general_duglars-1-l" then 
	end
	if npc_talk_index == "n042_general_duglars-1-m" then 
	end
	if npc_talk_index == "n042_general_duglars-1-n" then 
	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n042_general_duglars-5-b" then 
	end
	if npc_talk_index == "n042_general_duglars-5-c" then 
	end
	if npc_talk_index == "n042_general_duglars-5-d" then 
	end
	if npc_talk_index == "n042_general_duglars-5-e" then 
	end
	if npc_talk_index == "n042_general_duglars-5-f" then 
	end
	if npc_talk_index == "n042_general_duglars-5-g" then 
	end
	if npc_talk_index == "n042_general_duglars-5-h" then 
	end
	if npc_talk_index == "n042_general_duglars-5-i" then 
	end
	if npc_talk_index == "n042_general_duglars-5-j" then 
	end
	if npc_talk_index == "n042_general_duglars-5-k" then 
	end
	if npc_talk_index == "n042_general_duglars-5-l" then 
	end
	if npc_talk_index == "n042_general_duglars-5-m" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97280, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-5-n" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9729, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9729, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9729, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n042_general_duglars-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n042_general_duglars-1", "mqc05_9729_to_the_seadragon.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mqc05_9728_overlooking_cape_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n088_scholar_starshy-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-k" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-l" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n312_velskud_injured_wing--------------------------------------------------------------------------------
function mqc05_9728_overlooking_cape_OnTalk_n312_velskud_injured_wing( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n312_velskud_injured_wing-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n312_velskud_injured_wing-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n312_velskud_injured_wing-4-b" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-c" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-d" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-e" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-f" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-g" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-h" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-i" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-j" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-k" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-l" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-m" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-n" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-o" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-p" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-q" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-r" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-s" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-t" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-u" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-v" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-w" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-4-x" then 
	end
	if npc_talk_index == "n312_velskud_injured_wing-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9728_overlooking_cape_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9728);
end

function mqc05_9728_overlooking_cape_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9728);
end

function mqc05_9728_overlooking_cape_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9728);
	local questID=9728;
end

function mqc05_9728_overlooking_cape_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc05_9728_overlooking_cape_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc05_9728_overlooking_cape_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9728, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9728, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9728, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</GameServer>