<VillageServer>

function sq11_493_darkelf_teleju3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 281 then
		sq11_493_darkelf_teleju3_OnTalk_n281_darkelf_teleju(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n281_darkelf_teleju--------------------------------------------------------------------------------
function sq11_493_darkelf_teleju3_OnTalk_n281_darkelf_teleju(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n281_darkelf_teleju-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n281_darkelf_teleju-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n281_darkelf_teleju-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n281_darkelf_teleju-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n281_darkelf_teleju-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4931, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4932, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4933, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4934, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4935, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4936, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4937, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4938, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4939, false);
			 end 

	end
	if npc_talk_index == "n281_darkelf_teleju-accepting-acceptted" then
				api_quest_AddQuest(userObjID,493, 1);
				api_quest_SetJournalStep(userObjID,493, 1);
				api_quest_SetQuestStep(userObjID,493, 1);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n281_darkelf_teleju-2";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300258, 30000);

	end
	if npc_talk_index == "n281_darkelf_teleju-1-b" then 
	end
	if npc_talk_index == "n281_darkelf_teleju-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 1159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 201159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 3, 300258, 30000);
	end
	if npc_talk_index == "n281_darkelf_teleju-3-b" then 
	end
	if npc_talk_index == "n281_darkelf_teleju-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4931, true);
				 api_quest_RewardQuestUser(userObjID, 4931, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4932, true);
				 api_quest_RewardQuestUser(userObjID, 4932, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4933, true);
				 api_quest_RewardQuestUser(userObjID, 4933, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4934, true);
				 api_quest_RewardQuestUser(userObjID, 4934, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4935, true);
				 api_quest_RewardQuestUser(userObjID, 4935, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4936, true);
				 api_quest_RewardQuestUser(userObjID, 4936, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4937, true);
				 api_quest_RewardQuestUser(userObjID, 4937, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4938, true);
				 api_quest_RewardQuestUser(userObjID, 4938, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4939, true);
				 api_quest_RewardQuestUser(userObjID, 4939, questID, 1);
			 end 
	end
	if npc_talk_index == "n281_darkelf_teleju-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 494, 1);
					api_quest_SetQuestStep(userObjID, 494, 1);
					api_quest_SetJournalStep(userObjID, 494, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n281_darkelf_teleju-1", "sq11_494_darkelf_teleju4.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_493_darkelf_teleju3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 493);
	if qstep == 2 and CountIndex == 1159 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300258, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300258, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201159 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300258, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300258, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300258 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 1 and CountIndex == 1159 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300258, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300258, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201159 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300258, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300258, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300258 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function sq11_493_darkelf_teleju3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 493);
	if qstep == 2 and CountIndex == 1159 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201159 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300258 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1159 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201159 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300258 and Count >= TargetCount  then

	end
end

function sq11_493_darkelf_teleju3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 493);
	local questID=493;
end

function sq11_493_darkelf_teleju3_OnRemoteStart( userObjID, questID )
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300258, 30000);
end

function sq11_493_darkelf_teleju3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_493_darkelf_teleju3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,493, 1);
				api_quest_SetJournalStep(userObjID,493, 1);
				api_quest_SetQuestStep(userObjID,493, 1);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n281_darkelf_teleju-2";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300258, 30000);
end

</VillageServer>

<GameServer>
function sq11_493_darkelf_teleju3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 281 then
		sq11_493_darkelf_teleju3_OnTalk_n281_darkelf_teleju( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n281_darkelf_teleju--------------------------------------------------------------------------------
function sq11_493_darkelf_teleju3_OnTalk_n281_darkelf_teleju( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n281_darkelf_teleju-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n281_darkelf_teleju-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n281_darkelf_teleju-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n281_darkelf_teleju-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n281_darkelf_teleju-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4931, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4932, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4933, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4934, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4935, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4936, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4937, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4938, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4939, false);
			 end 

	end
	if npc_talk_index == "n281_darkelf_teleju-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,493, 1);
				api_quest_SetJournalStep( pRoom, userObjID,493, 1);
				api_quest_SetQuestStep( pRoom, userObjID,493, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n281_darkelf_teleju-2";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300258, 30000);

	end
	if npc_talk_index == "n281_darkelf_teleju-1-b" then 
	end
	if npc_talk_index == "n281_darkelf_teleju-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 1159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 201159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 3, 300258, 30000);
	end
	if npc_talk_index == "n281_darkelf_teleju-3-b" then 
	end
	if npc_talk_index == "n281_darkelf_teleju-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4931, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4931, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4932, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4932, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4933, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4933, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4934, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4934, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4935, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4935, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4936, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4936, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4937, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4937, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4938, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4938, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4939, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4939, questID, 1);
			 end 
	end
	if npc_talk_index == "n281_darkelf_teleju-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 494, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 494, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 494, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n281_darkelf_teleju-1", "sq11_494_darkelf_teleju4.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_493_darkelf_teleju3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 493);
	if qstep == 2 and CountIndex == 1159 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300258, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300258, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201159 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300258, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300258, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300258 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 1 and CountIndex == 1159 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300258, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300258, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201159 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300258, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300258, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300258 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function sq11_493_darkelf_teleju3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 493);
	if qstep == 2 and CountIndex == 1159 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201159 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300258 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1159 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201159 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300258 and Count >= TargetCount  then

	end
end

function sq11_493_darkelf_teleju3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 493);
	local questID=493;
end

function sq11_493_darkelf_teleju3_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300258, 30000);
end

function sq11_493_darkelf_teleju3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_493_darkelf_teleju3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,493, 1);
				api_quest_SetJournalStep( pRoom, userObjID,493, 1);
				api_quest_SetQuestStep( pRoom, userObjID,493, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n281_darkelf_teleju-2";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300258, 30000);
end

</GameServer>