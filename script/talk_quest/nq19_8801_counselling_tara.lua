<VillageServer>

function nq19_8801_counselling_tara_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 15 then
		nq19_8801_counselling_tara_OnTalk_n015_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		nq19_8801_counselling_tara_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n015_sorceress_tara--------------------------------------------------------------------------------
function nq19_8801_counselling_tara_OnTalk_n015_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n015_sorceress_tara-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n015_sorceress_tara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n015_sorceress_tara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n015_sorceress_tara-1-class1" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n015_sorceress_tara-1-a";

				else
									npc_talk_index = "n015_sorceress_tara-1-b";

				end
	end
	if npc_talk_index == "n015_sorceress_tara-1-c" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-c" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-d" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-e" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-f" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-g" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-h" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-j" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-j" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-i" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-j" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-class2" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n015_sorceress_tara-1-k";

				else
									npc_talk_index = "n015_sorceress_tara-1-l";

				end
	end
	if npc_talk_index == "n015_sorceress_tara-1-m" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-m" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function nq19_8801_counselling_tara_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-acceppted" then
				api_quest_AddQuest(userObjID,8801, 1);
				api_quest_SetJournalStep(userObjID,8801, 1);
				api_quest_SetQuestStep(userObjID,8801, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 15, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq19_8801_counselling_tara_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8801);
end

function nq19_8801_counselling_tara_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8801);
end

function nq19_8801_counselling_tara_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8801);
	local questID=8801;
end

function nq19_8801_counselling_tara_OnRemoteStart( userObjID, questID )
end

function nq19_8801_counselling_tara_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq19_8801_counselling_tara_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq19_8801_counselling_tara_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 15 then
		nq19_8801_counselling_tara_OnTalk_n015_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		nq19_8801_counselling_tara_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n015_sorceress_tara--------------------------------------------------------------------------------
function nq19_8801_counselling_tara_OnTalk_n015_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n015_sorceress_tara-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n015_sorceress_tara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n015_sorceress_tara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n015_sorceress_tara-1-class1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n015_sorceress_tara-1-a";

				else
									npc_talk_index = "n015_sorceress_tara-1-b";

				end
	end
	if npc_talk_index == "n015_sorceress_tara-1-c" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-c" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-d" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-e" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-f" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-g" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-h" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-j" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-j" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-i" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-j" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-class2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n015_sorceress_tara-1-k";

				else
									npc_talk_index = "n015_sorceress_tara-1-l";

				end
	end
	if npc_talk_index == "n015_sorceress_tara-1-m" then 
	end
	if npc_talk_index == "n015_sorceress_tara-1-m" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function nq19_8801_counselling_tara_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,8801, 1);
				api_quest_SetJournalStep( pRoom, userObjID,8801, 1);
				api_quest_SetQuestStep( pRoom, userObjID,8801, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 15, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq19_8801_counselling_tara_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8801);
end

function nq19_8801_counselling_tara_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8801);
end

function nq19_8801_counselling_tara_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8801);
	local questID=8801;
end

function nq19_8801_counselling_tara_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq19_8801_counselling_tara_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq19_8801_counselling_tara_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>