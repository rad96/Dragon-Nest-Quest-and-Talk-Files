<VillageServer>

function mqc05_9600_powder_dreams_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 216 then
		mqc05_9600_powder_dreams_OnTalk_n216_geraint_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1861 then
		mqc05_9600_powder_dreams_OnTalk_n1861_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		mqc05_9600_powder_dreams_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mqc05_9600_powder_dreams_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n216_geraint_wounded--------------------------------------------------------------------------------
function mqc05_9600_powder_dreams_OnTalk_n216_geraint_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1861_shaolong--------------------------------------------------------------------------------
function mqc05_9600_powder_dreams_OnTalk_n1861_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1861_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1861_shaolong-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1861_shaolong-2-b" then 
	end
	if npc_talk_index == "n1861_shaolong-2-c" then 
	end
	if npc_talk_index == "n1861_shaolong-2-d" then 
	end
	if npc_talk_index == "n1861_shaolong-2-e" then 
	end
	if npc_talk_index == "n1861_shaolong-2-f" then 
	end
	if npc_talk_index == "n1861_shaolong-2-g" then 
	end
	if npc_talk_index == "n1861_shaolong-2-h" then 
	end
	if npc_talk_index == "n1861_shaolong-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function mqc05_9600_powder_dreams_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n045_soceress_master_stella-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n045_soceress_master_stella-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n045_soceress_master_stella-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-3-questchk" then 
				if api_quest_IsMarkingCompleteQuest(userObjID, 465) == 1 then
									npc_talk_index = "n045_soceress_master_stella-3-d";

				else
									npc_talk_index = "n045_soceress_master_stella-3-b";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-3-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-g" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300251, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 931, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200931, 30000);
	end
	if npc_talk_index == "n045_soceress_master_stella-5-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 96000, true);
				 api_quest_RewardQuestUser(userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 96000, true);
				 api_quest_RewardQuestUser(userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 96000, true);
				 api_quest_RewardQuestUser(userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 96000, true);
				 api_quest_RewardQuestUser(userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 96000, true);
				 api_quest_RewardQuestUser(userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 96000, true);
				 api_quest_RewardQuestUser(userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 96000, true);
				 api_quest_RewardQuestUser(userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 96000, true);
				 api_quest_RewardQuestUser(userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 96000, true);
				 api_quest_RewardQuestUser(userObjID, 96000, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9601, 2);
					api_quest_SetQuestStep(userObjID, 9601, 1);
					api_quest_SetJournalStep(userObjID, 9601, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n045_soceress_master_stella-1", "mqc05_9601_dreaming_rose.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc05_9600_powder_dreams_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n039_bishop_ignasio-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9600, 2);
				api_quest_SetJournalStep(userObjID,9600, 1);
				api_quest_SetQuestStep(userObjID,9600, 1);
				npc_talk_index = "n039_bishop_ignasio-1";

	end
	if npc_talk_index == "n039_bishop_ignasio-1-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9600_powder_dreams_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9600);
	if qstep == 4 and CountIndex == 300251 then
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 4 and CountIndex == 931 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300251, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 4 and CountIndex == 200931 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300251, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mqc05_9600_powder_dreams_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9600);
	if qstep == 4 and CountIndex == 300251 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 931 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 200931 and Count >= TargetCount  then

	end
end

function mqc05_9600_powder_dreams_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9600);
	local questID=9600;
end

function mqc05_9600_powder_dreams_OnRemoteStart( userObjID, questID )
end

function mqc05_9600_powder_dreams_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc05_9600_powder_dreams_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9600, 2);
				api_quest_SetJournalStep(userObjID,9600, 1);
				api_quest_SetQuestStep(userObjID,9600, 1);
				npc_talk_index = "n039_bishop_ignasio-1";
end

</VillageServer>

<GameServer>
function mqc05_9600_powder_dreams_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 216 then
		mqc05_9600_powder_dreams_OnTalk_n216_geraint_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1861 then
		mqc05_9600_powder_dreams_OnTalk_n1861_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		mqc05_9600_powder_dreams_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mqc05_9600_powder_dreams_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n216_geraint_wounded--------------------------------------------------------------------------------
function mqc05_9600_powder_dreams_OnTalk_n216_geraint_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1861_shaolong--------------------------------------------------------------------------------
function mqc05_9600_powder_dreams_OnTalk_n1861_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1861_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1861_shaolong-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1861_shaolong-2-b" then 
	end
	if npc_talk_index == "n1861_shaolong-2-c" then 
	end
	if npc_talk_index == "n1861_shaolong-2-d" then 
	end
	if npc_talk_index == "n1861_shaolong-2-e" then 
	end
	if npc_talk_index == "n1861_shaolong-2-f" then 
	end
	if npc_talk_index == "n1861_shaolong-2-g" then 
	end
	if npc_talk_index == "n1861_shaolong-2-h" then 
	end
	if npc_talk_index == "n1861_shaolong-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function mqc05_9600_powder_dreams_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n045_soceress_master_stella-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n045_soceress_master_stella-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n045_soceress_master_stella-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-3-questchk" then 
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 465) == 1 then
									npc_talk_index = "n045_soceress_master_stella-3-d";

				else
									npc_talk_index = "n045_soceress_master_stella-3-b";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-3-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-g" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300251, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 931, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200931, 30000);
	end
	if npc_talk_index == "n045_soceress_master_stella-5-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96000, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-5-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9601, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9601, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9601, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-1", "mqc05_9601_dreaming_rose.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc05_9600_powder_dreams_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n039_bishop_ignasio-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9600, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9600, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9600, 1);
				npc_talk_index = "n039_bishop_ignasio-1";

	end
	if npc_talk_index == "n039_bishop_ignasio-1-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9600_powder_dreams_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9600);
	if qstep == 4 and CountIndex == 300251 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 4 and CountIndex == 931 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300251, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 4 and CountIndex == 200931 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300251, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mqc05_9600_powder_dreams_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9600);
	if qstep == 4 and CountIndex == 300251 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 931 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 200931 and Count >= TargetCount  then

	end
end

function mqc05_9600_powder_dreams_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9600);
	local questID=9600;
end

function mqc05_9600_powder_dreams_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc05_9600_powder_dreams_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc05_9600_powder_dreams_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9600, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9600, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9600, 1);
				npc_talk_index = "n039_bishop_ignasio-1";
end

</GameServer>