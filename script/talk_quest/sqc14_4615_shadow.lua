<VillageServer>

function sqc14_4615_shadow_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 999 then
		sqc14_4615_shadow_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n999_remote--------------------------------------------------------------------------------
function sqc14_4615_shadow_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n999_remote-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n999_remote-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4615_shadow_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4615);
end

function sqc14_4615_shadow_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4615);
end

function sqc14_4615_shadow_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4615);
	local questID=4615;
end

function sqc14_4615_shadow_OnRemoteStart( userObjID, questID )
end

function sqc14_4615_shadow_OnRemoteComplete( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, questID);
	if qstep == 2 then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_quest_RewardQuestUser(userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_quest_RewardQuestUser(userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_quest_RewardQuestUser(userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_quest_RewardQuestUser(userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_quest_RewardQuestUser(userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_quest_RewardQuestUser(userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_quest_RewardQuestUser(userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_quest_RewardQuestUser(userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_quest_RewardQuestUser(userObjID, 46150, questID, 1);
			 end 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
end

function sqc14_4615_shadow_CanRemoteCompleteStep( userObjID, questID, questStep )
	if questStep == 2 then
		return true;
	end

	return false;
end

function sqc14_4615_shadow_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sqc14_4615_shadow_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 999 then
		sqc14_4615_shadow_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n999_remote--------------------------------------------------------------------------------
function sqc14_4615_shadow_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n999_remote-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n999_remote-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4615_shadow_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4615);
end

function sqc14_4615_shadow_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4615);
end

function sqc14_4615_shadow_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4615);
	local questID=4615;
end

function sqc14_4615_shadow_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc14_4615_shadow_OnRemoteComplete( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, questID);
	if qstep == 2 then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 46150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 46150, questID, 1);
			 end 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
end

function sqc14_4615_shadow_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	if questStep == 2 then
		return true;
	end

	return false;
end

function sqc14_4615_shadow_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>