<VillageServer>

function sq_2263_noblesse_colabo_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 100 then
		sq_2263_noblesse_colabo_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq_2263_noblesse_colabo_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				if api_user_HasItem(userObjID, 805307967, 25) >= 25 then
									npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-1-b";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-h" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 22630, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 22630, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 22630, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 22630, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 22630, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 22630, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 22630, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 22630, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 22630, false);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-accepting-i" then
				api_quest_AddQuest(userObjID,2263, 1);
				api_quest_SetJournalStep(userObjID,2263, 1);
				api_quest_SetQuestStep(userObjID,2263, 1);
				api_quest_SetQuestMemo(userObjID, 2263, 1, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805307967, 25);
				if api_user_HasItem(userObjID, 805307967, 25) >= 25 then
									npc_talk_index = "n100_event_ilyn-1";
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestMemo(userObjID, 2263, 1, 2);

				else
									npc_talk_index = "n100_event_ilyn-accepting-i";

				end

	end
	if npc_talk_index == "n100_event_ilyn-1-exchange1" then 
				if api_user_HasItem(userObjID, 805307967, 25) >= 25 then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 22630, true);
				 api_quest_RewardQuestUser(userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 22630, true);
				 api_quest_RewardQuestUser(userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 22630, true);
				 api_quest_RewardQuestUser(userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 22630, true);
				 api_quest_RewardQuestUser(userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 22630, true);
				 api_quest_RewardQuestUser(userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 22630, true);
				 api_quest_RewardQuestUser(userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 22630, true);
				 api_quest_RewardQuestUser(userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 22630, true);
				 api_quest_RewardQuestUser(userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 22630, true);
				 api_quest_RewardQuestUser(userObjID, 22630, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-c";

				else
									npc_talk_index = "n100_event_ilyn-1-b";

				end
	end
	if npc_talk_index == "n100_event_ilyn-1-exit_event" then 
				api_npc_NextScript(userObjID, npcObjID, "quest", "n100_event_ilyn.xml");
	end
	if npc_talk_index == "n100_event_ilyn-1-complete" then 
				api_user_DelItem(userObjID, 805307967, 25, questID);

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_DelItem(userObjID, 805307967, 25, questID);


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2263_noblesse_colabo_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2263);
	if qstep == 1 and CountIndex == 805307967 then
				if api_user_HasItem(userObjID, 805307967, 25) >= 25  and api_quest_GetQuestMemo(userObjID, 2263, 1) == 1  then
									api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestMemo(userObjID, 2263, 1, 2);

				else
									if api_user_HasItem(userObjID, 805307967, 25) == -3  and api_quest_GetQuestMemo(userObjID, 2263, 1) == 2  then
									api_quest_SetJournalStep(userObjID, questID, 1);
				api_quest_SetQuestMemo(userObjID, 2263, 1, 1);

				else
				end

				end

	end
end

function sq_2263_noblesse_colabo_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2263);
	if qstep == 1 and CountIndex == 805307967 and Count >= TargetCount  then

	end
end

function sq_2263_noblesse_colabo_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2263);
	local questID=2263;
end

function sq_2263_noblesse_colabo_OnRemoteStart( userObjID, questID )
end

function sq_2263_noblesse_colabo_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_2263_noblesse_colabo_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq_2263_noblesse_colabo_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 100 then
		sq_2263_noblesse_colabo_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq_2263_noblesse_colabo_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				if api_user_HasItem( pRoom, userObjID, 805307967, 25) >= 25 then
									npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-1-b";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-h" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, false);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-accepting-i" then
				api_quest_AddQuest( pRoom, userObjID,2263, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2263, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2263, 1);
				api_quest_SetQuestMemo( pRoom, userObjID, 2263, 1, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805307967, 25);
				if api_user_HasItem( pRoom, userObjID, 805307967, 25) >= 25 then
									npc_talk_index = "n100_event_ilyn-1";
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestMemo( pRoom, userObjID, 2263, 1, 2);

				else
									npc_talk_index = "n100_event_ilyn-accepting-i";

				end

	end
	if npc_talk_index == "n100_event_ilyn-1-exchange1" then 
				if api_user_HasItem( pRoom, userObjID, 805307967, 25) >= 25 then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 22630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 22630, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-c";

				else
									npc_talk_index = "n100_event_ilyn-1-b";

				end
	end
	if npc_talk_index == "n100_event_ilyn-1-exit_event" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "quest", "n100_event_ilyn.xml");
	end
	if npc_talk_index == "n100_event_ilyn-1-complete" then 
				api_user_DelItem( pRoom, userObjID, 805307967, 25, questID);

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_DelItem( pRoom, userObjID, 805307967, 25, questID);


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2263_noblesse_colabo_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2263);
	if qstep == 1 and CountIndex == 805307967 then
				if api_user_HasItem( pRoom, userObjID, 805307967, 25) >= 25  and api_quest_GetQuestMemo( pRoom, userObjID, 2263, 1) == 1  then
									api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestMemo( pRoom, userObjID, 2263, 1, 2);

				else
									if api_user_HasItem( pRoom, userObjID, 805307967, 25) == -3  and api_quest_GetQuestMemo( pRoom, userObjID, 2263, 1) == 2  then
									api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				api_quest_SetQuestMemo( pRoom, userObjID, 2263, 1, 1);

				else
				end

				end

	end
end

function sq_2263_noblesse_colabo_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2263);
	if qstep == 1 and CountIndex == 805307967 and Count >= TargetCount  then

	end
end

function sq_2263_noblesse_colabo_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2263);
	local questID=2263;
end

function sq_2263_noblesse_colabo_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_2263_noblesse_colabo_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_2263_noblesse_colabo_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>