<VillageServer>

function nq11_5712_lily_30_reward_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 7 then
		nq11_5712_lily_30_reward_OnTalk_n007_lily(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n007_lily--------------------------------------------------------------------------------
function nq11_5712_lily_30_reward_OnTalk_n007_lily(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n007_lily-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n007_lily-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n007_lily-1";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 end 
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n007_lily-accepting-acceptted" then
				api_quest_AddQuest(userObjID,5712, 1);
				api_quest_SetJournalStep(userObjID,5712, 1);
				api_quest_SetQuestStep(userObjID,5712, 1);
				npc_talk_index = "n007_lily-1";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 end 

	end
	if npc_talk_index == "n007_lily-1-a" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq11_5712_lily_30_reward_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5712);
end

function nq11_5712_lily_30_reward_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5712);
end

function nq11_5712_lily_30_reward_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5712);
	local questID=5712;
end

function nq11_5712_lily_30_reward_OnRemoteStart( userObjID, questID )
end

function nq11_5712_lily_30_reward_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq11_5712_lily_30_reward_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,5712, 1);
				api_quest_SetJournalStep(userObjID,5712, 1);
				api_quest_SetQuestStep(userObjID,5712, 1);
				npc_talk_index = "n007_lily-1";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 57120, true);
				 api_quest_RewardQuestUser(userObjID, 57120, questID, 1);
			 end 
end

</VillageServer>

<GameServer>
function nq11_5712_lily_30_reward_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 7 then
		nq11_5712_lily_30_reward_OnTalk_n007_lily( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n007_lily--------------------------------------------------------------------------------
function nq11_5712_lily_30_reward_OnTalk_n007_lily( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n007_lily-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n007_lily-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n007_lily-1";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 end 
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n007_lily-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,5712, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5712, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5712, 1);
				npc_talk_index = "n007_lily-1";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 end 

	end
	if npc_talk_index == "n007_lily-1-a" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq11_5712_lily_30_reward_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5712);
end

function nq11_5712_lily_30_reward_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5712);
end

function nq11_5712_lily_30_reward_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5712);
	local questID=5712;
end

function nq11_5712_lily_30_reward_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq11_5712_lily_30_reward_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq11_5712_lily_30_reward_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,5712, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5712, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5712, 1);
				npc_talk_index = "n007_lily-1";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 57120, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 57120, questID, 1);
			 end 
end

</GameServer>