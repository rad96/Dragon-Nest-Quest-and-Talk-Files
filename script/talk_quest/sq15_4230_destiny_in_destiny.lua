<VillageServer>

function sq15_4230_destiny_in_destiny_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1023 then
		sq15_4230_destiny_in_destiny_OnTalk_n1023_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		sq15_4230_destiny_in_destiny_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1023_academic--------------------------------------------------------------------------------
function sq15_4230_destiny_in_destiny_OnTalk_n1023_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n1023_academic-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1023_academic-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1023_academic-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1023_academic-1-b" then 
	end
	if npc_talk_index == "n1023_academic-1-b" then 
	end
	if npc_talk_index == "n1023_academic-1-c" then 
	end
	if npc_talk_index == "n1023_academic-1-c" then 
	end
	if npc_talk_index == "n1023_academic-1-d" then 
	end
	if npc_talk_index == "n1023_academic-1-d" then 
	end
	if npc_talk_index == "n1023_academic-1-e" then 
	end
	if npc_talk_index == "n1023_academic-1-f" then 
	end
	if npc_talk_index == "n1023_academic-1-g" then 
	end
	if npc_talk_index == "n1023_academic-1-h" then 
	end
	if npc_talk_index == "n1023_academic-1-i" then 
	end
	if npc_talk_index == "n1023_academic-1-i" then 
	end
	if npc_talk_index == "n1023_academic-1-j" then 
	end
	if npc_talk_index == "n1023_academic-1-j" then 
	end
	if npc_talk_index == "n1023_academic-1-k" then 
	end
	if npc_talk_index == "n1023_academic-1-l" then 
	end
	if npc_talk_index == "n1023_academic-1-m" then 
	end
	if npc_talk_index == "n1023_academic-1-n" then 
	end
	if npc_talk_index == "n1023_academic-1-o" then 
	end
	if npc_talk_index == "n1023_academic-1-p" then 
	end
	if npc_talk_index == "n1023_academic-1-q" then 
	end
	if npc_talk_index == "n1023_academic-1-r" then 
	end
	if npc_talk_index == "n1023_academic-1-s" then 
	end
	if npc_talk_index == "n1023_academic-1-t" then 
	end
	if npc_talk_index == "n1023_academic-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				if api_quest_HasQuestItem(userObjID, 400379, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400379, api_quest_HasQuestItem(userObjID, 400379, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400380, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400380, api_quest_HasQuestItem(userObjID, 400380, 1));
				end
	end
	if npc_talk_index == "n1023_academic-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				if api_quest_HasQuestItem(userObjID, 400379, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400379, api_quest_HasQuestItem(userObjID, 400379, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400380, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400380, api_quest_HasQuestItem(userObjID, 400380, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq15_4230_destiny_in_destiny_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 42300, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 42300, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 42300, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 42300, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 42300, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 42300, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 42300, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 42300, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 42300, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4230, 1);
				api_quest_SetJournalStep(userObjID,4230, 1);
				api_quest_SetQuestStep(userObjID,4230, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 42300, true);
				 api_quest_RewardQuestUser(userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 42300, true);
				 api_quest_RewardQuestUser(userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 42300, true);
				 api_quest_RewardQuestUser(userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 42300, true);
				 api_quest_RewardQuestUser(userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 42300, true);
				 api_quest_RewardQuestUser(userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 42300, true);
				 api_quest_RewardQuestUser(userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 42300, true);
				 api_quest_RewardQuestUser(userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 42300, true);
				 api_quest_RewardQuestUser(userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 42300, true);
				 api_quest_RewardQuestUser(userObjID, 42300, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4230_destiny_in_destiny_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4230);
end

function sq15_4230_destiny_in_destiny_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4230);
end

function sq15_4230_destiny_in_destiny_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4230);
	local questID=4230;
end

function sq15_4230_destiny_in_destiny_OnRemoteStart( userObjID, questID )
end

function sq15_4230_destiny_in_destiny_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4230_destiny_in_destiny_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4230, 1);
				api_quest_SetJournalStep(userObjID,4230, 1);
				api_quest_SetQuestStep(userObjID,4230, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
end

</VillageServer>

<GameServer>
function sq15_4230_destiny_in_destiny_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1023 then
		sq15_4230_destiny_in_destiny_OnTalk_n1023_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		sq15_4230_destiny_in_destiny_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1023_academic--------------------------------------------------------------------------------
function sq15_4230_destiny_in_destiny_OnTalk_n1023_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n1023_academic-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1023_academic-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1023_academic-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1023_academic-1-b" then 
	end
	if npc_talk_index == "n1023_academic-1-b" then 
	end
	if npc_talk_index == "n1023_academic-1-c" then 
	end
	if npc_talk_index == "n1023_academic-1-c" then 
	end
	if npc_talk_index == "n1023_academic-1-d" then 
	end
	if npc_talk_index == "n1023_academic-1-d" then 
	end
	if npc_talk_index == "n1023_academic-1-e" then 
	end
	if npc_talk_index == "n1023_academic-1-f" then 
	end
	if npc_talk_index == "n1023_academic-1-g" then 
	end
	if npc_talk_index == "n1023_academic-1-h" then 
	end
	if npc_talk_index == "n1023_academic-1-i" then 
	end
	if npc_talk_index == "n1023_academic-1-i" then 
	end
	if npc_talk_index == "n1023_academic-1-j" then 
	end
	if npc_talk_index == "n1023_academic-1-j" then 
	end
	if npc_talk_index == "n1023_academic-1-k" then 
	end
	if npc_talk_index == "n1023_academic-1-l" then 
	end
	if npc_talk_index == "n1023_academic-1-m" then 
	end
	if npc_talk_index == "n1023_academic-1-n" then 
	end
	if npc_talk_index == "n1023_academic-1-o" then 
	end
	if npc_talk_index == "n1023_academic-1-p" then 
	end
	if npc_talk_index == "n1023_academic-1-q" then 
	end
	if npc_talk_index == "n1023_academic-1-r" then 
	end
	if npc_talk_index == "n1023_academic-1-s" then 
	end
	if npc_talk_index == "n1023_academic-1-t" then 
	end
	if npc_talk_index == "n1023_academic-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				if api_quest_HasQuestItem( pRoom, userObjID, 400379, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400379, api_quest_HasQuestItem( pRoom, userObjID, 400379, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400380, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400380, api_quest_HasQuestItem( pRoom, userObjID, 400380, 1));
				end
	end
	if npc_talk_index == "n1023_academic-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				if api_quest_HasQuestItem( pRoom, userObjID, 400379, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400379, api_quest_HasQuestItem( pRoom, userObjID, 400379, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400380, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400380, api_quest_HasQuestItem( pRoom, userObjID, 400380, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq15_4230_destiny_in_destiny_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4230, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4230, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4230, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42300, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4230_destiny_in_destiny_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4230);
end

function sq15_4230_destiny_in_destiny_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4230);
end

function sq15_4230_destiny_in_destiny_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4230);
	local questID=4230;
end

function sq15_4230_destiny_in_destiny_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4230_destiny_in_destiny_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4230_destiny_in_destiny_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4230, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4230, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4230, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
end

</GameServer>