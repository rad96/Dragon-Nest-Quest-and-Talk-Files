<VillageServer>

function gq11_3193_n412_dominion_drop_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 412 then
		gq11_3193_n412_dominion_drop_OnTalk_n412_guild_board(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n412_guild_board--------------------------------------------------------------------------------
function gq11_3193_n412_dominion_drop_OnTalk_n412_guild_board(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n412_guild_board-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n412_guild_board-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n412_guild_board-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n412_guild_board-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 31930, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 31930, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 31930, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 31930, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 31930, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 31930, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 31930, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 31930, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 31930, false);
			 end 

	end
	if npc_talk_index == "n412_guild_board-accepting-d" then
				api_quest_AddQuest(userObjID,3193, 1);
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 827, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200827, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300206, 1);
				api_quest_SetJournalStep(userObjID, questID, 1);

	end
	if npc_talk_index == "n412_guild_board-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 31930, true);
				 api_quest_RewardQuestUser(userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 31930, true);
				 api_quest_RewardQuestUser(userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 31930, true);
				 api_quest_RewardQuestUser(userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 31930, true);
				 api_quest_RewardQuestUser(userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 31930, true);
				 api_quest_RewardQuestUser(userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 31930, true);
				 api_quest_RewardQuestUser(userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 31930, true);
				 api_quest_RewardQuestUser(userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 31930, true);
				 api_quest_RewardQuestUser(userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 31930, true);
				 api_quest_RewardQuestUser(userObjID, 31930, questID, 1);
			 end 
	end
	if npc_talk_index == "n412_guild_board-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300206, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300206, api_quest_HasQuestItem(userObjID, 300206, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function gq11_3193_n412_dominion_drop_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 3193);
	if qstep == 1 and CountIndex == 827 then
				if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300206, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300206, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if qstep == 1 and CountIndex == 300206 then

	end
	if qstep == 1 and CountIndex == 200827 then
				api_quest_IncCounting(userObjID, 2, 827);

	end
end

function gq11_3193_n412_dominion_drop_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 3193);
	if qstep == 1 and CountIndex == 827 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300206 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 200827 and Count >= TargetCount  then

	end
end

function gq11_3193_n412_dominion_drop_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 3193);
	local questID=3193;
end

function gq11_3193_n412_dominion_drop_OnRemoteStart( userObjID, questID )
end

function gq11_3193_n412_dominion_drop_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function gq11_3193_n412_dominion_drop_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function gq11_3193_n412_dominion_drop_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 412 then
		gq11_3193_n412_dominion_drop_OnTalk_n412_guild_board( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n412_guild_board--------------------------------------------------------------------------------
function gq11_3193_n412_dominion_drop_OnTalk_n412_guild_board( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n412_guild_board-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n412_guild_board-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n412_guild_board-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n412_guild_board-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, false);
			 end 

	end
	if npc_talk_index == "n412_guild_board-accepting-d" then
				api_quest_AddQuest( pRoom, userObjID,3193, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 827, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200827, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300206, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);

	end
	if npc_talk_index == "n412_guild_board-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 31930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 31930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 31930, questID, 1);
			 end 
	end
	if npc_talk_index == "n412_guild_board-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300206, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300206, api_quest_HasQuestItem( pRoom, userObjID, 300206, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function gq11_3193_n412_dominion_drop_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 3193);
	if qstep == 1 and CountIndex == 827 then
				if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300206, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300206, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if qstep == 1 and CountIndex == 300206 then

	end
	if qstep == 1 and CountIndex == 200827 then
				api_quest_IncCounting( pRoom, userObjID, 2, 827);

	end
end

function gq11_3193_n412_dominion_drop_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 3193);
	if qstep == 1 and CountIndex == 827 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300206 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 200827 and Count >= TargetCount  then

	end
end

function gq11_3193_n412_dominion_drop_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 3193);
	local questID=3193;
end

function gq11_3193_n412_dominion_drop_OnRemoteStart( pRoom,  userObjID, questID )
end

function gq11_3193_n412_dominion_drop_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function gq11_3193_n412_dominion_drop_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>