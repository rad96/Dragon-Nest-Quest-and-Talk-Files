<VillageServer>

function sq11_499_powerup_mucle1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 98 then
		sq11_499_powerup_mucle1_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq11_499_powerup_mucle1_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n098_warrior_master_lodrigo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n098_warrior_master_lodrigo-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-i" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4990, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4990, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4990, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4990, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4990, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4990, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4990, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4990, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4990, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-acceptted" then
				api_quest_AddQuest(userObjID,499, 1);
				api_quest_SetJournalStep(userObjID,499, 1);
				api_quest_SetQuestStep(userObjID,499, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 331, 1 );
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4990, true);
				 api_quest_RewardQuestUser(userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4990, true);
				 api_quest_RewardQuestUser(userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4990, true);
				 api_quest_RewardQuestUser(userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4990, true);
				 api_quest_RewardQuestUser(userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4990, true);
				 api_quest_RewardQuestUser(userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4990, true);
				 api_quest_RewardQuestUser(userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4990, true);
				 api_quest_RewardQuestUser(userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4990, true);
				 api_quest_RewardQuestUser(userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4990, true);
				 api_quest_RewardQuestUser(userObjID, 4990, questID, 1);
			 end 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 500, 1);
					api_quest_SetQuestStep(userObjID, 500, 1);
					api_quest_SetJournalStep(userObjID, 500, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-d" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-e" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-f" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n098_warrior_master_lodrigo-1", "sq11_500_powerup_mucle2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_499_powerup_mucle1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 499);
	 local qstep= 1; 
	 if qstep == 1 then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 5 then 
				if  api_user_GetStageConstructionLevel( userObjID ) >= 0 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end
		 end 
	 end 
end

function sq11_499_powerup_mucle1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 499);
end

function sq11_499_powerup_mucle1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 499);
	local questID=499;
end

function sq11_499_powerup_mucle1_OnRemoteStart( userObjID, questID )
end

function sq11_499_powerup_mucle1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_499_powerup_mucle1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,499, 1);
				api_quest_SetJournalStep(userObjID,499, 1);
				api_quest_SetQuestStep(userObjID,499, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
end

</VillageServer>

<GameServer>
function sq11_499_powerup_mucle1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 98 then
		sq11_499_powerup_mucle1_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq11_499_powerup_mucle1_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n098_warrior_master_lodrigo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n098_warrior_master_lodrigo-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-i" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,499, 1);
				api_quest_SetJournalStep( pRoom, userObjID,499, 1);
				api_quest_SetQuestStep( pRoom, userObjID,499, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 331, 1 );
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4990, questID, 1);
			 end 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 500, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 500, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 500, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-d" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-e" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-f" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n098_warrior_master_lodrigo-1", "sq11_500_powerup_mucle2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_499_powerup_mucle1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 499);
	 local qstep= 1; 
	 if qstep == 1 then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 5 then 
				if  api_user_GetStageConstructionLevel( pRoom,  userObjID ) >= 0 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end
		 end 
	 end 
end

function sq11_499_powerup_mucle1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 499);
end

function sq11_499_powerup_mucle1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 499);
	local questID=499;
end

function sq11_499_powerup_mucle1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_499_powerup_mucle1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_499_powerup_mucle1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,499, 1);
				api_quest_SetJournalStep( pRoom, userObjID,499, 1);
				api_quest_SetQuestStep( pRoom, userObjID,499, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
end

</GameServer>