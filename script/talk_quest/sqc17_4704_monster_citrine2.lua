<VillageServer>

function sqc17_4704_monster_citrine2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1835 then
		sqc17_4704_monster_citrine2_OnTalk_n1835_conscience_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1878 then
		sqc17_4704_monster_citrine2_OnTalk_n1878_weak_alicanto(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1835_conscience_pope--------------------------------------------------------------------------------
function sqc17_4704_monster_citrine2_OnTalk_n1835_conscience_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1835_conscience_pope-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1835_conscience_pope-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1835_conscience_pope-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1835_conscience_pope-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1835_conscience_pope-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47040, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47040, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47040, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47040, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47040, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47040, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47040, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47040, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47040, false);
			 end 

	end
	if npc_talk_index == "n1835_conscience_pope-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4704, 1);
				api_quest_SetJournalStep(userObjID,4704, 1);
				api_quest_SetQuestStep(userObjID,4704, 1);
				npc_talk_index = "n1835_conscience_pope-1";

	end
	if npc_talk_index == "n1835_conscience_pope-1-b" then 
	end
	if npc_talk_index == "n1835_conscience_pope-1-c" then 
	end
	if npc_talk_index == "n1835_conscience_pope-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1835_conscience_pope-3-b" then 
	end
	if npc_talk_index == "n1835_conscience_pope-3-c" then 
	end
	if npc_talk_index == "n1835_conscience_pope-3-d" then 
	end
	if npc_talk_index == "n1835_conscience_pope-3-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47040, true);
				 api_quest_RewardQuestUser(userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47040, true);
				 api_quest_RewardQuestUser(userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47040, true);
				 api_quest_RewardQuestUser(userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47040, true);
				 api_quest_RewardQuestUser(userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47040, true);
				 api_quest_RewardQuestUser(userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47040, true);
				 api_quest_RewardQuestUser(userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47040, true);
				 api_quest_RewardQuestUser(userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47040, true);
				 api_quest_RewardQuestUser(userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47040, true);
				 api_quest_RewardQuestUser(userObjID, 47040, questID, 1);
			 end 
	end
	if npc_talk_index == "n1835_conscience_pope-3-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4705, 1);
					api_quest_SetQuestStep(userObjID, 4705, 1);
					api_quest_SetJournalStep(userObjID, 4705, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1835_conscience_pope-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1835_conscience_pope-1", "sqc17_4705_monster_citrine3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1878_weak_alicanto--------------------------------------------------------------------------------
function sqc17_4704_monster_citrine2_OnTalk_n1878_weak_alicanto(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1878_weak_alicanto-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1878_weak_alicanto-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1878_weak_alicanto-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1878_weak_alicanto-2-b" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-c" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-d" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-e" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-f" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-g" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-h" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-i" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-j" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-k" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4704_monster_citrine2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4704);
end

function sqc17_4704_monster_citrine2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4704);
end

function sqc17_4704_monster_citrine2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4704);
	local questID=4704;
end

function sqc17_4704_monster_citrine2_OnRemoteStart( userObjID, questID )
end

function sqc17_4704_monster_citrine2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc17_4704_monster_citrine2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4704, 1);
				api_quest_SetJournalStep(userObjID,4704, 1);
				api_quest_SetQuestStep(userObjID,4704, 1);
				npc_talk_index = "n1835_conscience_pope-1";
end

</VillageServer>

<GameServer>
function sqc17_4704_monster_citrine2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1835 then
		sqc17_4704_monster_citrine2_OnTalk_n1835_conscience_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1878 then
		sqc17_4704_monster_citrine2_OnTalk_n1878_weak_alicanto( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1835_conscience_pope--------------------------------------------------------------------------------
function sqc17_4704_monster_citrine2_OnTalk_n1835_conscience_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1835_conscience_pope-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1835_conscience_pope-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1835_conscience_pope-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1835_conscience_pope-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1835_conscience_pope-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, false);
			 end 

	end
	if npc_talk_index == "n1835_conscience_pope-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4704, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4704, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4704, 1);
				npc_talk_index = "n1835_conscience_pope-1";

	end
	if npc_talk_index == "n1835_conscience_pope-1-b" then 
	end
	if npc_talk_index == "n1835_conscience_pope-1-c" then 
	end
	if npc_talk_index == "n1835_conscience_pope-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1835_conscience_pope-3-b" then 
	end
	if npc_talk_index == "n1835_conscience_pope-3-c" then 
	end
	if npc_talk_index == "n1835_conscience_pope-3-d" then 
	end
	if npc_talk_index == "n1835_conscience_pope-3-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47040, questID, 1);
			 end 
	end
	if npc_talk_index == "n1835_conscience_pope-3-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4705, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4705, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4705, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1835_conscience_pope-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1835_conscience_pope-1", "sqc17_4705_monster_citrine3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1878_weak_alicanto--------------------------------------------------------------------------------
function sqc17_4704_monster_citrine2_OnTalk_n1878_weak_alicanto( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1878_weak_alicanto-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1878_weak_alicanto-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1878_weak_alicanto-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1878_weak_alicanto-2-b" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-c" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-d" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-e" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-f" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-g" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-h" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-i" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-j" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-2-k" then 
	end
	if npc_talk_index == "n1878_weak_alicanto-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4704_monster_citrine2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4704);
end

function sqc17_4704_monster_citrine2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4704);
end

function sqc17_4704_monster_citrine2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4704);
	local questID=4704;
end

function sqc17_4704_monster_citrine2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc17_4704_monster_citrine2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc17_4704_monster_citrine2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4704, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4704, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4704, 1);
				npc_talk_index = "n1835_conscience_pope-1";
end

</GameServer>