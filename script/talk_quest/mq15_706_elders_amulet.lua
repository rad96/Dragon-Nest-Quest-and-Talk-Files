<VillageServer>

function mq15_706_elders_amulet_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 471 then
		mq15_706_elders_amulet_OnTalk_n471_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_706_elders_amulet_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n471_charty--------------------------------------------------------------------------------
function mq15_706_elders_amulet_OnTalk_n471_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n471_charty-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n471_charty-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n471_charty-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n471_charty-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n471_charty-1-b" then 
	end
	if npc_talk_index == "n471_charty-1-c" then 
	end
	if npc_talk_index == "n471_charty-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);

				if api_quest_HasQuestItem(userObjID, 300381, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300381, api_quest_HasQuestItem(userObjID, 300381, 1));
				end
	end
	if npc_talk_index == "n471_charty-2-b" then 
	end
	if npc_talk_index == "n471_charty-2-c" then 
	end
	if npc_talk_index == "n471_charty-2-d" then 
	end
	if npc_talk_index == "n471_charty-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_706_elders_amulet_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-b" then
				api_quest_AddQuest(userObjID,706, 2);
				api_quest_SetJournalStep(userObjID,706, 1);
				api_quest_SetQuestStep(userObjID,706, 1);

	end
	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-3-g" then 
	end
	if npc_talk_index == "n707_sidel-3-h" then 
	end
	if npc_talk_index == "n707_sidel-3-i" then 
	end
	if npc_talk_index == "n707_sidel-3-j" then 
	end
	if npc_talk_index == "n707_sidel-3-k" then 
	end
	if npc_talk_index == "n707_sidel-3-l" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7061, true);
				 api_quest_RewardQuestUser(userObjID, 7061, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7062, true);
				 api_quest_RewardQuestUser(userObjID, 7062, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7063, true);
				 api_quest_RewardQuestUser(userObjID, 7063, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7064, true);
				 api_quest_RewardQuestUser(userObjID, 7064, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7065, true);
				 api_quest_RewardQuestUser(userObjID, 7065, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7066, true);
				 api_quest_RewardQuestUser(userObjID, 7066, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7061, true);
				 api_quest_RewardQuestUser(userObjID, 7061, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7061, true);
				 api_quest_RewardQuestUser(userObjID, 7061, questID, 1);
			 end 
	end
	if npc_talk_index == "n707_sidel-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 707, 2);
					api_quest_SetQuestStep(userObjID, 707, 1);
					api_quest_SetJournalStep(userObjID, 707, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n707_sidel-1", "mq15_707_unkind_reunion.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_706_elders_amulet_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 706);
end

function mq15_706_elders_amulet_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 706);
end

function mq15_706_elders_amulet_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 706);
	local questID=706;
end

function mq15_706_elders_amulet_OnRemoteStart( userObjID, questID )
end

function mq15_706_elders_amulet_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_706_elders_amulet_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mq15_706_elders_amulet_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 471 then
		mq15_706_elders_amulet_OnTalk_n471_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_706_elders_amulet_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n471_charty--------------------------------------------------------------------------------
function mq15_706_elders_amulet_OnTalk_n471_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n471_charty-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n471_charty-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n471_charty-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n471_charty-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n471_charty-1-b" then 
	end
	if npc_talk_index == "n471_charty-1-c" then 
	end
	if npc_talk_index == "n471_charty-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);

				if api_quest_HasQuestItem( pRoom, userObjID, 300381, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300381, api_quest_HasQuestItem( pRoom, userObjID, 300381, 1));
				end
	end
	if npc_talk_index == "n471_charty-2-b" then 
	end
	if npc_talk_index == "n471_charty-2-c" then 
	end
	if npc_talk_index == "n471_charty-2-d" then 
	end
	if npc_talk_index == "n471_charty-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_706_elders_amulet_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-b" then
				api_quest_AddQuest( pRoom, userObjID,706, 2);
				api_quest_SetJournalStep( pRoom, userObjID,706, 1);
				api_quest_SetQuestStep( pRoom, userObjID,706, 1);

	end
	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-3-g" then 
	end
	if npc_talk_index == "n707_sidel-3-h" then 
	end
	if npc_talk_index == "n707_sidel-3-i" then 
	end
	if npc_talk_index == "n707_sidel-3-j" then 
	end
	if npc_talk_index == "n707_sidel-3-k" then 
	end
	if npc_talk_index == "n707_sidel-3-l" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7061, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7061, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7062, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7062, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7063, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7063, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7064, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7064, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7065, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7065, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7066, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7066, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7061, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7061, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7061, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7061, questID, 1);
			 end 
	end
	if npc_talk_index == "n707_sidel-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 707, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 707, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 707, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n707_sidel-1", "mq15_707_unkind_reunion.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_706_elders_amulet_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 706);
end

function mq15_706_elders_amulet_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 706);
end

function mq15_706_elders_amulet_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 706);
	local questID=706;
end

function mq15_706_elders_amulet_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_706_elders_amulet_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_706_elders_amulet_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>