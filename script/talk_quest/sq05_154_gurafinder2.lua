<VillageServer>

function sq05_154_gurafinder2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 11 then
		sq05_154_gurafinder2_OnTalk_n011_priest_leonardo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 12 then
		sq05_154_gurafinder2_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 16 then
		sq05_154_gurafinder2_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 17 then
		sq05_154_gurafinder2_OnTalk_n017_trader_jeny(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n011_priest_leonardo--------------------------------------------------------------------------------
function sq05_154_gurafinder2_OnTalk_n011_priest_leonardo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n011_priest_leonardo-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n011_priest_leonardo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n011_priest_leonardo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n011_priest_leonardo-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n011_priest_leonardo-2-b" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2-c" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2-d" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2-e" then 
	end
	if npc_talk_index == "n011_priest_leonardo-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function sq05_154_gurafinder2_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n012_sorceress_master_cynthia-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n012_sorceress_master_cynthia-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n012_sorceress_master_cynthia-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-6-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-6-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-6-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-6-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-6-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n016_trader_borin--------------------------------------------------------------------------------
function sq05_154_gurafinder2_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n016_trader_borin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n016_trader_borin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n016_trader_borin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n016_trader_borin-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n016_trader_borin-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n016_trader_borin-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1541, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1541, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1541, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1542, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1541, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1541, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1541, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1541, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1541, false);
			 end 

	end
	if npc_talk_index == "n016_trader_borin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,154, 1);
				api_quest_SetJournalStep(userObjID,154, 1);
				api_quest_SetQuestStep(userObjID,154, 1);
				npc_talk_index = "n016_trader_borin-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300104, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300104, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n016_trader_borin-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n016_trader_borin-3-b" then 
	end
	if npc_talk_index == "n016_trader_borin-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n016_trader_borin-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end
	if npc_talk_index == "n016_trader_borin-7-b" then 
	end
	if npc_talk_index == "n016_trader_borin-7-c" then 
	end
	if npc_talk_index == "n016_trader_borin-7-d" then 
	end
	if npc_talk_index == "n016_trader_borin-7-e" then 
	end
	if npc_talk_index == "n016_trader_borin-7-f" then 
	end
	if npc_talk_index == "n016_trader_borin-7-g" then 
	end
	if npc_talk_index == "n016_trader_borin-7-h" then 
	end
	if npc_talk_index == "n016_trader_borin-7-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1541, true);
				 api_quest_RewardQuestUser(userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1541, true);
				 api_quest_RewardQuestUser(userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1541, true);
				 api_quest_RewardQuestUser(userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1542, true);
				 api_quest_RewardQuestUser(userObjID, 1542, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1541, true);
				 api_quest_RewardQuestUser(userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1541, true);
				 api_quest_RewardQuestUser(userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1541, true);
				 api_quest_RewardQuestUser(userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1541, true);
				 api_quest_RewardQuestUser(userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1541, true);
				 api_quest_RewardQuestUser(userObjID, 1541, questID, 1);
			 end 
	end
	if npc_talk_index == "n016_trader_borin-7-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300104, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300104, api_quest_HasQuestItem(userObjID, 300104, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n017_trader_jeny--------------------------------------------------------------------------------
function sq05_154_gurafinder2_OnTalk_n017_trader_jeny(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n017_trader_jeny-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n017_trader_jeny-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n017_trader_jeny-4";
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n017_trader_jeny-4-a";

				else
									npc_talk_index = "n017_trader_jeny-4";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n017_trader_jeny-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n017_trader_jeny-4-b" then 
	end
	if npc_talk_index == "n017_trader_jeny-4-c" then 
	end
	if npc_talk_index == "n017_trader_jeny-4-d" then 
	end
	if npc_talk_index == "n017_trader_jeny-4-e" then 
	end
	if npc_talk_index == "n017_trader_jeny-4-f" then 
	end
	if npc_talk_index == "n017_trader_jeny-4-g" then 
	end
	if npc_talk_index == "n017_trader_jeny-4-h" then 
	end
	if npc_talk_index == "n017_trader_jeny-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_154_gurafinder2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 154);
end

function sq05_154_gurafinder2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 154);
end

function sq05_154_gurafinder2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 154);
	local questID=154;
end

function sq05_154_gurafinder2_OnRemoteStart( userObjID, questID )
end

function sq05_154_gurafinder2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq05_154_gurafinder2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,154, 1);
				api_quest_SetJournalStep(userObjID,154, 1);
				api_quest_SetQuestStep(userObjID,154, 1);
				npc_talk_index = "n016_trader_borin-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300104, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300104, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
end

</VillageServer>

<GameServer>
function sq05_154_gurafinder2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 11 then
		sq05_154_gurafinder2_OnTalk_n011_priest_leonardo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 12 then
		sq05_154_gurafinder2_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 16 then
		sq05_154_gurafinder2_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 17 then
		sq05_154_gurafinder2_OnTalk_n017_trader_jeny( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n011_priest_leonardo--------------------------------------------------------------------------------
function sq05_154_gurafinder2_OnTalk_n011_priest_leonardo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n011_priest_leonardo-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n011_priest_leonardo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n011_priest_leonardo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n011_priest_leonardo-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n011_priest_leonardo-2-b" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2-c" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2-d" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2-e" then 
	end
	if npc_talk_index == "n011_priest_leonardo-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function sq05_154_gurafinder2_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n012_sorceress_master_cynthia-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n012_sorceress_master_cynthia-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n012_sorceress_master_cynthia-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-6-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-6-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-6-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-6-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-6-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n016_trader_borin--------------------------------------------------------------------------------
function sq05_154_gurafinder2_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n016_trader_borin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n016_trader_borin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n016_trader_borin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n016_trader_borin-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n016_trader_borin-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n016_trader_borin-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1542, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, false);
			 end 

	end
	if npc_talk_index == "n016_trader_borin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,154, 1);
				api_quest_SetJournalStep( pRoom, userObjID,154, 1);
				api_quest_SetQuestStep( pRoom, userObjID,154, 1);
				npc_talk_index = "n016_trader_borin-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300104, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300104, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n016_trader_borin-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n016_trader_borin-3-b" then 
	end
	if npc_talk_index == "n016_trader_borin-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n016_trader_borin-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end
	if npc_talk_index == "n016_trader_borin-7-b" then 
	end
	if npc_talk_index == "n016_trader_borin-7-c" then 
	end
	if npc_talk_index == "n016_trader_borin-7-d" then 
	end
	if npc_talk_index == "n016_trader_borin-7-e" then 
	end
	if npc_talk_index == "n016_trader_borin-7-f" then 
	end
	if npc_talk_index == "n016_trader_borin-7-g" then 
	end
	if npc_talk_index == "n016_trader_borin-7-h" then 
	end
	if npc_talk_index == "n016_trader_borin-7-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1542, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1542, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1541, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1541, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1541, questID, 1);
			 end 
	end
	if npc_talk_index == "n016_trader_borin-7-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300104, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300104, api_quest_HasQuestItem( pRoom, userObjID, 300104, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n017_trader_jeny--------------------------------------------------------------------------------
function sq05_154_gurafinder2_OnTalk_n017_trader_jeny( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n017_trader_jeny-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n017_trader_jeny-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n017_trader_jeny-4";
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n017_trader_jeny-4-a";

				else
									npc_talk_index = "n017_trader_jeny-4";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n017_trader_jeny-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n017_trader_jeny-4-b" then 
	end
	if npc_talk_index == "n017_trader_jeny-4-c" then 
	end
	if npc_talk_index == "n017_trader_jeny-4-d" then 
	end
	if npc_talk_index == "n017_trader_jeny-4-e" then 
	end
	if npc_talk_index == "n017_trader_jeny-4-f" then 
	end
	if npc_talk_index == "n017_trader_jeny-4-g" then 
	end
	if npc_talk_index == "n017_trader_jeny-4-h" then 
	end
	if npc_talk_index == "n017_trader_jeny-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_154_gurafinder2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 154);
end

function sq05_154_gurafinder2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 154);
end

function sq05_154_gurafinder2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 154);
	local questID=154;
end

function sq05_154_gurafinder2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq05_154_gurafinder2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq05_154_gurafinder2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,154, 1);
				api_quest_SetJournalStep( pRoom, userObjID,154, 1);
				api_quest_SetQuestStep( pRoom, userObjID,154, 1);
				npc_talk_index = "n016_trader_borin-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300104, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300104, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
end

</GameServer>