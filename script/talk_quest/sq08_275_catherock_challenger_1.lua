<VillageServer>

function sq08_275_catherock_challenger_1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 32 then
		sq08_275_catherock_challenger_1_OnTalk_n032_warrior_master_chandler(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n032_warrior_master_chandler--------------------------------------------------------------------------------
function sq08_275_catherock_challenger_1_OnTalk_n032_warrior_master_chandler(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n032_warrior_master_chandler-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n032_warrior_master_chandler-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n032_warrior_master_chandler-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n032_warrior_master_chandler-accepting-k" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2751, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2752, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2753, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2754, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2755, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2756, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2757, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2758, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2759, false);
			 end 

	end
	if npc_talk_index == "n032_warrior_master_chandler-accepting-acceptted" then
				api_quest_AddQuest(userObjID,275, 1);
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 21074, 1);
				api_quest_SetJournalStep(userObjID, questID, 1);
				npc_talk_index = "n032_warrior_master_chandler-accepting-l";

	end
	if npc_talk_index == "n032_warrior_master_chandler-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2751, true);
				 api_quest_RewardQuestUser(userObjID, 2751, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2752, true);
				 api_quest_RewardQuestUser(userObjID, 2752, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2753, true);
				 api_quest_RewardQuestUser(userObjID, 2753, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2754, true);
				 api_quest_RewardQuestUser(userObjID, 2754, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2755, true);
				 api_quest_RewardQuestUser(userObjID, 2755, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2756, true);
				 api_quest_RewardQuestUser(userObjID, 2756, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2757, true);
				 api_quest_RewardQuestUser(userObjID, 2757, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2758, true);
				 api_quest_RewardQuestUser(userObjID, 2758, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2759, true);
				 api_quest_RewardQuestUser(userObjID, 2759, questID, 1);
			 end 
	end
	if npc_talk_index == "n032_warrior_master_chandler-2-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-2-d" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-2-e" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_275_catherock_challenger_1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 275);
	if qstep == 1 and CountIndex == 21074 then

	end
end

function sq08_275_catherock_challenger_1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 275);
	if qstep == 1 and CountIndex == 21074 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function sq08_275_catherock_challenger_1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 275);
	local questID=275;
end

function sq08_275_catherock_challenger_1_OnRemoteStart( userObjID, questID )
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 21074, 1);
				api_quest_SetJournalStep(userObjID, questID, 1);
end

function sq08_275_catherock_challenger_1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_275_catherock_challenger_1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,275, 1);
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 21074, 1);
				api_quest_SetJournalStep(userObjID, questID, 1);
				npc_talk_index = "n032_warrior_master_chandler-accepting-l";
end

</VillageServer>

<GameServer>
function sq08_275_catherock_challenger_1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 32 then
		sq08_275_catherock_challenger_1_OnTalk_n032_warrior_master_chandler( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n032_warrior_master_chandler--------------------------------------------------------------------------------
function sq08_275_catherock_challenger_1_OnTalk_n032_warrior_master_chandler( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n032_warrior_master_chandler-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n032_warrior_master_chandler-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n032_warrior_master_chandler-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n032_warrior_master_chandler-accepting-k" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2751, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2752, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2753, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2754, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2755, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2756, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2757, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2758, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2759, false);
			 end 

	end
	if npc_talk_index == "n032_warrior_master_chandler-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,275, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 21074, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				npc_talk_index = "n032_warrior_master_chandler-accepting-l";

	end
	if npc_talk_index == "n032_warrior_master_chandler-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2751, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2751, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2752, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2752, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2753, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2753, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2754, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2754, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2755, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2755, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2756, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2756, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2757, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2757, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2758, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2758, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2759, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2759, questID, 1);
			 end 
	end
	if npc_talk_index == "n032_warrior_master_chandler-2-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-2-d" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-2-e" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_275_catherock_challenger_1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 275);
	if qstep == 1 and CountIndex == 21074 then

	end
end

function sq08_275_catherock_challenger_1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 275);
	if qstep == 1 and CountIndex == 21074 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function sq08_275_catherock_challenger_1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 275);
	local questID=275;
end

function sq08_275_catherock_challenger_1_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 21074, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
end

function sq08_275_catherock_challenger_1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_275_catherock_challenger_1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,275, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 21074, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				npc_talk_index = "n032_warrior_master_chandler-accepting-l";
end

</GameServer>