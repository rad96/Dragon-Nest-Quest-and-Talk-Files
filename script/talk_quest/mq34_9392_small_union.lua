<VillageServer>

function mq34_9392_small_union_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1067 then
		mq34_9392_small_union_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1233 then
		mq34_9392_small_union_OnTalk_n1233_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1234 then
		mq34_9392_small_union_OnTalk_n1234_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mq34_9392_small_union_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9392_small_union_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9392, 2);
				api_quest_SetJournalStep(userObjID,9392, 1);
				api_quest_SetQuestStep(userObjID,9392, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1233_white_dragon--------------------------------------------------------------------------------
function mq34_9392_small_union_OnTalk_n1233_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1233_white_dragon-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1233_white_dragon-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1233_white_dragon-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1233_white_dragon-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1233_white_dragon-2-class_check" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1233_white_dragon-2-a";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1233_white_dragon-2-b";

				else
									npc_talk_index = "n1233_white_dragon-2-c";

				end

				end
	end
	if npc_talk_index == "n1233_white_dragon-2-v" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-q" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-class_check_1" then 
				if api_user_GetUserClassID(userObjID) == 4 then
									npc_talk_index = "n1233_white_dragon-2-f";

				else
									if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n1233_white_dragon-2-g";

				else
									npc_talk_index = "n1233_white_dragon-2-e";

				end

				end
	end
	if npc_talk_index == "n1233_white_dragon-2-k" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-k" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1233_white_dragon-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1233_white_dragon-2-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-n" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-p" then 
	end
	if npc_talk_index == "n1233_white_dragon-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1233_white_dragon-2-r" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-s" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-t" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-u" then 
	end
	if npc_talk_index == "n1233_white_dragon-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1233_white_dragon-2-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-q" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-b" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-k" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1234_white_dragon--------------------------------------------------------------------------------
function mq34_9392_small_union_OnTalk_n1234_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1234_white_dragon-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1234_white_dragon-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1234_white_dragon-5-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93920, true);
				 api_quest_RewardQuestUser(userObjID, 93920, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9393, 2);
					api_quest_SetQuestStep(userObjID, 9393, 1);
					api_quest_SetJournalStep(userObjID, 9393, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400417, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400417, api_quest_HasQuestItem(userObjID, 400417, 1));
				end
	end
	if npc_talk_index == "n1234_white_dragon-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1234_white_dragon-1", "mq34_9393_the_beginning_of_legend.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mq34_9392_small_union_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n088_scholar_starshy-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n088_scholar_starshy-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-4-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400417, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400417, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9392_small_union_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9392);
end

function mq34_9392_small_union_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9392);
end

function mq34_9392_small_union_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9392);
	local questID=9392;
end

function mq34_9392_small_union_OnRemoteStart( userObjID, questID )
end

function mq34_9392_small_union_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9392_small_union_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9392, 2);
				api_quest_SetJournalStep(userObjID,9392, 1);
				api_quest_SetQuestStep(userObjID,9392, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</VillageServer>

<GameServer>
function mq34_9392_small_union_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1067 then
		mq34_9392_small_union_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1233 then
		mq34_9392_small_union_OnTalk_n1233_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1234 then
		mq34_9392_small_union_OnTalk_n1234_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mq34_9392_small_union_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9392_small_union_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9392, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9392, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9392, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1233_white_dragon--------------------------------------------------------------------------------
function mq34_9392_small_union_OnTalk_n1233_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1233_white_dragon-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1233_white_dragon-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1233_white_dragon-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1233_white_dragon-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1233_white_dragon-2-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1233_white_dragon-2-a";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1233_white_dragon-2-b";

				else
									npc_talk_index = "n1233_white_dragon-2-c";

				end

				end
	end
	if npc_talk_index == "n1233_white_dragon-2-v" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-q" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-class_check_1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									npc_talk_index = "n1233_white_dragon-2-f";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n1233_white_dragon-2-g";

				else
									npc_talk_index = "n1233_white_dragon-2-e";

				end

				end
	end
	if npc_talk_index == "n1233_white_dragon-2-k" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-k" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1233_white_dragon-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1233_white_dragon-2-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-n" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-p" then 
	end
	if npc_talk_index == "n1233_white_dragon-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1233_white_dragon-2-r" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-s" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-t" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-u" then 
	end
	if npc_talk_index == "n1233_white_dragon-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1233_white_dragon-2-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-2-q" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-b" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-k" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1234_white_dragon--------------------------------------------------------------------------------
function mq34_9392_small_union_OnTalk_n1234_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1234_white_dragon-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1234_white_dragon-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1234_white_dragon-5-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93920, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93920, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9393, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9393, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9393, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400417, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400417, api_quest_HasQuestItem( pRoom, userObjID, 400417, 1));
				end
	end
	if npc_talk_index == "n1234_white_dragon-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1234_white_dragon-1", "mq34_9393_the_beginning_of_legend.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mq34_9392_small_union_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n088_scholar_starshy-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n088_scholar_starshy-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-4-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400417, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400417, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9392_small_union_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9392);
end

function mq34_9392_small_union_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9392);
end

function mq34_9392_small_union_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9392);
	local questID=9392;
end

function mq34_9392_small_union_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9392_small_union_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9392_small_union_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9392, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9392, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9392, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</GameServer>