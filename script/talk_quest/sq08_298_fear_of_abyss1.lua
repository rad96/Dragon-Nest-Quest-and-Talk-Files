<VillageServer>

function sq08_298_fear_of_abyss1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 28 then
		sq08_298_fear_of_abyss1_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_298_fear_of_abyss1_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2981, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2982, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2983, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2984, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2985, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2986, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2987, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2987, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2987, false);
			 end 

	end
	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,298, 1);
				api_quest_SetJournalStep(userObjID,298, 1);
				api_quest_SetQuestStep(userObjID,298, 1);
				npc_talk_index = "n028_scholar_bailey-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300127, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300128, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300130, 1);
				api_quest_SetCountingInfo(userObjID, questID, 3, 3, 300131, 1);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200472, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200492, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 200510, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 200559, 30000);

	end
	if npc_talk_index == "n028_scholar_bailey-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2981, true);
				 api_quest_RewardQuestUser(userObjID, 2981, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2982, true);
				 api_quest_RewardQuestUser(userObjID, 2982, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2983, true);
				 api_quest_RewardQuestUser(userObjID, 2983, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2984, true);
				 api_quest_RewardQuestUser(userObjID, 2984, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2985, true);
				 api_quest_RewardQuestUser(userObjID, 2985, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2986, true);
				 api_quest_RewardQuestUser(userObjID, 2986, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2987, true);
				 api_quest_RewardQuestUser(userObjID, 2987, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2987, true);
				 api_quest_RewardQuestUser(userObjID, 2987, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2987, true);
				 api_quest_RewardQuestUser(userObjID, 2987, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-2-c" then 

				if api_quest_HasQuestItem(userObjID, 300127, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300127, api_quest_HasQuestItem(userObjID, 300127, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300128, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300128, api_quest_HasQuestItem(userObjID, 300128, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300130, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300130, api_quest_HasQuestItem(userObjID, 300130, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300131, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300131, api_quest_HasQuestItem(userObjID, 300131, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 299, 1);
					api_quest_SetQuestStep(userObjID, 299, 1);
					api_quest_SetJournalStep(userObjID, 299, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n028_scholar_bailey-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n028_scholar_bailey-1", "sq08_299_fear_of_abyss2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_298_fear_of_abyss1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 298);
	if qstep == 1 and CountIndex == 200472 then
				if api_quest_HasQuestItem(userObjID, 300127, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300127, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300127, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 200492 then
				if api_quest_HasQuestItem(userObjID, 300128, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300128, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300128, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 200510 then
				if api_quest_HasQuestItem(userObjID, 300130, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300130, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300130, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 200559 then
				if api_quest_HasQuestItem(userObjID, 300131, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300131, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300131, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 300127 then

	end
	if qstep == 1 and CountIndex == 300128 then

	end
	if qstep == 1 and CountIndex == 300130 then

	end
	if qstep == 1 and CountIndex == 300131 then

	end
end

function sq08_298_fear_of_abyss1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 298);
	if qstep == 1 and CountIndex == 200472 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200492 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200510 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200559 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300127 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300128 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300130 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300131 and Count >= TargetCount  then

	end
end

function sq08_298_fear_of_abyss1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 298);
	local questID=298;
end

function sq08_298_fear_of_abyss1_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300127, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300128, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300130, 1);
				api_quest_SetCountingInfo(userObjID, questID, 3, 3, 300131, 1);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200472, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200492, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 200510, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 200559, 30000);
end

function sq08_298_fear_of_abyss1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_298_fear_of_abyss1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,298, 1);
				api_quest_SetJournalStep(userObjID,298, 1);
				api_quest_SetQuestStep(userObjID,298, 1);
				npc_talk_index = "n028_scholar_bailey-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300127, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300128, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300130, 1);
				api_quest_SetCountingInfo(userObjID, questID, 3, 3, 300131, 1);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200472, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200492, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 200510, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 200559, 30000);
end

</VillageServer>

<GameServer>
function sq08_298_fear_of_abyss1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 28 then
		sq08_298_fear_of_abyss1_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_298_fear_of_abyss1_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2981, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2982, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2983, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2984, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2985, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2986, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2987, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2987, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2987, false);
			 end 

	end
	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,298, 1);
				api_quest_SetJournalStep( pRoom, userObjID,298, 1);
				api_quest_SetQuestStep( pRoom, userObjID,298, 1);
				npc_talk_index = "n028_scholar_bailey-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300127, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300128, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300130, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 3, 300131, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200472, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200492, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 200510, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 200559, 30000);

	end
	if npc_talk_index == "n028_scholar_bailey-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2981, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2981, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2982, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2982, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2983, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2983, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2984, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2984, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2985, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2985, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2986, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2986, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2987, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2987, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2987, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2987, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2987, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2987, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-2-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300127, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300127, api_quest_HasQuestItem( pRoom, userObjID, 300127, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300128, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300128, api_quest_HasQuestItem( pRoom, userObjID, 300128, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300130, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300130, api_quest_HasQuestItem( pRoom, userObjID, 300130, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300131, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300131, api_quest_HasQuestItem( pRoom, userObjID, 300131, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 299, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 299, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 299, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n028_scholar_bailey-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n028_scholar_bailey-1", "sq08_299_fear_of_abyss2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_298_fear_of_abyss1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 298);
	if qstep == 1 and CountIndex == 200472 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300127, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300127, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300127, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 200492 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300128, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300128, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300128, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 200510 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300130, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300130, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300130, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 200559 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300131, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300131, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300131, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300127, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300128, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300130, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300131, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 300127 then

	end
	if qstep == 1 and CountIndex == 300128 then

	end
	if qstep == 1 and CountIndex == 300130 then

	end
	if qstep == 1 and CountIndex == 300131 then

	end
end

function sq08_298_fear_of_abyss1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 298);
	if qstep == 1 and CountIndex == 200472 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200492 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200510 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200559 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300127 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300128 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300130 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300131 and Count >= TargetCount  then

	end
end

function sq08_298_fear_of_abyss1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 298);
	local questID=298;
end

function sq08_298_fear_of_abyss1_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300127, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300128, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300130, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 3, 300131, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200472, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200492, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 200510, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 200559, 30000);
end

function sq08_298_fear_of_abyss1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_298_fear_of_abyss1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,298, 1);
				api_quest_SetJournalStep( pRoom, userObjID,298, 1);
				api_quest_SetQuestStep( pRoom, userObjID,298, 1);
				npc_talk_index = "n028_scholar_bailey-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300127, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300128, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300130, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 3, 300131, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200472, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200492, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 200510, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 200559, 30000);
end

</GameServer>