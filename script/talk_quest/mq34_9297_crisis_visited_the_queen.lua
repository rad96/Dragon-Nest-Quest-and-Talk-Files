<VillageServer>

function mq34_9297_crisis_visited_the_queen_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1066 then
		mq34_9297_crisis_visited_the_queen_OnTalk_n1066_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mq34_9297_crisis_visited_the_queen_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1077 then
		mq34_9297_crisis_visited_the_queen_OnTalk_n1077_elf_guard_basha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1078 then
		mq34_9297_crisis_visited_the_queen_OnTalk_n1078_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1066_geraint_kid--------------------------------------------------------------------------------
function mq34_9297_crisis_visited_the_queen_OnTalk_n1066_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1066_geraint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9297_crisis_visited_the_queen_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1067_elder_elf-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9297, 2);
				api_quest_SetJournalStep(userObjID,9297, 1);
				api_quest_SetQuestStep(userObjID,9297, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1067_elder_elf-4-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-4-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92971, true);
				 api_quest_RewardQuestUser(userObjID, 92971, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92971, true);
				 api_quest_RewardQuestUser(userObjID, 92971, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92973, true);
				 api_quest_RewardQuestUser(userObjID, 92973, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92974, true);
				 api_quest_RewardQuestUser(userObjID, 92974, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92975, true);
				 api_quest_RewardQuestUser(userObjID, 92975, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92976, true);
				 api_quest_RewardQuestUser(userObjID, 92976, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92977, true);
				 api_quest_RewardQuestUser(userObjID, 92977, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92978, true);
				 api_quest_RewardQuestUser(userObjID, 92978, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92979, true);
				 api_quest_RewardQuestUser(userObjID, 92979, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9298, 2);
					api_quest_SetQuestStep(userObjID, 9298, 1);
					api_quest_SetJournalStep(userObjID, 9298, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1067_elder_elf-1", "mq34_9298_the_power_of_the_prophet.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1077_elf_guard_basha--------------------------------------------------------------------------------
function mq34_9297_crisis_visited_the_queen_OnTalk_n1077_elf_guard_basha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1077_elf_guard_basha-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1077_elf_guard_basha-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1077_elf_guard_basha-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1077_elf_guard_basha-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1077_elf_guard_basha-2-job2_2" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1077_elf_guard_basha-2-a";

				else
									npc_talk_index = "n1077_elf_guard_basha-2-b";

				end
	end
	if npc_talk_index == "n1077_elf_guard_basha-2-c" then 
	end
	if npc_talk_index == "n1077_elf_guard_basha-2-c" then 
	end
	if npc_talk_index == "n1077_elf_guard_basha-2-d" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1078_argenta--------------------------------------------------------------------------------
function mq34_9297_crisis_visited_the_queen_OnTalk_n1078_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1078_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1078_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1078_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1078_argenta-3-b" then 
	end
	if npc_talk_index == "n1078_argenta-3-c" then 
	end
	if npc_talk_index == "n1078_argenta-3-d" then 
	end
	if npc_talk_index == "n1078_argenta-3-e" then 
	end
	if npc_talk_index == "n1078_argenta-3-f" then 
	end
	if npc_talk_index == "n1078_argenta-3-g" then 
	end
	if npc_talk_index == "n1078_argenta-3-h" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9297_crisis_visited_the_queen_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9297);
end

function mq34_9297_crisis_visited_the_queen_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9297);
end

function mq34_9297_crisis_visited_the_queen_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9297);
	local questID=9297;
end

function mq34_9297_crisis_visited_the_queen_OnRemoteStart( userObjID, questID )
end

function mq34_9297_crisis_visited_the_queen_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9297_crisis_visited_the_queen_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9297, 2);
				api_quest_SetJournalStep(userObjID,9297, 1);
				api_quest_SetQuestStep(userObjID,9297, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</VillageServer>

<GameServer>
function mq34_9297_crisis_visited_the_queen_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1066 then
		mq34_9297_crisis_visited_the_queen_OnTalk_n1066_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mq34_9297_crisis_visited_the_queen_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1077 then
		mq34_9297_crisis_visited_the_queen_OnTalk_n1077_elf_guard_basha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1078 then
		mq34_9297_crisis_visited_the_queen_OnTalk_n1078_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1066_geraint_kid--------------------------------------------------------------------------------
function mq34_9297_crisis_visited_the_queen_OnTalk_n1066_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1066_geraint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9297_crisis_visited_the_queen_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1067_elder_elf-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9297, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9297, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9297, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1067_elder_elf-4-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-4-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92971, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92971, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92971, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92971, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92973, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92973, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92974, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92974, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92975, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92975, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92976, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92976, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92977, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92977, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92978, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92978, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92979, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92979, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9298, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9298, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9298, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1067_elder_elf-1", "mq34_9298_the_power_of_the_prophet.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1077_elf_guard_basha--------------------------------------------------------------------------------
function mq34_9297_crisis_visited_the_queen_OnTalk_n1077_elf_guard_basha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1077_elf_guard_basha-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1077_elf_guard_basha-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1077_elf_guard_basha-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1077_elf_guard_basha-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1077_elf_guard_basha-2-job2_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1077_elf_guard_basha-2-a";

				else
									npc_talk_index = "n1077_elf_guard_basha-2-b";

				end
	end
	if npc_talk_index == "n1077_elf_guard_basha-2-c" then 
	end
	if npc_talk_index == "n1077_elf_guard_basha-2-c" then 
	end
	if npc_talk_index == "n1077_elf_guard_basha-2-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1078_argenta--------------------------------------------------------------------------------
function mq34_9297_crisis_visited_the_queen_OnTalk_n1078_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1078_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1078_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1078_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1078_argenta-3-b" then 
	end
	if npc_talk_index == "n1078_argenta-3-c" then 
	end
	if npc_talk_index == "n1078_argenta-3-d" then 
	end
	if npc_talk_index == "n1078_argenta-3-e" then 
	end
	if npc_talk_index == "n1078_argenta-3-f" then 
	end
	if npc_talk_index == "n1078_argenta-3-g" then 
	end
	if npc_talk_index == "n1078_argenta-3-h" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9297_crisis_visited_the_queen_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9297);
end

function mq34_9297_crisis_visited_the_queen_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9297);
end

function mq34_9297_crisis_visited_the_queen_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9297);
	local questID=9297;
end

function mq34_9297_crisis_visited_the_queen_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9297_crisis_visited_the_queen_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9297_crisis_visited_the_queen_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9297, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9297, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9297, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</GameServer>