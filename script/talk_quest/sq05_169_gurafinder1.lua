<VillageServer>

function sq05_169_gurafinder1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 16 then
		sq05_169_gurafinder1_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 391 then
		sq05_169_gurafinder1_OnTalk_n391_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n016_trader_borin--------------------------------------------------------------------------------
function sq05_169_gurafinder1_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n016_trader_borin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-accepting-j" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1690, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1690, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1690, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1690, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1690, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1690, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1690, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1690, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1690, false);
			 end 

	end
	if npc_talk_index == "n016_trader_borin-accepting-acceptted" then
				npc_talk_index = "n016_trader_borin-1";
				api_quest_AddQuest(userObjID,169, 1);
				api_quest_SetJournalStep(userObjID,169, 1);
				api_quest_SetQuestStep(userObjID,169, 1);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300104, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300104, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n016_trader_borin-2-b" then 
	end
	if npc_talk_index == "n016_trader_borin-2-d" then 
	end
	if npc_talk_index == "n016_trader_borin-2-c" then 
	end
	if npc_talk_index == "n016_trader_borin-2-e" then 
	end
	if npc_talk_index == "n016_trader_borin-2-f" then 
	end
	if npc_talk_index == "n016_trader_borin-2-g" then 
	end
	if npc_talk_index == "n016_trader_borin-2-g" then 
	end
	if npc_talk_index == "n016_trader_borin-2-i" then 
	end
	if npc_talk_index == "n016_trader_borin-2-h" then 
	end
	if npc_talk_index == "n016_trader_borin-2-j" then 
	end
	if npc_talk_index == "n016_trader_borin-2-j" then 
	end
	if npc_talk_index == "n016_trader_borin-2-k" then 
	end
	if npc_talk_index == "n016_trader_borin-2-l" then 
	end
	if npc_talk_index == "n016_trader_borin-2-m" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1690, true);
				 api_quest_RewardQuestUser(userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1690, true);
				 api_quest_RewardQuestUser(userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1690, true);
				 api_quest_RewardQuestUser(userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1690, true);
				 api_quest_RewardQuestUser(userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1690, true);
				 api_quest_RewardQuestUser(userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1690, true);
				 api_quest_RewardQuestUser(userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1690, true);
				 api_quest_RewardQuestUser(userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1690, true);
				 api_quest_RewardQuestUser(userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1690, true);
				 api_quest_RewardQuestUser(userObjID, 1690, questID, 1);
			 end 
	end
	if npc_talk_index == "n016_trader_borin-2-n" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300104, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300104, api_quest_HasQuestItem(userObjID, 300104, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n391_academic_station--------------------------------------------------------------------------------
function sq05_169_gurafinder1_OnTalk_n391_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n391_academic_station-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n391_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n391_academic_station-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n391_academic_station-1" then 
	end
	if npc_talk_index == "n391_academic_station-1-c" then 
	end
	if npc_talk_index == "n391_academic_station-1-f" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n391_academic_station-1-e" then 
	end
	if npc_talk_index == "n391_academic_station-1" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_169_gurafinder1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 169);
end

function sq05_169_gurafinder1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 169);
end

function sq05_169_gurafinder1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 169);
	local questID=169;
end

function sq05_169_gurafinder1_OnRemoteStart( userObjID, questID )
end

function sq05_169_gurafinder1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq05_169_gurafinder1_ForceAccept( userObjID, npcObjID, questID )
				npc_talk_index = "n016_trader_borin-1";
				api_quest_AddQuest(userObjID,169, 1);
				api_quest_SetJournalStep(userObjID,169, 1);
				api_quest_SetQuestStep(userObjID,169, 1);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300104, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300104, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
end

</VillageServer>

<GameServer>
function sq05_169_gurafinder1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 16 then
		sq05_169_gurafinder1_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 391 then
		sq05_169_gurafinder1_OnTalk_n391_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n016_trader_borin--------------------------------------------------------------------------------
function sq05_169_gurafinder1_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n016_trader_borin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-accepting-j" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, false);
			 end 

	end
	if npc_talk_index == "n016_trader_borin-accepting-acceptted" then
				npc_talk_index = "n016_trader_borin-1";
				api_quest_AddQuest( pRoom, userObjID,169, 1);
				api_quest_SetJournalStep( pRoom, userObjID,169, 1);
				api_quest_SetQuestStep( pRoom, userObjID,169, 1);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300104, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300104, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n016_trader_borin-2-b" then 
	end
	if npc_talk_index == "n016_trader_borin-2-d" then 
	end
	if npc_talk_index == "n016_trader_borin-2-c" then 
	end
	if npc_talk_index == "n016_trader_borin-2-e" then 
	end
	if npc_talk_index == "n016_trader_borin-2-f" then 
	end
	if npc_talk_index == "n016_trader_borin-2-g" then 
	end
	if npc_talk_index == "n016_trader_borin-2-g" then 
	end
	if npc_talk_index == "n016_trader_borin-2-i" then 
	end
	if npc_talk_index == "n016_trader_borin-2-h" then 
	end
	if npc_talk_index == "n016_trader_borin-2-j" then 
	end
	if npc_talk_index == "n016_trader_borin-2-j" then 
	end
	if npc_talk_index == "n016_trader_borin-2-k" then 
	end
	if npc_talk_index == "n016_trader_borin-2-l" then 
	end
	if npc_talk_index == "n016_trader_borin-2-m" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1690, questID, 1);
			 end 
	end
	if npc_talk_index == "n016_trader_borin-2-n" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300104, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300104, api_quest_HasQuestItem( pRoom, userObjID, 300104, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n391_academic_station--------------------------------------------------------------------------------
function sq05_169_gurafinder1_OnTalk_n391_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n391_academic_station-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n391_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n391_academic_station-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n391_academic_station-1" then 
	end
	if npc_talk_index == "n391_academic_station-1-c" then 
	end
	if npc_talk_index == "n391_academic_station-1-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n391_academic_station-1-e" then 
	end
	if npc_talk_index == "n391_academic_station-1" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_169_gurafinder1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 169);
end

function sq05_169_gurafinder1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 169);
end

function sq05_169_gurafinder1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 169);
	local questID=169;
end

function sq05_169_gurafinder1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq05_169_gurafinder1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq05_169_gurafinder1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				npc_talk_index = "n016_trader_borin-1";
				api_quest_AddQuest( pRoom, userObjID,169, 1);
				api_quest_SetJournalStep( pRoom, userObjID,169, 1);
				api_quest_SetQuestStep( pRoom, userObjID,169, 1);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300104, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300104, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
end

</GameServer>