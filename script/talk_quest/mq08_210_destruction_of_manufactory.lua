<VillageServer>

function mq08_210_destruction_of_manufactory_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 164 then
		mq08_210_destruction_of_manufactory_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function mq08_210_destruction_of_manufactory_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n164_argenta_wounded-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n164_argenta_wounded-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n164_argenta_wounded-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-accepting-acceptted" then
				api_quest_AddQuest(userObjID,210, 2);
				api_quest_SetJournalStep(userObjID,210, 1);
				api_quest_SetQuestStep(userObjID,210, 1);
				npc_talk_index = "n164_argenta_wounded-1";

	end
	if npc_talk_index == "n164_argenta_wounded-1-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-d" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-e" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-f" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-g" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n164_argenta_wounded-1-a" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2101, true);
				 api_quest_RewardQuestUser(userObjID, 2101, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2102, true);
				 api_quest_RewardQuestUser(userObjID, 2102, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2103, true);
				 api_quest_RewardQuestUser(userObjID, 2103, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2104, true);
				 api_quest_RewardQuestUser(userObjID, 2104, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2105, true);
				 api_quest_RewardQuestUser(userObjID, 2105, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2105, true);
				 api_quest_RewardQuestUser(userObjID, 2105, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2101, true);
				 api_quest_RewardQuestUser(userObjID, 2101, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2101, true);
				 api_quest_RewardQuestUser(userObjID, 2101, questID, 1);
			 end 
	end
	if npc_talk_index == "n164_argenta_wounded-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 211, 2);
					api_quest_SetQuestStep(userObjID, 211, 1);
					api_quest_SetJournalStep(userObjID, 211, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n164_argenta_wounded-1", "mq08_211_red_army.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_210_destruction_of_manufactory_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 210);
end

function mq08_210_destruction_of_manufactory_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 210);
end

function mq08_210_destruction_of_manufactory_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 210);
	local questID=210;
end

function mq08_210_destruction_of_manufactory_OnRemoteStart( userObjID, questID )
end

function mq08_210_destruction_of_manufactory_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq08_210_destruction_of_manufactory_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,210, 2);
				api_quest_SetJournalStep(userObjID,210, 1);
				api_quest_SetQuestStep(userObjID,210, 1);
				npc_talk_index = "n164_argenta_wounded-1";
end

</VillageServer>

<GameServer>
function mq08_210_destruction_of_manufactory_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 164 then
		mq08_210_destruction_of_manufactory_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function mq08_210_destruction_of_manufactory_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n164_argenta_wounded-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n164_argenta_wounded-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n164_argenta_wounded-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,210, 2);
				api_quest_SetJournalStep( pRoom, userObjID,210, 1);
				api_quest_SetQuestStep( pRoom, userObjID,210, 1);
				npc_talk_index = "n164_argenta_wounded-1";

	end
	if npc_talk_index == "n164_argenta_wounded-1-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-d" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-e" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-f" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-g" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n164_argenta_wounded-1-a" then 
	end
	if npc_talk_index == "n164_argenta_wounded-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2101, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2101, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2102, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2102, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2103, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2103, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2104, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2104, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2105, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2105, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2105, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2105, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2101, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2101, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2101, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2101, questID, 1);
			 end 
	end
	if npc_talk_index == "n164_argenta_wounded-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 211, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 211, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 211, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n164_argenta_wounded-1", "mq08_211_red_army.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_210_destruction_of_manufactory_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 210);
end

function mq08_210_destruction_of_manufactory_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 210);
end

function mq08_210_destruction_of_manufactory_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 210);
	local questID=210;
end

function mq08_210_destruction_of_manufactory_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq08_210_destruction_of_manufactory_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq08_210_destruction_of_manufactory_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,210, 2);
				api_quest_SetJournalStep( pRoom, userObjID,210, 1);
				api_quest_SetQuestStep( pRoom, userObjID,210, 1);
				npc_talk_index = "n164_argenta_wounded-1";
end

</GameServer>