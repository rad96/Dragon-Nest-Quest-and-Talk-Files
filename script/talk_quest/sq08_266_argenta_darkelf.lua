<VillageServer>

function sq08_266_argenta_darkelf_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 164 then
		sq08_266_argenta_darkelf_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 190 then
		sq08_266_argenta_darkelf_OnTalk_n190_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 191 then
		sq08_266_argenta_darkelf_OnTalk_n191_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function sq08_266_argenta_darkelf_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n164_argenta_wounded-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n164_argenta_wounded-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2661, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2662, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2663, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2664, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2665, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2666, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2667, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2668, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2669, false);
			 end 

	end
	if npc_talk_index == "n164_argenta_wounded-accepting-acceptted" then
				api_quest_AddQuest(userObjID,266, 1);
				api_quest_SetJournalStep(userObjID,266, 1);
				api_quest_SetQuestStep(userObjID,266, 1);
				npc_talk_index = "n164_argenta_wounded-1";

	end
	if npc_talk_index == "n164_argenta_wounded-8-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-8-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-8-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2661, true);
				 api_quest_RewardQuestUser(userObjID, 2661, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2662, true);
				 api_quest_RewardQuestUser(userObjID, 2662, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2663, true);
				 api_quest_RewardQuestUser(userObjID, 2663, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2664, true);
				 api_quest_RewardQuestUser(userObjID, 2664, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2665, true);
				 api_quest_RewardQuestUser(userObjID, 2665, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2666, true);
				 api_quest_RewardQuestUser(userObjID, 2666, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2667, true);
				 api_quest_RewardQuestUser(userObjID, 2667, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2668, true);
				 api_quest_RewardQuestUser(userObjID, 2668, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2669, true);
				 api_quest_RewardQuestUser(userObjID, 2669, questID, 1);
			 end 
	end
	if npc_talk_index == "n164_argenta_wounded-8-e" then 

				if api_quest_HasQuestItem(userObjID, 300115, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300115, api_quest_HasQuestItem(userObjID, 300115, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 267, 1);
					api_quest_SetQuestStep(userObjID, 267, 1);
					api_quest_SetJournalStep(userObjID, 267, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n164_argenta_wounded-8-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n164_argenta_wounded-1", "sq08_267_crisis_of_david.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n190_slave_david--------------------------------------------------------------------------------
function sq08_266_argenta_darkelf_OnTalk_n190_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n190_slave_david-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n190_slave_david-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n190_slave_david-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n190_slave_david-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n190_slave_david-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n190_slave_david-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n190_slave_david-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n190_slave_david-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n190_slave_david-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n190_slave_david-1-b" then 
	end
	if npc_talk_index == "n190_slave_david-1-c" then 
	end
	if npc_talk_index == "n190_slave_david-1-d" then 
	end
	if npc_talk_index == "n190_slave_david-1-e" then 
	end
	if npc_talk_index == "n190_slave_david-1-f" then 
	end
	if npc_talk_index == "n190_slave_david-1-g" then 
	end
	if npc_talk_index == "n190_slave_david-1-h" then 
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n190_slave_david-3-b" then 
	end
	if npc_talk_index == "n190_slave_david-3-c" then 
	end
	if npc_talk_index == "n190_slave_david-3-d" then 
	end
	if npc_talk_index == "n190_slave_david-3-e" then 
	end
	if npc_talk_index == "n190_slave_david-3-f" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n190_slave_david-5-b" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 612, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200612, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300115, 1);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 608, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200608, 30000);
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n190_slave_david-7-b" then 
	end
	if npc_talk_index == "n190_slave_david-7-c" then 
	end
	if npc_talk_index == "n190_slave_david-7-david89" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n190_slave_david-7-d";

				else
									if api_user_GetUserClassID(userObjID) == 2 then
									npc_talk_index = "n190_slave_david-7-e";

				else
									if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n190_slave_david-7-g";

				else
									if api_user_GetUserClassID(userObjID) == 4 then
									npc_talk_index = "n190_slave_david-7-f";

				else
									if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n190_slave_david-7-i";

				else
									if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n190_slave_david-7-j";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n190_slave_david-7-k";

				else
									npc_talk_index = "n190_slave_david-7-k";

				end

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n190_slave_david-7-david90" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end
	if npc_talk_index == "n190_slave_david-7-david91" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end
	if npc_talk_index == "n190_slave_david-7-david92" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end
	if npc_talk_index == "n190_slave_david-7-david93" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end
	if npc_talk_index == "n190_slave_david-7-david95" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end
	if npc_talk_index == "n190_slave_david-7-david96" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end
	if npc_talk_index == "n190_slave_david-7-david97" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n191_slave_david--------------------------------------------------------------------------------
function sq08_266_argenta_darkelf_OnTalk_n191_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n191_slave_david-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n191_slave_david-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n191_slave_david-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_266_argenta_darkelf_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 266);
	if qstep == 6 and CountIndex == 612 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300115, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300115, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 6 and CountIndex == 200612 then
				api_quest_IncCounting(userObjID, 2, 612);

	end
	if qstep == 6 and CountIndex == 300115 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 5);

	end
	if qstep == 6 and CountIndex == 608 then
				api_quest_IncCounting(userObjID, 2, 612);

	end
	if qstep == 6 and CountIndex == 200608 then
				api_quest_IncCounting(userObjID, 2, 612);

	end
end

function sq08_266_argenta_darkelf_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 266);
	if qstep == 6 and CountIndex == 612 and Count >= TargetCount  then

	end
	if qstep == 6 and CountIndex == 200612 and Count >= TargetCount  then

	end
	if qstep == 6 and CountIndex == 300115 and Count >= TargetCount  then

	end
	if qstep == 6 and CountIndex == 608 and Count >= TargetCount  then

	end
	if qstep == 6 and CountIndex == 200608 and Count >= TargetCount  then

	end
end

function sq08_266_argenta_darkelf_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 266);
	local questID=266;
end

function sq08_266_argenta_darkelf_OnRemoteStart( userObjID, questID )
end

function sq08_266_argenta_darkelf_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_266_argenta_darkelf_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,266, 1);
				api_quest_SetJournalStep(userObjID,266, 1);
				api_quest_SetQuestStep(userObjID,266, 1);
				npc_talk_index = "n164_argenta_wounded-1";
end

</VillageServer>

<GameServer>
function sq08_266_argenta_darkelf_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 164 then
		sq08_266_argenta_darkelf_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 190 then
		sq08_266_argenta_darkelf_OnTalk_n190_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 191 then
		sq08_266_argenta_darkelf_OnTalk_n191_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function sq08_266_argenta_darkelf_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n164_argenta_wounded-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n164_argenta_wounded-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2661, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2662, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2663, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2664, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2665, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2666, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2667, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2668, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2669, false);
			 end 

	end
	if npc_talk_index == "n164_argenta_wounded-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,266, 1);
				api_quest_SetJournalStep( pRoom, userObjID,266, 1);
				api_quest_SetQuestStep( pRoom, userObjID,266, 1);
				npc_talk_index = "n164_argenta_wounded-1";

	end
	if npc_talk_index == "n164_argenta_wounded-8-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-8-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-8-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2661, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2661, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2662, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2662, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2663, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2663, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2664, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2664, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2665, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2665, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2666, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2666, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2667, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2667, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2668, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2668, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2669, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2669, questID, 1);
			 end 
	end
	if npc_talk_index == "n164_argenta_wounded-8-e" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300115, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300115, api_quest_HasQuestItem( pRoom, userObjID, 300115, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 267, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 267, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 267, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n164_argenta_wounded-8-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n164_argenta_wounded-1", "sq08_267_crisis_of_david.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n190_slave_david--------------------------------------------------------------------------------
function sq08_266_argenta_darkelf_OnTalk_n190_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n190_slave_david-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n190_slave_david-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n190_slave_david-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n190_slave_david-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n190_slave_david-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n190_slave_david-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n190_slave_david-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n190_slave_david-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n190_slave_david-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n190_slave_david-1-b" then 
	end
	if npc_talk_index == "n190_slave_david-1-c" then 
	end
	if npc_talk_index == "n190_slave_david-1-d" then 
	end
	if npc_talk_index == "n190_slave_david-1-e" then 
	end
	if npc_talk_index == "n190_slave_david-1-f" then 
	end
	if npc_talk_index == "n190_slave_david-1-g" then 
	end
	if npc_talk_index == "n190_slave_david-1-h" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n190_slave_david-3-b" then 
	end
	if npc_talk_index == "n190_slave_david-3-c" then 
	end
	if npc_talk_index == "n190_slave_david-3-d" then 
	end
	if npc_talk_index == "n190_slave_david-3-e" then 
	end
	if npc_talk_index == "n190_slave_david-3-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n190_slave_david-5-b" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 612, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200612, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300115, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 608, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200608, 30000);
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n190_slave_david-7-b" then 
	end
	if npc_talk_index == "n190_slave_david-7-c" then 
	end
	if npc_talk_index == "n190_slave_david-7-david89" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n190_slave_david-7-d";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									npc_talk_index = "n190_slave_david-7-e";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n190_slave_david-7-g";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									npc_talk_index = "n190_slave_david-7-f";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n190_slave_david-7-i";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n190_slave_david-7-j";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n190_slave_david-7-k";

				else
									npc_talk_index = "n190_slave_david-7-k";

				end

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n190_slave_david-7-david90" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end
	if npc_talk_index == "n190_slave_david-7-david91" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end
	if npc_talk_index == "n190_slave_david-7-david92" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end
	if npc_talk_index == "n190_slave_david-7-david93" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end
	if npc_talk_index == "n190_slave_david-7-david95" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end
	if npc_talk_index == "n190_slave_david-7-david96" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end
	if npc_talk_index == "n190_slave_david-7-david97" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				npc_talk_index = "n190_slave_david-7-h";
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n191_slave_david--------------------------------------------------------------------------------
function sq08_266_argenta_darkelf_OnTalk_n191_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n191_slave_david-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n191_slave_david-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n191_slave_david-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_266_argenta_darkelf_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 266);
	if qstep == 6 and CountIndex == 612 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300115, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300115, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 6 and CountIndex == 200612 then
				api_quest_IncCounting( pRoom, userObjID, 2, 612);

	end
	if qstep == 6 and CountIndex == 300115 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

	end
	if qstep == 6 and CountIndex == 608 then
				api_quest_IncCounting( pRoom, userObjID, 2, 612);

	end
	if qstep == 6 and CountIndex == 200608 then
				api_quest_IncCounting( pRoom, userObjID, 2, 612);

	end
end

function sq08_266_argenta_darkelf_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 266);
	if qstep == 6 and CountIndex == 612 and Count >= TargetCount  then

	end
	if qstep == 6 and CountIndex == 200612 and Count >= TargetCount  then

	end
	if qstep == 6 and CountIndex == 300115 and Count >= TargetCount  then

	end
	if qstep == 6 and CountIndex == 608 and Count >= TargetCount  then

	end
	if qstep == 6 and CountIndex == 200608 and Count >= TargetCount  then

	end
end

function sq08_266_argenta_darkelf_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 266);
	local questID=266;
end

function sq08_266_argenta_darkelf_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_266_argenta_darkelf_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_266_argenta_darkelf_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,266, 1);
				api_quest_SetJournalStep( pRoom, userObjID,266, 1);
				api_quest_SetQuestStep( pRoom, userObjID,266, 1);
				npc_talk_index = "n164_argenta_wounded-1";
end

</GameServer>