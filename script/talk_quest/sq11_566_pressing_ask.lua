<VillageServer>

function sq11_566_pressing_ask_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 38 then
		sq11_566_pressing_ask_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function sq11_566_pressing_ask_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n038_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5661, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5662, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5663, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5664, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5665, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5666, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5667, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5668, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5669, false);
			 end 

	end
	if npc_talk_index == "n038_royal_magician_kalaen-accepting-acceptted" then
				api_quest_AddQuest(userObjID,566, 1);
				api_quest_SetJournalStep(userObjID,566, 1);
				api_quest_SetQuestStep(userObjID,566, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1023, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201023, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300566, 10);
				npc_talk_index = "n038_royal_magician_kalaen-1";

	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5661, true);
				 api_quest_RewardQuestUser(userObjID, 5661, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5662, true);
				 api_quest_RewardQuestUser(userObjID, 5662, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5663, true);
				 api_quest_RewardQuestUser(userObjID, 5663, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5664, true);
				 api_quest_RewardQuestUser(userObjID, 5664, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5665, true);
				 api_quest_RewardQuestUser(userObjID, 5665, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5666, true);
				 api_quest_RewardQuestUser(userObjID, 5666, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5667, true);
				 api_quest_RewardQuestUser(userObjID, 5667, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5668, true);
				 api_quest_RewardQuestUser(userObjID, 5668, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5669, true);
				 api_quest_RewardQuestUser(userObjID, 5669, questID, 1);
			 end 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-f" then 

				if api_quest_HasQuestItem(userObjID, 300566, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300566, api_quest_HasQuestItem(userObjID, 300566, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 38, 600 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_566_pressing_ask_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 566);
	if qstep == 1 and CountIndex == 1023 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300566, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300566, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300566 then

	end
	if qstep == 1 and CountIndex == 201023 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300566, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300566, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_566_pressing_ask_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 566);
	if qstep == 1 and CountIndex == 1023 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300566 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201023 and Count >= TargetCount  then

	end
end

function sq11_566_pressing_ask_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 566);
	local questID=566;
end

function sq11_566_pressing_ask_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1023, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201023, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300566, 10);
end

function sq11_566_pressing_ask_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_566_pressing_ask_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,566, 1);
				api_quest_SetJournalStep(userObjID,566, 1);
				api_quest_SetQuestStep(userObjID,566, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1023, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201023, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300566, 10);
				npc_talk_index = "n038_royal_magician_kalaen-1";
end

</VillageServer>

<GameServer>
function sq11_566_pressing_ask_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 38 then
		sq11_566_pressing_ask_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function sq11_566_pressing_ask_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n038_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5661, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5662, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5663, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5664, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5665, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5666, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5667, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5668, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5669, false);
			 end 

	end
	if npc_talk_index == "n038_royal_magician_kalaen-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,566, 1);
				api_quest_SetJournalStep( pRoom, userObjID,566, 1);
				api_quest_SetQuestStep( pRoom, userObjID,566, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1023, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201023, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300566, 10);
				npc_talk_index = "n038_royal_magician_kalaen-1";

	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5661, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5661, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5662, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5662, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5663, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5663, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5664, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5664, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5665, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5665, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5666, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5666, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5667, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5667, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5668, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5668, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5669, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5669, questID, 1);
			 end 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-f" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300566, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300566, api_quest_HasQuestItem( pRoom, userObjID, 300566, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 38, 600 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_566_pressing_ask_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 566);
	if qstep == 1 and CountIndex == 1023 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300566, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300566, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300566 then

	end
	if qstep == 1 and CountIndex == 201023 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300566, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300566, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_566_pressing_ask_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 566);
	if qstep == 1 and CountIndex == 1023 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300566 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201023 and Count >= TargetCount  then

	end
end

function sq11_566_pressing_ask_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 566);
	local questID=566;
end

function sq11_566_pressing_ask_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1023, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201023, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300566, 10);
end

function sq11_566_pressing_ask_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_566_pressing_ask_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,566, 1);
				api_quest_SetJournalStep( pRoom, userObjID,566, 1);
				api_quest_SetQuestStep( pRoom, userObjID,566, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1023, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201023, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300566, 10);
				npc_talk_index = "n038_royal_magician_kalaen-1";
end

</GameServer>