<VillageServer>

function mq34_9271_what_they_want_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9271_what_they_want_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1054 then
		mq34_9271_what_they_want_OnTalk_n1054_azelia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		mq34_9271_what_they_want_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9271_what_they_want_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1047_argenta-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9271, 2);
				api_quest_SetJournalStep(userObjID,9271, 1);
				api_quest_SetQuestStep(userObjID,9271, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1047_argenta-5-b" then 
	end
	if npc_talk_index == "n1047_argenta-5-c" then 
	end
	if npc_talk_index == "n1047_argenta-5-d" then 
	end
	if npc_talk_index == "n1047_argenta-5-e" then 
	end
	if npc_talk_index == "n1047_argenta-5-f" then 
	end
	if npc_talk_index == "n1047_argenta-5-g" then 
	end
	if npc_talk_index == "n1047_argenta-5-h" then 
	end
	if npc_talk_index == "n1047_argenta-5-i" then 
	end
	if npc_talk_index == "n1047_argenta-5-j" then 
	end
	if npc_talk_index == "n1047_argenta-5-k" then 
	end
	if npc_talk_index == "n1047_argenta-5-l" then 
	end
	if npc_talk_index == "n1047_argenta-5-m" then 
	end
	if npc_talk_index == "n1047_argenta-5-n" then 
	end
	if npc_talk_index == "n1047_argenta-5-o" then 
	end
	if npc_talk_index == "n1047_argenta-5-p" then 
	end
	if npc_talk_index == "n1047_argenta-5-q" then 
	end
	if npc_talk_index == "n1047_argenta-5-r" then 
	end
	if npc_talk_index == "n1047_argenta-5-s" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92710, true);
				 api_quest_RewardQuestUser(userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92710, true);
				 api_quest_RewardQuestUser(userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92710, true);
				 api_quest_RewardQuestUser(userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92710, true);
				 api_quest_RewardQuestUser(userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92710, true);
				 api_quest_RewardQuestUser(userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92710, true);
				 api_quest_RewardQuestUser(userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92710, true);
				 api_quest_RewardQuestUser(userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92710, true);
				 api_quest_RewardQuestUser(userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92710, true);
				 api_quest_RewardQuestUser(userObjID, 92710, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9272, 2);
					api_quest_SetQuestStep(userObjID, 9272, 1);
					api_quest_SetJournalStep(userObjID, 9272, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1047_argenta-1", "mq34_9272_night_of_conclave.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1054_azelia--------------------------------------------------------------------------------
function mq34_9271_what_they_want_OnTalk_n1054_azelia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1054_azelia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1054_azelia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1054_azelia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1054_azelia-2-b" then 
	end
	if npc_talk_index == "n1054_azelia-2-c" then 
	end
	if npc_talk_index == "n1054_azelia-2-d" then 
	end
	if npc_talk_index == "n1054_azelia-2-e" then 
	end
	if npc_talk_index == "n1054_azelia-2-f" then 
	end
	if npc_talk_index == "n1054_azelia-2-g" then 
	end
	if npc_talk_index == "n1054_azelia-2-h" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function mq34_9271_what_they_want_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n044_archer_master_ishilien-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n044_archer_master_ishilien-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-3-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-h" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-i" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-j" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-k" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9271_what_they_want_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9271);
end

function mq34_9271_what_they_want_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9271);
end

function mq34_9271_what_they_want_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9271);
	local questID=9271;
end

function mq34_9271_what_they_want_OnRemoteStart( userObjID, questID )
end

function mq34_9271_what_they_want_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9271_what_they_want_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9271, 2);
				api_quest_SetJournalStep(userObjID,9271, 1);
				api_quest_SetQuestStep(userObjID,9271, 1);
				npc_talk_index = "n1047_argenta-1";
end

</VillageServer>

<GameServer>
function mq34_9271_what_they_want_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9271_what_they_want_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1054 then
		mq34_9271_what_they_want_OnTalk_n1054_azelia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		mq34_9271_what_they_want_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9271_what_they_want_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1047_argenta-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9271, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9271, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9271, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1047_argenta-5-b" then 
	end
	if npc_talk_index == "n1047_argenta-5-c" then 
	end
	if npc_talk_index == "n1047_argenta-5-d" then 
	end
	if npc_talk_index == "n1047_argenta-5-e" then 
	end
	if npc_talk_index == "n1047_argenta-5-f" then 
	end
	if npc_talk_index == "n1047_argenta-5-g" then 
	end
	if npc_talk_index == "n1047_argenta-5-h" then 
	end
	if npc_talk_index == "n1047_argenta-5-i" then 
	end
	if npc_talk_index == "n1047_argenta-5-j" then 
	end
	if npc_talk_index == "n1047_argenta-5-k" then 
	end
	if npc_talk_index == "n1047_argenta-5-l" then 
	end
	if npc_talk_index == "n1047_argenta-5-m" then 
	end
	if npc_talk_index == "n1047_argenta-5-n" then 
	end
	if npc_talk_index == "n1047_argenta-5-o" then 
	end
	if npc_talk_index == "n1047_argenta-5-p" then 
	end
	if npc_talk_index == "n1047_argenta-5-q" then 
	end
	if npc_talk_index == "n1047_argenta-5-r" then 
	end
	if npc_talk_index == "n1047_argenta-5-s" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92710, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9272, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9272, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9272, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1047_argenta-1", "mq34_9272_night_of_conclave.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1054_azelia--------------------------------------------------------------------------------
function mq34_9271_what_they_want_OnTalk_n1054_azelia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1054_azelia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1054_azelia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1054_azelia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1054_azelia-2-b" then 
	end
	if npc_talk_index == "n1054_azelia-2-c" then 
	end
	if npc_talk_index == "n1054_azelia-2-d" then 
	end
	if npc_talk_index == "n1054_azelia-2-e" then 
	end
	if npc_talk_index == "n1054_azelia-2-f" then 
	end
	if npc_talk_index == "n1054_azelia-2-g" then 
	end
	if npc_talk_index == "n1054_azelia-2-h" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function mq34_9271_what_they_want_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n044_archer_master_ishilien-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n044_archer_master_ishilien-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-3-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-h" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-i" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-j" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-k" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9271_what_they_want_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9271);
end

function mq34_9271_what_they_want_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9271);
end

function mq34_9271_what_they_want_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9271);
	local questID=9271;
end

function mq34_9271_what_they_want_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9271_what_they_want_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9271_what_they_want_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9271, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9271, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9271, 1);
				npc_talk_index = "n1047_argenta-1";
end

</GameServer>