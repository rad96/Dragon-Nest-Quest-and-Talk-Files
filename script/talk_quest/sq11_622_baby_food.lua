<VillageServer>

function sq11_622_baby_food_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 318 then
		sq11_622_baby_food_OnTalk_n318_mandra_mama(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n318_mandra_mama--------------------------------------------------------------------------------
function sq11_622_baby_food_OnTalk_n318_mandra_mama(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n318_mandra_mama-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n318_mandra_mama-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n318_mandra_mama-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n318_mandra_mama-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n318_mandra_mama-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6220, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6220, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6220, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6220, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6220, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6220, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6220, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6220, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6220, false);
			 end 

	end
	if npc_talk_index == "n318_mandra_mama-accepting-acceptted" then
				api_quest_AddQuest(userObjID,622, 1);
				api_quest_SetJournalStep(userObjID,622, 1);
				api_quest_SetQuestStep(userObjID,622, 1);
				npc_talk_index = "n318_mandra_mama-1";

	end
	if npc_talk_index == "n318_mandra_mama-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1025, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201025, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300347, 2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n318_mandra_mama-3-b" then 
	end
	if npc_talk_index == "n318_mandra_mama-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6220, true);
				 api_quest_RewardQuestUser(userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6220, true);
				 api_quest_RewardQuestUser(userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6220, true);
				 api_quest_RewardQuestUser(userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6220, true);
				 api_quest_RewardQuestUser(userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6220, true);
				 api_quest_RewardQuestUser(userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6220, true);
				 api_quest_RewardQuestUser(userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6220, true);
				 api_quest_RewardQuestUser(userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6220, true);
				 api_quest_RewardQuestUser(userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6220, true);
				 api_quest_RewardQuestUser(userObjID, 6220, questID, 1);
			 end 
	end
	if npc_talk_index == "n318_mandra_mama-3-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem(userObjID, 300347, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300347, api_quest_HasQuestItem(userObjID, 300347, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 623, 1);
					api_quest_SetQuestStep(userObjID, 623, 1);
					api_quest_SetJournalStep(userObjID, 623, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n318_mandra_mama-1", "sq11_623_glow_rapidly.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_622_baby_food_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 622);
	if qstep == 2 and CountIndex == 1025 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300347, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300347, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201025 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300347, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300347, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300347 then

	end
end

function sq11_622_baby_food_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 622);
	if qstep == 2 and CountIndex == 1025 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201025 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300347 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function sq11_622_baby_food_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 622);
	local questID=622;
end

function sq11_622_baby_food_OnRemoteStart( userObjID, questID )
end

function sq11_622_baby_food_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_622_baby_food_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,622, 1);
				api_quest_SetJournalStep(userObjID,622, 1);
				api_quest_SetQuestStep(userObjID,622, 1);
				npc_talk_index = "n318_mandra_mama-1";
end

</VillageServer>

<GameServer>
function sq11_622_baby_food_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 318 then
		sq11_622_baby_food_OnTalk_n318_mandra_mama( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n318_mandra_mama--------------------------------------------------------------------------------
function sq11_622_baby_food_OnTalk_n318_mandra_mama( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n318_mandra_mama-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n318_mandra_mama-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n318_mandra_mama-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n318_mandra_mama-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n318_mandra_mama-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, false);
			 end 

	end
	if npc_talk_index == "n318_mandra_mama-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,622, 1);
				api_quest_SetJournalStep( pRoom, userObjID,622, 1);
				api_quest_SetQuestStep( pRoom, userObjID,622, 1);
				npc_talk_index = "n318_mandra_mama-1";

	end
	if npc_talk_index == "n318_mandra_mama-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1025, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201025, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300347, 2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n318_mandra_mama-3-b" then 
	end
	if npc_talk_index == "n318_mandra_mama-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6220, questID, 1);
			 end 
	end
	if npc_talk_index == "n318_mandra_mama-3-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem( pRoom, userObjID, 300347, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300347, api_quest_HasQuestItem( pRoom, userObjID, 300347, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 623, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 623, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 623, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n318_mandra_mama-1", "sq11_623_glow_rapidly.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_622_baby_food_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 622);
	if qstep == 2 and CountIndex == 1025 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300347, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300347, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201025 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300347, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300347, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300347 then

	end
end

function sq11_622_baby_food_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 622);
	if qstep == 2 and CountIndex == 1025 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201025 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300347 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function sq11_622_baby_food_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 622);
	local questID=622;
end

function sq11_622_baby_food_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_622_baby_food_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_622_baby_food_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,622, 1);
				api_quest_SetJournalStep( pRoom, userObjID,622, 1);
				api_quest_SetQuestStep( pRoom, userObjID,622, 1);
				npc_talk_index = "n318_mandra_mama-1";
end

</GameServer>