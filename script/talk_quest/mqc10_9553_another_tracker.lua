<VillageServer>

function mqc10_9553_another_tracker_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1737 then
		mqc10_9553_another_tracker_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1732 then
		mqc10_9553_another_tracker_OnTalk_n1732_rambert_temple(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1737_eltia--------------------------------------------------------------------------------
function mqc10_9553_another_tracker_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1737_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1737_eltia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1737_eltia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9553, 2);
				api_quest_SetJournalStep(userObjID,9553, 1);
				api_quest_SetQuestStep(userObjID,9553, 1);
				npc_talk_index = "n1737_eltia-1";

	end
	if npc_talk_index == "n1737_eltia-1-b" then 
	end
	if npc_talk_index == "n1737_eltia-1-c" then 
	end
	if npc_talk_index == "n1737_eltia-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1737_eltia-3-b" then 
	end
	if npc_talk_index == "n1737_eltia-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95530, true);
				 api_quest_RewardQuestUser(userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95530, true);
				 api_quest_RewardQuestUser(userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95530, true);
				 api_quest_RewardQuestUser(userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95530, true);
				 api_quest_RewardQuestUser(userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95530, true);
				 api_quest_RewardQuestUser(userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95530, true);
				 api_quest_RewardQuestUser(userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95530, true);
				 api_quest_RewardQuestUser(userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95530, true);
				 api_quest_RewardQuestUser(userObjID, 95530, questID, 1);
			 end 
	end
	if npc_talk_index == "n1737_eltia-3-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9554, 2);
					api_quest_SetQuestStep(userObjID, 9554, 1);
					api_quest_SetJournalStep(userObjID, 9554, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1737_eltia-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1737_eltia-1", "mqc10_9554_future_in_the_hand.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1732_rambert_temple--------------------------------------------------------------------------------
function mqc10_9553_another_tracker_OnTalk_n1732_rambert_temple(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1732_rambert_temple-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1732_rambert_temple-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1732_rambert_temple-2-b" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-c" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-d" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-e" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-f" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-g" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-h" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-i" then 
	end
	if npc_talk_index == "n1732_rambert_temple-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc10_9553_another_tracker_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9553);
end

function mqc10_9553_another_tracker_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9553);
end

function mqc10_9553_another_tracker_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9553);
	local questID=9553;
end

function mqc10_9553_another_tracker_OnRemoteStart( userObjID, questID )
end

function mqc10_9553_another_tracker_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc10_9553_another_tracker_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9553, 2);
				api_quest_SetJournalStep(userObjID,9553, 1);
				api_quest_SetQuestStep(userObjID,9553, 1);
				npc_talk_index = "n1737_eltia-1";
end

</VillageServer>

<GameServer>
function mqc10_9553_another_tracker_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1737 then
		mqc10_9553_another_tracker_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1732 then
		mqc10_9553_another_tracker_OnTalk_n1732_rambert_temple( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1737_eltia--------------------------------------------------------------------------------
function mqc10_9553_another_tracker_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1737_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1737_eltia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1737_eltia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9553, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9553, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9553, 1);
				npc_talk_index = "n1737_eltia-1";

	end
	if npc_talk_index == "n1737_eltia-1-b" then 
	end
	if npc_talk_index == "n1737_eltia-1-c" then 
	end
	if npc_talk_index == "n1737_eltia-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1737_eltia-3-b" then 
	end
	if npc_talk_index == "n1737_eltia-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95530, questID, 1);
			 end 
	end
	if npc_talk_index == "n1737_eltia-3-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9554, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9554, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9554, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1737_eltia-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1737_eltia-1", "mqc10_9554_future_in_the_hand.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1732_rambert_temple--------------------------------------------------------------------------------
function mqc10_9553_another_tracker_OnTalk_n1732_rambert_temple( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1732_rambert_temple-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1732_rambert_temple-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1732_rambert_temple-2-b" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-c" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-d" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-e" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-f" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-g" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-h" then 
	end
	if npc_talk_index == "n1732_rambert_temple-2-i" then 
	end
	if npc_talk_index == "n1732_rambert_temple-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc10_9553_another_tracker_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9553);
end

function mqc10_9553_another_tracker_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9553);
end

function mqc10_9553_another_tracker_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9553);
	local questID=9553;
end

function mqc10_9553_another_tracker_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc10_9553_another_tracker_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc10_9553_another_tracker_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9553, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9553, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9553, 1);
				npc_talk_index = "n1737_eltia-1";
end

</GameServer>