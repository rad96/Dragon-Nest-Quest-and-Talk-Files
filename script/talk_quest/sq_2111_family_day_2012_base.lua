<VillageServer>

function sq_2111_family_day_2012_base_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 100 then
		sq_2111_family_day_2012_base_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq_2111_family_day_2012_base_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				if api_user_HasItem(userObjID, 805306984, 15) >= 15 then
									api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n100_event_ilyn-2";
				else
									npc_talk_index = "n100_event_ilyn-1";
				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n100_event_ilyn-2";
				if api_user_HasItem(userObjID, 805306984, 15) >= 15 then
									npc_talk_index = "n100_event_ilyn-2";
				else
									npc_talk_index = "n100_event_ilyn-2-c";				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 805306984, 30002);

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 	
				 api_ui_OpenQuestReward(userObjID, 21110, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21110, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21110, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21110, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21110, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21110, false);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2111, 1);
				api_quest_SetJournalStep(userObjID,2111, 1);
				api_quest_SetQuestStep(userObjID,2111, 1);
				if api_user_HasItem(userObjID, 805306984, 15) >= 15 then
									api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n100_event_ilyn-2";
				else
									api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805306984, 30002);
				npc_talk_index = "n100_event_ilyn-1";
				end

	end
	if npc_talk_index == "n100_event_ilyn-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 	
					 api_ui_OpenQuestReward(userObjID, 21110, true);
					 api_quest_RewardQuestUser(userObjID, 21110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
					 api_ui_OpenQuestReward(userObjID, 21110, true);
					 api_quest_RewardQuestUser(userObjID, 21110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
					 api_ui_OpenQuestReward(userObjID, 21110, true);
					 api_quest_RewardQuestUser(userObjID, 21110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
					 api_ui_OpenQuestReward(userObjID, 21110, true);
					 api_quest_RewardQuestUser(userObjID, 21110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
					 api_ui_OpenQuestReward(userObjID, 21110, true);
					 api_quest_RewardQuestUser(userObjID, 21110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
					 api_ui_OpenQuestReward(userObjID, 21110, true);
					 api_quest_RewardQuestUser(userObjID, 21110, questID, 1);
			 end 
	end
	if npc_talk_index == "n100_event_ilyn-2-b" then 
				api_user_DelItem(userObjID, 805306984, 15, questID);

				if api_quest_CompleteQuest(userObjID, questID, true, false) == 1  then
				else
					npc_talk_index = "_full_inventory";

				end	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2111_family_day_2012_base_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2111);
	if qstep == 3 and CountIndex == 805306984 then

	end
	if qstep == 1 and CountIndex == 805306984 then

	end
end

function sq_2111_family_day_2012_base_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2111);
	if qstep == 3 and CountIndex == 805306984 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 805306984 and Count >= TargetCount  then
				if api_user_HasItem(userObjID, 805306984, 15) >= 15 then
									api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

	end
end

function sq_2111_family_day_2012_base_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2111);
	local questID=2111;
end

</VillageServer>

<GameServer>
function sq_2111_family_day_2012_base_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 100 then
		sq_2111_family_day_2012_base_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq_2111_family_day_2012_base_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				if api_user_HasItem( pRoom, userObjID, 805306984, 15) >= 15 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n100_event_ilyn-2";
				else
									npc_talk_index = "n100_event_ilyn-1";
				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n100_event_ilyn-2";
				if api_user_HasItem( pRoom, userObjID, 805306984, 15) >= 15 then
									npc_talk_index = "n100_event_ilyn-2";
				else
									npc_talk_index = "n100_event_ilyn-2-c";				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 805306984, 30002);

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 	
				 api_ui_OpenQuestReward( pRoom, userObjID, 21110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21110, false);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2111, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2111, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2111, 1);
				if api_user_HasItem( pRoom, userObjID, 805306984, 15) >= 15 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n100_event_ilyn-2";
				else
									api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805306984, 30002);
				npc_talk_index = "n100_event_ilyn-1";
				end

	end
	if npc_talk_index == "n100_event_ilyn-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 	
					 api_ui_OpenQuestReward( pRoom, userObjID, 21110, true);
					 api_quest_RewardQuestUser( pRoom, userObjID, 21110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
					 api_ui_OpenQuestReward( pRoom, userObjID, 21110, true);
					 api_quest_RewardQuestUser( pRoom, userObjID, 21110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
					 api_ui_OpenQuestReward( pRoom, userObjID, 21110, true);
					 api_quest_RewardQuestUser( pRoom, userObjID, 21110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
					 api_ui_OpenQuestReward( pRoom, userObjID, 21110, true);
					 api_quest_RewardQuestUser( pRoom, userObjID, 21110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
					 api_ui_OpenQuestReward( pRoom, userObjID, 21110, true);
					 api_quest_RewardQuestUser( pRoom, userObjID, 21110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
					 api_ui_OpenQuestReward( pRoom, userObjID, 21110, true);
					 api_quest_RewardQuestUser( pRoom, userObjID, 21110, questID, 1);
			 end 
	end
	if npc_talk_index == "n100_event_ilyn-2-b" then 
				api_user_DelItem( pRoom, userObjID, 805306984, 15, questID);

				if api_quest_CompleteQuest( pRoom, userObjID, questID, true, false) == 1  then
				else
					npc_talk_index = "_full_inventory";

				end	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2111_family_day_2012_base_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2111);
	if qstep == 3 and CountIndex == 805306984 then

	end
	if qstep == 1 and CountIndex == 805306984 then

	end
end

function sq_2111_family_day_2012_base_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2111);
	if qstep == 3 and CountIndex == 805306984 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 805306984 and Count >= TargetCount  then
				if api_user_HasItem( pRoom, userObjID, 805306984, 15) >= 15 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

	end
end

function sq_2111_family_day_2012_base_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2111);
	local questID=2111;
end

</GameServer>