<VillageServer>

function mq05_097_he_has_chosen_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 12 then
		mq05_097_he_has_chosen_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 535 then
		mq05_097_he_has_chosen_OnTalk_n535_angelica(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 537 then
		mq05_097_he_has_chosen_OnTalk_n537_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 538 then
		mq05_097_he_has_chosen_OnTalk_n538_edan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 539 then
		mq05_097_he_has_chosen_OnTalk_n539_angelica(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_097_he_has_chosen_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n012_sorceress_master_cynthia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,97, 2);
				api_quest_SetJournalStep(userObjID,97, 1);
				api_quest_SetQuestStep(userObjID,97, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n535_angelica--------------------------------------------------------------------------------
function mq05_097_he_has_chosen_OnTalk_n535_angelica(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n535_angelica-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n535_angelica-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n535_angelica-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n535_angelica-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n537_cleric_jake--------------------------------------------------------------------------------
function mq05_097_he_has_chosen_OnTalk_n537_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n537_cleric_jake-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n537_cleric_jake-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n537_cleric_jake-1-b" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-c" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-d" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-e" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-e" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-f" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-g" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-h" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-h" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-i" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-j" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-k" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-l" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-m" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-n" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-o" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-p" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-q" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-r" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-camera_1" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n538_edan--------------------------------------------------------------------------------
function mq05_097_he_has_chosen_OnTalk_n538_edan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n538_edan-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n538_edan-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n539_angelica--------------------------------------------------------------------------------
function mq05_097_he_has_chosen_OnTalk_n539_angelica(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n539_angelica-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n539_angelica-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n539_angelica-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n539_angelica-2-b" then 
	end
	if npc_talk_index == "n539_angelica-2-c" then 
	end
	if npc_talk_index == "n539_angelica-2-d" then 
	end
	if npc_talk_index == "n539_angelica-2-e" then 
	end
	if npc_talk_index == "n539_angelica-2-f" then 
	end
	if npc_talk_index == "n539_angelica-2-g" then 
	end
	if npc_talk_index == "n539_angelica-2-h" then 
	end
	if npc_talk_index == "n539_angelica-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n539_angelica-3-b" then 
	end
	if npc_talk_index == "n539_angelica-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 970, true);
				 api_quest_RewardQuestUser(userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 970, true);
				 api_quest_RewardQuestUser(userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 970, true);
				 api_quest_RewardQuestUser(userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 970, true);
				 api_quest_RewardQuestUser(userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 970, true);
				 api_quest_RewardQuestUser(userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 970, true);
				 api_quest_RewardQuestUser(userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 970, true);
				 api_quest_RewardQuestUser(userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 970, true);
				 api_quest_RewardQuestUser(userObjID, 970, questID, 1);
			 end 
	end
	if npc_talk_index == "n539_angelica-3-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 98, 2);
					api_quest_SetQuestStep(userObjID, 98, 1);
					api_quest_SetJournalStep(userObjID, 98, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n539_angelica-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n539_angelica-1", "mq05_098_end_the_search.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_097_he_has_chosen_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 97);
end

function mq05_097_he_has_chosen_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 97);
end

function mq05_097_he_has_chosen_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 97);
	local questID=97;
end

function mq05_097_he_has_chosen_OnRemoteStart( userObjID, questID )
end

function mq05_097_he_has_chosen_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq05_097_he_has_chosen_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,97, 2);
				api_quest_SetJournalStep(userObjID,97, 1);
				api_quest_SetQuestStep(userObjID,97, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1";
end

</VillageServer>

<GameServer>
function mq05_097_he_has_chosen_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 12 then
		mq05_097_he_has_chosen_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 535 then
		mq05_097_he_has_chosen_OnTalk_n535_angelica( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 537 then
		mq05_097_he_has_chosen_OnTalk_n537_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 538 then
		mq05_097_he_has_chosen_OnTalk_n538_edan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 539 then
		mq05_097_he_has_chosen_OnTalk_n539_angelica( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_097_he_has_chosen_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n012_sorceress_master_cynthia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,97, 2);
				api_quest_SetJournalStep( pRoom, userObjID,97, 1);
				api_quest_SetQuestStep( pRoom, userObjID,97, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n535_angelica--------------------------------------------------------------------------------
function mq05_097_he_has_chosen_OnTalk_n535_angelica( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n535_angelica-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n535_angelica-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n535_angelica-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n535_angelica-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n537_cleric_jake--------------------------------------------------------------------------------
function mq05_097_he_has_chosen_OnTalk_n537_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n537_cleric_jake-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n537_cleric_jake-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n537_cleric_jake-1-b" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-c" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-d" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-e" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-e" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-f" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-g" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-h" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-h" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-i" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-j" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-k" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-l" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-m" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-n" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-o" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-p" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-q" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-r" then 
	end
	if npc_talk_index == "n537_cleric_jake-1-camera_1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n538_edan--------------------------------------------------------------------------------
function mq05_097_he_has_chosen_OnTalk_n538_edan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n538_edan-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n538_edan-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n539_angelica--------------------------------------------------------------------------------
function mq05_097_he_has_chosen_OnTalk_n539_angelica( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n539_angelica-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n539_angelica-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n539_angelica-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n539_angelica-2-b" then 
	end
	if npc_talk_index == "n539_angelica-2-c" then 
	end
	if npc_talk_index == "n539_angelica-2-d" then 
	end
	if npc_talk_index == "n539_angelica-2-e" then 
	end
	if npc_talk_index == "n539_angelica-2-f" then 
	end
	if npc_talk_index == "n539_angelica-2-g" then 
	end
	if npc_talk_index == "n539_angelica-2-h" then 
	end
	if npc_talk_index == "n539_angelica-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n539_angelica-3-b" then 
	end
	if npc_talk_index == "n539_angelica-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 970, questID, 1);
			 end 
	end
	if npc_talk_index == "n539_angelica-3-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 98, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 98, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 98, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n539_angelica-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n539_angelica-1", "mq05_098_end_the_search.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_097_he_has_chosen_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 97);
end

function mq05_097_he_has_chosen_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 97);
end

function mq05_097_he_has_chosen_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 97);
	local questID=97;
end

function mq05_097_he_has_chosen_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq05_097_he_has_chosen_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq05_097_he_has_chosen_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,97, 2);
				api_quest_SetJournalStep( pRoom, userObjID,97, 1);
				api_quest_SetQuestStep( pRoom, userObjID,97, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1";
end

</GameServer>