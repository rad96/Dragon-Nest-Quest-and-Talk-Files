<VillageServer>

function sq05_153_gurafinder1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 16 then
		sq05_153_gurafinder1_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n016_trader_borin--------------------------------------------------------------------------------
function sq05_153_gurafinder1_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n016_trader_borin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n016_trader_borin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-accepting-j" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1531, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1531, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1531, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1532, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1531, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1531, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1531, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1531, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1531, false);
			 end 

	end
	if npc_talk_index == "n016_trader_borin-accepting-acceptted" then
				npc_talk_index = "n016_trader_borin-1";
				api_quest_AddQuest(userObjID,153, 1);
				api_quest_SetJournalStep(userObjID,153, 1);
				api_quest_SetQuestStep(userObjID,153, 1);

	end
	if npc_talk_index == "n016_trader_borin-2" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300104, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300104, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300105, 30002);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300106, 30002);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300107, 30002);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 286, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 287, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 288, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 289, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 291, 30000);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n016_trader_borin-3-b" then 
	end
	if npc_talk_index == "n016_trader_borin-3-c" then 
	end
	if npc_talk_index == "n016_trader_borin-3-d" then 
	end
	if npc_talk_index == "n016_trader_borin-3-e" then 
	end
	if npc_talk_index == "n016_trader_borin-3-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1531, true);
				 api_quest_RewardQuestUser(userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1531, true);
				 api_quest_RewardQuestUser(userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1531, true);
				 api_quest_RewardQuestUser(userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1532, true);
				 api_quest_RewardQuestUser(userObjID, 1532, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1531, true);
				 api_quest_RewardQuestUser(userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1531, true);
				 api_quest_RewardQuestUser(userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1531, true);
				 api_quest_RewardQuestUser(userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1531, true);
				 api_quest_RewardQuestUser(userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1531, true);
				 api_quest_RewardQuestUser(userObjID, 1531, questID, 1);
			 end 
	end
	if npc_talk_index == "n016_trader_borin-3-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300104, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300104, api_quest_HasQuestItem(userObjID, 300104, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300105, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300105, api_quest_HasQuestItem(userObjID, 300105, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300106, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300106, api_quest_HasQuestItem(userObjID, 300106, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300107, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300107, api_quest_HasQuestItem(userObjID, 300107, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_153_gurafinder1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 153);
	if qstep == 2 and CountIndex == 300105 then

				local TP_1_14_21 = {};
				api_user_UserMessage(userObjID,6, 1240087, TP_1_14_21);
				if api_quest_HasQuestItem(userObjID, 300105, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300106, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300107, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 300106 then

				local TP_1_14_21 = {};
				api_user_UserMessage(userObjID,6, 1240087, TP_1_14_21);
				if api_quest_HasQuestItem(userObjID, 300105, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300106, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300107, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 300107 then

				local TP_1_14_21 = {};
				api_user_UserMessage(userObjID,6, 1240087, TP_1_14_21);
				if api_quest_HasQuestItem(userObjID, 300105, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300106, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300107, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 286 then
				if api_quest_HasQuestItem(userObjID, 300105, 1) >= 1 then
				else
									local rcval_1_22 = math.random(1,48);
				if rcval_1_22 >= 1 and rcval_1_22 < 10 then
	
				local TP_1_10 = {};
				api_user_UserMessage(userObjID,6, 1240083, TP_1_10);
				end
				if rcval_1_22 >= 10 and rcval_1_22 < 20 then
	
				local TP_1_11 = {};
				api_user_UserMessage(userObjID,6, 1240084, TP_1_11);
				end
				if rcval_1_22 >= 20 and rcval_1_22 < 30 then
	
				local TP_1_12 = {};
				api_user_UserMessage(userObjID,6, 1240085, TP_1_12);
				end
				if rcval_1_22 >= 30 and rcval_1_22 < 40 then
	
				local TP_1_13 = {};
				api_user_UserMessage(userObjID,6, 1240086, TP_1_13);
				end
				if rcval_1_22 >= 40 and rcval_1_22 < 48 then
					if api_quest_CheckQuestInvenForAddItem(userObjID, 300105, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300105, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				end

				end

	end
	if qstep == 2 and CountIndex == 287 then
				if api_quest_HasQuestItem(userObjID, 300106, 1) >= 1 then
				else
									local rcval_1_23 = math.random(1,80);
				if rcval_1_23 >= 1 and rcval_1_23 < 10 then
	
				local TP_1_10 = {};
				api_user_UserMessage(userObjID,6, 1240083, TP_1_10);
				end
				if rcval_1_23 >= 10 and rcval_1_23 < 20 then
	
				local TP_1_11 = {};
				api_user_UserMessage(userObjID,6, 1240084, TP_1_11);
				end
				if rcval_1_23 >= 20 and rcval_1_23 < 30 then
	
				local TP_1_12 = {};
				api_user_UserMessage(userObjID,6, 1240085, TP_1_12);
				end
				if rcval_1_23 >= 30 and rcval_1_23 < 40 then
	
				local TP_1_13 = {};
				api_user_UserMessage(userObjID,6, 1240086, TP_1_13);
				end
				if rcval_1_23 >= 40 and rcval_1_23 < 80 then
					if api_quest_CheckQuestInvenForAddItem(userObjID, 300106, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300106, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				end

				end

	end
	if qstep == 2 and CountIndex == 288 then
				if api_quest_HasQuestItem(userObjID, 300107, 1) >= 1 then
				else
									local rcval_1_24 = math.random(1,48);
				if rcval_1_24 >= 1 and rcval_1_24 < 10 then
	
				local TP_1_10 = {};
				api_user_UserMessage(userObjID,6, 1240083, TP_1_10);
				end
				if rcval_1_24 >= 10 and rcval_1_24 < 20 then
	
				local TP_1_11 = {};
				api_user_UserMessage(userObjID,6, 1240084, TP_1_11);
				end
				if rcval_1_24 >= 20 and rcval_1_24 < 30 then
	
				local TP_1_12 = {};
				api_user_UserMessage(userObjID,6, 1240085, TP_1_12);
				end
				if rcval_1_24 >= 30 and rcval_1_24 < 40 then
	
				local TP_1_13 = {};
				api_user_UserMessage(userObjID,6, 1240086, TP_1_13);
				end
				if rcval_1_24 >= 40 and rcval_1_24 < 48 then
					if api_quest_CheckQuestInvenForAddItem(userObjID, 300107, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300107, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				end

				end

	end
	if qstep == 2 and CountIndex == 289 then
				if api_quest_HasQuestItem(userObjID, 300107, 1) >= 1 then
				else
									local rcval_1_24 = math.random(1,48);
				if rcval_1_24 >= 1 and rcval_1_24 < 10 then
	
				local TP_1_10 = {};
				api_user_UserMessage(userObjID,6, 1240083, TP_1_10);
				end
				if rcval_1_24 >= 10 and rcval_1_24 < 20 then
	
				local TP_1_11 = {};
				api_user_UserMessage(userObjID,6, 1240084, TP_1_11);
				end
				if rcval_1_24 >= 20 and rcval_1_24 < 30 then
	
				local TP_1_12 = {};
				api_user_UserMessage(userObjID,6, 1240085, TP_1_12);
				end
				if rcval_1_24 >= 30 and rcval_1_24 < 40 then
	
				local TP_1_13 = {};
				api_user_UserMessage(userObjID,6, 1240086, TP_1_13);
				end
				if rcval_1_24 >= 40 and rcval_1_24 < 48 then
					if api_quest_CheckQuestInvenForAddItem(userObjID, 300107, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300107, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				end

				end

	end
	if qstep == 2 and CountIndex == 291 then
				if api_quest_HasQuestItem(userObjID, 300107, 1) >= 1 then
				else
									local rcval_1_24 = math.random(1,48);
				if rcval_1_24 >= 1 and rcval_1_24 < 10 then
	
				local TP_1_10 = {};
				api_user_UserMessage(userObjID,6, 1240083, TP_1_10);
				end
				if rcval_1_24 >= 10 and rcval_1_24 < 20 then
	
				local TP_1_11 = {};
				api_user_UserMessage(userObjID,6, 1240084, TP_1_11);
				end
				if rcval_1_24 >= 20 and rcval_1_24 < 30 then
	
				local TP_1_12 = {};
				api_user_UserMessage(userObjID,6, 1240085, TP_1_12);
				end
				if rcval_1_24 >= 30 and rcval_1_24 < 40 then
	
				local TP_1_13 = {};
				api_user_UserMessage(userObjID,6, 1240086, TP_1_13);
				end
				if rcval_1_24 >= 40 and rcval_1_24 < 48 then
					if api_quest_CheckQuestInvenForAddItem(userObjID, 300107, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300107, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				end

				end

	end
end

function sq05_153_gurafinder1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 153);
	if qstep == 2 and CountIndex == 300105 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300106 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300107 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 286 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 287 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 288 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 289 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 291 and Count >= TargetCount  then

	end
end

function sq05_153_gurafinder1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 153);
	local questID=153;
end

function sq05_153_gurafinder1_OnRemoteStart( userObjID, questID )
end

function sq05_153_gurafinder1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq05_153_gurafinder1_ForceAccept( userObjID, npcObjID, questID )
				npc_talk_index = "n016_trader_borin-1";
				api_quest_AddQuest(userObjID,153, 1);
				api_quest_SetJournalStep(userObjID,153, 1);
				api_quest_SetQuestStep(userObjID,153, 1);
end

</VillageServer>

<GameServer>
function sq05_153_gurafinder1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 16 then
		sq05_153_gurafinder1_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n016_trader_borin--------------------------------------------------------------------------------
function sq05_153_gurafinder1_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n016_trader_borin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n016_trader_borin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-accepting-j" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1532, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, false);
			 end 

	end
	if npc_talk_index == "n016_trader_borin-accepting-acceptted" then
				npc_talk_index = "n016_trader_borin-1";
				api_quest_AddQuest( pRoom, userObjID,153, 1);
				api_quest_SetJournalStep( pRoom, userObjID,153, 1);
				api_quest_SetQuestStep( pRoom, userObjID,153, 1);

	end
	if npc_talk_index == "n016_trader_borin-2" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300104, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300104, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300105, 30002);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300106, 30002);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300107, 30002);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 286, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 287, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 288, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 289, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 291, 30000);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n016_trader_borin-3-b" then 
	end
	if npc_talk_index == "n016_trader_borin-3-c" then 
	end
	if npc_talk_index == "n016_trader_borin-3-d" then 
	end
	if npc_talk_index == "n016_trader_borin-3-e" then 
	end
	if npc_talk_index == "n016_trader_borin-3-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1532, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1532, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1531, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1531, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1531, questID, 1);
			 end 
	end
	if npc_talk_index == "n016_trader_borin-3-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300104, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300104, api_quest_HasQuestItem( pRoom, userObjID, 300104, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300105, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300105, api_quest_HasQuestItem( pRoom, userObjID, 300105, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300106, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300106, api_quest_HasQuestItem( pRoom, userObjID, 300106, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300107, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300107, api_quest_HasQuestItem( pRoom, userObjID, 300107, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_153_gurafinder1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 153);
	if qstep == 2 and CountIndex == 300105 then

				local TP_1_14_21 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240087, TP_1_14_21);
				if api_quest_HasQuestItem( pRoom, userObjID, 300105, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300106, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300107, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 300106 then

				local TP_1_14_21 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240087, TP_1_14_21);
				if api_quest_HasQuestItem( pRoom, userObjID, 300105, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300106, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300107, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 300107 then

				local TP_1_14_21 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240087, TP_1_14_21);
				if api_quest_HasQuestItem( pRoom, userObjID, 300105, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300106, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300107, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 286 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300105, 1) >= 1 then
				else
									local rcval_1_22 = math.random(1,48);
				if rcval_1_22 >= 1 and rcval_1_22 < 10 then
	
				local TP_1_10 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240083, TP_1_10);
				end
				if rcval_1_22 >= 10 and rcval_1_22 < 20 then
	
				local TP_1_11 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240084, TP_1_11);
				end
				if rcval_1_22 >= 20 and rcval_1_22 < 30 then
	
				local TP_1_12 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240085, TP_1_12);
				end
				if rcval_1_22 >= 30 and rcval_1_22 < 40 then
	
				local TP_1_13 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240086, TP_1_13);
				end
				if rcval_1_22 >= 40 and rcval_1_22 < 48 then
					if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300105, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300105, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				end

				end

	end
	if qstep == 2 and CountIndex == 287 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300106, 1) >= 1 then
				else
									local rcval_1_23 = math.random(1,80);
				if rcval_1_23 >= 1 and rcval_1_23 < 10 then
	
				local TP_1_10 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240083, TP_1_10);
				end
				if rcval_1_23 >= 10 and rcval_1_23 < 20 then
	
				local TP_1_11 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240084, TP_1_11);
				end
				if rcval_1_23 >= 20 and rcval_1_23 < 30 then
	
				local TP_1_12 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240085, TP_1_12);
				end
				if rcval_1_23 >= 30 and rcval_1_23 < 40 then
	
				local TP_1_13 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240086, TP_1_13);
				end
				if rcval_1_23 >= 40 and rcval_1_23 < 80 then
					if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300106, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300106, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				end

				end

	end
	if qstep == 2 and CountIndex == 288 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300107, 1) >= 1 then
				else
									local rcval_1_24 = math.random(1,48);
				if rcval_1_24 >= 1 and rcval_1_24 < 10 then
	
				local TP_1_10 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240083, TP_1_10);
				end
				if rcval_1_24 >= 10 and rcval_1_24 < 20 then
	
				local TP_1_11 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240084, TP_1_11);
				end
				if rcval_1_24 >= 20 and rcval_1_24 < 30 then
	
				local TP_1_12 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240085, TP_1_12);
				end
				if rcval_1_24 >= 30 and rcval_1_24 < 40 then
	
				local TP_1_13 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240086, TP_1_13);
				end
				if rcval_1_24 >= 40 and rcval_1_24 < 48 then
					if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300107, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300107, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				end

				end

	end
	if qstep == 2 and CountIndex == 289 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300107, 1) >= 1 then
				else
									local rcval_1_24 = math.random(1,48);
				if rcval_1_24 >= 1 and rcval_1_24 < 10 then
	
				local TP_1_10 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240083, TP_1_10);
				end
				if rcval_1_24 >= 10 and rcval_1_24 < 20 then
	
				local TP_1_11 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240084, TP_1_11);
				end
				if rcval_1_24 >= 20 and rcval_1_24 < 30 then
	
				local TP_1_12 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240085, TP_1_12);
				end
				if rcval_1_24 >= 30 and rcval_1_24 < 40 then
	
				local TP_1_13 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240086, TP_1_13);
				end
				if rcval_1_24 >= 40 and rcval_1_24 < 48 then
					if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300107, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300107, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				end

				end

	end
	if qstep == 2 and CountIndex == 291 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300107, 1) >= 1 then
				else
									local rcval_1_24 = math.random(1,48);
				if rcval_1_24 >= 1 and rcval_1_24 < 10 then
	
				local TP_1_10 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240083, TP_1_10);
				end
				if rcval_1_24 >= 10 and rcval_1_24 < 20 then
	
				local TP_1_11 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240084, TP_1_11);
				end
				if rcval_1_24 >= 20 and rcval_1_24 < 30 then
	
				local TP_1_12 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240085, TP_1_12);
				end
				if rcval_1_24 >= 30 and rcval_1_24 < 40 then
	
				local TP_1_13 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240086, TP_1_13);
				end
				if rcval_1_24 >= 40 and rcval_1_24 < 48 then
					if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300107, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300107, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				end

				end

	end
end

function sq05_153_gurafinder1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 153);
	if qstep == 2 and CountIndex == 300105 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300106 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300107 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 286 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 287 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 288 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 289 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 291 and Count >= TargetCount  then

	end
end

function sq05_153_gurafinder1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 153);
	local questID=153;
end

function sq05_153_gurafinder1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq05_153_gurafinder1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq05_153_gurafinder1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				npc_talk_index = "n016_trader_borin-1";
				api_quest_AddQuest( pRoom, userObjID,153, 1);
				api_quest_SetJournalStep( pRoom, userObjID,153, 1);
				api_quest_SetQuestStep( pRoom, userObjID,153, 1);
end

</GameServer>