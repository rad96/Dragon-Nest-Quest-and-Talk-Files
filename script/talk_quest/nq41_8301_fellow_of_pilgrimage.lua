<VillageServer>

function nq41_8301_fellow_of_pilgrimage_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 714 then
		nq41_8301_fellow_of_pilgrimage_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n714_cleric_yohan--------------------------------------------------------------------------------
function nq41_8301_fellow_of_pilgrimage_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n714_cleric_yohan-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n714_cleric_yohan-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-accepting-acceptted" then
				api_quest_AddQuest(userObjID,8301, 1);
				api_quest_SetJournalStep(userObjID,8301, 1);
				api_quest_SetQuestStep(userObjID,8301, 1);
				npc_talk_index = "n714_cleric_yohan-1";

	end
	if npc_talk_index == "n714_cleric_yohan-1-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 714, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq41_8301_fellow_of_pilgrimage_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8301);
end

function nq41_8301_fellow_of_pilgrimage_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8301);
end

function nq41_8301_fellow_of_pilgrimage_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8301);
	local questID=8301;
end

function nq41_8301_fellow_of_pilgrimage_OnRemoteStart( userObjID, questID )
end

function nq41_8301_fellow_of_pilgrimage_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq41_8301_fellow_of_pilgrimage_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,8301, 1);
				api_quest_SetJournalStep(userObjID,8301, 1);
				api_quest_SetQuestStep(userObjID,8301, 1);
				npc_talk_index = "n714_cleric_yohan-1";
end

</VillageServer>

<GameServer>
function nq41_8301_fellow_of_pilgrimage_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 714 then
		nq41_8301_fellow_of_pilgrimage_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n714_cleric_yohan--------------------------------------------------------------------------------
function nq41_8301_fellow_of_pilgrimage_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n714_cleric_yohan-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n714_cleric_yohan-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,8301, 1);
				api_quest_SetJournalStep( pRoom, userObjID,8301, 1);
				api_quest_SetQuestStep( pRoom, userObjID,8301, 1);
				npc_talk_index = "n714_cleric_yohan-1";

	end
	if npc_talk_index == "n714_cleric_yohan-1-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 714, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq41_8301_fellow_of_pilgrimage_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8301);
end

function nq41_8301_fellow_of_pilgrimage_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8301);
end

function nq41_8301_fellow_of_pilgrimage_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8301);
	local questID=8301;
end

function nq41_8301_fellow_of_pilgrimage_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq41_8301_fellow_of_pilgrimage_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq41_8301_fellow_of_pilgrimage_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,8301, 1);
				api_quest_SetJournalStep( pRoom, userObjID,8301, 1);
				api_quest_SetQuestStep( pRoom, userObjID,8301, 1);
				npc_talk_index = "n714_cleric_yohan-1";
end

</GameServer>