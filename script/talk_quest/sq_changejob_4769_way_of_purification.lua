<VillageServer>

function sq_changejob_4769_way_of_purification_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 954 then
		sq_changejob_4769_way_of_purification_OnTalk_n954_darklair_rosetta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2018 then
		sq_changejob_4769_way_of_purification_OnTalk_n2018_darklair_parfait(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2022 then
		sq_changejob_4769_way_of_purification_OnTalk_n2022_darkavenger(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2023 then
		sq_changejob_4769_way_of_purification_OnTalk_n2023_darklair_parfait(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n954_darklair_rosetta--------------------------------------------------------------------------------
function sq_changejob_4769_way_of_purification_OnTalk_n954_darklair_rosetta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n954_darklair_rosetta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n954_darklair_rosetta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n954_darklair_rosetta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n954_darklair_rosetta-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n954_darklair_rosetta-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47690, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47690, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47690, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47690, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47690, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47690, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47690, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47690, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47690, false);
			 end 

	end
	if npc_talk_index == "n954_darklair_rosetta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4769, 1);
				api_quest_SetJournalStep(userObjID,4769, 1);
				api_quest_SetQuestStep(userObjID,4769, 1);
				npc_talk_index = "n954_darklair_rosetta-1";

	end
	if npc_talk_index == "n954_darklair_rosetta-1-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n954_darklair_rosetta-5-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-d" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-e" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-f" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-g" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47690, true);
				 api_quest_RewardQuestUser(userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47690, true);
				 api_quest_RewardQuestUser(userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47690, true);
				 api_quest_RewardQuestUser(userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47690, true);
				 api_quest_RewardQuestUser(userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47690, true);
				 api_quest_RewardQuestUser(userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47690, true);
				 api_quest_RewardQuestUser(userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47690, true);
				 api_quest_RewardQuestUser(userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47690, true);
				 api_quest_RewardQuestUser(userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47690, true);
				 api_quest_RewardQuestUser(userObjID, 47690, questID, 1);
			 end 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-i" then 
				api_user_SetUserJobID(userObjID, 81);

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2018_darklair_parfait--------------------------------------------------------------------------------
function sq_changejob_4769_way_of_purification_OnTalk_n2018_darklair_parfait(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2018_darklair_parfait-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n2018_darklair_parfait-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n2018_darklair_parfait-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2018_darklair_parfait-2-b" then 
	end
	if npc_talk_index == "n2018_darklair_parfait-2-c" then 
	end
	if npc_talk_index == "n2018_darklair_parfait-2-d" then 
	end
	if npc_talk_index == "n2018_darklair_parfait-2-e" then 
	end
	if npc_talk_index == "n2018_darklair_parfait-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n2018_darklair_parfait-2-move1" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									npc_talk_index = "n2018_darklair_parfait-2-g";

				end
	end
	if npc_talk_index == "n2018_darklair_parfait-3-move2" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									api_user_ChangeMap(userObjID,13035,2);

				else
									npc_talk_index = "n2018_darklair_parfait-3-a";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2022_darkavenger--------------------------------------------------------------------------------
function sq_changejob_4769_way_of_purification_OnTalk_n2022_darkavenger(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2022_darkavenger-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n2022_darkavenger-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2023_darklair_parfait--------------------------------------------------------------------------------
function sq_changejob_4769_way_of_purification_OnTalk_n2023_darklair_parfait(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2023_darklair_parfait-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n2023_darklair_parfait-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n2023_darklair_parfait-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2023_darklair_parfait-3-b" then 
	end
	if npc_talk_index == "n2023_darklair_parfait-3-c" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_changejob_4769_way_of_purification_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4769);
end

function sq_changejob_4769_way_of_purification_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4769);
end

function sq_changejob_4769_way_of_purification_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4769);
	local questID=4769;
end

function sq_changejob_4769_way_of_purification_OnRemoteStart( userObjID, questID )
end

function sq_changejob_4769_way_of_purification_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_changejob_4769_way_of_purification_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4769, 1);
				api_quest_SetJournalStep(userObjID,4769, 1);
				api_quest_SetQuestStep(userObjID,4769, 1);
				npc_talk_index = "n954_darklair_rosetta-1";
end

</VillageServer>

<GameServer>
function sq_changejob_4769_way_of_purification_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 954 then
		sq_changejob_4769_way_of_purification_OnTalk_n954_darklair_rosetta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2018 then
		sq_changejob_4769_way_of_purification_OnTalk_n2018_darklair_parfait( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2022 then
		sq_changejob_4769_way_of_purification_OnTalk_n2022_darkavenger( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2023 then
		sq_changejob_4769_way_of_purification_OnTalk_n2023_darklair_parfait( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n954_darklair_rosetta--------------------------------------------------------------------------------
function sq_changejob_4769_way_of_purification_OnTalk_n954_darklair_rosetta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n954_darklair_rosetta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n954_darklair_rosetta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n954_darklair_rosetta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n954_darklair_rosetta-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n954_darklair_rosetta-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, false);
			 end 

	end
	if npc_talk_index == "n954_darklair_rosetta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4769, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4769, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4769, 1);
				npc_talk_index = "n954_darklair_rosetta-1";

	end
	if npc_talk_index == "n954_darklair_rosetta-1-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n954_darklair_rosetta-5-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-d" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-e" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-f" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-g" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47690, questID, 1);
			 end 
	end
	if npc_talk_index == "n954_darklair_rosetta-5-i" then 
				api_user_SetUserJobID( pRoom, userObjID, 81);

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2018_darklair_parfait--------------------------------------------------------------------------------
function sq_changejob_4769_way_of_purification_OnTalk_n2018_darklair_parfait( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2018_darklair_parfait-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n2018_darklair_parfait-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n2018_darklair_parfait-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2018_darklair_parfait-2-b" then 
	end
	if npc_talk_index == "n2018_darklair_parfait-2-c" then 
	end
	if npc_talk_index == "n2018_darklair_parfait-2-d" then 
	end
	if npc_talk_index == "n2018_darklair_parfait-2-e" then 
	end
	if npc_talk_index == "n2018_darklair_parfait-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n2018_darklair_parfait-2-move1" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									npc_talk_index = "n2018_darklair_parfait-2-g";

				end
	end
	if npc_talk_index == "n2018_darklair_parfait-3-move2" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									api_user_ChangeMap( pRoom, userObjID,13035,2);

				else
									npc_talk_index = "n2018_darklair_parfait-3-a";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2022_darkavenger--------------------------------------------------------------------------------
function sq_changejob_4769_way_of_purification_OnTalk_n2022_darkavenger( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2022_darkavenger-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n2022_darkavenger-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2023_darklair_parfait--------------------------------------------------------------------------------
function sq_changejob_4769_way_of_purification_OnTalk_n2023_darklair_parfait( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2023_darklair_parfait-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n2023_darklair_parfait-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n2023_darklair_parfait-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2023_darklair_parfait-3-b" then 
	end
	if npc_talk_index == "n2023_darklair_parfait-3-c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_changejob_4769_way_of_purification_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4769);
end

function sq_changejob_4769_way_of_purification_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4769);
end

function sq_changejob_4769_way_of_purification_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4769);
	local questID=4769;
end

function sq_changejob_4769_way_of_purification_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_changejob_4769_way_of_purification_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_changejob_4769_way_of_purification_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4769, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4769, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4769, 1);
				npc_talk_index = "n954_darklair_rosetta-1";
end

</GameServer>