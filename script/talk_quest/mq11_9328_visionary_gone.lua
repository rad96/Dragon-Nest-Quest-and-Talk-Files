<VillageServer>

function mq11_9328_visionary_gone_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1166 then
		mq11_9328_visionary_gone_OnTalk_n1166_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1167 then
		mq11_9328_visionary_gone_OnTalk_n1167_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 209 then
		mq11_9328_visionary_gone_OnTalk_n209_elite_soldier(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 519 then
		mq11_9328_visionary_gone_OnTalk_n519_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 532 then
		mq11_9328_visionary_gone_OnTalk_n532_rambert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1166_lunaria--------------------------------------------------------------------------------
function mq11_9328_visionary_gone_OnTalk_n1166_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1166_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1167_academic--------------------------------------------------------------------------------
function mq11_9328_visionary_gone_OnTalk_n1167_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1167_academic-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1167_academic-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1167_academic-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1167_academic-3-b" then 
	end
	if npc_talk_index == "n1167_academic-3-c" then 
	end
	if npc_talk_index == "n1167_academic-3-d" then 
	end
	if npc_talk_index == "n1167_academic-3-e" then 
	end
	if npc_talk_index == "n1167_academic-3-f" then 
	end
	if npc_talk_index == "n1167_academic-3-g" then 
	end
	if npc_talk_index == "n1167_academic-3-h" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n209_elite_soldier--------------------------------------------------------------------------------
function mq11_9328_visionary_gone_OnTalk_n209_elite_soldier(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n209_elite_soldier-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n209_elite_soldier-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n209_elite_soldier-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9328, 2);
				api_quest_SetJournalStep(userObjID,9328, 1);
				api_quest_SetQuestStep(userObjID,9328, 1);
				npc_talk_index = "n209_elite_soldier-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n519_cian--------------------------------------------------------------------------------
function mq11_9328_visionary_gone_OnTalk_n519_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n519_cian-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n519_cian-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n519_cian-4-b" then 
	end
	if npc_talk_index == "n519_cian-4-c" then 
	end
	if npc_talk_index == "n519_cian-4-c" then 
	end
	if npc_talk_index == "n519_cian-4-c" then 
	end
	if npc_talk_index == "n519_cian-4-d" then 
	end
	if npc_talk_index == "n519_cian-4-e" then 
	end
	if npc_talk_index == "n519_cian-4-f" then 
	end
	if npc_talk_index == "n519_cian-4-f" then 
	end
	if npc_talk_index == "n519_cian-4-g" then 
	end
	if npc_talk_index == "n519_cian-4-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93280, true);
				 api_quest_RewardQuestUser(userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93280, true);
				 api_quest_RewardQuestUser(userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93280, true);
				 api_quest_RewardQuestUser(userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93280, true);
				 api_quest_RewardQuestUser(userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93280, true);
				 api_quest_RewardQuestUser(userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93280, true);
				 api_quest_RewardQuestUser(userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93280, true);
				 api_quest_RewardQuestUser(userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93280, true);
				 api_quest_RewardQuestUser(userObjID, 93280, questID, 1);
			 end 
	end
	if npc_talk_index == "n519_cian-4-i" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9329, 2);
					api_quest_SetQuestStep(userObjID, 9329, 1);
					api_quest_SetJournalStep(userObjID, 9329, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n519_cian-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n519_cian-1", "mq11_9329_clue_of_curse.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n532_rambert--------------------------------------------------------------------------------
function mq11_9328_visionary_gone_OnTalk_n532_rambert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n532_rambert-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n532_rambert-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n532_rambert-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n532_rambert-2-b" then 
	end
	if npc_talk_index == "n532_rambert-2-c" then 
	end
	if npc_talk_index == "n532_rambert-2-d" then 
	end
	if npc_talk_index == "n532_rambert-2-e" then 
	end
	if npc_talk_index == "n532_rambert-2-f" then 
	end
	if npc_talk_index == "n532_rambert-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_9328_visionary_gone_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9328);
end

function mq11_9328_visionary_gone_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9328);
end

function mq11_9328_visionary_gone_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9328);
	local questID=9328;
end

function mq11_9328_visionary_gone_OnRemoteStart( userObjID, questID )
end

function mq11_9328_visionary_gone_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq11_9328_visionary_gone_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9328, 2);
				api_quest_SetJournalStep(userObjID,9328, 1);
				api_quest_SetQuestStep(userObjID,9328, 1);
				npc_talk_index = "n209_elite_soldier-1";
end

</VillageServer>

<GameServer>
function mq11_9328_visionary_gone_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1166 then
		mq11_9328_visionary_gone_OnTalk_n1166_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1167 then
		mq11_9328_visionary_gone_OnTalk_n1167_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 209 then
		mq11_9328_visionary_gone_OnTalk_n209_elite_soldier( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 519 then
		mq11_9328_visionary_gone_OnTalk_n519_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 532 then
		mq11_9328_visionary_gone_OnTalk_n532_rambert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1166_lunaria--------------------------------------------------------------------------------
function mq11_9328_visionary_gone_OnTalk_n1166_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1166_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1167_academic--------------------------------------------------------------------------------
function mq11_9328_visionary_gone_OnTalk_n1167_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1167_academic-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1167_academic-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1167_academic-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1167_academic-3-b" then 
	end
	if npc_talk_index == "n1167_academic-3-c" then 
	end
	if npc_talk_index == "n1167_academic-3-d" then 
	end
	if npc_talk_index == "n1167_academic-3-e" then 
	end
	if npc_talk_index == "n1167_academic-3-f" then 
	end
	if npc_talk_index == "n1167_academic-3-g" then 
	end
	if npc_talk_index == "n1167_academic-3-h" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n209_elite_soldier--------------------------------------------------------------------------------
function mq11_9328_visionary_gone_OnTalk_n209_elite_soldier( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n209_elite_soldier-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n209_elite_soldier-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n209_elite_soldier-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9328, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9328, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9328, 1);
				npc_talk_index = "n209_elite_soldier-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n519_cian--------------------------------------------------------------------------------
function mq11_9328_visionary_gone_OnTalk_n519_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n519_cian-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n519_cian-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n519_cian-4-b" then 
	end
	if npc_talk_index == "n519_cian-4-c" then 
	end
	if npc_talk_index == "n519_cian-4-c" then 
	end
	if npc_talk_index == "n519_cian-4-c" then 
	end
	if npc_talk_index == "n519_cian-4-d" then 
	end
	if npc_talk_index == "n519_cian-4-e" then 
	end
	if npc_talk_index == "n519_cian-4-f" then 
	end
	if npc_talk_index == "n519_cian-4-f" then 
	end
	if npc_talk_index == "n519_cian-4-g" then 
	end
	if npc_talk_index == "n519_cian-4-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93280, questID, 1);
			 end 
	end
	if npc_talk_index == "n519_cian-4-i" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9329, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9329, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9329, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n519_cian-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n519_cian-1", "mq11_9329_clue_of_curse.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n532_rambert--------------------------------------------------------------------------------
function mq11_9328_visionary_gone_OnTalk_n532_rambert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n532_rambert-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n532_rambert-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n532_rambert-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n532_rambert-2-b" then 
	end
	if npc_talk_index == "n532_rambert-2-c" then 
	end
	if npc_talk_index == "n532_rambert-2-d" then 
	end
	if npc_talk_index == "n532_rambert-2-e" then 
	end
	if npc_talk_index == "n532_rambert-2-f" then 
	end
	if npc_talk_index == "n532_rambert-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_9328_visionary_gone_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9328);
end

function mq11_9328_visionary_gone_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9328);
end

function mq11_9328_visionary_gone_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9328);
	local questID=9328;
end

function mq11_9328_visionary_gone_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq11_9328_visionary_gone_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq11_9328_visionary_gone_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9328, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9328, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9328, 1);
				npc_talk_index = "n209_elite_soldier-1";
end

</GameServer>