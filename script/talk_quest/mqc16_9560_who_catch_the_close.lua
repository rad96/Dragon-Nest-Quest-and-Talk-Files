<VillageServer>

function mqc16_9560_who_catch_the_close_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 40 then
		mqc16_9560_who_catch_the_close_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mqc16_9560_who_catch_the_close_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 41 then
		mqc16_9560_who_catch_the_close_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mqc16_9560_who_catch_the_close_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mqc16_9560_who_catch_the_close_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mqc16_9560_who_catch_the_close_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 249 then
		mqc16_9560_who_catch_the_close_OnTalk_n249_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mqc16_9560_who_catch_the_close_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n040_king_casius--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n040_king_casius-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n040_king_casius-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n040_king_casius-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n040_king_casius-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n040_king_casius-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n040_king_casius-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n040_king_casius-1-b" then 
	end
	if npc_talk_index == "n040_king_casius-1-b" then 
	end
	if npc_talk_index == "n040_king_casius-1-b" then 
	end
	if npc_talk_index == "n040_king_casius-1-c" then 
	end
	if npc_talk_index == "n040_king_casius-1-d" then 
	end
	if npc_talk_index == "n040_king_casius-1-e" then 
	end
	if npc_talk_index == "n040_king_casius-1-f" then 
	end
	if npc_talk_index == "n040_king_casius-1-g" then 
	end
	if npc_talk_index == "n040_king_casius-1-h" then 
	end
	if npc_talk_index == "n040_king_casius-1-i" then 
	end
	if npc_talk_index == "n040_king_casius-1-j" then 
	end
	if npc_talk_index == "n040_king_casius-1-k" then 
	end
	if npc_talk_index == "n040_king_casius-1-l" then 
	end
	if npc_talk_index == "n040_king_casius-1-m" then 
	end
	if npc_talk_index == "n040_king_casius-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n040_king_casius-2-b" then 
	end
	if npc_talk_index == "n040_king_casius-2-c" then 
	end
	if npc_talk_index == "n040_king_casius-2-c" then 
	end
	if npc_talk_index == "n040_king_casius-2-d" then 
	end
	if npc_talk_index == "n040_king_casius-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n040_king_casius-3" then 
	end
	if npc_talk_index == "n040_king_casius-3-c" then 
	end
	if npc_talk_index == "n040_king_casius-3-d" then 
	end
	if npc_talk_index == "n040_king_casius-3-e" then 
	end
	if npc_talk_index == "n040_king_casius-3-f" then 
	end
	if npc_talk_index == "n040_king_casius-3-g" then 
	end
	if npc_talk_index == "n040_king_casius-3-h" then 
	end
	if npc_talk_index == "n040_king_casius-3-i" then 
	end
	if npc_talk_index == "n040_king_casius-3-j" then 
	end
	if npc_talk_index == "n040_king_casius-3-k" then 
	end
	if npc_talk_index == "n040_king_casius-3-k" then 
	end
	if npc_talk_index == "n040_king_casius-3-l" then 
	end
	if npc_talk_index == "n040_king_casius-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n040_king_casius-5-b" then 
	end
	if npc_talk_index == "n040_king_casius-5-c" then 
	end
	if npc_talk_index == "n040_king_casius-5-d" then 
	end
	if npc_talk_index == "n040_king_casius-5-e" then 
	end
	if npc_talk_index == "n040_king_casius-5-f" then 
	end
	if npc_talk_index == "n040_king_casius-5-g" then 
	end
	if npc_talk_index == "n040_king_casius-5-h" then 
	end
	if npc_talk_index == "n040_king_casius-5-i" then 
	end
	if npc_talk_index == "n040_king_casius-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
	end
	if npc_talk_index == "n040_king_casius-6-b" then 
	end
	if npc_talk_index == "n040_king_casius-6-c" then 
	end
	if npc_talk_index == "n040_king_casius-6-d" then 
	end
	if npc_talk_index == "n040_king_casius-6-e" then 
	end
	if npc_talk_index == "n040_king_casius-6-f" then 
	end
	if npc_talk_index == "n040_king_casius-6-g" then 
	end
	if npc_talk_index == "n040_king_casius-6-h" then 
	end
	if npc_talk_index == "n040_king_casius-6-i" then 
	end
	if npc_talk_index == "n040_king_casius-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n038_royal_magician_kalaen-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n038_royal_magician_kalaen-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-7-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-e" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-g" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-h" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-i" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n041_duke_stwart--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n041_duke_stwart-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n041_duke_stwart-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-4-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-4-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-4-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-4-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-4-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-4-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n041_duke_stwart-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n822_teramai-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n822_teramai-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-8-b" then 
	end
	if npc_talk_index == "n822_teramai-8-c" then 
	end
	if npc_talk_index == "n822_teramai-9" then 
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end
	if npc_talk_index == "n822_teramai-9" then 
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n1067_elder_elf-10";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1067_elder_elf-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,9560, 2);
				api_quest_SetJournalStep(userObjID,9560, 1);
				api_quest_SetQuestStep(userObjID,9560, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-9-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-9-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-9-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-9-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-10" then 
				api_quest_SetQuestStep(userObjID, questID,10);
				api_quest_SetJournalStep(userObjID, questID, 8);
	end
	if npc_talk_index == "n1067_elder_elf-10" then 
				api_quest_SetQuestStep(userObjID, questID,10);
				api_quest_SetJournalStep(userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n249_saint_guard--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n249_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n249_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n249_saint_guard-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n249_saint_guard-accepting-acceptted_1" then
				api_quest_AddQuest(userObjID,9560, 2);
				api_quest_SetJournalStep(userObjID,9560, 1);
				api_quest_SetQuestStep(userObjID,9560, 1);
				npc_talk_index = "n249_saint_guard-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n1065_geraint_kid-10";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1065_geraint_kid-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-10-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95600, true);
				 api_quest_RewardQuestUser(userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95600, true);
				 api_quest_RewardQuestUser(userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95600, true);
				 api_quest_RewardQuestUser(userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95600, true);
				 api_quest_RewardQuestUser(userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95600, true);
				 api_quest_RewardQuestUser(userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95600, true);
				 api_quest_RewardQuestUser(userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95600, true);
				 api_quest_RewardQuestUser(userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95600, true);
				 api_quest_RewardQuestUser(userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95600, true);
				 api_quest_RewardQuestUser(userObjID, 95600, questID, 1);
			 end 
	end
	if npc_talk_index == "n1065_geraint_kid-10-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9561, 2);
					api_quest_SetQuestStep(userObjID, 9561, 1);
					api_quest_SetJournalStep(userObjID, 9561, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1065_geraint_kid-10-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1065_geraint_kid-1", "mqc16_9561_princess_and_knight.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9560_who_catch_the_close_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9560);
end

function mqc16_9560_who_catch_the_close_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9560);
end

function mqc16_9560_who_catch_the_close_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9560);
	local questID=9560;
end

function mqc16_9560_who_catch_the_close_OnRemoteStart( userObjID, questID )
end

function mqc16_9560_who_catch_the_close_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc16_9560_who_catch_the_close_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mqc16_9560_who_catch_the_close_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 40 then
		mqc16_9560_who_catch_the_close_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mqc16_9560_who_catch_the_close_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 41 then
		mqc16_9560_who_catch_the_close_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mqc16_9560_who_catch_the_close_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mqc16_9560_who_catch_the_close_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mqc16_9560_who_catch_the_close_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 249 then
		mqc16_9560_who_catch_the_close_OnTalk_n249_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mqc16_9560_who_catch_the_close_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n040_king_casius--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n040_king_casius-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n040_king_casius-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n040_king_casius-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n040_king_casius-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n040_king_casius-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n040_king_casius-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n040_king_casius-1-b" then 
	end
	if npc_talk_index == "n040_king_casius-1-b" then 
	end
	if npc_talk_index == "n040_king_casius-1-b" then 
	end
	if npc_talk_index == "n040_king_casius-1-c" then 
	end
	if npc_talk_index == "n040_king_casius-1-d" then 
	end
	if npc_talk_index == "n040_king_casius-1-e" then 
	end
	if npc_talk_index == "n040_king_casius-1-f" then 
	end
	if npc_talk_index == "n040_king_casius-1-g" then 
	end
	if npc_talk_index == "n040_king_casius-1-h" then 
	end
	if npc_talk_index == "n040_king_casius-1-i" then 
	end
	if npc_talk_index == "n040_king_casius-1-j" then 
	end
	if npc_talk_index == "n040_king_casius-1-k" then 
	end
	if npc_talk_index == "n040_king_casius-1-l" then 
	end
	if npc_talk_index == "n040_king_casius-1-m" then 
	end
	if npc_talk_index == "n040_king_casius-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n040_king_casius-2-b" then 
	end
	if npc_talk_index == "n040_king_casius-2-c" then 
	end
	if npc_talk_index == "n040_king_casius-2-c" then 
	end
	if npc_talk_index == "n040_king_casius-2-d" then 
	end
	if npc_talk_index == "n040_king_casius-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n040_king_casius-3" then 
	end
	if npc_talk_index == "n040_king_casius-3-c" then 
	end
	if npc_talk_index == "n040_king_casius-3-d" then 
	end
	if npc_talk_index == "n040_king_casius-3-e" then 
	end
	if npc_talk_index == "n040_king_casius-3-f" then 
	end
	if npc_talk_index == "n040_king_casius-3-g" then 
	end
	if npc_talk_index == "n040_king_casius-3-h" then 
	end
	if npc_talk_index == "n040_king_casius-3-i" then 
	end
	if npc_talk_index == "n040_king_casius-3-j" then 
	end
	if npc_talk_index == "n040_king_casius-3-k" then 
	end
	if npc_talk_index == "n040_king_casius-3-k" then 
	end
	if npc_talk_index == "n040_king_casius-3-l" then 
	end
	if npc_talk_index == "n040_king_casius-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n040_king_casius-5-b" then 
	end
	if npc_talk_index == "n040_king_casius-5-c" then 
	end
	if npc_talk_index == "n040_king_casius-5-d" then 
	end
	if npc_talk_index == "n040_king_casius-5-e" then 
	end
	if npc_talk_index == "n040_king_casius-5-f" then 
	end
	if npc_talk_index == "n040_king_casius-5-g" then 
	end
	if npc_talk_index == "n040_king_casius-5-h" then 
	end
	if npc_talk_index == "n040_king_casius-5-i" then 
	end
	if npc_talk_index == "n040_king_casius-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
	end
	if npc_talk_index == "n040_king_casius-6-b" then 
	end
	if npc_talk_index == "n040_king_casius-6-c" then 
	end
	if npc_talk_index == "n040_king_casius-6-d" then 
	end
	if npc_talk_index == "n040_king_casius-6-e" then 
	end
	if npc_talk_index == "n040_king_casius-6-f" then 
	end
	if npc_talk_index == "n040_king_casius-6-g" then 
	end
	if npc_talk_index == "n040_king_casius-6-h" then 
	end
	if npc_talk_index == "n040_king_casius-6-i" then 
	end
	if npc_talk_index == "n040_king_casius-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n038_royal_magician_kalaen-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n038_royal_magician_kalaen-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-7-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-e" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-g" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-h" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-7-i" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n041_duke_stwart--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n041_duke_stwart-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n041_duke_stwart-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-4-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-4-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-4-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-4-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-4-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-4-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n041_duke_stwart-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n822_teramai-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n822_teramai-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-8-b" then 
	end
	if npc_talk_index == "n822_teramai-8-c" then 
	end
	if npc_talk_index == "n822_teramai-9" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end
	if npc_talk_index == "n822_teramai-9" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n1067_elder_elf-10";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1067_elder_elf-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,9560, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9560, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9560, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-9-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-9-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-9-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-9-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-10" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,10);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);
	end
	if npc_talk_index == "n1067_elder_elf-10" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,10);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n249_saint_guard--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n249_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n249_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n249_saint_guard-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n249_saint_guard-accepting-acceptted_1" then
				api_quest_AddQuest( pRoom, userObjID,9560, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9560, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9560, 1);
				npc_talk_index = "n249_saint_guard-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc16_9560_who_catch_the_close_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n1065_geraint_kid-10";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1065_geraint_kid-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-10-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95600, questID, 1);
			 end 
	end
	if npc_talk_index == "n1065_geraint_kid-10-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9561, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9561, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9561, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1065_geraint_kid-10-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1065_geraint_kid-1", "mqc16_9561_princess_and_knight.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9560_who_catch_the_close_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9560);
end

function mqc16_9560_who_catch_the_close_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9560);
end

function mqc16_9560_who_catch_the_close_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9560);
	local questID=9560;
end

function mqc16_9560_who_catch_the_close_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc16_9560_who_catch_the_close_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc16_9560_who_catch_the_close_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>