<VillageServer>

function mq15_9245_my_mission_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 393 then
		mq15_9245_my_mission_OnTalk_n393_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 528 then
		mq15_9245_my_mission_OnTalk_n528_acamedic_jasmin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n393_academic_station--------------------------------------------------------------------------------
function mq15_9245_my_mission_OnTalk_n393_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n393_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n393_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n393_academic_station-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n393_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9245, 2);
				api_quest_SetJournalStep(userObjID,9245, 1);
				api_quest_SetQuestStep(userObjID,9245, 1);
				npc_talk_index = "n393_academic_station-1";

	end
	if npc_talk_index == "n393_academic_station-2-chk_lv" then 
				if api_user_GetUserLevel(userObjID) >= 58 then
									npc_talk_index = "n393_academic_station-2-a";

				else
									npc_talk_index = "n393_academic_station-2-r";

				end
	end
	if npc_talk_index == "n393_academic_station-2-b" then 
	end
	if npc_talk_index == "n393_academic_station-2-c" then 
	end
	if npc_talk_index == "n393_academic_station-2-d" then 
	end
	if npc_talk_index == "n393_academic_station-2-e" then 
	end
	if npc_talk_index == "n393_academic_station-2-f" then 
	end
	if npc_talk_index == "n393_academic_station-2-g" then 
	end
	if npc_talk_index == "n393_academic_station-2-h" then 
	end
	if npc_talk_index == "n393_academic_station-2-i" then 
	end
	if npc_talk_index == "n393_academic_station-2-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92450, true);
				 api_quest_RewardQuestUser(userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92450, true);
				 api_quest_RewardQuestUser(userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92450, true);
				 api_quest_RewardQuestUser(userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92450, true);
				 api_quest_RewardQuestUser(userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92450, true);
				 api_quest_RewardQuestUser(userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92450, true);
				 api_quest_RewardQuestUser(userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92450, true);
				 api_quest_RewardQuestUser(userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92450, true);
				 api_quest_RewardQuestUser(userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92450, true);
				 api_quest_RewardQuestUser(userObjID, 92450, questID, 1);
			 end 
	end
	if npc_talk_index == "n393_academic_station-2-k" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9251, 2);
					api_quest_SetQuestStep(userObjID, 9251, 1);
					api_quest_SetJournalStep(userObjID, 9251, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n393_academic_station-2-l" then 
	end
	if npc_talk_index == "n393_academic_station-2-m" then 
	end
	if npc_talk_index == "n393_academic_station-2-n" then 
	end
	if npc_talk_index == "n393_academic_station-2-o" then 
	end
	if npc_talk_index == "n393_academic_station-2-p" then 
	end
	if npc_talk_index == "n393_academic_station-2-q" then 
	end
	if npc_talk_index == "n393_academic_station-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n393_academic_station-1", "mq15_9251_update.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n528_acamedic_jasmin--------------------------------------------------------------------------------
function mq15_9245_my_mission_OnTalk_n528_acamedic_jasmin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9245_my_mission_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9245);
end

function mq15_9245_my_mission_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9245);
end

function mq15_9245_my_mission_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9245);
	local questID=9245;
end

function mq15_9245_my_mission_OnRemoteStart( userObjID, questID )
end

function mq15_9245_my_mission_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9245_my_mission_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9245, 2);
				api_quest_SetJournalStep(userObjID,9245, 1);
				api_quest_SetQuestStep(userObjID,9245, 1);
				npc_talk_index = "n393_academic_station-1";
end

</VillageServer>

<GameServer>
function mq15_9245_my_mission_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 393 then
		mq15_9245_my_mission_OnTalk_n393_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 528 then
		mq15_9245_my_mission_OnTalk_n528_acamedic_jasmin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n393_academic_station--------------------------------------------------------------------------------
function mq15_9245_my_mission_OnTalk_n393_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n393_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n393_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n393_academic_station-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n393_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9245, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9245, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9245, 1);
				npc_talk_index = "n393_academic_station-1";

	end
	if npc_talk_index == "n393_academic_station-2-chk_lv" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 58 then
									npc_talk_index = "n393_academic_station-2-a";

				else
									npc_talk_index = "n393_academic_station-2-r";

				end
	end
	if npc_talk_index == "n393_academic_station-2-b" then 
	end
	if npc_talk_index == "n393_academic_station-2-c" then 
	end
	if npc_talk_index == "n393_academic_station-2-d" then 
	end
	if npc_talk_index == "n393_academic_station-2-e" then 
	end
	if npc_talk_index == "n393_academic_station-2-f" then 
	end
	if npc_talk_index == "n393_academic_station-2-g" then 
	end
	if npc_talk_index == "n393_academic_station-2-h" then 
	end
	if npc_talk_index == "n393_academic_station-2-i" then 
	end
	if npc_talk_index == "n393_academic_station-2-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92450, questID, 1);
			 end 
	end
	if npc_talk_index == "n393_academic_station-2-k" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9251, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9251, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9251, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n393_academic_station-2-l" then 
	end
	if npc_talk_index == "n393_academic_station-2-m" then 
	end
	if npc_talk_index == "n393_academic_station-2-n" then 
	end
	if npc_talk_index == "n393_academic_station-2-o" then 
	end
	if npc_talk_index == "n393_academic_station-2-p" then 
	end
	if npc_talk_index == "n393_academic_station-2-q" then 
	end
	if npc_talk_index == "n393_academic_station-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n393_academic_station-1", "mq15_9251_update.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n528_acamedic_jasmin--------------------------------------------------------------------------------
function mq15_9245_my_mission_OnTalk_n528_acamedic_jasmin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9245_my_mission_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9245);
end

function mq15_9245_my_mission_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9245);
end

function mq15_9245_my_mission_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9245);
	local questID=9245;
end

function mq15_9245_my_mission_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9245_my_mission_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9245_my_mission_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9245, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9245, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9245, 1);
				npc_talk_index = "n393_academic_station-1";
end

</GameServer>