<VillageServer>

function sq05_157_the_reverse_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1225 then
		sq05_157_the_reverse_OnTalk_n1225_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1226 then
		sq05_157_the_reverse_OnTalk_n1226_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 13 then
		sq05_157_the_reverse_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 175 then
		sq05_157_the_reverse_OnTalk_n175_cleric_cedric(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 60 then
		sq05_157_the_reverse_OnTalk_n060_guard_cleric_alcane(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1225_cleric_tomas--------------------------------------------------------------------------------
function sq05_157_the_reverse_OnTalk_n1225_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1225_cleric_tomas-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1225_cleric_tomas-1-change_map1" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13028,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1226_cleric_tomas--------------------------------------------------------------------------------
function sq05_157_the_reverse_OnTalk_n1226_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1226_cleric_tomas-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1226_cleric_tomas-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1226_cleric_tomas-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1226_cleric_tomas-5-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1570, true);
				 api_quest_RewardQuestUser(userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1570, true);
				 api_quest_RewardQuestUser(userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1570, true);
				 api_quest_RewardQuestUser(userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1570, true);
				 api_quest_RewardQuestUser(userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1570, true);
				 api_quest_RewardQuestUser(userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1570, true);
				 api_quest_RewardQuestUser(userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1570, true);
				 api_quest_RewardQuestUser(userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1570, true);
				 api_quest_RewardQuestUser(userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1570, true);
				 api_quest_RewardQuestUser(userObjID, 1570, questID, 1);
			 end 
	end
	if npc_talk_index == "n1226_cleric_tomas-5-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 201, 2);
					api_quest_SetQuestStep(userObjID, 201, 1);
					api_quest_SetJournalStep(userObjID, 201, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n013_cleric_tomas--------------------------------------------------------------------------------
function sq05_157_the_reverse_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n013_cleric_tomas-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n013_cleric_tomas-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n013_cleric_tomas-5";
				api_user_ChangeMap(userObjID,13028,1);
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-accepting-acceptted" then
				npc_talk_index = "n013_cleric_tomas-1";
				api_quest_AddQuest(userObjID,157, 2);
				api_quest_SetJournalStep(userObjID,157, 1);
				api_quest_SetQuestStep(userObjID,157, 1);

	end
	if npc_talk_index == "n013_cleric_tomas-accepting-acceptted2" then
				npc_talk_index = "n013_cleric_tomas-1";
				api_quest_AddQuest(userObjID,157, 2);
				api_quest_SetJournalStep(userObjID,157, 1);
				api_quest_SetQuestStep(userObjID,157, 1);

	end
	if npc_talk_index == "n013_cleric_tomas-1-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-party_ch01" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n013_cleric_tomas-1-d";

				else
									npc_talk_index = "n013_cleric_tomas-1-e";

				end
	end
	if npc_talk_index == "n013_cleric_tomas-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n013_cleric_tomas-2-move_02" then 
				api_user_ChangeMap(userObjID,13028,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n175_cleric_cedric--------------------------------------------------------------------------------
function sq05_157_the_reverse_OnTalk_n175_cleric_cedric(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n175_cleric_cedric-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n175_cleric_cedric-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n175_cleric_cedric-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n175_cleric_cedric-4-b" then 
	end
	if npc_talk_index == "n175_cleric_cedric-4-c" then 
	end
	if npc_talk_index == "n175_cleric_cedric-4-d" then 
	end
	if npc_talk_index == "n175_cleric_cedric-4-e" then 
	end
	if npc_talk_index == "n175_cleric_cedric-4-f" then 
	end
	if npc_talk_index == "n175_cleric_cedric-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n060_guard_cleric_alcane--------------------------------------------------------------------------------
function sq05_157_the_reverse_OnTalk_n060_guard_cleric_alcane(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n060_guard_cleric_alcane-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n060_guard_cleric_alcane-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n060_guard_cleric_alcane-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_157_the_reverse_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 157);
end

function sq05_157_the_reverse_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 157);
end

function sq05_157_the_reverse_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 157);
	local questID=157;
end

function sq05_157_the_reverse_OnRemoteStart( userObjID, questID )
end

function sq05_157_the_reverse_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq05_157_the_reverse_ForceAccept( userObjID, npcObjID, questID )
				npc_talk_index = "n013_cleric_tomas-1";
				api_quest_AddQuest(userObjID,157, 2);
				api_quest_SetJournalStep(userObjID,157, 1);
				api_quest_SetQuestStep(userObjID,157, 1);
end

</VillageServer>

<GameServer>
function sq05_157_the_reverse_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1225 then
		sq05_157_the_reverse_OnTalk_n1225_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1226 then
		sq05_157_the_reverse_OnTalk_n1226_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 13 then
		sq05_157_the_reverse_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 175 then
		sq05_157_the_reverse_OnTalk_n175_cleric_cedric( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 60 then
		sq05_157_the_reverse_OnTalk_n060_guard_cleric_alcane( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1225_cleric_tomas--------------------------------------------------------------------------------
function sq05_157_the_reverse_OnTalk_n1225_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1225_cleric_tomas-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1225_cleric_tomas-1-change_map1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13028,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1226_cleric_tomas--------------------------------------------------------------------------------
function sq05_157_the_reverse_OnTalk_n1226_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1226_cleric_tomas-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1226_cleric_tomas-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1226_cleric_tomas-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1226_cleric_tomas-5-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1570, questID, 1);
			 end 
	end
	if npc_talk_index == "n1226_cleric_tomas-5-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 201, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 201, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 201, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n013_cleric_tomas--------------------------------------------------------------------------------
function sq05_157_the_reverse_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n013_cleric_tomas-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n013_cleric_tomas-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n013_cleric_tomas-5";
				api_user_ChangeMap( pRoom, userObjID,13028,1);
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-accepting-acceptted" then
				npc_talk_index = "n013_cleric_tomas-1";
				api_quest_AddQuest( pRoom, userObjID,157, 2);
				api_quest_SetJournalStep( pRoom, userObjID,157, 1);
				api_quest_SetQuestStep( pRoom, userObjID,157, 1);

	end
	if npc_talk_index == "n013_cleric_tomas-accepting-acceptted2" then
				npc_talk_index = "n013_cleric_tomas-1";
				api_quest_AddQuest( pRoom, userObjID,157, 2);
				api_quest_SetJournalStep( pRoom, userObjID,157, 1);
				api_quest_SetQuestStep( pRoom, userObjID,157, 1);

	end
	if npc_talk_index == "n013_cleric_tomas-1-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-party_ch01" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n013_cleric_tomas-1-d";

				else
									npc_talk_index = "n013_cleric_tomas-1-e";

				end
	end
	if npc_talk_index == "n013_cleric_tomas-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n013_cleric_tomas-2-move_02" then 
				api_user_ChangeMap( pRoom, userObjID,13028,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n175_cleric_cedric--------------------------------------------------------------------------------
function sq05_157_the_reverse_OnTalk_n175_cleric_cedric( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n175_cleric_cedric-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n175_cleric_cedric-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n175_cleric_cedric-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n175_cleric_cedric-4-b" then 
	end
	if npc_talk_index == "n175_cleric_cedric-4-c" then 
	end
	if npc_talk_index == "n175_cleric_cedric-4-d" then 
	end
	if npc_talk_index == "n175_cleric_cedric-4-e" then 
	end
	if npc_talk_index == "n175_cleric_cedric-4-f" then 
	end
	if npc_talk_index == "n175_cleric_cedric-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n060_guard_cleric_alcane--------------------------------------------------------------------------------
function sq05_157_the_reverse_OnTalk_n060_guard_cleric_alcane( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n060_guard_cleric_alcane-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n060_guard_cleric_alcane-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n060_guard_cleric_alcane-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_157_the_reverse_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 157);
end

function sq05_157_the_reverse_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 157);
end

function sq05_157_the_reverse_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 157);
	local questID=157;
end

function sq05_157_the_reverse_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq05_157_the_reverse_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq05_157_the_reverse_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				npc_talk_index = "n013_cleric_tomas-1";
				api_quest_AddQuest( pRoom, userObjID,157, 2);
				api_quest_SetJournalStep( pRoom, userObjID,157, 1);
				api_quest_SetQuestStep( pRoom, userObjID,157, 1);
end

</GameServer>