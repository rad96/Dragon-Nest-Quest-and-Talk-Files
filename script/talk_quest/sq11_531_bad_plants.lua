<VillageServer>

function sq11_531_bad_plants_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 98 then
		sq11_531_bad_plants_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq11_531_bad_plants_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n098_warrior_master_lodrigo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5311, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5312, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5313, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5314, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5315, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5316, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5317, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5318, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5319, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-accepted" then
				api_quest_AddQuest(userObjID,531, 1);
				api_quest_SetJournalStep(userObjID,531, 1);
				api_quest_SetQuestStep(userObjID,531, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 977, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 303503, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400421, 1);

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-b" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5311, true);
				 api_quest_RewardQuestUser(userObjID, 5311, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5312, true);
				 api_quest_RewardQuestUser(userObjID, 5312, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5313, true);
				 api_quest_RewardQuestUser(userObjID, 5313, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5314, true);
				 api_quest_RewardQuestUser(userObjID, 5314, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5315, true);
				 api_quest_RewardQuestUser(userObjID, 5315, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5316, true);
				 api_quest_RewardQuestUser(userObjID, 5316, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5317, true);
				 api_quest_RewardQuestUser(userObjID, 5317, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5318, true);
				 api_quest_RewardQuestUser(userObjID, 5318, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5319, true);
				 api_quest_RewardQuestUser(userObjID, 5319, questID, 1);
			 end 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-d" then 

				if api_quest_HasQuestItem(userObjID, 400421, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400421, api_quest_HasQuestItem(userObjID, 400421, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_531_bad_plants_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 531);
	if qstep == 1 and CountIndex == 977 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400421, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400421, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400421 then

	end
	if qstep == 1 and CountIndex == 303503 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400421, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400421, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_531_bad_plants_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 531);
	if qstep == 1 and CountIndex == 977 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400421 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 303503 and Count >= TargetCount  then

	end
end

function sq11_531_bad_plants_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 531);
	local questID=531;
end

function sq11_531_bad_plants_OnRemoteStart( userObjID, questID )
end

function sq11_531_bad_plants_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_531_bad_plants_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_531_bad_plants_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 98 then
		sq11_531_bad_plants_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq11_531_bad_plants_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n098_warrior_master_lodrigo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5311, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5312, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5313, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5314, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5315, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5316, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5317, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5318, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5319, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-accepted" then
				api_quest_AddQuest( pRoom, userObjID,531, 1);
				api_quest_SetJournalStep( pRoom, userObjID,531, 1);
				api_quest_SetQuestStep( pRoom, userObjID,531, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 977, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 303503, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400421, 1);

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-b" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5311, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5311, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5312, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5312, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5313, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5313, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5314, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5314, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5315, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5315, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5316, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5316, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5317, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5317, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5318, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5318, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5319, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5319, questID, 1);
			 end 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-2-d" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400421, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400421, api_quest_HasQuestItem( pRoom, userObjID, 400421, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_531_bad_plants_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 531);
	if qstep == 1 and CountIndex == 977 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400421, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400421, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400421 then

	end
	if qstep == 1 and CountIndex == 303503 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400421, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400421, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_531_bad_plants_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 531);
	if qstep == 1 and CountIndex == 977 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400421 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 303503 and Count >= TargetCount  then

	end
end

function sq11_531_bad_plants_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 531);
	local questID=531;
end

function sq11_531_bad_plants_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_531_bad_plants_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_531_bad_plants_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>