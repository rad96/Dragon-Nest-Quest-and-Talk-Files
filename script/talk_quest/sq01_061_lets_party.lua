<VillageServer>

function sq01_061_lets_party_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 3 then
		sq01_061_lets_party_OnTalk_n003_trader_dolis(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n003_trader_dolis--------------------------------------------------------------------------------
function sq01_061_lets_party_OnTalk_n003_trader_dolis(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n003_trader_dolis-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n003_trader_dolis-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n003_trader_dolis-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n003_trader_dolis-3";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 611, true);
				 api_quest_RewardQuestUser(userObjID, 611, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 612, true);
				 api_quest_RewardQuestUser(userObjID, 612, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 612, true);
				 api_quest_RewardQuestUser(userObjID, 612, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 612, true);
				 api_quest_RewardQuestUser(userObjID, 612, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 611, true);
				 api_quest_RewardQuestUser(userObjID, 611, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 616, true);
				 api_quest_RewardQuestUser(userObjID, 616, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 617, true);
				 api_quest_RewardQuestUser(userObjID, 617, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 611, true);
				 api_quest_RewardQuestUser(userObjID, 611, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 611, true);
				 api_quest_RewardQuestUser(userObjID, 611, questID, 1);
			 end 
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n003_trader_dolis-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 611, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 612, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 612, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 612, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 611, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 616, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 617, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 611, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 611, false);
			 end 

	end
	if npc_talk_index == "n003_trader_dolis-accepting-acceptted" then
				api_quest_AddQuest(userObjID,61, 1);
				api_quest_SetJournalStep(userObjID,61, 1);
				api_quest_SetQuestStep(userObjID,61, 1);
				npc_talk_index = "n003_trader_dolis-1";

	end
	if npc_talk_index == "n003_trader_dolis-1-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-c" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-d" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-e" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-f" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n003_trader_dolis-3-a" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300201, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300201, api_quest_HasQuestItem(userObjID, 300201, 1));
				end
	end
	if npc_talk_index == "n003_trader_dolis-3-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq01_061_lets_party_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 61);
end

function sq01_061_lets_party_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 61);
end

function sq01_061_lets_party_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 61);
	local questID=61;
end

function sq01_061_lets_party_OnRemoteStart( userObjID, questID )
end

function sq01_061_lets_party_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq01_061_lets_party_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,61, 1);
				api_quest_SetJournalStep(userObjID,61, 1);
				api_quest_SetQuestStep(userObjID,61, 1);
				npc_talk_index = "n003_trader_dolis-1";
end

</VillageServer>

<GameServer>
function sq01_061_lets_party_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 3 then
		sq01_061_lets_party_OnTalk_n003_trader_dolis( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n003_trader_dolis--------------------------------------------------------------------------------
function sq01_061_lets_party_OnTalk_n003_trader_dolis( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n003_trader_dolis-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n003_trader_dolis-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n003_trader_dolis-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n003_trader_dolis-3";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 611, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 611, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 612, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 612, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 612, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 612, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 612, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 612, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 611, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 611, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 616, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 616, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 617, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 617, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 611, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 611, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 611, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 611, questID, 1);
			 end 
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n003_trader_dolis-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 611, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 612, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 612, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 612, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 611, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 616, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 617, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 611, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 611, false);
			 end 

	end
	if npc_talk_index == "n003_trader_dolis-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,61, 1);
				api_quest_SetJournalStep( pRoom, userObjID,61, 1);
				api_quest_SetQuestStep( pRoom, userObjID,61, 1);
				npc_talk_index = "n003_trader_dolis-1";

	end
	if npc_talk_index == "n003_trader_dolis-1-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-c" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-d" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-e" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n003_trader_dolis-3-a" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300201, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300201, api_quest_HasQuestItem( pRoom, userObjID, 300201, 1));
				end
	end
	if npc_talk_index == "n003_trader_dolis-3-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq01_061_lets_party_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 61);
end

function sq01_061_lets_party_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 61);
end

function sq01_061_lets_party_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 61);
	local questID=61;
end

function sq01_061_lets_party_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq01_061_lets_party_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq01_061_lets_party_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,61, 1);
				api_quest_SetJournalStep( pRoom, userObjID,61, 1);
				api_quest_SetQuestStep( pRoom, userObjID,61, 1);
				npc_talk_index = "n003_trader_dolis-1";
end

</GameServer>