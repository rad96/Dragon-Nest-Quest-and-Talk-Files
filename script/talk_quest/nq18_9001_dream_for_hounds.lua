<VillageServer>

function nq18_9001_dream_for_hounds_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 193 then
		nq18_9001_dream_for_hounds_OnTalk_n193_santaund(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n193_santaund--------------------------------------------------------------------------------
function nq18_9001_dream_for_hounds_OnTalk_n193_santaund(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n193_santaund-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n193_santaund-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n193_santaund-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n193_santaund-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n193_santaund-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n193_santaund-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n193_santaund-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9001, 1);
				api_quest_SetJournalStep(userObjID,9001, 1);
				api_quest_SetQuestStep(userObjID,9001, 1);
				npc_talk_index = "n193_santaund-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400199, 1);

	end
	if npc_talk_index == "n193_santaund-1-b" then 
	end
	if npc_talk_index == "n193_santaund-1-c" then 
	end
	if npc_talk_index == "n193_santaund-1-d" then 
	end
	if npc_talk_index == "n193_santaund-1-e" then 
	end
	if npc_talk_index == "n193_santaund-1-f" then 
	end
	if npc_talk_index == "n193_santaund-1-g" then 
	end
	if npc_talk_index == "n193_santaund-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n193_santaund-1-h" then 
	end
	if npc_talk_index == "n193_santaund-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n193_santaund-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n193_santaund-4" then 

				if api_quest_HasQuestItem(userObjID, 400198, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400198, api_quest_HasQuestItem(userObjID, 400198, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400199, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400199, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n193_santaund-4-a" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq18_9001_dream_for_hounds_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9001);
	if qstep == 1 and CountIndex == 400199 then
				api_quest_SetQuestStep(userObjID, questID,4);

	end
end

function nq18_9001_dream_for_hounds_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9001);
	if qstep == 1 and CountIndex == 400199 and Count >= TargetCount  then

	end
end

function nq18_9001_dream_for_hounds_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9001);
	local questID=9001;
end

function nq18_9001_dream_for_hounds_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400199, 1);
end

function nq18_9001_dream_for_hounds_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq18_9001_dream_for_hounds_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9001, 1);
				api_quest_SetJournalStep(userObjID,9001, 1);
				api_quest_SetQuestStep(userObjID,9001, 1);
				npc_talk_index = "n193_santaund-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400199, 1);
end

</VillageServer>

<GameServer>
function nq18_9001_dream_for_hounds_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 193 then
		nq18_9001_dream_for_hounds_OnTalk_n193_santaund( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n193_santaund--------------------------------------------------------------------------------
function nq18_9001_dream_for_hounds_OnTalk_n193_santaund( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n193_santaund-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n193_santaund-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n193_santaund-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n193_santaund-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n193_santaund-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n193_santaund-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n193_santaund-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9001, 1);
				api_quest_SetJournalStep( pRoom, userObjID,9001, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9001, 1);
				npc_talk_index = "n193_santaund-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400199, 1);

	end
	if npc_talk_index == "n193_santaund-1-b" then 
	end
	if npc_talk_index == "n193_santaund-1-c" then 
	end
	if npc_talk_index == "n193_santaund-1-d" then 
	end
	if npc_talk_index == "n193_santaund-1-e" then 
	end
	if npc_talk_index == "n193_santaund-1-f" then 
	end
	if npc_talk_index == "n193_santaund-1-g" then 
	end
	if npc_talk_index == "n193_santaund-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n193_santaund-1-h" then 
	end
	if npc_talk_index == "n193_santaund-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n193_santaund-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n193_santaund-4" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400198, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400198, api_quest_HasQuestItem( pRoom, userObjID, 400198, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400199, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400199, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n193_santaund-4-a" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq18_9001_dream_for_hounds_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9001);
	if qstep == 1 and CountIndex == 400199 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);

	end
end

function nq18_9001_dream_for_hounds_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9001);
	if qstep == 1 and CountIndex == 400199 and Count >= TargetCount  then

	end
end

function nq18_9001_dream_for_hounds_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9001);
	local questID=9001;
end

function nq18_9001_dream_for_hounds_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400199, 1);
end

function nq18_9001_dream_for_hounds_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq18_9001_dream_for_hounds_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9001, 1);
				api_quest_SetJournalStep( pRoom, userObjID,9001, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9001, 1);
				npc_talk_index = "n193_santaund-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400199, 1);
end

</GameServer>