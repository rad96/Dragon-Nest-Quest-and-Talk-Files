<VillageServer>

function mq08_188_clue_out_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1101 then
		mq08_188_clue_out_OnTalk_n1101_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 23 then
		mq08_188_clue_out_OnTalk_n023_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mq08_188_clue_out_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 31 then
		mq08_188_clue_out_OnTalk_n031_adventurer_wilber(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 53 then
		mq08_188_clue_out_OnTalk_n053_adventurer_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 542 then
		mq08_188_clue_out_OnTalk_n542_edan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1101_lunaria--------------------------------------------------------------------------------
function mq08_188_clue_out_OnTalk_n1101_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1101_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1101_lunaria-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1101_lunaria-2-b" then 
	end
	if npc_talk_index == "n1101_lunaria-2-c" then 
	end
	if npc_talk_index == "n1101_lunaria-2-d" then 
	end
	if npc_talk_index == "n1101_lunaria-2-e" then 
	end
	if npc_talk_index == "n1101_lunaria-2-f" then 
	end
	if npc_talk_index == "n1101_lunaria-2-g" then 
	end
	if npc_talk_index == "n1101_lunaria-2-h" then 
	end
	if npc_talk_index == "n1101_lunaria-2-i" then 
	end
	if npc_talk_index == "n1101_lunaria-2-j" then 
	end
	if npc_talk_index == "n1101_lunaria-2-k" then 
	end
	if npc_talk_index == "n1101_lunaria-2-k" then 
	end
	if npc_talk_index == "n1101_lunaria-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n023_ranger_fugus--------------------------------------------------------------------------------
function mq08_188_clue_out_OnTalk_n023_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n023_ranger_fugus-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mq08_188_clue_out_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest(userObjID,188, 2);
				api_quest_SetJournalStep(userObjID,188, 1);
				api_quest_SetQuestStep(userObjID,188, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-h" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-j" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-g" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-h" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-j" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-i" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-j" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-k" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-l" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-m" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-n" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-o" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-p" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-q" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-6-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-6-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-6-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-6-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1880, true);
				 api_quest_RewardQuestUser(userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1880, true);
				 api_quest_RewardQuestUser(userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1880, true);
				 api_quest_RewardQuestUser(userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1880, true);
				 api_quest_RewardQuestUser(userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1880, true);
				 api_quest_RewardQuestUser(userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1880, true);
				 api_quest_RewardQuestUser(userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1880, true);
				 api_quest_RewardQuestUser(userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1880, true);
				 api_quest_RewardQuestUser(userObjID, 1880, questID, 1);
			 end 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-6-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 189, 2);
					api_quest_SetQuestStep(userObjID, 189, 1);
					api_quest_SetJournalStep(userObjID, 189, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n024_adventurer_guildmaster_decud-1", "mq08_189_surprise_people.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n031_adventurer_wilber--------------------------------------------------------------------------------
function mq08_188_clue_out_OnTalk_n031_adventurer_wilber(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n031_adventurer_wilber-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n031_adventurer_wilber-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n031_adventurer_wilber-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n031_adventurer_wilber-3-b" then 
	end
	if npc_talk_index == "n031_adventurer_wilber-3-c" then 
	end
	if npc_talk_index == "n031_adventurer_wilber-3-d" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 395, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200395, 30000);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n053_adventurer_wounded--------------------------------------------------------------------------------
function mq08_188_clue_out_OnTalk_n053_adventurer_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n053_adventurer_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n053_adventurer_wounded-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n542_edan--------------------------------------------------------------------------------
function mq08_188_clue_out_OnTalk_n542_edan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n542_edan-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n542_edan-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n542_edan-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n542_edan-5-b" then 
	end
	if npc_talk_index == "n542_edan-5-c" then 
	end
	if npc_talk_index == "n542_edan-5-d" then 
	end
	if npc_talk_index == "n542_edan-5-e" then 
	end
	if npc_talk_index == "n542_edan-5-f" then 
	end
	if npc_talk_index == "n542_edan-5-g" then 
	end
	if npc_talk_index == "n542_edan-5-h" then 
	end
	if npc_talk_index == "n542_edan-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_188_clue_out_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 188);
	if qstep == 4 and CountIndex == 395 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);

	end
	if qstep == 4 and CountIndex == 200395 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);

	end
end

function mq08_188_clue_out_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 188);
	if qstep == 4 and CountIndex == 395 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 200395 and Count >= TargetCount  then

	end
end

function mq08_188_clue_out_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 188);
	local questID=188;
end

function mq08_188_clue_out_OnRemoteStart( userObjID, questID )
end

function mq08_188_clue_out_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq08_188_clue_out_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,188, 2);
				api_quest_SetJournalStep(userObjID,188, 1);
				api_quest_SetQuestStep(userObjID,188, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
end

</VillageServer>

<GameServer>
function mq08_188_clue_out_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1101 then
		mq08_188_clue_out_OnTalk_n1101_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 23 then
		mq08_188_clue_out_OnTalk_n023_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mq08_188_clue_out_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 31 then
		mq08_188_clue_out_OnTalk_n031_adventurer_wilber( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 53 then
		mq08_188_clue_out_OnTalk_n053_adventurer_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 542 then
		mq08_188_clue_out_OnTalk_n542_edan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1101_lunaria--------------------------------------------------------------------------------
function mq08_188_clue_out_OnTalk_n1101_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1101_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1101_lunaria-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1101_lunaria-2-b" then 
	end
	if npc_talk_index == "n1101_lunaria-2-c" then 
	end
	if npc_talk_index == "n1101_lunaria-2-d" then 
	end
	if npc_talk_index == "n1101_lunaria-2-e" then 
	end
	if npc_talk_index == "n1101_lunaria-2-f" then 
	end
	if npc_talk_index == "n1101_lunaria-2-g" then 
	end
	if npc_talk_index == "n1101_lunaria-2-h" then 
	end
	if npc_talk_index == "n1101_lunaria-2-i" then 
	end
	if npc_talk_index == "n1101_lunaria-2-j" then 
	end
	if npc_talk_index == "n1101_lunaria-2-k" then 
	end
	if npc_talk_index == "n1101_lunaria-2-k" then 
	end
	if npc_talk_index == "n1101_lunaria-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n023_ranger_fugus--------------------------------------------------------------------------------
function mq08_188_clue_out_OnTalk_n023_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n023_ranger_fugus-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mq08_188_clue_out_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,188, 2);
				api_quest_SetJournalStep( pRoom, userObjID,188, 1);
				api_quest_SetQuestStep( pRoom, userObjID,188, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-h" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-j" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-g" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-h" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-j" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-i" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-j" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-k" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-l" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-m" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-n" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-o" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-p" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-q" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-6-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-6-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-6-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-6-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1880, questID, 1);
			 end 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-6-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 189, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 189, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 189, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n024_adventurer_guildmaster_decud-1", "mq08_189_surprise_people.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n031_adventurer_wilber--------------------------------------------------------------------------------
function mq08_188_clue_out_OnTalk_n031_adventurer_wilber( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n031_adventurer_wilber-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n031_adventurer_wilber-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n031_adventurer_wilber-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n031_adventurer_wilber-3-b" then 
	end
	if npc_talk_index == "n031_adventurer_wilber-3-c" then 
	end
	if npc_talk_index == "n031_adventurer_wilber-3-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 395, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200395, 30000);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n053_adventurer_wounded--------------------------------------------------------------------------------
function mq08_188_clue_out_OnTalk_n053_adventurer_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n053_adventurer_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n053_adventurer_wounded-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n542_edan--------------------------------------------------------------------------------
function mq08_188_clue_out_OnTalk_n542_edan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n542_edan-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n542_edan-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n542_edan-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n542_edan-5-b" then 
	end
	if npc_talk_index == "n542_edan-5-c" then 
	end
	if npc_talk_index == "n542_edan-5-d" then 
	end
	if npc_talk_index == "n542_edan-5-e" then 
	end
	if npc_talk_index == "n542_edan-5-f" then 
	end
	if npc_talk_index == "n542_edan-5-g" then 
	end
	if npc_talk_index == "n542_edan-5-h" then 
	end
	if npc_talk_index == "n542_edan-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_188_clue_out_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 188);
	if qstep == 4 and CountIndex == 395 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

	end
	if qstep == 4 and CountIndex == 200395 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

	end
end

function mq08_188_clue_out_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 188);
	if qstep == 4 and CountIndex == 395 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 200395 and Count >= TargetCount  then

	end
end

function mq08_188_clue_out_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 188);
	local questID=188;
end

function mq08_188_clue_out_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq08_188_clue_out_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq08_188_clue_out_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,188, 2);
				api_quest_SetJournalStep( pRoom, userObjID,188, 1);
				api_quest_SetQuestStep( pRoom, userObjID,188, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
end

</GameServer>