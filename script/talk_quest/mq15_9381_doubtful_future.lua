<VillageServer>

function mq15_9381_doubtful_future_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 45 then
		mq15_9381_doubtful_future_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 875 then
		mq15_9381_doubtful_future_OnTalk_n875_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function mq15_9381_doubtful_future_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n045_soceress_master_stella-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9381, 2);
				api_quest_SetJournalStep(userObjID,9381, 1);
				api_quest_SetQuestStep(userObjID,9381, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n045_soceress_master_stella-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93810, true);
				 api_quest_RewardQuestUser(userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93810, true);
				 api_quest_RewardQuestUser(userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93810, true);
				 api_quest_RewardQuestUser(userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93810, true);
				 api_quest_RewardQuestUser(userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93810, true);
				 api_quest_RewardQuestUser(userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93810, true);
				 api_quest_RewardQuestUser(userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93810, true);
				 api_quest_RewardQuestUser(userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93810, true);
				 api_quest_RewardQuestUser(userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93810, true);
				 api_quest_RewardQuestUser(userObjID, 93810, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9382, 2);
					api_quest_SetQuestStep(userObjID, 9382, 1);
					api_quest_SetJournalStep(userObjID, 9382, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n045_soceress_master_stella-1", "mq15_9382_by_time_for_time.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n875_academic--------------------------------------------------------------------------------
function mq15_9381_doubtful_future_OnTalk_n875_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n875_academic-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n875_academic-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n875_academic-2-b" then 
	end
	if npc_talk_index == "n875_academic-2-c" then 
	end
	if npc_talk_index == "n875_academic-2-d" then 
	end
	if npc_talk_index == "n875_academic-2-e" then 
	end
	if npc_talk_index == "n875_academic-2-f" then 
	end
	if npc_talk_index == "n875_academic-2-g" then 
	end
	if npc_talk_index == "n875_academic-2-h" then 
	end
	if npc_talk_index == "n875_academic-2-i" then 
	end
	if npc_talk_index == "n875_academic-2-i" then 
	end
	if npc_talk_index == "n875_academic-2-i" then 
	end
	if npc_talk_index == "n875_academic-2-j" then 
	end
	if npc_talk_index == "n875_academic-2-k" then 
	end
	if npc_talk_index == "n875_academic-2-l" then 
	end
	if npc_talk_index == "n875_academic-2-m" then 
	end
	if npc_talk_index == "n875_academic-2-n" then 
	end
	if npc_talk_index == "n875_academic-2-n" then 
	end
	if npc_talk_index == "n875_academic-2-n" then 
	end
	if npc_talk_index == "n875_academic-2-o" then 
	end
	if npc_talk_index == "n875_academic-2-o" then 
	end
	if npc_talk_index == "n875_academic-2-o" then 
	end
	if npc_talk_index == "n875_academic-2-p" then 
	end
	if npc_talk_index == "n875_academic-2-q" then 
	end
	if npc_talk_index == "n875_academic-2-r" then 
	end
	if npc_talk_index == "n875_academic-2-s" then 
	end
	if npc_talk_index == "n875_academic-2-t" then 
	end
	if npc_talk_index == "n875_academic-2-u" then 
	end
	if npc_talk_index == "n875_academic-2-v" then 
	end
	if npc_talk_index == "n875_academic-2-w" then 
	end
	if npc_talk_index == "n875_academic-2-x" then 
	end
	if npc_talk_index == "n875_academic-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				if api_quest_HasQuestItem(userObjID, 400336, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400336, api_quest_HasQuestItem(userObjID, 400336, 1));
				end
	end
	if npc_talk_index == "n875_academic-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				if api_quest_HasQuestItem(userObjID, 400336, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400336, api_quest_HasQuestItem(userObjID, 400336, 1));
				end
	end
	if npc_talk_index == "n875_academic-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				if api_quest_HasQuestItem(userObjID, 400336, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400336, api_quest_HasQuestItem(userObjID, 400336, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9381_doubtful_future_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9381);
end

function mq15_9381_doubtful_future_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9381);
end

function mq15_9381_doubtful_future_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9381);
	local questID=9381;
end

function mq15_9381_doubtful_future_OnRemoteStart( userObjID, questID )
end

function mq15_9381_doubtful_future_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9381_doubtful_future_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9381, 2);
				api_quest_SetJournalStep(userObjID,9381, 1);
				api_quest_SetQuestStep(userObjID,9381, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
end

</VillageServer>

<GameServer>
function mq15_9381_doubtful_future_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 45 then
		mq15_9381_doubtful_future_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 875 then
		mq15_9381_doubtful_future_OnTalk_n875_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function mq15_9381_doubtful_future_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n045_soceress_master_stella-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9381, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9381, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9381, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n045_soceress_master_stella-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93810, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9382, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9382, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9382, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-1", "mq15_9382_by_time_for_time.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n875_academic--------------------------------------------------------------------------------
function mq15_9381_doubtful_future_OnTalk_n875_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n875_academic-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n875_academic-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n875_academic-2-b" then 
	end
	if npc_talk_index == "n875_academic-2-c" then 
	end
	if npc_talk_index == "n875_academic-2-d" then 
	end
	if npc_talk_index == "n875_academic-2-e" then 
	end
	if npc_talk_index == "n875_academic-2-f" then 
	end
	if npc_talk_index == "n875_academic-2-g" then 
	end
	if npc_talk_index == "n875_academic-2-h" then 
	end
	if npc_talk_index == "n875_academic-2-i" then 
	end
	if npc_talk_index == "n875_academic-2-i" then 
	end
	if npc_talk_index == "n875_academic-2-i" then 
	end
	if npc_talk_index == "n875_academic-2-j" then 
	end
	if npc_talk_index == "n875_academic-2-k" then 
	end
	if npc_talk_index == "n875_academic-2-l" then 
	end
	if npc_talk_index == "n875_academic-2-m" then 
	end
	if npc_talk_index == "n875_academic-2-n" then 
	end
	if npc_talk_index == "n875_academic-2-n" then 
	end
	if npc_talk_index == "n875_academic-2-n" then 
	end
	if npc_talk_index == "n875_academic-2-o" then 
	end
	if npc_talk_index == "n875_academic-2-o" then 
	end
	if npc_talk_index == "n875_academic-2-o" then 
	end
	if npc_talk_index == "n875_academic-2-p" then 
	end
	if npc_talk_index == "n875_academic-2-q" then 
	end
	if npc_talk_index == "n875_academic-2-r" then 
	end
	if npc_talk_index == "n875_academic-2-s" then 
	end
	if npc_talk_index == "n875_academic-2-t" then 
	end
	if npc_talk_index == "n875_academic-2-u" then 
	end
	if npc_talk_index == "n875_academic-2-v" then 
	end
	if npc_talk_index == "n875_academic-2-w" then 
	end
	if npc_talk_index == "n875_academic-2-x" then 
	end
	if npc_talk_index == "n875_academic-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				if api_quest_HasQuestItem( pRoom, userObjID, 400336, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400336, api_quest_HasQuestItem( pRoom, userObjID, 400336, 1));
				end
	end
	if npc_talk_index == "n875_academic-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				if api_quest_HasQuestItem( pRoom, userObjID, 400336, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400336, api_quest_HasQuestItem( pRoom, userObjID, 400336, 1));
				end
	end
	if npc_talk_index == "n875_academic-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				if api_quest_HasQuestItem( pRoom, userObjID, 400336, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400336, api_quest_HasQuestItem( pRoom, userObjID, 400336, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9381_doubtful_future_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9381);
end

function mq15_9381_doubtful_future_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9381);
end

function mq15_9381_doubtful_future_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9381);
	local questID=9381;
end

function mq15_9381_doubtful_future_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9381_doubtful_future_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9381_doubtful_future_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9381, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9381, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9381, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
end

</GameServer>