<VillageServer>

function mqc16_9569_penitent_man_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1782 then
		mqc16_9569_penitent_man_OnTalk_n1782_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1786 then
		mqc16_9569_penitent_man_OnTalk_n1786_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1793 then
		mqc16_9569_penitent_man_OnTalk_n1793_rose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1794 then
		mqc16_9569_penitent_man_OnTalk_n1794_rojalin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1795 then
		mqc16_9569_penitent_man_OnTalk_n1795_rojalin_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1796 then
		mqc16_9569_penitent_man_OnTalk_n1796_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mqc16_9569_penitent_man_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 40 then
		mqc16_9569_penitent_man_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 41 then
		mqc16_9569_penitent_man_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mqc16_9569_penitent_man_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mqc16_9569_penitent_man_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1782_rubinat--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n1782_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1782_rubinat-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1786_rubinat--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n1786_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1786_rubinat-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1786_rubinat-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1786_rubinat-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1786_rubinat-7-b" then 
	end
	if npc_talk_index == "n1786_rubinat-7-c" then 
	end
	if npc_talk_index == "n1786_rubinat-7-d" then 
	end
	if npc_talk_index == "n1786_rubinat-7-d" then 
	end
	if npc_talk_index == "n1786_rubinat-7-d" then 
	end
	if npc_talk_index == "n1786_rubinat-7-e" then 
	end
	if npc_talk_index == "n1786_rubinat-7-e" then 
	end
	if npc_talk_index == "n1786_rubinat-7-e" then 
	end
	if npc_talk_index == "n1786_rubinat-7-f" then 
	end
	if npc_talk_index == "n1786_rubinat-7-g" then 
	end
	if npc_talk_index == "n1786_rubinat-7-h" then 
	end
	if npc_talk_index == "n1786_rubinat-7-h" then 
	end
	if npc_talk_index == "n1786_rubinat-7-i" then 
	end
	if npc_talk_index == "n1786_rubinat-7-i" then 
	end
	if npc_talk_index == "n1786_rubinat-7-j" then 
	end
	if npc_talk_index == "n1786_rubinat-7-k" then 
	end
	if npc_talk_index == "n1786_rubinat-7-l" then 
	end
	if npc_talk_index == "n1786_rubinat-7-l" then 
	end
	if npc_talk_index == "n1786_rubinat-7-m" then 
	end
	if npc_talk_index == "n1786_rubinat-7-m" then 
	end
	if npc_talk_index == "n1786_rubinat-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 8);
	end
	if npc_talk_index == "n1786_rubinat-8-class_check_5" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1786_rubinat-8-b";

				else
									npc_talk_index = "n1786_rubinat-8-d";

				end
	end
	if npc_talk_index == "n1786_rubinat-8-c" then 
	end
	if npc_talk_index == "n1786_rubinat-8-d" then 
	end
	if npc_talk_index == "n1786_rubinat-8-d" then 
	end
	if npc_talk_index == "n1786_rubinat-8-e" then 
	end
	if npc_talk_index == "n1786_rubinat-8-e" then 
	end
	if npc_talk_index == "n1786_rubinat-8-f" then 
	end
	if npc_talk_index == "n1786_rubinat-8-f" then 
	end
	if npc_talk_index == "n1786_rubinat-8-f" then 
	end
	if npc_talk_index == "n1786_rubinat-8-g" then 
	end
	if npc_talk_index == "n1786_rubinat-9" then 
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 9);
	end
	if npc_talk_index == "n1786_rubinat-9-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 end 
	end
	if npc_talk_index == "n1786_rubinat-9-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95690, true);
				 api_quest_RewardQuestUser(userObjID, 95690, questID, 1);
			 end 
	end
	if npc_talk_index == "n1786_rubinat-9-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1793_rose--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n1793_rose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1793_rose-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1793_rose-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1793_rose-4-b" then 
	end
	if npc_talk_index == "n1793_rose-4-b" then 
	end
	if npc_talk_index == "n1793_rose-4-b" then 
	end
	if npc_talk_index == "n1793_rose-4-class_check" then 
				if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n1793_rose-4-e";

				else
									npc_talk_index = "n1793_rose-4-c";

				end
	end
	if npc_talk_index == "n1793_rose-4-d" then 
	end
	if npc_talk_index == "n1793_rose-4-d" then 
	end
	if npc_talk_index == "n1793_rose-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n1793_rose-4-f" then 
	end
	if npc_talk_index == "n1793_rose-4-g" then 
	end
	if npc_talk_index == "n1793_rose-4-h" then 
	end
	if npc_talk_index == "n1793_rose-4-i" then 
	end
	if npc_talk_index == "n1793_rose-4-j" then 
	end
	if npc_talk_index == "n1793_rose-4-k" then 
	end
	if npc_talk_index == "n1793_rose-4-k" then 
	end
	if npc_talk_index == "n1793_rose-4-l" then 
	end
	if npc_talk_index == "n1793_rose-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1794_rojalin--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n1794_rojalin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1794_rojalin-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1794_rojalin-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1794_rojalin-6-b" then 
	end
	if npc_talk_index == "n1794_rojalin-6-c" then 
	end
	if npc_talk_index == "n1794_rojalin-6-class_check_3" then 
				if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n1794_rojalin-6-d";

				else
									npc_talk_index = "n1794_rojalin-7";
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 7);

				end
	end
	if npc_talk_index == "n1794_rojalin-6-e" then 
	end
	if npc_talk_index == "n1794_rojalin-6-f" then 
	end
	if npc_talk_index == "n1794_rojalin-6-g" then 
	end
	if npc_talk_index == "n1794_rojalin-6-h" then 
	end
	if npc_talk_index == "n1794_rojalin-6-i" then 
	end
	if npc_talk_index == "n1794_rojalin-6-i" then 
	end
	if npc_talk_index == "n1794_rojalin-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1795_rojalin_memory--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n1795_rojalin_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1795_rojalin_memory-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1795_rojalin_memory-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1795_rojalin_memory-5-class_check_2" then 
				if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n1795_rojalin_memory-5-a";

				else
									npc_talk_index = "n1795_rojalin_memory-6";
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);

				end
	end
	if npc_talk_index == "n1795_rojalin_memory-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1796_rubinat--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n1796_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1796_rubinat-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1796_rubinat-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1796_rubinat-3-b" then 
	end
	if npc_talk_index == "n1796_rubinat-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1796_rubinat-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n040_king_casius--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n041_duke_stwart--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n822_teramai-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9569, 2);
				api_quest_SetJournalStep(userObjID,9569, 1);
				api_quest_SetQuestStep(userObjID,9569, 1);
				npc_talk_index = "n822_teramai-1";

	end
	if npc_talk_index == "n822_teramai-1-b" then 
	end
	if npc_talk_index == "n822_teramai-1-c" then 
	end
	if npc_talk_index == "n822_teramai-1-d" then 
	end
	if npc_talk_index == "n822_teramai-1-e" then 
	end
	if npc_talk_index == "n822_teramai-1-f" then 
	end
	if npc_talk_index == "n822_teramai-1-g" then 
	end
	if npc_talk_index == "n822_teramai-1-h" then 
	end
	if npc_talk_index == "n822_teramai-1-i" then 
	end
	if npc_talk_index == "n822_teramai-1-j" then 
	end
	if npc_talk_index == "n822_teramai-1-k" then 
	end
	if npc_talk_index == "n822_teramai-1-k" then 
	end
	if npc_talk_index == "n822_teramai-1-l" then 
	end
	if npc_talk_index == "n822_teramai-1-m" then 
	end
	if npc_talk_index == "n822_teramai-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n822_teramai-2-b" then 
	end
	if npc_talk_index == "n822_teramai-2-c" then 
	end
	if npc_talk_index == "n822_teramai-2-d" then 
	end
	if npc_talk_index == "n822_teramai-2-e" then 
	end
	if npc_talk_index == "n822_teramai-2-f" then 
	end
	if npc_talk_index == "n822_teramai-2-g" then 
	end
	if npc_talk_index == "n822_teramai-2-h" then 
	end
	if npc_talk_index == "n822_teramai-2-i" then 
	end
	if npc_talk_index == "n822_teramai-2-j" then 
	end
	if npc_talk_index == "n822_teramai-2-k" then 
	end
	if npc_talk_index == "n822_teramai-2-l" then 
	end
	if npc_talk_index == "n822_teramai-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9569_penitent_man_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9569);
end

function mqc16_9569_penitent_man_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9569);
end

function mqc16_9569_penitent_man_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9569);
	local questID=9569;
end

function mqc16_9569_penitent_man_OnRemoteStart( userObjID, questID )
end

function mqc16_9569_penitent_man_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc16_9569_penitent_man_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9569, 2);
				api_quest_SetJournalStep(userObjID,9569, 1);
				api_quest_SetQuestStep(userObjID,9569, 1);
				npc_talk_index = "n822_teramai-1";
end

</VillageServer>

<GameServer>
function mqc16_9569_penitent_man_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1782 then
		mqc16_9569_penitent_man_OnTalk_n1782_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1786 then
		mqc16_9569_penitent_man_OnTalk_n1786_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1793 then
		mqc16_9569_penitent_man_OnTalk_n1793_rose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1794 then
		mqc16_9569_penitent_man_OnTalk_n1794_rojalin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1795 then
		mqc16_9569_penitent_man_OnTalk_n1795_rojalin_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1796 then
		mqc16_9569_penitent_man_OnTalk_n1796_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mqc16_9569_penitent_man_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 40 then
		mqc16_9569_penitent_man_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 41 then
		mqc16_9569_penitent_man_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mqc16_9569_penitent_man_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mqc16_9569_penitent_man_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1782_rubinat--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n1782_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1782_rubinat-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1786_rubinat--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n1786_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1786_rubinat-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1786_rubinat-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1786_rubinat-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1786_rubinat-7-b" then 
	end
	if npc_talk_index == "n1786_rubinat-7-c" then 
	end
	if npc_talk_index == "n1786_rubinat-7-d" then 
	end
	if npc_talk_index == "n1786_rubinat-7-d" then 
	end
	if npc_talk_index == "n1786_rubinat-7-d" then 
	end
	if npc_talk_index == "n1786_rubinat-7-e" then 
	end
	if npc_talk_index == "n1786_rubinat-7-e" then 
	end
	if npc_talk_index == "n1786_rubinat-7-e" then 
	end
	if npc_talk_index == "n1786_rubinat-7-f" then 
	end
	if npc_talk_index == "n1786_rubinat-7-g" then 
	end
	if npc_talk_index == "n1786_rubinat-7-h" then 
	end
	if npc_talk_index == "n1786_rubinat-7-h" then 
	end
	if npc_talk_index == "n1786_rubinat-7-i" then 
	end
	if npc_talk_index == "n1786_rubinat-7-i" then 
	end
	if npc_talk_index == "n1786_rubinat-7-j" then 
	end
	if npc_talk_index == "n1786_rubinat-7-k" then 
	end
	if npc_talk_index == "n1786_rubinat-7-l" then 
	end
	if npc_talk_index == "n1786_rubinat-7-l" then 
	end
	if npc_talk_index == "n1786_rubinat-7-m" then 
	end
	if npc_talk_index == "n1786_rubinat-7-m" then 
	end
	if npc_talk_index == "n1786_rubinat-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);
	end
	if npc_talk_index == "n1786_rubinat-8-class_check_5" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1786_rubinat-8-b";

				else
									npc_talk_index = "n1786_rubinat-8-d";

				end
	end
	if npc_talk_index == "n1786_rubinat-8-c" then 
	end
	if npc_talk_index == "n1786_rubinat-8-d" then 
	end
	if npc_talk_index == "n1786_rubinat-8-d" then 
	end
	if npc_talk_index == "n1786_rubinat-8-e" then 
	end
	if npc_talk_index == "n1786_rubinat-8-e" then 
	end
	if npc_talk_index == "n1786_rubinat-8-f" then 
	end
	if npc_talk_index == "n1786_rubinat-8-f" then 
	end
	if npc_talk_index == "n1786_rubinat-8-f" then 
	end
	if npc_talk_index == "n1786_rubinat-8-g" then 
	end
	if npc_talk_index == "n1786_rubinat-9" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 9);
	end
	if npc_talk_index == "n1786_rubinat-9-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 end 
	end
	if npc_talk_index == "n1786_rubinat-9-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95690, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95690, questID, 1);
			 end 
	end
	if npc_talk_index == "n1786_rubinat-9-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1793_rose--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n1793_rose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1793_rose-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1793_rose-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1793_rose-4-b" then 
	end
	if npc_talk_index == "n1793_rose-4-b" then 
	end
	if npc_talk_index == "n1793_rose-4-b" then 
	end
	if npc_talk_index == "n1793_rose-4-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n1793_rose-4-e";

				else
									npc_talk_index = "n1793_rose-4-c";

				end
	end
	if npc_talk_index == "n1793_rose-4-d" then 
	end
	if npc_talk_index == "n1793_rose-4-d" then 
	end
	if npc_talk_index == "n1793_rose-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n1793_rose-4-f" then 
	end
	if npc_talk_index == "n1793_rose-4-g" then 
	end
	if npc_talk_index == "n1793_rose-4-h" then 
	end
	if npc_talk_index == "n1793_rose-4-i" then 
	end
	if npc_talk_index == "n1793_rose-4-j" then 
	end
	if npc_talk_index == "n1793_rose-4-k" then 
	end
	if npc_talk_index == "n1793_rose-4-k" then 
	end
	if npc_talk_index == "n1793_rose-4-l" then 
	end
	if npc_talk_index == "n1793_rose-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1794_rojalin--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n1794_rojalin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1794_rojalin-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1794_rojalin-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1794_rojalin-6-b" then 
	end
	if npc_talk_index == "n1794_rojalin-6-c" then 
	end
	if npc_talk_index == "n1794_rojalin-6-class_check_3" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n1794_rojalin-6-d";

				else
									npc_talk_index = "n1794_rojalin-7";
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);

				end
	end
	if npc_talk_index == "n1794_rojalin-6-e" then 
	end
	if npc_talk_index == "n1794_rojalin-6-f" then 
	end
	if npc_talk_index == "n1794_rojalin-6-g" then 
	end
	if npc_talk_index == "n1794_rojalin-6-h" then 
	end
	if npc_talk_index == "n1794_rojalin-6-i" then 
	end
	if npc_talk_index == "n1794_rojalin-6-i" then 
	end
	if npc_talk_index == "n1794_rojalin-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1795_rojalin_memory--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n1795_rojalin_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1795_rojalin_memory-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1795_rojalin_memory-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1795_rojalin_memory-5-class_check_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n1795_rojalin_memory-5-a";

				else
									npc_talk_index = "n1795_rojalin_memory-6";
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);

				end
	end
	if npc_talk_index == "n1795_rojalin_memory-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1796_rubinat--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n1796_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1796_rubinat-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1796_rubinat-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1796_rubinat-3-b" then 
	end
	if npc_talk_index == "n1796_rubinat-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1796_rubinat-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n040_king_casius--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n041_duke_stwart--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mqc16_9569_penitent_man_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n822_teramai-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9569, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9569, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9569, 1);
				npc_talk_index = "n822_teramai-1";

	end
	if npc_talk_index == "n822_teramai-1-b" then 
	end
	if npc_talk_index == "n822_teramai-1-c" then 
	end
	if npc_talk_index == "n822_teramai-1-d" then 
	end
	if npc_talk_index == "n822_teramai-1-e" then 
	end
	if npc_talk_index == "n822_teramai-1-f" then 
	end
	if npc_talk_index == "n822_teramai-1-g" then 
	end
	if npc_talk_index == "n822_teramai-1-h" then 
	end
	if npc_talk_index == "n822_teramai-1-i" then 
	end
	if npc_talk_index == "n822_teramai-1-j" then 
	end
	if npc_talk_index == "n822_teramai-1-k" then 
	end
	if npc_talk_index == "n822_teramai-1-k" then 
	end
	if npc_talk_index == "n822_teramai-1-l" then 
	end
	if npc_talk_index == "n822_teramai-1-m" then 
	end
	if npc_talk_index == "n822_teramai-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n822_teramai-2-b" then 
	end
	if npc_talk_index == "n822_teramai-2-c" then 
	end
	if npc_talk_index == "n822_teramai-2-d" then 
	end
	if npc_talk_index == "n822_teramai-2-e" then 
	end
	if npc_talk_index == "n822_teramai-2-f" then 
	end
	if npc_talk_index == "n822_teramai-2-g" then 
	end
	if npc_talk_index == "n822_teramai-2-h" then 
	end
	if npc_talk_index == "n822_teramai-2-i" then 
	end
	if npc_talk_index == "n822_teramai-2-j" then 
	end
	if npc_talk_index == "n822_teramai-2-k" then 
	end
	if npc_talk_index == "n822_teramai-2-l" then 
	end
	if npc_talk_index == "n822_teramai-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9569_penitent_man_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9569);
end

function mqc16_9569_penitent_man_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9569);
end

function mqc16_9569_penitent_man_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9569);
	local questID=9569;
end

function mqc16_9569_penitent_man_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc16_9569_penitent_man_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc16_9569_penitent_man_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9569, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9569, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9569, 1);
				npc_talk_index = "n822_teramai-1";
end

</GameServer>