
g_lua_CoolTimeTable1 = 
{

	Beholder_Attack_Assault = 3000,
	Beholder_Attack_CenterEye_Magic = 13000,
	Beholder_Attack_Jump = 7000,
	Beholder_Attack_Poison = 8000,
	Beholder_Attack_Shout = -1,
	Beholder_Attack_Side_Eye_Magic = 4000,
	Beholder_Attack_Spin = -1,
	Beholder_Stand = 15000,

--[[
	Beholder_Attack_Assault = -1,
	Beholder_Attack_CenterEye_Magic = -1,
	Beholder_Attack_Jump = -1,
	Beholder_Attack_Poison = -1,
	Beholder_Attack_Shout = -1,
	Beholder_Attack_Side_Eye_Magic = -1,
	Beholder_Attack_Spin = -1,
	Beholder_Stand = -1,
--]]	
	Beholder_Move_Back= 0,
	Beholder_Move_Front= 0,
	Beholder_Move_Left= 0,
	Beholder_Move_Right= 0,

	Beholder_Walk_Back= 5000,
	Beholder_Walk_Front= 5000,
	Beholder_Walk_Left= 5000,
	Beholder_Walk_Right= 5000,	

}

g_lua_CoolTimeTable2 = 
{
	Beholder_Attack_Assault = 9000,
	Beholder_Attack_CenterEye_Magic = 3000,
	Beholder_Attack_Jump = 4000,
	Beholder_Attack_Poison = 5000,
	Beholder_Attack_Shout = 7000,
	Beholder_Attack_Side_Eye_Magic = 4000,
	Beholder_Attack_Spin = 15000,
	Beholder_Stand = 20000,
	
	Beholder_Move_Back= -1,
	Beholder_Move_Front= -1,
	Beholder_Move_Left= -1,
	Beholder_Move_Right= -1,

	Beholder_Walk_Back= 5000,
	Beholder_Walk_Front= 5000,
	Beholder_Walk_Left= 5000,
	Beholder_Walk_Right= 5000,
}

g_lua_CoolTimeTable3 = 
{
	Beholder_Attack_Assault = 6000,
	Beholder_Attack_CenterEye_Magic = 12000,
	Beholder_Attack_Jump = 4000,
	Beholder_Attack_Poison = 5000,
	Beholder_Attack_Shout = 7000,
	Beholder_Attack_Side_Eye_Magic = 12000,
	Beholder_Attack_Spin = 9000,
	Beholder_Stand = 30000,

	Beholder_Move_Back= 5000,
	Beholder_Move_Front= 5000,
	Beholder_Move_Left= 5000,
	Beholder_Move_Right= 5000,

	Beholder_Walk_Back= -1,
	Beholder_Walk_Front= -1,
	Beholder_Walk_Left= -1,
	Beholder_Walk_Right= -1
}

g_lua_CoolTimeTable4 = 
{
	Beholder_Attack_Assault = 19000,
	Beholder_Attack_CenterEye_Magic = 3000,
	Beholder_Attack_Jump = 14000,
	Beholder_Attack_Poison = 15000,
	Beholder_Attack_Shout = 7000,
	Beholder_Attack_Side_Eye_Magic = 4000,
	Beholder_Attack_Spin = 5000,
	Beholder_Stand = 60000,

	Beholder_Move_Back= 5000,
	Beholder_Move_Front= 5000,
	Beholder_Move_Left= 5000,
	Beholder_Move_Right= 5000,

	Beholder_Walk_Back= -1,
	Beholder_Walk_Front= -1,
	Beholder_Walk_Left= -1,
	Beholder_Walk_Right= -1
}
