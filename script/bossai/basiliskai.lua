-- Basilisk ai

g_lua_KeepupTimeTable = 
{
	Ground		= 60000,
	Pillar		= 15000,
	Vaulting	= 3000,
	OverTurned	= 15000,
}

g_lua_CoolTimeTable = 
{
	Ground_Stand = 0,
	Ground_TailAttack = 6000,
	Ground_BodyAttack = 10000,
	Ground_RunAttack = 26000,
	Ground_StandUp = 0,
	Ground_StandDead = 0,

	Turn_Left = 2000,
	Turn_Right = 2000,
	FootAttack_Left = 8000,
	FootAttack_Right = 8000,

	OverTurned = 0,
	OverTurned_Dead = 0,
	OverTurned_Hit = 0,

	Pillar_Climb = 0,
	Pillar_Stand = 0,
	Pillar_ThornAttack = 2000,
	Pillar_LoseBalance = 0,
	Pillar_KeepBalance = 0,
	Pillar_Fall = 0,
	Pillar_ComeDown = 0,

	Vaulting_Climb = 0,
	Vaulting_Stand = 0,
	Vaulting_ThornAttack = 3000,
	Vaulting_LoseBalance = 0,
	Vaulting_KeepBalance = 0,
	Vaulting_Fall = 0,
}

-- 바실리스크 정보

g_luaBasiliskPropertyTable =
{
	-- 기둥에 기어 올라갈때 소환할 몬스터 그룹 id
	nGenerationMonsterGroupID = 111;
	-- 기둥에 기어 올라갈때 소환할 몬스터 카운트
	nGenerationMonsterCount 	= 4;

	-- 몬스터 소환할때 현재 액터가 이 숫자 이상이면 더이상 소환하지 않는다.
	nMaxActorSize	= 50;

	-- 아머 하나당 내구도
--	fArmorDurability = 50;
	fArmorDurability = 1;

	-- 아머 하나당 방어력 퍼센트
	fArmorDefencePercent = 8;

	-- 아머 갯수
	fArmorCnt = 12;

	-- 원거리 공격 방어력 퍼센트
	fRangeDefencePercent = 25;

	-- 땅에 떨어졌을때 아머가 깍이는 내구도 
	fFall_to_the_ground_damage = 120;

	-- 다리 갯수
	fLegCnt = 8;
	
	-- 다리 맞았을때 내구도 깍이는 값
	fLegDurabilityDamage = 60;
	
	-- 다리 하나당 내구도
	fLegDurability = 100;

	-- 다리의 내구도 시간당 회복값
	fLegDurabilityRepairValue = 3;

	-- 다리의 내구도 회복 시간
	fLegDurabilityRepairTime   = 1000;

	-- 다리 내구도가 얼마나 떨어졌을때 밸런스를 잃어버리냐.
	fLoseBalanceValue = 200;
	fLoseBalanceValue = 2;
	
	-- 다리 내구도가 얼마나 떨어졌을때 밸런스 잃어버린 상태에서 떨어지냐
	fFallDownValue = 60;
	fFallDownValue = 6;
}


--[[
		nState		:  현재 바실리스크의 상태  1이면 Overturned 
		AttackType  :  0 이면 근거리 1이면 원거리 
		AttackFoot  :  1이면 다리공격 0 이면 딴부위
		nOriginalDamage : 원래 데미지값, 
		nArmorCnt	:  남아있는 껍질 갯수
		
]]--
function lua_CalcDamage( nOverTurned, AttackType, AttackFoot, nOriginalDamage, nArmorCnt )

	local	nResultDamage = 0.0;
	local   nAttackFoot = 100;
	
	-- 발을 공격하면 50% 추가
	if ( AttackFoot == 1 ) then
		nAttackFoot = 150;
	end;
		
	-- overturned
	if ( nOverTurned == 1 ) then
		-- 근거리 공격일경우
		if ( AttackType == 0 ) then 
			nResultDamage = nOriginalDamage;
		
		-- 원거리 공격일경우
		else
			nResultDamage = nOriginalDamage * (g_luaBasiliskPropertyTable.nRangeDefencePercent/100) ;
		end

	-- not overturned 			
	else
		-- 근거리 공격일경우
		if ( AttackType == 0 ) then 
			nResultDamage = nOriginalDamage * ( 1 - ( nArmorCnt * g_luaBasiliskPropertyTable.nArmorDefencePercent + nAttackFoot )/100 );
		
		-- 원거리 공격일경우
		else
			nResultDamage = nOriginalDamage * (g_luaBasiliskPropertyTable.nRangeDefencePercent/100) * ( 1 - ( nArmorCnt * g_luaBasiliskPropertyTable.nArmorDefencePercent + nAttackFoot) /100 );
		end
	
	end
	
	return nResultDamage;
end
